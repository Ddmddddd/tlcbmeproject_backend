package aliyun

import (
	"testing"
)

func TestSms_getFields(t *testing.T) {
	sms := &Sms{
		AccessKeyId:  "testId",
		AccessSecret: "testSecret",
		//Timestamp: time.Now().Format("2006-01-02T15:04:05Z"),
		Timestamp:        "2017-07-12T02:42:19Z",
		Format:           "XML",
		SignatureMethod:  "HMAC-SHA1",
		SignatureVersion: "1.0",
		SignatureNonce:   "45e25e9b-0a6f-4070-8c85-2956eda1b466",
		Signature:        "",

		// 业务参数
		Action:        "SendSms",
		Version:       "2017-05-25",
		RegionId:      "cn-hangzhou",
		PhoneNumbers:  "15300000001",
		SignName:      "阿里云短信测试专用",
		TemplateCode:  "SMS_71390007",
		TemplateParam: `{"customer":"test"}`,
		OutId:         "123",
	}

	//fields := aliyunsms.getFields()
	//for i, v := range fields {
	//	t.Log(i+1, "-", v.Key, ":", v.Value)
	//}

	expectParam := "Signature=zJDF%2BLrzhj%2FThnlvIToysFRq6t4%3D&AccessKeyId=testId&Action=SendSms&Format=XML&OutId=123&PhoneNumbers=15300000001&RegionId=cn-hangzhou&SignName=%E9%98%BF%E9%87%8C%E4%BA%91%E7%9F%AD%E4%BF%A1%E6%B5%8B%E8%AF%95%E4%B8%93%E7%94%A8&SignatureMethod=HMAC-SHA1&SignatureNonce=45e25e9b-0a6f-4070-8c85-2956eda1b466&SignatureVersion=1.0&TemplateCode=SMS_71390007&TemplateParam=%7B%22customer%22%3A%22test%22%7D&Timestamp=2017-07-12T02%3A42%3A19Z&Version=2017-05-25"
	actualParam := sms.GetParam()
	if expectParam != actualParam {
		t.Log("expect param:", expectParam)
		t.Log("actual param:", expectParam)
		t.Error("param are not same")
	}
}

func TestSms_sign(t *testing.T) {
	key := "testSecret&"
	value := "GET&%2F&AccessKeyId%3DtestId%26Action%3DSendSms%26Format%3DXML%26OutId%3D123%26PhoneNumbers%3D15300000001%26RegionId%3Dcn-hangzhou%26SignName%3D%25E9%2598%25BF%25E9%2587%258C%25E4%25BA%2591%25E7%259F%25AD%25E4%25BF%25A1%25E6%25B5%258B%25E8%25AF%2595%25E4%25B8%2593%25E7%2594%25A8%26SignatureMethod%3DHMAC-SHA1%26SignatureNonce%3D45e25e9b-0a6f-4070-8c85-2956eda1b466%26SignatureVersion%3D1.0%26TemplateCode%3DSMS_71390007%26TemplateParam%3D%257B%2522customer%2522%253A%2522test%2522%257D%26Timestamp%3D2017-07-12T02%253A42%253A19Z%26Version%3D2017-05-25"
	signature := "zJDF+Lrzhj/ThnlvIToysFRq6t4="
	signatureEncoded := "zJDF%2BLrzhj%2FThnlvIToysFRq6t4%3D"

	sms := &Sms{}
	result := sms.sign(key, value)
	if result != signature {
		t.Fatal("sign result error: expect =", signature, "; actual =", result)
	}

	resultEncoded := sms.specialEncodeUrl(result)
	if resultEncoded != signatureEncoded {
		t.Fatal("encode result error: expect =", signatureEncoded, "; actual =", resultEncoded)
	}
}
