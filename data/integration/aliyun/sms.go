package aliyun

import (
	"crypto/hmac"
	"crypto/sha1"
	"encoding/base64"
	"fmt"
	"net/url"
	"reflect"
	"sort"
	"strings"
)

// https://help.aliyun.com/document_detail/56189.html?spm=a2c4g.11186623.6.590.3b4357771dZ8TG
type Sms struct {
	// 系统参数
	AccessKeyId      string `key:"AccessKeyId" required:"true" note:"ID"`
	AccessSecret     string `key:"-" required:"true" note:"签名密钥"`
	Timestamp        string `key:"Timestamp" required:"true" note:"格式为：yyyy-MM-dd’T’HH:mm:ss’Z’；时区为：GMT"`
	Format           string `key:"Format" required:"false" note:"没传默认为JSON，可选填值：XML"`
	SignatureMethod  string `key:"SignatureMethod" required:"true" note:"建议固定值：HMAC-SHA1"`
	SignatureVersion string `key:"SignatureVersion" required:"true" note:"建议固定值：1.0"`
	SignatureNonce   string `key:"SignatureNonce" required:"true" note:"用于请求的防重放攻击，每次请求唯一，JAVA语言建议用：java.util.UUID.randomUUID()生成即可"`
	Signature        string `key:"-" required:"true" note:"最终生成的签名结果值"`

	// 业务参数
	Action        string `key:"Action" required:"true" note:"API的命名，固定值，如发送短信API的值为：SendSms"`
	Version       string `key:"Version" required:"true" note:"API的版本，固定值，如短信API的值为：2017-05-25"`
	RegionId      string `key:"RegionId" required:"true" note:"API支持的RegionID，如短信API的值为：cn-hangzhou"`
	PhoneNumbers  string `key:"PhoneNumbers" required:"true" note:"短信接收号码,支持以逗号分隔的形式进行批量调用，批量上限为1000个手机号码"`
	SignName      string `key:"SignName" required:"true" note:"短信签名,例如：云通信"`
	TemplateCode  string `key:"TemplateCode" required:"true" note:"短信模板ID,例如：SMS_0000"`
	TemplateParam string `key:"TemplateParam" required:"false" note:"短信模板变量替换JSON串,例如：{“code”:”1234”,”product”:”ytx”}"`
	OutId         string `key:"OutId" required:"false" note:"外部流水扩展字段,例如：abcdefgh"`
}

func (s *Sms) GetParam() string {
	params := url.Values{}
	fields := s.getFields()
	for _, v := range fields {
		params.Add(v.Key, fmt.Sprint(v.Value))
	}

	rawParam := params.Encode()
	sbSignParam := strings.Builder{}
	sbSignParam.WriteString("GET&%2F&")
	sbSignParam.WriteString(s.specialEncodeUrl(rawParam))

	key := s.AccessSecret + "&"
	signature := s.sign(key, sbSignParam.String())

	param := strings.Builder{}
	param.WriteString("Signature=")
	param.WriteString(s.specialEncodeUrl(signature))
	param.WriteString("&")
	param.WriteString(rawParam)

	return param.String()
}

func (s *Sms) sign(key, value string) string {
	h := hmac.New(sha1.New, []byte(key))
	h.Write([]byte(value))
	bytes := h.Sum(nil)

	return base64.StdEncoding.EncodeToString(bytes)
}

func (s *Sms) specialEncodeUrl(v string) string {
	v0 := url.QueryEscape(v)

	// POP规则, 在一般的URLEncode后再增加三种字符替换:
	// 加号（+）替换成 %20
	// 星号（*）替换成 %2A
	// %7E 替换回波浪号（~）
	v1 := strings.Replace(v0, "+", "%20", -1)
	v2 := strings.Replace(v1, "*", "%2A", -1)
	v3 := strings.Replace(v2, "%7E", "~", -1)

	return v3
}

func (s *Sms) getFields() []*smsField {
	fields := make(map[string]*smsField)

	v := reflect.ValueOf(s).Elem()
	n := v.NumField()
	t := v.Type()
	for i := 0; i < n; i++ {
		valueField := v.Field(i)
		// ignore private field
		if !valueField.CanInterface() {
			continue
		}
		if !valueField.CanAddr() {
			continue
		}

		typeField := t.Field(i)
		// filed define
		fieldName := typeField.Tag.Get("key")
		if fieldName == "-" {
			continue
		}
		if fieldName == "" {
			fieldName = typeField.Name
		}

		info := smsField{Key: fieldName}
		info.Value = valueField.Interface()
		fields[fieldName] = &info
	}

	keys := make([]string, 0)
	for k, _ := range fields {
		if k == "-" {
			continue
		}
		keys = append(keys, k)
	}
	sort.Strings(keys)

	results := make([]*smsField, 0)
	for _, k := range keys {
		v, ok := fields[k]
		if !ok {
			continue
		}
		if v.valueEmpty() {
			continue
		}

		results = append(results, v)
	}

	return results
}

type smsField struct {
	Key   string
	Value interface{}
}

func (s *smsField) valueEmpty() bool {
	if s.Value == nil {
		return true
	}
	v := reflect.ValueOf(s.Value)
	switch v.Kind() {
	case reflect.Chan, reflect.Func, reflect.Map, reflect.Ptr, reflect.Interface, reflect.Slice:
		if v.IsNil() {
			return true
		}
	}

	if v.Kind() == reflect.Interface {
		v = v.Elem()
	}
	if v.Kind() == reflect.Ptr {
		if v.IsNil() {
			return true
		}
		v = v.Elem()
	}

	ev := fmt.Sprint(v)
	if len(ev) == 0 {
		return true
	}

	return false
}
