package nutrientdb

import "tlcbme_project/data/model/nutrient"

type Recipe_composeBase struct {
}

func (s Recipe_composeBase) TableName() string {
	return "recipe_compose"
}

// 菜肴组成
type Recipe_compose struct {
	Recipe_composeBase
	// 序号 主键
	Serial_No uint64 `sql:"Serial_No" auto:"true" primary:"true"`
	// 菜肴编号 recipe.ID
	RECIPE_ID string `sql:"RECIPE_ID"`
	// 组成食物名称 菜肴的一种组成食物
	FOOD_NAME *string `sql:"FOOD_NAME"`
	// 食物成分序号
	FOOD_ID *string `sql:"FOOD_ID"`
	// 实际每份净重
	WEIGHT float64 `sql:"WEIGHT"`
	// 净重
	NET_WEIGHT *string `sql:"NET_WEIGHT"`
}

func (s *Recipe_compose) CopyTo(target *nutrient.RecipeCompose) {
	if target == nil {
		return
	}
	target.SerialNo = s.Serial_No
	target.RecipeID = s.RECIPE_ID
	if s.FOOD_NAME != nil {
		target.FoodName = string(*s.FOOD_NAME)
	}
	if s.FOOD_ID != nil {
		target.FoodID = string(*s.FOOD_ID)
	}
	target.WEIGHT = s.WEIGHT
	if s.NET_WEIGHT != nil {
		target.NetWeight = string(*s.NET_WEIGHT)
	}
}

func (s *Recipe_compose) CopyFrom(source *nutrient.RecipeCompose) {
	if source == nil {
		return
	}
	s.Serial_No = source.SerialNo
	s.RECIPE_ID = source.RecipeID
	fOOD_NAME := string(source.FoodName)
	s.FOOD_NAME = &fOOD_NAME
	fOOD_ID := string(source.FoodID)
	s.FOOD_ID = &fOOD_ID
	s.WEIGHT = source.WEIGHT
	nET_WEIGHT := string(source.NetWeight)
	s.NET_WEIGHT = &nET_WEIGHT
}
