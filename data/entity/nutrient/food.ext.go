package nutrientdb

import (
	"fmt"
	"tlcbme_project/data/model/doctor"
	"tlcbme_project/data/model/nutrient"
)

type FoodNameFilter struct {
	Alias string `sql:"Alias" filter:"like"`
	// 食物名称
	NAME string `sql:"NAME" filter:"like"`
}

type VersionFilter struct {
	// 版本
	Version1 uint64 `sql:"Version"`
	Version2 uint64 `sql:"Version"`
}

type FoodIDFilter struct {
	// 食物ID
	ID *string `sql:"ID"`
}

func (s *FoodNameFilter) CopyFrom(source *doctor.FoodSearchFilter) {
	if source == nil {
		return
	}

	if len(source.Name) > 0 {
		s.Alias = fmt.Sprint("%", source.Name, "%")
		s.NAME = fmt.Sprint("%", source.Name, "%")
	}
}

type FoodAlias struct {
	FoodBase
	Frequency_Flag uint64 `sql:"Frequency_Flag"`
	Alias          string `sql:"Alias"`
	//
	Serial_No uint64 `sql:"Serial_No" auto:"true" primary:"true"`
}

func (s *Food) CopyToEx(target *nutrient.FoodSearchResult) {
	if target == nil {
		return
	}
	target.NAME = s.NAME
	target.Serial_No = s.Serial_No
	if s.FOOD_TYPE_ID != nil {
		target.FOOD_TYPE_ID = *s.FOOD_TYPE_ID
	}
	target.ID = *s.ID
	target.Source = "food"
}

