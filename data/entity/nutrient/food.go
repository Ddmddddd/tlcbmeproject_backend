package nutrientdb

import "tlcbme_project/data/model/nutrient"

type FoodBase struct {
}

func (s FoodBase) TableName() string {
	return "food"
}

type Food struct {
	FoodBase
	//
	Serial_No uint64 `sql:"Serial_No" auto:"true" primary:"true"`
	// 版本
	Version uint64 `sql:"Version"`
	// 食物ID
	ID *string `sql:"ID"`
	// 食物名称
	NAME string `sql:"NAME"`
	// 食部
	FOOD_DEP *string `sql:"FOOD_DEP"`
	// 能量(千卡)
	ENERGY_KC *float64 `sql:"ENERGY_KC"`
	// 蛋白质
	PROTEIN *float64 `sql:"PROTEIN"`
	// 脂肪
	FETT *float64 `sql:"FETT"`
	// 碳水化合物
	CARBOHYDRATE *float64 `sql:"CARBOHYDRATE"`
	// 膳食纤维
	DF *float64 `sql:"DF"`
	// 可溶性膳食纤维
	DF_SOLU *float64 `sql:"DF_SOLU"`
	// 不可溶膳食纤维
	DF_NO_SOLU *float64 `sql:"DF_NO_SOLU"`
	// 钙
	CA *float64 `sql:"CA"`
	// 铁
	FE *float64 `sql:"FE"`
	// 锌
	ZN *float64 `sql:"ZN"`
	// 硒
	SE *float64 `sql:"SE"`
	// 铜
	CU *float64 `sql:"CU"`
	// 锰
	MN *float64 `sql:"MN"`
	// 镁
	MG *float64 `sql:"MG"`
	// 钠
	NA *float64 `sql:"NA"`
	// 钾
	POT *float64 `sql:"POT"`
	// 磷
	PHO *float64 `sql:"PHO"`
	// 维生素A
	VITA *float64 `sql:"VITA"`
	// 维生素E
	VITE *float64 `sql:"VITE"`
	// 维生素B1
	VITB1 *float64 `sql:"VITB1"`
	// 维生素B2
	VITB2 *float64 `sql:"VITB2"`
	// 维生素C
	VITC *float64 `sql:"VITC"`
	// 烟酸
	VITPP *float64 `sql:"VITPP"`
	// 天冬氨酸
	ASP *float64 `sql:"ASP"`
	// 苏氨酸
	THR *float64 `sql:"THR"`
	// 丝氨酸
	SER *float64 `sql:"SER"`
	// 谷氨酸
	GLU *float64 `sql:"GLU"`
	// 脯氨酸
	PRO *float64 `sql:"PRO"`
	// 甘氨酸
	GLY *float64 `sql:"GLY"`
	// 丙氨酸
	ALA *float64 `sql:"ALA"`
	// 半胱氨酸
	CYS *float64 `sql:"CYS"`
	// 缬氨酸
	VAL *float64 `sql:"VAL"`
	// 蛋氨酸
	MET *float64 `sql:"MET"`
	// 异亮氨酸
	ILE *float64 `sql:"ILE"`
	// 亮氨酸
	LEU *float64 `sql:"LEU"`
	// 酪氨酸
	TYR *float64 `sql:"TYR"`
	// 苯丙氨酸
	PHE *float64 `sql:"PHE"`
	// 赖氨酸
	LYS *float64 `sql:"LYS"`
	// 组氨酸
	HIS *float64 `sql:"HIS"`
	// 精氨酸
	ARG *float64 `sql:"ARG"`
	// 色氨酸
	TRP *float64 `sql:"TRP"`
	// C6:0
	FA60 *float64 `sql:"FA60"`
	// C8:0
	FA80 *float64 `sql:"FA80"`
	// C10:0
	FA100 *float64 `sql:"FA100"`
	// C11:0
	FA110 *float64 `sql:"FA110"`
	// C12:0
	FA120 *float64 `sql:"FA120"`
	// C13:0
	FA130 *float64 `sql:"FA130"`
	// C14:0
	FA140 *float64 `sql:"FA140"`
	// C14:1
	FA141 *float64 `sql:"FA141"`
	// C15:0
	FA150 *float64 `sql:"FA150"`
	// C15:1
	FA151 *float64 `sql:"FA151"`
	// C16:0
	FA160 *float64 `sql:"FA160"`
	// C16:1
	FA161 *float64 `sql:"FA161"`
	// C16:2
	FA162 *float64 `sql:"FA162"`
	// C17:1
	FA171 *float64 `sql:"FA171"`
	// C17:0
	FA170 *float64 `sql:"FA170"`
	// C18:0
	FA180 *float64 `sql:"FA180"`
	// C18:1
	FA181 *float64 `sql:"FA181"`
	// C18:2
	FA182 *float64 `sql:"FA182"`
	// C18:3
	FA183 *float64 `sql:"FA183"`
	// C19:0
	FA190 *float64 `sql:"FA190"`
	// C20:0
	FA200 *float64 `sql:"FA200"`
	// C20:1
	FA201 *float64 `sql:"FA201"`
	// C20:2
	FA202 *float64 `sql:"FA202"`
	// C20:3
	FA203 *float64 `sql:"FA203"`
	// C20:4
	FA204 *float64 `sql:"FA204"`
	// C20:5
	FA205 *float64 `sql:"FA205"`
	// C22:0
	FA220 *float64 `sql:"FA220"`
	// C22:1
	FA221 *float64 `sql:"FA221"`
	// C22:3
	FA223 *float64 `sql:"FA223"`
	// C22:4
	FA224 *float64 `sql:"FA224"`
	// C22:5
	FA225 *float64 `sql:"FA225"`
	// 胆固醇
	CHO *float64 `sql:"CHO"`
	// 水
	WATER *float64 `sql:"WATER"`
	// SFA
	SFA *float64 `sql:"SFA"`
	// 叶酸
	USP *float64 `sql:"USP"`
	// 碘
	IODINE *float64 `sql:"IODINE"`
	// 灰分
	ASH *float64 `sql:"ASH"`
	// GI
	GI *float64 `sql:"GI"`
	// MUFA
	MUFA *float64 `sql:"MUFA"`
	// PUFA
	PUFA *float64 `sql:"PUFA"`
	// 胡萝卜素
	RENIERATENE *float64 `sql:"RENIERATENE"`
	// 视黄醇
	PREPALIN *float64 `sql:"PREPALIN"`
	// 硫胺素
	Thiamine *float64 `sql:"Thiamine"`
	// 核黄素
	Riboflavin *float64 `sql:"Riboflavin"`
	// 维生素E(α-E)
	VITE_AE *float64 `sql:"VITE_AE"`
	// 维生素E(β-γ)
	VITE_BY *float64 `sql:"VITE_BY"`
	// 维生素E(δ-E)
	VITE_QE *float64 `sql:"VITE_QE"`
	// 嘌呤
	PURINE *float64 `sql:"PURINE"`
	// 类别
	FOOD_TYPE_ID *string `sql:"FOOD_TYPE_ID"`
	// 维生素K
	VITK *float64 `sql:"VITK"`
	// 维生素D
	VITD *float64 `sql:"VITD"`
	// 维生素B6
	VITB6 *float64 `sql:"VITB6"`
	// 维生素B12
	VITB12 *float64 `sql:"VITB12"`
	// 拼音首字母
	SPELL *string `sql:"SPELL"`
	// 简易类别
	SIMPLE_TYPE_ID *string `sql:"SIMPLE_TYPE_ID"`
	// 编码
	CODE *string `sql:"CODE"`
	// 地区
	AREA *string `sql:"AREA"`
	// 能量(千焦)
	ENERGY_KJ *float64 `sql:"ENERGY_KJ"`
	// 脂肪酸
	FAT *float64 `sql:"FAT"`
	// TOTFA
	TOTFA *float64 `sql:"TOTFA"`
	// UNK
	UNK *float64 `sql:"UNK"`
	// FASAT
	FASAT *float64 `sql:"FASAT"`
	// FA226
	FA226 *float64 `sql:"FA226"`
	// FAMOMO
	FAMOMO *float64 `sql:"FAMOMO"`
	// FAPOLY
	FAPOLY *float64 `sql:"FAPOLY"`
	// FAUN
	FAUN *float64 `sql:"FAUN"`
	// 泛酸
	VITB5 *float64 `sql:"VITB5"`
	// 生物素
	VH *float64 `sql:"VH"`
	// 胆碱
	VITB4 *float64 `sql:"VITB4"`
	// 氟
	FI *float64 `sql:"FI"`
	// 铬
	CR *float64 `sql:"CR"`
	// 钼
	MO *float64 `sql:"MO"`
	// 氯
	CL *float64 `sql:"CL"`
	// 废弃率
	FOOD_REJECT_RATE *float64 `sql:"FOOD_REJECT_RATE"`
	// 含硫氨基酸
	SAA *float64 `sql:"SAA"`
	// 芳香族氨基酸
	AAA *float64 `sql:"AAA"`
	// FA4:0
	FA40 *float64 `sql:"FA40"`
	// FA24:0
	FA240 *float64 `sql:"FA240"`
	// FA24:1
	FA241 *float64 `sql:"FA241"`
	// 食物数据来源
	Data_From *string `sql:"Data_From"`
	// SFA比例
	SFA_Rate *float64 `sql:"SFA_Rate"`
	// MUFA比例
	MUFA_Rate *float64 `sql:"MUFA_Rate"`
	// PUFA比例
	PUFA_Rate *float64 `sql:"PUFA_Rate"`
	// FA18:04
	FA184 *float64 `sql:"FA184"`
	// 动物蛋白
	Animal_Protein *uint64 `sql:"Animal_Protein"`
	// 大豆类蛋白
	Soy_Protein *uint64 `sql:"Soy_Protein"`
	// 坚果蛋白
	Nut_Protein *uint64 `sql:"Nut_Protein"`
	// 其他植物蛋白
	Other_Vegetable_Protein *uint64 `sql:"Other_Vegetable_Protein"`
	// 其他蛋白质来源
	Other_Protein *uint64 `sql:"Other_Protein"`
	// 动物脂肪
	Animal_Fat *uint64 `sql:"Animal_Fat"`
	// 植物脂肪
	Vegetable_Fat *uint64 `sql:"Vegetable_Fat"`
	// 脂肪其他来源
	Other_Fat      *uint64 `sql:"Other_Fat"`
	Frequency_Flag uint64  `sql:"Frequency_Flag"`
	Alias          string  `sql:"Alias"`
}

func (s *Food) CopyTo(target *nutrient.Food) {
	if target == nil {
		return
	}
	target.Serial_No = s.Serial_No
	target.Version = s.Version
	if s.ID != nil {
		target.ID = string(*s.ID)
	}
	target.NAME = s.NAME
	if s.FOOD_DEP != nil {
		target.FOOD_DEP = string(*s.FOOD_DEP)
	}
	target.ENERGY_KC = s.ENERGY_KC
	target.PROTEIN = s.PROTEIN
	target.FETT = s.FETT
	target.CARBOHYDRATE = s.CARBOHYDRATE
	target.DF = s.DF
	target.DF_SOLU = s.DF_SOLU
	target.DF_NO_SOLU = s.DF_NO_SOLU
	target.CA = s.CA
	target.FE = s.FE
	target.ZN = s.ZN
	target.SE = s.SE
	target.CU = s.CU
	target.MN = s.MN
	target.MG = s.MG
	target.NA = s.NA
	target.POT = s.POT
	target.PHO = s.PHO
	target.VITA = s.VITA
	target.VITE = s.VITE
	target.VITB1 = s.VITB1
	target.VITB2 = s.VITB2
	target.VITC = s.VITC
	target.VITPP = s.VITPP
	target.ASP = s.ASP
	target.THR = s.THR
	target.SER = s.SER
	target.GLU = s.GLU
	target.PRO = s.PRO
	target.GLY = s.GLY
	target.ALA = s.ALA
	target.CYS = s.CYS
	target.VAL = s.VAL
	target.MET = s.MET
	target.ILE = s.ILE
	target.LEU = s.LEU
	target.TYR = s.TYR
	target.PHE = s.PHE
	target.LYS = s.LYS
	target.HIS = s.HIS
	target.ARG = s.ARG
	target.TRP = s.TRP
	target.FA60 = s.FA60
	target.FA80 = s.FA80
	target.FA100 = s.FA100
	target.FA110 = s.FA110
	target.FA120 = s.FA120
	target.FA130 = s.FA130
	target.FA140 = s.FA140
	target.FA141 = s.FA141
	target.FA150 = s.FA150
	target.FA151 = s.FA151
	target.FA160 = s.FA160
	target.FA161 = s.FA161
	target.FA162 = s.FA162
	target.FA171 = s.FA171
	target.FA170 = s.FA170
	target.FA180 = s.FA180
	target.FA181 = s.FA181
	target.FA182 = s.FA182
	target.FA183 = s.FA183
	target.FA190 = s.FA190
	target.FA200 = s.FA200
	target.FA201 = s.FA201
	target.FA202 = s.FA202
	target.FA203 = s.FA203
	target.FA204 = s.FA204
	target.FA205 = s.FA205
	target.FA220 = s.FA220
	target.FA221 = s.FA221
	target.FA223 = s.FA223
	target.FA224 = s.FA224
	target.FA225 = s.FA225
	target.CHO = s.CHO
	target.WATER = s.WATER
	target.SFA = s.SFA
	target.USP = s.USP
	target.IODINE = s.IODINE
	target.ASH = s.ASH
	target.GI = s.GI
	target.MUFA = s.MUFA
	target.PUFA = s.PUFA
	target.RENIERATENE = s.RENIERATENE
	target.PREPALIN = s.PREPALIN
	target.Thiamine = s.Thiamine
	target.Riboflavin = s.Riboflavin
	target.VITE_AE = s.VITE_AE
	target.VITE_BY = s.VITE_BY
	target.VITE_QE = s.VITE_QE
	target.PURINE = s.PURINE
	if s.FOOD_TYPE_ID != nil {
		target.FOOD_TYPE_ID = string(*s.FOOD_TYPE_ID)
	}
	target.VITK = s.VITK
	target.VITD = s.VITD
	target.VITB6 = s.VITB6
	target.VITB12 = s.VITB12
	if s.SPELL != nil {
		target.SPELL = string(*s.SPELL)
	}
	if s.SIMPLE_TYPE_ID != nil {
		target.SIMPLE_TYPE_ID = string(*s.SIMPLE_TYPE_ID)
	}
	if s.CODE != nil {
		target.CODE = string(*s.CODE)
	}
	if s.AREA != nil {
		target.AREA = string(*s.AREA)
	}
	target.ENERGY_KJ = s.ENERGY_KJ
	target.FAT = s.FAT
	target.TOTFA = s.TOTFA
	target.UNK = s.UNK
	target.FASAT = s.FASAT
	target.FA226 = s.FA226
	target.FAMOMO = s.FAMOMO
	target.FAPOLY = s.FAPOLY
	target.FAUN = s.FAUN
	target.VITB5 = s.VITB5
	target.VH = s.VH
	target.VITB4 = s.VITB4
	target.FI = s.FI
	target.CR = s.CR
	target.MO = s.MO
	target.CL = s.CL
	target.FOOD_REJECT_RATE = s.FOOD_REJECT_RATE
	target.SAA = s.SAA
	target.AAA = s.AAA
	target.FA40 = s.FA40
	target.FA240 = s.FA240
	target.FA241 = s.FA241
	if s.Data_From != nil {
		target.Data_From = string(*s.Data_From)
	}
	target.SFA_Rate = s.SFA_Rate
	target.MUFA_Rate = s.MUFA_Rate
	target.PUFA_Rate = s.PUFA_Rate
	target.FA184 = s.FA184
	target.Animal_Protein = s.Animal_Protein
	target.Soy_Protein = s.Soy_Protein
	target.Nut_Protein = s.Nut_Protein
	target.Other_Vegetable_Protein = s.Other_Vegetable_Protein
	target.Other_Protein = s.Other_Protein
	target.Animal_Fat = s.Animal_Fat
	target.Vegetable_Fat = s.Vegetable_Fat
	target.Other_Fat = s.Other_Fat
}

func (s *Food) CopyFrom(source *nutrient.Food) {
	if source == nil {
		return
	}
	s.Serial_No = source.Serial_No
	s.Version = source.Version
	iD := string(source.ID)
	s.ID = &iD
	s.NAME = source.NAME
	fOOD_DEP := string(source.FOOD_DEP)
	s.FOOD_DEP = &fOOD_DEP
	s.ENERGY_KC = source.ENERGY_KC
	s.PROTEIN = source.PROTEIN
	s.FETT = source.FETT
	s.CARBOHYDRATE = source.CARBOHYDRATE
	s.DF = source.DF
	s.DF_SOLU = source.DF_SOLU
	s.DF_NO_SOLU = source.DF_NO_SOLU
	s.CA = source.CA
	s.FE = source.FE
	s.ZN = source.ZN
	s.SE = source.SE
	s.CU = source.CU
	s.MN = source.MN
	s.MG = source.MG
	s.NA = source.NA
	s.POT = source.POT
	s.PHO = source.PHO
	s.VITA = source.VITA
	s.VITE = source.VITE
	s.VITB1 = source.VITB1
	s.VITB2 = source.VITB2
	s.VITC = source.VITC
	s.VITPP = source.VITPP
	s.ASP = source.ASP
	s.THR = source.THR
	s.SER = source.SER
	s.GLU = source.GLU
	s.PRO = source.PRO
	s.GLY = source.GLY
	s.ALA = source.ALA
	s.CYS = source.CYS
	s.VAL = source.VAL
	s.MET = source.MET
	s.ILE = source.ILE
	s.LEU = source.LEU
	s.TYR = source.TYR
	s.PHE = source.PHE
	s.LYS = source.LYS
	s.HIS = source.HIS
	s.ARG = source.ARG
	s.TRP = source.TRP
	s.FA60 = source.FA60
	s.FA80 = source.FA80
	s.FA100 = source.FA100
	s.FA110 = source.FA110
	s.FA120 = source.FA120
	s.FA130 = source.FA130
	s.FA140 = source.FA140
	s.FA141 = source.FA141
	s.FA150 = source.FA150
	s.FA151 = source.FA151
	s.FA160 = source.FA160
	s.FA161 = source.FA161
	s.FA162 = source.FA162
	s.FA171 = source.FA171
	s.FA170 = source.FA170
	s.FA180 = source.FA180
	s.FA181 = source.FA181
	s.FA182 = source.FA182
	s.FA183 = source.FA183
	s.FA190 = source.FA190
	s.FA200 = source.FA200
	s.FA201 = source.FA201
	s.FA202 = source.FA202
	s.FA203 = source.FA203
	s.FA204 = source.FA204
	s.FA205 = source.FA205
	s.FA220 = source.FA220
	s.FA221 = source.FA221
	s.FA223 = source.FA223
	s.FA224 = source.FA224
	s.FA225 = source.FA225
	s.CHO = source.CHO
	s.WATER = source.WATER
	s.SFA = source.SFA
	s.USP = source.USP
	s.IODINE = source.IODINE
	s.ASH = source.ASH
	s.GI = source.GI
	s.MUFA = source.MUFA
	s.PUFA = source.PUFA
	s.RENIERATENE = source.RENIERATENE
	s.PREPALIN = source.PREPALIN
	s.Thiamine = source.Thiamine
	s.Riboflavin = source.Riboflavin
	s.VITE_AE = source.VITE_AE
	s.VITE_BY = source.VITE_BY
	s.VITE_QE = source.VITE_QE
	s.PURINE = source.PURINE
	fOOD_TYPE_ID := string(source.FOOD_TYPE_ID)
	s.FOOD_TYPE_ID = &fOOD_TYPE_ID
	s.VITK = source.VITK
	s.VITD = source.VITD
	s.VITB6 = source.VITB6
	s.VITB12 = source.VITB12
	sPELL := string(source.SPELL)
	s.SPELL = &sPELL
	sIMPLE_TYPE_ID := string(source.SIMPLE_TYPE_ID)
	s.SIMPLE_TYPE_ID = &sIMPLE_TYPE_ID
	cODE := string(source.CODE)
	s.CODE = &cODE
	aREA := string(source.AREA)
	s.AREA = &aREA
	s.ENERGY_KJ = source.ENERGY_KJ
	s.FAT = source.FAT
	s.TOTFA = source.TOTFA
	s.UNK = source.UNK
	s.FASAT = source.FASAT
	s.FA226 = source.FA226
	s.FAMOMO = source.FAMOMO
	s.FAPOLY = source.FAPOLY
	s.FAUN = source.FAUN
	s.VITB5 = source.VITB5
	s.VH = source.VH
	s.VITB4 = source.VITB4
	s.FI = source.FI
	s.CR = source.CR
	s.MO = source.MO
	s.CL = source.CL
	s.FOOD_REJECT_RATE = source.FOOD_REJECT_RATE
	s.SAA = source.SAA
	s.AAA = source.AAA
	s.FA40 = source.FA40
	s.FA240 = source.FA240
	s.FA241 = source.FA241
	data_From := string(source.Data_From)
	s.Data_From = &data_From
	s.SFA_Rate = source.SFA_Rate
	s.MUFA_Rate = source.MUFA_Rate
	s.PUFA_Rate = source.PUFA_Rate
	s.FA184 = source.FA184
	s.Animal_Protein = source.Animal_Protein
	s.Soy_Protein = source.Soy_Protein
	s.Nut_Protein = source.Nut_Protein
	s.Other_Vegetable_Protein = source.Other_Vegetable_Protein
	s.Other_Protein = source.Other_Protein
	s.Animal_Fat = source.Animal_Fat
	s.Vegetable_Fat = source.Vegetable_Fat
	s.Other_Fat = source.Other_Fat
}
