package nutrientdb

import "tlcbme_project/data/model/nutrient"

type ViewfoodBase struct {
}

func (s ViewfoodBase) TableName() string {
	return "viewfood"
}

// VIEW
type Viewfood struct {
	ViewfoodBase
	//
	Serial_No uint64 `sql:"Serial_No"`
	//
	Version uint64 `sql:"Version"`
	//
	Name *string `sql:"Name"`
	//
	ENERGY_KC *float64 `sql:"ENERGY_KC"`
	//
	PROTEIN *float64 `sql:"PROTEIN"`
	//
	FETT *float64 `sql:"FETT"`
	//
	CARBOHYDRATE *float64 `sql:"CARBOHYDRATE"`
	//
	NA *float64 `sql:"NA"`
	//
	DF *float64 `sql:"DF"`
	//
	ZN *float64 `sql:"ZN"`
	//
	MG *float64 `sql:"MG"`
	//
	FE *float64 `sql:"FE"`
	//
	CA *float64 `sql:"CA"`
	//
	POT *float64 `sql:"POT"`
	//
	VITA *float64 `sql:"VITA"`
	//
	VITB1 *float64 `sql:"VITB1"`
	//
	VITB2 *float64 `sql:"VITB2"`
	//
	VITC *float64 `sql:"VITC"`
	//
	VITD *float64 `sql:"VITD"`
	//
	CHO *float64 `sql:"CHO"`
	//
	USP *float64 `sql:"USP"`
	//
	IODINE *float64 `sql:"IODINE"`
	//
	SE *float64 `sql:"SE"`
	//
	VITB6 *float64 `sql:"VITB6"`
	//
	VITE *float64 `sql:"VITE"`
	//
	Frequency_Flag uint64 `sql:"Frequency_Flag"`
	//
	Alias string `sql:"Alias"`
	//
	Source       string `sql:"Source"`
	FOOD_TYPE_ID string `sql:"FOOD_TYPE_ID"`
}

func (s *Viewfood) CopyTo(target *nutrient.Viewfood) {
	if target == nil {
		return
	}
	target.Serial_No = s.Serial_No
	target.Version = s.Version
	if s.Name != nil {
		target.Name = string(*s.Name)
	}
	target.ENERGY_KC = s.ENERGY_KC
	target.PROTEIN = s.PROTEIN
	target.FETT = s.FETT
	target.CARBOHYDRATE = s.CARBOHYDRATE
	target.NA = s.NA
	target.DF = s.DF
	target.ZN = s.ZN
	target.MG = s.MG
	target.FE = s.FE
	target.CA = s.CA
	target.POT = s.POT
	target.VITA = s.VITA
	target.VITB1 = s.VITB1
	target.VITB2 = s.VITB2
	target.VITC = s.VITC
	target.VITD = s.VITD
	target.CHO = s.CHO
	target.USP = s.USP
	target.IODINE = s.IODINE
	target.SE = s.SE
	target.VITB6 = s.VITB6
	target.VITE = s.VITE
	target.Frequency_Flag = s.Frequency_Flag
	target.Alias = s.Alias
	target.Source = s.Source
	target.FOOD_TYPE_ID = s.FOOD_TYPE_ID
}

func (s *Viewfood) CopyFrom(source *nutrient.Viewfood) {
	if source == nil {
		return
	}
	s.Serial_No = source.Serial_No
	s.Version = source.Version
	name := string(source.Name)
	s.Name = &name
	s.ENERGY_KC = source.ENERGY_KC
	s.PROTEIN = source.PROTEIN
	s.FETT = source.FETT
	s.CARBOHYDRATE = source.CARBOHYDRATE
	s.NA = source.NA
	s.DF = source.DF
	s.ZN = source.ZN
	s.MG = source.MG
	s.FE = source.FE
	s.CA = source.CA
	s.POT = source.POT
	s.VITA = source.VITA
	s.VITB1 = source.VITB1
	s.VITB2 = source.VITB2
	s.VITC = source.VITC
	s.VITD = source.VITD
	s.CHO = source.CHO
	s.USP = source.USP
	s.IODINE = source.IODINE
	s.SE = source.SE
	s.VITB6 = source.VITB6
	s.VITE = source.VITE
	s.Frequency_Flag = source.Frequency_Flag
	s.Alias = source.Alias
	s.Source = source.Source
	s.FOOD_TYPE_ID = source.FOOD_TYPE_ID
}
