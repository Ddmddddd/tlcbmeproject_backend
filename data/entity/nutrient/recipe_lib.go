package nutrientdb

import "tlcbme_project/data/model/nutrient"

type Recipe_libBase struct {
}

func (s Recipe_libBase) TableName() string {
	return "recipe_lib"
}

// 菜肴主表
type Recipe_lib struct {
	Recipe_libBase
	// 序号 主键
	Serial_No uint64 `sql:"Serial_No" auto:"true" primary:"true"`
	// 唯一标识 编号
	ID string `sql:"ID"`
	// 名称 例如：莲藕炒肉
	NAME string `sql:"NAME"`
	// 例如：locr
	SPELL *string `sql:"SPELL"`
	// 实际每份熟重（含汤） 单位 克
	WEIGHT_SOUP *float64 `sql:"WEIGHT_SOUP"`
	// 实际每份熟重（去汤）  单位 克
	WEIGHT_NO_SOUP *float64 `sql:"WEIGHT_NO_SOUP"`
	// 总熟重（含汤）
	ALL_WEIGHT_SOUP *float64 `sql:"ALL_WEIGHT_SOUP"`
	// 总熟重（不含汤）
	ALL_WEIGHT_NO_SOUP *float64 `sql:"ALL_WEIGHT_NO_SOUP"`
	// 每份生重 单位 克
	COOKED_WEIGHT *float64 `sql:"COOKED_WEIGHT"`
	// 生熟比  100代表全生，0代表全熟，单位 %
	RAW_COOKED *float64 `sql:"RAW_COOKED"`
	// 常见程度 越大越常见
	FREQUENCY_FLAG *uint64 `sql:"FREQUENCY_FLAG"`
	// 照片
	PHOTO *string `sql:"PHOTO"`
	// 别名
	ALIAS *string `sql:"ALIAS"`
	// 成本 单位 元
	COST *float64 `sql:"COST"`
	// 价格 单位 元
	PRICE *float64 `sql:"PRICE"`
	// 来自recipe_dep表
	BELONG_DEP_ID *string `sql:"BELONG_DEP_ID"`
	// 说明
	DESCS *string `sql:"DESCS"`
	// 菜系标签  鲁菜、川菜…,来自recipe_flag_dict表
	CUISINE_FLAG *string `sql:"CUISINE_FLAG"`
	// 食材标签 新鲜、冷冻、冷藏、腌制…
	FOOD_FLAG *string `sql:"FOOD_FLAG"`
	// 性状标签
	FOOD_TYPE *string `sql:"FOOD_TYPE"`
	// 烹饪方法标签 炒、爆、熘、炸、烹、煎…
	COOK_METHOD *string `sql:"COOK_METHOD"`
	// 餐次类别标签 早餐…
	MEAL_FLAG *string `sql:"MEAL_FLAG"`
	// 菜肴类别标签  主食…
	DISH_FLAG *string `sql:"DISH_FLAG"`
	// 季节标签 春，夏…
	SEASON_FLAG *string `sql:"SEASON_FLAG"`
	// 适宜饭种标签  普通饭、软饭、半流质…
	SUITABLE_RICE_FLAG *string `sql:"SUITABLE_RICE_FLAG"`
	// 口感标签 脆、绵、软…
	TEXTURE_FLAG *string `sql:"TEXTURE_FLAG"`
	// 口味标签 麻、辣、酸、甜…
	FLAVOR_FLAG *string `sql:"FLAVOR_FLAG"`
	// 疾病标签 高血糖、高血压…
	DISIEASE_FLAG *string `sql:"DISIEASE_FLAG"`
	// 营养标签 高热量、低热量…
	NUTRITION_FLAG *string `sql:"NUTRITION_FLAG"`
	// 健康标签
	HEALTH_SCENE_FLAG *string `sql:"HEALTH_SCENE_FLAG"`
	// 适用年龄标签
	SORT_PERSON_FLAG *string `sql:"SORT_PERSON_FLAG"`
	// 红绿灯标识（PHI指数） 标签  红、黄…
	PHI_FLAG *string `sql:"PHI_FLAG"`
	// 颜色标签 白，红，橙，黄…
	COLOUR_FLAG *string `sql:"COLOUR_FLAG"`
	// 菜肴做法
	DISH_PRECTICE *string `sql:"DISH_PRECTICE"`
	// 价格 使用频率标签 常用、一般、较少使用…
	USAGE_RATE_FLAG *string `sql:"USAGE_RATE_FLAG"`
	// 临时 菜肴来源的ID
	SOURCE *string `sql:"SOURCE"`
}

func (s *Recipe_lib) CopyTo(target *nutrient.Recipe_lib) {
	if target == nil {
		return
	}
	target.Serial_No = s.Serial_No
	target.ID = s.ID
	target.NAME = s.NAME
	if s.SPELL != nil {
		target.SPELL = string(*s.SPELL)
	}
	target.WEIGHT_SOUP = s.WEIGHT_SOUP
	target.WEIGHT_NO_SOUP = s.WEIGHT_NO_SOUP
	target.ALL_WEIGHT_SOUP = s.ALL_WEIGHT_SOUP
	target.ALL_WEIGHT_NO_SOUP = s.ALL_WEIGHT_NO_SOUP
	target.COOKED_WEIGHT = s.COOKED_WEIGHT
	target.RAW_COOKED = s.RAW_COOKED
	target.FREQUENCY_FLAG = s.FREQUENCY_FLAG
	target.PHOTO = *s.PHOTO
	if s.ALIAS != nil {
		target.ALIAS = string(*s.ALIAS)
	}
	target.COST = s.COST
	target.PRICE = s.PRICE
	if s.BELONG_DEP_ID != nil {
		target.BELONG_DEP_ID = string(*s.BELONG_DEP_ID)
	}
	if s.DESCS != nil {
		target.DESCS = string(*s.DESCS)
	}
	if s.CUISINE_FLAG != nil {
		target.CUISINE_FLAG = string(*s.CUISINE_FLAG)
	}
	if s.FOOD_FLAG != nil {
		target.FOOD_FLAG = string(*s.FOOD_FLAG)
	}
	if s.FOOD_TYPE != nil {
		target.FOOD_TYPE = string(*s.FOOD_TYPE)
	}
	if s.COOK_METHOD != nil {
		target.COOK_METHOD = string(*s.COOK_METHOD)
	}
	if s.MEAL_FLAG != nil {
		target.MEAL_FLAG = string(*s.MEAL_FLAG)
	}
	if s.DISH_FLAG != nil {
		target.DISH_FLAG = string(*s.DISH_FLAG)
	}
	if s.SEASON_FLAG != nil {
		target.SEASON_FLAG = string(*s.SEASON_FLAG)
	}
	if s.SUITABLE_RICE_FLAG != nil {
		target.SUITABLE_RICE_FLAG = string(*s.SUITABLE_RICE_FLAG)
	}
	if s.TEXTURE_FLAG != nil {
		target.TEXTURE_FLAG = string(*s.TEXTURE_FLAG)
	}
	if s.FLAVOR_FLAG != nil {
		target.FLAVOR_FLAG = string(*s.FLAVOR_FLAG)
	}
	if s.DISIEASE_FLAG != nil {
		target.DISIEASE_FLAG = string(*s.DISIEASE_FLAG)
	}
	if s.NUTRITION_FLAG != nil {
		target.NUTRITION_FLAG = string(*s.NUTRITION_FLAG)
	}
	if s.HEALTH_SCENE_FLAG != nil {
		target.HEALTH_SCENE_FLAG = string(*s.HEALTH_SCENE_FLAG)
	}
	if s.SORT_PERSON_FLAG != nil {
		target.SORT_PERSON_FLAG = string(*s.SORT_PERSON_FLAG)
	}
	if s.PHI_FLAG != nil {
		target.PHI_FLAG = string(*s.PHI_FLAG)
	}
	if s.COLOUR_FLAG != nil {
		target.COLOUR_FLAG = string(*s.COLOUR_FLAG)
	}
	if s.DISH_PRECTICE != nil {
		target.DISH_PRECTICE = string(*s.DISH_PRECTICE)
	}
	if s.USAGE_RATE_FLAG != nil {
		target.USAGE_RATE_FLAG = string(*s.USAGE_RATE_FLAG)
	}
	if s.SOURCE != nil {
		target.SOURCE = string(*s.SOURCE)
	}
}

func (s *Recipe_lib) CopyFrom(source *nutrient.Recipe_lib) {
	if source == nil {
		return
	}
	s.Serial_No = source.Serial_No
	s.ID = source.ID
	s.NAME = source.NAME
	sPELL := string(source.SPELL)
	s.SPELL = &sPELL
	s.WEIGHT_SOUP = source.WEIGHT_SOUP
	s.WEIGHT_NO_SOUP = source.WEIGHT_NO_SOUP
	s.ALL_WEIGHT_SOUP = source.ALL_WEIGHT_SOUP
	s.ALL_WEIGHT_NO_SOUP = source.ALL_WEIGHT_NO_SOUP
	s.COOKED_WEIGHT = source.COOKED_WEIGHT
	s.RAW_COOKED = source.RAW_COOKED
	s.FREQUENCY_FLAG = source.FREQUENCY_FLAG
	s.PHOTO = &source.PHOTO
	aLIAS := string(source.ALIAS)
	s.ALIAS = &aLIAS
	s.COST = source.COST
	s.PRICE = source.PRICE
	bELONG_DEP_ID := string(source.BELONG_DEP_ID)
	s.BELONG_DEP_ID = &bELONG_DEP_ID
	dESCS := string(source.DESCS)
	s.DESCS = &dESCS
	cUISINE_FLAG := string(source.CUISINE_FLAG)
	s.CUISINE_FLAG = &cUISINE_FLAG
	fOOD_FLAG := string(source.FOOD_FLAG)
	s.FOOD_FLAG = &fOOD_FLAG
	fOOD_TYPE := string(source.FOOD_TYPE)
	s.FOOD_TYPE = &fOOD_TYPE
	cOOK_METHOD := string(source.COOK_METHOD)
	s.COOK_METHOD = &cOOK_METHOD
	mEAL_FLAG := string(source.MEAL_FLAG)
	s.MEAL_FLAG = &mEAL_FLAG
	dISH_FLAG := string(source.DISH_FLAG)
	s.DISH_FLAG = &dISH_FLAG
	sEASON_FLAG := string(source.SEASON_FLAG)
	s.SEASON_FLAG = &sEASON_FLAG
	sUITABLE_RICE_FLAG := string(source.SUITABLE_RICE_FLAG)
	s.SUITABLE_RICE_FLAG = &sUITABLE_RICE_FLAG
	tEXTURE_FLAG := string(source.TEXTURE_FLAG)
	s.TEXTURE_FLAG = &tEXTURE_FLAG
	fLAVOR_FLAG := string(source.FLAVOR_FLAG)
	s.FLAVOR_FLAG = &fLAVOR_FLAG
	dISIEASE_FLAG := string(source.DISIEASE_FLAG)
	s.DISIEASE_FLAG = &dISIEASE_FLAG
	nUTRITION_FLAG := string(source.NUTRITION_FLAG)
	s.NUTRITION_FLAG = &nUTRITION_FLAG
	hEALTH_SCENE_FLAG := string(source.HEALTH_SCENE_FLAG)
	s.HEALTH_SCENE_FLAG = &hEALTH_SCENE_FLAG
	sORT_PERSON_FLAG := string(source.SORT_PERSON_FLAG)
	s.SORT_PERSON_FLAG = &sORT_PERSON_FLAG
	pHI_FLAG := string(source.PHI_FLAG)
	s.PHI_FLAG = &pHI_FLAG
	cOLOUR_FLAG := string(source.COLOUR_FLAG)
	s.COLOUR_FLAG = &cOLOUR_FLAG
	dISH_PRECTICE := string(source.DISH_PRECTICE)
	s.DISH_PRECTICE = &dISH_PRECTICE
	uSAGE_RATE_FLAG := string(source.USAGE_RATE_FLAG)
	s.USAGE_RATE_FLAG = &uSAGE_RATE_FLAG
	sOURCE := string(source.SOURCE)
	s.SOURCE = &sOURCE
}
