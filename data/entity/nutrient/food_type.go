package nutrientdb

import "tlcbme_project/data/model/nutrient"

type Food_typeBase struct {
}

func (s Food_typeBase) TableName() string {
	return "food_type"
}

type Food_type struct {
	Food_typeBase
	//
	Serial_No uint64 `sql:"Serial_No" auto:"true" primary:"true"`
	// 版本
	Version uint64 `sql:"Version"`
	//
	ID string `sql:"ID"`
	//
	NAME *string `sql:"NAME"`
	//
	PARENT_ID *string `sql:"PARENT_ID"`
	//
	SORT *uint64 `sql:"SORT"`
	//
	DESCS *string `sql:"DESCS"`
}

func (s *Food_type) CopyTo(target *nutrient.Food_type) {
	if target == nil {
		return
	}
	target.Serial_No = s.Serial_No
	target.Version = s.Version
	target.ID = s.ID
	if s.NAME != nil {
		target.NAME = string(*s.NAME)
	}
	if s.PARENT_ID != nil {
		target.PARENT_ID = string(*s.PARENT_ID)
	}
	target.SORT = s.SORT
	if s.DESCS != nil {
		target.DESCS = string(*s.DESCS)
	}
}

func (s *Food_type) CopyFrom(source *nutrient.Food_type) {
	if source == nil {
		return
	}
	s.Serial_No = source.Serial_No
	s.Version = source.Version
	s.ID = source.ID
	nAME := string(source.NAME)
	s.NAME = &nAME
	pARENT_ID := string(source.PARENT_ID)
	s.PARENT_ID = &pARENT_ID
	s.SORT = source.SORT
	dESCS := string(source.DESCS)
	s.DESCS = &dESCS
}
