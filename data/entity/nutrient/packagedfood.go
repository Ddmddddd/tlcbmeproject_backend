package nutrientdb

import "tlcbme_project/data/model/nutrient"

type PackagedfoodBase struct {
}

func (s PackagedfoodBase) TableName() string {
	return "packagedfood"
}

type Packagedfood struct {
	PackagedfoodBase
	// 序号
	Serial_No uint64 `sql:"Serial_No" auto:"true" primary:"true"`
	// 1-固体，2-液体
	Quality *uint64 `sql:"Quality"`
	// 来源
	Resource *string `sql:"Resource"`
	// 条形码
	Code *string `sql:"Code"`
	// 名称
	Name *string `sql:"Name"`
	// 品牌
	Brand *string `sql:"Brand"`
	// 制造商
	Manufaturer *string `sql:"Manufaturer"`
	// 类型ID，对应类型表
	FOOD_TYPE_ID *string `sql:"FOOD_TYPE_ID"`
	// 口味
	Flavor *string `sql:"Flavor"`
	// 包装单位的克数/毫升数
	PackingSizeNum *float64 `sql:"PackingSizeNum"`
	// 包装单位
	PackingSizeUnit *string `sql:"PackingSizeUnit"`
	// 营养素对应份量
	Portion *string `sql:"Portion"`
	// 热量，千焦单位
	ENERGY_KJ *float64 `sql:"ENERGY_KJ"`
	// 热量，千卡单位
	ENERGY_KC *float64 `sql:"ENERGY_KC"`
	// 脂肪
	FETT *float64 `sql:"FETT"`
	// 蛋白质
	PROTEIN *float64 `sql:"PROTEIN"`
	// 碳水化合物
	CARBOHYDRATE *float64 `sql:"CARBOHYDRATE"`
	// 糖类
	SUGAR *float64 `sql:"SUGAR"`
	// 纤维素
	DF *float64 `sql:"DF"`
	// 钠
	NA *float64 `sql:"NA"`
}

func (s *Packagedfood) CopyTo(target *nutrient.Packagedfood) {
	if target == nil {
		return
	}
	target.Serial_No = s.Serial_No
	target.Quality = s.Quality
	if s.Resource != nil {
		target.Resource = string(*s.Resource)
	}
	if s.Code != nil {
		target.Code = string(*s.Code)
	}
	if s.Name != nil {
		target.Name = string(*s.Name)
	}
	if s.Brand != nil {
		target.Brand = string(*s.Brand)
	}
	if s.Manufaturer != nil {
		target.Manufaturer = string(*s.Manufaturer)
	}
	if s.FOOD_TYPE_ID != nil {
		target.FOOD_TYPE_ID = string(*s.FOOD_TYPE_ID)
	}
	if s.Flavor != nil {
		target.Flavor = string(*s.Flavor)
	}
	target.PackingSizeNum = s.PackingSizeNum
	if s.PackingSizeUnit != nil {
		target.PackingSizeUnit = string(*s.PackingSizeUnit)
	}
	if s.Portion != nil {
		target.Portion = string(*s.Portion)
	}
	target.ENERGY_KJ = s.ENERGY_KJ
	target.ENERGY_KC = s.ENERGY_KC
	target.FETT = s.FETT
	target.PROTEIN = s.PROTEIN
	target.CARBOHYDRATE = s.CARBOHYDRATE
	target.SUGAR = s.SUGAR
	target.DF = s.DF
	target.NA = s.NA
}

func (s *Packagedfood) CopyFrom(source *nutrient.Packagedfood) {
	if source == nil {
		return
	}
	s.Serial_No = source.Serial_No
	s.Quality = source.Quality
	resource := string(source.Resource)
	s.Resource = &resource
	code := string(source.Code)
	s.Code = &code
	name := string(source.Name)
	s.Name = &name
	brand := string(source.Brand)
	s.Brand = &brand
	manufaturer := string(source.Manufaturer)
	s.Manufaturer = &manufaturer
	fOOD_TYPE_ID := string(source.FOOD_TYPE_ID)
	s.FOOD_TYPE_ID = &fOOD_TYPE_ID
	flavor := string(source.Flavor)
	s.Flavor = &flavor
	s.PackingSizeNum = source.PackingSizeNum
	packingSizeUnit := string(source.PackingSizeUnit)
	s.PackingSizeUnit = &packingSizeUnit
	portion := string(source.Portion)
	s.Portion = &portion
	s.ENERGY_KJ = source.ENERGY_KJ
	s.ENERGY_KC = source.ENERGY_KC
	s.FETT = source.FETT
	s.PROTEIN = source.PROTEIN
	s.CARBOHYDRATE = source.CARBOHYDRATE
	s.SUGAR = source.SUGAR
	s.DF = source.DF
	s.NA = source.NA
}
