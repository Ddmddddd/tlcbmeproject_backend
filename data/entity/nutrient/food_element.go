package nutrientdb

import "tlcbme_project/data/model/nutrient"

type Food_elementBase struct {
}

func (s Food_elementBase) TableName() string {
	return "food_element"
}

type Food_element struct {
	Food_elementBase
	//
	Serial_No uint64 `sql:"Serial_No" auto:"true" primary:"true"`
	//
	ID string `sql:"ID"`
	// 元素名称
	NAME *string `sql:"NAME"`
	// 元素字段
	FIELD *string `sql:"FIELD"`
	// 单位英文
	UNIT *string `sql:"UNIT"`
	// 单位中文
	UNIT_NAME *string `sql:"UNIT_NAME"`
	//
	PARENT_ID *string `sql:"PARENT_ID"`
}

func (s *Food_element) CopyTo(target *nutrient.Food_element) {
	if target == nil {
		return
	}
	target.Serial_No = s.Serial_No
	target.ID = s.ID
	if s.NAME != nil {
		target.NAME = string(*s.NAME)
	}
	if s.FIELD != nil {
		target.FIELD = string(*s.FIELD)
	}
	if s.UNIT != nil {
		target.UNIT = string(*s.UNIT)
	}
	if s.UNIT_NAME != nil {
		target.UNIT_NAME = string(*s.UNIT_NAME)
	}
	if s.PARENT_ID != nil {
		target.PARENT_ID = string(*s.PARENT_ID)
	}
}

func (s *Food_element) CopyFrom(source *nutrient.Food_element) {
	if source == nil {
		return
	}
	s.Serial_No = source.Serial_No
	s.ID = source.ID
	nAME := string(source.NAME)
	s.NAME = &nAME
	fIELD := string(source.FIELD)
	s.FIELD = &fIELD
	uNIT := string(source.UNIT)
	s.UNIT = &uNIT
	uNIT_NAME := string(source.UNIT_NAME)
	s.UNIT_NAME = &uNIT_NAME
	pARENT_ID := string(source.PARENT_ID)
	s.PARENT_ID = &pARENT_ID
}
