package nutrientdb

import (
	"tlcbme_project/data/model/nutrient"
)

type MaxRecipeNo struct {
	// 序号 主键
	SerialNo uint64 `sql:"Serial_No" auto:"true" primary:"true"`
}

type MaxRecipeFilter struct {
	SerialNo uint64 `sql:"Serial_No" `
}

func (s *Recipe_lib) CopyFromEx(source *nutrient.NewRecipeFromApp) {
	if source == nil {
		return
	}
	s.NAME = source.Name
	s.SOURCE = &source.Source
	s.BELONG_DEP_ID = &source.BelongDepID
}
