package nutrientdb

import "tlcbme_project/data/model/nutrient"

type Food_measure_unitBase struct {
}

func (s Food_measure_unitBase) TableName() string {
	return "food_measure_unit"
}

type Food_measure_unit struct {
	Food_measure_unitBase
	// 唯一序号，主键
	Serial_No uint64 `sql:"Serial_No" primary:"true" auto:"true"`
	// 食品唯一序号，外键，关联food表
	Food_Serial_No uint64 `sql:"Food_Serial_No"`
	// 数值部分，对应“1只(大)鸡蛋(70.0克，可食部分62.0克)”里面的“1”
	Number_Part float64 `sql:"Number_Part"`
	// 单位部分，对应“1只(大)鸡蛋(70.0克，可食部分62.0克)”里面的“只”
	Unit_Part string `sql:"Unit_Part"`
	// 单位名称部分，比如单位“g”，对应的名称为“克”
	Unit_Name_Part string `sql:"Unit_Name_Part"`
	// 程度部分，对应“1只(大)鸡蛋(70.0克，可食部分62.0克)”里面的“大”
	Level_Part *string `sql:"Level_Part"`
	// 总重部分，对应“1只(大)鸡蛋(70.0克，可食部分62.0克)”里面的“70.0”
	Allup_Part *float64 `sql:"Allup_Part"`
	// 可食部分，对应“1只(大)鸡蛋(70.0克，可食部分62.0克)”里面的“62.0”
	Edible_Part float64 `sql:"Edible_Part"`
	// 0 - 非标准度量单位，1 - 标准度量单位
	Flag uint64 `sql:"Flag"`
}

func (s *Food_measure_unit) CopyTo(target *nutrient.Food_measure_unit) {
	if target == nil {
		return
	}
	target.Serial_No = s.Serial_No
	target.Food_Serial_No = s.Food_Serial_No
	target.Number_Part = s.Number_Part
	target.Unit_Part = s.Unit_Part
	target.Unit_Name_Part = s.Unit_Name_Part
	if s.Level_Part != nil {
		target.Level_Part = string(*s.Level_Part)
	}
	target.Allup_Part = s.Allup_Part
	target.Edible_Part = s.Edible_Part
	target.Flag = s.Flag
}

func (s *Food_measure_unit) CopyFrom(source *nutrient.Food_measure_unit) {
	if source == nil {
		return
	}
	s.Serial_No = source.Serial_No
	s.Food_Serial_No = source.Food_Serial_No
	s.Number_Part = source.Number_Part
	s.Unit_Part = source.Unit_Part
	s.Unit_Name_Part = source.Unit_Name_Part
	level_Part := string(source.Level_Part)
	s.Level_Part = &level_Part
	s.Allup_Part = source.Allup_Part
	s.Edible_Part = source.Edible_Part
	s.Flag = source.Flag
}
