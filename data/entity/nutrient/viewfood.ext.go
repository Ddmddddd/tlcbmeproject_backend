package nutrientdb

import "tlcbme_project/data/model/nutrient"

type FoodFilter struct {
	//
	Serial_No uint64 `sql:"Serial_No" `
	//
	Source string `sql:"Source"`
}

type FoodOrder struct {
	ViewfoodBase
	Frequency_Flag uint64 `sql:"Frequency_Flag" order:"DESC"`
}

func (s *Viewfood) CopyToEx(target *nutrient.FoodSearchResult) {
	if target == nil {
		return
	}
	if s.Name != nil {
		target.NAME = *s.Name
	}
	target.Serial_No = s.Serial_No
	target.FOOD_TYPE_ID = s.FOOD_TYPE_ID
	target.Source = s.Source
	//target.ID = *s.ID
}
