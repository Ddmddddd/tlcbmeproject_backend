package nutrientdb

type Food_measure_unit_Filter struct {
	// 食品唯一序号，外键，关联food表
	Food_Serial_No uint64 `sql:"Food_Serial_No"`
	// 0 - 非标准度量单位，1 - 标准度量单位
	Flag uint64 `sql:"Flag"`
}

type Food_measure_unit_serialNo_filter struct {
	// 食品唯一序号，外键，关联food表
	Food_Serial_No uint64 `sql:"Food_Serial_No"`
}
