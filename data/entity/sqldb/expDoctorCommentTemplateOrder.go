package sqldb

import (
	"encoding/json"
	"tlcbme_project/data/model/doctor"
)

type ExpDoctorCommentTemplateOrderBase struct {
}

func (s ExpDoctorCommentTemplateOrderBase) TableName() string {
	return "ExpDoctorCommentTemplateOrder"
}

type ExpDoctorCommentTemplateOrder struct {
	ExpDoctorCommentTemplateOrderBase
	//
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 编辑者id
	EditorID uint64 `sql:"EditorID"`
	// 评论模板顺序
	TemplateOrder *string `sql:"TemplateOrder"`
}

func (s *ExpDoctorCommentTemplateOrder) CopyTo(target *doctor.ExpDoctorCommentTemplateOrder) {
	if target == nil {
		return
	}
	target.SerialNo = s.SerialNo
	target.EditorID = s.EditorID
	templateOrder := s.TemplateOrder
	if templateOrder != nil {
		if len(*templateOrder) > 0 {
			json.Unmarshal([]byte(*templateOrder), &target.TemplateOrder)
		}
	}
}
