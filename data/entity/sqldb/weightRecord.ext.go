package sqldb

import (
	"github.com/ktpswjz/httpserver/types"
	"time"
	"tlcbme_project/data/model/doctor"
)

type WeightRecordFilter struct {
	WeightRecordBase

	// 序号 主键，自增 例如:324
	SerialNo *uint64 `sql:"SerialNo"`
}

type WeightRecordPatientFilter struct {
	WeightRecordBase

	// 用户ID 关联患者用户登录表用户ID 例如:232442
	PatientID uint64 `sql:"PatientID"`
}

type WeightRecordData struct {
	WeightRecordBase

	// 序号 主键，自增 例如:324
	SerialNo uint64 `sql:"SerialNo"`
	// 体重 单位：kg 例如:60.5
	Weight float64 `sql:"Weight"`
	// 测量时间 测量体重时的时间 例如:2018-07-03 14:45:00
	MeasureDateTime *time.Time `sql:"MeasureDateTime"`
	// 备注 用于说明测量时的情况 例如:有氧运动半小时后
	Memo *string `sql:"Memo"`
}

type WeightRecordDataOrder struct {
	WeightRecordBase

	// 测量时间 测量血压的时间 例如:2018-07-03 14:45:00
	MeasureDateTime *time.Time `sql:"MeasureDateTime"`
}

type WeightRecordDataDescOrder struct {
	WeightRecordBase

	// 测量时间 测量血压的时间 例如:2018-07-03 14:45:00
	MeasureDateTime *time.Time `sql:"MeasureDateTime" order:"desc"`
}

type PatientUserWaistlineFilter struct {
	WeightRecordBase

	// 用户ID 关联患者用户登录表用户ID 例如:232442
	PatientID uint64 `sql:"PatientID"`
}

type WaistlineOrder struct {
	WeightRecordBase
	// 测量时间 测量体重的时间 例如:2018-07-03 14:45:00
	MeasureStartDate *time.Time `sql:"MeasureDateTime"`
}

type WeightRecordDataFilter struct {
	WeightRecordBase

	// 用户ID 关联患者用户登录表用户ID 例如:232442
	PatientID uint64 `sql:"PatientID"`

	// 测量时间 测量体重的时间 例如:2018-07-03 14:45:00
	MeasureStartDate *time.Time `sql:"MeasureDateTime" filter:">="`
	MeasureEndDate   *time.Time `sql:"MeasureDateTime" filter:"<"`
}

type WeightRecordDataLowFilter struct {

	// 用户ID 关联患者用户登录表用户ID 例如:232442
	PatientID uint64 `sql:"PatientID"`

	// 测量时间 测量体重的时间 例如:2018-07-03 14:45:00
	MeasureStartDate *time.Time `sql:"MeasureDateTime" filter:"<"`
}

func (s *WeightRecordDataLowFilter) CopyFrom(source *doctor.WeightRecordDataFilterEx) {
	if source == nil {
		return
	}

	s.PatientID = source.PatientID
	if source.MeasureStartDate != nil {
		s.MeasureStartDate = source.MeasureStartDate.ToDate(0)
	}
}

func (s *WeightRecordData) CopyTo(target *doctor.WeightRecordData) {
	if target == nil {
		return
	}

	target.Weight = s.Weight
	if s.MeasureDateTime == nil {
		target.MeasureDateTime = nil
	} else {
		measureDateTime := types.Time(*s.MeasureDateTime)
		target.MeasureDateTime = &measureDateTime
	}
	if s.Memo != nil {
		target.Memo = string(*s.Memo)
	}
}

func (s *WeightRecordDataFilter) CopyFrom(source *doctor.WeightRecordDataFilterEx) {
	if source == nil {
		return
	}

	s.PatientID = source.PatientID
	if source.MeasureStartDate != nil {
		s.MeasureStartDate = source.MeasureStartDate.ToDate(0)
	}
	if source.MeasureEndDate != nil {
		s.MeasureEndDate = source.MeasureEndDate.ToDate(1)
	}
}

func (s *WeightRecord) CopyFromAppEx(source *doctor.WeightRecordForAppEx) {
	if source == nil {
		return
	}

	s.SerialNo = source.SerialNo
	s.PatientID = source.PatientID
	s.Weight = source.Weight
	s.Waistline = source.Waistline
	s.Height = source.Height
	timePoint := TypeToTimePoint(source.Type)
	s.TimePoint = &timePoint
	memo := string(source.Memo)
	s.Memo = &memo
	photo := string(source.Photo)
	s.Photo = &photo
	if source.MeasureDateTime == nil {
		s.MeasureDateTime = nil
	} else {
		measureDateTime := time.Time(*source.MeasureDateTime)
		s.MeasureDateTime = &measureDateTime
	}
	now := time.Now()
	s.InputDateTime = &now
	s.Water = source.Water
	s.Sleep = source.Sleep
}

func (s *WeightRecord) CopyToApp(target *doctor.WeightRecordForApp) {
	if target == nil {
		return
	}
	target.SerialNo = s.SerialNo
	target.Weight = s.Weight

	target.Type = TimePointToType(s.TimePoint)
	target.Waistline = s.Waistline
	if s.Memo != nil {
		target.Memo = string(*s.Memo)
	}
	if s.MeasureDateTime == nil {
		target.MeasureDateTime = nil
	} else {
		measureDateTime := types.Time(*s.MeasureDateTime)
		target.MeasureDateTime = &measureDateTime
	}
	if s.Photo != nil {
		target.Photo = string(*s.Photo)
	}
	target.Water = s.Water
	target.Sleep = s.Sleep

}

type WeightRecordPatientAndDateFilter struct {
	WeightRecordBase

	// 用户ID 关联患者用户登录表用户ID 例如:232442
	PatientID uint64 `sql:"PatientID"`

	// 测量时间 测量体重的时间 例如:2018-07-03 14:45:00
	MeasureStartDate *time.Time `sql:"MeasureDateTime" filter:">="`
}

func (s *WeightRecord) CopyToPhoto(target *doctor.WeightRecordForPhoto) {
	if target == nil {
		return
	}
	target.Weight = s.Weight
	if s.MeasureDateTime == nil {
		target.MeasureDateTime = nil
	} else {
		measureDateTime := types.Time(*s.MeasureDateTime)
		target.MeasureDateTime = &measureDateTime
	}
	if s.Photo != nil {
		target.Photo = string(*s.Photo)
	}
}

//以组队形式读取体重记录
type WeightRecordMeasureDateTime struct {
	WeightRecordBase
	// 测量时间 测量体重时的时间 例如:2018-07-03 14:45:00
	MeasureDateTime *time.Time `sql:"MeasureDateTime"`
}

type WeightRecordFilterByPatientIDList struct {
	WeightRecordBase
	PatientIDList []uint64 `sql:"PatientID" filter:"in"`
}

type WeightRecordWeightValue struct {
	WeightRecordBase
	// 体重 单位：kg 例如:60.5
	Weight float64 `sql:"Weight"`
}

type WeightRecordMostRecentFilter struct {
	WeightRecordBase
	MeasureDateTime *time.Time `sql:"MeasureDateTime" filter:"<="`
}
