package sqldb

import (
	"tlcbme_project/data/model/doctor"
	"github.com/ktpswjz/httpserver/types"
	"time"
)

type UsabilityStayTimeBase struct {
}

func (s UsabilityStayTimeBase) TableName() string {
	return "UsabilityStayTime"
}

// 平台运维
// 用户行为统计
// 停留界面时间统计表
// 注释：本表记录用户在应用界面的停留时间
type UsabilityStayTime struct {
	UsabilityStayTimeBase
	// 序号 主键，自增 例如:1
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 用户名 关联患者用户登录表用户名 例如:aa
	UserName string `sql:"UserName"`
	// 应用标识 唯一标识应用 例如:abcdefghijklmn
	AppId string `sql:"AppId"`
	// 应用类别 Android、iOS或者微信小程序 例如:Android
	AppType string `sql:"AppType"`
	// 应用版本 当前应用版本 例如:1.0.1.0
	AppVersion string `sql:"AppVersion"`
	// 界面名称 当前界面名称 例如:MainPage
	PageName string `sql:"PageName"`
	// 进入时间 进入当前界面的时间点 例如:2018-12-12 08:00:00
	EnterTime *time.Time `sql:"EnterTime"`
	// 退出时间 离开当前界面的时间点 例如:2018-12-12 08:01:00
	ExitTime *time.Time `sql:"ExitTime"`
	// 停留时间 停留在当前界面的时间，单位：s 例如:60
	StayTime string `sql:"StayTime"`
}

func (s *UsabilityStayTime) CopyTo(target *doctor.UsabilityStayTime) {
	if target == nil {
		return
	}
	target.SerialNo = s.SerialNo
	target.UserName = s.UserName
	target.AppId = s.AppId
	target.AppType = s.AppType
	target.AppVersion = s.AppVersion
	target.PageName = s.PageName
	if s.EnterTime == nil {
		target.EnterTime = nil
	} else {
		enterTime := types.Time(*s.EnterTime)
		target.EnterTime = &enterTime
	}
	if s.ExitTime == nil {
		target.ExitTime = nil
	} else {
		exitTime := types.Time(*s.ExitTime)
		target.ExitTime = &exitTime
	}
	target.StayTime = s.StayTime
}

func (s *UsabilityStayTime) CopyFrom(source *doctor.UsabilityStayTime) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.UserName = source.UserName
	s.AppId = source.AppId
	s.AppType = source.AppType
	s.AppVersion = source.AppVersion
	s.PageName = source.PageName
	if source.EnterTime == nil {
		s.EnterTime = nil
	} else {
		enterTime := time.Time(*source.EnterTime)
		s.EnterTime = &enterTime
	}
	if source.ExitTime == nil {
		s.ExitTime = nil
	} else {
		exitTime := time.Time(*source.ExitTime)
		s.ExitTime = &exitTime
	}
	s.StayTime = source.StayTime
}
