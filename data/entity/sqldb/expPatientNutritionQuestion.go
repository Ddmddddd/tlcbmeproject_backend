package sqldb

import (
	"github.com/ktpswjz/httpserver/types"
	"time"
	"tlcbme_project/data/model/doctor"
)

type ExpPatientNutritionQuestionBase struct {
}

func (s ExpPatientNutritionQuestion) TableName() string {
	return "ExpPatientNutritionQuestion"
}

type ExpPatientNutritionQuestion struct {
	ExpPatientNutritionQuestionBase
	//
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	//
	PatientID uint64 `sql:"PatientID"`
	//
	Answer *string `sql:"Answer"`
	//
	CreateDate *time.Time `sql:"CreateDate"`
}

func (s *ExpPatientNutritionQuestion) CopyTo(target *doctor.ExpPatientNutritionQuestion) {
	if target == nil {
		return
	}
	target.SerialNo = s.SerialNo
	target.PatientID = &s.PatientID
	if s.Answer != nil {
		target.Answer = string(*s.Answer)
	}
	if s.CreateDate == nil {
		target.CreateDate = nil
	} else {
		createDate := types.Time(*s.CreateDate)
		target.CreateDate = &createDate
	}
}

func (s *ExpPatientNutritionQuestion) CopyFrom(source *doctor.ExpPatientNutritionQuestion) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.PatientID = *source.PatientID
	answer := string(source.Answer)
	s.Answer = &answer
	if source.CreateDate == nil {
		s.CreateDate = nil
	} else {
		createDate := time.Time(*source.CreateDate)
		s.CreateDate = &createDate
	}
}
