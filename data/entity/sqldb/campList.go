package sqldb

import (
	"time"
	"tlcbme_project/data/model/doctor"
)

type CampListBase struct {
}

func (s CampListBase) TableName() string {
	return "CampList"
}

type CampList struct {
	CampListBase
	// 序号
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 训练营名称
	Name string `sql:"Name"`
	// 训练营描述
	Desc *string `sql:"Desc"`
	// 是否有效
	IsValid uint8 `sql:"IsValid"`
	// 创建时间
	CreateDateTime *time.Time `sql:"CreateDateTime"`
	// 更新时间
	UpdateDateTime *time.Time `sql:"UpdateDateTime"`
}

func (s *CampList) CopyTo(target *doctor.CampList) {
	if target == nil {
		return
	}
	target.SerialNo = s.SerialNo
	target.Name = s.Name
	target.IsValid = s.IsValid
	if s.Desc != nil {
		target.Desc = s.Desc
	}
	if s.CreateDateTime != nil {
		target.CreateDateTime = s.CreateDateTime
	}
	if s.UpdateDateTime != nil {
		target.UpdateDateTime = s.UpdateDateTime
	}
}

func (s *CampList) CopyToInfo(target *doctor.CampListInfo) {
	if target == nil {
		return
	}
	target.SerialNo = s.SerialNo
	target.Name = s.Name
	target.Desc = s.Desc
}

func (s *CampList) CopyFrom(source *doctor.CampList) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.Name = source.Name
	s.IsValid = source.IsValid
	s.Desc = source.Desc
	s.CreateDateTime = source.CreateDateTime
	s.UpdateDateTime = source.UpdateDateTime
}

func (s *CampList) CopyFromAddInput(source *doctor.CampListAddInput) {
	if source == nil {
		return
	}
	s.Name = source.Name
	s.Desc = source.Desc
}