package sqldb

import (
	"time"
	"tlcbme_project/data/model/doctor"
)

type ExpDoctorCommentToPatientSportDietRecordFilter struct {
	ExpDoctorCommentToPatientBase
	// 对应sportDietRecord表中的序号
	SportDietRecord uint64 `sql:"SportDietRecord"`
}

type ExpDoctorCommentToPatientUpdate struct {
	ExpDoctorCommentToPatientBase
	//
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 医生/营养师的评论
	Comment string `sql:"Comment"`
	// 修改时间
	UpdateTime *time.Time `sql:"UpdateTime"`
}

type ExpDoctorCommentToPatientReply struct {
	ExpDoctorCommentToPatientBase
	//
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	//
	PatientReply *string `sql:"PatientReply"`
}

func (s *ExpDoctorCommentToPatientUpdate) CopyFromCreate(source *doctor.ExpDoctorCommentToPatientCreate) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.Comment = source.Comment
	if source.UpdateTime == nil {
		s.UpdateTime = nil
	} else {
		updateTime := time.Time(*source.UpdateTime)
		s.UpdateTime = &updateTime
	}
}

type ExpDoctorCommentToPatientUnreadGet struct {
	ExpDoctorCommentToPatientBase
	// 患者是否已读，0-未读，1-已读
	ReadFlag uint64 `sql:"ReadFlag"`
	// 患者id
	PatientID uint64 `sql:"PatientID"`
}

func (s *ExpDoctorCommentToPatientUnreadGet) CopyFrom(source *doctor.ExpDoctorCommentToPatientUnreadGet) {
	if source == nil {
		return
	}
	s.ReadFlag = source.ReadFlag
	s.PatientID = source.PatientID
}

type ExpDoctorCommentToPatientRead struct {
	ExpDoctorCommentToPatientBase
	//
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 患者是否已读，0-未读，1-已读
	ReadFlag uint64 `sql:"ReadFlag"`
}

type ExpDoctorCommentToPatientFilter struct {
	ExpDoctorCommentToPatientBase
	//
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
}

type ExpDoctorCommentToPatientAllUnreadGet struct {
	ExpDoctorCommentToPatientBase
	// 患者是否已读，0-未读，1-已读
	ReadFlag uint64 `sql:"ReadFlag"`
}

type ExpDoctorCommentToPatientSportDietRecordSnosFilter struct {
	ExpDoctorCommentToPatientBase
	// 对应sportDietRecord表中的序号
	SportDietRecordList []uint64 `sql:"SportDietRecord" filter:"in"`
}

type ExpDoctorCommentToPatientTodayUnreadGet struct {
	ExpDoctorCommentToPatientBase
	// 患者是否已读，0-未读，1-已读
	ReadFlag uint64 `sql:"ReadFlag"`
	// 患者id
	PatientID uint64 `sql:"PatientID"`
	// 创建时间
	CreateDateTime *time.Time `sql:"CreateDateTime"`
}

func (s *ExpDoctorCommentToPatientTodayUnreadGet) CopyFrom(source *doctor.ExpDoctorCommentToPatientTodayUnreadGet) {
	if source == nil {
		return
	}
	s.ReadFlag = source.ReadFlag
	s.PatientID = source.PatientID
	if source.CreateDateTime == nil {
		s.CreateDateTime = nil
	} else {
		createDateTime := time.Time(*source.CreateDateTime)
		s.CreateDateTime = &createDateTime
	}
}

func (s *ExpDoctorCommentToPatientReply) CopyFromReply(source *doctor.ExpDoctorCommentToPatientReply) {
	if source == nil {
		return
	}
	s.PatientReply = source.PatientReply
	s.SerialNo = source.SerialNo
}