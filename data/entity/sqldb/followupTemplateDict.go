package sqldb

import "tlcbme_project/data/model/doctor"

type FollowupTemplateDictBase struct {
}

func (s FollowupTemplateDictBase) TableName() string {
	return "FollowupTemplateDict"
}

// 平台运维
// 基础字典
// 随访记录模板字典
// 注释：该表保存随访记录模板字典。
type FollowupTemplateDict struct {
	FollowupTemplateDictBase
	// 序号 主键，自增 例如:324
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 字典项代码  例如:1
	ItemCode uint64 `sql:"ItemCode"`
	// 字典项名称  例如:高血压随访模板
	ItemName string `sql:"ItemName"`
	// 模板所属管理分类代码 表示当前模板属于哪类慢病 例如:2323
	ManageClassCode uint64 `sql:"ManageClassCode"`
	// 输入码 用于快速输入，可以是拼音首字母或缩写 例如:gxy
	InputCode *string `sql:"InputCode"`
	// 有效性 用于描述本记录是否有效，0-无效，1-有效，默认值为1 例如:1
	IsValid uint64 `sql:"IsValid"`
}

func (s *FollowupTemplateDict) CopyTo(target *doctor.FollowupTemplateDict) {
	if target == nil {
		return
	}
	target.SerialNo = s.SerialNo
	target.ItemCode = s.ItemCode
	target.ItemName = s.ItemName
	target.ManageClassCode = s.ManageClassCode
	if s.InputCode != nil {
		target.InputCode = string(*s.InputCode)
	}
	target.IsValid = s.IsValid
}

func (s *FollowupTemplateDict) CopyFrom(source *doctor.FollowupTemplateDict) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.ItemCode = source.ItemCode
	s.ItemName = source.ItemName
	s.ManageClassCode = source.ManageClassCode
	inputCode := string(source.InputCode)
	s.InputCode = &inputCode
	s.IsValid = source.IsValid
}
