package sqldb

import (
	"tlcbme_project/data/model/doctor"
	"encoding/json"
	"fmt"
	"github.com/ktpswjz/httpserver/types"
	"time"
)

func (s *ManageApiFailRecord) CopyToEx(target *doctor.ManageApiFailRecordEx) {
	if target == nil {
		return
	}
	target.SerialNo = s.SerialNo
	target.PatientID = s.PatientID
	target.ProviderType = s.ProviderType
	target.BusinessName = s.BusinessName
	target.RepeatCount = s.RepeatCount
	target.ErrMsg = s.ErrMsg
	if s.DateTime == nil {
		target.DateTime = nil
	} else {
		dateTime := types.Time(*s.DateTime)
		target.DateTime = &dateTime
	}
	target.Uri = s.Uri
	if len(s.Input) > 0 {
		err := json.Unmarshal(s.Input, &target.Input)
		if err != nil {
			target.Input = string(s.Input)
		}
	}
}

type ManageApiFailRecordOrder struct {
	ManageApiFailRecordBase

	// 调用间 接口调用日期及时间 例如:2018-08-04 18:23:53
	DateTime *time.Time `sql:"DateTime" order:"DESC"`
	// 序号  例如:1808041823390094
	SerialNo uint64 `sql:"SerialNo"  order:"DESC"`
}

type ManageApiFailRecordOrder2 struct {
	ManageApiFailRecordBase

	// 调用间 接口调用日期及时间 例如:2018-08-04 18:23:53
	DateTime *time.Time `sql:"DateTime"`
	// 序号  例如:1808041823390094
	SerialNo uint64 `sql:"SerialNo"`
}

type ManageApiFailRecordFilter struct {
	ManageApiFailRecordBase
	// 序号  例如:1808041823390094
	SerialNos []uint64 `sql:"SerialNo" auto:"true" primary:"true" filter:"IN"`
	// 用户ID 关联患者用户登录表用户ID 例如:232442
	PatientID *uint64 `sql:"PatientID"`
	// 服务类型 接口服务提供者类型，1-高血压服务；2-糖尿病服务, 如1等 例如:1
	ProviderType *uint64 `sql:"ProviderType"`
	// 调用间 接口调用日期及时间 例如:2018-08-04 18:23:53
	DateStart *time.Time `sql:"DateTime" filter:">="`
	DateEnd   *time.Time `sql:"DateTime" filter:"<"`
	// 业务名称 接口服务名称，如注冊病人、注冊疾病等 例如:注冊病人
	BusinessName string `sql:"BusinessName" filter:"like"`
	// 地址 调用接口地址 例如:/1.0/patient/regpatient
	Uri string `sql:"Uri" filter:"like"`
}

func (s *ManageApiFailRecordFilter) CopyFrom(source *doctor.ManageApiFailRecordFilter) {
	if source == nil {
		return
	}
	if len(source.SerialNos) > 0 {
		s.SerialNos = source.SerialNos
	}
	s.PatientID = source.PatientID
	s.ProviderType = source.ProviderType

	if source.DateStart != nil {
		s.DateStart = source.DateStart.ToDate(0)
	}
	if source.DateEnd != nil {
		s.DateEnd = source.DateEnd.ToDate(1)
	}

	if len(source.BusinessName) > 0 {
		s.BusinessName = fmt.Sprint("%", source.BusinessName, "%")
	}
	if len(source.Uri) > 0 {
		s.Uri = fmt.Sprint("%", source.Uri, "%")
	}
}

type ManageApiFailRecordUpdate struct {
	ManageApiFailRecordBase
	// 序号  例如:1808041823390094
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 重试次数 尝试调用次数 例如:0
	RepeatCount uint64 `sql:"RepeatCount"`
	// 错误信息 错误信息 例如:0
	ErrMsg string `sql:"ErrMsg"`
	// 调用间 接口调用日期及时间 例如:2018-08-04 18:23:53
	DateTime *time.Time `sql:"DateTime"`
}
