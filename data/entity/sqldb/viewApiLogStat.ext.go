package sqldb

import (
	"tlcbme_project/data/model/doctor"
	"fmt"
	"time"
)

type ViewApiLogStatFilter struct {
	ViewApiLogStatBase

	// 地址 调用接口地址 例如:/auth/info
	Uri string `sql:"Uri" filter:"like"`
}

func (s *ViewApiLogStatFilter) CopyFrom(source *doctor.ViewApiLogStatFilter) {
	if source == nil {
		return
	}

	if len(source.Uri) > 0 {
		s.Uri = fmt.Sprint("%", source.Uri, "%")
	}
}

func (s *ViewApiLogStat) CopyToEx(target *doctor.ViewApiLogStatEx) {
	if target == nil {
		return
	}
	s.CopyTo(&target.ViewApiLogStat)

	if s.AvgTime != nil {
		target.AvgTimeText = time.Duration(int64(*s.AvgTime)).String()
	}
	if s.MaxTime != nil {
		target.MaxTimeText = time.Duration(int64(*s.MaxTime)).String()
	}
	if s.MinTime != nil {
		target.MinTimeText = time.Duration(int64(*s.MinTime)).String()
	}
}
