package sqldb

import "tlcbme_project/data/model/doctor"

func (s *DrugUseRecordIndexModifiedLog) CopyFromCreate(source *doctor.DrugUseRecordIndexModifiedLogCreate) {
	if source == nil {
		return
	}
	s.DURISerialNo = source.DURISerialNo
	s.PatientID = source.PatientID
	s.DrugName = source.DrugName
	dosage := string(source.Dosage)
	s.Dosage = &dosage
	freq := string(source.Freq)
	s.Freq = &freq
	s.ModifyType = source.ModifyType
}

type DrugUseRecordIndexModifiedLogOrder struct {
	DrugUseRecordIndexModifiedLogBase

	// 修改时间 例如:2018-08-12 13:22:21
	ModifyDateTime *string `sql:"ModifyDateTime"`
}

type DrugUseRecordIndexModifiedLogFilter struct {
	DrugUseRecordIndexModifiedLogBase

	// 序号 主键，自增 例如:324
	SerialNo *uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 用药记录索引表序号 关联DrugUseRecordIndex表的SerialNo 例如:232442
	DURISerialNo *uint64 `sql:"DURISerialNo"`
	// 用户ID 关联患者用户登录表用户ID 例如:232442
	PatientID *uint64 `sql:"PatientID"`
	// 药品名称  例如:氨氯地平片
	DrugName string `sql:"DrugName"`
}

func (s *DrugUseRecordIndexModifiedLogFilter) CopyFrom(source *doctor.DrugUseRecordIndexModifiedLogFilter) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.DURISerialNo = source.DURISerialNo
	s.PatientID = source.PatientID
	s.DrugName = source.DrugName
}
