package sqldb

import "time"

type ExpIntegralFilter struct {
	ExpIntegralBase
	//
	PatientID uint64 `sql:"PatientID"`
	//
	Reason string `sql:"Reason"`
	//
	CreateDateTime *time.Time `sql:"CreateDateTime" filter:">"`
}

type ExpIntegralPatientFilter struct {
	ExpIntegralBase
	//
	PatientID uint64 `sql:"PatientID"`
}

type ExpIntegralUnreadGet struct {
	ExpIntegralBase
	//
	PatientID uint64 `sql:"PatientID"`
	//
	ReadFlag uint64 `sql:"ReadFlag"`
}

type ExpIntegralReadUpdate struct {
	ExpIntegralBase
	//
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	//
	ReadFlag uint64 `sql:"ReadFlag"`
}

type ExpIntegralUpdate struct {
	ExpIntegralBase
	Integral uint64 `sql:"Integral"`
	CreateDateTime *time.Time `sql:"CreateDateTime"`
	ReadFlag uint64 `sql:"ReadFlag"`
}