package sqldb

import (
	"tlcbme_project/data/enum"
	"tlcbme_project/data/model/doctor"
	"time"
)

type DoctorUserBaseInfoEdit struct {
	DoctorUserBaseInfoBase

	// 姓名  例如:张三
	Name string `sql:"Name"`
	// 性别 0-未知, 1-男, 2-女，9-未说明 例如:1
	Sex *uint64 `sql:"Sex"`
	// 出生日期 例如:2000-12-12
	DateOfBirth *time.Time `sql:"DateOfBirth"`
	// 身份证号 例如:330106200012129876
	IdentityCardNumber string `sql:"IdentityCardNumber"`
	// 国籍 GB/T 2659-2000 世界各国和地区名称 例如:中国
	Country string `sql:"Country"`
	// 民族 GB 3304-1991 中国各民族名称 例如:汉族
	Nation string `sql:"Nation"`
	// 籍贯  例如:浙江杭州
	NativePlace string `sql:"NativePlace"`
	// 头像照片 例如:
	Photo string `sql:"Photo"`
	// 昵称 例如:蓝精灵
	Nickname string `sql:"Nickname"`
	// 个性签名 例如:在那山的那边海的那边有一群蓝精灵
	PersonalSign string `sql:"PersonalSign"`
	// 联系电话 例如:13812344321
	Phone string `sql:"Phone"`
	// 文化程度 GB/T 4658-1984 文化程度 例如:大学
	EducationLevel string `sql:"EducationLevel"`
	// 职务 例如:科主任
	Job string `sql:"Job"`
	// 职称 例如:主任医师
	Title string `sql:"Title"`
}

func (s *DoctorUserBaseInfoEdit) CopyFrom(source *doctor.DoctorUserBaseInfoEdit) {
	if source == nil {
		return
	}

	s.Name = source.Name
	s.Sex = source.Sex
	if source.DateOfBirth == nil {
		s.DateOfBirth = nil
	} else {
		dateOfBirth := time.Time(*source.DateOfBirth)
		s.DateOfBirth = &dateOfBirth
	}
	s.IdentityCardNumber = source.IdentityCardNumber
	s.Country = source.Country
	s.Nation = source.Nation
	s.NativePlace = source.NativePlace
	s.Photo = source.Photo
	s.Nickname = source.Nickname
	s.PersonalSign = source.PersonalSign
	s.Phone = source.Phone
	s.EducationLevel = source.EducationLevel
	s.Job = source.Job
	s.Title = source.Title
}

func (s *DoctorUserBaseInfo) CopyFromEdit(source *doctor.DoctorUserBaseInfoEdit) {
	if source == nil {
		return
	}
	s.UserID = source.UserID
	s.Name = source.Name
	s.Sex = source.Sex
	if source.DateOfBirth == nil {
		s.DateOfBirth = nil
	} else {
		dateOfBirth := time.Time(*source.DateOfBirth)
		s.DateOfBirth = &dateOfBirth
	}
	identityCardNumber := string(source.IdentityCardNumber)
	s.IdentityCardNumber = &identityCardNumber
	country := string(source.Country)
	s.Country = &country
	nation := string(source.Nation)
	s.Nation = &nation
	nativePlace := string(source.NativePlace)
	s.NativePlace = &nativePlace
	s.Photo = []byte(source.Photo)
	nickname := string(source.Nickname)
	s.Nickname = &nickname
	personalSign := string(source.PersonalSign)
	s.PersonalSign = &personalSign
	phone := string(source.Phone)
	s.Phone = &phone
	educationLevel := string(source.EducationLevel)
	s.EducationLevel = &educationLevel
	job := string(source.Job)
	s.Job = &job
	title := string(source.Title)
	s.Title = &title
}

func (s *DoctorUserBaseInfo) CopyToEx(target *doctor.DoctorUserBaseInfoEx) {
	if target == nil {
		return
	}
	s.CopyTo(&target.DoctorUserBaseInfo)

	if s.Sex != nil {
		target.SexText = enum.Sexes.Value(*s.Sex)
	}
}

type DoctorUserBaseInfoFilter struct {
	DoctorUserBaseInfoBase

	// 用户ID 关联医生用户登录表用户ID 例如:232442
	UserID uint64 `sql:"UserID"`
}
