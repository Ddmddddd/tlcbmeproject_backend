package sqldb

import (
	"github.com/ktpswjz/httpserver/types"
	"time"
	doctor2 "tlcbme_project/data/model/doctor"
)

type ShareRecordBase struct {
}

func (s ShareRecordBase) TableName() string {
	return "ShareRecord"
}

type ShareRecord struct {
	ShareRecordBase
	//
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 分享人
	PatientID uint64 `sql:"PatientID"`
	// 分享时间
	ShareDateTime *time.Time `sql:"ShareDateTime"`
	// 餐次
	TimePoint string `sql:"TimePoint"`
	// 分享图片
	ShareImage string `sql:"ShareImage"`
}

func (s *ShareRecord) CopyTo(target *doctor2.ShareRecord) {
	if target == nil {
		return
	}
	target.SerialNo = s.SerialNo
	target.PatientID = s.PatientID
	if s.ShareDateTime == nil {
		target.ShareDateTime = nil
	} else {
		shareDateTime := types.Time(*s.ShareDateTime)
		target.ShareDateTime = &shareDateTime
	}
	target.TimePoint = s.TimePoint
	target.ShareImage = s.ShareImage
}

func (s *ShareRecord) CopyFrom(source *doctor2.ShareRecord) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.PatientID = source.PatientID
	if source.ShareDateTime == nil {
		s.ShareDateTime = nil
	} else {
		shareDateTime := time.Time(*source.ShareDateTime)
		s.ShareDateTime = &shareDateTime
	}
	s.TimePoint = source.TimePoint
	s.ShareImage = source.ShareImage
}
