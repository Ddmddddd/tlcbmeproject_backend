package sqldb

import "time"

type ViewweightrecordFilter struct {
	// 测量时间 测量体重时的时间 例如:2018-07-03 14:45:00
	Measuredatetime *time.Time `sql:"MeasureDateTime" filter:"<"`
	// 管理机构代码 管理该患者的机构 例如:897798
	Orgcode *string `sql:"OrgCode"`
}
