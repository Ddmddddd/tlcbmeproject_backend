package sqldb

import (
	"tlcbme_project/data/model/doctor"
	"github.com/ktpswjz/httpserver/types"
	"time"
)

type ExpActionPlanRecordBase struct {
}

func (s ExpActionPlanRecordBase) TableName() string {
	return "ExpActionPlanRecord"
}

// 行动计划记录表
// 用于记录患者的行动计划完成情况
type ExpActionPlanRecord struct {
	ExpActionPlanRecordBase
	// 序号
	SerialNo uint64 `sql:"SerialNo"  auto:"true" primary:"true"`
	// 用户ID
	PatientID uint64 `sql:"PatientID"`
	// 行动计划
	ActionPlan string `sql:"ActionPlan"`
	// 行动计划完成情况，0-少量完成，1-部分完成，2-全部完成
	Completion uint64 `sql:"Completion"`
	// 优先级，分为0-5，数值越大优先级越高
	Level uint64 `sql:"Level"`
	// 创建时间
	CreateDateTime *time.Time `sql:"CreateDateTime"`
	// 对应引擎端生活计划的id，用于提交、修改等操作
	SeqActionPlan uint64 `sql:"SeqActionPlan"`
	// 对应ExpActionPlan的SerialNo，用于查询、修改等操作
	ActionPlanID uint64 `sql:"ActionPlanID"`
}

func (s *ExpActionPlanRecord) CopyTo(target *doctor.ExpActionPlanRecord) {
	if target == nil {
		return
	}
	target.SerialNo = s.SerialNo
	target.PatientID = s.PatientID
	target.ActionPlan = s.ActionPlan
	target.Completion = s.Completion
	target.Level = s.Level
	if s.CreateDateTime == nil {
		target.CreateDateTime = nil
	} else {
		createDateTime := types.Time(*s.CreateDateTime)
		target.CreateDateTime = &createDateTime
	}
	target.SeqActionPlan = s.SeqActionPlan
	target.ActionPlanID = s.ActionPlanID
}

func (s *ExpActionPlanRecord) CopyFrom(source *doctor.ExpActionPlanRecord) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.PatientID = source.PatientID
	s.ActionPlan = source.ActionPlan
	s.Completion = source.Completion
	s.Level = source.Level
	if source.CreateDateTime == nil {
		s.CreateDateTime = nil
	} else {
		createDateTime := time.Time(*source.CreateDateTime)
		s.CreateDateTime = &createDateTime
	}
	s.SeqActionPlan = source.SeqActionPlan
	s.ActionPlanID = source.ActionPlanID
}

func (s *ExpActionPlanRecord) CopyFromApp(source *doctor.ExpActionPlanRecordForApp) {
	if source == nil {
		return
	}
	s.PatientID = source.PatientID
	s.ActionPlan = source.ActionPlan
	s.Completion = source.Completion
	s.Level = source.Level
	if source.CreateDateTime == nil {
		s.CreateDateTime = nil
	} else {
		createDateTime := time.Time(*source.CreateDateTime)
		s.CreateDateTime = &createDateTime
	}
	s.SeqActionPlan = source.SeqActionPlan
	s.ActionPlanID = source.ActionPlanID
}

func (s *ExpActionPlanRecord) CopyFromActionPlan(source *doctor.ExpActionPlan) {
	if source == nil {
		return
	}
	s.PatientID = source.PatientID
	s.ActionPlan = source.ActionPlan
	s.SeqActionPlan = source.SeqActionPlan
	s.Level = source.Level
	s.ActionPlanID = source.SerialNo
}