package sqldb

import "tlcbme_project/data/model/doctor"

type SmsTemplateBase struct {
}

func (s SmsTemplateBase) TableName() string {
	return "SmsTemplate"
}

// 平台运维
// 短信
// 短信模板
// 注释：本表保存短信模板
type SmsTemplate struct {
	SmsTemplateBase
	// 序号 主键，自增 例如:324
	SerialNo uint64 `sql:"SerialNo" primary:"true"`
	// 模板类型 0-验证码，1-短信通知，2-推广短信 例如:1
	TemplateType uint64 `sql:"TemplateType"`
	// 模板代码 阿里云分配的短信模板代码 例如:SMS_1330248924729
	TemplateCode string `sql:"TemplateCode"`
	// 模板名称  例如:复诊提醒短信
	TemplateName string `sql:"TemplateName"`
	// 短信内容  例如:【浙大健康小微】${患者姓名}您好，您的复诊时间是${复诊时间}，请您于近日前往医院复查。祝您健康！
	TemplateContent string `sql:"TemplateContent"`
	// 有效性 用于描述本记录是否有效，0-无效，1-有效，默认值为1 例如:1
	IsValid uint64 `sql:"IsValid"`
}

func (s *SmsTemplate) CopyTo(target *doctor.SmsTemplate) {
	if target == nil {
		return
	}
	target.SerialNo = s.SerialNo
	target.TemplateType = s.TemplateType
	target.TemplateCode = s.TemplateCode
	target.TemplateName = s.TemplateName
	target.TemplateContent = s.TemplateContent
	target.IsValid = s.IsValid
}

func (s *SmsTemplate) CopyFrom(source *doctor.SmsTemplate) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.TemplateType = source.TemplateType
	s.TemplateCode = source.TemplateCode
	s.TemplateName = source.TemplateName
	s.TemplateContent = source.TemplateContent
	s.IsValid = source.IsValid
}
