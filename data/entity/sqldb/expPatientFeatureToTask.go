package sqldb

import (
	"tlcbme_project/data/model/doctor"
	"github.com/ktpswjz/httpserver/types"
	"time"
)

type ExpPatientFeatureToTaskBase struct {
}

func (s ExpPatientFeatureToTaskBase) TableName() string {
	return "ExpPatientFeatureToTask"
}

type ExpPatientFeatureToTask struct {
	ExpPatientFeatureToTaskBase
	//
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 任务序号
	TaskID uint64 `sql:"TaskID"`
	// 任务名称
	TaskName string `sql:"TaskName"`
	// 对应特征
	Feature string `sql:"Feature"`
	// 编辑者id
	EditorID uint64 `sql:"EditorID"`
	// 创建时间
	CreateDateTime *time.Time `sql:"CreateDateTime"`
	// 更新时间
	UpdateTime *time.Time `sql:"UpdateTime"`
}

func (s *ExpPatientFeatureToTask) CopyTo(target *doctor.ExpPatientFeatureToTask) {
	if target == nil {
		return
	}
	target.SerialNo = s.SerialNo
	target.TaskID = s.TaskID
	target.TaskName = s.TaskName
	target.Feature = s.Feature
	target.EditorID = s.EditorID
	if s.CreateDateTime == nil {
		target.CreateDateTime = nil
	} else {
		createDateTime := types.Time(*s.CreateDateTime)
		target.CreateDateTime = &createDateTime
	}
	if s.UpdateTime == nil {
		target.UpdateTime = nil
	} else {
		updateTime := types.Time(*s.UpdateTime)
		target.UpdateTime = &updateTime
	}
}

func (s *ExpPatientFeatureToTask) CopyFrom(source *doctor.ExpPatientFeatureToTask) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.TaskID = source.TaskID
	s.TaskName = source.TaskName
	s.Feature = source.Feature
	s.EditorID = source.EditorID
	if source.CreateDateTime == nil {
		s.CreateDateTime = nil
	} else {
		createDateTime := time.Time(*source.CreateDateTime)
		s.CreateDateTime = &createDateTime
	}
	if source.UpdateTime == nil {
		s.UpdateTime = nil
	} else {
		updateTime := time.Time(*source.UpdateTime)
		s.UpdateTime = &updateTime
	}
}
