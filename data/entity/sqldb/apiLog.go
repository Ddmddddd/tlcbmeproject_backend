package sqldb

import (
	"tlcbme_project/data/model/doctor"
	"github.com/ktpswjz/httpserver/types"
	"time"
)

type ApiLogBase struct {
}

func (s ApiLogBase) TableName() string {
	return "ApiLog"
}

// 平台运维
// 日志
// 平台接口调用记录
type ApiLog struct {
	ApiLogBase
	// 请求序号  例如:1808041823390094
	SerialNo uint64 `sql:"SerialNo" primary:"true"`
	// 调用方法 调用接口使用的方法，如GET、 POST等 例如:POST
	Method string `sql:"Method"`
	// 协议 调用接口使用的协议，http或https 例如:http
	Schema string `sql:"Schema"`
	// 地址 调用接口地址 例如:/auth/info
	Uri string `sql:"Uri"`
	// 开始时间 接口收到请求时间 例如:2018-08-04 18:23:53
	StartTime *time.Time `sql:"StartTime"`
	// 结束时间 接口收到请求后返回时间 例如:2018-08-04 18:23:55
	EndTime *time.Time `sql:"EndTime"`
	// 耗时 接口耗时，单位纳秒 例如:1808
	ElapseTime uint64 `sql:"ElapseTime"`
	// 耗时文本 接口耗时显示文本 例如:199.305µs
	ElapseTimeText string `sql:"ElapseTimeText"`
	// 调用者IP地址 接口调用者的远程IP地址 例如:192.168.1.101
	RIP string `sql:"RIP"`
	// 结果 接口调用结果，0表示成功，其它表示失败 例如:0
	Result uint64 `sql:"Result"`
	// 输入参数
	Input []byte `sql:"Input"`
	// 输出参数
	Output []byte `sql:"Output"`
	// 地址栏参数
	Param []byte `sql:"Param"`
	// 响应标识ID 例如:hbp-1222-8-9c5e06b9-7a6f-4715-a341-f5428a2d0ffb
	ResponseID *string `sql:"ResponseID"`
}

func (s *ApiLog) CopyTo(target *doctor.ApiLog) {
	if target == nil {
		return
	}
	target.SerialNo = s.SerialNo
	target.Method = s.Method
	target.Schema = s.Schema
	target.Uri = s.Uri
	if s.StartTime == nil {
		target.StartTime = nil
	} else {
		startTime := types.Time(*s.StartTime)
		target.StartTime = &startTime
	}
	if s.EndTime == nil {
		target.EndTime = nil
	} else {
		endTime := types.Time(*s.EndTime)
		target.EndTime = &endTime
	}
	target.ElapseTime = s.ElapseTime
	target.ElapseTimeText = s.ElapseTimeText
	target.RIP = s.RIP
	target.Result = s.Result
	target.Input = s.Input
	target.Output = s.Output
	target.Param = s.Param
	if s.ResponseID != nil {
		target.ResponseID = string(*s.ResponseID)
	}
}

func (s *ApiLog) CopyFrom(source *doctor.ApiLog) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.Method = source.Method
	s.Schema = source.Schema
	s.Uri = source.Uri
	if source.StartTime == nil {
		s.StartTime = nil
	} else {
		startTime := time.Time(*source.StartTime)
		s.StartTime = &startTime
	}
	if source.EndTime == nil {
		s.EndTime = nil
	} else {
		endTime := time.Time(*source.EndTime)
		s.EndTime = &endTime
	}
	s.ElapseTime = source.ElapseTime
	s.ElapseTimeText = source.ElapseTimeText
	s.RIP = source.RIP
	s.Result = source.Result
	s.Input = source.Input
	s.Output = source.Output
	s.Param = source.Param
	responseID := string(source.ResponseID)
	s.ResponseID = &responseID
}
