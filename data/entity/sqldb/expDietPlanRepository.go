package sqldb

import (
	doctor2 "tlcbme_project/data/model/doctor"
)

type ExpDietPlanRepositoryBase struct {
}

func (s ExpDietPlanRepositoryBase) TableName() string {
	return "ExpDietPlanRepository"
}

type ExpDietPlanRepository struct {
	ExpDietPlanRepositoryBase
	//
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 饮食目标
	Plan string `sql:"Plan"`
	// 图片url
	PicUrl *string `sql:"PicUrl"`
	// 周or天
	Type *string `sql:"Type"`
	//优先级
	Level uint64 `sql:"Level"`
}

func (s *ExpDietPlanRepository) CopyTo(target *doctor2.ExpDietPlanRepository) {
	if target == nil {
		return
	}
	target.SerialNo = s.SerialNo
	target.Plan = s.Plan
	if s.PicUrl != nil {
		target.PicUrl = string(*s.PicUrl)
	}
	if s.Type != nil {
		target.Type = string(*s.Type)
	}
	target.Level = s.Level
}

func (s *ExpDietPlanRepository) CopyFrom(source *doctor2.ExpDietPlanRepository) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.Plan = source.Plan
	picUrl := string(source.PicUrl)
	s.PicUrl = &picUrl
	stype := string(source.Type)
	s.Type = &stype
	s.Level = source.Level
}
