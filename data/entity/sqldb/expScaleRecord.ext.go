package sqldb

type ExpScaleRecordFilter struct {
	ExpScaleRecordBase
	// 序号
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
}

type ExpScaleRecordFilterByID struct {
	ExpScaleRecordBase
	// 用户ID
	PatientID uint64 `sql:"PatientID"`
}