package sqldb

import "time"

type TlcTeamIntegralBase struct {
}

func (s TlcTeamIntegralBase) TableName() string {
	return "tlcteamintegral"
}

type TlcTeamIntegral struct {
	TlcTeamIntegralBase
	SerialNo   uint64     `sql:"SerialNo"`
	TeamSno    uint64     `sql:"TeamSno"`
	TaskSno    uint64     `sql:"TaskSno"`
	Integral   uint64     `sql:"Integral"`
	CreateTime *time.Time `sql:"CreateTime"`
}
