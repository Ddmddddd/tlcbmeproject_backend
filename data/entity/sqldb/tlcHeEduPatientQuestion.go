package sqldb

import (
	"time"
)

type TlcHeEduPatientQuestionBase struct {
}

func (s TlcHeEduPatientQuestionBase) TableName() string {
	return "tlcheedupatientquestion"
}

type TlcHeEduPatientQuestion struct {
	TlcHeEduPatientQuestionBase
	Id           uint64     `sql:"id" auto:"true" primary:"true"`
	UserID       uint64     `sql:"UserID"`
	Question_id  uint64     `sql:"question_id"`
	Status       uint64     `sql:"status"`
	Answer_time  *time.Time `sql:"answer_time"`
	Knowledge_id uint64     `sql:"knowledge_id"`
}
