package sqldb

import (
	"bytes"
	"tlcbme_project/data/model/doctor"
	"encoding/gob"
	"github.com/ktpswjz/httpserver/types"
	"time"
)

type ViewdruguserecordBase struct {
}

func (s ViewdruguserecordBase) TableName() string {
	return "ViewDrugUseRecord"
}

func (s ViewdruguserecordBase) SetFilter(v interface{}) {

}

func (s *ViewdruguserecordBase) Clone() (interface{}, error) {
	return &ViewdruguserecordBase{}, nil
}

// VIEW
type Viewdruguserecord struct {
	ViewdruguserecordBase
	// 序号 主键，自增 例如:324
	Serialno uint64 `sql:"SerialNo"`
	// 用户ID 关联患者用户登录表用户ID 例如:232442
	Patientid uint64 `sql:"PatientID"`
	// 用药时间 用药的实际时间 例如:2018-07-03 14:45:00
	Usedatetime *time.Time `sql:"UseDateTime"`
	// 管理机构代码 管理该患者的机构 例如:897798
	Orgcode *string `sql:"OrgCode"`
}

func (s *Viewdruguserecord) Clone() (interface{}, error) {
	t := &Viewdruguserecord{}

	buf := new(bytes.Buffer)
	enc := gob.NewEncoder(buf)
	dec := gob.NewDecoder(buf)
	err := enc.Encode(s)
	if err != nil {
		return nil, err
	}
	err = dec.Decode(t)
	if err != nil {
		return nil, err
	}

	return t, nil
}

func (s *Viewdruguserecord) CopyTo(target *doctor.Viewdruguserecord) {
	if target == nil {
		return
	}
	target.Serialno = s.Serialno
	target.Patientid = s.Patientid
	if s.Usedatetime == nil {
		target.Usedatetime = nil
	} else {
		usedatetime := types.Time(*s.Usedatetime)
		target.Usedatetime = &usedatetime
	}
	if s.Orgcode != nil {
		target.Orgcode = string(*s.Orgcode)
	}
}

func (s *Viewdruguserecord) CopyFrom(source *doctor.Viewdruguserecord) {
	if source == nil {
		return
	}
	s.Serialno = source.Serialno
	s.Patientid = source.Patientid
	if source.Usedatetime == nil {
		s.Usedatetime = nil
	} else {
		usedatetime := time.Time(*source.Usedatetime)
		s.Usedatetime = &usedatetime
	}
	orgcode := string(source.Orgcode)
	s.Orgcode = &orgcode
}
