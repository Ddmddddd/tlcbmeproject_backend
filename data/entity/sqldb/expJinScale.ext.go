package sqldb

type ExpJinScaleEmpty struct {
	ExpJinScaleBase
}

type ExpJinScaleNameFilter struct {
	ExpJinScaleBase
	// 问卷名称
	ScaleName string `sql:"ScaleName"`
}

type ExpJinScaleCampFilter struct {
	ExpJinScaleBase
	Description *string `sql:"Description"`
}
