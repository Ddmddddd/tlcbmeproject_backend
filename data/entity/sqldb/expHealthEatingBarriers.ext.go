package sqldb

type ExpHealthEatingBarriersFilter struct {
	ExpHealthEatingBarriersBase
	// 序号
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
}

type ExpHealthEatingBarriersOrder struct {
	ExpHealthEatingBarriersBase
	// 序号
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
}

