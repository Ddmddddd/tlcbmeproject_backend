package sqldb

import (
	"tlcbme_project/data/model/doctor"
	"encoding/json"
	"github.com/ktpswjz/httpserver/types"
	"time"
)

type ExpJinScaleRecordBase struct {
}

func (s ExpJinScaleRecordBase) TableName() string {
	return "ExpJinScaleRecord"
}

type ExpJinScaleRecord struct {
	ExpJinScaleRecordBase
	//
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 患者id
	PatientID uint64 `sql:"PatientID"`
	// 问卷名称
	ScaleName string `sql:"ScaleName"`
	// 对应金数据里的serial_number
	SerialNumber float64 `sql:"SerialNumber"`
	// 问卷内容
	Record string `sql:"Record"`
	//
	CreateDateTime *time.Time `sql:"CreateDateTime"`
}

func (s *ExpJinScaleRecord) CopyTo(target *doctor.ExpJinScaleRecord) {
	if target == nil {
		return
	}
	target.SerialNo = s.SerialNo
	target.PatientID = s.PatientID
	target.ScaleName = s.ScaleName
	target.SerialNumber = s.SerialNumber
	record := s.Record
	if len(record) > 0 {
		json.Unmarshal([]byte(record), &target.Record)
	}

	if s.CreateDateTime == nil {
		target.CreateDateTime = nil
	} else {
		createDateTime := types.Time(*s.CreateDateTime)
		target.CreateDateTime = &createDateTime
	}
}

func (s *ExpJinScaleRecord) CopyFrom(source *doctor.ExpJinScaleRecord) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.PatientID = source.PatientID
	s.ScaleName = source.ScaleName
	s.SerialNumber = source.SerialNumber
	if source.Record != nil {
		recordData, err := json.Marshal(source.Record)
		if err == nil {
			record := string(recordData)
			s.Record = record
		}
	}

	if source.CreateDateTime == nil {
		s.CreateDateTime = nil
	} else {
		createDateTime := time.Time(*source.CreateDateTime)
		s.CreateDateTime = &createDateTime
	}
}
