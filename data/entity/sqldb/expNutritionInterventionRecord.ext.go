package sqldb

import "time"

type ExpNutritionInterventionRecordFilter struct {
	ExpNutritionInterventionRecordBase
	// 患者ID
	PatientID *uint64 `sql:"PatientID"`
	// 总结结束日期
	EndDate *time.Time `sql:"EndDate" filter:">="`
}

type ExpNutritionInterventionRecordOrder struct {
	// 序列号
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true" order:"DESC"`
}

type ExpNutritionInterventionRecordHabitUpdate struct {
	ExpNutritionInterventionRecordBase
	// 饮食目标完成次数
	Habit *string `sql:"Habit"`
}

type ExpNutritionInterventionRecordNoFilter struct {
	ExpNutritionInterventionRecordBase
	// 序列号
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
}
