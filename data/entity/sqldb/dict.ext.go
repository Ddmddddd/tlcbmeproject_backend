package sqldb

type DictFilter struct {
	SerialNo *uint64 `sql:"SerialNo" auto:"true"`
}

type DictCodeItemFilter struct {
	// 输入码 用于快速输入，可以是拼音首字母或缩写 例如:gxy
	InputCode string `sql:"InputCode" filter:"like"`
	// 有效性 用于描述本记录是否有效，0-无效，1-有效，默认值为1 例如:1
	IsValid uint64 `sql:"IsValid"`
}
