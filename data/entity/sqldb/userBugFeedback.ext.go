package sqldb

import (
	"time"
	"tlcbme_project/data/model/doctor"
)

func (s *UserBugFeedback) CopyFromApp(source *doctor.UserBugFeedbackApp) {
	if source == nil {
		return
	}
	s.PatientID = &source.PatientID
	content := string(source.Content)
	s.Content = &content
	photo := string(source.Photo)
	s.Photo = &photo
	if source.DateTime == nil {
		s.DateTime = nil
	} else {
		dateTime := time.Time(*source.DateTime)
		s.DateTime = &dateTime
	}
}