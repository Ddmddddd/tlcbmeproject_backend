package sqldb

import (
	"tlcbme_project/data/model/doctor"
	"github.com/ktpswjz/httpserver/types"
	"time"
)

type ManagementApplicationAuditBase struct {
}

func (s ManagementApplicationAuditBase) TableName() string {
	return "ManagementApplicationAudit"
}

// 慢病管理业务
// 工作平台管理数据
// 管理申请审核
// 注释：该表保存待审核的患者记录。审核通过后，将患者记录写入到管理患者索引表。
type ManagementApplicationAudit struct {
	ManagementApplicationAuditBase
	// 序号 主键，自增 例如:324
	SerialNo uint64 `sql:"SerialNo" auto:"true"`
	// 用户ID 关联患者用户登录表用户ID 例如:232442
	PatientID uint64 `sql:"PatientID"`
	// 诊断 例如:高血压，糖尿病
	Diagnosis string `sql:"Diagnosis"`
	// 诊断备注  例如:血压 150/90 mmHg，血糖 7.9 mmol/L
	DiagnosisMemo string `sql:"DiagnosisMemo"`
	// 就诊机构代码 例如:123
	VisitOrgCode string `sql:"VisitOrgCode"`
	// 机构内就诊号 例如:123456
	OrgVisitID string `sql:"OrgVisitID"`
	// 审核状态 0-未审核，1-审核通过，2-审核不通过 例如:0
	Status uint64 `sql:"Status"`
	// 审核人ID 审核人对应的用户ID 例如:32423
	AuditID uint64 `sql:"AuditID"`
	// 审核人姓名 例如:张三
	AuditName string `sql:"AuditName"`
	// 审核时间 例如:2018-07-03 13:00:00
	AuditDateTime *time.Time `sql:"AuditDateTime"`
	// 审核不通过原因 最近一次审核不通过的原因。只有当状态为2时有效。 例如:信息不够完善
	RefuseReason string `sql:"RefuseReason"`
}

func (s *ManagementApplicationAudit) CopyTo(target *doctor.ManagementApplicationAudit) {
	if target == nil {
		return
	}
	target.SerialNo = s.SerialNo
	target.PatientID = s.PatientID
	target.Diagnosis = s.Diagnosis
	target.DiagnosisMemo = s.DiagnosisMemo
	target.VisitOrgCode = s.VisitOrgCode
	target.OrgVisitID = s.OrgVisitID
	target.Status = s.Status
	target.AuditID = s.AuditID
	target.AuditName = s.AuditName
	if s.AuditDateTime == nil {
		target.AuditDateTime = nil
	} else {
		auditDateTime := types.Time(*s.AuditDateTime)
		target.AuditDateTime = &auditDateTime
	}
	target.RefuseReason = s.RefuseReason
}

func (s *ManagementApplicationAudit) CopyFrom(source *doctor.ManagementApplicationAudit) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.PatientID = source.PatientID
	s.Diagnosis = source.Diagnosis
	s.DiagnosisMemo = source.DiagnosisMemo
	s.VisitOrgCode = source.VisitOrgCode
	s.OrgVisitID = source.OrgVisitID
	s.Status = source.Status
	s.AuditID = source.AuditID
	s.AuditName = source.AuditName
	if source.AuditDateTime == nil {
		s.AuditDateTime = nil
	} else {
		auditDateTime := time.Time(*source.AuditDateTime)
		s.AuditDateTime = &auditDateTime
	}
	s.RefuseReason = source.RefuseReason
}
