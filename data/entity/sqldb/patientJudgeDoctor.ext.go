package sqldb

import (
	"time"
	"tlcbme_project/data/model/doctor"
)

func (s *PatientJudgeDoctor) CopyFromApp(source *doctor.PatientJudgeDoctorApp) {
	if source == nil {
		return
	}
	s.PatientID = &source.PatientID
	s.DoctorID = &source.DoctorID
	content := string(source.Content)
	s.Content = &content
	s.Star = &source.Star
	if source.DateTime == nil {
		s.DateTime = nil
	} else {
		dateTime := time.Time(*source.DateTime)
		s.DateTime = &dateTime
	}
}
