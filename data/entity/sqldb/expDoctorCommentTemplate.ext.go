package sqldb

type ExpDoctorCommentTemplateTypeFilter struct {
	ExpDoctorCommentTemplateBase
	// 模板类型，1-饮食评论模板
	TemplateType uint64 `sql:"TemplateType"`
	// 开放属性，0-所有，1-私人
	OpenType uint64 `sql:"OpenType"`
	// 编辑者id
	EditorID uint64 `sql:"EditorID"`
}

type ExpDoctorCommentTemplateOpenTypeFilter struct {
	ExpDoctorCommentTemplateBase
	// 开放属性，0-所有，1-私人
	OpenType uint64 `sql:"OpenType"`
}

type ExpDoctorCommentTemplateFilter struct {
	ExpDoctorCommentTemplateBase
	//
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
}

type ExpDoctorCommentTemplateOpenTypeOrderDesc struct {
	ExpDoctorCommentTemplateBase
	// 开放属性，0-所有，1-私人
	OpenType uint64 `sql:"OpenType" order:"desc"`
}

type ExpDoctorCommentTemplateEditorFilter struct {
	ExpDoctorCommentTemplateBase
	//
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 编辑者id
	EditorID uint64 `sql:"EditorID"`
}

type ExpDoctorCommentTemplateOpenTypeFilter2 struct {
	ExpDoctorCommentTemplateBase
	//
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 开放属性，0-所有，1-私人
	OpenType uint64 `sql:"OpenType"`
}
