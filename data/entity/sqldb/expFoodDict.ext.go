package sqldb

import "tlcbme_project/data/model/doctor"

type ExpFoodDictCategoryFilter struct {
	ExpFoodDictBase
	// 分类
	Category uint64 `sql:"Category"`
}

func (s *ExpFoodDictCategoryFilter) CopyFrom(source *doctor.ExpFoodDictCategoryFilter) {
	if source == nil {
		return
	}
	s.Category = source.Category
}

type FoodSearchFilter struct {
	ExpFoodDictBase
	// 名字
	Name string `sql:"Name" filter:"like"`
}

func (s *FoodSearchFilter) CopyFrom(source *doctor.FoodSearchFilter) {
	if source == nil {
		return
	}
	s.Name = source.Name
}