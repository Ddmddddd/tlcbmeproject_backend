package sqldb

import (
	"tlcbme_project/data/model/doctor"
	"github.com/ktpswjz/httpserver/types"
	"time"
)

type ExpKnowledgeToPatientBase struct {
}

func (s ExpKnowledgeToPatientBase) TableName() string {
	return "ExpKnowledgeToPatient"
}

// 该表存储患者每天收到的知识，内容只涉及增和查，不涉及删和改
type ExpKnowledgeToPatient struct {
	ExpKnowledgeToPatientBase
	//
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 患者id
	PatientID uint64 `sql:"PatientID"`
	// 知识类型
	Type string `sql:"Type"`
	// 知识id
	KnowledgeID uint64 `sql:"KnowledgeID"`
	// 创建时间
	CreateDateTime *time.Time `sql:"CreateDateTime"`
	// 对应计划的日期
	DayIndex uint64 `sql:"DayIndex"`
	// 记录完成情况，0-未完成，1-已阅读、未答题，2-已完成
	Status uint64 `sql:"Status"`
}

func (s *ExpKnowledgeToPatient) CopyTo(target *doctor.ExpKnowledgeToPatient) {
	if target == nil {
		return
	}
	target.SerialNo = s.SerialNo
	target.PatientID = s.PatientID
	target.Type = s.Type
	target.KnowledgeID = s.KnowledgeID
	if s.CreateDateTime == nil {
		target.CreateDateTime = nil
	} else {
		createDateTime := types.Time(*s.CreateDateTime)
		target.CreateDateTime = &createDateTime
	}
	target.DayIndex = s.DayIndex
	target.Status = s.Status
}

func (s *ExpKnowledgeToPatient) CopyFrom(source *doctor.ExpKnowledgeToPatient) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.PatientID = source.PatientID
	s.Type = source.Type
	s.KnowledgeID = source.KnowledgeID
	if source.CreateDateTime == nil {
		s.CreateDateTime = nil
	} else {
		createDateTime := time.Time(*source.CreateDateTime)
		s.CreateDateTime = &createDateTime
	}
	s.DayIndex = source.DayIndex
	s.Status = source.Status
}
