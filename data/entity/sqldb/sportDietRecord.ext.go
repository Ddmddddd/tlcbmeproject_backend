package sqldb

import (
	"bytes"
	"github.com/ktpswjz/httpserver/types"
	"strconv"
	"time"
	"tlcbme_project/data/model/doctor"
)

type SportDietRecordFilter struct {
	SportDietRecordBase

	// 序号 主键，自增 例如:324
	SerialNo *uint64 `sql:"SerialNo"`
}

type StepRecordData struct {
	SportDietRecordBase

	// 序号 主键，自增 例如:324
	SerialNo uint64 `sql:"SerialNo"`
	// 步数 单位：步 例如:3699
	StepCount *string `sql:"ItemValue"`
	// 发生时间 记录运动的时间 例如:2018-07-03 14:45:00
	HappenDateTime *time.Time `sql:"HappenDateTime"`
	// 备注 用于说明测量时的情况 例如:有氧运动半小时后
	Memo *string `sql:"Memo"`
}

type DietRecordData struct {
	SportDietRecordBase

	// 序号组 例如:324，248
	SerialNo string `sql:"SerialNo"`
	// 食物类型 例如:肉类
	Kinds string `sql:"ItemName"`
	// 食量 单位：g 例如:60
	Appetite *string `sql:"ItemValue"`
	// 发生时间 记录运动的时间 例如:2018-07-03 14:45:00
	HappenDateTime *time.Time `sql:"HappenDateTime"`
	HappenPlace    *string    `sql:"HappenPlace"`
	// 记录的照片
	Photo *string `sql:"Photo"`
	// 备注 用于说明测量时的情况 例如:有氧运动半小时后
	Memo         *string `sql:"Memo"`
	DietRecordNo uint64  `sql:"DietRecordNo"`
}

type DietRecordDataEx struct {
	DietRecordData

	// 序号 主键，自增 例如:324
	SerialNo string `sql:"SerialNo"`
	// 时间点 早、中、晚 例如:早
	TimePoint *string `sql:"TimePoint"`
}

type SportDietRecordDataFilter struct {
	SportDietRecordBase

	// 用户ID 关联患者用户登录表用户ID 例如:232442
	PatientID uint64 `sql:"PatientID"`
	// 记录类型 用于标识是饮食还是运动，目前其值为以下两个之一：饮食、运动 例如:饮食
	RecordType string `sql:"RecordType"`
	// 发生时间 记录运动/饮食的时间 例如:2018-07-03 14:45:00
	HappenStartDate *time.Time `sql:"HappenDateTime" filter:">="`
	HappenEndDate   *time.Time `sql:"HappenDateTime" filter:"<"`
}

type SportDietRecordDataOrder struct {
	SportDietRecordBase

	// 发生时间 记录运动的时间 例如:2018-07-03 14:45:00
	HappenDateTime *time.Time `sql:"HappenDateTime"`
}

type StepRecordDataFilter struct {
	SportDietRecordBase

	// 用户ID 关联患者用户登录表用户ID 例如:232442
	PatientID uint64 `sql:"PatientID"`

	// 发生时间 记录运动的时间 例如:2018-07-03 14:45:00
	HappenStartDate *time.Time `sql:"HappenDateTime" filter:">="`
	HappenEndDate   *time.Time `sql:"HappenDateTime" filter:"<"`
	ItemName        string     `sql:"ItemName"`
}

type DietRecordDataForTeamFuncFilter struct {
	SportDietRecordBase

	// 记录类型 用于标识是饮食还是运动，目前其值为以下两个之一：饮食、运动 例如:饮食
	RecordType string `sql:"RecordType"`

	// 用户ID 关联患者用户登录表用户ID 例如:232442
	PatientID uint64 `sql:"PatientID"`

	// 发生时间 记录运动的时间 例如:2018-07-03 14:45:00
	HappenStartDate *time.Time `sql:"HappenDateTime" filter:">="`

	// 时间点 早、中、晚 例如:早
	TimePoint *string `sql:"TimePoint"`
}

type DietRecordDataForTeamFuncGroupFilter struct {
	SportDietRecordBase

	// 记录类型 用于标识是饮食还是运动，目前其值为以下两个之一：饮食、运动 例如:饮食
	RecordType string `sql:"RecordType"`

	// 用户ID 关联患者用户登录表用户ID 例如:232442
	PatientID uint64 `sql:"PatientID"`

	// 时间点 早、中、晚 例如:早
	TimePoint *string `sql:"TimePoint"`
}

func (s *StepRecordData) CopyTo(target *doctor.StepRecordData) {
	if target == nil {
		return
	}
	stepCount, err := strconv.ParseUint(*s.StepCount, 10, 64)
	if err == nil {
		target.StepCount = stepCount
	} else {
		target.StepCount = uint64(0)
	}
	if s.HappenDateTime == nil {
		target.HappenDateTime = nil
	} else {
		happenDateTime := types.Time(*s.HappenDateTime)
		target.HappenDateTime = &happenDateTime
	}
	if s.Memo != nil {
		target.Memo = string(*s.Memo)
	}
}

func (s *SportDietRecordDataFilter) CopyFrom(source *doctor.SportDietRecordDataFilterEx) {
	if source == nil {
		return
	}

	s.PatientID = source.PatientID
	if source.HappenStartDate != nil {
		s.HappenStartDate = source.HappenStartDate.ToDate(0)
	}
	if source.HappenEndDate != nil {
		s.HappenEndDate = source.HappenEndDate.ToDate(1)
	}
}

func (s *StepRecordDataFilter) CopyFrom(source *doctor.SportDietRecordDataFilterEx) {
	if source == nil {
		return
	}

	s.PatientID = source.PatientID
	if source.HappenStartDate != nil {
		s.HappenStartDate = source.HappenStartDate.ToDate(0)
	}
	if source.HappenEndDate != nil {
		s.HappenEndDate = source.HappenEndDate.ToDate(1)
	}
	s.ItemName = string("步行")
}

func (s *SportDietRecord) CopyFromSportRecord(source *doctor.SportRecordForAppEx) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.PatientID = source.PatientID
	s.RecordType = string("运动")
	s.ItemName = source.SportsType
	intensity := string(source.Intensity)
	s.SportIntensity = &intensity
	s.Photo = &source.Photo
	var buf bytes.Buffer
	buf.WriteString(source.SportsType)
	if source.SportsType == "微信运动" {
		stepCount := strconv.FormatUint(source.StepCount, 10)
		s.ItemValue = &stepCount
		buf.WriteString(stepCount)
		itemUnit := string("步")
		s.ItemUnit = &itemUnit
		buf.WriteString("步 运动强度")
		buf.WriteString(source.Intensity)
		//s.Description = buf.String()
	} else {
		durationTime := strconv.FormatUint(source.DurationTime, 10)
		s.ItemValue = &durationTime
		itemUnit := string("min")
		s.ItemUnit = &itemUnit
		buf.WriteString(durationTime)
		buf.WriteString("min 运动强度")
		buf.WriteString(source.Intensity)
		//s.Description = buf.String()
	}
	memo := source.Memo
	s.Memo = &memo
	if source.HappenDateTime == nil {
		s.HappenDateTime = nil
	} else {
		measureDateTime := time.Time(*source.HappenDateTime)
		s.HappenDateTime = &measureDateTime
	}
	now := time.Now()
	s.InputDateTime = &now
	s.Description = source.Description
}

func (s *SportDietRecord) CopyToSportRecordData(target *doctor.SportRecordData) {
	if target == nil {
		return
	}

	target.SportsType = s.ItemName
	if s.SportIntensity != nil {
		target.Intensity = string(*s.SportIntensity)
	}
	itemValue, err := strconv.ParseUint(*s.ItemValue, 10, 64)
	if err != nil {
		itemValue = uint64(0)
	}
	if s.ItemName == "微信运动" {
		target.StepCount = itemValue
	} else {
		target.DurationTime = itemValue
	}
	if s.Memo != nil {
		target.Memo = string(*s.Memo)
	}
	if s.HappenDateTime == nil {
		target.HappenDateTime = nil
	} else {
		measureDateTime := types.Time(*s.HappenDateTime)
		target.HappenDateTime = &measureDateTime
	}
}

func (s *SportDietRecord) CopyToSportRecord(target *doctor.SportRecordForApp) {
	if target == nil {
		return
	}
	target.Description = s.Description
	target.SerialNo = s.SerialNo
	target.SportsType = s.ItemName
	if s.SportIntensity != nil {
		target.Intensity = string(*s.SportIntensity)
	}
	itemValue, err := strconv.ParseUint(*s.ItemValue, 10, 64)
	if err != nil {
		itemValue = uint64(0)
	}
	if s.ItemName == "微信运动" {
		target.StepCount = itemValue
	} else {
		target.DurationTime = itemValue
	}
	if s.Photo != nil {
		target.Photo = *s.Photo
	}
	if s.Memo != nil {
		target.Memo = string(*s.Memo)
	}
	if s.HappenDateTime == nil {
		target.HappenDateTime = nil
	} else {
		measureDateTime := types.Time(*s.HappenDateTime)
		target.HappenDateTime = &measureDateTime
	}
}

func (s *DietRecordData) CopyTo(target *doctor.DietRecordData) {
	if target == nil {
		return
	}

	target.Kinds = s.Kinds
	if s.Photo != nil {
		target.Photo = *s.Photo
	}
	if s.Appetite != nil {
		target.Appetite = string(*s.Appetite)
	}
	if s.HappenDateTime == nil {
		target.HappenDateTime = nil
	} else {
		happenDateTime := types.Time(*s.HappenDateTime)
		target.HappenDateTime = &happenDateTime
	}
	if s.Memo != nil {
		target.Memo = string(*s.Memo)
	}
}

func (s *DietRecordDataEx) CopyToApp(target *doctor.DietRecordForApp) {
	if target == nil {
		return
	}

	target.SerialNo = s.SerialNo
	target.Kinds = s.Kinds
	if s.Appetite != nil {
		target.Appetite = string(*s.Appetite)
	}
	if s.HappenDateTime == nil {
		target.HappenDateTime = nil
	} else {
		happenDateTime := types.Time(*s.HappenDateTime)
		target.HappenDateTime = &happenDateTime
	}
	if s.Photo != nil {
		target.Photo = *s.Photo
	}
	if s.Memo != nil {
		target.Memo = string(*s.Memo)
	}
	if s.HappenPlace != nil {
		target.HappenPlace = string(*s.HappenPlace)
	}
	target.Type = TimePointToType(s.TimePoint)
	target.DietRecordNo = s.DietRecordNo

}

//type SportDietRecordPatientAndItemFilter struct {
//	SportDietRecordBase
//	// 用户ID 关联患者用户登录表用户ID 例如:232442
//	PatientID 		uint64 		`sql:"PatientID"`
//	ItemName        string     	`sql:"ItemName"`
//}

func (s *SportDietRecord) CopyFromWeRunData(patientID uint64, source *doctor.WeRunData) {
	if source == nil {
		return
	}
	s.PatientID = patientID
	s.RecordType = "运动"
	s.ItemName = "微信运动"
	itemValue := strconv.FormatUint(source.Step, 10)
	s.ItemValue = &itemValue
	itemUnit := "步"
	s.ItemUnit = &itemUnit
	var buf bytes.Buffer
	buf.WriteString("步行")
	buf.WriteString(itemValue)
	buf.WriteString("步")
	s.Description = buf.String()
	tm := time.Unix(int64(source.TimeStamp), 0)
	s.HappenDateTime = &tm
	now := time.Now()
	s.InputDateTime = &now
}

// 组队功能模块查询微信步数

type SportDietRecordHappenDateTime struct {
	SportDietRecordBase
	// 发生时间 实际发生的时间 例如:2018-07-03 14:45:00
	HappenDateTime *time.Time `sql:"HappenDateTime"`
}

type SportDietRecordStepFilterByPatientIDList struct {
	SportDietRecordBase
	// 记录类型 用于标识是饮食还是运动，目前其值为以下两个之一：饮食、运动 例如:饮食
	RecordType string `sql:"RecordType"`
	// 项目名称 例如：米饭 例如:米饭
	ItemName string `sql:"ItemName"`

	PatientIDList []uint64 `sql:"PatientID" filter:"in"`
}

type SportDietRecordStepItemValue struct {
	SportDietRecordBase
	// 项目值 例如：200 例如:200
	ItemValue *string `sql:"ItemValue"`
}
