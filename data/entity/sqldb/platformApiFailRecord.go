package sqldb

import (
	"tlcbme_project/data/model/doctor"
	"github.com/ktpswjz/httpserver/types"
	"time"
)

type PlatformApiFailRecordBase struct {
}

func (s PlatformApiFailRecordBase) TableName() string {
	return "PlatformApiFailRecord"
}

// 平台运维
// 日志
// 接口调用失败记录表
// 注释：本表保存信息平台服务接口调用失败的记录。
type PlatformApiFailRecord struct {
	PlatformApiFailRecordBase
	// 序号 主键，自增 例如:324
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 用户ID 关联患者用户登录表用户ID 例如:232442
	PatientID uint64 `sql:"PatientID"`
	// 业务名称 接口服务名称，如数据报送等 例如:数据报送
	BusinessName string `sql:"BusinessName"`
	// 重试次数 尝试调用次数 例如:0
	RepeatCount uint64 `sql:"RepeatCount"`
	// 错误信息 错误信息 例如:0
	ErrMsg string `sql:"ErrMsg"`
	// 调用时间 接口调用日期及时间 例如:2018-08-04 18:23:53
	DateTime *time.Time `sql:"DateTime"`
	// 地址 调用接口地址 例如:/1.0/patient/regpatient
	Uri string `sql:"Uri"`
	// 输入参数 输入数据 例如:{json}
	Input []byte `sql:"Input"`
}

func (s *PlatformApiFailRecord) CopyTo(target *doctor.PlatformApiFailRecord) {
	if target == nil {
		return
	}
	target.SerialNo = s.SerialNo
	target.PatientID = s.PatientID
	target.BusinessName = s.BusinessName
	target.RepeatCount = s.RepeatCount
	target.ErrMsg = s.ErrMsg
	if s.DateTime == nil {
		target.DateTime = nil
	} else {
		dateTime := types.Time(*s.DateTime)
		target.DateTime = &dateTime
	}
	target.Uri = s.Uri
	target.Input = s.Input
}

func (s *PlatformApiFailRecord) CopyFrom(source *doctor.PlatformApiFailRecord) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.PatientID = source.PatientID
	s.BusinessName = source.BusinessName
	s.RepeatCount = source.RepeatCount
	s.ErrMsg = source.ErrMsg
	if source.DateTime == nil {
		s.DateTime = nil
	} else {
		dateTime := time.Time(*source.DateTime)
		s.DateTime = &dateTime
	}
	s.Uri = source.Uri
	s.Input = source.Input
}
