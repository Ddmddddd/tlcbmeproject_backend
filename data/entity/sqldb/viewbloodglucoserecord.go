package sqldb

import (
	"bytes"
	"tlcbme_project/data/model/doctor"
	"encoding/gob"
	"github.com/ktpswjz/httpserver/types"
	"time"
)

type ViewbloodglucoserecordBase struct {
}

func (s ViewbloodglucoserecordBase) TableName() string {
	return "ViewBloodGlucoseRecord"
}

func (s ViewbloodglucoserecordBase) SetFilter(v interface{}) {

}

func (s *ViewbloodglucoserecordBase) Clone() (interface{}, error) {
	return &ViewbloodglucoserecordBase{}, nil
}

// VIEW
type Viewbloodglucoserecord struct {
	ViewbloodglucoserecordBase
	// 序号 主键，自增 例如:324
	Serialno uint64 `sql:"SerialNo"`
	// 用户ID 关联患者用户登录表用户ID 例如:232442
	Patientid uint64 `sql:"PatientID"`
	// 血糖值 单位：mmol/L 例如:6.1
	Bloodglucose float64 `sql:"BloodGlucose"`
	// 测量时间 测量血糖时的时间 例如:2018-07-03 14:45:00
	Measuredatetime *time.Time `sql:"MeasureDateTime"`
	// 管理机构代码 管理该患者的机构 例如:897798
	Orgcode *string `sql:"OrgCode"`
}

func (s *Viewbloodglucoserecord) Clone() (interface{}, error) {
	t := &Viewbloodglucoserecord{}

	buf := new(bytes.Buffer)
	enc := gob.NewEncoder(buf)
	dec := gob.NewDecoder(buf)
	err := enc.Encode(s)
	if err != nil {
		return nil, err
	}
	err = dec.Decode(t)
	if err != nil {
		return nil, err
	}

	return t, nil
}

func (s *Viewbloodglucoserecord) CopyTo(target *doctor.Viewbloodglucoserecord) {
	if target == nil {
		return
	}
	target.Serialno = s.Serialno
	target.Patientid = s.Patientid
	target.Bloodglucose = s.Bloodglucose
	if s.Measuredatetime == nil {
		target.Measuredatetime = nil
	} else {
		measuredatetime := types.Time(*s.Measuredatetime)
		target.Measuredatetime = &measuredatetime
	}
	if s.Orgcode != nil {
		target.Orgcode = string(*s.Orgcode)
	}
}

func (s *Viewbloodglucoserecord) CopyFrom(source *doctor.Viewbloodglucoserecord) {
	if source == nil {
		return
	}
	s.Serialno = source.Serialno
	s.Patientid = source.Patientid
	s.Bloodglucose = source.Bloodglucose
	if source.Measuredatetime == nil {
		s.Measuredatetime = nil
	} else {
		measuredatetime := time.Time(*source.Measuredatetime)
		s.Measuredatetime = &measuredatetime
	}
	orgcode := string(source.Orgcode)
	s.Orgcode = &orgcode
}
