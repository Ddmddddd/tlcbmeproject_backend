package sqldb

import (
	"tlcbme_project/data/model/doctor"
	"github.com/ktpswjz/httpserver/types"
	"time"
)

type MonthlyBPInfoBase struct {
}

func (s MonthlyBPInfoBase) TableName() string {
	return "MonthlyBPInfo"
}

// 慢病管理业务
// 患者APP月报数据
// 月血压统计信息表
// 注释：本表保存患者的月血压统计记录。
type MonthlyBPInfo struct {
	MonthlyBPInfoBase
	// 序号 主键，自增 例如:324
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 用户ID 关联患者用户登录表用户ID 例如:232442
	PatientID uint64 `sql:"PatientID"`
	// 月份 年份+月份（2016-09） 例如:2016-09
	YearMonth string `sql:"YearMonth"`
	// 当前血压 当月最后一次血压 例如:132/82
	CurrentBP string `sql:"CurrentBP"`
	// 目标血压 控制血压的目标 例如:140/90
	GoalBP string `sql:"GoalBP"`
	// 最大收缩压 当月出现的最大收缩压 例如:156
	MaxSBP uint64 `sql:"MaxSBP"`
	// 最大收缩压时间 当月出现最大收缩压的时间 例如:2016-09-10 12:00:00
	MaxSBPTime *time.Time `sql:"MaxSBPTime"`
	// 最小收缩压 当月出现的最小收缩压 例如:124
	MinSBP uint64 `sql:"MinSBP"`
	// 最小收缩压时间 当月出现最小收缩压的时间 例如:2016-09-11 12:00:00
	MinSBPTime *time.Time `sql:"MinSBPTime"`
	// 最大舒张压 当月出现的最大舒张压 例如:102
	MaxDBP uint64 `sql:"MaxDBP"`
	// 最大舒张压时间 当月出现最大舒张压的时间 例如:2016-09-12 12:00:00
	MaxDBPTime *time.Time `sql:"MaxDBPTime"`
	// 最小舒张压 当月出现的最小舒张压 例如:83
	MinDBP uint64 `sql:"MinDBP"`
	// 最小舒张压时间 当月出现最小舒张压的时间 例如:2016-09-13 12:00:00
	MinDBPTime *time.Time `sql:"MinDBPTime"`
	// 最大脉压差 当月出现的最大脉压差 例如:48
	MaxRange uint64 `sql:"MaxRange"`
	// 最大脉压差时间 当月出现最大脉压差的时间 例如:2016-09-14 12:00:00
	MaxRangeTime *time.Time `sql:"MaxRangeTime"`
	// 最小脉压差 当月出现的最小脉压差 例如:26
	MinRange uint64 `sql:"MinRange"`
	// 最小脉压差时间 当月出现最小脉压差的时间 例如:2016-09-15 12:00:00
	MinRangeTime *time.Time `sql:"MinRangeTime"`
	// 平均收缩压 当月的平均收缩压 例如:135
	AvgSBP uint64 `sql:"AvgSBP"`
	// 平均舒张压 当月的平均舒张压 例如:98
	AvgDBP uint64 `sql:"AvgDBP"`
	// 平均脉压差 当月的平均脉压差 例如:35
	AvgRange uint64 `sql:"AvgRange"`
	// 血压正常次数 当月正常血压的次数 例如:6
	NormalTimes uint64 `sql:"NormalTimes"`
	// 血压偏高次数 当月偏高血压的次数 例如:4
	HigherTimes uint64 `sql:"HigherTimes"`
	// 血压偏低次数 当月偏低血压的次数 例如:2
	LowerTimes uint64 `sql:"LowerTimes"`
}

func (s *MonthlyBPInfo) CopyTo(target *doctor.MonthlyBPInfo) {
	if target == nil {
		return
	}
	target.SerialNo = s.SerialNo
	target.PatientID = s.PatientID
	target.YearMonth = s.YearMonth
	target.CurrentBP = s.CurrentBP
	target.GoalBP = s.GoalBP
	target.MaxSBP = s.MaxSBP
	if s.MaxSBPTime == nil {
		target.MaxSBPTime = nil
	} else {
		maxSBPTime := types.Time(*s.MaxSBPTime)
		target.MaxSBPTime = &maxSBPTime
	}
	target.MinSBP = s.MinSBP
	if s.MinSBPTime == nil {
		target.MinSBPTime = nil
	} else {
		minSBPTime := types.Time(*s.MinSBPTime)
		target.MinSBPTime = &minSBPTime
	}
	target.MaxDBP = s.MaxDBP
	if s.MaxDBPTime == nil {
		target.MaxDBPTime = nil
	} else {
		maxDBPTime := types.Time(*s.MaxDBPTime)
		target.MaxDBPTime = &maxDBPTime
	}
	target.MinDBP = s.MinDBP
	if s.MinDBPTime == nil {
		target.MinDBPTime = nil
	} else {
		minDBPTime := types.Time(*s.MinDBPTime)
		target.MinDBPTime = &minDBPTime
	}
	target.MaxRange = s.MaxRange
	if s.MaxRangeTime == nil {
		target.MaxRangeTime = nil
	} else {
		maxRangeTime := types.Time(*s.MaxRangeTime)
		target.MaxRangeTime = &maxRangeTime
	}
	target.MinRange = s.MinRange
	if s.MinRangeTime == nil {
		target.MinRangeTime = nil
	} else {
		minRangeTime := types.Time(*s.MinRangeTime)
		target.MinRangeTime = &minRangeTime
	}
	target.AvgSBP = s.AvgSBP
	target.AvgDBP = s.AvgDBP
	target.AvgRange = s.AvgRange
	target.NormalTimes = s.NormalTimes
	target.HigherTimes = s.HigherTimes
	target.LowerTimes = s.LowerTimes
}

func (s *MonthlyBPInfo) CopyFrom(source *doctor.MonthlyBPInfo) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.PatientID = source.PatientID
	s.YearMonth = source.YearMonth
	s.CurrentBP = source.CurrentBP
	s.GoalBP = source.GoalBP
	s.MaxSBP = source.MaxSBP
	if source.MaxSBPTime == nil {
		s.MaxSBPTime = nil
	} else {
		maxSBPTime := time.Time(*source.MaxSBPTime)
		s.MaxSBPTime = &maxSBPTime
	}
	s.MinSBP = source.MinSBP
	if source.MinSBPTime == nil {
		s.MinSBPTime = nil
	} else {
		minSBPTime := time.Time(*source.MinSBPTime)
		s.MinSBPTime = &minSBPTime
	}
	s.MaxDBP = source.MaxDBP
	if source.MaxDBPTime == nil {
		s.MaxDBPTime = nil
	} else {
		maxDBPTime := time.Time(*source.MaxDBPTime)
		s.MaxDBPTime = &maxDBPTime
	}
	s.MinDBP = source.MinDBP
	if source.MinDBPTime == nil {
		s.MinDBPTime = nil
	} else {
		minDBPTime := time.Time(*source.MinDBPTime)
		s.MinDBPTime = &minDBPTime
	}
	s.MaxRange = source.MaxRange
	if source.MaxRangeTime == nil {
		s.MaxRangeTime = nil
	} else {
		maxRangeTime := time.Time(*source.MaxRangeTime)
		s.MaxRangeTime = &maxRangeTime
	}
	s.MinRange = source.MinRange
	if source.MinRangeTime == nil {
		s.MinRangeTime = nil
	} else {
		minRangeTime := time.Time(*source.MinRangeTime)
		s.MinRangeTime = &minRangeTime
	}
	s.AvgSBP = source.AvgSBP
	s.AvgDBP = source.AvgDBP
	s.AvgRange = source.AvgRange
	s.NormalTimes = source.NormalTimes
	s.HigherTimes = source.HigherTimes
	s.LowerTimes = source.LowerTimes
}
