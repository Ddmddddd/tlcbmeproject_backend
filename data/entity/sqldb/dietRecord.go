package sqldb

import (
	"github.com/ktpswjz/httpserver/types"
	"time"
	"tlcbme_project/data/model/doctor"
)

type DietRecordBase struct {
}

func (s DietRecordBase) TableName() string {
	return "DietRecord"
}

// 饮食记录表
type DietRecord struct {
	DietRecordBase
	// 序号\\n324\\nFCM\\n主键，自增
	SerialNo uint64 `sql:"SerialNo" primary:"true" auto:"true"`
	// 用餐日期\n2018-07-03 \nFCM\n用餐的实际日期
	EatDateTime *time.Time `sql:"EatDateTime"`
	// 录入时间\n2018-07-03 14:00:00\nFCM\n将用药情况录入系统的时间
	InputDateTime *time.Time `sql:"InputDateTime"`
	// 用餐类型 早餐、中餐、晚餐
	DietType string `sql:"DietType"`
	// 用户ID
	PatientID uint64 `sql:"PatientID"`
}

func (s *DietRecord) CopyTo(target *doctor.DietRecord) {
	if target == nil {
		return
	}
	target.SerialNo = s.SerialNo
	if s.EatDateTime == nil {
		target.EatDateTime = nil
	} else {
		eatDateTime := types.Time(*s.EatDateTime)
		target.EatDateTime = &eatDateTime
	}
	if s.InputDateTime == nil {
		target.InputDateTime = nil
	} else {
		inputDateTime := types.Time(*s.InputDateTime)
		target.InputDateTime = &inputDateTime
	}
	target.DietType = s.DietType
	target.PatientID = s.PatientID
}

func (s *DietRecord) CopyFrom(source *doctor.DietRecord) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	if source.EatDateTime == nil {
		s.EatDateTime = nil
	} else {
		eatDateTime := time.Time(*source.EatDateTime)
		s.EatDateTime = &eatDateTime
	}
	if source.InputDateTime == nil {
		s.InputDateTime = nil
	} else {
		inputDateTime := time.Time(*source.InputDateTime)
		s.InputDateTime = &inputDateTime
	}
	s.DietType = source.DietType
	s.PatientID = source.PatientID
}

func (s *DietRecord) CopyFromAppEx(source *doctor.DietRecordEx) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	if source.EatDateTime == nil {
		s.EatDateTime = nil
	} else {
		eatDateTime := time.Time(*source.EatDateTime)
		s.EatDateTime = &eatDateTime
	}
	if source.InputDateTime == nil {
		s.InputDateTime = nil
	} else {
		inputDateTime := time.Time(*source.InputDateTime)
		s.InputDateTime = &inputDateTime
	}
	s.DietType = source.DietType
	s.PatientID = source.PatientID
}
