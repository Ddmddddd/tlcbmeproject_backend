package sqldb

import (
	"tlcbme_project/data/model/doctor"
	"encoding/json"
	"github.com/ktpswjz/httpserver/types"
	"time"
)

type WeChatRemindRecordBase struct {
}

func (s WeChatRemindRecordBase) TableName() string {
	return "WeChatRemindRecord"
}

type WeChatRemindRecord struct {
	WeChatRemindRecordBase
	// 序号
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 患者ID
	PatientID uint64 `sql:"PatientID"`
	// 接收患者的openid
	ReceiverOpenID string `sql:"ReceiverOpenID"`
	// 发送消息对应的订阅消息模板
	TemplateID string `sql:"TemplateID"`
	// 发送消息的具体内容
	RemindContent string `sql:"RemindContent"`
	// 发送时间
	SendDateTime *time.Time `sql:"SendDateTime"`
	// 请求响应
	Response string `sql:"Response"`
}

func (s *WeChatRemindRecord) CopyTo(target *doctor.WeChatRemindRecord) {
	if target == nil {
		return
	}
	target.SerialNo = s.SerialNo
	target.PatientID = s.PatientID
	target.ReceiverOpenID = s.ReceiverOpenID
	target.TemplateID = s.TemplateID
	remindContent := s.RemindContent
	if len(remindContent) > 0 {
		json.Unmarshal([]byte(remindContent), &target.RemindContent)
	}

	if s.SendDateTime == nil {
		target.SendDateTime = nil
	} else {
		sendDateTime := types.Time(*s.SendDateTime)
		target.SendDateTime = &sendDateTime
	}
	response := s.Response
	if len(response) > 0 {
		json.Unmarshal([]byte(response), &target.Response)
	}
}

func (s *WeChatRemindRecord) CopyFrom(source *doctor.WeChatRemindRecord) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.PatientID = source.PatientID
	s.ReceiverOpenID = source.ReceiverOpenID
	s.TemplateID = source.TemplateID
	if source.RemindContent != nil {
		remindContentData, err := json.Marshal(source.RemindContent)
		if err == nil {
			remindContent := string(remindContentData)
			s.RemindContent = remindContent
		}
	}

	if source.SendDateTime == nil {
		s.SendDateTime = nil
	} else {
		sendDateTime := time.Time(*source.SendDateTime)
		s.SendDateTime = &sendDateTime
	}
	if source.Response != nil {
		response, err := json.Marshal(source.Response)
		if err == nil {
			response := string(response)
			s.Response = response
		}
	}
}

func (s *WeChatRemindRecord) CopyFromSend(source *doctor.WeChatRemindSend) {
	if source == nil {
		return
	}
	s.PatientID = source.PatientID
	s.ReceiverOpenID = source.ReceiverOpenID
	s.TemplateID = source.TemplateID
	if source.Data != nil {
		remindContentData, err := json.Marshal(source.Data)
		if err == nil {
			remindContent := string(remindContentData)
			s.RemindContent = remindContent
		}
	}
}