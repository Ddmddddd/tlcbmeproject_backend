package sqldb

import (
	"tlcbme_project/data/model/doctor"
	"github.com/ktpswjz/httpserver/types"
	"time"
)

type UsabilityDeviceBase struct {
}

func (s UsabilityDeviceBase) TableName() string {
	return "UsabilityDevice"
}

// 平台运维
// 用户行为统计
// 用户设备统计表
// 注释：本表记录用户使用的设备信息
type UsabilityDevice struct {
	UsabilityDeviceBase
	// 序号 主键，自增 例如:1
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 用户名 关联患者用户登录表用户名 例如:aa
	UserName string `sql:"UserName"`
	// 应用标识 唯一标识应用 例如:abcdefghijklmn
	AppId string `sql:"AppId"`
	// 设备序列号 当前设备的序列号 例如:cn64wksj3sm8e
	DeviceIMEI *string `sql:"DeviceIMEI"`
	// 手机品牌 当前手机的品牌 例如:vivo x9 plus
	PhoneBrand *string `sql:"PhoneBrand"`
	// 系统版本 手机操作系统版本 例如:Android 7.0
	SystemVersion *string `sql:"SystemVersion"`
	// 记录时间 最后一次记录的时间 例如:2018-12-12 08:00:00
	RecordTime *time.Time `sql:"RecordTime"`
}

func (s *UsabilityDevice) CopyTo(target *doctor.UsabilityDevice) {
	if target == nil {
		return
	}
	target.SerialNo = s.SerialNo
	target.UserName = s.UserName
	target.AppId = s.AppId
	if s.DeviceIMEI != nil {
		target.DeviceIMEI = string(*s.DeviceIMEI)
	}
	if s.PhoneBrand != nil {
		target.PhoneBrand = string(*s.PhoneBrand)
	}
	if s.SystemVersion != nil {
		target.SystemVersion = string(*s.SystemVersion)
	}
	if s.RecordTime == nil {
		target.RecordTime = nil
	} else {
		recordTime := types.Time(*s.RecordTime)
		target.RecordTime = &recordTime
	}
}

func (s *UsabilityDevice) CopyFrom(source *doctor.UsabilityDevice) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.UserName = source.UserName
	s.AppId = source.AppId
	deviceIMEI := string(source.DeviceIMEI)
	s.DeviceIMEI = &deviceIMEI
	phoneBrand := string(source.PhoneBrand)
	s.PhoneBrand = &phoneBrand
	systemVersion := string(source.SystemVersion)
	s.SystemVersion = &systemVersion
	if source.RecordTime == nil {
		s.RecordTime = nil
	} else {
		recordTime := time.Time(*source.RecordTime)
		s.RecordTime = &recordTime
	}
}
