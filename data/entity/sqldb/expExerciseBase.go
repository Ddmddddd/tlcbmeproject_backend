package sqldb

import (
	doctor2 "tlcbme_project/data/model/doctor"
)

type ExpExerciseBaseBase struct {
}

func (s ExpExerciseBaseBase) TableName() string {
	return "ExpExerciseBase"
}

type ExpExerciseBase struct {
	ExpExerciseBaseBase
	//
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 训练板块，例如：热身、正式训练、拉伸活动
	Mode string `sql:"Mode"`
	// 运动
	Exercise string `sql:"Exercise"`
	// 目标肌群
	Target string `sql:"Target"`
	// 建议次数
	AdviceTimes string `sql:"AdviceTimes"`
	// 动作目的
	Purpose string `sql:"Purpose"`
	// 视频链接
	Media *string `sql:"Media"`
}

func (s *ExpExerciseBase) CopyTo(target *doctor2.ExpExerciseBase) {
	if target == nil {
		return
	}
	target.SerialNo = s.SerialNo
	target.Mode = s.Mode
	target.Exercise = s.Exercise
	target.Target = s.Target
	target.AdviceTimes = s.AdviceTimes
	target.Purpose = s.Purpose
	if s.Media != nil {
		target.Media = string(*s.Media)
	}
}

func (s *ExpExerciseBase) CopyFrom(source *doctor2.ExpExerciseBase) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.Mode = source.Mode
	s.Exercise = source.Exercise
	s.Target = source.Target
	s.AdviceTimes = source.AdviceTimes
	s.Purpose = source.Purpose
	media := string(source.Media)
	s.Media = &media
}
