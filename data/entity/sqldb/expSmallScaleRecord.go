package sqldb

import (
	"tlcbme_project/data/model/doctor"
	"encoding/json"
	"github.com/ktpswjz/httpserver/types"
	"time"
)

type ExpSmallScaleRecordBase struct {
}

func (s ExpSmallScaleRecordBase) TableName() string {
	return "ExpSmallScaleRecord"
}

// 患者小量表填写记录
type ExpSmallScaleRecord struct {
	ExpSmallScaleRecordBase
	// 序号
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 用户ID
	PatientID uint64 `sql:"PatientID"`
	// 量表结果
	Result string `sql:"Result"`
	// 量表ID
	SmallScaleID uint64 `sql:"SmallScaleID"`
	// 提交时间
	CreateDateTime *time.Time `sql:"CreateDateTime"`
}

func (s *ExpSmallScaleRecord) CopyTo(target *doctor.ExpSmallScaleRecord) {
	if target == nil {
		return
	}
	target.SerialNo = s.SerialNo
	target.PatientID = s.PatientID
	target.SmallScaleID = s.SmallScaleID
	result := s.Result
	if len(result) > 0 {
		json.Unmarshal([]byte(result), &target.Result)
	}
	if s.CreateDateTime == nil {
		target.CreateDateTime = nil
	} else {
		createDateTime := types.Time(*s.CreateDateTime)
		target.CreateDateTime = &createDateTime
	}
}

func (s *ExpSmallScaleRecord) CopyFrom(source *doctor.ExpSmallScaleRecord) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.PatientID = source.PatientID
	s.SmallScaleID = source.SmallScaleID
	if source.Result != nil {
		resultData, err := json.Marshal(source.Result)
		if err == nil {
			result := string(resultData)
			s.Result = result
		}
	}
	if source.CreateDateTime == nil {
		s.CreateDateTime = nil
	} else {
		createDateTime := time.Time(*source.CreateDateTime)
		s.CreateDateTime = &createDateTime
	}
}

func (s *ExpSmallScaleRecord) CopyFromInput(source *doctor.ExpSmallScaleRecordInput) {
	if source == nil {
		return
	}
	s.PatientID = source.PatientID
	s.SmallScaleID = source.SmallScaleID
	if source.Result != nil {
		resultData, err := json.Marshal(source.Result)
		if err == nil {
			result := string(resultData)
			s.Result = result
		}
	}
	if source.CreateDateTime == nil {
		s.CreateDateTime = nil
	} else {
		createDateTime := time.Time(*source.CreateDateTime)
		s.CreateDateTime = &createDateTime
	}
}