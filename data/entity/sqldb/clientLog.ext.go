package sqldb

import (
	"tlcbme_project/data/model/doctor"
	"encoding/json"
	"fmt"
	"github.com/ktpswjz/httpserver/types"
	"time"
)

type ClientLogOrder struct {
	ClientLogBase

	// 开始时间 接口收到请求时间 例如:2018-08-04 18:23:53
	StartTime *time.Time `sql:"StartTime" order:"DESC"`
	// 序号  例如:1808041823390094
	SerialNo uint64 `sql:"SerialNo"  order:"DESC"`
}

type ClientLogFilterBase struct {
	ClientLogBase

	// 请求序号  例如:1808041823390094
	SerialNo *uint64 `sql:"SerialNo"`
}

func (s *ClientLogFilterBase) CopyFrom(source *doctor.ClientLogFilterBase) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
}

type ClientLogFilter struct {
	ClientLogFilterBase

	// 调用方法 调用接口使用的方法，如GET、 POST等 例如:POST
	Method string `sql:"Method"`
	// 协议 调用接口使用的协议，http或https 例如:http
	Schema string `sql:"Schema"`
	// 主机或IP 接口地址主机或IP 例如:example.com
	Host string `sql:"Host" filter:"like"`
	// 端口号 接口地址中的端口号 例如:80
	Port string `sql:"Port"`
	// 地址 接口地址路径 例如:/auth/info
	Uri string `sql:"Uri" filter:"like"`
	// 开始时间 接口收到请求时间 例如:2018-08-04 18:23:53
	StartTime *time.Time `sql:"StartTime" filter:">="`
	// 结束时间 接口收到请求后返回时间 例如:2018-08-04 18:23:55
	EndTime *time.Time `sql:"StartTime" filter:"<"`
	// 结果 接口调用结果，0表示成功，其它表示失败 例如:0
	Result *uint64 `sql:"Result"`
	// 响应标识ID 例如:hbp-1222-8-9c5e06b9-7a6f-4715-a341-f5428a2d0fea
	ResponseID string `sql:"ResponseID" filter:"like"`
}

func (s *ClientLogFilter) CopyFrom(source *doctor.ClientLogFilter) {
	if source == nil {
		return
	}
	s.ClientLogFilterBase.CopyFrom(&source.ClientLogFilterBase)

	s.Method = source.Method
	s.Schema = source.Schema
	if len(source.Uri) > 0 {
		s.Uri = fmt.Sprint("%", source.Uri, "%")
	}
	if len(source.Host) > 0 {
		s.Host = fmt.Sprint("%", source.Host, "%")
	}
	if len(source.Port) > 0 {
		s.Port = fmt.Sprint("%", source.Port, "%")
	}
	if len(source.ResponseID) > 0 {
		s.ResponseID = fmt.Sprint("%", source.ResponseID, "%")
	}
	if source.StartTime == nil {
		s.StartTime = nil
	} else {
		startTime := time.Time(*source.StartTime)
		s.StartTime = &startTime
	}
	if source.EndTime == nil {
		s.EndTime = nil
	} else {
		endTime := time.Time(*source.EndTime)
		s.EndTime = &endTime
	}

	s.Result = source.Result
}

type ClientLogInfo struct {
	ClientLogBase
	// 序号  例如:1808041823390094
	SerialNo uint64 `sql:"SerialNo" auto:"true"`
	// 调用方法 调用接口使用的方法，如GET、 POST等 例如:POST
	Method string `sql:"Method"`
	// 协议 调用接口使用的协议，http或https 例如:http
	Schema string `sql:"Schema"`
	// 主机或IP 接口地址主机或IP 例如:example.com
	Host string `sql:"Host"`
	// 端口号 接口地址中的端口号 例如:80
	Port string `sql:"Port"`
	// 地址 接口地址路径 例如:/auth/info
	Uri string `sql:"Uri"`
	// 开始时间 接口收到请求时间 例如:2018-08-04 18:23:53
	StartTime *time.Time `sql:"StartTime"`
	// 结束时间 接口收到请求后返回时间 例如:2018-08-04 18:23:55
	EndTime *time.Time `sql:"EndTime"`
	// 耗时 接口耗时，单位纳秒 例如:1808
	ElapseTime uint64 `sql:"ElapseTime"`
	// 耗时文本 接口耗时显示文本 例如:199.305µs
	ElapseTimeText string `sql:"ElapseTimeText"`
	// 结果 接口调用结果，0表示成功，其它表示失败 例如:0
	Result uint64 `sql:"Result"`
	// 错误信息 接口调用错误时的信息 例如:异常
	ErrorMessage *string `sql:"ErrorMessage"`
	// 响应标识ID 例如:hbp-1222-8-9c5e06b9-7a6f-4715-a341-f5428a2d0fea
	ResponseID *string `sql:"ResponseID"`
}

func (s *ClientLogInfo) CopyTo(target *doctor.ClientLogInfo) {
	if target == nil {
		return
	}
	target.SerialNo = s.SerialNo
	target.Method = s.Method
	target.Schema = s.Schema
	target.Host = s.Host
	target.Port = s.Port
	target.Uri = s.Uri
	if s.StartTime == nil {
		target.StartTime = nil
	} else {
		startTime := types.Time(*s.StartTime)
		target.StartTime = &startTime
	}
	if s.EndTime == nil {
		target.EndTime = nil
	} else {
		endTime := types.Time(*s.EndTime)
		target.EndTime = &endTime
	}
	target.ElapseTime = s.ElapseTime
	target.ElapseTimeText = s.ElapseTimeText
	target.Result = s.Result
	if s.ErrorMessage != nil {
		target.ErrorMessage = string(*s.ErrorMessage)
	}
	if s.ResponseID != nil {
		target.ResponseID = string(*s.ResponseID)
	}
}

type ClientLogArgument struct {
	ClientLogBase

	// 响应标识ID 例如:hbp-1222-8-9c5e06b9-7a6f-4715-a341-f5428a2d0fea
	ResponseID *string `sql:"ResponseID"`
	// 输入参数
	Input []byte `sql:"Input"`
	// 输出参数
	Output []byte `sql:"Output"`
	// 地址栏参数
	Param []byte `sql:"Param"`
}

func (s *ClientLogArgument) CopyTo(target *doctor.ClientLogArgument) {
	if target == nil {
		return
	}

	if s.ResponseID != nil {
		target.ResponseID = string(*s.ResponseID)
	}

	if len(s.Input) > 0 {
		err := json.Unmarshal(s.Input, &target.Input)
		if err != nil {
			target.Input = string(s.Input)
		}
	}

	if len(s.Output) > 0 {
		err := json.Unmarshal(s.Output, &target.Output)
		if err != nil {
			target.Output = string(s.Output)
		}
	}

	if len(s.Param) > 0 {
		json.Unmarshal(s.Param, &target.Param)
	}
}
