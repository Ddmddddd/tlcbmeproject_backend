package sqldb

import (
	"tlcbme_project/data/model/doctor"
	"github.com/ktpswjz/httpserver/types"
	"time"
)

type DiscomfortRecordFilter struct {
	DiscomfortRecordBase

	// 序号 主键，自增 例如:324
	SerialNo *uint64 `sql:"SerialNo"`
}

type DiscomfortRecordData struct {
	DiscomfortRecordBase

	// 序号 主键，自增 例如:324
	SerialNo uint64 `sql:"SerialNo"`
	// 不适情况 如果有多种不适，之间用逗号分隔。例如：剧烈头痛，恶心呕吐。 例如:剧烈头痛，恶心呕吐
	Discomfort string `sql:"Discomfort"`
	// 备注  例如:
	Memo *string `sql:"Memo"`
	// 发生时间 不适情况发生的时间 例如:2018-07-03 14:45:00
	HappenDateTime *time.Time `sql:"HappenDateTime"`
}

type DiscomfortRecordDataFilter struct {
	DiscomfortRecordBase

	// 用户ID 关联患者用户登录表用户ID 例如:232442
	PatientID uint64 `sql:"PatientID"`

	// 发生时间 不适情况发生的时间 例如:2018-07-03 14:45:00
	HappenStartDate *time.Time `sql:"HappenDateTime" filter:">="`
	HappenEndDate   *time.Time `sql:"HappenDateTime" filter:"<"`
}

type DiscomfortRecordDataOrder struct {
	DiscomfortRecordBase

	// 发生时间 不适情况发生的时间 例如:2018-07-03 14:45:00
	HappenDateTime *time.Time `sql:"HappenDateTime"`
}

func (s *DiscomfortRecordData) CopyTo(target *doctor.DiscomfortRecordData) {
	if target == nil {
		return
	}

	target.Discomfort = s.Discomfort
	if s.HappenDateTime == nil {
		target.HappenDateTime = nil
	} else {
		happenDateTime := types.Time(*s.HappenDateTime)
		target.HappenDateTime = &happenDateTime
	}
	if s.Memo != nil {
		target.Memo = string(*s.Memo)
	}
}

func (s *DiscomfortRecordDataFilter) CopyFrom(source *doctor.DiscomfortRecordDataFilterEx) {
	if source == nil {
		return
	}

	s.PatientID = source.PatientID
	if source.HappenStartDate != nil {
		s.HappenStartDate = source.HappenStartDate.ToDate(0)
	}
	if source.HappenEndDate != nil {
		s.HappenEndDate = source.HappenEndDate.ToDate(1)
	}
}

func (s *DiscomfortRecord) CopyFromAppEx(source *doctor.DiscomfortRecordForAppEx) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.PatientID = source.PatientID
	s.Discomfort = source.Discomfort
	memo := string(source.Memo)
	s.Memo = &memo
	if source.HappenDateTime == nil {
		s.HappenDateTime = nil
	} else {
		measureDateTime := time.Time(*source.HappenDateTime)
		s.HappenDateTime = &measureDateTime
	}
	now := time.Now()
	s.InputDateTime = &now
}

func (s *DiscomfortRecord) CopyToApp(target *doctor.DiscomfortRecordForApp) {
	if target == nil {
		return
	}
	target.SerialNo = s.SerialNo
	target.Discomfort = s.Discomfort
	if s.Memo != nil {
		target.Memo = string(*s.Memo)
	}
	if s.HappenDateTime == nil {
		target.HappenDateTime = nil
	} else {
		happenDateTime := types.Time(*s.HappenDateTime)
		target.HappenDateTime = &happenDateTime
	}
}
