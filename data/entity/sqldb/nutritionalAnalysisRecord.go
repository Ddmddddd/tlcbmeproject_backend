package sqldb

import (
	"encoding/json"
	"github.com/ktpswjz/httpserver/types"
	"time"
	"tlcbme_project/data/model/doctor"
)

type NutritionalAnalysisRecordBase struct {
}

func (s NutritionalAnalysisRecordBase) TableName() string {
	return "NutritionalAnalysisRecord"
}

// 营养分析结论记录表
type NutritionalAnalysisRecord struct {
	NutritionalAnalysisRecordBase
	//
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 分析结论
	Conclusion string `sql:"Conclusion"`
	// 营养评分分数
	Grade float64 `sql:"Grade"`
	// 营养评分等级 1- 一般，2- 良好，3-优秀
	GradeLevel uint64 `sql:"GradeLevel"`
	// 食物种类，主食，蔬菜等
	FoodKinds *string `sql:"FoodKinds"`
	// 限制性营养素 钠
	Na *float64 `sql:"Na"`
	// 限制性营养素 添加糖
	Sugar *float64 `sql:"Sugar"`
	// 限制性营养素 脂肪
	Fat *float64 `sql:"Fat"`
	// 推荐营养素 蛋白质
	Protein *float64 `sql:"Protein"`
	// 推荐营养素 蛋白质
	DietaryFiber *float64 `sql:"dietaryFiber"`
	// 推荐营养素 钙
	Ca *float64 `sql:"Ca"`
	// 推荐营养素 钙
	K *float64 `sql:"K"`
	// 推荐营养素 镁
	Mg *float64 `sql:"Mg"`
	// 推荐营养素 铁
	Fe *float64 `sql:"Fe"`
	// 推荐营养素 维生素A
	Va *float64 `sql:"Va"`
	// 推荐营养素 维生素B1
	Vb1 *float64 `sql:"Vb1"`
	// 推荐营养素 维生素B2
	Vb2 *float64 `sql:"Vb2"`
	// 推荐营养素 维生素C
	Vc *float64 `sql:"Vc"`
	// 推荐营养素 维生素D
	Vd *float64 `sql:"Vd"`
	// 推荐营养素 锌
	Zn *float64 `sql:"Zn"`
	// 其他营养素 胆固醇
	Cholesterol *float64 `sql:"Cholesterol"`
	// 其他营养素 叶酸
	FolicAcid *float64 `sql:"FolicAcid"`
	// 其他营养素 碘
	I *float64 `sql:"I"`
	// 其他营养素 硒
	Se *float64 `sql:"Se"`
	// 其他营养素 维生素B6
	Bb6 *float64 `sql:"Bb6"`
	// 其他营养素 维生素E
	Ve *float64 `sql:"Ve"`
	// 用户ID
	PatientID        uint64     `sql:"PatientID"`
	AnalysisDateTime *time.Time `sql:"AnalysisDateTime"`
	Energy           float64    `sql:"Energy"`
	Carbohydrate     *float64   `sql:"Carbohydrate"`
	DietPlan         *string    `sql:"DietPlan"`
}

func (s *NutritionalAnalysisRecord) CopyTo(target *doctor.NutritionalAnalysisRecord) {
	if target == nil {
		return
	}
	target.SerialNo = s.SerialNo
	target.Conclusion = s.Conclusion
	target.Grade = s.Grade
	target.GradeLevel = s.GradeLevel
	if s.FoodKinds != nil {
		target.FoodKinds = string(*s.FoodKinds)
	}
	target.Na = s.Na
	target.Sugar = s.Sugar
	target.Fat = s.Fat
	target.Protein = s.Protein
	target.DietaryFiber = s.DietaryFiber
	target.Ca = s.Ca
	target.K = s.K
	target.Mg = s.Mg
	target.Fe = s.Fe
	target.Va = s.Va
	target.Vb1 = s.Vb1
	target.Vb2 = s.Vb2
	target.Vc = s.Vc
	target.Vd = s.Vd
	target.Zn = s.Zn
	target.Cholesterol = s.Cholesterol
	target.FolicAcid = s.FolicAcid
	target.I = s.I
	target.Se = s.Se
	target.Bb6 = s.Bb6
	target.Ve = s.Ve
	target.PatientID = s.PatientID
	target.Energy = s.Energy
	target.Carbohydrate = s.Carbohydrate
	if s.AnalysisDateTime != nil {
		time1 := types.Time(*s.AnalysisDateTime)
		target.AnalysisDateTime = &time1
	}

	if s.DietPlan != nil {
		record := *s.DietPlan
		if len(record) > 0 {
			json.Unmarshal([]byte(record), &target.DietPlan)
		}
	}
}

func (s *NutritionalAnalysisRecord) CopyFrom(source *doctor.NutritionalAnalysisRecord) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.Conclusion = source.Conclusion
	s.Grade = source.Grade
	s.GradeLevel = source.GradeLevel
	foodKinds := string(source.FoodKinds)
	s.FoodKinds = &foodKinds
	s.Na = source.Na
	s.Sugar = source.Sugar
	s.Fat = source.Fat
	s.Protein = source.Protein
	s.DietaryFiber = source.DietaryFiber
	s.Ca = source.Ca
	s.K = source.K
	s.Mg = source.Mg
	s.Fe = source.Fe
	s.Va = source.Va
	s.Vb1 = source.Vb1
	s.Vb2 = source.Vb2
	s.Vc = source.Vc
	s.Vd = source.Vd
	s.Zn = source.Zn
	s.Cholesterol = source.Cholesterol
	s.FolicAcid = source.FolicAcid
	s.I = source.I
	s.Se = source.Se
	s.Bb6 = source.Bb6
	s.Ve = source.Ve
	s.PatientID = source.PatientID
	s.Energy = source.Energy
	s.Carbohydrate = source.Carbohydrate
	if source.AnalysisDateTime != nil {
		time1 := time.Time(*source.AnalysisDateTime)
		s.AnalysisDateTime = &time1
	}
	recordData, err := json.Marshal(source.DietPlan)
	if err == nil {
		record := string(recordData)
		s.DietPlan = &record
	}

}
