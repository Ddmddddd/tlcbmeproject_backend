package sqldb

import "tlcbme_project/data/model/doctor"

type KnowledgeInfoBase struct {
}

func (s KnowledgeInfoBase) TableName() string {
	return "tlcheeduknowledge"
}

type KnowledgeInfo struct {
	KnowledgeInfoBase
	Id             uint64 `sql:"id"`
	Title          string `sql:"title"`
	Content        string `sql:"content"`
	ImageURL       string `sql:"imageURL"`
	LoveNum        uint64 `sql:"loveNum"`
	Comments       uint64 `sql:"comments"`
	Type           uint64 `sql:"type"`
	Dayindex       uint64 `sql:"dayindex"`
	ReadAlreadyNum uint64 `sql:"readAlreadyNum"`
}

func (s *KnowledgeInfo) CopyTo(target *doctor.KnowledgeInfo) {
	if target == nil {
		return
	}
	target.Id = s.Id
	target.Title = s.Title
	target.Content = s.Content
	target.ImageURL = s.ImageURL
	target.LoveNum = s.LoveNum
	target.Comments = s.Comments
	target.Type = s.Type
	target.Dayindex = s.Dayindex
	target.ReadAlreadyNum = s.ReadAlreadyNum
}
