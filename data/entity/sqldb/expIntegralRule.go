package sqldb

import "tlcbme_project/data/model/doctor"

type ExpIntegralRuleBase struct {
}

func (s ExpIntegralRuleBase) TableName() string {
	return "ExpIntegralRule"
}

// 积分规则
type ExpIntegralRule struct {
	ExpIntegralRuleBase
	// 序号\n324\nFCM\n主键，自增
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 每日称重打卡：积分加x
	Weigh *uint64 `sql:"Weigh"`
	// 每日三餐拍照打卡：完成第一次用餐打卡积分加x
	FirstThreeMeals *uint64 `sql:"FirstThreeMeals"`
	// 完成第二次用餐打卡积分加x
	SecondThreeMeals *uint64 `sql:"SecondThreeMeals"`
	// 完成第三次用餐打卡积分加x
	ThirdThreeMeals *uint64 `sql:"ThirdThreeMeals"`
	// 每日步数：每日运动步数6000-7999步,积分加x
	FirstSteps *uint64 `sql:"FirstSteps"`
	// 每日步数8000-9999步，积分加x
	SecondSteps *uint64 `sql:"SecondSteps"`
	// 10000步以上，积分加x
	ThirdSteps *uint64 `sql:"ThirdSteps"`
	// 每日运动打卡：运动时长≥20分钟，并且上传照片，积分加x。
	Sport *uint64 `sql:"Sport"`
	// 每日健康教育， 积分加x
	LearnKnw *uint64 `sql:"LearnKnw"`
	//
	OrgCode string `sql:"OrgCode"`
}

func (s *ExpIntegralRule) CopyTo(target *doctor.ExpIntegralRule) {
	if target == nil {
		return
	}
	target.SerialNo = s.SerialNo
	target.Weigh = s.Weigh
	target.FirstThreeMeals = s.FirstThreeMeals
	target.SecondThreeMeals = s.SecondThreeMeals
	target.ThirdThreeMeals = s.ThirdThreeMeals
	target.FirstSteps = s.FirstSteps
	target.SecondSteps = s.SecondSteps
	target.ThirdSteps = s.ThirdSteps
	target.Sport = s.Sport
	target.LearnKnw = s.LearnKnw
	target.OrgCode = s.OrgCode
}

func (s *ExpIntegralRule) CopyFrom(source *doctor.ExpIntegralRule) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.Weigh = source.Weigh
	s.FirstThreeMeals = source.FirstThreeMeals
	s.SecondThreeMeals = source.SecondThreeMeals
	s.ThirdThreeMeals = source.ThirdThreeMeals
	s.FirstSteps = source.FirstSteps
	s.SecondSteps = source.SecondSteps
	s.ThirdSteps = source.ThirdSteps
	s.Sport = source.Sport
	s.LearnKnw = source.LearnKnw
	s.OrgCode = source.OrgCode
}
