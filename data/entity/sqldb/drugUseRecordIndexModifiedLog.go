package sqldb

import (
	"tlcbme_project/data/model/doctor"
	"github.com/ktpswjz/httpserver/types"
	"time"
)

type DrugUseRecordIndexModifiedLogBase struct {
}

func (s DrugUseRecordIndexModifiedLogBase) TableName() string {
	return "DrugUseRecordIndexModifiedLog"
}

// 慢病管理业务
// 患者自我管理数据
// 用药记录索引变更记录
// 注释：该表保存所有患者的用药记录索引变更记录，只要用药物记录索引表有变化都需要往本表写入一条记录。
type DrugUseRecordIndexModifiedLog struct {
	DrugUseRecordIndexModifiedLogBase
	// 序号 主键，自增 例如:324
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 用药记录索引表序号 关联DrugUseRecordIndex表的SerialNo 例如:232442
	DURISerialNo *uint64 `sql:"DURISerialNo"`
	// 用户ID 关联患者用户登录表用户ID 例如:232442
	PatientID uint64 `sql:"PatientID"`
	// 药品名称  例如:氨氯地平片
	DrugName string `sql:"DrugName"`
	// 剂量 例如:5mg
	Dosage *string `sql:"Dosage"`
	// 频次 例如:1次/日
	Freq *string `sql:"Freq"`
	// 修改类型 值为以下三种：开始使用，调整用法，停止使用 例如:开始使用
	ModifyType string `sql:"ModifyType"`
	// 修改时间 例如:2018-08-12 13:22:21
	ModifyDateTime *time.Time `sql:"ModifyDateTime"`
}

func (s *DrugUseRecordIndexModifiedLog) CopyTo(target *doctor.DrugUseRecordIndexModifiedLog) {
	if target == nil {
		return
	}
	target.SerialNo = s.SerialNo
	target.DURISerialNo = s.DURISerialNo
	target.PatientID = s.PatientID
	target.DrugName = s.DrugName
	if s.Dosage != nil {
		target.Dosage = string(*s.Dosage)
	}
	if s.Freq != nil {
		target.Freq = string(*s.Freq)
	}
	target.ModifyType = s.ModifyType
	if s.ModifyDateTime == nil {
		target.ModifyDateTime = nil
	} else {
		modifyDateTime := types.Time(*s.ModifyDateTime)
		target.ModifyDateTime = &modifyDateTime
	}
}

func (s *DrugUseRecordIndexModifiedLog) CopyFrom(source *doctor.DrugUseRecordIndexModifiedLog) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.DURISerialNo = source.DURISerialNo
	s.PatientID = source.PatientID
	s.DrugName = source.DrugName
	dosage := string(source.Dosage)
	s.Dosage = &dosage
	freq := string(source.Freq)
	s.Freq = &freq
	s.ModifyType = source.ModifyType
	if source.ModifyDateTime == nil {
		s.ModifyDateTime = nil
	} else {
		modifyDateTime := time.Time(*source.ModifyDateTime)
		s.ModifyDateTime = &modifyDateTime
	}
}
