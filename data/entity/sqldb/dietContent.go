package sqldb

import "tlcbme_project/data/model/doctor"

type DietContentBase struct {
}

func (s DietContentBase) TableName() string {
	return "DietContent"
}

// 饮食具体内容
type DietContent struct {
	DietContentBase
	//
	SerialNo uint64 `sql:"SerialNo" primary:"true" auto:"true"`
	// 食物在营养数据库的id
	NutritionSerialNo uint64 `sql:"NutritionSerialNo"`
	// 食物来源
	NutritionSource string `sql:"NutritionSource"`
	// 食物名称
	FoodName string `sql:"FoodName"`
	// 度量值 按标准度量单位计算
	MeasureValue float64 `sql:"MeasureValue"`
	// 用餐记录关联id
	DietRecordSerialNo uint64 `sql:"DietRecordSerialNo"`
	// 食物种类
	FoodTypeId string `sql:"FoodTypeID"`
	// 用户ID
	PatientID uint64 `sql:"PatientID"`

	Memo *string `sql:"Memo"`
}

func (s *DietContent) CopyTo(target *doctor.DietContent) {
	if target == nil {
		return
	}
	target.SerialNo = s.SerialNo
	target.NutritionSerialNo = s.NutritionSerialNo
	target.FoodName = s.FoodName
	target.MeasureValue = s.MeasureValue
	target.DietRecordSerialNo = s.DietRecordSerialNo
	target.FoodTypeId = s.FoodTypeId
	target.PatientID = s.PatientID
	target.NutritionSource = s.NutritionSource
	if s.Memo != nil {
		target.Memo = *s.Memo
	}

}

func (s *DietContent) CopyFrom(source *doctor.DietContent) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.NutritionSerialNo = source.NutritionSerialNo
	s.FoodName = source.FoodName
	s.MeasureValue = source.MeasureValue
	s.DietRecordSerialNo = source.DietRecordSerialNo
	s.FoodTypeId = source.FoodTypeId
	s.PatientID = source.PatientID
	s.NutritionSource = source.NutritionSource
	s.Memo = &source.Memo
}
