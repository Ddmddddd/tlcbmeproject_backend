package sqldb

import (
	"github.com/ktpswjz/httpserver/types"
	"time"
	doctor2 "tlcbme_project/data/model/doctor"
)

type ExpPatientDietPlanBase struct {
}

func (s ExpPatientDietPlanBase) TableName() string {
	return "ExpPatientDietPlan"
}

type ExpPatientDietPlan struct {
	ExpPatientDietPlanBase
	//
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 患者id
	PatientID uint64 `sql:"PatientID"`
	// 饮食计划
	DietPlan string `sql:"DietPlan"`
	// 图片
	PicUrl string `sql:"PicUrl"`
	// 开始时间
	StartDate *time.Time `sql:"StartDate"`
	// 结束时间
	EndDate *time.Time `sql:"EndDate"`
	// 重要程度
	Level string `sql:"Level"`
	// 是否完成，0-未完成，1-已完成
	Complete uint64 `sql:"Complete"`
	// 改变状态，0-正常，1-被修改，需要患者查看，给予提示
	ChangeStatus uint64 `sql:"ChangeStatus"`
	// 创建时间
	CreateDateTime *time.Time `sql:"CreateDateTime"`
	// 更新时间
	UpdateDateTime *time.Time `sql:"UpdateDateTime"`
}

func (s *ExpPatientDietPlan) CopyTo(target *doctor2.ExpPatientDietPlan) {
	if target == nil {
		return
	}
	target.SerialNo = s.SerialNo
	target.PatientID = s.PatientID
	target.DietPlan = s.DietPlan
	target.PicUrl = s.PicUrl
	if s.StartDate == nil {
		target.StartDate = nil
	} else {
		startDate := types.Time(*s.StartDate)
		target.StartDate = &startDate
	}
	if s.EndDate == nil {
		target.EndDate = nil
	} else {
		endDate := types.Time(*s.EndDate)
		target.EndDate = &endDate
	}
	target.Level = s.Level
	target.Complete = s.Complete
	target.ChangeStatus = s.ChangeStatus
	if s.CreateDateTime == nil {
		target.CreateDateTime = nil
	} else {
		createDateTime := types.Time(*s.CreateDateTime)
		target.CreateDateTime = &createDateTime
	}
	if s.UpdateDateTime == nil {
		target.UpdateDateTime = nil
	} else {
		updateDateTime := types.Time(*s.UpdateDateTime)
		target.UpdateDateTime = &updateDateTime
	}
}

func (s *ExpPatientDietPlan) CopyFrom(source *doctor2.ExpPatientDietPlan) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.PatientID = source.PatientID
	s.DietPlan = source.DietPlan
	s.PicUrl = source.PicUrl
	if source.StartDate == nil {
		s.StartDate = nil
	} else {
		startDate := time.Time(*source.StartDate)
		s.StartDate = &startDate
	}
	if source.EndDate == nil {
		s.EndDate = nil
	} else {
		endDate := time.Time(*source.EndDate)
		s.EndDate = &endDate
	}
	s.Level = source.Level
	s.Complete = source.Complete
	s.ChangeStatus = source.ChangeStatus
	if source.CreateDateTime == nil {
		s.CreateDateTime = nil
	} else {
		createDateTime := time.Time(*source.CreateDateTime)
		s.CreateDateTime = &createDateTime
	}
	if source.UpdateDateTime == nil {
		s.UpdateDateTime = nil
	} else {
		updateDateTime := time.Time(*source.UpdateDateTime)
		s.UpdateDateTime = &updateDateTime
	}
}

func (s *ExpPatientDietPlan) CopyFromEdit(source *doctor2.ExpPatientDietPlan) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.PatientID = source.PatientID
	s.DietPlan = source.DietPlan
	if source.StartDate == nil {
		s.StartDate = nil
	} else {
		startDate := time.Time(*source.StartDate)
		s.StartDate = &startDate
	}
	if source.EndDate == nil {
		s.EndDate = nil
	} else {
		endDate := time.Time(*source.EndDate)
		s.EndDate = &endDate
	}
	s.Level = source.Level
	s.Complete = source.Complete
	s.ChangeStatus = 1
	now := time.Now()
	s.UpdateDateTime = &now
}

func (s *ExpPatientDietPlan) CopyFromEditAndDelay(source *doctor2.ExpPatientDietPlan) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.PatientID = source.PatientID
	s.DietPlan = source.DietPlan
	if source.StartDate == nil {
		s.StartDate = nil
	} else {
		startDate := time.Time(*source.StartDate).AddDate(0,0,7)
		s.StartDate = &startDate
	}
	if source.EndDate == nil {
		s.EndDate = nil
	} else {
		endDate := time.Time(*source.EndDate).AddDate(0,0,7)
		s.EndDate = &endDate
	}
	s.Level = source.Level
	s.Complete = source.Complete
	s.ChangeStatus = 1
	now := time.Now()
	s.UpdateDateTime = &now
}