package sqldb

import "tlcbme_project/data/model/doctor"

type MonthlyWeightInfoBase struct {
}

func (s MonthlyWeightInfoBase) TableName() string {
	return "MonthlyWeightInfo"
}

// 慢病管理业务
// 患者APP月报数据
// 月体重统计信息表
// 注释：本表保存患者的月体重统计记录。
type MonthlyWeightInfo struct {
	MonthlyWeightInfoBase
	// 序号 主键，自增 例如:324
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 用户ID 关联患者用户登录表用户ID 例如:232442
	PatientID uint64 `sql:"PatientID"`
	// 月份 年份+月份（2016-09） 例如:2016-09
	YearMonth string `sql:"YearMonth"`
	// 当前身高 当月的身高 例如:170
	CurrentHeight *uint64 `sql:"CurrentHeight"`
	// 当前体重 当月最后一次体重 例如:60.5
	CurrentWeight float64 `sql:"CurrentWeight"`
	// 目标体重 当月目标的体重 例如:65
	TargetWeight float64 `sql:"TargetWeight"`
	// 当前BMI 当月最后一次体重对应的BMI值 例如:22.6
	CurrentBMI float64 `sql:"CurrentBMI"`
}

func (s *MonthlyWeightInfo) CopyTo(target *doctor.MonthlyWeightInfo) {
	if target == nil {
		return
	}
	target.SerialNo = s.SerialNo
	target.PatientID = s.PatientID
	target.YearMonth = s.YearMonth
	target.CurrentHeight = s.CurrentHeight
	target.CurrentWeight = s.CurrentWeight
	target.TargetWeight = s.TargetWeight
	target.CurrentBMI = s.CurrentBMI
}

func (s *MonthlyWeightInfo) CopyFrom(source *doctor.MonthlyWeightInfo) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.PatientID = source.PatientID
	s.YearMonth = source.YearMonth
	s.CurrentHeight = source.CurrentHeight
	s.CurrentWeight = source.CurrentWeight
	s.TargetWeight = source.TargetWeight
	s.CurrentBMI = source.CurrentBMI
}
