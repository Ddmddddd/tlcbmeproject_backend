package sqldb

import (
	"tlcbme_project/data/enum"
	"tlcbme_project/data/model/doctor"
	"strings"
	"time"
)

func (s *ViewPatient) CopyToEx(target *doctor.ViewPatientEx, bmi *float64) {
	if target == nil {
		return
	}
	s.CopyTo(&target.ViewPatient)

	if s.Sex != nil {
		target.SexText = enum.Sexes.Value(*s.Sex)
	}
	if s.DateOfBirth != nil {
		age := time.Now().Sub(*s.DateOfBirth)
		target.Age = int64(age.Hours() / (24 * 365))
	}

	tagCount := 0
	tag := &strings.Builder{}
	if s.ManageClass != nil {
		vs := strings.Split(*s.ManageClass, ",")
		for _, v := range vs {
			tv := strings.TrimSpace(v)
			if len(tv) < 1 {
				continue
			}
			tagCount++
			if tagCount > 1 {
				tag.WriteString(" ")
			}
			tag.WriteString(tv)
		}
	}
	if s.PatientFeature != nil {
		vs := strings.Split(*s.PatientFeature, ",")
		for _, v := range vs {
			tv := strings.TrimSpace(v)
			if len(tv) < 1 {
				continue
			}
			tagCount++
			if tagCount > 1 {
				tag.WriteString(" ")
			}
			tag.WriteString(tv)
		}
	}

	if target.Age >= 45 && target.Age < 60 {
		tag.WriteString(" 中年人")
	} else if target.Age >= 60 {
		tag.WriteString(" 老年人")
	}

	if bmi != nil {
		bmi := *bmi
		if bmi >= 24 && bmi < 27 {
			tag.WriteString(" 超重")
		} else if bmi >= 27 && bmi < 30 {
			tag.WriteString(" 肥胖")
		} else if bmi >= 30 {
			tag.WriteString(" 重度肥胖")
		}
	}

	target.Tag = tag.String()
}

type ViewPatientFilter struct {
	ViewPatientBase

	// 用户ID 关联医生用户登录表用户ID 例如:232442
	PatientID *uint64 `sql:"PatientID"`
	// 身份证号 例如:330106200012129876
	IdentityCardNumber string `sql:"IdentityCardNumber"`
}

func (s *ViewPatientFilter) CopyFrom(source *doctor.ViewPatientFilter) {
	if source == nil {
		return
	}
	s.PatientID = source.PatientID
	s.IdentityCardNumber = source.IdentityCardNumber
}
