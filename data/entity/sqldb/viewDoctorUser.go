package sqldb

import "tlcbme_project/data/model/doctor"

type ViewDoctorUserBase struct {
}

func (s ViewDoctorUserBase) TableName() string {
	return "ViewDoctorUser"
}

// VIEW
type ViewDoctorUser struct {
	ViewDoctorUserBase
	// 用户ID 主键，自增 例如:
	UserID uint64 `sql:"UserID"`
	// 用户名 如果注册时没有指定用户名，则系统默认分配一个用户名，格式为cdmwb+userID，例如cdmwb123 例如:zhangsan
	UserName string `sql:"UserName"`
	// 手机号 绑定的手机号 例如:13818765432
	MobilePhone *string `sql:"MobilePhone"`
	// 电子邮箱 绑定的电子邮箱地址 例如:tlcbme_project@vico-lab.com
	Email *string `sql:"Email"`
	// 用户状态 0-正常，1-冻结，9-已注销 例如:0
	Status uint64 `sql:"Status"`
	// 登录次数 累计登录次数 例如:12
	LoginCount uint64 `sql:"LoginCount"`
	// 姓名  例如:张三
	Name *string `sql:"Name"`
	// 性别 0-未知, 1-男, 2-女，9-未说明 例如:1
	Sex *uint64 `sql:"Sex"`
	// 联系电话 例如:13812344321
	Phone *string `sql:"Phone"`
	// 头像照片 例如:
	Photo []byte `sql:"Photo"`
	// 执业机构代码 该医生用户所在的执业机构 例如:897798
	OrgCode *string `sql:"OrgCode"`
	// 用户类别 0-医生，1-健康管理师，11-医生兼健康管理师 例如:0
	UserType *uint64 `sql:"UserType"`
	// 是否验证通过 0-未认证，1-已认证，2-认证不通过 例如:0
	VerifiedStatus *uint64 `sql:"VerifiedStatus"`
	// 机构名称  例如:第一人民医院
	OrgName *string `sql:"OrgName"`
}

func (s *ViewDoctorUser) CopyTo(target *doctor.ViewDoctorUser) {
	if target == nil {
		return
	}
	target.UserID = s.UserID
	target.UserName = s.UserName
	if s.MobilePhone != nil {
		target.MobilePhone = string(*s.MobilePhone)
	}
	if s.Email != nil {
		target.Email = string(*s.Email)
	}
	target.Status = s.Status
	target.LoginCount = s.LoginCount
	target.Sex = s.Sex
	if s.Name != nil {
		target.Name = string(*s.Name)
	}
	if s.Phone != nil {
		target.Phone = string(*s.Phone)
	}
	target.Photo = string(s.Photo)
	if s.OrgCode != nil {
		target.OrgCode = string(*s.OrgCode)
	}
	target.UserType = s.UserType
	target.VerifiedStatus = s.VerifiedStatus
	if s.OrgName != nil {
		target.OrgName = string(*s.OrgName)
	}
}

func (s *ViewDoctorUser) CopyFrom(source *doctor.ViewDoctorUser) {
	if source == nil {
		return
	}
	s.UserID = source.UserID
	s.UserName = source.UserName
	mobilePhone := string(source.MobilePhone)
	s.MobilePhone = &mobilePhone
	email := string(source.Email)
	s.Email = &email
	s.Status = source.Status
	s.LoginCount = source.LoginCount
	name := string(source.Name)
	s.Name = &name
	s.Sex = source.Sex
	phone := string(source.Phone)
	s.Phone = &phone
	s.Photo = []byte(source.Photo)
	orgCode := string(source.OrgCode)
	s.OrgCode = &orgCode
	s.UserType = source.UserType
	s.VerifiedStatus = source.VerifiedStatus
	orgName := string(source.OrgName)
	s.OrgName = &orgName
}
