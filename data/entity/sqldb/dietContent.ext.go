package sqldb

import "tlcbme_project/data/model/doctor"

type DietContentFilter struct {
	DietContentBase
	DietRecordSerialNo uint64 `sql:"DietRecordSerialNo"`
}

func (s *DietContentFilter) CopyFrom(source *doctor.DietContentFilter) {
	s.DietRecordSerialNo = source.DietRecordSerialNo
}
