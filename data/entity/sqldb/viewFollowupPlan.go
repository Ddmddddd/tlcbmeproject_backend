package sqldb

import (
	"tlcbme_project/data/model/doctor"
	"github.com/ktpswjz/httpserver/types"
	"time"
)

type ViewFollowupPlanBase struct {
}

func (s ViewFollowupPlanBase) TableName() string {
	return "ViewFollowupPlan"
}

// VIEW
type ViewFollowupPlan struct {
	ViewFollowupPlanBase
	// 序号 主键，自增 例如:324
	SerialNo uint64 `sql:"SerialNo"`
	// 用户ID 关联患者用户登录表用户ID 例如:232442
	PatientID uint64 `sql:"PatientID"`
	// 姓名  例如:张三
	PatientName *string `sql:"PatientName"`
	// 性别 0-未知, 1-男, 2-女，9-未说明 例如:1
	Sex *uint64 `sql:"Sex"`
	// 出生日期 例如:2000-12-12
	DateOfBirth *time.Time `sql:"DateOfBirth"`
	// 身份证号 例如:330106200012129876
	IdentityCardNumber *string `sql:"IdentityCardNumber"`
	// 联系电话 例如:13812344321
	Phone *string `sql:"Phone"`
	// 文化程度 GB/T 4658-1984 文化程度 例如:大学
	EducationLevel *string `sql:"EducationLevel"`
	// 职业类别 GB/T 6565-1999 职业分类 例如:
	JobType *string `sql:"JobType"`
	// 身高 单位：cm 例如:165
	Height *uint64 `sql:"Height"`
	// 累计随访次数 本次管理的累计随访次数 例如:3
	FollowupTimes *uint64 `sql:"FollowupTimes"`
	// 上次随访日期 例如:2018-06-15
	LastFollowupDate *time.Time `sql:"LastFollowupDate"`
	// 计划随访日期 例如:2018-07-15
	FollowupDate *time.Time `sql:"FollowupDate"`
	// 随访类型 随访的类型（原因） 例如:三个月例行随访
	FollowUpType string `sql:"FollowUpType"`
	// 随访状态 0-待随访，1-已随访，2-已忽略，3-已过期，9-已作废。 例如:0
	FollowStatus uint64 `sql:"FollowStatus"`
	// 创建时间 随访计划的创建时间 例如:2018-07-15 09:33:22
	CreateDateTime *time.Time `sql:"CreateDateTime"`
	// 创建者姓名 随访计划创建者的姓名 例如:张三
	CreatorName string `sql:"CreatorName"`
	// 最后更新日期 例如:2018-08-15
	UpdateTime *time.Time `sql:"UpdateTime"`
	// 患者特点 如有多个，之间用逗号分隔，例如：老年人，肥胖，残疾人 例如:老年人，肥胖
	PatientFeature *string `sql:"PatientFeature"`
	// 管理状态 0-管理中，1-迁出，2-已终止管理 例如:0
	ManageStatus *uint64 `sql:"ManageStatus"`
	// 管理分类 如有多个，之间用逗号分隔，例如：高血压，糖尿病 例如:高血压，糖尿病
	ManageClass *string `sql:"ManageClass"`
	//
	ManageLevel []byte `sql:"ManageLevel"`
	//
	DmManageLevel []byte `sql:"DmManageLevel"`
	//
	ManageLevelStartDateTime []byte `sql:"manageLevelStartDateTime"`
	//
	DmManageLevelStartDateTime []byte `sql:"dmManageLevelStartDateTime"`
	// 管理机构代码 管理该患者的机构 例如:897798
	OrgCode *string `sql:"OrgCode"`
	// 医生ID 例如:232
	DoctorID *uint64 `sql:"DoctorID"`
	// 医生姓名 例如:张三
	DoctorName *string `sql:"DoctorName"`
	// 健康管理师ID 例如:232
	HealthManagerID *uint64 `sql:"HealthManagerID"`
	// 健康管理师姓名 例如:李四
	HealthManagerName *string `sql:"HealthManagerName"`
	// 管理开始时间 最近一次管理的开始时间，如果是刚注册，则默认为审核通过时间 例如:2018-07-03 13:15:00
	ManageStartDateTime *time.Time `sql:"ManageStartDateTime"`
	// 依从度 值为0至5，0表示依从度未知 例如:4
	ComplianceRate *uint64 `sql:"ComplianceRate"`
	// 管理终止时间 该字段只有当ManageStatus字段为2时才有意义 例如:2018-08-03 13:15:00
	ManageEndDateTime *time.Time `sql:"ManageEndDateTime"`
	// 管理终止的原因 该字段只有当ManageStatus字段为2时才有意义 例如:亡故（2018-08-03，车祸死亡）
	ManageEndReason *string `sql:"ManageEndReason"`
}

func (s *ViewFollowupPlan) CopyTo(target *doctor.ViewFollowupPlan) {
	if target == nil {
		return
	}
	target.SerialNo = s.SerialNo
	target.PatientID = s.PatientID
	if s.PatientName != nil {
		target.PatientName = string(*s.PatientName)
	}
	target.Sex = s.Sex
	if s.DateOfBirth == nil {
		target.DateOfBirth = nil
	} else {
		dateOfBirth := types.Time(*s.DateOfBirth)
		target.DateOfBirth = &dateOfBirth
	}
	if s.IdentityCardNumber != nil {
		target.IdentityCardNumber = string(*s.IdentityCardNumber)
	}
	if s.Phone != nil {
		target.Phone = string(*s.Phone)
	}
	if s.EducationLevel != nil {
		target.EducationLevel = string(*s.EducationLevel)
	}
	if s.JobType != nil {
		target.JobType = string(*s.JobType)
	}
	target.Height = s.Height
	target.FollowupTimes = s.FollowupTimes
	if s.LastFollowupDate == nil {
		target.LastFollowupDate = nil
	} else {
		lastFollowupDate := types.Time(*s.LastFollowupDate)
		target.LastFollowupDate = &lastFollowupDate
	}
	if s.FollowupDate == nil {
		target.FollowupDate = nil
	} else {
		followupDate := types.Time(*s.FollowupDate)
		target.FollowupDate = &followupDate
	}
	target.FollowUpType = s.FollowUpType
	target.FollowStatus = s.FollowStatus
	if s.CreateDateTime == nil {
		target.CreateDateTime = nil
	} else {
		createDateTime := types.Time(*s.CreateDateTime)
		target.CreateDateTime = &createDateTime
	}
	target.CreatorName = s.CreatorName
	if s.UpdateTime == nil {
		target.UpdateTime = nil
	} else {
		updateTime := types.Time(*s.UpdateTime)
		target.UpdateTime = &updateTime
	}
	if s.PatientFeature != nil {
		target.PatientFeature = string(*s.PatientFeature)
	}
	target.ManageStatus = s.ManageStatus
	if s.ManageClass != nil {
		target.ManageClass = string(*s.ManageClass)
	}
	target.ManageLevel = string(s.ManageLevel)
	target.DmManageLevel = string(s.DmManageLevel)
	target.ManageLevelStartDateTime = string(s.ManageLevelStartDateTime)
	target.DmManageLevelStartDateTime = string(s.DmManageLevelStartDateTime)
	if s.OrgCode != nil {
		target.OrgCode = string(*s.OrgCode)
	}
	target.DoctorID = s.DoctorID
	if s.DoctorName != nil {
		target.DoctorName = string(*s.DoctorName)
	}
	target.HealthManagerID = s.HealthManagerID
	if s.HealthManagerName != nil {
		target.HealthManagerName = string(*s.HealthManagerName)
	}
	if s.ManageStartDateTime == nil {
		target.ManageStartDateTime = nil
	} else {
		manageStartDateTime := types.Time(*s.ManageStartDateTime)
		target.ManageStartDateTime = &manageStartDateTime
	}
	target.ComplianceRate = s.ComplianceRate
	if s.ManageEndDateTime == nil {
		target.ManageEndDateTime = nil
	} else {
		manageEndDateTime := types.Time(*s.ManageEndDateTime)
		target.ManageEndDateTime = &manageEndDateTime
	}
	if s.ManageEndReason != nil {
		target.ManageEndReason = string(*s.ManageEndReason)
	}
}

func (s *ViewFollowupPlan) CopyFrom(source *doctor.ViewFollowupPlan) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.PatientID = source.PatientID
	patientName := string(source.PatientName)
	s.PatientName = &patientName
	s.Sex = source.Sex
	if source.DateOfBirth == nil {
		s.DateOfBirth = nil
	} else {
		dateOfBirth := time.Time(*source.DateOfBirth)
		s.DateOfBirth = &dateOfBirth
	}
	identityCardNumber := string(source.IdentityCardNumber)
	s.IdentityCardNumber = &identityCardNumber
	phone := string(source.Phone)
	s.Phone = &phone
	educationLevel := string(source.EducationLevel)
	s.EducationLevel = &educationLevel
	jobType := string(source.JobType)
	s.JobType = &jobType
	s.Height = source.Height
	s.FollowupTimes = source.FollowupTimes
	if source.LastFollowupDate == nil {
		s.LastFollowupDate = nil
	} else {
		lastFollowupDate := time.Time(*source.LastFollowupDate)
		s.LastFollowupDate = &lastFollowupDate
	}
	if source.FollowupDate == nil {
		s.FollowupDate = nil
	} else {
		followupDate := time.Time(*source.FollowupDate)
		s.FollowupDate = &followupDate
	}
	s.FollowUpType = source.FollowUpType
	s.FollowStatus = source.FollowStatus
	if source.CreateDateTime == nil {
		s.CreateDateTime = nil
	} else {
		createDateTime := time.Time(*source.CreateDateTime)
		s.CreateDateTime = &createDateTime
	}
	s.CreatorName = source.CreatorName
	if source.UpdateTime == nil {
		s.UpdateTime = nil
	} else {
		updateTime := time.Time(*source.UpdateTime)
		s.UpdateTime = &updateTime
	}
	patientFeature := string(source.PatientFeature)
	s.PatientFeature = &patientFeature
	s.ManageStatus = source.ManageStatus
	manageClass := string(source.ManageClass)
	s.ManageClass = &manageClass
	s.ManageLevel = []byte(source.ManageLevel)
	s.DmManageLevel = []byte(source.DmManageLevel)
	s.ManageLevelStartDateTime = []byte(source.ManageLevelStartDateTime)
	s.DmManageLevelStartDateTime = []byte(source.DmManageLevelStartDateTime)
	orgCode := string(source.OrgCode)
	s.OrgCode = &orgCode
	s.DoctorID = source.DoctorID
	doctorName := string(source.DoctorName)
	s.DoctorName = &doctorName
	s.HealthManagerID = source.HealthManagerID
	healthManagerName := string(source.HealthManagerName)
	s.HealthManagerName = &healthManagerName
	if source.ManageStartDateTime == nil {
		s.ManageStartDateTime = nil
	} else {
		manageStartDateTime := time.Time(*source.ManageStartDateTime)
		s.ManageStartDateTime = &manageStartDateTime
	}
	s.ComplianceRate = source.ComplianceRate
	if source.ManageEndDateTime == nil {
		s.ManageEndDateTime = nil
	} else {
		manageEndDateTime := time.Time(*source.ManageEndDateTime)
		s.ManageEndDateTime = &manageEndDateTime
	}
	manageEndReason := string(source.ManageEndReason)
	s.ManageEndReason = &manageEndReason
}
