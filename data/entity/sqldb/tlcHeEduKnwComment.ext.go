package sqldb

import "tlcbme_project/data/model/doctor"

type PatientCommentKnw struct {
	TlcHeEduKnwCommentBase
	Knowledge_id uint64 `sql:"knowledge_id"`
	UserID       uint64 `sql:"UserID"`
	Content      string `sql:"content"`
}

func (s *PatientCommentKnw) CopyFrom(source *doctor.TlcPatientSendKnwComment) {
	if source == nil {
		return
	}
	s.Knowledge_id = source.Knowledge_id
	s.UserID = source.PateintID
	s.Content = source.Content
}

type TlcHeEduKnwCommentFilterByKnwID struct {
	TlcHeEduKnwCommentBase
	Knowledge_id uint64 `sql:"knowledge_id"`
}
