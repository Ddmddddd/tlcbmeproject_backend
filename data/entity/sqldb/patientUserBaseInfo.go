package sqldb

import (
	"github.com/ktpswjz/httpserver/types"
	"time"
	"tlcbme_project/data/model/doctor"
)

type PatientUserBaseInfoBase struct {
}

func (s PatientUserBaseInfoBase) TableName() string {
	return "PatientUserBaseInfo"
}

// 平台运维
// 平台用户管理
// 患者用户基本信息表
// 注释：该表保存患者用户基本资料，每位患者用户对应该表中的一条记录。
type PatientUserBaseInfo struct {
	PatientUserBaseInfoBase
	// 序号 主键，自增 例如:324
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 用户ID 关联医生用户登录表用户ID 例如:232442
	UserID uint64 `sql:"UserID"`
	// 姓名  例如:张三
	Name string `sql:"Name"`
	// 性别 0-未知, 1-男, 2-女，9-未说明 例如:1
	Sex *uint64 `sql:"Sex"`
	// 出生日期 例如:2000-12-12
	DateOfBirth *time.Time `sql:"DateOfBirth"`
	// 身份证号 例如:330106200012129876
	IdentityCardNumber *string `sql:"IdentityCardNumber"`
	// 国籍 GB/T 2659-2000 世界各国和地区名称 例如:中国
	Country *string `sql:"Country"`
	// 民族 GB 3304-1991 中国各民族名称 例如:汉族
	Nation *string `sql:"Nation"`
	// 籍贯  例如:浙江杭州
	NativePlace *string `sql:"NativePlace"`
	// 婚姻状况 GB/T 2261.2-2003 例如:
	MarriageStatus *string `sql:"MarriageStatus"`
	// 文化程度 GB/T 4658-1984 文化程度 例如:大学
	EducationLevel *string `sql:"EducationLevel"`
	// 职业类别 GB/T 6565-1999 职业分类 例如:
	JobType *string `sql:"JobType"`
	// 头像照片 例如:
	Photo []byte `sql:"Photo"`
	// 昵称 例如:蓝精灵
	Nickname *string `sql:"Nickname"`
	// 个性签名 例如:在那山的那边海的那边有一群蓝精灵
	PersonalSign *string `sql:"PersonalSign"`
	// 联系电话 例如:13812344321
	Phone *string `sql:"Phone"`
	// 身高 单位：cm 例如:165
	Height *uint64 `sql:"Height"`
	// 体重 最后一次录入的体重，单位：kg 例如:60.5
	Weight *float64 `sql:"Weight"`
	// 腰围 最后一次录入的腰围，单位：cm 例如:60.5
	Waistline *float64 `sql:"Waistline"`
}

func (s *PatientUserBaseInfo) CopyTo(target *doctor.PatientUserBaseInfo) {
	if target == nil {
		return
	}
	target.SerialNo = s.SerialNo
	target.UserID = s.UserID
	target.Name = s.Name
	target.Sex = s.Sex
	if s.DateOfBirth == nil {
		target.DateOfBirth = nil
	} else {
		dateOfBirth := types.Time(*s.DateOfBirth)
		target.DateOfBirth = &dateOfBirth
	}
	if s.IdentityCardNumber != nil {
		target.IdentityCardNumber = string(*s.IdentityCardNumber)
	}
	if s.Country != nil {
		target.Country = string(*s.Country)
	}
	if s.Nation != nil {
		target.Nation = string(*s.Nation)
	}
	if s.NativePlace != nil {
		target.NativePlace = string(*s.NativePlace)
	}
	if s.MarriageStatus != nil {
		target.MarriageStatus = string(*s.MarriageStatus)
	}
	if s.EducationLevel != nil {
		target.EducationLevel = string(*s.EducationLevel)
	}
	if s.JobType != nil {
		target.JobType = string(*s.JobType)
	}
	target.Photo = string(s.Photo)
	if s.Nickname != nil {
		target.Nickname = string(*s.Nickname)
	}
	if s.PersonalSign != nil {
		target.PersonalSign = string(*s.PersonalSign)
	}
	if s.Phone != nil {
		target.Phone = string(*s.Phone)
	}
	target.Height = s.Height
	target.Weight = s.Weight
	target.Waistline = s.Waistline
}

func (s *PatientUserBaseInfo) CopyFrom(source *doctor.PatientUserBaseInfo) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.UserID = source.UserID
	s.Name = source.Name
	s.Sex = source.Sex
	if source.DateOfBirth == nil {
		s.DateOfBirth = nil
	} else {
		dateOfBirth := time.Time(*source.DateOfBirth)
		s.DateOfBirth = &dateOfBirth
	}
	identityCardNumber := string(source.IdentityCardNumber)
	s.IdentityCardNumber = &identityCardNumber
	country := string(source.Country)
	s.Country = &country
	nation := string(source.Nation)
	s.Nation = &nation
	nativePlace := string(source.NativePlace)
	s.NativePlace = &nativePlace
	marriageStatus := string(source.MarriageStatus)
	s.MarriageStatus = &marriageStatus
	educationLevel := string(source.EducationLevel)
	s.EducationLevel = &educationLevel
	jobType := string(source.JobType)
	s.JobType = &jobType
	s.Photo = []byte(source.Photo)
	nickname := string(source.Nickname)
	s.Nickname = &nickname
	personalSign := string(source.PersonalSign)
	s.PersonalSign = &personalSign
	phone := string(source.Phone)
	s.Phone = &phone
	s.Height = source.Height
	s.Weight = source.Weight
	s.Waistline = source.Waistline
}

func (s *PatientUserBaseInfo) CopyFromInstrument(source *doctor.PatientUserInfoCreateForApp) {
	if source == nil {
		return
	}
	s.Name = source.Name
	if source.Sex == "男" {
		sex := uint64(1)
		s.Sex = &sex
	} else {
		sex := uint64(2)
		s.Sex = &sex
	}
	if source.DateOfBirth == nil {
		s.DateOfBirth = nil
	} else {
		dateOfBirth := time.Time(*source.DateOfBirth)
		s.DateOfBirth = &dateOfBirth
	}
	identityCardNumber := string(source.IdentityCardNumber)
	s.IdentityCardNumber = &identityCardNumber
}
