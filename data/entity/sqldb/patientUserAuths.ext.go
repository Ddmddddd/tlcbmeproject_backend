package sqldb

import (
	"time"
	"tlcbme_project/data/model/doctor"
)

type PatientUserAuthsFilter struct {
	PatientUserAuthsBase

	// 用户ID 主键，自增 例如:
	UserID *uint64 `sql:"UserID" auto:"true"`
	// 用户名
	UserName string `sql:"UserName"`
	// 手机号 绑定的手机号 例如:13818765432
	MobilePhone string `sql:"MobilePhone"`
	// 电子邮箱 绑定的电子邮箱地址 例如:tlcbme_project@vico-lab.com
	Email string `sql:"Email"`
}

type PatientUserAuthsUserIdFilter struct {
	PatientUserAuthsBase

	// 用户ID  例如:
	UserID uint64 `sql:"UserID" auto:"true"`
}

type PatientUserAuthsRegistDate struct {
	PatientUserAuthsBase

	// 注册时间  例如:2018-07-02 15:00:00
	RegistDateTime *time.Time `sql:"RegistDateTime"`
}

type PatientUserAuthsLoginUpdate struct {
	PatientUserAuthsBase

	// 登录次数 累计登录次数 例如:12
	LoginCount uint64 `sql:"LoginCount"`
	// 最后一次登录时间  例如:2018-07-02 15:10:00
	LastLoginDateTime *time.Time `sql:"LastLoginDateTime"`
}

type PatientUserAuthsPasswordUpdate struct {
	PatientUserAuthsBase

	// 登录密码 加密后存储 例如:291fakfjwi98234fsf23fjw
	Password string `sql:"Password"`
	// 登录密码格式 0-明文，11-MD5，21-SHA1，22-SHA256，23-SHA384，24-SHA512 例如:11
	PasswordFormat uint64 `sql:"PasswordFormat"`
}

type PatientUserAuthsPhoneUpdate struct {
	PatientUserAuthsBase

	// 用户ID  例如:
	UserID uint64 `sql:"UserID" primary:"true"`
	// 手机号 绑定的手机号 例如:13818765432
	MobilePhone *string `sql:"MobilePhone"`
}

type PatientUserAuthsRegistDateFilter struct {
	PatientUserAuthsBase

	// 注册时间  例如:2018-07-02 15:00:00
	StartDateTime *time.Time `sql:"RegistDateTime" filter:">="`
	EndDateTime   *time.Time `sql:"RegistDateTime" filter:"<="`
}

type PatientUserAuthsUserNameFilter struct {
	PatientUserAuthsBase

	// 用户ID  例如:
	UserName string `sql:"UserName"`
}

type PatientUserAuthsUpdateTime struct {
	PatientUserAuthsBase
	// 用户ID 主键，自增 例如:
	UserID uint64 `sql:"UserID" auto:"true" primary:"true"`
	// 累计使用时长 单位：秒 例如:24342
	UseSeconds uint64 `sql:"UseSeconds"`
}

func (s *PatientUserAuthsUpdateTime) CopyFromUpdate(source *doctor.PatientUserAuthsUpdateTime) {
	if source == nil {
		return
	}
	s.UserID = source.UserID
	s.UseSeconds = source.UseSeconds
}

func (s *PatientUserAuthsUpdateTime) CopyToUpdate(source *doctor.PatientUserAuths) {
	if source == nil {
		return
	}
	s.UserID = source.UserID
	s.UseSeconds = source.UseSeconds
}