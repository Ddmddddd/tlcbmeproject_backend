package sqldb

import (
	"tlcbme_project/data/model/doctor"
)

type TagInfoBase struct {
}

func (s TagInfoBase) TableName() string {
	return "taginfo"
}

type TagInfo struct {
	TagInfoBase
	Id  uint64 `sql:"id" auto:"true" primary:"true"`
	Tag string `sql:"tag"`
}

func (s *TagInfo) CopyTo(target *doctor.Tag) {
	if target == nil {
		return
	}
	target.Id = s.Id
	target.Tag = s.Tag
}
