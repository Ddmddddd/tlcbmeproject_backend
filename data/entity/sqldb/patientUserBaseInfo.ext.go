package sqldb

import (
	"github.com/ktpswjz/httpserver/types"
	"time"
	"tlcbme_project/data/model/doctor"
)

type PatientUserBaseInfoFilter struct {
	PatientUserBaseInfoBase

	// 身份证号 例如:330106200012129876
	IdentityCardNumber *string `sql:"IdentityCardNumber"`
}

type PatientUserBaseInfoExcludedIdFilter struct {
	PatientUserBaseInfoBase
	// 用户ID 主键，自增 例如:
	UserID uint64 `sql:"UserID" auto:"true" filter:"!="`
}

type PatientUserBaseInfoAuthenticationFilter struct {
	PatientUserBaseInfoBase

	// 用户名 如果注册时没有指定用户名，则系统默认分配一个用户名，格式为cdmwb+userID，例如cdmwb123 例如:zhangsan
	UserName string `sql:"UserName"`
	// 手机号 绑定的手机号 例如:13818765432
	MobilePhone string `sql:"MobilePhone"`
	// 电子邮箱 绑定的电子邮箱地址 例如:tlcbme_project@vico-lab.com
	Email string `sql:"Email"`
}

type PatientUserBaseInfoUserIdFilter struct {
	PatientUserBaseInfoBase
	// 用户ID  例如:
	UserID uint64 `sql:"UserID"`
}

type PatientUserBaseInfoUpdate struct {
	PatientUserBaseInfoBase
	// 姓名  例如:张三
	Name string `sql:"Name"`
	// 性别 0-未知, 1-男, 2-女，9-未说明 例如:1
	Sex *uint64 `sql:"Sex"`
	// 出生日期 例如:2000-12-12
	DateOfBirth *time.Time `sql:"DateOfBirth"`
	// 职业类别 GB/T 6565-1999 职业分类 例如:
	JobType *string `sql:"JobType"`
	// 昵称 例如:蓝精灵
	Nickname *string `sql:"Nickname"`
	// 身高 单位：cm 例如:165
	Height *uint64 `sql:"Height"`
	// 体重 最后一次录入的体重，单位：kg 例如:60.5
	Weight *float64 `sql:"Weight"`
}

func (s *PatientUserBaseInfo) CopyToApp(target *doctor.PatientUserInfoForApp) {
	if target == nil {
		return
	}
	target.PatientID = s.UserID
	target.Name = s.Name
	sex := "未知"
	if s.Sex != nil {
		nSex := uint64(*s.Sex)
		switch nSex {
		case 1:
			sex = "男"
		case 2:
			sex = "女"
		}
	}
	target.Sex = sex
	if s.DateOfBirth == nil {
		target.DateOfBirth = nil
	} else {
		dateOfBirth := types.Time(*s.DateOfBirth)
		target.DateOfBirth = &dateOfBirth
	}
	if s.IdentityCardNumber != nil {
		target.IdentityCardNumber = string(*s.IdentityCardNumber)
	}
	target.Photo = string(s.Photo)
	if s.Nickname != nil {
		target.Nickname = string(*s.Nickname)
	}
	if s.Phone != nil {
		target.PhoneNumber = string(*s.Phone)
	}
	if s.Height != nil {
		target.NewestHeight = uint64(*s.Height)
	}
	if s.Weight != nil {
		target.NewestWeight = float64(*s.Weight)
	}
	if s.Country != nil {
		target.Country = string(*s.Country)
	}
}

func (s *PatientUserBaseInfo) CopyFromApp(source *doctor.PatientUserInfoCreateForApp) {
	if source == nil {
		return
	}
	s.Name = source.Name
	sex := uint64(0)
	switch source.Sex {
	case "男":
		sex = 1
	case "女":
		sex = 2
	}
	s.Sex = &sex
	if source.DateOfBirth == nil {
		s.DateOfBirth = nil
	} else {
		dateOfBirth := time.Time(*source.DateOfBirth)
		s.DateOfBirth = &dateOfBirth
	}
	//identityCardNumber := string(source.IdentityCardNumber)
	//s.IdentityCardNumber = &identityCardNumber
	educationLevel := string(source.EducationLevel)
	s.EducationLevel = &educationLevel
	jobType := string(source.JobType)
	s.JobType = &jobType
	nickname := string(source.Nickname)
	s.Nickname = &nickname
	phone := string(source.PhoneNumber)
	s.Phone = &phone
	height := uint64(source.Height)
	s.Height = &height
	weight := float64(source.Weight)
	s.Weight = &weight
	country := string(source.Country)
	s.Country = &country
}

type PatientUserBaseInfoUpdateWeight struct {
	PatientUserBaseInfoBase

	// 用户ID 关联患者用户登录表用户ID 例如:232442
	UserID uint64 `sql:"UserID" primary:"true"`
	// 体重 最后一次录入的体重，单位：kg 例如:60.5
	Weight    *float64 `sql:"Weight"`
	Waistline *float64 `sql:"Waistline"`
	// 身高 单位：cm 例如:165
	Height *uint64 `sql:"Height"`
}

type PatientUserBaseInfoBmi struct {
	PatientUserBaseInfoBase

	// 身高 单位：cm 例如:165
	Height *uint64 `sql:"Height"`
	// 体重 最后一次录入的体重，单位：kg 例如:60.5
	Weight *float64 `sql:"Weight"`
}

func (s *PatientUserBaseInfoBmi) GetBmi() *float64 {
	if s.Weight == nil {
		return nil
	}
	if s.Height == nil {
		return nil
	}

	height := float64(*s.Height) / 100
	bmi := *s.Weight / height / height

	return &bmi
}

type PatientUserBaseInfoBmiFilter struct {
	PatientUserBaseInfoBase

	// 用户ID 关联患者用户登录表用户ID 例如:232442
	UserID uint64 `sql:"UserID"`
}

type PatientUserHeightAndWeight struct {
	PatientUserBaseInfoBase

	// 身高 单位：cm 例如:165
	Height *uint64 `sql:"Height"`
	// 体重 最后一次录入的体重，单位：kg 例如:60.5
	Weight *float64 `sql:"Weight"`
}

type PatientUserWaistline struct {
	PatientUserBaseInfoBase

	// 腰围 单位：cm 例如:60.5
	Waistline *float64 `sql:"Waistline"`
}

type PatientUserHeight struct {
	PatientUserBaseInfoBase

	// 身高 单位：cm 例如:165
	Height *uint64 `sql:"Height"`
}

func (s *PatientUserHeightAndWeight) CopyTo(target *doctor.PatientUserHeightAndWeight) {
	if target == nil {
		return
	}

	target.Height = s.Height
	target.Weight = s.Weight
}
func (s *PatientUserHeight) CopyFrom(target *doctor.PatientUserHeight) {
	if target == nil {
		return
	}

	s.Height = target.Height
}

type PatientUserBaseInfoMonthly struct {
	PatientUserBaseInfoBase

	// 用户ID 关联医生用户登录表用户ID 例如:232442
	UserID uint64 `sql:"UserID"`
	// 身高 单位：cm 例如:165
	Height *uint64 `sql:"Height"`
}

type PatientUserBaseInfoUpdatePhone struct {
	PatientUserBaseInfoBase

	// 用户ID 关联患者用户登录表用户ID 例如:232442
	UserID uint64 `sql:"UserID" primary:"true"`
	// 联系电话 例如:13812344321
	Phone *string `sql:"Phone"`
}
type PatientUserBaseInfoUpdateCountry struct {
	PatientUserBaseInfoBase

	// 所属训练营 例如：测试训练营
	Country string `sql:"Country"`
}
type PatientUserBaseInfoExt struct {
	PatientUserBaseInfoBase
	// 序号 主键，自增 例如:324
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 姓名  例如:张三
	Name string `sql:"Name"`
	// 性别 0-未知, 1-男, 2-女，9-未说明 例如:1
	Sex *uint64 `sql:"Sex"`
	// 出生日期 例如:2000-12-12
	DateOfBirth *time.Time `sql:"DateOfBirth"`
	// 身份证号 例如:330106200012129876
	IdentityCardNumber *string `sql:"IdentityCardNumber"`
	// 文化程度 GB/T 4658-1984 文化程度 例如:大学
	EducationLevel *string `sql:"EducationLevel"`
	// 职业类别 GB/T 6565-1999 职业分类 例如:
	JobType *string `sql:"JobType"`
	// 联系电话 例如:13812344321
	Phone *string `sql:"Phone"`
	// 身高 单位：cm 例如:165
	Height *uint64 `sql:"Height"`
	// 体重 最后一次录入的体重，单位：kg 例如:60.5
	Weight *float64 `sql:"Weight"`

	// 腰围 最后一次录入的腰围，单位：cm 例如:60.5
	Waistline *float64 `sql:"Waistline"`
	// 目标热量值
	GoalEnergy *float64 `sql:"GoalEnergy"`
}

func (s *PatientUserBaseInfoExt) CopyFromExt(source *doctor.PatientUserBaseInfoModify) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.Name = source.Name
	s.Sex = source.Sex
	if source.DateOfBirth == nil {
		s.DateOfBirth = nil
	} else {
		dateOfBirth := time.Time(*source.DateOfBirth)
		s.DateOfBirth = &dateOfBirth
	}
	identityCardNumber := string(source.IdentityCardNumber)
	s.IdentityCardNumber = &identityCardNumber

	educationLevel := string(source.EducationLevel)
	s.EducationLevel = &educationLevel
	jobType := string(source.JobType)
	s.JobType = &jobType
	phone := string(source.Phone)
	s.Phone = &phone
	s.Height = source.Height
	s.Weight = source.Weight
	s.Waistline = source.Waistline
	s.GoalEnergy = source.GoalEnergy

}

type PatientUserBaseInfoUpdateNickname struct {
	PatientUserBaseInfoBase
	// 昵称 例如:蓝精灵
	Nickname *string `sql:"Nickname"`
}

type PatientUserBaseInfoPhoneFilter struct {
	PatientUserBaseInfoBase
	// 联系电话 例如:13812344321
	Phone *string `sql:"Phone"`
}

func (s *PatientUserBaseInfoUpdate) CopyFromUpdate(source *doctor.PatientUserInfoUpdate) {
	if source == nil {
		return
	}
	s.Name = source.Name
	s.Nickname = &source.Nickname
	sex := uint64(0)
	if source.Sex != "" {
		nSex := source.Sex
		switch nSex {
		case "男":
			sex = uint64(1)
		case "女":
			sex = uint64(2)
		}
	}
	s.Sex = &sex
	if source.DateOfBirth == nil {
		s.DateOfBirth = nil
	} else {
		dateOfBirth := time.Time(*source.DateOfBirth)
		s.DateOfBirth = &dateOfBirth
	}
	jobType := source.JobType
	s.JobType = &jobType
	s.Height = &source.Height
	s.Weight = &source.Weight
}

type PatientUserInfoUpdateQuestion struct {
	PatientUserBaseInfoBase
	// 用户ID 关联医生用户登录表用户ID 例如:232442
	UserID uint64 `sql:"UserID"`
	// 性别 0-未知, 1-男, 2-女，9-未说明 例如:1
	Sex *uint64 `sql:"Sex"`
	// 出生日期 例如:2000-12-12
	DateOfBirth *time.Time `sql:"DateOfBirth"`
	// 职业类别 GB/T 6565-1999 职业分类 例如:
	JobType *string `sql:"JobType"`
	// 身高 单位：cm 例如:165
	Height *uint64 `sql:"Height"`
	// 体重 最后一次录入的体重，单位：kg 例如:60.5
	Weight *float64 `sql:"Weight"`
}

func (s *PatientUserInfoUpdateQuestion) CopyFromQuestionInput(source *doctor.ExpPatientNutritionalQuestionInput) {
	if source == nil {
		return
	}
	s.UserID = source.UserID
	s.Sex = source.Sex
	if source.DateOfBirth == nil {
		s.DateOfBirth = nil
	} else {
		dateOfBirth := time.Time(*source.DateOfBirth)
		s.DateOfBirth = &dateOfBirth
	}
	jobType := source.JobType
	s.JobType = &jobType
	s.Height = source.Height
	s.Weight = source.Weight
}
