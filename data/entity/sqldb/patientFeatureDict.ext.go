package sqldb

import (
	"tlcbme_project/data/model/doctor"
	"fmt"
)

func (s *PatientFeatureDict) CopyToCodeItem(target *doctor.DictCodeItem) {
	if target == nil {
		return
	}

	target.Code = s.ItemCode
	target.Name = s.ItemName
}

type PatientFeatureDictCodeItemFilter struct {
	PatientFeatureDictBase
	DictCodeItemFilter
}

func (s *PatientFeatureDictCodeItemFilter) CopyFrom(source *doctor.DictCodeItemFilter) {
	if source == nil {
		return
	}

	if len(source.InputCode) > 0 {
		s.InputCode = fmt.Sprint("%", source.InputCode, "%")
	}
	s.IsValid = 1
}

type PatientFeatureOrder struct {
	PatientFeatureDictBase

	// 排序 用于字典项目排序，从1开始往后排 例如:1
	ItemSortValue *uint64 `sql:"ItemSortValue" order:"asc"`
}
