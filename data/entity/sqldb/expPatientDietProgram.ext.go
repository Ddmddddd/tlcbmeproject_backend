package sqldb

type ExpPatientDietProgramFilter struct {
	ExpPatientDietProgramBase
	PatientID *uint64 `sql:"PatientID"`
}
