package sqldb

import (
	"tlcbme_project/data/model/doctor"
	"github.com/ktpswjz/httpserver/types"
	"time"
)

type DrugRecordFilter struct {
	DrugUseRecordBase

	// 序号 主键，自增 例如:324
	SerialNo *uint64 `sql:"SerialNo"`
}

type DrugRecordData struct {
	DrugUseRecordBase

	// 药品名称  例如:氨氯地平片
	DrugName string `sql:"DrugName"`
	// 剂量 例如:5mg
	Dosage *string `sql:"Dosage"`
	// 备注  例如:
	Memo *string `sql:"Memo"`
	// 用药时间 用药的实际时间 例如:2018-07-03 14:45:00
	UseDateTime *time.Time `sql:"UseDateTime"`
}

type DrugRecordDataEx struct {
	DrugRecordData

	// 序号组 例如:324,248
	SerialNo string `sql:"SerialNo"`
	// 时间点 早、中、晚 例如:早
	TimePoint *string `sql:"TimePoint"`
}

type DrugRecordDataOrder struct {
	DrugUseRecordBase

	// 用药时间 服用药物的时间 例如:2018-07-03 14:45:00
	UseDateTime *time.Time `sql:"UseDateTime"`
}

type DrugRecordDataFilter struct {
	DrugUseRecordBase

	// 用户ID 关联患者用户登录表用户ID 例如:232442
	PatientID uint64 `sql:"PatientID"`

	// 用药时间 服用药物的时间 例如:2018-07-03 14:45:00
	UseStartDate *time.Time `sql:"UseDateTime" filter:">="`
	UseEndDate   *time.Time `sql:"UseDateTime" filter:"<"`
}

func (s *DrugRecordDataFilter) CopyFrom(source *doctor.DrugRecordDataFilterEx) {
	if source == nil {
		return
	}

	s.PatientID = source.PatientID
	if source.UseStartDate != nil {
		s.UseStartDate = source.UseStartDate.ToDate(0)
	}
	if source.UseEndDate != nil {
		s.UseEndDate = source.UseEndDate.ToDate(1)
	}
}

func (s *DrugRecordData) CopyTo(target *doctor.DrugRecordData) {
	if target == nil {
		return
	}

	target.DrugName = s.DrugName
	target.Dosage = string(*s.Dosage)
	if s.UseDateTime == nil {
		target.UseDateTime = nil
	} else {
		useDateTime := types.Time(*s.UseDateTime)
		target.UseDateTime = &useDateTime
	}
	if s.Memo != nil {
		target.Memo = string(*s.Memo)
	}
}

func (s *DrugRecordDataEx) CopyToApp(target *doctor.DrugRecordForApp) {
	if target == nil {
		return
	}

	target.SerialNo = s.SerialNo
	target.DrugName = s.DrugName
	target.Dosage = string(*s.Dosage)
	if s.UseDateTime == nil {
		target.UseDateTime = nil
	} else {
		useDateTime := types.Time(*s.UseDateTime)
		target.UseDateTime = &useDateTime
	}
	if s.Memo != nil {
		target.Memo = string(*s.Memo)
	}
	target.Type = TimePointToType(s.TimePoint)
}

type DrugRecordOrder struct {
	DrugUseRecordBase

	// 用药时间 服用药物的时间 例如:2018-07-03 14:45:00
	UseDateTime *time.Time `sql:"UseDateTime"`
}

type DrugUseRecordFilter struct {
	DrugUseRecordBase

	// 序号 主键，自增 例如:324
	SerialNo *uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 用户ID 关联患者用户登录表用户ID 例如:232442
	PatientID *uint64 `sql:"PatientID"`
	// 时间点 早、中、晚 例如:早
	TimePoint string `sql:"TimePoint"`
	// 药品名称  例如:氨氯地平片
	DrugName string `sql:"DrugName"`
	// 用药时间 用药的实际时间 例如:2018-07-03 14:45:00
	UseDateStart *time.Time `sql:"UseDateTime" filter:">="`
	UseDateEnd   *time.Time `sql:"UseDateTime" filter:"<"`
}

func (s *DrugUseRecordFilter) CopyFrom(source *doctor.DrugUseRecordFilter) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.PatientID = source.PatientID
	s.TimePoint = source.TimePoint
	s.DrugName = source.DrugName
	if source.UseDateStart != nil {
		s.UseDateStart = source.UseDateStart.ToDate(0)
	}
	if source.UseDateEnd != nil {
		s.UseDateEnd = source.UseDateEnd.ToDate(1)
	}
}
