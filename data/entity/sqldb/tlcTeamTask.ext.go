package sqldb

import "time"

type TlcTeamTaskFilterByTeamSno struct {
	TlcTeamTaskBase
	TeamSno uint64 `sql:"TeamSno"`
}

type TlcTeamTaskInsert struct {
	TlcTeamTaskBase
	TeamSno     uint64     `sql:"TeamSno"`
	TeamTaskSno uint64     `sql:"TeamTaskSno"`
	CreateTime  *time.Time `sql:"CreateTime"`
}

type TlcTeamTaskFilter struct {
	TlcTeamTaskBase
	TeamSno     uint64 `sql:"TeamSno"`
	TeamTaskSno uint64 `sql:"TeamTaskSno"`
}
