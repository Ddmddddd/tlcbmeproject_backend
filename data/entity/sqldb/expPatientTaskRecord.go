package sqldb

import (
	"tlcbme_project/data/model/doctor"
	"encoding/json"
	"github.com/ktpswjz/httpserver/types"
	"time"
)

type ExpPatientTaskRecordBase struct {
}

func (s ExpPatientTaskRecordBase) TableName() string {
	return "ExpPatientTaskRecord"
}

type ExpPatientTaskRecord struct {
	ExpPatientTaskRecordBase
	//
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 患者id
	PatientID uint64 `sql:"PatientID"`
	// 对应taskRepository中的serialNo，为0则代表该任务不是按期完成的
	TaskID uint64 `sql:"TaskID"`
	// 父任务名
	ParentTaskName *string `sql:"ParentTaskName"`
	// 任务名
	TaskName string `sql:"TaskName"`
	// 创建时间
	CreateDateTime *time.Time `sql:"CreateDateTime"`
	// 获取积分
	Credit uint64 `sql:"Credit"`
	// 任务完成具体记录
	Record *string `sql:"Record"`
}

func (s *ExpPatientTaskRecord) CopyTo(target *doctor.ExpPatientTaskRecord) {
	if target == nil {
		return
	}
	target.SerialNo = s.SerialNo
	target.PatientID = s.PatientID
	target.TaskID = s.TaskID
	if s.ParentTaskName ==nil {
		target.ParentTaskName = nil
	} else {
		target.ParentTaskName = s.ParentTaskName
	}
	target.TaskName = s.TaskName
	if s.CreateDateTime == nil {
		target.CreateDateTime = nil
	} else {
		createDateTime := types.Time(*s.CreateDateTime)
		target.CreateDateTime = &createDateTime
	}
	target.Credit = s.Credit
	record := s.Record
	if record != nil {
		if len(*record) > 0 {
			json.Unmarshal([]byte(*record), &target.Record)
		}
	}

}

func (s *ExpPatientTaskRecord) CopyFrom(source *doctor.ExpPatientTaskRecord) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.PatientID = source.PatientID
	s.TaskID = source.TaskID
	if source.ParentTaskName == nil {
		s.ParentTaskName = nil
	} else {
		s.ParentTaskName = source.ParentTaskName
	}
	s.TaskName = source.TaskName
	if source.CreateDateTime == nil {
		s.CreateDateTime = nil
	} else {
		createDateTime := time.Time(*source.CreateDateTime)
		s.CreateDateTime = &createDateTime
	}
	s.Credit = source.Credit
	if source.Record != nil {
		recordData, err := json.Marshal(source.Record)
		if err == nil {
			record := string(recordData)
			s.Record = &record
		}
	}

}

func (s *ExpPatientTaskRecord) CopyToCreditHistory(target *doctor.ExpPatientTaskCreditHistory) {
	if target == nil {
		return
	}
	target.TaskName = s.TaskName
	if s.CreateDateTime == nil {
		target.CreateDateTime = nil
	} else {
		createDateTime := types.Time(*s.CreateDateTime)
		target.CreateDateTime = &createDateTime
	}
	target.Credit = s.Credit
}