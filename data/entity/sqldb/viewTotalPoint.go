package sqldb

import "tlcbme_project/data/model/doctor"

type ViewTotalPointBase struct {
}

func (s ViewTotalPointBase) TableName() string {
	return "ViewTotalPoint"
}

type ViewTotalPoint struct {
	ViewTotalPointBase
	//
	PatientID uint64 `sql:"patientID"`
	//
	PatientName string `sql:"patientName"`
	//
	TotalPoint uint64 `sql:"totalPoint"`
}

func (s *ViewTotalPoint) CopyTo(target *doctor.ViewTotalPoint) {
	if target == nil {
		return
	}
	target.PatientID = s.PatientID
	target.PatientName = s.PatientName
	target.TotalPoint = s.TotalPoint
}

func (s *ViewTotalPoint) CopyFrom(source *doctor.ViewTotalPoint) {
	if source == nil {
		return
	}
	s.PatientID = source.PatientID
	s.PatientName = source.PatientName
	s.TotalPoint = source.TotalPoint
}
