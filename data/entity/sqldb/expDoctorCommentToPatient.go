package sqldb

import (
	"github.com/ktpswjz/httpserver/types"
	"time"
	"tlcbme_project/data/model/doctor"
)

type ExpDoctorCommentToPatientBase struct {
}

func (s ExpDoctorCommentToPatientBase) TableName() string {
	return "ExpDoctorCommentToPatient"
}

type ExpDoctorCommentToPatient struct {
	ExpDoctorCommentToPatientBase
	//
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 对应sportDietRecord表中的序号
	SportDietRecord uint64 `sql:"SportDietRecord"`
	// 医生/营养师的评论
	PatientReply *string `sql:"PatientReply"`
	//用户对于该条评论的回复
	Comment string `sql:"Comment"`
	// 患者是否已读，0-未读，1-已读
	ReadFlag uint64 `sql:"ReadFlag"`
	// 患者id
	PatientID uint64 `sql:"PatientID"`
	// 创建时间
	CreateDateTime *time.Time `sql:"CreateDateTime"`
	// 修改时间
	UpdateTime *time.Time `sql:"UpdateTime"`
}

func (s *ExpDoctorCommentToPatient) CopyTo(target *doctor.ExpDoctorCommentToPatient) {
	if target == nil {
		return
	}
	target.SerialNo = s.SerialNo
	target.SportDietRecord = s.SportDietRecord
	target.Comment = s.Comment
	target.PatientReply = s.PatientReply
	target.ReadFlag = s.ReadFlag
	target.PatientID = s.PatientID
	if s.CreateDateTime == nil {
		target.CreateDateTime = nil
	} else {
		createDateTime := types.Time(*s.CreateDateTime)
		target.CreateDateTime = &createDateTime
	}
	if s.UpdateTime == nil {
		target.UpdateTime = nil
	} else {
		updateTime := types.Time(*s.UpdateTime)
		target.UpdateTime = &updateTime
	}
}

func (s *ExpDoctorCommentToPatient) CopyFrom(source *doctor.ExpDoctorCommentToPatient) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.SportDietRecord = source.SportDietRecord
	s.Comment = source.Comment
	s.PatientReply = source.PatientReply
	s.ReadFlag = source.ReadFlag
	s.PatientID = source.PatientID
	if source.CreateDateTime == nil {
		s.CreateDateTime = nil
	} else {
		createDateTime := time.Time(*source.CreateDateTime)
		s.CreateDateTime = &createDateTime
	}
	if source.UpdateTime == nil {
		s.UpdateTime = nil
	} else {
		updateTime := time.Time(*source.UpdateTime)
		s.UpdateTime = &updateTime
	}
}

func (s *ExpDoctorCommentToPatient) CopyFromCreate(source *doctor.ExpDoctorCommentToPatientCreate) {
	if source == nil {
		return
	}
	s.SportDietRecord = source.SportDietRecord
	s.Comment = source.Comment
	if len(source.Comment) == 0 {
		s.Comment = " "
	}
	s.ReadFlag = source.ReadFlag
	s.PatientID = source.PatientID
	if source.CreateDateTime == nil {
		s.CreateDateTime = nil
	} else {
		createDateTime := time.Time(*source.CreateDateTime)
		s.CreateDateTime = &createDateTime
	}
	if source.UpdateTime == nil {
		s.UpdateTime = nil
	} else {
		updateTime := time.Time(*source.UpdateTime)
		s.UpdateTime = &updateTime
	}
}

func (s *ExpDoctorCommentToPatient) CopyToWithDietInfo(target *doctor.ExpDoctorCommentToPatientWithDietInfo) {
	if target == nil {
		return
	}
	target.SerialNo = s.SerialNo
	target.SportDietRecord = s.SportDietRecord
	target.Comment = s.Comment
	target.ReadFlag = s.ReadFlag
	target.PatientID = s.PatientID
	if s.CreateDateTime == nil {
		target.CreateDateTime = nil
	} else {
		createDateTime := types.Time(*s.CreateDateTime)
		target.CreateDateTime = &createDateTime
	}
	if s.UpdateTime == nil {
		target.UpdateTime = nil
	} else {
		updateTime := types.Time(*s.UpdateTime)
		target.UpdateTime = &updateTime
	}
}
