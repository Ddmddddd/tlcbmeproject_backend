package sqldb

import (
	"encoding/json"
	"github.com/ktpswjz/httpserver/types"
	"time"
	"tlcbme_project/data/model/doctor"
)

type ViewManagedIndexBase struct {
}

func (s ViewManagedIndexBase) TableName() string {
	return "ViewManagedIndex"
}

// VIEW
type ViewManagedIndex struct {
	ViewManagedIndexBase
	// 序号 主键，自增 例如:324
	SerialNo uint64 `sql:"SerialNo"`
	// 用户ID 关联患者用户登录表用户ID 例如:232442
	PatientID uint64 `sql:"PatientID"`
	// 姓名  例如:张三
	PatientName *string `sql:"PatientName"`
	// 性别 0-未知, 1-男, 2-女，9-未说明 例如:1
	Sex *uint64 `sql:"Sex"`
	// 出生日期 例如:2000-12-12
	DateOfBirth *time.Time `sql:"DateOfBirth"`
	// 患者特点 如有多个，之间用逗号分隔，例如：老年人，肥胖，残疾人 例如:老年人，肥胖
	PatientFeature *string `sql:"PatientFeature"`
	// 管理状态 0-管理中，1-迁出，2-已终止管理 例如:0
	ManageStatus uint64 `sql:"ManageStatus"`
	// 管理分类 如有多个，之间用逗号分隔，例如：高血压，糖尿病 例如:高血压，糖尿病
	ManageClass *string `sql:"ManageClass"`
	// 管理分类代码，例如：1
	ManageClassCode *uint64 `sql:"ManageClassCode"`
	// 管理机构代码 管理该患者的机构 例如:897798
	OrgCode *string `sql:"OrgCode"`
	// 医生ID 例如:232
	DoctorID *uint64 `sql:"DoctorID"`
	// 医生姓名 例如:张三
	DoctorName *string `sql:"DoctorName"`
	// 健康管理师ID 例如:232
	HealthManagerID *uint64 `sql:"HealthManagerID"`
	// 健康管理师姓名 例如:李四
	HealthManagerName *string `sql:"HealthManagerName"`
	// 管理开始时间 最近一次管理的开始时间，如果是刚注册，则默认为审核通过时间 例如:2018-07-03 13:15:00
	ManageStartDateTime *time.Time `sql:"ManageStartDateTime"`
	// 依从度 值为0至5，0表示依从度未知 例如:4
	ComplianceRate *uint64 `sql:"ComplianceRate"`
	// 患者活跃度 用户参与管理的活跃度，0-沉睡，1-活跃，默认值为1 例如:1
	PatientActiveDegree uint64 `sql:"PatientActiveDegree"`
	// 管理终止时间 该字段只有当ManageStatus字段为2时才有意义 例如:2018-08-03 13:15:00
	ManageEndDateTime *time.Time `sql:"ManageEndDateTime"`
	// 管理终止的原因 该字段只有当ManageStatus字段为2时才有意义 例如:亡故（2018-08-03，车祸死亡）
	ManageEndReason *string `sql:"ManageEndReason"`
	//
	ManageLevel []byte `sql:"ManageLevel"`
	//
	DmManageLevel []byte `sql:"DmManageLevel"`
	// 扩展信息 保存不同管理分类的扩展信息 例如:{htn:{}, dm:{}}
	Ext *string `sql:"Ext"`
}

func (s *ViewManagedIndex) CopyTo(target *doctor.ViewManagedIndex) {
	if target == nil {
		return
	}
	target.SerialNo = s.SerialNo
	target.PatientID = s.PatientID
	if s.PatientName != nil {
		target.PatientName = string(*s.PatientName)
	}
	target.Sex = s.Sex
	if s.DateOfBirth == nil {
		target.DateOfBirth = nil
	} else {
		dateOfBirth := types.Time(*s.DateOfBirth)
		target.DateOfBirth = &dateOfBirth
	}
	if s.PatientFeature != nil {
		target.PatientFeature = string(*s.PatientFeature)
	}
	target.ManageStatus = s.ManageStatus
	if s.ManageClass != nil {
		target.ManageClass = string(*s.ManageClass)
	}
	target.ManageClassCode = s.ManageClassCode
	if s.OrgCode != nil {
		target.OrgCode = string(*s.OrgCode)
	}
	target.DoctorID = s.DoctorID
	if s.DoctorName != nil {
		target.DoctorName = string(*s.DoctorName)
	}
	target.HealthManagerID = s.HealthManagerID
	if s.HealthManagerName != nil {
		target.HealthManagerName = string(*s.HealthManagerName)
	}
	if s.ManageStartDateTime == nil {
		target.ManageStartDateTime = nil
	} else {
		manageStartDateTime := types.Time(*s.ManageStartDateTime)
		target.ManageStartDateTime = &manageStartDateTime
	}
	target.ComplianceRate = s.ComplianceRate
	target.PatientActiveDegree = s.PatientActiveDegree
	if s.ManageEndDateTime == nil {
		target.ManageEndDateTime = nil
	} else {
		manageEndDateTime := types.Time(*s.ManageEndDateTime)
		target.ManageEndDateTime = &manageEndDateTime
	}
	if s.ManageEndReason != nil {
		target.ManageEndReason = string(*s.ManageEndReason)
	}
	target.ManageLevel = string(s.ManageLevel)
	target.DmManageLevel = string(s.DmManageLevel)
	ext := ""
	if s.Ext != nil {
		ext = *s.Ext
	}
	if len(ext) > 0 {
		json.Unmarshal([]byte(ext), &target.Ext)
	}

}

func (s *ViewManagedIndex) CopyFrom(source *doctor.ViewManagedIndex) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.PatientID = source.PatientID
	patientName := string(source.PatientName)
	s.PatientName = &patientName
	s.Sex = source.Sex
	if source.DateOfBirth == nil {
		s.DateOfBirth = nil
	} else {
		dateOfBirth := time.Time(*source.DateOfBirth)
		s.DateOfBirth = &dateOfBirth
	}
	patientFeature := string(source.PatientFeature)
	s.PatientFeature = &patientFeature
	s.ManageStatus = source.ManageStatus
	manageClass := string(source.ManageClass)
	s.ManageClass = &manageClass
	s.ManageClassCode = source.ManageClassCode
	orgCode := string(source.OrgCode)
	s.OrgCode = &orgCode
	s.DoctorID = source.DoctorID
	doctorName := string(source.DoctorName)
	s.DoctorName = &doctorName
	s.HealthManagerID = source.HealthManagerID
	healthManagerName := string(source.HealthManagerName)
	s.HealthManagerName = &healthManagerName
	if source.ManageStartDateTime == nil {
		s.ManageStartDateTime = nil
	} else {
		manageStartDateTime := time.Time(*source.ManageStartDateTime)
		s.ManageStartDateTime = &manageStartDateTime
	}
	s.ComplianceRate = source.ComplianceRate
	s.PatientActiveDegree = source.PatientActiveDegree
	if source.ManageEndDateTime == nil {
		s.ManageEndDateTime = nil
	} else {
		manageEndDateTime := time.Time(*source.ManageEndDateTime)
		s.ManageEndDateTime = &manageEndDateTime
	}
	manageEndReason := string(source.ManageEndReason)
	s.ManageEndReason = &manageEndReason
	s.ManageLevel = []byte(source.ManageLevel)
	s.DmManageLevel = []byte(source.DmManageLevel)
	if source.Ext != nil {
		extData, err := json.Marshal(source.Ext)
		if err == nil {
			ext := string(extData)
			s.Ext = &ext
		}
	}

}
