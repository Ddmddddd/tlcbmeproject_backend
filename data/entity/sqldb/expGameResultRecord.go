package sqldb

import (
	"tlcbme_project/data/model/doctor"
	"github.com/ktpswjz/httpserver/types"
	"time"
)

type ExpGameResultRecordBase struct {
}

func (s ExpGameResultRecordBase) TableName() string {
	return "ExpGameResultRecord"
}

// 用户游戏得分表
// 用于记录用户每次游戏的得分与完成时间
type ExpGameResultRecord struct {
	ExpGameResultRecordBase
	// 序号
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 用户ID
	PatientID uint64 `sql:"PatientID"`
	// 用户姓名，涉及隐私，可考虑让用户选择录入一个昵称
	PatientName string `sql:"PatientName"`
	// 得分
	Point uint64 `sql:"Point"`
	// 完成时间
	CreateDateTime *time.Time `sql:"CreateDateTime"`
}

func (s *ExpGameResultRecord) CopyTo(target *doctor.ExpGameResultRecord) {
	if target == nil {
		return
	}
	target.SerialNo = s.SerialNo
	target.PatientID = s.PatientID
	target.PatientName = s.PatientName
	target.Point = s.Point
	if s.CreateDateTime == nil {
		target.CreateDateTime = nil
	} else {
		createDateTime := types.Time(*s.CreateDateTime)
		target.CreateDateTime = &createDateTime
	}
}

func (s *ExpGameResultRecord) CopyFrom(source *doctor.ExpGameResultRecord) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.PatientID = source.PatientID
	s.PatientName = source.PatientName
	s.Point = source.Point
	if source.CreateDateTime == nil {
		s.CreateDateTime = nil
	} else {
		createDateTime := time.Time(*source.CreateDateTime)
		s.CreateDateTime = &createDateTime
	}
}
