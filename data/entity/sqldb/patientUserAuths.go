package sqldb

import (
	"tlcbme_project/data/model/doctor"
	"github.com/ktpswjz/httpserver/types"
	"time"
)

type PatientUserAuthsBase struct {
}

func (s PatientUserAuthsBase) TableName() string {
	return "PatientUserAuths"
}

// 平台运维
// 平台用户管理
// 患者用户登录表
// 注释：该表保存患者用户站内登录账号，用于登录验证。
type PatientUserAuths struct {
	PatientUserAuthsBase
	// 用户ID 主键，自增 例如:
	UserID uint64 `sql:"UserID" auto:"true" primary:"true"`
	// 用户名 如果注册时没有指定用户名，则系统默认分配一个用户名，格式为cdmwb+userID，例如cdmwb123 例如:zhangsan
	UserName string `sql:"UserName"`
	// 手机号 绑定的手机号 例如:13818765432
	MobilePhone *string `sql:"MobilePhone"`
	// 电子邮箱 绑定的电子邮箱地址 例如:tlcbme_project@vico-lab.com
	Email *string `sql:"Email"`
	// 登录密码 加密后存储 例如:291fakfjwi98234fsf23fjw
	Password string `sql:"Password"`
	// 登录密码格式 0-明文，1-MD5，2-SHA1，3-SHA256，4-SHA384，5-SHA512 例如:1
	PasswordFormat uint64 `sql:"PasswordFormat"`
	// 用户状态 0-正常，1-冻结，9-已注销 例如:0
	Status uint64 `sql:"Status"`
	// 注册时间  例如:2018-07-02 15:00:00
	RegistDateTime *time.Time `sql:"RegistDateTime"`
	// 登录次数 累计登录次数 例如:12
	LoginCount uint64 `sql:"LoginCount"`
	// 最后一次登录时间  例如:2018-07-02 15:10:00
	LastLoginDateTime *time.Time `sql:"LastLoginDateTime"`
	// 累计使用时长 单位：秒 例如:24342
	UseSeconds uint64 `sql:"UseSeconds"`
}

func (s *PatientUserAuths) CopyTo(target *doctor.PatientUserAuths) {
	if target == nil {
		return
	}
	target.UserID = s.UserID
	target.UserName = s.UserName
	if s.MobilePhone != nil {
		target.MobilePhone = string(*s.MobilePhone)
	}
	if s.Email != nil {
		target.Email = string(*s.Email)
	}
	target.Password = s.Password
	target.PasswordFormat = s.PasswordFormat
	target.Status = s.Status
	if s.RegistDateTime == nil {
		target.RegistDateTime = nil
	} else {
		registDateTime := types.Time(*s.RegistDateTime)
		target.RegistDateTime = &registDateTime
	}
	target.LoginCount = s.LoginCount
	if s.LastLoginDateTime == nil {
		target.LastLoginDateTime = nil
	} else {
		lastLoginDateTime := types.Time(*s.LastLoginDateTime)
		target.LastLoginDateTime = &lastLoginDateTime
	}
	target.UseSeconds = s.UseSeconds
}

func (s *PatientUserAuths) CopyFrom(source *doctor.PatientUserAuths) {
	if source == nil {
		return
	}
	s.UserID = source.UserID
	s.UserName = source.UserName
	mobilePhone := string(source.MobilePhone)
	s.MobilePhone = &mobilePhone
	email := string(source.Email)
	s.Email = &email
	s.Password = source.Password
	s.PasswordFormat = source.PasswordFormat
	s.Status = source.Status
	if source.RegistDateTime == nil {
		s.RegistDateTime = nil
	} else {
		registDateTime := time.Time(*source.RegistDateTime)
		s.RegistDateTime = &registDateTime
	}
	s.LoginCount = source.LoginCount
	if source.LastLoginDateTime == nil {
		s.LastLoginDateTime = nil
	} else {
		lastLoginDateTime := time.Time(*source.LastLoginDateTime)
		s.LastLoginDateTime = &lastLoginDateTime
	}
	s.UseSeconds = source.UseSeconds
}
