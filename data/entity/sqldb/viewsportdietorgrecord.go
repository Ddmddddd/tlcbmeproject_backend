package sqldb

import (
	"bytes"
	"tlcbme_project/data/model/doctor"
	"encoding/gob"
	"github.com/ktpswjz/httpserver/types"
	"time"
)

type ViewsportdietorgrecordBase struct {
}

func (s ViewsportdietorgrecordBase) TableName() string {
	return "ViewSportDietOrgRecord"
}

func (s ViewsportdietorgrecordBase) SetFilter(v interface{}) {

}

func (s *ViewsportdietorgrecordBase) Clone() (interface{}, error) {
	return &ViewsportdietorgrecordBase{}, nil
}

// VIEW
type Viewsportdietorgrecord struct {
	ViewsportdietorgrecordBase
	// 序号 主键，自增 例如:324
	Serialno uint64 `sql:"SerialNo"`
	// 用户ID 关联患者用户登录表用户ID 例如:232442
	Patientid uint64 `sql:"PatientID"`
	// 用户ID 关联患者用户登录表用户ID 例如:232442
	RecordType string `sql:"RecordType"`
	// 发生时间 实际发生的时间 例如:2018-07-03 14:45:00
	Happendatetime *time.Time `sql:"HappenDateTime"`
	// 管理机构代码 管理该患者的机构 例如:897798
	Orgcode *string `sql:"OrgCode"`
}

func (s *Viewsportdietorgrecord) Clone() (interface{}, error) {
	t := &Viewsportdietorgrecord{}

	buf := new(bytes.Buffer)
	enc := gob.NewEncoder(buf)
	dec := gob.NewDecoder(buf)
	err := enc.Encode(s)
	if err != nil {
		return nil, err
	}
	err = dec.Decode(t)
	if err != nil {
		return nil, err
	}

	return t, nil
}

func (s *Viewsportdietorgrecord) CopyTo(target *doctor.Viewsportdietorgrecord) {
	if target == nil {
		return
	}
	target.Serialno = s.Serialno
	target.Patientid = s.Patientid
	target.RecordType = s.RecordType
	if s.Happendatetime == nil {
		target.Happendatetime = nil
	} else {
		happendatetime := types.Time(*s.Happendatetime)
		target.Happendatetime = &happendatetime
	}
	if s.Orgcode != nil {
		target.Orgcode = string(*s.Orgcode)
	}
}

func (s *Viewsportdietorgrecord) CopyFrom(source *doctor.Viewsportdietorgrecord) {
	if source == nil {
		return
	}
	s.Serialno = source.Serialno
	s.Patientid = source.Patientid
	s.RecordType = source.RecordType
	if source.Happendatetime == nil {
		s.Happendatetime = nil
	} else {
		happendatetime := time.Time(*source.Happendatetime)
		s.Happendatetime = &happendatetime
	}
	orgcode := string(source.Orgcode)
	s.Orgcode = &orgcode
}
