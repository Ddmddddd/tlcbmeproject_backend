package sqldb

import "time"

type TlcKnwScheduleBase struct {
}

func (s TlcKnwScheduleBase) TableName() string {
	return "tlcknwschedule"
}

type TlcKnwSchedule struct {
	TlcKnwScheduleBase

	SerialNo     uint64     `sql:"SerialNo"`
	ScheduleName string     `sql:"ScheduleName"`
	StartTime    *time.Time `sql:"StartTime"`
	HoldDayNum   uint64     `sql:"HoldDayNum"`
}
