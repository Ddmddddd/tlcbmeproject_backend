package sqldb

import (
	"tlcbme_project/data/model/doctor"
	"github.com/ktpswjz/httpserver/types"
	"time"
)

type ExpScaleToPatientBase struct {
}

func (s ExpScaleToPatientBase) TableName() string {
	return "ExpScaleToPatient"
}

// 量表推送
// 为患者推送的量表，每个患者不同
type ExpScaleToPatient struct {
	ExpScaleToPatientBase
	// 序号
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 患者ID
	PatientID uint64 `sql:"PatientID"`
	// 量表id
	ScaleID *uint64 `sql:"ScaleID"`
	// 任务id
	TaskID *uint64 `sql:"TaskID"`
	// 小量表id
	SmallScaleID *uint64 `sql:"SmallScaleID"`
	// 状态，0-未完成，1-已完成，2-放弃完成
	Status uint64 `sql:"Status"`
	// 创建时间
	CreateDateTime *time.Time `sql:"CreateDateTime"`
	// 更新时间
	UpdateTime *time.Time `sql:"UpdateTime"`
	// 如果任务status=1，该字段标明问卷record中的序号
	RecordID *uint64 `sql:"RecordID"`
}

func (s *ExpScaleToPatient) CopyTo(target *doctor.ExpScaleToPatient) {
	if target == nil {
		return
	}
	target.SerialNo = s.SerialNo
	target.PatientID = s.PatientID
	target.ScaleID = s.ScaleID
	target.TaskID = s.TaskID
	target.SmallScaleID = s.SmallScaleID
	target.Status = s.Status
	if s.CreateDateTime == nil {
		target.CreateDateTime = nil
	} else {
		createDateTime := types.Time(*s.CreateDateTime)
		target.CreateDateTime = &createDateTime
	}
	if s.UpdateTime == nil {
		target.UpdateTime = nil
	} else {
		updateTime := types.Time(*s.UpdateTime)
		target.UpdateTime = &updateTime
	}
	target.RecordID = s.RecordID
}

func (s *ExpScaleToPatient) CopyFrom(source *doctor.ExpScaleToPatient) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.PatientID = source.PatientID
	s.ScaleID = source.ScaleID
	s.TaskID = source.TaskID
	s.SmallScaleID = source.SmallScaleID
	s.Status = source.Status
	if source.CreateDateTime == nil {
		s.CreateDateTime = nil
	} else {
		createDateTime := time.Time(*source.CreateDateTime)
		s.CreateDateTime = &createDateTime
	}
	if source.UpdateTime == nil {
		s.UpdateTime = nil
	} else {
		updateTime := time.Time(*source.UpdateTime)
		s.UpdateTime = &updateTime
	}
	s.RecordID = source.RecordID
}
