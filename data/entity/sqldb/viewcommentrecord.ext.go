package sqldb

import (
	"fmt"
	"time"
	"tlcbme_project/data/model/doctor"
)

type ViewCommentRecordHealthManagerIDFilter struct {
	ViewCommentRecordBase
	// 任务名
	TaskName *string `sql:"TaskName"`
	// 健康管理师ID 例如:232
	HealthManagerID *uint64 `sql:"HealthManagerID"`
	// 患者姓名
	Name           *string    `sql:"Name" filter:"like"`
	ParentTaskName *string    `sql:"ParentTaskName"`
	StartDateTime  *time.Time `sql:"CreateDateTime" filter:">="`
	EndDateTime    *time.Time `sql:"CreateDateTime" filter:"<="`
}

type ViewCommentRecordAllFilter struct {
	ViewCommentRecordBase
	// 任务名
	TaskName *string `sql:"TaskName"`
	// 患者id
	PatientID      *uint64    `sql:"PatientID"`
	ParentTaskName *string    `sql:"ParentTaskName"`
	StartDateTime  *time.Time `sql:"CreateDateTime" filter:">="`
	EndDateTime    *time.Time `sql:"CreateDateTime" filter:"<="`
}

type ViewCommentRecordTimeOrderDesc struct {
	ViewCommentRecordBase
	// 创建时间
	CreateDateTime *time.Time `sql:"CreateDateTime" order:"desc"`
}

func (s *ViewCommentRecordHealthManagerIDFilter) CopyFrom(source *doctor.ExpPatientTaskRecordGetByName) {
	if source == nil {
		return
	}
	s.TaskName = &source.TaskName
	s.HealthManagerID = &source.HealthManagerID
	s.ParentTaskName = source.ParentTaskName
	s.StartDateTime = (*time.Time)(source.StartDateTime)
	s.EndDateTime = (*time.Time)(source.EndDateTime)
	if len(source.Name) > 0 {
		name := fmt.Sprint("%", source.Name, "%")
		s.Name = &name
	}
}

func (s *ViewCommentRecordAllFilter) CopyFromAll(source *doctor.ExpPatientTaskRecordGetByNameAll) {
	if source == nil {
		return
	}
	s.TaskName = &source.TaskName
	s.ParentTaskName = source.ParentTaskName
	s.StartDateTime = (*time.Time)(source.StartDateTime)
	s.EndDateTime = (*time.Time)(source.EndDateTime)
	s.PatientID = &source.PatientID
}
