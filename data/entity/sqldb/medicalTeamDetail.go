package sqldb

import "tlcbme_project/data/model/doctor"

type MedicalTeamDetailBase struct {
}

func (s MedicalTeamDetailBase) TableName() string {
	return "MedicalTeamDetail"
}

// 平台运维
// 平台用户管理
// 医疗团队成员
// 注释：该表保存医疗团队中的成员列表
type MedicalTeamDetail struct {
	MedicalTeamDetailBase
	// 序号 主键，自增 例如:324
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 团队代码 对应医疗团队的团队ID 例如:1
	TeamCode uint64 `sql:"TeamCode"`
	// 医生用户ID 对应医生用户表的用户ID字段 例如:123
	UserID uint64 `sql:"UserID"`
	// 是否团队负责人 0-普通成员，1-团队负责人 例如:0
	IsTeamLeader uint64 `sql:"IsTeamLeader"`
}

func (s *MedicalTeamDetail) CopyTo(target *doctor.MedicalTeamDetail) {
	if target == nil {
		return
	}
	target.SerialNo = s.SerialNo
	target.TeamCode = s.TeamCode
	target.UserID = s.UserID
	target.IsTeamLeader = s.IsTeamLeader
}

func (s *MedicalTeamDetail) CopyFrom(source *doctor.MedicalTeamDetail) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.TeamCode = source.TeamCode
	s.UserID = source.UserID
	s.IsTeamLeader = source.IsTeamLeader
}
