package sqldb

import (
	"tlcbme_project/data/enum"
	"tlcbme_project/data/model/doctor"
	"fmt"
	"github.com/ktpswjz/httpserver/types"
	"time"
)

type DoctorUserAuthsAuthenticationFilter struct {
	DoctorUserAuthsBase

	// 用户名 如果注册时没有指定用户名，则系统默认分配一个用户名，格式为cdmwb+userID，例如cdmwb123 例如:zhangsan
	UserName string `sql:"UserName"`
	// 手机号 绑定的手机号 例如:13818765432
	MobilePhone string `sql:"MobilePhone"`
	// 电子邮箱 绑定的电子邮箱地址 例如:tlcbme_project@vico-lab.com
	Email string `sql:"Email"`
}

type DoctorUserAuthsUpdateFilter struct {
	DoctorUserAuthsBase

	// 用户ID 主键，自增 例如:
	UserID uint64 `sql:"UserID" auto:"true"`
}

type DoctorUserAuthsInfo struct {
	DoctorUserAuthsBase
	// 用户ID 主键，自增 例如:
	UserID uint64 `sql:"UserID" auto:"true"`
	// 用户名 如果注册时没有指定用户名，则系统默认分配一个用户名，格式为cdmwb+userID，例如cdmwb123 例如:zhangsan
	UserName string `sql:"UserName"`
	// 手机号 绑定的手机号 例如:13818765432
	MobilePhone string `sql:"MobilePhone"`
	// 电子邮箱 绑定的电子邮箱地址 例如:tlcbme_project@vico-lab.com
	Email string `sql:"Email"`
	// 登录密码 加密后存储 例如:291fakfjwi98234fsf23fjw
	Password string `sql:"Password"`
	// 登录密码格式 0-明文，11-MD5，21-SHA1，22-SHA256，23-SHA384，24-SHA512 例如:11
	PasswordFormat uint64 `sql:"PasswordFormat"`
	// 用户状态 0-正常，1-冻结，9-已注销 例如:0
	Status uint64 `sql:"Status"`
	// 注册时间  例如:2018-07-02 15:00:00
	RegistDateTime *time.Time `sql:"RegistDateTime"`
	// 登录次数 累计登录次数 例如:12
	LoginCount uint64 `sql:"LoginCount"`
	// 最后一次登录时间  例如:2018-07-02 15:10:00
	LastLoginDateTime *time.Time `sql:"LastLoginDateTime"`
	// 累计使用时长 单位：秒 例如:24342
	UseSeconds uint64 `sql:"UseSeconds"`
}

func (s *DoctorUserAuthsInfo) CopyTo(target *doctor.DoctorUserAuthsInfo) {
	if target == nil {
		return
	}
	target.UserID = s.UserID
	target.UserName = s.UserName
	target.MobilePhone = s.MobilePhone
	target.Email = s.Email
	target.Status = s.Status
	target.StatusText = enum.AccountStatuses.Value(s.Status)
	if s.RegistDateTime == nil {
		target.RegistDateTime = nil
	} else {
		registDateTime := types.Time(*s.RegistDateTime)
		target.RegistDateTime = &registDateTime
	}
	target.LoginCount = s.LoginCount
	if s.LastLoginDateTime == nil {
		target.LastLoginDateTime = nil
	} else {
		lastLoginDateTime := types.Time(*s.LastLoginDateTime)
		target.LastLoginDateTime = &lastLoginDateTime
	}
	target.UseSeconds = s.UseSeconds
}

type DoctorUserAuthsInfoFilter struct {
	DoctorUserAuthsBase
	// 用户ID 主键，自增 例如:
	UserID *uint64 `sql:"UserID" auto:"true"`
	// 用户名 如果注册时没有指定用户名，则系统默认分配一个用户名，格式为cdmwb+userID，例如cdmwb123 例如:zhangsan
	UserName *string `sql:"UserName"`
	// 手机号 绑定的手机号 例如:13818765432
	MobilePhone *string `sql:"MobilePhone"`
	// 电子邮箱 绑定的电子邮箱地址 例如:tlcbme_project@vico-lab.com
	Email *string `sql:"Email"`
	// 用户状态 0-正常，1-冻结，9-已注销 例如:0
	Status *uint64 `sql:"Status"`
}

func (s *DoctorUserAuthsInfoFilter) CopyFrom(source *doctor.DoctorUserAuthsInfoFilter) {
	if source == nil {
		return
	}
	s.UserID = source.UserID
	s.UserName = source.UserName
	s.MobilePhone = source.MobilePhone
	s.Email = source.Email
	s.Status = source.Status
}

type DoctorUserAuthsInfoLikeFilter struct {
	ViewDoctorUserBase

	// 用户名 如果注册时没有指定用户名，则系统默认分配一个用户名，格式为cdmwb+userID，例如cdmwb123 例如:zhangsan
	UserName string `sql:"UserName" filter:"like"`
	// 手机号 绑定的手机号 例如:13818765432
	MobilePhone string `sql:"MobilePhone" filter:"like"`
	// 电子邮箱 绑定的电子邮箱地址 例如:tlcbme_project@vico-lab.com
	OrgName string `sql:"OrgName" filter:"like"`
}

func (s *DoctorUserAuthsInfoLikeFilter) CopyFrom(source *doctor.DoctorUserAuthsInfoLikeFilter) {
	if source == nil {
		return
	}
	if len(source.Account) > 0 {
		s.UserName = fmt.Sprint("%", source.Account, "%")
		s.MobilePhone = fmt.Sprint("%", source.Account, "%")
		s.OrgName = fmt.Sprint("%", source.Account, "%")
	}
}

type DoctorUserAuthsInfoStatusFilter struct {
	DoctorUserAuthsBase

	// 用户状态 0-正常，1-冻结，9-已注销 例如:0
	Statuses []uint64 `sql:"Status" filter:"in"`
}

func (s *DoctorUserAuthsInfoStatusFilter) CopyFrom(source *doctor.DoctorUserAuthsInfoLikeFilter) {
	if source == nil {
		return
	}
	if len(source.Statuses) > 0 {
		s.Statuses = source.Statuses
	}
}

type DoctorUserAuthsExcludedIdFilter struct {
	DoctorUserAuthsBase
	// 用户ID 主键，自增 例如:
	UserID uint64 `sql:"UserID" auto:"true" filter:"!="`
}

type DoctorUserAuthsEdit struct {
	DoctorUserAuthsBase

	// 用户名 如果注册时没有指定用户名，则系统默认分配一个用户名，格式为cdmwb+userID，例如cdmwb123 例如:zhangsan
	UserName *string `sql:"UserName"`
	// 手机号 绑定的手机号 例如:13818765432
	MobilePhone *string `sql:"MobilePhone"`
	// 电子邮箱 绑定的电子邮箱地址 例如:tlcbme_project@vico-lab.com
	Email *string `sql:"Email"`
	// 用户状态 0-正常，1-冻结，9-已注销 例如:0
	Status uint64 `sql:"Status"`
}

func (s *DoctorUserAuthsEdit) CopyFrom(source *doctor.DoctorUserAuthsEdit) {
	if source == nil {
		return
	}
	userName := string(source.UserName)
	s.UserName = &userName
	mobilePhone := string(source.MobilePhone)
	s.MobilePhone = &mobilePhone
	email := string(source.Email)
	s.Email = &email
	s.Status = source.Status
}

type DoctorUserAuthsPassword struct {
	DoctorUserAuthsBase
	// 用户ID 主键，自增 例如:

	// 登录密码 加密后存储 例如:291fakfjwi98234fsf23fjw
	Password string `sql:"Password"`
	// 登录密码格式 0-明文，11-MD5，21-SHA1，22-SHA256，23-SHA384，24-SHA512 例如:11
	PasswordFormat uint64 `sql:"PasswordFormat"`
}

type DoctorUserAuthsIdFilter struct {
	DoctorUserAuthsBase
	// 用户ID 主键，自增 例如:
	UserID uint64 `sql:"UserID" auto:"true"`
}

type DoctorUserAuthsLoginUpdate struct {
	DoctorUserAuthsBase

	// 登录次数 累计登录次数 例如:12
	LoginCount uint64 `sql:"LoginCount"`
	// 最后一次登录时间  例如:2018-07-02 15:10:00
	LastLoginDateTime *time.Time `sql:"LastLoginDateTime"`
}

type DoctorUserAuthsPasswordUpdate struct {
	DoctorUserAuthsBase

	// 登录密码 加密后存储 例如:291fakfjwi98234fsf23fjw
	Password string `sql:"Password"`
	// 登录密码格式 0-明文，11-MD5，21-SHA1，22-SHA256，23-SHA384，24-SHA512 例如:11
	PasswordFormat uint64 `sql:"PasswordFormat"`
}

type DoctorUserAuthsBaseInfo struct {
	DoctorUserAuthsBase

	// 用户ID 主键，自增 例如:
	UserID uint64 `sql:"UserID"`
	// 用户名 如果注册时没有指定用户名，则系统默认分配一个用户名，格式为cdmwb+userID，例如cdmwb123 例如:zhangsan
	UserName string `sql:"UserName"`
	// 姓名  例如:张三
	Name string `sql:"Name"`
}

func (s *DoctorUserAuthsBaseInfo) CopyTo(target *doctor.DoctorUserAuthsBaseInfo) {
	if target == nil {
		return
	}
	target.UserID = s.UserID
	target.UserName = s.UserName
	target.Name = s.Name
}
