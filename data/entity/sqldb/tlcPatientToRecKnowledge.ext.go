package sqldb

type TlcPatientToRecKnowledgeFilterByDayIndex struct {
	TlcPatientToRecKnowledgeBase
	Dayindex uint64 `sql:"dayindex"`
	UserID   uint64 `sql:"UserID"`
}

type TlcPatientToRecKnowledgeTodayFilter struct {
	TlcPatientToRecKnowledgeBase
	UserID   uint64 `sql:"UserID"`
	Dayindex uint64 `sql:"dayindex"`
	Status   uint64 `sql:"status"`
}

type TlcPatientToRecKnowledgeGetStarFilter struct {
	TlcPatientToRecKnowledgeBase
	UserID       uint64 `sql:"UserID"`
	Knowledge_id uint64 `sql:"knowledge_id"`
}

type TlcPatientToRecKnowledgeUpdateStar struct {
	TlcPatientToRecKnowledgeBase
	Stars uint64 `sql:"stars"`
}

type TlcPatientToRecKnowledgeUpdateStarFilter struct {
	TlcPatientToRecKnowledgeBase
	UserID       uint64 `sql:"UserID"`
	Knowledge_id uint64 `sql:"knowledge_id"`
}

type TlcPatientToRecKnowledgeChangeStatus struct {
	TlcPatientToRecKnowledgeBase
	Status uint64 `sql:"status"`
}

type TlcPatientToRecKnowledgeChangeStatusFilter struct {
	TlcPatientToRecKnowledgeBase
	UserID       uint64 `sql:"UserID"`
	Knowledge_id uint64 `sql:"knowledge_id"`
}

type TlcPatientToRecKnowledgeFilterByPatientID struct {
	TlcPatientToRecKnowledgeBase
	UserID uint64 `sql:"UserID"`
}

type TlcPatientToRecKnowledgeOrderByDayIndex struct {
	TlcPatientToRecKnowledgeBase
	Dayindex uint64 `sql:"dayindex"`
}

type TlcPatientToRecKnowledgeKnoStatusStatisticFilter struct {
	TlcPatientToRecKnowledgeBase
	Knowledge_id uint64 `sql:"knowledge_id"`
	Status       uint64 `sql:"status"`
}

type TlcPatientToRecKnowledgeFilterByUserAndKnwID struct {
	TlcPatientToRecKnowledgeBase
	UserID       uint64 `sql:"UserID"`
	Knowledge_id uint64 `sql:"knowledge_id"`
}
