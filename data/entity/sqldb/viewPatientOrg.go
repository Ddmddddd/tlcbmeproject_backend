package sqldb

import "tlcbme_project/data/model/doctor"

type ViewPatientOrgBase struct {
}

func (s ViewPatientOrgBase) TableName() string {
	return "ViewPatientOrg"
}

// VIEW
type ViewPatientOrg struct {
	ViewPatientOrgBase
	// 用户ID 关联医生用户登录表用户ID 例如:232442
	PatientID uint64 `sql:"PatientID"`
	// 姓名  例如:张三
	Name string `sql:"Name"`
	// 管理机构代码 管理该患者的机构 例如:897798
	OrgCode *string `sql:"OrgCode"`
	// 医生姓名 例如:张三
	DoctorName *string `sql:"DoctorName"`
	// 机构名称  例如:第一人民医院
	OrgName *string `sql:"OrgName"`
}

func (s *ViewPatientOrg) CopyTo(target *doctor.ViewPatientOrg) {
	if target == nil {
		return
	}
	target.PatientID = s.PatientID
	target.Name = s.Name
	if s.OrgCode != nil {
		target.OrgCode = string(*s.OrgCode)
	}
	if s.DoctorName != nil {
		target.DoctorName = string(*s.DoctorName)
	}
	if s.OrgName != nil {
		target.OrgName = string(*s.OrgName)
	}
}

func (s *ViewPatientOrg) CopyFrom(source *doctor.ViewPatientOrg) {
	if source == nil {
		return
	}
	s.PatientID = source.PatientID
	s.Name = source.Name
	orgCode := string(source.OrgCode)
	s.OrgCode = &orgCode
	doctorName := string(source.DoctorName)
	s.DoctorName = &doctorName
	orgName := string(source.OrgName)
	s.OrgName = &orgName
}
