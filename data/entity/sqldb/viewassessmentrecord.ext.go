package sqldb

import "time"

type ViewassessmentrecordOrder struct {
	ViewassessmentrecordBase
	// 测量时间 测量血压的时间 例如:2018-07-03 14:45:00
	Assessdatetime *time.Time `sql:"AssessDateTime" order:"DESC" `
}

type ViewassessmentrecordFilter struct {
	// 评估名称  例如:高血压危险分层评估
	Assessmentname string `sql:"AssessmentName" filter:"like"`
	// 管理机构代码 管理该患者的机构 例如:897798
	Orgcode *string `sql:"OrgCode"`
	// 评估时间  例如:2018-08-01 10:00:00
	Assessdatetime *time.Time `sql:"AssessDateTime" filter:"<"`
}
