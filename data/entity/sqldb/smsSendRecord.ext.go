package sqldb

import (
	"tlcbme_project/data/model/doctor"
	"time"
)

type SmsSendRecordFilter struct {
	SmsSendRecordBase

	// 模板类型 0-验证码，1-短信通知，2-推广短信 例如:1
	TemplateType *uint64 `sql:"TemplateType"`
	// 短信发送时间 例如:2020-02-16 14:23:00
	SendStartDatetime *time.Time `sql:"SendDatetime" filter:">="`
	// 短信发送时间 例如:2020-02-16 14:23:00
	SendEndDatetime *time.Time `sql:"SendDatetime" filter:"<"`
	// 患者ID 例如:324
	PatientID *uint64 `sql:"PatientID"`
}
type SmsSendRecordOrder struct {
	SmsSendRecordBase

	// 消息时间 消息发生的时间 例如:2018-07-03 14:45:00
	SendDatetime *time.Time `sql:"SendDatetime" order:"DESC"`
}

func (s *SmsSendRecordFilter) CopyFrom(source *doctor.SmsSendRecordFilter) {
	if source == nil {
		return
	}
	templateType := uint64(1)
	s.PatientID = source.PatientID
	now := time.Now()
	startTime := now.AddDate(0, -3, 0)
	start := time.Date(startTime.Year(), startTime.Month(), startTime.Day(), 0, 0, 0, 0, now.Location())
	s.SendStartDatetime = &start
	s.SendEndDatetime = &now
	s.TemplateType = &templateType
}
