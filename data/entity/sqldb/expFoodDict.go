package sqldb

import "tlcbme_project/data/model/doctor"

type ExpFoodDictBase struct {
}

func (s ExpFoodDictBase) TableName() string {
	return "ExpFoodDict"
}

// 食物表
type ExpFoodDict struct {
	ExpFoodDictBase
	// 食物编号
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 分类
	Category uint64 `sql:"Category"`
	// 名字
	Name string `sql:"Name"`
	// 图片地址
	ImageUrl *string `sql:"ImageUrl"`
	// 每100g卡路里
	Calorie float64 `sql:"Calorie"`
	// 推荐性,10最好,1不推荐,0未知
	Recommended uint64 `sql:"Recommended"`
	// 评价
	Comment string `sql:"Comment"`
	// 碳水化合物(克)
	NutritionCarbohydrate *float64 `sql:"NutritionCarbohydrate"`
	// 脂肪(克)
	NutritionFat *float64 `sql:"NutritionFat"`
	// 蛋白质(克)
	NutritionProtein *float64 `sql:"NutritionProtein"`
	// 纤维素(克)
	NutritionFibre *float64 `sql:"NutritionFibre"`
	// 维生素A(微克)
	NutritionVitaminA *float64 `sql:"NutritionVitaminA"`
	// 维生素C(毫克)
	NutritionVitaminC *float64 `sql:"NutritionVitaminC"`
	// 维生素E(毫克)
	NutritionVitaminE *float64 `sql:"NutritionVitaminE"`
	// 胡萝卜素(微克)
	NutritionCarotene *float64 `sql:"NutritionCarotene"`
	// 硫胺素(毫克)
	NutritionThiamine *float64 `sql:"NutritionThiamine"`
	// 核黄素(毫克)
	NutritionRiboflavin *float64 `sql:"NutritionRiboflavin"`
	// 烟酸(毫克)
	NutritionNiacin *float64 `sql:"NutritionNiacin"`
	// 胆固醇(毫克)
	NutritionCholesterol *float64 `sql:"NutritionCholesterol"`
	// 镁(毫克)
	NutritionMagnesium *float64 `sql:"NutritionMagnesium"`
	// 钙(毫克)
	Nutrition_calcium *float64 `sql:"nutrition_calcium"`
	// 铁(毫克)
	NutritionIron *float64 `sql:"NutritionIron"`
	// 锌(毫克)
	NutritionZinc *float64 `sql:"NutritionZinc"`
	// 铜(毫克)
	NutritionCopper *float64 `sql:"NutritionCopper"`
	// 锰(毫克)
	NutritionManganese *float64 `sql:"NutritionManganese"`
	// 钾(毫克)
	NutritionPotassium *float64 `sql:"NutritionPotassium"`
	// 磷(毫克)
	NutritionPhosphorus *float64 `sql:"NutritionPhosphorus"`
	// 钠(毫克)
	NutritionSodium *float64 `sql:"NutritionSodium"`
	// 硒(微克)
	NutritionSelenium *float64 `sql:"NutritionSelenium"`
}

func (s *ExpFoodDict) CopyTo(target *doctor.ExpFoodDict) {
	if target == nil {
		return
	}
	target.SerialNo = s.SerialNo
	target.Category = s.Category
	target.Name = s.Name
	if s.ImageUrl != nil {
		target.ImageUrl = string(*s.ImageUrl)
	}
	target.Calorie = s.Calorie
	target.Recommended = s.Recommended
	target.Comment = s.Comment
	target.NutritionCarbohydrate = s.NutritionCarbohydrate
	target.NutritionFat = s.NutritionFat
	target.NutritionProtein = s.NutritionProtein
	target.NutritionFibre = s.NutritionFibre
	target.NutritionVitaminA = s.NutritionVitaminA
	target.NutritionVitaminC = s.NutritionVitaminC
	target.NutritionVitaminE = s.NutritionVitaminE
	target.NutritionCarotene = s.NutritionCarotene
	target.NutritionThiamine = s.NutritionThiamine
	target.NutritionRiboflavin = s.NutritionRiboflavin
	target.NutritionNiacin = s.NutritionNiacin
	target.NutritionCholesterol = s.NutritionCholesterol
	target.NutritionMagnesium = s.NutritionMagnesium
	target.Nutrition_calcium = s.Nutrition_calcium
	target.NutritionIron = s.NutritionIron
	target.NutritionZinc = s.NutritionZinc
	target.NutritionCopper = s.NutritionCopper
	target.NutritionManganese = s.NutritionManganese
	target.NutritionPotassium = s.NutritionPotassium
	target.NutritionPhosphorus = s.NutritionPhosphorus
	target.NutritionSodium = s.NutritionSodium
	target.NutritionSelenium = s.NutritionSelenium
}

func (s *ExpFoodDict) CopyFrom(source *doctor.ExpFoodDict) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.Category = source.Category
	s.Name = source.Name
	imageUrl := string(source.ImageUrl)
	s.ImageUrl = &imageUrl
	s.Calorie = source.Calorie
	s.Recommended = source.Recommended
	s.Comment = source.Comment
	s.NutritionCarbohydrate = source.NutritionCarbohydrate
	s.NutritionFat = source.NutritionFat
	s.NutritionProtein = source.NutritionProtein
	s.NutritionFibre = source.NutritionFibre
	s.NutritionVitaminA = source.NutritionVitaminA
	s.NutritionVitaminC = source.NutritionVitaminC
	s.NutritionVitaminE = source.NutritionVitaminE
	s.NutritionCarotene = source.NutritionCarotene
	s.NutritionThiamine = source.NutritionThiamine
	s.NutritionRiboflavin = source.NutritionRiboflavin
	s.NutritionNiacin = source.NutritionNiacin
	s.NutritionCholesterol = source.NutritionCholesterol
	s.NutritionMagnesium = source.NutritionMagnesium
	s.Nutrition_calcium = source.Nutrition_calcium
	s.NutritionIron = source.NutritionIron
	s.NutritionZinc = source.NutritionZinc
	s.NutritionCopper = source.NutritionCopper
	s.NutritionManganese = source.NutritionManganese
	s.NutritionPotassium = source.NutritionPotassium
	s.NutritionPhosphorus = source.NutritionPhosphorus
	s.NutritionSodium = source.NutritionSodium
	s.NutritionSelenium = source.NutritionSelenium
}
