package sqldb

import (
	"tlcbme_project/data/model/doctor"
	"github.com/ktpswjz/httpserver/types"
	"time"
)

type ExpKnowledgeBase struct {
}

func (s ExpKnowledgeBase) TableName() string {
	return "ExpKnowledge"
}

type ExpKnowledge struct {
	ExpKnowledgeBase
	//
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 知识类型，例如：“粗粮”
	Type string `sql:"Type"`
	// 知识标题
	KnoTitle string `sql:"KnoTitle"`
	// 知识内容，富文本形式
	Content string `sql:"Content"`
	// 知识配图
	ImageUrl *string `sql:"ImageUrl"`
	// 创建时间
	CreateDateTime *time.Time `sql:"CreateDateTime"`
	// 编辑者id
	EditorID uint64 `sql:"EditorID"`
	// 状态
	Status uint64 `sql:"Status"`
	// 更新时间
	UpdateTime *time.Time `sql:"UpdateTime"`
}

func (s *ExpKnowledge) CopyTo(target *doctor.ExpKnowledge) {
	if target == nil {
		return
	}
	target.SerialNo = s.SerialNo
	target.Type = s.Type
	target.KnoTitle = s.KnoTitle
	target.Content = s.Content
	if s.ImageUrl == nil {
		target.ImageUrl = nil
	} else {
		target.ImageUrl = s.ImageUrl
	}
	if s.CreateDateTime == nil {
		target.CreateDateTime = nil
	} else {
		createDateTime := types.Time(*s.CreateDateTime)
		target.CreateDateTime = &createDateTime
	}
	target.EditorID = s.EditorID
	target.Status = s.Status
	if s.UpdateTime == nil {
		target.UpdateTime = nil
	} else {
		updateTime := types.Time(*s.UpdateTime)
		target.UpdateTime = &updateTime
	}
}

func (s *ExpKnowledge) CopyFrom(source *doctor.ExpKnowledge) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.Type = source.Type
	s.KnoTitle = source.KnoTitle
	s.Content = source.Content
	if source.ImageUrl == nil {
		s.ImageUrl = nil
	} else {
		s.ImageUrl = source.ImageUrl
	}
	if source.CreateDateTime == nil {
		s.CreateDateTime = nil
	} else {
		createDateTime := time.Time(*source.CreateDateTime)
		s.CreateDateTime = &createDateTime
	}
	s.EditorID = source.EditorID
	s.Status = source.Status
	if source.UpdateTime == nil {
		s.UpdateTime = nil
	} else {
		updateTime := time.Time(*source.UpdateTime)
		s.UpdateTime = &updateTime
	}
}

func (s *ExpKnowledge) CopyFromAddInput(source *doctor.ExpKnowledgeAddInput) {
	if source == nil {
		return
	}
	s.Type = source.Type
	s.KnoTitle = source.KnoTitle
	s.Content = source.Content
	if source.ImageUrl == nil {
		s.ImageUrl = nil
	} else {
		s.ImageUrl = source.ImageUrl
	}
	if source.CreateDateTime == nil {
		s.CreateDateTime = nil
	} else {
		createDateTime := time.Time(*source.CreateDateTime)
		s.CreateDateTime = &createDateTime
	}
	s.EditorID = source.EditorID
}