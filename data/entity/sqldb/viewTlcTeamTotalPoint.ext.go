package sqldb

type ViewTlcTeamTotalPointFilterByTeamSno struct {
	ViewTlcTeamTotalPointBase
	TeamSno uint64 `sql:"TeamSno"`
}

type ViewTlcTeamTotalPointFilterByGroup struct {
	ViewTlcTeamTotalPointBase
	BelongToGroup string `sql:"BelongToGroup"`
}

type ViewTlcTeamTotalPointOrderByIntegral struct {
	ViewTlcTeamTotalPointBase
	TeamTotalIntegral uint64 `sql:"TeamTotalIntegral" order:"desc"`
}
