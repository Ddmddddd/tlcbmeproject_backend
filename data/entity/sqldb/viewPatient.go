package sqldb

import (
	"tlcbme_project/data/model/doctor"
	"github.com/ktpswjz/httpserver/types"
	"time"
)

type ViewPatientBase struct {
}

func (s ViewPatientBase) TableName() string {
	return "ViewPatient"
}

// VIEW
type ViewPatient struct {
	ViewPatientBase
	// 用户ID 关联医生用户登录表用户ID 例如:232442
	PatientID uint64 `sql:"PatientID"`
	// 姓名  例如:张三
	PatientName string `sql:"PatientName"`
	// 性别 0-未知, 1-男, 2-女，9-未说明 例如:1
	Sex *uint64 `sql:"Sex"`
	// 出生日期 例如:2000-12-12
	DateOfBirth *time.Time `sql:"DateOfBirth"`
	// 身份证号 例如:330106200012129876
	IdentityCardNumber *string `sql:"IdentityCardNumber"`
	// 联系电话 例如:13812344321
	Phone *string `sql:"Phone"`
	// 患者特点 如有多个，之间用逗号分隔，例如：老年人，肥胖，残疾人 例如:老年人，肥胖
	PatientFeature *string `sql:"PatientFeature"`
	// 管理分类 如有多个，之间用逗号分隔，例如：高血压，糖尿病 例如:高血压，糖尿病
	ManageClass *string `sql:"ManageClass"`
	// 管理机构代码 管理该患者的机构 例如:897798
	OrgCode *string `sql:"OrgCode"`
	// 医生ID 例如:232
	DoctorID *uint64 `sql:"DoctorID"`
	// 医生姓名 例如:张三
	DoctorName *string `sql:"DoctorName"`
	// 健康管理师ID 例如:232
	HealthManagerID *uint64 `sql:"HealthManagerID"`
	// 健康管理师姓名 例如:李四
	HealthManagerName *string `sql:"HealthManagerName"`
}

func (s *ViewPatient) CopyTo(target *doctor.ViewPatient) {
	if target == nil {
		return
	}
	target.PatientID = s.PatientID
	target.PatientName = s.PatientName
	target.Sex = s.Sex
	if s.DateOfBirth == nil {
		target.DateOfBirth = nil
	} else {
		dateOfBirth := types.Time(*s.DateOfBirth)
		target.DateOfBirth = &dateOfBirth
	}
	if s.IdentityCardNumber != nil {
		target.IdentityCardNumber = string(*s.IdentityCardNumber)
	}
	if s.Phone != nil {
		target.Phone = string(*s.Phone)
	}
	if s.PatientFeature != nil {
		target.PatientFeature = string(*s.PatientFeature)
	}
	if s.ManageClass != nil {
		target.ManageClass = string(*s.ManageClass)
	}
	if s.OrgCode != nil {
		target.OrgCode = string(*s.OrgCode)
	}
	target.DoctorID = s.DoctorID
	if s.DoctorName != nil {
		target.DoctorName = string(*s.DoctorName)
	}
	target.HealthManagerID = s.HealthManagerID
	if s.HealthManagerName != nil {
		target.HealthManagerName = string(*s.HealthManagerName)
	}
}

func (s *ViewPatient) CopyFrom(source *doctor.ViewPatient) {
	if source == nil {
		return
	}
	s.PatientID = source.PatientID
	s.PatientName = source.PatientName
	s.Sex = source.Sex
	if source.DateOfBirth == nil {
		s.DateOfBirth = nil
	} else {
		dateOfBirth := time.Time(*source.DateOfBirth)
		s.DateOfBirth = &dateOfBirth
	}
	identityCardNumber := string(source.IdentityCardNumber)
	s.IdentityCardNumber = &identityCardNumber
	phone := string(source.Phone)
	s.Phone = &phone
	patientFeature := string(source.PatientFeature)
	s.PatientFeature = &patientFeature
	manageClass := string(source.ManageClass)
	s.ManageClass = &manageClass
	orgCode := string(source.OrgCode)
	s.OrgCode = &orgCode
	s.DoctorID = source.DoctorID
	doctorName := string(source.DoctorName)
	s.DoctorName = &doctorName
	s.HealthManagerID = source.HealthManagerID
	healthManagerName := string(source.HealthManagerName)
	s.HealthManagerName = &healthManagerName
}
