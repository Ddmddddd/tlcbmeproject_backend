package sqldb

import (
	"tlcbme_project/data/model/doctor"
	"time"
)

type ExpKnowledgeTypeFilter struct {
	ExpKnowledgeBase
	// 知识类型，例如：“粗粮”
	Type string `sql:"Type"`
	// 状态
	Status uint64 `sql:"Status"`
}

type ExpKnowledgeSerialNoFilter struct {
	ExpKnowledgeBase
	//
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
}

type ExpKnowledgeEdit struct {
	ExpKnowledgeBase
	//
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 知识标题
	KnoTitle string `sql:"KnoTitle"`
	// 知识内容，富文本形式
	Content string `sql:"Content"`
	// 知识配图
	ImageUrl *string `sql:"ImageUrl"`
	// 编辑者id
	EditorID uint64 `sql:"EditorID"`
	// 更新时间
	UpdateTime *time.Time `sql:"UpdateTime"`
}

func (s *ExpKnowledgeEdit) CopyFrom(source *doctor.ExpKnowledgeEditInput) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.KnoTitle = source.KnoTitle
	s.Content = source.Content
	s.ImageUrl = source.ImageUrl
	s.EditorID = source.EditorID
	if source.UpdateTime == nil {
		s.UpdateTime = nil
	} else {
		updateTime := time.Time(*source.UpdateTime)
		s.UpdateTime = &updateTime
	}
}

type ExpKnowledgeDeleteInput struct {
	ExpKnowledgeBase
	//
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 编辑者id
	EditorID uint64 `sql:"EditorID"`
	// 状态
	Status uint64 `sql:"Status"`
	// 更新时间
	UpdateTime *time.Time `sql:"UpdateTime"`
}

func (s *ExpKnowledgeDeleteInput) CopyFromDeleteInput(source *doctor.ExpKnowledgeDeleteInput) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.EditorID = source.EditorID
	s.Status = source.Status
	if source.UpdateTime == nil {
		s.UpdateTime = nil
	} else {
		updateTime := time.Time(*source.UpdateTime)
		s.UpdateTime = &updateTime
	}
}

type ExpKnowledgeSerialNosFilter struct {
	ExpKnowledgeBase
	//
	SerialNos []uint64 `sql:"SerialNo" filter:"in"`
}