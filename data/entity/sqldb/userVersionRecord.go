package sqldb

import "tlcbme_project/data/model/doctor"

type UserVersionRecordBase struct {
}

func (s UserVersionRecordBase) TableName() string {
	return "UserVersionRecord"
}

// 平台运维
// 患者APP配置
// 患者APP版本记录表
// 注释：本表记录用户当前所用APP的版本信息
type UserVersionRecord struct {
	UserVersionRecordBase
	// 序号 例如:1
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 用户ID 关联患者用户登录表用户ID 例如:232442
	UserID uint64 `sql:"UserID"`
	// 版本代码 例如:1
	VersionCode uint64 `sql:"VersionCode"`
	// 版本名称 例如:1.0.0.1
	VersionName string `sql:"VersionName"`
}

func (s *UserVersionRecord) CopyTo(target *doctor.UserVersionRecord) {
	if target == nil {
		return
	}
	target.SerialNo = s.SerialNo
	target.UserID = s.UserID
	target.VersionCode = s.VersionCode
	target.VersionName = s.VersionName
}

func (s *UserVersionRecord) CopyFrom(source *doctor.UserVersionRecord) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.UserID = source.UserID
	s.VersionCode = source.VersionCode
	s.VersionName = source.VersionName
}
