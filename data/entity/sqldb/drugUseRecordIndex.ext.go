package sqldb

import (
	"tlcbme_project/data/model/doctor"
	"time"
)

func (s *DrugUseRecordIndex) CopyToEx(target *doctor.DrugUseRecordIndexEx) {
	if target == nil {
		return
	}
	s.CopyTo(&target.DrugUseRecordIndex)

	if s.FirstUseDateTime != nil {
		dateStart := time.Date(s.FirstUseDateTime.Year(), s.FirstUseDateTime.Month(), s.FirstUseDateTime.Day(), 0, 0, 0, 0, s.FirstUseDateTime.Location())
		now := time.Now().Add(time.Hour * 24)
		dateEnd := time.Date(now.Year(), now.Month(), now.Day(), 0, 0, 0, 0, now.Location())

		if s.Status != 0 {
			if s.LastModifyDateTime != nil {
				dateEnd = time.Date(s.LastModifyDateTime.Year(), s.LastModifyDateTime.Month(), s.LastModifyDateTime.Day(), 0, 0, 0, 0, s.LastModifyDateTime.Location())
				dateEnd = dateEnd.Add(time.Hour * 24)
			}
		}

		days := dateEnd.Add(-time.Nanosecond).Sub(dateStart)
		target.UsedDays = int64(days / time.Hour / 24)
	} else {
		target.UsedDays = -1
	}
}

type DrugUseRecordIndexOrder struct {
	DrugUseRecordIndexBase

	// 状态 0-使用中，1-已停用 例如:0
	Status uint64 `sql:"Status"`
	// 首次使用时间 例如:2018-01-01 08:50:00
	FirstUseDateTime *time.Time `sql:"FirstUseDateTime"`
}

type DrugUseRecordIndexFilter struct {
	DrugUseRecordIndexBase

	// 序号 主键，自增 例如:324
	SerialNo *uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 用户ID 关联患者用户登录表用户ID 例如:232442
	PatientID *uint64 `sql:"PatientID"`
	// 药品名称  例如:氨氯地平片
	DrugName string `sql:"DrugName"`
	// 状态 0-使用中，1-已停用 例如:0
	Status *uint64 `sql:"Status"`
	// 是否调整过用法 0-未调整过，1-调整过 例如:0
	IsModified *uint64 `sql:"IsModified"`
}

func (s *DrugUseRecordIndexFilter) CopyFrom(source *doctor.DrugUseRecordIndexFilter) {
	if source == nil {
		return
	}

	s.SerialNo = source.SerialNo
	s.PatientID = source.PatientID
	s.Status = source.Status
	s.IsModified = source.IsModified
}

type DrugUseRecordIndexUpdateStatus struct {
	DrugUseRecordIndexBase
	// 序号 主键，自增 例如:324
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 状态 0-使用中，1-已停用 例如:0
	Status uint64 `sql:"Status"`
	// 是否调整过用法 0-未调整过，1-调整过 例如:0
	IsModified uint64 `sql:"IsModified"`
	// 最后一次调整用法的时间 该字段只有当IsModified为1时才有意义 例如:2018-08-08 13:30:33
	LastModifyDateTime *time.Time `sql:"LastModifyDateTime"`
}

type DrugUseRecordIndexUpdateUsage struct {
	DrugUseRecordIndexBase
	// 序号 主键，自增 例如:324
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 剂量 例如:5mg
	Dosage *string `sql:"Dosage"`
	// 频次 例如:1次/日
	Freq *string `sql:"Freq"`
	// 是否调整过用法 0-未调整过，1-调整过 例如:0
	IsModified uint64 `sql:"IsModified"`
	// 最后一次调整用法的时间 该字段只有当IsModified为1时才有意义 例如:2018-08-08 13:30:33
	LastModifyDateTime *time.Time `sql:"LastModifyDateTime"`
}
