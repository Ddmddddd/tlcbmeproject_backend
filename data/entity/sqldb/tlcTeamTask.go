package sqldb

import "time"

type TlcTeamTaskBase struct {
}

func (s TlcTeamTaskBase) TableName() string {
	return "tlcteamtask"
}

type TlcTeamTask struct {
	TlcTeamTaskBase
	SerialNo    uint64     `sql:"SerialNo"`
	TeamSno     uint64     `sql:"TeamSno"`
	TeamTaskSno uint64     `sql:"TeamTaskSno"`
	CreateTime  *time.Time `sql:"CreateTime"`
}
