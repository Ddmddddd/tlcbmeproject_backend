package sqldb

import (
	"bytes"
	"tlcbme_project/data/model/doctor"
	"encoding/gob"
	"time"
)

type ViewfollowuprecordBase struct {
}

func (s ViewfollowuprecordBase) TableName() string {
	return "ViewFollowupRecord"
}

func (s ViewfollowuprecordBase) SetFilter(v interface{}) {

}

func (s *ViewfollowuprecordBase) Clone() (interface{}, error) {
	return &ViewfollowuprecordBase{}, nil
}

// VIEW
type Viewfollowuprecord struct {
	ViewfollowuprecordBase
	// 序号 主键，自增 例如:324
	Serialno uint64 `sql:"SerialNo"`
	// 用户ID 关联患者用户登录表用户ID 例如:232442
	Patientid uint64 `sql:"PatientID"`
	// 随访类型  例如:三个月例行随访
	Followuptype *string `sql:"FollowupType"`
	// 随访时间  例如:2018-08-01 10:00:00
	FollowupDateTime *time.Time `sql:"FollowupDateTime"`
	// 管理机构代码 管理该患者的机构 例如:897798
	Orgcode *string `sql:"OrgCode"`
}

func (s *Viewfollowuprecord) Clone() (interface{}, error) {
	t := &Viewfollowuprecord{}

	buf := new(bytes.Buffer)
	enc := gob.NewEncoder(buf)
	dec := gob.NewDecoder(buf)
	err := enc.Encode(s)
	if err != nil {
		return nil, err
	}
	err = dec.Decode(t)
	if err != nil {
		return nil, err
	}

	return t, nil
}

func (s *Viewfollowuprecord) CopyTo(target *doctor.Viewfollowuprecord) {
	if target == nil {
		return
	}
	target.Serialno = s.Serialno
	target.Patientid = s.Patientid
	if s.Followuptype != nil {
		target.Followuptype = string(*s.Followuptype)
	}
	if s.Orgcode != nil {
		target.Orgcode = string(*s.Orgcode)
	}
}

func (s *Viewfollowuprecord) CopyFrom(source *doctor.Viewfollowuprecord) {
	if source == nil {
		return
	}
	s.Serialno = source.Serialno
	s.Patientid = source.Patientid
	followuptype := string(source.Followuptype)
	s.Followuptype = &followuptype
	orgcode := string(source.Orgcode)
	s.Orgcode = &orgcode
}
