package sqldb

import (
	"strconv"
	"time"
	"tlcbme_project/data/enum"
	"tlcbme_project/data/model/doctor"
)

type TlcTeamTaskRepositoryBase struct {
}

func (s TlcTeamTaskRepositoryBase) TableName() string {
	return "tlcteamtaskrepository"
}

type TlcTeamTaskRepository struct {
	TlcTeamTaskRepositoryBase
	SerialNo          uint64     `sql:"SerialNo"`
	TaskName          string     `sql:"TaskName"`
	Content           string     `sql:"Content"`
	DescriptionPicUrl string     `sql:"DescriptionPicUrl"`
	Level             uint64     `sql:"Level"`
	CreateTime        *time.Time `sql:"CreateTime"`
}

func (s *TlcTeamTaskRepository) CopyToTaskForSelf(target *doctor.TlcTeamTaskForSelf) {
	if target == nil {
		return
	}
	target.TaskID = s.SerialNo
	target.TaskName = s.TaskName
	target.Content = s.Content
	target.DescriptionPicUrl = s.DescriptionPicUrl
}

func (s *TlcTeamTaskRepository) CopyToTaskForTeam(target *doctor.TlcTeamTaskData) {
	if target == nil {
		return
	}
	target.TaskID = s.SerialNo
	target.TaskName = s.TaskName
	target.Content = s.Content
	target.DescriptionPicUrl = s.DescriptionPicUrl
	target.Status = 1
}

func (s *TlcTeamTaskRepository) GetTaskRespondedIntegralTemplate() *doctor.TlcTeamTaskMapWithIntegralTem {

	// example:每日记录饮食：完成率>0%，积分+1；完成率>=50%，积分+3；完成率=100%，积分+5；

	TaskIntegralRuleInfo := &doctor.TlcTeamTaskMapWithIntegralTem{}

	baseSeq := "积分+"

	template := enum.TlcTeamTaskIntegralRuleTems.FirstLevParticipation().Value + baseSeq + strconv.FormatUint(enum.TlcTeamTaskIntegralRuleTems.FirstLevParticipation().Key*s.Level, 10) + "；" +
		enum.TlcTeamTaskIntegralRuleTems.SecondLevParticipation().Value + baseSeq + strconv.FormatUint(enum.TlcTeamTaskIntegralRuleTems.SecondLevParticipation().Key*s.Level, 10) + "；" +
		enum.TlcTeamTaskIntegralRuleTems.ThirdLevParticipation().Value + baseSeq + strconv.FormatUint(enum.TlcTeamTaskIntegralRuleTems.ThirdLevParticipation().Key*s.Level, 10)

	TaskIntegralRuleInfo.IntegralTemplate = template

	switch s.TaskName {
	case "饮食":
		TaskIntegralRuleInfo.RuleDescription = "每日记录饮食："
	case "运动":
		TaskIntegralRuleInfo.RuleDescription = "每日记录运动："
	case "教育":
		TaskIntegralRuleInfo.RuleDescription = "每日阅读知识："
	}
	return TaskIntegralRuleInfo
}
