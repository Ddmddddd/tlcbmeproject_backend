package sqldb

import "time"

type ExpScaleToPatientSmallScaleFilter struct {
	ExpScaleToPatientBase
	// 小量表id
	SmallScaleID *uint64 `sql:"SmallScaleID"`
	// 状态，0-未完成，1-已完成，2-放弃完成
	Status uint64 `sql:"Status"`
}

type ExpScaleToPatientUpdate struct {
	ExpScaleToPatientBase
	// 序号
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 状态，0-未完成，1-已完成，2-放弃完成
	Status uint64 `sql:"Status"`
	// 更新时间
	UpdateTime *time.Time `sql:"UpdateTime"`
	// 如果任务status=1，该字段标明问卷record中的序号
	RecordID *uint64 `sql:"RecordID"`
}

type ExpScaleToPatientUndoneFilter struct {
	ExpScaleToPatientBase
	// 患者ID
	PatientID uint64 `sql:"PatientID"`
	// 状态，0-未完成，1-已完成，2-放弃完成
	Status uint64 `sql:"Status"`
}