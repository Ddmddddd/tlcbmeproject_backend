package sqldb

import "tlcbme_project/data/model/doctor"

type MonthlyAnalysisBase struct {
}

func (s MonthlyAnalysisBase) TableName() string {
	return "MonthlyAnalysis"
}

// 慢病管理业务
// 患者APP月报数据
// 月数据解读信息表
// 注释：本表保存患者的当月自我管理数据解读记录。
type MonthlyAnalysis struct {
	MonthlyAnalysisBase
	// 序号 主键，自增 例如:324
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 用户ID 关联患者用户登录表用户ID 例如:232442
	PatientID uint64 `sql:"PatientID"`
	// 月份 年份+月份（2016-09） 例如:2016-09
	YearMonth string `sql:"YearMonth"`
	// 完成度分析 对当月总体管理情况的分析 例如:记录血压、体重保持得很好，记录服药有所放松
	CompletenessAnalysis *string `sql:"CompletenessAnalysis"`
	// 血压趋势分析 对当月血压趋势图的分析 例如:血压平稳下降，达标仍需努力
	BpTrendAnalysis *string `sql:"BpTrendAnalysis"`
	// 血压数据解读 对当月血压数据的解读 例如:一半以上测量达标，高血压值不可忽略
	BpAnalysis *string `sql:"BpAnalysis"`
	// 心率趋势分析 对当月心率趋势图的分析 例如:心率曲线相对平稳，达标仍需努力
	HrTrendAnalysis *string `sql:"HrTrendAnalysis"`
	// 心率数据解读 对当月心率数据的解读 例如:心率值基本正常，偶尔偏快
	HrAnalysis *string `sql:"HrAnalysis"`
	// 体重趋势分析 对当月体重趋势图的分析 例如:体重基本没变，偷懒了吧？
	WeightTrendAnalysis *string `sql:"WeightTrendAnalysis"`
	// 体重数据解读 对当月体重数据的解读 例如:需要控制饮食，加强锻炼了
	WeightAnalysis *string `sql:"WeightAnalysis"`
	// 健康管理师建议 根据本月的总体情况健康管理师给出的建议 例如:继续保持，坚持管理
	AdviceFromDoctor *string `sql:"AdviceFromDoctor"`
}

func (s *MonthlyAnalysis) CopyTo(target *doctor.MonthlyAnalysis) {
	if target == nil {
		return
	}
	target.SerialNo = s.SerialNo
	target.PatientID = s.PatientID
	target.YearMonth = s.YearMonth
	if s.CompletenessAnalysis != nil {
		target.CompletenessAnalysis = string(*s.CompletenessAnalysis)
	}
	if s.BpTrendAnalysis != nil {
		target.BpTrendAnalysis = string(*s.BpTrendAnalysis)
	}
	if s.BpAnalysis != nil {
		target.BpAnalysis = string(*s.BpAnalysis)
	}
	if s.HrTrendAnalysis != nil {
		target.HrTrendAnalysis = string(*s.HrTrendAnalysis)
	}
	if s.HrAnalysis != nil {
		target.HrAnalysis = string(*s.HrAnalysis)
	}
	if s.WeightTrendAnalysis != nil {
		target.WeightTrendAnalysis = string(*s.WeightTrendAnalysis)
	}
	if s.WeightAnalysis != nil {
		target.WeightAnalysis = string(*s.WeightAnalysis)
	}
	if s.AdviceFromDoctor != nil {
		target.AdviceFromDoctor = string(*s.AdviceFromDoctor)
	}
}

func (s *MonthlyAnalysis) CopyFrom(source *doctor.MonthlyAnalysis) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.PatientID = source.PatientID
	s.YearMonth = source.YearMonth
	completenessAnalysis := string(source.CompletenessAnalysis)
	s.CompletenessAnalysis = &completenessAnalysis
	bpTrendAnalysis := string(source.BpTrendAnalysis)
	s.BpTrendAnalysis = &bpTrendAnalysis
	bpAnalysis := string(source.BpAnalysis)
	s.BpAnalysis = &bpAnalysis
	hrTrendAnalysis := string(source.HrTrendAnalysis)
	s.HrTrendAnalysis = &hrTrendAnalysis
	hrAnalysis := string(source.HrAnalysis)
	s.HrAnalysis = &hrAnalysis
	weightTrendAnalysis := string(source.WeightTrendAnalysis)
	s.WeightTrendAnalysis = &weightTrendAnalysis
	weightAnalysis := string(source.WeightAnalysis)
	s.WeightAnalysis = &weightAnalysis
	adviceFromDoctor := string(source.AdviceFromDoctor)
	s.AdviceFromDoctor = &adviceFromDoctor
}
