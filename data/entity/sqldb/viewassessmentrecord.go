package sqldb

import (
	"bytes"
	"tlcbme_project/data/model/doctor"
	"encoding/gob"
	"github.com/ktpswjz/httpserver/types"
	"time"
)

type ViewassessmentrecordBase struct {
}

func (s ViewassessmentrecordBase) TableName() string {
	return "ViewAssessmentRecord"
}

func (s ViewassessmentrecordBase) SetFilter(v interface{}) {

}

func (s *ViewassessmentrecordBase) Clone() (interface{}, error) {
	return &ViewassessmentrecordBase{}, nil
}

// VIEW
type Viewassessmentrecord struct {
	ViewassessmentrecordBase
	// 序号 主键，自增 例如:324
	Serialno uint64 `sql:"SerialNo"`
	// 用户ID 关联患者用户登录表用户ID 例如:232442
	Patientid uint64 `sql:"PatientID"`
	// 评估时间  例如:2018-08-01 10:00:00
	Assessdatetime *time.Time `sql:"AssessDateTime"`
	// 评估摘要 摘要规则根据所用评估表不同而不同，一般此处填入评估结果。 例如:中危
	Summary string `sql:"Summary"`
	// 评估名称  例如:高血压危险分层评估
	Assessmentname string `sql:"AssessmentName"`
	// 管理机构代码 管理该患者的机构 例如:897798
	Orgcode *string `sql:"OrgCode"`
}

func (s *Viewassessmentrecord) Clone() (interface{}, error) {
	t := &Viewassessmentrecord{}

	buf := new(bytes.Buffer)
	enc := gob.NewEncoder(buf)
	dec := gob.NewDecoder(buf)
	err := enc.Encode(s)
	if err != nil {
		return nil, err
	}
	err = dec.Decode(t)
	if err != nil {
		return nil, err
	}

	return t, nil
}

func (s *Viewassessmentrecord) CopyTo(target *doctor.Viewassessmentrecord) {
	if target == nil {
		return
	}
	target.Serialno = s.Serialno
	target.Patientid = s.Patientid
	if s.Assessdatetime == nil {
		target.Assessdatetime = nil
	} else {
		assessdatetime := types.Time(*s.Assessdatetime)
		target.Assessdatetime = &assessdatetime
	}
	target.Summary = s.Summary
	target.Assessmentname = s.Assessmentname
	if s.Orgcode != nil {
		target.Orgcode = string(*s.Orgcode)
	}
}

func (s *Viewassessmentrecord) CopyFrom(source *doctor.Viewassessmentrecord) {
	if source == nil {
		return
	}
	s.Serialno = source.Serialno
	s.Patientid = source.Patientid
	if source.Assessdatetime == nil {
		s.Assessdatetime = nil
	} else {
		assessdatetime := time.Time(*source.Assessdatetime)
		s.Assessdatetime = &assessdatetime
	}
	s.Summary = source.Summary
	s.Assessmentname = source.Assessmentname
	orgcode := string(source.Orgcode)
	s.Orgcode = &orgcode
}
