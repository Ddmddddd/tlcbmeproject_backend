package sqldb

type TlcPatientToRecKnowledgeBase struct {
}

func (s TlcPatientToRecKnowledgeBase) TableName() string {
	return "tlcheedurecommendknw"
}

type TlcPatientToRecKnowledge struct {
	TlcPatientToRecKnowledgeBase
	Id           uint64 `sql:"id" auto:"true" primary:"true"`
	UserID       uint64 `sql:"UserID"`
	Knowledge_id uint64 `sql:"knowledge_id"`
	Dayindex     uint64 `sql:"dayindex"`
	Status       uint64 `sql:"status"`
	Stars        uint64 `sql:"stars"`
}
