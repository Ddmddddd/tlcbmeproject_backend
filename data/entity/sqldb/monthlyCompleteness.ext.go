package sqldb

type MonthlyCompletenessFilter struct {
	MonthlyCompletenessBase

	// 用户ID 关联患者用户登录表用户ID 例如:232442
	PatientId uint64 `sql:"PatientId"`
	// 月份 年份+月份（2016-09） 例如:2016-09
	YearMonth string `sql:"YearMonth"`
}
