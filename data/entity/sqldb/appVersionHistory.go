package sqldb

import (
	"tlcbme_project/data/model/doctor"
	"github.com/ktpswjz/httpserver/types"
	"time"
)

type AppVersionHistoryBase struct {
}

func (s AppVersionHistoryBase) TableName() string {
	return "AppVersionHistory"
}

// 平台运维
// 患者APP配置
// APP版本历史记录表
// 注释：本表记录APP的版本更新记录
type AppVersionHistory struct {
	AppVersionHistoryBase
	// 序号 例如:1
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 版本代码 例如:1
	VersionCode uint64 `sql:"VersionCode"`
	// 版本名称 例如:1.0.0.1
	VersionName string `sql:"VersionName"`
	// 更新时间  例如:2018-07-06 14:55:00
	UpdateDate *time.Time `sql:"UpdateDate"`
	// 更新内容 例如:修改Bug
	UpdateContent string `sql:"UpdateContent"`
	// 是否强制更新 0-否，1-强制更新 例如:0
	IsForced uint64 `sql:"IsForced"`
	// 最小兼容版本代码 例如:1
	CompMinVersion uint64 `sql:"CompMinVersion"`
}

func (s *AppVersionHistory) CopyTo(target *doctor.AppVersionHistory) {
	if target == nil {
		return
	}
	target.SerialNo = s.SerialNo
	target.VersionCode = s.VersionCode
	target.VersionName = s.VersionName
	if s.UpdateDate == nil {
		target.UpdateDate = nil
	} else {
		updateDate := types.Time(*s.UpdateDate)
		target.UpdateDate = &updateDate
	}
	target.UpdateContent = s.UpdateContent
	target.IsForced = s.IsForced
	target.CompMinVersion = s.CompMinVersion
}

func (s *AppVersionHistory) CopyFrom(source *doctor.AppVersionHistory) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.VersionCode = source.VersionCode
	s.VersionName = source.VersionName
	if source.UpdateDate == nil {
		s.UpdateDate = nil
	} else {
		updateDate := time.Time(*source.UpdateDate)
		s.UpdateDate = &updateDate
	}
	s.UpdateContent = source.UpdateContent
	s.IsForced = source.IsForced
	s.CompMinVersion = source.CompMinVersion
}
