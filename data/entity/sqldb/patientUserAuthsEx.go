package sqldb

import "tlcbme_project/data/model/doctor"

type PatientUserAuthsExBase struct {
}

func (s PatientUserAuthsExBase) TableName() string {
	return "PatientUserAuthsEx"
}

// 平台运维
// 平台用户管理
// 患者用户登录扩展表
// 注释：该表保存患者用户第三方登录账号，例如微信号、QQ号方式登录。
type PatientUserAuthsEx struct {
	PatientUserAuthsExBase
	// 序号 主键，自增 例如:324
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 用户ID 关联患者用户登录表用户ID 例如:232442
	UserID uint64 `sql:"UserID"`
	// 账号类型 微信、QQ、微博 例如:微信
	IdentityType string `sql:"IdentityType"`
	// 账号 例如微信unionID 例如:32424243
	Identitier string `sql:"Identitier"`
	// 凭证 例如access_token 例如:3rf3242445fewfw34345f54
	Credential *string `sql:"Credential"`
	// 状态 0-正常，1-禁用 例如:0
	Status *string `sql:"Status"`
}

func (s *PatientUserAuthsEx) CopyTo(target *doctor.PatientUserAuthsEx) {
	if target == nil {
		return
	}
	target.SerialNo = s.SerialNo
	target.UserID = s.UserID
	target.IdentityType = s.IdentityType
	target.Identitier = s.Identitier
	if s.Credential != nil {
		target.Credential = string(*s.Credential)
	}
	if s.Status != nil {
		target.Status = string(*s.Status)
	}
}

func (s *PatientUserAuthsEx) CopyFrom(source *doctor.PatientUserAuthsEx) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.UserID = source.UserID
	s.IdentityType = source.IdentityType
	s.Identitier = source.Identitier
	credential := string(source.Credential)
	s.Credential = &credential
	status := string(source.Status)
	s.Status = &status
}
