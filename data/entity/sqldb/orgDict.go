package sqldb

import "tlcbme_project/data/model/doctor"

type OrgDictBase struct {
}

func (s OrgDictBase) TableName() string {
	return "OrgDict"
}

// 平台运维
// 基础字典
// 机构字典
// 注释：本表保存机构信息
type OrgDict struct {
	OrgDictBase
	// 序号 主键，自增 例如:324
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 机构代码 组织机构代码 例如:2894782947239239
	OrgCode string `sql:"OrgCode"`
	// 机构名称  例如:第一人民医院
	OrgName string `sql:"OrgName"`
	// 机构LOGO 例如:
	OrgLogo []byte `sql:"OrgLogo"`
	// 机构类别 对应机构类别字典的SmallTypeCode 例如:23232
	OrgType *string `sql:"OrgType"`
	// 医院等级 0-未知级别; 1-一级丙等; 2-一级乙等; 3-一级甲等; 4-二级丙等; 5-二级乙等; 6-二级甲等; 7-三级丙等; 8-三级乙等; 9-三级甲等;  例如:9
	HospitalClass *uint64 `sql:"HospitalClass"`
	// 医院专业分类 0-综合医院，1-专科医院，3-诊所 例如:0
	HospitalClass2 *uint64 `sql:"HospitalClass2"`
	// 医院所有制分类 0-公办医院，1-民营医院，2-私人医院 例如:0
	HospitalClass3 *uint64 `sql:"HospitalClass3"`
	// 医院所属地区级别 0-省级，1-市级，2-区县，3-乡，4-村室 例如:0
	HospitalClass4 *uint64 `sql:"HospitalClass4"`
	// 上级机构代码 如无上级机构，该字段为空；如有上级机构，字段中填写上级机构的OrgCode。 例如:232342
	ParentOrgCode *string `sql:"ParentOrgCode"`
	// 行政区划 例如:2332232
	DivisionCode *string `sql:"DivisionCode"`
	// 地址 例如:杭州市余杭区文一西路998号
	Address *string `sql:"Address"`
	// 联系人姓名 例如:张三
	ContactName *string `sql:"ContactName"`
	// 联系电话 例如:057187654321
	ContactPhone *string `sql:"ContactPhone"`
	// 负责人姓名 例如:李四
	ResponsiblePersonName *string `sql:"ResponsiblePersonName"`
	// 备注 例如:
	Memo *string `sql:"Memo"`
	// 排序 用于字典项目排序，从1开始往后排 例如:1
	ItemSortValue *uint64 `sql:"ItemSortValue"`
	// 输入码 用于快速输入，可以是拼音首字母或缩写 例如:gxy
	InputCode *string `sql:"InputCode"`
	// 有效性 用于描述本记录是否有效，0-无效，1-有效，默认值为1 例如:1
	IsValid *uint64 `sql:"IsValid"`
}

func (s *OrgDict) CopyTo(target *doctor.OrgDict) {
	if target == nil {
		return
	}
	target.SerialNo = s.SerialNo
	target.OrgCode = s.OrgCode
	target.OrgName = s.OrgName
	target.OrgLogo = string(s.OrgLogo)
	if s.OrgType != nil {
		target.OrgType = string(*s.OrgType)
	}
	target.HospitalClass = s.HospitalClass
	target.HospitalClass2 = s.HospitalClass2
	target.HospitalClass3 = s.HospitalClass3
	target.HospitalClass4 = s.HospitalClass4
	if s.ParentOrgCode != nil {
		target.ParentOrgCode = string(*s.ParentOrgCode)
	}
	if s.DivisionCode != nil {
		target.DivisionCode = string(*s.DivisionCode)
	}
	if s.Address != nil {
		target.Address = string(*s.Address)
	}
	if s.ContactName != nil {
		target.ContactName = string(*s.ContactName)
	}
	if s.ContactPhone != nil {
		target.ContactPhone = string(*s.ContactPhone)
	}
	if s.ResponsiblePersonName != nil {
		target.ResponsiblePersonName = string(*s.ResponsiblePersonName)
	}
	if s.Memo != nil {
		target.Memo = string(*s.Memo)
	}
	target.ItemSortValue = s.ItemSortValue
	if s.InputCode != nil {
		target.InputCode = string(*s.InputCode)
	}
	target.IsValid = s.IsValid
}

func (s *OrgDict) CopyFrom(source *doctor.OrgDict) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.OrgCode = source.OrgCode
	s.OrgName = source.OrgName
	s.OrgLogo = []byte(source.OrgLogo)
	orgType := string(source.OrgType)
	s.OrgType = &orgType
	s.HospitalClass = source.HospitalClass
	s.HospitalClass2 = source.HospitalClass2
	s.HospitalClass3 = source.HospitalClass3
	s.HospitalClass4 = source.HospitalClass4
	parentOrgCode := string(source.ParentOrgCode)
	s.ParentOrgCode = &parentOrgCode
	divisionCode := string(source.DivisionCode)
	s.DivisionCode = &divisionCode
	address := string(source.Address)
	s.Address = &address
	contactName := string(source.ContactName)
	s.ContactName = &contactName
	contactPhone := string(source.ContactPhone)
	s.ContactPhone = &contactPhone
	responsiblePersonName := string(source.ResponsiblePersonName)
	s.ResponsiblePersonName = &responsiblePersonName
	memo := string(source.Memo)
	s.Memo = &memo
	s.ItemSortValue = source.ItemSortValue
	inputCode := string(source.InputCode)
	s.InputCode = &inputCode
	s.IsValid = source.IsValid
}
