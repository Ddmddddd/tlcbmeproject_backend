package sqldb

import (
	"github.com/ktpswjz/httpserver/types"
	"time"
	"tlcbme_project/data/model/doctor"
)

type ExpPatientDietProgramBase struct {
}

func (s ExpPatientDietProgramBase) TableName() string {
	return "ExpPatientDietProgram"
}

type ExpPatientDietProgram struct {
	ExpPatientDietProgramBase
	//
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	//
	PatientID *uint64 `sql:"PatientID"`
	//
	MealTime *string `sql:"MealTime"`
	//
	Recipe *string `sql:"Recipe"`
	//
	Food *string `sql:"Food"`
	//
	Quantity *string `sql:"Quantity"`
	//
	Estimate *string `sql:"Estimate"`
	//
	Memo *string `sql:"Memo"`
	//
	UpdateDate *time.Time `sql:"UpdateDate"`
}

func (s *ExpPatientDietProgram) CopyTo(target *doctor.ExpPatientDietProgram) {
	if target == nil {
		return
	}
	target.SerialNo = s.SerialNo
	target.PatientID = s.PatientID
	if s.MealTime != nil {
		target.MealTime = string(*s.MealTime)
	}
	if s.Recipe != nil {
		target.Recipe = string(*s.Recipe)
	}
	if s.Food != nil {
		target.Food = string(*s.Food)
	}
	if s.Quantity != nil {
		target.Quantity = string(*s.Quantity)
	}
	if s.Estimate != nil {
		target.Estimate = string(*s.Estimate)
	}
	if s.Memo != nil {
		target.Memo = string(*s.Memo)
	}
	if s.UpdateDate == nil {
		target.UpdateDate = nil
	} else {
		updateDate := types.Time(*s.UpdateDate)
		target.UpdateDate = &updateDate
	}
}

func (s *ExpPatientDietProgram) CopyFrom(source *doctor.ExpPatientDietProgram) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.PatientID = source.PatientID
	mealTime := string(source.MealTime)
	s.MealTime = &mealTime
	recipe := string(source.Recipe)
	s.Recipe = &recipe
	food := string(source.Food)
	s.Food = &food
	quantity := string(source.Quantity)
	s.Quantity = &quantity
	estimate := string(source.Estimate)
	s.Estimate = &estimate
	memo := string(source.Memo)
	s.Memo = &memo
	if source.UpdateDate == nil {
		s.UpdateDate = nil
	} else {
		updateDate := time.Time(*source.UpdateDate)
		s.UpdateDate = &updateDate
	}
}
