package sqldb

import "time"

type TlcTeamInfoBase struct {
}

func (s TlcTeamInfoBase) TableName() string {
	return "tlcteaminfo"
}

type TlcTeamInfo struct {
	TlcTeamInfoBase
	SerialNo       uint64     `sql:"SerialNo"`
	TeamNickName   string     `sql:"TeamNickName"`
	BelongToGroup  string     `sql:"BelongToGroup"`
	MemNumUp       uint64     `sql:"MemNumUp"`
	TeamGenerateID string     `sql:"TeamGenerateID"`
	CreatorID      uint64     `sql:"CreatorID"`
	CreateTime     *time.Time `sql:"CreateTime"`
}
