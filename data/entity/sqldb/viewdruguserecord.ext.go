package sqldb

import "time"

type ViewdruguserecordFilter struct {
	// 用药时间 用药的实际时间 例如:2018-07-03 14:45:00
	Usedatetime *time.Time `sql:"UseDateTime" filter:"<"`
	// 管理机构代码 管理该患者的机构 例如:897798
	Orgcode *string `sql:"OrgCode"`
}
