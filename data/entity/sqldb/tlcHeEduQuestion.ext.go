package sqldb

type TlcHeEduQuestionFilterByQuestionId struct {
	TlcHeEduQuestionBase
	Id uint64 `sql:"id"`
}

type TlcHeEduQuestionFilterByKnwId struct {
	TlcHeEduQuestionBase
	Knowledge_id uint64 `sql:"knowledge_id"`
}
