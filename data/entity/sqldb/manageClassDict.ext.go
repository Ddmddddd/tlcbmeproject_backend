package sqldb

import (
	"tlcbme_project/data/model/doctor"
	"fmt"
)

func (s *ManageClassDict) CopyToCodeItem(target *doctor.DictCodeItem) {
	if target == nil {
		return
	}

	target.Code = s.ItemCode
	target.Name = s.ItemName
}

type ManageClassDictCodeItemFilter struct {
	PatientFeatureDictBase
	DictCodeItemFilter
}

func (s *ManageClassDictCodeItemFilter) CopyFrom(source *doctor.DictCodeItemFilter) {
	if source == nil {
		return
	}

	if len(source.InputCode) > 0 {
		s.InputCode = fmt.Sprint("%", source.InputCode, "%")
	}
	s.IsValid = 1
}
