package sqldb

import (
	"tlcbme_project/data/model/doctor"
	"github.com/ktpswjz/httpserver/types"
	"time"
)

type DrugUseRecordIndexBase struct {
}

func (s DrugUseRecordIndexBase) TableName() string {
	return "DrugUseRecordIndex"
}

// 慢病管理业务
// 患者自我管理数据
// 用药记录索引
// 注释：该表保存所有患者的用药记录索引
type DrugUseRecordIndex struct {
	DrugUseRecordIndexBase
	// 序号 主键，自增 例如:324
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 用户ID 关联患者用户登录表用户ID 例如:232442
	PatientID uint64 `sql:"PatientID"`
	// 药品名称  例如:氨氯地平片
	DrugName string `sql:"DrugName"`
	// 剂量 例如:5mg
	Dosage *string `sql:"Dosage"`
	// 频次 例如:1次/日
	Freq *string `sql:"Freq"`
	// 状态 0-使用中，1-已停用 例如:0
	Status uint64 `sql:"Status"`
	// 首次使用时间 例如:2018-01-01 08:50:00
	FirstUseDateTime *time.Time `sql:"FirstUseDateTime"`
	// 最近开始使用时间 例如:2018-01-01 08:50:00
	LastUseDateTime *time.Time `sql:"LastUseDateTime"`
	// 这条记录的创建者 如果是医生录入的，这里填医生姓名；如果是系统整理出来的，这里为空。 例如:张三
	CreateBy *string `sql:"CreateBy"`
	// 是否调整过用法 0-未调整过，1-调整过 例如:0
	IsModified uint64 `sql:"IsModified"`
	// 最后一次调整用法的时间 该字段只有当IsModified为1时才有意义 例如:2018-08-08 13:30:33
	LastModifyDateTime *time.Time `sql:"LastModifyDateTime"`
}

func (s *DrugUseRecordIndex) CopyTo(target *doctor.DrugUseRecordIndex) {
	if target == nil {
		return
	}
	target.SerialNo = s.SerialNo
	target.PatientID = s.PatientID
	target.DrugName = s.DrugName
	if s.Dosage != nil {
		target.Dosage = string(*s.Dosage)
	}
	if s.Freq != nil {
		target.Freq = string(*s.Freq)
	}
	target.Status = s.Status
	if s.FirstUseDateTime == nil {
		target.FirstUseDateTime = nil
	} else {
		firstUseDateTime := types.Time(*s.FirstUseDateTime)
		target.FirstUseDateTime = &firstUseDateTime
	}
	if s.LastUseDateTime == nil {
		target.LastUseDateTime = nil
	} else {
		lastUseDateTime := types.Time(*s.LastUseDateTime)
		target.LastUseDateTime = &lastUseDateTime
	}
	if s.CreateBy != nil {
		target.CreateBy = string(*s.CreateBy)
	}
	target.IsModified = s.IsModified
	if s.LastModifyDateTime == nil {
		target.LastModifyDateTime = nil
	} else {
		lastModifyDateTime := types.Time(*s.LastModifyDateTime)
		target.LastModifyDateTime = &lastModifyDateTime
	}
}

func (s *DrugUseRecordIndex) CopyFrom(source *doctor.DrugUseRecordIndex) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.PatientID = source.PatientID
	s.DrugName = source.DrugName
	dosage := string(source.Dosage)
	s.Dosage = &dosage
	freq := string(source.Freq)
	s.Freq = &freq
	s.Status = source.Status
	if source.FirstUseDateTime == nil {
		s.FirstUseDateTime = nil
	} else {
		firstUseDateTime := time.Time(*source.FirstUseDateTime)
		s.FirstUseDateTime = &firstUseDateTime
	}
	if source.LastUseDateTime == nil {
		s.LastUseDateTime = nil
	} else {
		lastUseDateTime := time.Time(*source.LastUseDateTime)
		s.LastUseDateTime = &lastUseDateTime
	}
	createBy := string(source.CreateBy)
	s.CreateBy = &createBy
	s.IsModified = source.IsModified
	if source.LastModifyDateTime == nil {
		s.LastModifyDateTime = nil
	} else {
		lastModifyDateTime := time.Time(*source.LastModifyDateTime)
		s.LastModifyDateTime = &lastModifyDateTime
	}
}
