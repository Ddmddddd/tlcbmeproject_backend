package sqldb

import "time"

type ViewfollowuprecordFilter struct {
	// 随访类型  例如:三个月例行随访
	Followuptype string `sql:"FollowupType"`
	// 管理机构代码 管理该患者的机构 例如:897798
	Orgcode *string `sql:"OrgCode"`
	// 随访时间  例如:2018-08-01 10:00:00
	FollowupDateTime *time.Time `sql:"FollowupDateTime" filter:"<"`
}
