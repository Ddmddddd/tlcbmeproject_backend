package sqldb

import (
	"github.com/ktpswjz/httpserver/types"
	"time"
	"tlcbme_project/data/model/doctor"
)

type ExpPatientNutritionalIndicatorBase struct {
}

func (s ExpPatientNutritionalIndicatorBase) TableName() string {
	return "ExpPatientNutritionalIndicator"
}

// 患者营养指标
type ExpPatientNutritionalIndicator struct {
	ExpPatientNutritionalIndicatorBase
	// 序号\n324\nFCM\n主键，自增
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 患者id
	UserID uint64 `sql:"UserID"`
	// 目标热量值 单位千卡
	GoalEnergy          *float64 `sql:"GoalEnergy"`
	GoalCarbohydrateMin *float64 `sql:"GoalCarbohydrateMin"`
	GoalCarbohydrateMax *float64 `sql:"GoalCarbohydrateMax"`
	GoalFatMin          *float64 `sql:"GoalFatMin"`
	GoalFatMax          *float64 `sql:"GoalFatMax"`
	GoalProteinMin      *float64 `sql:"GoalProteinMin"`
	GoalProteinMax      *float64 `sql:"GoalProteinMax"`
	GoalWeight			*float64 `sql:"GoalWeight"`
	GoalProtein			*float64 `sql:"GoalProtein"`
	GoalDF 				*float64 `sql:"GoalDF"`
	DietProgramID			*uint64 `sql:"DietProgramID"`
	UpdateTime			*time.Time `sql:"UpdateTime"`
}

func (s *ExpPatientNutritionalIndicator) CopyTo(target *doctor.ExpPatientNutritionalIndicator) {
	if target == nil {
		return
	}
	target.SerialNo = s.SerialNo
	target.UserID = s.UserID
	target.GoalEnergy = s.GoalEnergy
	target.GoalCarbohydrateMax = s.GoalCarbohydrateMax
	target.GoalCarbohydrateMin = s.GoalCarbohydrateMin
	target.GoalFatMax = s.GoalFatMax
	target.GoalFatMin = s.GoalFatMin
	target.GoalProteinMax = s.GoalProteinMax
	target.GoalProteinMin = s.GoalProteinMin
	target.GoalWeight = s.GoalWeight
	target.GoalProtein = s.GoalProtein
	target.GoalDF = s.GoalDF
	target.DietProgramID = s.DietProgramID
	if s.UpdateTime == nil {
		target.UpdateTime = nil
	} else {
		dateTime := types.Time(*s.UpdateTime)
		target.UpdateTime = &dateTime
	}
}

func (s *ExpPatientNutritionalIndicator) CopyFrom(source *doctor.ExpPatientNutritionalIndicator) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.UserID = source.UserID
	s.GoalEnergy = source.GoalEnergy
	s.GoalCarbohydrateMax = source.GoalCarbohydrateMax
	s.GoalCarbohydrateMin = source.GoalCarbohydrateMin
	s.GoalFatMax = source.GoalFatMax
	s.GoalFatMin = source.GoalFatMin
	s.GoalProteinMax = source.GoalProteinMax
	s.GoalProteinMin = source.GoalProteinMin
	s.GoalWeight = source.GoalWeight
	s.GoalProtein = source.GoalProtein
	s.GoalDF = source.GoalDF
	s.DietProgramID = source.DietProgramID
	if source.UpdateTime == nil {
		s.UpdateTime = nil
	} else {
		dateTime := time.Time(*source.UpdateTime)
		s.UpdateTime = &dateTime
	}
}

func (s *ExpPatientNutritionalIndicator) CopyFromApp(source *doctor.ExpPatientNutritionalIndicator) {
	if source == nil {
		return
	}
	s.UserID = source.UserID
	s.GoalEnergy = source.GoalEnergy
	s.GoalCarbohydrateMax = source.GoalCarbohydrateMax
	s.GoalCarbohydrateMin = source.GoalCarbohydrateMin
	s.GoalFatMax = source.GoalFatMax
	s.GoalFatMin = source.GoalFatMin
	s.GoalProteinMax = source.GoalProteinMax
	s.GoalProteinMin = source.GoalProteinMin
}
