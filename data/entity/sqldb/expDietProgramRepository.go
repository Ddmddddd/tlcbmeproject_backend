package sqldb

import "tlcbme_project/data/model/doctor"

type ExpDietProgramRepositoryBase struct {
}

func (s ExpDietProgramRepositoryBase) TableName() string {
	return "ExpDietProgramRepository"
}

type ExpDietProgramRepository struct {
	ExpDietProgramRepositoryBase
	// 序列号
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 方案ID
	ProgramID *uint64 `sql:"ProgramID"`
	// 餐次
	MealTime *string `sql:"MealTime"`
	// 菜品
	Recipe *string `sql:"Recipe"`
	// 组成食物
	Food *string `sql:"Food"`
	// 组成食物份量
	Quantity *string `sql:"Quantity"`
	// 组成食物估计份量
	Estimate *string `sql:"Estimate"`
	// 组成食物备注
	Memo *string `sql:"Memo"`
	// 方案标记目标能量
	Energy *float64 `sql:"Energy"`
}

func (s *ExpDietProgramRepository) CopyTo(target *doctor.ExpDietProgramRepository) {
	if target == nil {
		return
	}
	target.SerialNo = s.SerialNo
	target.ProgramID = s.ProgramID
	if s.MealTime != nil {
		target.MealTime = string(*s.MealTime)
	}
	if s.Recipe != nil {
		target.Recipe = string(*s.Recipe)
	}
	if s.Food != nil {
		target.Food = string(*s.Food)
	}
	if s.Quantity != nil {
		target.Quantity = string(*s.Quantity)
	}
	if s.Estimate != nil {
		target.Estimate = string(*s.Estimate)
	}
	if s.Memo != nil {
		target.Memo = string(*s.Memo)
	}
	target.Energy = s.Energy
}

func (s *ExpDietProgramRepository) CopyFrom(source *doctor.ExpDietProgramRepository) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.ProgramID = source.ProgramID
	mealTime := string(source.MealTime)
	s.MealTime = &mealTime
	recipe := string(source.Recipe)
	s.Recipe = &recipe
	food := string(source.Food)
	s.Food = &food
	quantity := string(source.Quantity)
	s.Quantity = &quantity
	estimate := string(source.Estimate)
	s.Estimate = &estimate
	memo := string(source.Memo)
	s.Memo = &memo
	s.Energy = source.Energy
}
