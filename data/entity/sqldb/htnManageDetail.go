package sqldb

import (
	"tlcbme_project/data/model/doctor"
	"github.com/ktpswjz/httpserver/types"
	"time"
)

type HtnManageDetailBase struct {
}

func (s HtnManageDetailBase) TableName() string {
	return "HtnManageDetail"
}

// 慢病管理业务
// 工作平台管理数据
// 高血压管理详情
// 注释：本表保存高血压管理的详情
type HtnManageDetail struct {
	HtnManageDetailBase
	// 序号 主键，自增 例如:324
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 用户ID 关联患者用户登录表用户ID 例如:232442
	PatientID uint64 `sql:"PatientID"`
	// 管理等级 当前管理等级，0-新患者，1-一级管理，2-二级管理，9-终止管理 例如:0
	ManageLevel uint64 `sql:"ManageLevel"`
	// 管理等级开始时间 当前管理等级开始时间 例如:2018-07-03 13:15:00
	ManageLevelStartDateTime *time.Time `sql:"ManageLevelStartDateTime"`
}

func (s *HtnManageDetail) CopyTo(target *doctor.HtnManageDetail) {
	if target == nil {
		return
	}
	target.SerialNo = s.SerialNo
	target.PatientID = s.PatientID
	target.ManageLevel = s.ManageLevel
	if s.ManageLevelStartDateTime == nil {
		target.ManageLevelStartDateTime = nil
	} else {
		manageLevelStartDateTime := types.Time(*s.ManageLevelStartDateTime)
		target.ManageLevelStartDateTime = &manageLevelStartDateTime
	}
}

func (s *HtnManageDetail) CopyFrom(source *doctor.HtnManageDetail) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.PatientID = source.PatientID
	s.ManageLevel = source.ManageLevel
	if source.ManageLevelStartDateTime == nil {
		s.ManageLevelStartDateTime = nil
	} else {
		manageLevelStartDateTime := time.Time(*source.ManageLevelStartDateTime)
		s.ManageLevelStartDateTime = &manageLevelStartDateTime
	}
}
