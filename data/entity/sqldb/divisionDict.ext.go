package sqldb

import (
	"tlcbme_project/data/model/doctor"
	"fmt"
)

type DivisionBaseInfo struct {
	DivisionDictBase

	// 行政区划代码
	ItemCode string `sql:"ItemCode"`
	// 行政区划名称
	ItemName string `sql:"ItemName"`
	// 行政区划全称
	FullName string `sql:"FullName"`
}

func (s *DivisionBaseInfo) CopyTo(target *doctor.DivisionBaseInfo) {
	if target == nil {
		return
	}
	target.ItemCode = s.ItemCode
	target.ItemName = s.ItemName
	target.FullName = s.FullName
}

func (s *DivisionDict) CopyToTree(target *doctor.DivisionDictTreeItem) {
	if target == nil {
		return
	}
	target.SerialNo = s.SerialNo
	target.ItemCode = s.ItemCode
	target.ItemName = s.ItemName
	target.FullName = s.FullName
	if s.ParentDivisionCode != nil {
		target.ParentDivisionCode = string(*s.ParentDivisionCode)
	}
	if s.IsValid != nil {
		if *s.IsValid == 0 {
			target.Disabled = true
		}
	}
}

type DivisionDictFilter struct {
	DivisionDictBase
	// 序号 主键，自增 例如:324
	SerialNo *uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 行政区划代码
	ItemCode string `sql:"ItemCode"`
	// 行政区划全称
	FullName string `sql:"FullName"`
	// 上级行政区划代码
	ParentDivisionCode *string `sql:"ParentDivisionCode"`
}

type DivisionDictOrder struct {
	DivisionDictBase

	// 行政区划代码
	ItemCode string `sql:"ItemCode" index:"2"`

	// 排序 用于字典项目排序，从1开始往后排 例如:1
	ItemSortValue *uint64 `sql:"ItemSortValue" index:"1"`
}

func (s *DivisionDictFilter) CopyFrom(source *doctor.DivisionDictFilter) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.ItemCode = source.ItemCode
	if len(source.FullName) > 0 {
		s.FullName = fmt.Sprint("%", source.FullName, "%")
	}
}

type DivisionDictTree struct {
	items      map[string]*doctor.DivisionDictTree
	itemsAdmin map[string]*doctor.DivisionDictTreeAdmin
	datas      []interface{}
}

func (s *DivisionDictTree) Init() {
	s.items = make(map[string]*doctor.DivisionDictTree)
	s.itemsAdmin = make(map[string]*doctor.DivisionDictTreeAdmin)
	s.datas = make([]interface{}, 0)
}

func (s *DivisionDictTree) Append(item *DivisionDict) {
	if item == nil {
		return
	}
	data := &doctor.DivisionDictTree{}
	item.CopyToTree(&data.DivisionDictTreeItem)
	data.Children = make([]*doctor.DivisionDictTree, 0)

	s.items[data.ItemCode] = data
	if item.ParentDivisionCode == nil {
		s.datas = append(s.datas, data)
	} else {
		parent, ok := s.items[*item.ParentDivisionCode]
		if ok {
			parent.Children = append(parent.Children, data)
		} else {
			s.datas = append(s.datas, data)
		}
	}
}

func (s *DivisionDictTree) AppendAdmin(item *DivisionDict) {
	if item == nil {
		return
	}
	data := &doctor.DivisionDictTreeAdmin{}
	item.CopyToTree(&data.DivisionDictTreeItem)
	data.Children = make([]*doctor.DivisionDictTreeAdmin, 0)

	s.itemsAdmin[data.ItemCode] = data
	if item.ParentDivisionCode == nil {
		s.datas = append(s.datas, data)
	} else {
		parent, ok := s.itemsAdmin[*item.ParentDivisionCode]
		if ok {
			parent.Children = append(parent.Children, data)
		} else {
			s.datas = append(s.datas, data)
		}
	}
}

func (s *DivisionDictTree) Data() []interface{} {
	return s.datas
}
