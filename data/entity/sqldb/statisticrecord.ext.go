package sqldb

type StatisticrecordFilter struct {
	// 统计类型，1-管理人数，2-活跃人数，3-血压数据，4-预警血压数据
	Type uint64 `sql:"Type"`
	// 年
	Year uint64 `sql:"Year"`
	// 月
	Month uint64 `sql:"Month"`
	//
	Orgcode string `sql:"OrgCode"`
}

type StatisticrecordExtFilter struct {
	// 年
	Year uint64 `sql:"Year"`
	// 月
	Month uint64 `sql:"Month"`
	//
	Orgcode string `sql:"OrgCode"`
}

type StatisticrecordAllFilter struct {
	// 统计类型，1-管理人数，2-活跃人数，3-血压数据，4-预警血压数据
	Type uint64 `sql:"Type"`
	//
	Orgcode string `sql:"OrgCode"`
}
