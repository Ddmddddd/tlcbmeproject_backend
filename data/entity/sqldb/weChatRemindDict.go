package sqldb

import (
	"tlcbme_project/data/model/doctor"
	"encoding/json"
)

type WeChatRemindDictBase struct {
}

func (s WeChatRemindDictBase) TableName() string {
	return "WeChatRemindDict"
}

type WeChatRemindDict struct {
	WeChatRemindDictBase
	// 序号
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 对应微信订阅消息的template_id
	TemplateID string `sql:"TemplateID"`
	// 点击卡片后的跳转页面，例如：index?foo=bar
	Page *string `sql:"Page"`
	// 模板内容，格式如：{ "key1": { "value": any }, "key2": { "value": any } }
	Data string `sql:"Data"`
	// 进入小程序的语言类型，支持zh_CN(简体中文)、en_US(英文)、zh_HK(繁体中文)、zh_TW(繁体中文)，默认为zh_CN
	Lang *string `sql:"Lang"`
	// 跳转小程序类型：developer为开发版；trial为体验版；formal为正式版；默认为正式版
	MiniprogramState *string `sql:"MiniprogramState"`
}

func (s *WeChatRemindDict) CopyTo(target *doctor.WeChatRemindDict) {
	if target == nil {
		return
	}
	target.SerialNo = s.SerialNo
	target.TemplateID = s.TemplateID
	if s.Page != nil {
		target.Page = string(*s.Page)
	}
	data := s.Data
	if len(data) > 0 {
		json.Unmarshal([]byte(data), &target.Data)
	}

	if s.Lang != nil {
		target.Lang = string(*s.Lang)
	}
	if s.MiniprogramState != nil {
		target.MiniprogramState = string(*s.MiniprogramState)
	}
}

func (s *WeChatRemindDict) CopyFrom(source *doctor.WeChatRemindDict) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.TemplateID = source.TemplateID
	page := string(source.Page)
	s.Page = &page
	if source.Data != nil {
		dataData, err := json.Marshal(source.Data)
		if err == nil {
			data := string(dataData)
			s.Data = data
		}
	}

	lang := string(source.Lang)
	s.Lang = &lang
	miniprogramState := string(source.MiniprogramState)
	s.MiniprogramState = &miniprogramState
}
