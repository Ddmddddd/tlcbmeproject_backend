package sqldb

import "time"

type ExpPatientTaskListStatusFilter struct {
	ExpPatientTaskListBase
	// 状态，0-正常，1-弃用，2-执行完毕
	Status uint64 `sql:"Status"`
	// 患者ID
	PatientID uint64 `sql:"PatientID"`
}

type ExpPatientTaskListDayIndexUpdate struct {
	ExpPatientTaskListBase
	// 序号
	SerialNo uint64 `sql:"SerialNo" primary:"true"`
	// 任务组，存放数组
	TaskList string `sql:"TaskList"`
	// 进行到的时间
	DayIndex uint64 `sql:"DayIndex"`
	// 更新时间
	UpdateTime *time.Time `sql:"UpdateTime"`
}

type ExpPatientTaskListSno struct {
	ExpPatientTaskListBase
	// 序号
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
}
