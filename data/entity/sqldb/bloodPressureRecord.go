package sqldb

import (
	"tlcbme_project/data/model/doctor"
	"github.com/ktpswjz/httpserver/types"
	"time"
)

type BloodPressureRecordBase struct {
}

func (s BloodPressureRecordBase) TableName() string {
	return "BloodPressureRecord"
}

// 慢病管理业务
// 患者自我管理数据
// 血压记录表
// 注释：本表保存患者的血压记录。
type BloodPressureRecord struct {
	BloodPressureRecordBase
	// 序号 主键，自增 例如:324
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 用户ID 关联患者用户登录表用户ID 例如:232442
	PatientID uint64 `sql:"PatientID"`
	// 收缩压 单位：mmHg 例如:110
	SystolicPressure uint64 `sql:"SystolicPressure"`
	// 舒张压 单位：mmHg 例如:78
	DiastolicPressure uint64 `sql:"DiastolicPressure"`
	// 心率 单位：次/分钟 例如:65
	HeartRate *uint64 `sql:"HeartRate"`
	// 测量部位 0-未知，1-左上臂，2-左手腕，3-右上臂，4-右手腕 例如:0
	MeasureBodyPart *uint64 `sql:"MeasureBodyPart"`
	// 备注 用于说明测量时的情况，例如有无服药，药品名称和剂量 例如:服用硝苯地平控释片2小时后
	Memo *string `sql:"Memo"`
	// 测量场所 0-未知，1-家里，2-医院，3-药店，9-其他场所 例如:0
	MeasurePlace *uint64 `sql:"MeasurePlace"`
	// 时间点 早、中、晚 例如:早
	TimePoint *string `sql:"TimePoint"`
	// 测量时间 测量血压的时间 例如:2018-07-03 14:45:00
	MeasureDateTime *time.Time `sql:"MeasureDateTime"`
	// 录入时间 将血压值录入系统的时间 例如:2018-07-03 14:00:00
	InputDateTime *time.Time `sql:"InputDateTime"`
}

func (s *BloodPressureRecord) CopyTo(target *doctor.BloodPressureRecord) {
	if target == nil {
		return
	}
	target.SerialNo = s.SerialNo
	target.PatientID = s.PatientID
	target.SystolicPressure = s.SystolicPressure
	target.DiastolicPressure = s.DiastolicPressure
	target.HeartRate = s.HeartRate
	target.MeasureBodyPart = s.MeasureBodyPart
	if s.Memo != nil {
		target.Memo = string(*s.Memo)
	}
	target.MeasurePlace = s.MeasurePlace
	if s.TimePoint != nil {
		target.TimePoint = string(*s.TimePoint)
	}
	if s.MeasureDateTime == nil {
		target.MeasureDateTime = nil
	} else {
		measureDateTime := types.Time(*s.MeasureDateTime)
		target.MeasureDateTime = &measureDateTime
	}
	if s.InputDateTime == nil {
		target.InputDateTime = nil
	} else {
		inputDateTime := types.Time(*s.InputDateTime)
		target.InputDateTime = &inputDateTime
	}
}

func (s *BloodPressureRecord) CopyFrom(source *doctor.BloodPressureRecord) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.PatientID = source.PatientID
	s.SystolicPressure = source.SystolicPressure
	s.DiastolicPressure = source.DiastolicPressure
	s.HeartRate = source.HeartRate
	s.MeasureBodyPart = source.MeasureBodyPart
	memo := string(source.Memo)
	s.Memo = &memo
	s.MeasurePlace = source.MeasurePlace
	timePoint := string(source.TimePoint)
	s.TimePoint = &timePoint
	if source.MeasureDateTime == nil {
		s.MeasureDateTime = nil
	} else {
		measureDateTime := time.Time(*source.MeasureDateTime)
		s.MeasureDateTime = &measureDateTime
	}
	if source.InputDateTime == nil {
		s.InputDateTime = nil
	} else {
		inputDateTime := time.Time(*source.InputDateTime)
		s.InputDateTime = &inputDateTime
	}
}
