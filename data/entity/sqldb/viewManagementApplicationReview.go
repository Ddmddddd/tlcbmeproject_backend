package sqldb

import (
	"tlcbme_project/data/model/doctor"
	"github.com/ktpswjz/httpserver/types"
	"time"
)

type ViewManagementApplicationReviewBase struct {
}

func (s ViewManagementApplicationReviewBase) TableName() string {
	return "ViewManagementApplicationReview"
}

// VIEW
type ViewManagementApplicationReview struct {
	ViewManagementApplicationReviewBase
	// 序号 主键，自增 例如:324
	SerialNo uint64 `sql:"SerialNo"`
	// 用户ID 关联患者用户登录表用户ID 例如:232442
	PatientID uint64 `sql:"PatientID"`
	// 姓名  例如:张三
	PatientName *string `sql:"PatientName"`
	// 来源 表示从哪里发起的申请，其值可以为：APP、微信小程序 例如:APP
	ApplicationFrom *string `sql:"ApplicationFrom"`
	// 注册时间  例如:2018-07-02 15:00:00
	RegistDateTime *time.Time `sql:"RegistDateTime"`
	// 诊断 例如:高血压，糖尿病
	Diagnosis *string `sql:"Diagnosis"`
	// 诊断备注  例如:血压 150/90 mmHg，血糖 7.9 mmol/L
	DiagnosisMemo *string `sql:"DiagnosisMemo"`
	// 就诊机构代码 例如:123
	VisitOrgCode *string `sql:"VisitOrgCode"`
	// 机构内就诊号 例如:123456
	OrgVisitID *string `sql:"OrgVisitID"`
	// 健康管理师ID 患者注册时指定的健康管理师 例如:12432
	HealthManagerID *uint64 `sql:"HealthManagerID"`
	// 审核状态 0-未审核，1-审核通过，2-审核不通过，3-忽略。 例如:0
	Status uint64 `sql:"Status"`
	// 审核人ID 审核人对应的用户ID 例如:32423
	ReviewerID *uint64 `sql:"ReviewerID"`
	// 审核人姓名 例如:张三
	ReviewerName *string `sql:"ReviewerName"`
	// 审核时间 例如:2018-07-03 13:00:00
	ReviewDateTime *time.Time `sql:"ReviewDateTime"`
	// 审核不通过原因 最近一次审核不通过的原因。只有当状态为2时有效。 例如:信息不够完善
	RefuseReason *string `sql:"RefuseReason"`
	// 性别 0-未知, 1-男, 2-女，9-未说明 例如:1
	Sex *uint64 `sql:"Sex"`
	// 出生日期 例如:2000-12-12
	DateOfBirth *time.Time `sql:"DateOfBirth"`
	// 身份证号 例如:330106200012129876
	IdentityCardNumber *string `sql:"IdentityCardNumber"`
	// 国籍 GB/T 2659-2000 世界各国和地区名称 例如:中国
	Country *string `sql:"Country"`
	// 民族 GB 3304-1991 中国各民族名称 例如:汉族
	Nation *string `sql:"Nation"`
	// 籍贯  例如:浙江杭州
	NativePlace *string `sql:"NativePlace"`
	// 婚姻状况 GB/T 2261.2-2003 例如:
	MarriageStatus *string `sql:"MarriageStatus"`
	// 文化程度 GB/T 4658-1984 文化程度 例如:大学
	EducationLevel *string `sql:"EducationLevel"`
	// 职业类别 GB/T 6565-1999 职业分类 例如:
	JobType *string `sql:"JobType"`
	// 昵称 例如:蓝精灵
	Nickname *string `sql:"Nickname"`
	// 个性签名 例如:在那山的那边海的那边有一群蓝精灵
	PersonalSign *string `sql:"PersonalSign"`
	// 联系电话 例如:13812344321
	Phone *string `sql:"Phone"`
	// 身高 单位：cm 例如:165
	Height *uint64 `sql:"Height"`
	// 体重 最后一次录入的体重，单位：kg 例如:60.5
	Weight *float64 `sql:"Weight"`
}

func (s *ViewManagementApplicationReview) CopyTo(target *doctor.ViewManagementApplicationReview) {
	if target == nil {
		return
	}
	target.SerialNo = s.SerialNo
	target.PatientID = s.PatientID
	if s.PatientName != nil {
		target.PatientName = string(*s.PatientName)
	}
	if s.ApplicationFrom != nil {
		target.ApplicationFrom = string(*s.ApplicationFrom)
	}
	if s.RegistDateTime == nil {
		target.RegistDateTime = nil
	} else {
		registDateTime := types.Time(*s.RegistDateTime)
		target.RegistDateTime = &registDateTime
	}
	if s.Diagnosis != nil {
		target.Diagnosis = string(*s.Diagnosis)
	}
	if s.DiagnosisMemo != nil {
		target.DiagnosisMemo = string(*s.DiagnosisMemo)
	}
	if s.VisitOrgCode != nil {
		target.VisitOrgCode = string(*s.VisitOrgCode)
	}
	if s.OrgVisitID != nil {
		target.OrgVisitID = string(*s.OrgVisitID)
	}
	target.HealthManagerID = s.HealthManagerID
	target.Status = s.Status
	target.ReviewerID = s.ReviewerID
	if s.ReviewerName != nil {
		target.ReviewerName = string(*s.ReviewerName)
	}
	if s.ReviewDateTime == nil {
		target.ReviewDateTime = nil
	} else {
		reviewDateTime := types.Time(*s.ReviewDateTime)
		target.ReviewDateTime = &reviewDateTime
	}
	if s.RefuseReason != nil {
		target.RefuseReason = string(*s.RefuseReason)
	}
	target.Sex = s.Sex
	if s.DateOfBirth == nil {
		target.DateOfBirth = nil
	} else {
		dateOfBirth := types.Time(*s.DateOfBirth)
		target.DateOfBirth = &dateOfBirth
	}
	if s.IdentityCardNumber != nil {
		target.IdentityCardNumber = string(*s.IdentityCardNumber)
	}
	if s.Country != nil {
		target.Country = string(*s.Country)
	}
	if s.Nation != nil {
		target.Nation = string(*s.Nation)
	}
	if s.NativePlace != nil {
		target.NativePlace = string(*s.NativePlace)
	}
	if s.MarriageStatus != nil {
		target.MarriageStatus = string(*s.MarriageStatus)
	}
	if s.EducationLevel != nil {
		target.EducationLevel = string(*s.EducationLevel)
	}
	if s.JobType != nil {
		target.JobType = string(*s.JobType)
	}
	if s.Nickname != nil {
		target.Nickname = string(*s.Nickname)
	}
	if s.PersonalSign != nil {
		target.PersonalSign = string(*s.PersonalSign)
	}
	if s.Phone != nil {
		target.Phone = string(*s.Phone)
	}
	target.Height = s.Height
	target.Weight = s.Weight
}

func (s *ViewManagementApplicationReview) CopyFrom(source *doctor.ViewManagementApplicationReview) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.PatientID = source.PatientID
	patientName := string(source.PatientName)
	s.PatientName = &patientName
	applicationFrom := string(source.ApplicationFrom)
	s.ApplicationFrom = &applicationFrom
	if source.RegistDateTime == nil {
		s.RegistDateTime = nil
	} else {
		registDateTime := time.Time(*source.RegistDateTime)
		s.RegistDateTime = &registDateTime
	}
	diagnosis := string(source.Diagnosis)
	s.Diagnosis = &diagnosis
	diagnosisMemo := string(source.DiagnosisMemo)
	s.DiagnosisMemo = &diagnosisMemo
	visitOrgCode := string(source.VisitOrgCode)
	s.VisitOrgCode = &visitOrgCode
	orgVisitID := string(source.OrgVisitID)
	s.OrgVisitID = &orgVisitID
	s.HealthManagerID = source.HealthManagerID
	s.Status = source.Status
	s.ReviewerID = source.ReviewerID
	reviewerName := string(source.ReviewerName)
	s.ReviewerName = &reviewerName
	if source.ReviewDateTime == nil {
		s.ReviewDateTime = nil
	} else {
		reviewDateTime := time.Time(*source.ReviewDateTime)
		s.ReviewDateTime = &reviewDateTime
	}
	refuseReason := string(source.RefuseReason)
	s.RefuseReason = &refuseReason
	s.Sex = source.Sex
	if source.DateOfBirth == nil {
		s.DateOfBirth = nil
	} else {
		dateOfBirth := time.Time(*source.DateOfBirth)
		s.DateOfBirth = &dateOfBirth
	}
	identityCardNumber := string(source.IdentityCardNumber)
	s.IdentityCardNumber = &identityCardNumber
	country := string(source.Country)
	s.Country = &country
	nation := string(source.Nation)
	s.Nation = &nation
	nativePlace := string(source.NativePlace)
	s.NativePlace = &nativePlace
	marriageStatus := string(source.MarriageStatus)
	s.MarriageStatus = &marriageStatus
	educationLevel := string(source.EducationLevel)
	s.EducationLevel = &educationLevel
	jobType := string(source.JobType)
	s.JobType = &jobType
	nickname := string(source.Nickname)
	s.Nickname = &nickname
	personalSign := string(source.PersonalSign)
	s.PersonalSign = &personalSign
	phone := string(source.Phone)
	s.Phone = &phone
	s.Height = source.Height
	s.Weight = source.Weight
}
