package sqldb

import "time"

type ViewBloodGlucoseFilter struct {
	// 管理机构代码 管理该患者的机构 例如:897798
	Orgcode *string `sql:"OrgCode"`
	// 测量时间 测量血压的时间 例如:2018-07-03 14:45:00
	Measuredatetime *time.Time `sql:"MeasureDateTime" filter:"<=" `
}

type ViewBloodGlucoseOrder struct {
	ViewbloodglucoserecordBase
	// 测量时间 测量血压的时间 例如:2018-07-03 14:45:00
	Measuredatetime *time.Time `sql:"MeasureDateTime" order:"DESC" `
}

type ViewBloodGlucoseQualifiedFilter struct {
	// 管理机构代码 管理该患者的机构 例如:897798
	Orgcode          *string `sql:"OrgCode"`
	BloodglucoseLow  float64 `sql:"BloodGlucose" filter:">"`
	BloodglucoseHigh float64 `sql:"BloodGlucose" filter:"<="`
	// 测量时间 测量血压的时间 例如:2018-07-03 14:45:00
	Measuredatetime *time.Time `sql:"MeasureDateTime" filter:"<="`
}
