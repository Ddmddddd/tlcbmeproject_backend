package sqldb

import "time"

type TlcTeamUserBase struct {
}

func (s TlcTeamUserBase) TableName() string {
	return "tlcteamuser"
}

type TlcTeamUser struct {
	TlcTeamUserBase
	SerialNo     uint64     `sql:"SerialNo"`
	UserID       uint64     `sql:"UserID"`
	TeamSerialNo uint64     `sql:"TeamSerialNo"`
	JoinTime     *time.Time `sql:"JoinTime"`
}
