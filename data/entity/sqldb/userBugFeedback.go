package sqldb

import (
	"github.com/ktpswjz/httpserver/types"
	"time"
	"tlcbme_project/data/model/doctor"
)

type UserBugFeedbackBase struct {
}

func (s UserBugFeedbackBase) TableName() string {
	return "userbugfeedback"
}

type UserBugFeedback struct {
	UserBugFeedbackBase
	//
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	//
	PatientID *uint64 `sql:"PatientID"`
	//
	Content *string `sql:"Content"`
	//
	Photo *string `sql:"Photo"`
	//
	DateTime *time.Time `sql:"DateTime"`
}

func (s *UserBugFeedback) CopyTo(target *doctor.UserBugFeedback) {
	if target == nil {
		return
	}
	target.SerialNo = s.SerialNo
	target.PatientID = s.PatientID
	if s.Content != nil {
		target.Content = string(*s.Content)
	}
	if s.Photo != nil {
		target.Photo = string(*s.Photo)
	}
	if s.DateTime == nil {
		target.DateTime = nil
	} else {
		dateTime := types.Time(*s.DateTime)
		target.DateTime = &dateTime
	}
}

func (s *UserBugFeedback) CopyFrom(source *doctor.UserBugFeedback) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.PatientID = source.PatientID
	content := string(source.Content)
	s.Content = &content
	photo := string(source.Photo)
	s.Photo = &photo
	if source.DateTime == nil {
		s.DateTime = nil
	} else {
		dateTime := time.Time(*source.DateTime)
		s.DateTime = &dateTime
	}
}
