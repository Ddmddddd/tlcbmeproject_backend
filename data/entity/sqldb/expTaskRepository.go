package sqldb

import (
	"tlcbme_project/data/model/doctor"
	"github.com/ktpswjz/httpserver/types"
	"time"
)

type ExpTaskRepositoryBase struct {
}

func (s ExpTaskRepositoryBase) TableName() string {
	return "ExpTaskRepository"
}

type ExpTaskRepository struct {
	ExpTaskRepositoryBase
	//
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 任务名称，包含周任务和日任务
	TaskName string `sql:"TaskName"`
	// 为空代表为周任务，否则代表日任务
	ParentTaskID uint64 `sql:"ParentTaskID"`
	// 为空代表周任务，否则代表日任务，该值为日任务的顺序，例如：1，代表该任务为第一天的任务
	TaskOrder uint64 `sql:"TaskOrder"`
	// 任务触发时间，为空则没有特定时间
	TaskTime *time.Time `sql:"TaskTime"`
	// 提醒任务，可选为“微信提醒”、“短信提醒”，为空则没有提醒任务
	RemindTask *string `sql:"RemindTask"`
	// 任务目标
	Target *string `sql:"Target"`
	// 编辑者id
	EditorID uint64 `sql:"EditorID"`
	// 创建时间
	CreateDateTime *time.Time `sql:"CreateDateTime"`
	// 0-正常，1-弃用
	Status uint64 `sql:"Status"`
	// 更新时间
	UpdateTime *time.Time `sql:"UpdateTime"`
}

func (s *ExpTaskRepository) CopyTo(target *doctor.ExpTaskRepository) {
	if target == nil {
		return
	}
	target.SerialNo = s.SerialNo
	target.TaskName = s.TaskName
	target.ParentTaskID = s.ParentTaskID
	target.TaskOrder = s.TaskOrder
	if s.TaskTime == nil {
		target.TaskTime = nil
	} else {
		taskTime := types.Time(*s.TaskTime)
		target.TaskTime = &taskTime
	}
	if s.RemindTask != nil {
		target.RemindTask = string(*s.RemindTask)
	}
	if s.Target != nil {
		target.Target = string(*s.Target)
	}
	target.EditorID = s.EditorID
	if s.CreateDateTime == nil {
		target.CreateDateTime = nil
	} else {
		createDateTime := types.Time(*s.CreateDateTime)
		target.CreateDateTime = &createDateTime
	}
	target.Status = s.Status
	if s.UpdateTime == nil {
		target.UpdateTime = nil
	} else {
		updateTime := types.Time(*s.UpdateTime)
		target.UpdateTime = &updateTime
	}
}

func (s *ExpTaskRepository) CopyFrom(source *doctor.ExpTaskRepository) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.TaskName = source.TaskName
	s.ParentTaskID = source.ParentTaskID
	s.TaskOrder = source.TaskOrder
	if source.TaskTime == nil {
		s.TaskTime = nil
	} else {
		taskTime := time.Time(*source.TaskTime)
		s.TaskTime = &taskTime
	}
	remindTask := string(source.RemindTask)
	s.RemindTask = &remindTask
	target := string(source.Target)
	s.Target = &target
	s.EditorID = source.EditorID
	if source.CreateDateTime == nil {
		s.CreateDateTime = nil
	} else {
		createDateTime := time.Time(*source.CreateDateTime)
		s.CreateDateTime = &createDateTime
	}
	s.Status = source.Status
	if source.UpdateTime == nil {
		s.UpdateTime = nil
	} else {
		updateTime := time.Time(*source.UpdateTime)
		s.UpdateTime = &updateTime
	}
}

func (s *ExpTaskRepository) CopyToTree(target *doctor.ExpTaskRepositoryTree) {
	if target == nil {
		return
	}
	target.SerialNo = s.SerialNo
	target.TaskName = s.TaskName
	target.TaskOrder = s.TaskOrder
	if s.TaskTime == nil {
		target.TaskTime = nil
	} else {
		taskTime := types.Time(*s.TaskTime)
		target.TaskTime = &taskTime
	}
	if s.RemindTask != nil {
		target.RemindTask = string(*s.RemindTask)
	}
	if s.Target != nil {
		target.Target = string(*s.Target)
	}
}

func (s *ExpTaskRepository) CopyFromAddInput(source *doctor.ExpTaskRepositoryAddInput) {
	if source == nil {
		return
	}
	s.TaskName = source.TaskName
	s.ParentTaskID = source.ParentTaskID
	s.TaskOrder = source.TaskOrder
	target := string(source.Target)
	if source.TaskTime == nil {
		s.TaskTime = nil
	} else {
		taskTime := time.Time(*source.TaskTime)
		s.TaskTime = &taskTime
	}
	remindTask := string(source.RemindTask)
	s.RemindTask = &remindTask
	s.Target = &target
	s.EditorID = source.EditorID
	if source.CreateDateTime == nil {
		s.CreateDateTime = nil
	} else {
		createDateTime := time.Time(*source.CreateDateTime)
		s.CreateDateTime = &createDateTime
	}
}

func (s *ExpTaskRepository) CopyFromDeleteInput(source *doctor.ExpTaskRepositoryDeleteInput) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.EditorID = source.EditorID
	s.Status = source.Status
	if source.UpdateTime == nil {
		s.UpdateTime = nil
	} else {
		updateTime := time.Time(*source.UpdateTime)
		s.UpdateTime = &updateTime
	}
}
