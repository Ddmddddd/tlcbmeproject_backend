package sqldb

import "tlcbme_project/data/model/doctor"

type SmsTemplateParamBase struct {
}

func (s SmsTemplateParamBase) TableName() string {
	return "SmsTemplateParam"
}

// 平台运维
// 短信
// 短信模板参数
// 注释：本表保存短信模板可以使用的参数
type SmsTemplateParam struct {
	SmsTemplateParamBase
	// 序号 主键，自增 例如:324
	SerialNo uint64 `sql:"SerialNo" primary:"true"`
	// 参数名称 参数名称不能重名 例如:复诊时间
	TemplateParamName string `sql:"TemplateParamName"`
	// 参数数据类型 0-文本，1-整数，3-日期，4-日期时间 例如:0
	ParamDataType uint64 `sql:"ParamDataType"`
	// 是否可编辑 0-不可编辑，使用后台给的值，例如姓名；1-允许编辑，例如复诊时间，用户可以编辑 例如:0
	Editable uint64 `sql:"Editable"`
	// 输入码 用于快速输入，可以是拼音首字母或缩写 例如:gxy
	InputCode *string `sql:"InputCode"`
	// 有效性 用于描述本记录是否有效，0-无效，1-有效，默认值为1 例如:1
	IsValid uint64 `sql:"IsValid"`
}

func (s *SmsTemplateParam) CopyTo(target *doctor.SmsTemplateParam) {
	if target == nil {
		return
	}
	target.SerialNo = s.SerialNo
	target.TemplateParamName = s.TemplateParamName
	target.ParamDataType = s.ParamDataType
	target.Editable = s.Editable
	if s.InputCode != nil {
		target.InputCode = string(*s.InputCode)
	}
	target.IsValid = s.IsValid
}

func (s *SmsTemplateParam) CopyFrom(source *doctor.SmsTemplateParam) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.TemplateParamName = source.TemplateParamName
	s.ParamDataType = source.ParamDataType
	s.Editable = source.Editable
	inputCode := string(source.InputCode)
	s.InputCode = &inputCode
	s.IsValid = source.IsValid
}
