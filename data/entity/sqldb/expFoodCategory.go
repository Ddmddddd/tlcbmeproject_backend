package sqldb

import "tlcbme_project/data/model/doctor"

type ExpFoodCategoryBase struct {
}

func (s ExpFoodCategoryBase) TableName() string {
	return "ExpFoodCategory"
}

// 食物分类表
type ExpFoodCategory struct {
	ExpFoodCategoryBase
	// 分类编号
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 名称
	Name string `sql:"Name"`
}

func (s *ExpFoodCategory) CopyTo(target *doctor.ExpFoodCategory) {
	if target == nil {
		return
	}
	target.SerialNo= s.SerialNo
	target.Name = s.Name
}

func (s *ExpFoodCategory) CopyFrom(source *doctor.ExpFoodCategory) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.Name = source.Name
}
