package sqldb

type ExpCommonScaleSerialNoFilter struct {
	ExpCommonScaleBase
	// 序号
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
}

type ExpCommonScaleSerialNoListFilter struct {
	ExpCommonScaleBase
	// 序号列表
	SerialNos []uint64 `sql:"SerialNo" filter:"in"`
}

type ExpCommonScaleTypeExclude struct {
	ExpCommonScaleBase
	// 量表属性，0-默认量表，仅对患者开放，1-普通量表，2-需要引擎生成生活计划的量表
	ScaleType uint64 `sql:"ScaleType" filter:"!="`
}