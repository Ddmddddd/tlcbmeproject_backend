package sqldb

type WeChatRemindDictFilter struct {
	WeChatRemindDictBase
	// 对应微信订阅消息的template_id
	TemplateID string `sql:"TemplateID"`
}