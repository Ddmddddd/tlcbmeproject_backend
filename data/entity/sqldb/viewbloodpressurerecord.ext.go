package sqldb

import "time"

type ViewBloodPressureFilter struct {
	// 管理机构代码 管理该患者的机构 例如:897798
	Orgcode *string `sql:"OrgCode"`
	// 测量时间 测量血压的时间 例如:2018-07-03 14:45:00
	Measuredatetime *time.Time `sql:"MeasureDateTime" filter:"<="`
}

type ViewBloodPressureOrder struct {
	ViewbloodpressurerecordBase
	// 测量时间 测量血压的时间 例如:2018-07-03 14:45:00
	Measuredatetime *time.Time `sql:"MeasureDateTime" order:"DESC" `
}

type ViewBloodPressureQualifiedFilter struct {
	// 管理机构代码 管理该患者的机构 例如:897798
	Orgcode *string `sql:"OrgCode"`
	// 收缩压 单位：mmHg 例如:110
	SystolicpressureLow  uint64 `sql:"SystolicPressure" filter:">="`
	SystolicpressureHigh uint64 `sql:"SystolicPressure" filter:"<="`
	// 舒张压 单位：mmHg 例如:78
	DiastolicpressureLow  uint64 `sql:"DiastolicPressure" filter:">="`
	DiastolicpressureHigh uint64 `sql:"DiastolicPressure" filter:"<="`

	// 测量时间 测量血压的时间 例如:2018-07-03 14:45:00
	Measuredatetime *time.Time `sql:"MeasureDateTime" filter:"<="`
}
