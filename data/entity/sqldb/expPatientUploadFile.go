package sqldb

import "tlcbme_project/data/model/doctor"

type ExpPatientUploadFileBase struct {
}

func (s ExpPatientUploadFileBase) TableName() string {
	return "ExpPatientUploadFile"
}

type ExpPatientUploadFile struct {
	ExpPatientUploadFileBase
	//
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 患者id
	PatientID uint64 `sql:"PatientID"`
	// 健康方案地址
	HealthReport *string `sql:"HealthReport"`
	// 健康方案文档名称
	HealthReportName *string `sql:"HealthReportName"`
	// 饮食处方地址
	DietPrescription *string `sql:"DietPrescription"`
	// 饮食处方文档名称
	DietPrescriptionName *string `sql:"DietPrescriptionName"`
	// 运动处方地址
	ExercisePrescription *string `sql:"ExercisePrescription"`
	// 运动处方地址
	ExercisePrescriptionName *string `sql:"ExercisePrescriptionName"`
}

func (s *ExpPatientUploadFile) CopyTo(target *doctor.ExpPatientUploadFile) {
	if target == nil {
		return
	}
	target.SerialNo = s.SerialNo
	target.PatientID = s.PatientID
	if s.HealthReport != nil {
		target.HealthReport = string(*s.HealthReport)
	}
	if s.HealthReportName != nil {
		target.HealthReportName = string(*s.HealthReportName)
	}
	if s.DietPrescription != nil {
		target.DietPrescription = string(*s.DietPrescription)
	}
	if s.DietPrescriptionName != nil {
		target.DietPrescriptionName = string(*s.DietPrescriptionName)
	}
	if s.ExercisePrescription != nil {
		target.ExercisePrescription = string(*s.ExercisePrescription)
	}
	if s.ExercisePrescriptionName != nil {
		target.ExercisePrescriptionName = string(*s.ExercisePrescriptionName)
	}
}

func (s *ExpPatientUploadFile) CopyFrom(source *doctor.ExpPatientUploadFile) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.PatientID = source.PatientID
	healthReport := string(source.HealthReport)
	s.HealthReport = &healthReport
	healthReportName := string(source.HealthReportName)
	s.HealthReportName = &healthReportName
	dietPrescription := string(source.DietPrescription)
	s.DietPrescription = &dietPrescription
	dietPrescriptionName := string(source.DietPrescriptionName)
	s.DietPrescriptionName = &dietPrescriptionName
	exercisePrescription := string(source.ExercisePrescription)
	s.ExercisePrescription = &exercisePrescription
	exercisePrescriptionName := string(source.ExercisePrescriptionName)
	s.ExercisePrescriptionName = &exercisePrescriptionName
}

func (s *ExpPatientUploadFile) CopyFromCreate(source *doctor.ExpPatientUploadFile) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.PatientID = source.PatientID
	if source.HealthReport != "" {
		healthReport := string(source.HealthReport)
		s.HealthReport = &healthReport
	}
	if source.HealthReportName != "" {
		healthReportName := string(source.HealthReportName)
		s.HealthReportName = &healthReportName
	}
	if source.DietPrescription != "" {
		dietPrescription := string(source.DietPrescription)
		s.DietPrescription = &dietPrescription
	}
	if source.DietPrescriptionName != "" {
		dietPrescriptionName := string(source.DietPrescriptionName)
		s.DietPrescriptionName = &dietPrescriptionName
	}
	if source.ExercisePrescription != "" {
		exercisePrescription := string(source.ExercisePrescription)
		s.ExercisePrescription = &exercisePrescription
	}
	if source.ExercisePrescriptionName != "" {
		exercisePrescriptionName := string(source.ExercisePrescriptionName)
		s.ExercisePrescriptionName = &exercisePrescriptionName
	}
}
