package sqldb

import "tlcbme_project/data/model/doctor"

type AssessmentSheetDictBase struct {
}

func (s AssessmentSheetDictBase) TableName() string {
	return "AssessmentSheetDict"
}

// 平台运维
// 基础字典
// 评估表字典
// 注释：该表保存评估表字典。
type AssessmentSheetDict struct {
	AssessmentSheetDictBase
	// 序号 主键，自增 例如:324
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 字典项代码  例如:1
	ItemCode uint64 `sql:"ItemCode"`
	// 字典项名称  例如:高血压危险分层评估
	ItemName string `sql:"ItemName"`
	// 评估表所属管理分类代码 表示当前模板属于哪类慢病，0或null表示通用。 例如:2323
	ManageClassCode uint64 `sql:"ManageClassCode"`
	// 输入码 用于快速输入，可以是拼音首字母或缩写 例如:gxy
	InputCode *string `sql:"InputCode"`
	// 有效性 用于描述本记录是否有效，0-无效，1-有效，默认值为1 例如:1
	IsValid uint64 `sql:"IsValid"`
}

func (s *AssessmentSheetDict) CopyTo(target *doctor.AssessmentSheetDict) {
	if target == nil {
		return
	}
	target.SerialNo = s.SerialNo
	target.ItemCode = s.ItemCode
	target.ItemName = s.ItemName
	target.ManageClassCode = s.ManageClassCode
	if s.InputCode != nil {
		target.InputCode = string(*s.InputCode)
	}
	target.IsValid = s.IsValid
}

func (s *AssessmentSheetDict) CopyFrom(source *doctor.AssessmentSheetDict) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.ItemCode = source.ItemCode
	s.ItemName = source.ItemName
	s.ManageClassCode = source.ManageClassCode
	inputCode := string(source.InputCode)
	s.InputCode = &inputCode
	s.IsValid = source.IsValid
}
