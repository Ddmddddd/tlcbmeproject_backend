package sqldb

import (
	"bytes"
	"tlcbme_project/data/model/doctor"
	"encoding/gob"
	"github.com/ktpswjz/httpserver/types"
	"time"
)

type ViewbloodpressurerecordBase struct {
}

func (s ViewbloodpressurerecordBase) TableName() string {
	return "ViewBloodPressureRecord"
}

func (s ViewbloodpressurerecordBase) SetFilter(v interface{}) {

}

func (s *ViewbloodpressurerecordBase) Clone() (interface{}, error) {
	return &ViewbloodpressurerecordBase{}, nil
}

// VIEW
type Viewbloodpressurerecord struct {
	ViewbloodpressurerecordBase
	// 序号 主键，自增 例如:324
	Serialno uint64 `sql:"SerialNo"`
	// 用户ID 关联患者用户登录表用户ID 例如:232442
	Patientid uint64 `sql:"PatientID"`
	// 收缩压 单位：mmHg 例如:110
	Systolicpressure uint64 `sql:"SystolicPressure"`
	// 舒张压 单位：mmHg 例如:78
	Diastolicpressure uint64 `sql:"DiastolicPressure"`
	// 测量时间 测量血压的时间 例如:2018-07-03 14:45:00
	Measuredatetime *time.Time `sql:"MeasureDateTime"`
	// 管理机构代码 管理该患者的机构 例如:897798
	Orgcode *string `sql:"OrgCode"`
}

func (s *Viewbloodpressurerecord) Clone() (interface{}, error) {
	t := &Viewbloodpressurerecord{}

	buf := new(bytes.Buffer)
	enc := gob.NewEncoder(buf)
	dec := gob.NewDecoder(buf)
	err := enc.Encode(s)
	if err != nil {
		return nil, err
	}
	err = dec.Decode(t)
	if err != nil {
		return nil, err
	}

	return t, nil
}

func (s *Viewbloodpressurerecord) CopyTo(target *doctor.Viewbloodpressurerecord) {
	if target == nil {
		return
	}
	target.Serialno = s.Serialno
	target.Patientid = s.Patientid
	target.Systolicpressure = s.Systolicpressure
	target.Diastolicpressure = s.Diastolicpressure
	if s.Measuredatetime == nil {
		target.Measuredatetime = nil
	} else {
		measuredatetime := types.Time(*s.Measuredatetime)
		target.Measuredatetime = &measuredatetime
	}
	if s.Orgcode != nil {
		target.Orgcode = string(*s.Orgcode)
	}
}

func (s *Viewbloodpressurerecord) CopyFrom(source *doctor.Viewbloodpressurerecord) {
	if source == nil {
		return
	}
	s.Serialno = source.Serialno
	s.Patientid = source.Patientid
	s.Systolicpressure = source.Systolicpressure
	s.Diastolicpressure = source.Diastolicpressure
	if source.Measuredatetime == nil {
		s.Measuredatetime = nil
	} else {
		measuredatetime := time.Time(*source.Measuredatetime)
		s.Measuredatetime = &measuredatetime
	}
	orgcode := string(source.Orgcode)
	s.Orgcode = &orgcode
}
