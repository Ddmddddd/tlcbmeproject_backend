package sqldb

import (
	"time"
)

type TlcHeEduPatientQuestionFilterByPatientID struct {
	TlcHeEduPatientQuestionBase
	UserID uint64 `sql:"UserID"`
}

type TlcHeEduPatientQuestionFilterByPatientAndKnwId struct {
	TlcHeEduPatientQuestionBase
	UserID       uint64 `sql:"UserID"`
	Knowledge_id uint64 `sql:"knowledge_id"`
}

type TlcHeEduPatientQuestionPreRequestFilter struct {
	TlcHeEduPatientQuestionBase
	Question_id uint64 `sql:"question_id"`
	UserID      uint64 `sql:"UserID"`
}

type TlcHeEduPatientGetCertainQuestion struct {
	TlcHeEduPatientQuestionBase
	Question_id  uint64 `sql:"question_id"`
	UserID       uint64 `sql:"UserID"`
	Knowledge_id uint64 `sql:"knowledge_id"`
}

type TlcHeEduPatientReceivedQuestion struct {
	TlcHeEduPatientQuestionBase
	Question_id uint64 `sql:"question_id"`
	Status      uint64 `sql:"status"`
}

type TlcHeEduPatientQuestionUpdate struct {
	TlcHeEduPatientQuestionBase
	Status      uint64     `sql:"status"`
	Answer_time *time.Time `sql:"answer_time"`
}

type TlcHeEduPatientQuestionRecordUpdateFilter struct {
	TlcHeEduPatientQuestionBase
	UserID      uint64 `sql:"UserID"`
	Question_id uint64 `sql:"question_id"`
}

type TlcHeEduPatientQuestionFilterByQuestionId struct {
	TlcHeEduPatientQuestionBase
	Question_id uint64 `sql:"question_id"`
}
