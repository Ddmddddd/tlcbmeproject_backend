package sqldb

import "tlcbme_project/data/model/doctor"

//这个文件中写一些过滤器，以及其他的一些函数

type PatientToTagUserIdFilter struct {
	PatientToTagBase
	UserID uint64 `sql:"UserID"`
}

type PatientToTagDeleteFilter struct {
	PatientToTagBase
	UserID uint64 `sql:"UserID"`
	Tag_id uint64 `sql:"tag_id"`
}

func (s *PatientToTagDeleteFilter) CopyFrom(source *doctor.PatientToTagDelete) {
	s.UserID = source.PatientID
	s.Tag_id = source.Tag_id
}
