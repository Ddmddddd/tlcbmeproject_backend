package sqldb

import (
	"time"
)

type CampListValidFilter struct {
	IsValid uint8 `sql:"IsValid"`
}

type CampListValidAndPrimaryKeyFilter struct {
	SerialNo uint64 `sql:"SerialNo"`
	IsValid uint8 `sql:"IsValid"`
}

type CampListPrimaryKeyFilter struct {
	SerialNo uint64 `sql:"SerialNo"`
}

type CampListUpdate struct {
	CampListBase
	Name string `sql:"Name"`
	Desc *string `sql:"Desc"`
	UpdateDateTime *time.Time `sql:"UpdateDateTime"`
}

type CampListName struct {
	CampListBase
	Name string `sql:"Name"`
}
