package sqldb

import (
	doctor2 "tlcbme_project/data/model/doctor"
)

type ExpExercisePlanBase struct {
}

func (s ExpExercisePlanBase) TableName() string {
	return "ExpExercisePlan"
}

type ExpExercisePlan struct {
	ExpExercisePlanBase
	//
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 患者id
	PatientID uint64 `sql:"PatientID"`
	// 训练计划
	ExercisePlan string `sql:"ExercisePlan"`
	// 完成次数
	Counts uint64 `sql:"Counts"`
}

func (s *ExpExercisePlan) CopyTo(target *doctor2.ExpExercisePlan) {
	if target == nil {
		return
	}
	target.SerialNo = s.SerialNo
	target.PatientID = s.PatientID
	target.ExercisePlan = s.ExercisePlan
	target.Counts = s.Counts
}

func (s *ExpExercisePlan) CopyFrom(source *doctor2.ExpExercisePlan) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.PatientID = source.PatientID
	s.ExercisePlan = source.ExercisePlan
	s.Counts = source.Counts
}
