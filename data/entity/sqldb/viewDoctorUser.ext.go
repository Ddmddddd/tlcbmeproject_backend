package sqldb

import (
	"tlcbme_project/data/enum"
	"tlcbme_project/data/model/doctor"
)

func (s *ViewDoctorUser) CopyToEx(target *doctor.ViewDoctorUserEx) {
	if target == nil {
		return
	}
	s.CopyTo(&target.ViewDoctorUser)

	target.StatusText = enum.AccountStatuses.Value(s.Status)
	if s.UserType != nil {
		target.UserTypeText = enum.DoctorUserTypes.Value(*s.UserType)
	}
	if s.VerifiedStatus != nil {
		target.VerifiedStatusText = enum.VerifiedStatuses.Value(*s.VerifiedStatus)
	}
}

type ViewDoctorUserFilter struct {
	ViewDoctorUserBase
	// 用户ID 主键，自增 例如:
	UserID uint64 `sql:"UserID"`
}

type ViewDoctorUserDict struct {
	ViewDoctorUserBase
	// 用户ID 主键，自增 例如:
	UserID uint64 `sql:"UserID"`
	// 姓名  例如:张三
	Name *string `sql:"Name"`
}

func (s *ViewDoctorUserDict) CopyTo(target *doctor.ViewDoctorUserDict) {
	if target == nil {
		return
	}

	target.UserID = s.UserID
	if s.Name != nil {
		target.Name = string(*s.Name)
	}
}

type ViewDoctorUserDictFilter struct {
	ViewDoctorUserBase

	// 用户类别 0-医生，1-健康管理师 例如:0
	UserType *uint64 `sql:"UserType"`
	// 执业机构代码 该医生用户所在的执业机构 例如:897798
	OrgCode *string `sql:"OrgCode"`
}

func (s *ViewDoctorUserDictFilter) CopyFrom(source *doctor.ViewDoctorUserDictFilter) {
	if source == nil {
		return
	}

	s.UserType = source.UserType
}
