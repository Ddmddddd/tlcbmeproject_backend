package sqldb

import (
	"tlcbme_project/data/model/doctor"
	"tlcbme_project/data/model/manage"
	"github.com/ktpswjz/httpserver/types"
	"time"
)

type ExpActionPlanBase struct {
}

func (s ExpActionPlanBase) TableName() string {
	return "ExpActionPlan"
}

// 患者行动计划表
// 最先由引擎生成，后续可由医生或患者进行调整修改
// 可由患者本人或其管理医师修改
// 修改方式为，将原生活计划status置为0，新增生活计划
type ExpActionPlan struct {
	ExpActionPlanBase
	// 序号
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 用户ID
	PatientID uint64 `sql:"PatientID"`
	// 行动计划标题
	Title string `sql:"Title"`
	// 行动计划
	ActionPlan string `sql:"ActionPlan"`
	// 计划类型
	Type string `sql:"Type"`
	// 计划描述
	Description string `sql:"Description"`
	// 状态，0-使用中，1-已取消
	Status uint64 `sql:"Status"`
	// 创建时间
	CreateDateTime *time.Time `sql:"CreateDateTime"`
	// 更新时间
	UpdateDateTime *time.Time `sql:"UpdateDateTime"`
	// 编辑者，0-引擎规则，1-医生，2-患者
	EditorType uint64 `sql:"EditorType"`
	// 优先级，分为0-5，数值越大优先级越高
	Level uint64 `sql:"Level"`
	// 对应引擎端生活计划的id，用于提交、修改等操作
	SeqActionPlan uint64 `sql:"SeqActionPlan"`
	// 记录次数
	RecordTimes uint64 `sql:"RecordTimes"`
	// 需求次数
	RequireTimes uint64 `sql:"RequireTimes"`
}

func (s *ExpActionPlan) CopyTo(target *doctor.ExpActionPlan) {
	if target == nil {
		return
	}
	target.SerialNo = s.SerialNo
	target.PatientID = s.PatientID
	target.Title = s.Title
	target.ActionPlan = s.ActionPlan
	target.Description = s.Description
	target.Type = s.Type
	target.Status = s.Status
	if s.CreateDateTime == nil {
		target.CreateDateTime = nil
	} else {
		createDateTime := types.Time(*s.CreateDateTime)
		target.CreateDateTime = &createDateTime
	}
	if s.UpdateDateTime == nil {
		target.UpdateDateTime = nil
	} else {
		updateDateTime := types.Time(*s.UpdateDateTime)
		target.UpdateDateTime = &updateDateTime
	}
	target.EditorType = s.EditorType
	target.Level = s.Level
	target.SeqActionPlan = s.SeqActionPlan
	target.RecordTimes = s.RecordTimes
	target.RequireTimes = s.RequireTimes
}

func (s *ExpActionPlan) CopyFrom(source *doctor.ExpActionPlan) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.PatientID = source.PatientID
	s.Title = source.Title
	s.ActionPlan = source.ActionPlan
	s.Description = source.Description
	s.Type = source.Type
	s.Status = source.Status
	if source.CreateDateTime == nil {
		s.CreateDateTime = nil
	} else {
		createDateTime := time.Time(*source.CreateDateTime)
		s.CreateDateTime = &createDateTime
	}
	if source.UpdateDateTime == nil {
		s.UpdateDateTime = nil
	} else {
		updateDateTime := time.Time(*source.UpdateDateTime)
		s.UpdateDateTime = &updateDateTime
	}
	s.EditorType = source.EditorType
	s.Level = source.Level
	s.SeqActionPlan = source.SeqActionPlan
	s.RecordTimes = source.RecordTimes
	s.RequireTimes = source.RequireTimes
}

func (s *ExpActionPlan) CopyFromManage(source *manage.InputDataActionPlan) {
	if source == nil {
		return
	}
	s.ActionPlan = source.ActionPlan
	s.Title = source.Title
	s.Level = source.Level
	s.SeqActionPlan = source.SeqActionPlan
	s.Description = source.Description
	s.Type = source.Type
}

func (s *ExpActionPlan) CopyFromCreate(source *doctor.ExpPatientActionPlanCreate) {
	if source == nil {
		return
	}
	s.PatientID = source.PatientID
	s.ActionPlan = source.ActionPlan
	if source.CreateDateTime == nil {
		s.CreateDateTime = nil
	} else {
		createDateTime := time.Time(*source.CreateDateTime)
		s.CreateDateTime = &createDateTime
	}
	s.EditorType = source.EditorType
	s.Level = source.Level
}