package sqldb

import (
	"tlcbme_project/data/model/doctor"
	"github.com/ktpswjz/httpserver/types"
	"time"
)

type ExpPatientBodyExamImageBase struct {
}

func (s ExpPatientBodyExamImageBase) TableName() string {
	return "ExpPatientBodyExamImage"
}

type ExpPatientBodyExamImage struct {
	ExpPatientBodyExamImageBase
	//
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	//
	PatientID uint64 `sql:"PatientID"`
	//
	ImgUrl string `sql:"ImgUrl"`
	//
	CreateDateTime *time.Time `sql:"CreateDateTime"`
}

func (s *ExpPatientBodyExamImage) CopyTo(target *doctor.ExpPatientBodyExamImage) {
	if target == nil {
		return
	}
	target.SerialNo = s.SerialNo
	target.PatientID = s.PatientID
	target.ImgUrl = s.ImgUrl
	if s.CreateDateTime == nil {
		target.CreateDateTime = nil
	} else {
		createDateTime := types.Time(*s.CreateDateTime)
		target.CreateDateTime = &createDateTime
	}
}

func (s *ExpPatientBodyExamImage) CopyFrom(source *doctor.ExpPatientBodyExamImage) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.PatientID = source.PatientID
	s.ImgUrl = source.ImgUrl
	if source.CreateDateTime == nil {
		s.CreateDateTime = nil
	} else {
		createDateTime := time.Time(*source.CreateDateTime)
		s.CreateDateTime = &createDateTime
	}
}

func (s *ExpPatientBodyExamImage) CopyFromCreate(source *doctor.ExpPatientBodyExamImageCreate) {
	if source == nil {
		return
	}
	s.PatientID = source.PatientID
	if source.CreateDateTime == nil {
		s.CreateDateTime = nil
	} else {
		createDateTime := time.Time(*source.CreateDateTime)
		s.CreateDateTime = &createDateTime
	}
}