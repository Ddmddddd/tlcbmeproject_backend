package sqldb

import "tlcbme_project/data/model/doctor"

type PatientCollectedKnowledgeFilterByUserID struct {
	PatientCollectedKnowledgeBase
	UserID uint64 `sql:"UserID"`
}

type PatientCollectedKnowledgeDeleteFilter struct {
	PatientCollectedKnowledgeBase
	UserID                 uint64 `sql:"UserID"`
	Collected_knowledge_id uint64 `sql:"collected_knowledge_id"`
}

func (s *PatientCollectedKnowledgeDeleteFilter) CopyFrom(source *doctor.PatientCollectedKnowledgeDelete) {
	if source == nil {
		return
	}
	s.UserID = source.PatientID
	s.Collected_knowledge_id = source.Collected_knowledge_id
}
