package sqldb

import (
	"github.com/ktpswjz/httpserver/types"
	"time"
	"tlcbme_project/data/model/doctor"
)

type BloodGlucoseRecordBase struct {
}

func (s BloodGlucoseRecordBase) TableName() string {
	return "BloodGlucoseRecord"
}

// 慢病管理业务
// 患者自我管理数据
// 血糖记录表
// 注释：本表保存患者的血糖记录。
type BloodGlucoseRecord struct {
	BloodGlucoseRecordBase
	// 序号 主键，自增 例如:324
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 用户ID 关联患者用户登录表用户ID 例如:232442
	PatientID uint64 `sql:"PatientID"`
	// 血样类型 毛细血管血、静脉血、动脉血、新生儿血 例如:毛细血管血
	BloodType *string `sql:"BloodType"`
	// 血糖值 单位：mmol/L 例如:6.1
	BloodGlucose float64 `sql:"BloodGlucose"`
	// 血糖值 单位：mmol/L 例如:6.1
	BloodKetone *float64 `sql:"BloodKetone"`
	// 测量时间点 空腹、早餐前、早餐后2h、午餐前、午餐后2h、晚餐前、晚餐后2h、睡前、夜间 例如:空腹
	TimePoint *string `sql:"TimePoint"`
	// 备注  例如:最近在口服降糖药
	Memo *string `sql:"Memo"`
	// 测量时间 测量血糖时的时间 例如:2018-07-03 14:45:00
	MeasureDateTime *time.Time `sql:"MeasureDateTime"`
	// 录入时间 将血糖值录入系统的时间 例如:2018-07-03 14:00:00
	InputDateTime *time.Time `sql:"InputDateTime"`
}

func (s *BloodGlucoseRecord) CopyTo(target *doctor.BloodGlucoseRecord) {
	if target == nil {
		return
	}
	target.SerialNo = s.SerialNo
	target.PatientID = s.PatientID
	if s.BloodType != nil {
		target.BloodType = string(*s.BloodType)
	}
	target.BloodGlucose = s.BloodGlucose
	target.BloodKetone = s.BloodKetone
	if s.TimePoint != nil {
		//target.TimePoint = string(*s.TimePoint)
		if *s.TimePoint == "0" {
			target.TimePoint = "晨起空腹"
		} else if *s.TimePoint == "1" {
			target.TimePoint = "早餐后"
		} else if *s.TimePoint == "2" {
			target.TimePoint = "午餐前"
		} else if *s.TimePoint == "3" {
			target.TimePoint = "午餐后"
		} else if *s.TimePoint == "4" {
			target.TimePoint = "晚餐前"
		} else if *s.TimePoint == "5" {
			target.TimePoint = "晚餐后"
		} else if *s.TimePoint == "6" {
			target.TimePoint = "睡前"
		}
	}
	if s.Memo != nil {
		target.Memo = string(*s.Memo)
	}
	if s.MeasureDateTime == nil {
		target.MeasureDateTime = nil
	} else {
		measureDateTime := types.Time(*s.MeasureDateTime)
		target.MeasureDateTime = &measureDateTime
	}
	if s.InputDateTime == nil {
		target.InputDateTime = nil
	} else {
		inputDateTime := types.Time(*s.InputDateTime)
		target.InputDateTime = &inputDateTime
	}
}

func (s *BloodGlucoseRecord) CopyFrom(source *doctor.BloodGlucoseRecord) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.PatientID = source.PatientID
	bloodType := string(source.BloodType)
	s.BloodType = &bloodType
	s.BloodGlucose = source.BloodGlucose
	timePoint := string(source.TimePoint)
	s.BloodKetone = source.BloodKetone
	s.TimePoint = &timePoint
	memo := string(source.Memo)
	s.Memo = &memo
	if source.MeasureDateTime == nil {
		s.MeasureDateTime = nil
	} else {
		measureDateTime := time.Time(*source.MeasureDateTime)
		s.MeasureDateTime = &measureDateTime
	}
	if source.InputDateTime == nil {
		s.InputDateTime = nil
	} else {
		inputDateTime := time.Time(*source.InputDateTime)
		s.InputDateTime = &inputDateTime
	}
}
