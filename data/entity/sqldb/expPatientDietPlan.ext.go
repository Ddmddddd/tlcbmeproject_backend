package sqldb

import (
	"github.com/ktpswjz/httpserver/types"
	"time"
	doctor2 "tlcbme_project/data/model/doctor"
)

type ExpPatientDietPlanFilter struct {
	ExpPatientDietPlanBase
	// 患者id
	PatientID uint64 `sql:"PatientID"`
}

type ExpPatientDietPlanGetFilter struct {
	ExpPatientDietPlanBase
	// 患者id
	PatientID uint64 `sql:"PatientID"`
	// 重要程度
	Level string `sql:"Level" filter:"!="`
}

type ExpPatientDietPlanLevelFilter struct {
	ExpPatientDietPlanBase
	// 患者id
	PatientID uint64 `sql:"PatientID"`
	// 重要程度
	Level string `sql:"Level" filter:"="`
}

type ExpPatientDietPlanChangeFilter struct {
	ExpPatientDietPlanBase
	// 患者id
	PatientID uint64 `sql:"PatientID"`
	// 改变状态，0-正常，1-被修改，需要患者查看，给予提示
	ChangeStatus uint64 `sql:"ChangeStatus"`
}

type ExpPatientDietPlanSno struct {
	ExpPatientDietPlanBase
	//
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
}

type ExpPatientDietPlanRead struct {
	ExpPatientDietPlanBase
	//
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 改变状态，0-正常，1-被修改，需要患者查看，给予提示
	ChangeStatus uint64 `sql:"ChangeStatus"`
}

type ExpPatientDietPlanFour struct {
	ExpPatientDietPlanBase
	//
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 患者id
	PatientID uint64 `sql:"PatientID"`
	// 饮食计划
	DietPlan string `sql:"DietPlan"`
	// 图片
	PicUrl string `sql:"PicUrl"`
	// 开始时间
	StartDate *time.Time `sql:"StartDate"`
}

func (s *ExpPatientDietPlanFour) CopyToFour(target *doctor2.ExpPatientDietPlanFour) {
	if target == nil {
		return
	}
	target.SerialNo = s.SerialNo
	target.PatientID = s.PatientID
	target.DietPlan = s.DietPlan
	target.PicUrl = s.PicUrl
	if s.StartDate == nil {
		target.StartDate = nil
	} else {
		startDate := types.Time(*s.StartDate)
		target.StartDate = &startDate
	}
}

type ExpPatientDietPlanAddItem struct {
	ExpPatientDietPlanBase
	// 患者id
	PatientID uint64 `sql:"PatientID"`
	// 饮食计划
	DietPlan string `sql:"DietPlan"`
	// 图片
	PicUrl string `sql:"PicUrl"`
	// 开始时间
	StartDate *time.Time `sql:"StartDate"`
	// 结束时间
	EndDate *time.Time `sql:"EndDate"`
	// 重要程度
	Level string `sql:"Level"`
	// 是否完成，0-未完成，1-已完成
	Complete uint64 `sql:"Complete"`
	// 改变状态，0-正常，1-被修改，需要患者查看，给予提示
	ChangeStatus uint64 `sql:"ChangeStatus"`
	// 创建时间
	CreateDateTime *time.Time `sql:"CreateDateTime"`
	// 更新时间
	UpdateDateTime *time.Time `sql:"UpdateDateTime"`
}

func (s *ExpPatientDietPlanAddItem) CopyFrom(source *doctor2.ExpPatientDietPlan) {
	if source == nil {
		return
	}
	s.PatientID = source.PatientID
	s.DietPlan = source.DietPlan
	if source.StartDate == nil {
		s.StartDate = nil
	} else {
		startDate := time.Time(*source.StartDate)
		s.StartDate = &startDate
	}
	if source.EndDate == nil {
		s.EndDate = nil
	} else {
		endDate := time.Time(*source.EndDate)
		s.EndDate = &endDate
	}
	s.Level = source.Level
	s.Complete = source.Complete
	s.ChangeStatus = 1
	now := time.Now()
	s.UpdateDateTime = &now
	s.CreateDateTime = &now
}
