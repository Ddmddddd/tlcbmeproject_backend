package sqldb

import (
	"tlcbme_project/data/model/doctor"
	"github.com/ktpswjz/httpserver/types"
	"time"
)

type ManagementApplicationReviewBase struct {
}

func (s ManagementApplicationReviewBase) TableName() string {
	return "ManagementApplicationReview"
}

// 慢病管理业务
// 工作平台管理数据
// 管理申请审核
// 注释：该表保存待审核的患者记录。审核通过后，将患者记录写入到管理患者索引表。
type ManagementApplicationReview struct {
	ManagementApplicationReviewBase
	// 序号 主键，自增 例如:324
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 用户ID 关联患者用户登录表用户ID 例如:232442
	PatientID uint64 `sql:"PatientID"`
	// 来源 表示从哪里发起的申请，其值可以为：APP、微信小程序 例如:APP
	ApplicationFrom *string `sql:"ApplicationFrom"`
	// 诊断 例如:高血压，糖尿病
	Diagnosis *string `sql:"Diagnosis"`
	// 诊断备注  例如:血压 150/90 mmHg，血糖 7.9 mmol/L
	DiagnosisMemo *string `sql:"DiagnosisMemo"`
	// 就诊机构代码 例如:123
	VisitOrgCode *string `sql:"VisitOrgCode"`
	// 机构内就诊号 例如:123456
	OrgVisitID *string `sql:"OrgVisitID"`
	// 健康管理师ID 患者注册时指定的健康管理师 例如:12432
	HealthManagerID *uint64 `sql:"HealthManagerID"`
	// 审核状态 0-未审核，1-审核通过，2-审核不通过，3-忽略。 例如:0
	Status uint64 `sql:"Status"`
	// 审核人ID 审核人对应的用户ID 例如:32423
	ReviewerID *uint64 `sql:"ReviewerID"`
	// 审核人姓名 例如:张三
	ReviewerName *string `sql:"ReviewerName"`
	// 审核时间 例如:2018-07-03 13:00:00
	ReviewDateTime *time.Time `sql:"ReviewDateTime"`
	// 审核不通过原因 最近一次审核不通过的原因。只有当状态为2时有效。 例如:信息不够完善
	RefuseReason *string `sql:"RefuseReason"`
}

func (s *ManagementApplicationReview) CopyTo(target *doctor.ManagementApplicationReview) {
	if target == nil {
		return
	}
	target.SerialNo = s.SerialNo
	target.PatientID = s.PatientID
	if s.ApplicationFrom != nil {
		target.ApplicationFrom = string(*s.ApplicationFrom)
	}
	if s.Diagnosis != nil {
		target.Diagnosis = string(*s.Diagnosis)
	}
	if s.DiagnosisMemo != nil {
		target.DiagnosisMemo = string(*s.DiagnosisMemo)
	}
	if s.VisitOrgCode != nil {
		target.VisitOrgCode = string(*s.VisitOrgCode)
	}
	if s.OrgVisitID != nil {
		target.OrgVisitID = string(*s.OrgVisitID)
	}
	target.HealthManagerID = s.HealthManagerID
	target.Status = s.Status
	target.ReviewerID = s.ReviewerID
	if s.ReviewerName != nil {
		target.ReviewerName = string(*s.ReviewerName)
	}
	if s.ReviewDateTime == nil {
		target.ReviewDateTime = nil
	} else {
		reviewDateTime := types.Time(*s.ReviewDateTime)
		target.ReviewDateTime = &reviewDateTime
	}
	if s.RefuseReason != nil {
		target.RefuseReason = string(*s.RefuseReason)
	}
}

func (s *ManagementApplicationReview) CopyFrom(source *doctor.ManagementApplicationReview) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.PatientID = source.PatientID
	applicationFrom := string(source.ApplicationFrom)
	s.ApplicationFrom = &applicationFrom
	diagnosis := string(source.Diagnosis)
	s.Diagnosis = &diagnosis
	diagnosisMemo := string(source.DiagnosisMemo)
	s.DiagnosisMemo = &diagnosisMemo
	visitOrgCode := string(source.VisitOrgCode)
	s.VisitOrgCode = &visitOrgCode
	orgVisitID := string(source.OrgVisitID)
	s.OrgVisitID = &orgVisitID
	s.HealthManagerID = source.HealthManagerID
	s.Status = source.Status
	s.ReviewerID = source.ReviewerID
	reviewerName := string(source.ReviewerName)
	s.ReviewerName = &reviewerName
	if source.ReviewDateTime == nil {
		s.ReviewDateTime = nil
	} else {
		reviewDateTime := time.Time(*source.ReviewDateTime)
		s.ReviewDateTime = &reviewDateTime
	}
	refuseReason := string(source.RefuseReason)
	s.RefuseReason = &refuseReason
}
