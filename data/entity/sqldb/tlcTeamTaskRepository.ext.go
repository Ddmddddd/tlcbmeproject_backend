package sqldb

type TlcTeamTaskRepositoryFilterBySno struct {
	TlcTeamTaskRepositoryBase
	SerialNo uint64 `sql:"SerialNo"`
}

type TlcTeamTaskRepositoryFilterByTaskName struct {
	TlcTeamTaskRepositoryBase
	TaskName string `sql:"TaskName"`
}
