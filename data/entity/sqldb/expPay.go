package sqldb

import (
	"encoding/json"
	"github.com/ktpswjz/httpserver/types"
	"time"
	doctor2 "tlcbme_project/data/model/doctor"
)

type ExpPayBase struct {
}

func (s ExpPayBase) TableName() string {
	return "ExpPay"
}

type ExpPay struct {
	ExpPayBase
	//
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	//
	PatientID uint64 `sql:"PatientID"`
	//
	OpenId string `sql:"OpenId"`
	//
	Amount float64 `sql:"Amount"`
	//
	PartnerTradeNo string `sql:"ParternerTradeNo"`
	//
	Desc *string `sql:"Desc"`
	//
	Result string `sql:"Result"`
	//
	CreateDateTime *time.Time `sql:"CreateDateTime"`
}

func (s *ExpPay) CopyTo(target *doctor2.ExpPay) {
	if target == nil {
		return
	}
	target.SerialNo = s.SerialNo
	target.PatientID = s.PatientID
	target.OpenId = s.OpenId
	target.Amount = s.Amount
	target.PartnerTradeNo = s.PartnerTradeNo
	if s.Desc != nil {
		target.Desc = string(*s.Desc)
	}
	result := s.Result
	if len(result) > 0 {
		json.Unmarshal([]byte(result), &target.Result)
	}

	if s.CreateDateTime == nil {
		target.CreateDateTime = nil
	} else {
		createDateTime := types.Time(*s.CreateDateTime)
		target.CreateDateTime = &createDateTime
	}
}

func (s *ExpPay) CopyFrom(source *doctor2.ExpPay) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.PatientID = source.PatientID
	s.OpenId = source.OpenId
	s.Amount = source.Amount
	s.PartnerTradeNo = source.PartnerTradeNo
	desc := string(source.Desc)
	s.Desc = &desc
	if source.Result != nil {
		resultData, err := json.Marshal(source.Result)
		if err == nil {
			result := string(resultData)
			s.Result = result
		}
	}

	if source.CreateDateTime == nil {
		s.CreateDateTime = nil
	} else {
		createDateTime := time.Time(*source.CreateDateTime)
		s.CreateDateTime = &createDateTime
	}
}
