package sqldb

import (
	"github.com/ktpswjz/httpserver/types"
	"time"
	"tlcbme_project/data/model/doctor"
)

type BloodGlucoseRecordSerialNoFilter struct {
	BloodGlucoseRecordBase

	// 序号 主键，自增 例如:324
	SerialNo *uint64 `sql:"SerialNo"`
}

func (s *BloodGlucoseRecord) CopyToEx(target *doctor.BloodGlucoseRecordEx) {
	if target == nil {
		return
	}
	s.CopyTo(&target.BloodGlucoseRecord)
}

func (s *BloodGlucoseRecord) CopyToTrend(target *doctor.BloodGlucoseRecordTrend) {
	if target == nil {
		return
	}

	if s.BloodType != nil {
		target.BloodType = string(*s.BloodType)
	}
	target.BloodGlucose = s.BloodGlucose
	if s.TimePoint != nil {
		if *s.TimePoint == "0" {
			target.TimePoint = "晨起空腹"
		} else if *s.TimePoint == "1" {
			target.TimePoint = "早餐后"
		} else if *s.TimePoint == "2" {
			target.TimePoint = "午餐前"
		} else if *s.TimePoint == "3" {
			target.TimePoint = "午餐后"
		} else if *s.TimePoint == "4" {
			target.TimePoint = "晚餐前"
		} else if *s.TimePoint == "5" {
			target.TimePoint = "晚餐后"
		} else if *s.TimePoint == "6" {
			target.TimePoint = "睡前"
		}
		//target.TimePoint = string(*s.TimePoint)
	}
	if s.MeasureDateTime == nil {
		target.MeasureDateTime = nil
	} else {
		measureDateTime := types.Time(*s.MeasureDateTime)
		target.MeasureDateTime = &measureDateTime
	}
}

func (s *BloodGlucoseRecord) CopyFromCreate(source *doctor.BloodGlucoseRecordCreate) {
	if source == nil {
		return
	}
	bloodType := string(source.BloodType)
	s.BloodType = &bloodType
	s.BloodGlucose = source.BloodGlucose
	timePoint := string(source.TimePoint)
	s.BloodKetone = source.BloodKetone
	s.TimePoint = &timePoint
	memo := string(source.Memo)
	s.Memo = &memo
	if source.MeasureDateTime == nil {
		s.MeasureDateTime = nil
	} else {
		measureDateTime := time.Time(*source.MeasureDateTime)
		s.MeasureDateTime = &measureDateTime
	}
}

func (s *BloodGlucoseRecord) CopyFromCreateEx(source *doctor.BloodGlucoseRecordCreateEx) {
	if source == nil {
		return
	}
	s.CopyFromCreate(&source.BloodGlucoseRecordCreate)
	s.PatientID = source.PatientID
}

type BloodGlucoseRecordOrder struct {
	BloodGlucoseRecordBase

	// 测量时间 测量血糖时的时间 例如:2018-07-03 14:45:00
	MeasureDateTime *time.Time `sql:"MeasureDateTime"`
}

type BloodGlucoseRecordDownOrder struct {
	BloodGlucoseRecordBase

	// 测量时间 测量血糖时的时间 例如:2018-07-03 14:45:00
	MeasureDateTime *time.Time `sql:"MeasureDateTime" order:"DESC"`
}

type BloodGlucoseRecordFilter struct {
	BloodGlucoseRecordBase
	// 序号 主键，自增 例如:324
	SerialNo *uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 用户ID 关联患者用户登录表用户ID 例如:232442
	PatientID *uint64 `sql:"PatientID"`
	// 血样类型 毛细血管血、静脉血、动脉血、新生儿血 例如:毛细血管血
	BloodType *string `sql:"BloodType"`
	// 测量时间点 空腹、早餐前、早餐后2h、午餐前、午餐后2h、晚餐前、晚餐后2h、睡前、夜间 例如:空腹
	TimePoints []*string `sql:"TimePoint" filter:"in"`
	// 测量时间 测量血糖时的时间 例如:2018-07-03 14:45:00
	MeasureDateStart *time.Time `sql:"MeasureDateTime" filter:">="`
	MeasureDateEnd   *time.Time `sql:"MeasureDateTime" filter:"<"`
}

func (s *BloodGlucoseRecordFilter) CopyFrom(source *doctor.BloodGlucoseRecordFilter) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.PatientID = source.PatientID
	s.BloodType = source.BloodType
	if len(source.TimePoints) > 0 {
		s.TimePoints = source.TimePoints
	}
	if source.MeasureDateStart != nil {
		s.MeasureDateStart = source.MeasureDateStart.ToDate(0)
	}
	if source.MeasureDateEnd != nil {
		s.MeasureDateEnd = source.MeasureDateEnd.ToDate(1)
	}
}

type BloodGlucoseRecordDataFilter struct {
	BloodGlucoseRecordBase

	// 用户ID 关联患者用户登录表用户ID 例如:232442
	PatientID uint64 `sql:"PatientID"`

	// 测量时间 测量血糖的时间 例如:2018-07-03 14:45:00
	MeasureStartDate *time.Time `sql:"MeasureDateTime" filter:">="`
	MeasureEndDate   *time.Time `sql:"MeasureDateTime" filter:"<"`
}

func (s *BloodGlucoseRecordDataFilter) CopyFrom(source *doctor.BloodGlucoseRecordDataFilterEx) {
	if source == nil {
		return
	}

	s.PatientID = source.PatientID
	if source.MeasureStartDate != nil {
		s.MeasureStartDate = source.MeasureStartDate.ToDate(0)
	}
	if source.MeasureEndDate != nil {
		s.MeasureEndDate = source.MeasureEndDate.ToDate(1)
	}
}

func (s *BloodGlucoseRecord) CopyToApp(target *doctor.BloodGlucoseRecordForApp) {
	if target == nil {
		return
	}
	target.SerialNo = s.SerialNo
	target.BloodGlucose = s.BloodGlucose
	if s.TimePoint != nil {
		target.TimePoint = string(*s.TimePoint)
	}
	if s.Memo != nil {
		target.Memo = string(*s.Memo)
	}
	if s.MeasureDateTime == nil {
		target.MeasureDateTime = nil
	} else {
		measureDateTime := types.Time(*s.MeasureDateTime)
		target.MeasureDateTime = &measureDateTime
	}
}

func (s *BloodGlucoseRecord) CopyFromAppEx(source *doctor.BloodGlucoseRecordForAppEx) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.PatientID = source.PatientID
	s.BloodGlucose = source.BloodGlucose
	timePoint := string(source.TimePoint)
	s.TimePoint = &timePoint
	memo := string(source.Memo)
	s.Memo = &memo
	if source.MeasureDateTime == nil {
		s.MeasureDateTime = nil
	} else {
		measureDateTime := time.Time(*source.MeasureDateTime)
		s.MeasureDateTime = &measureDateTime
	}
	now := time.Now()
	s.InputDateTime = &now
}

func (s *BloodGlucoseRecord) typeToTimePoint(t uint64) string {
	timePoint := string("")
	switch t {
	case 1:
		timePoint = string("晨起空腹")
	case 2:
		timePoint = string("早餐后")
	case 3:
		timePoint = string("午餐前")
	case 4:
		timePoint = string("午餐后")
	case 5:
		timePoint = string("晚餐前")
	case 6:
		timePoint = string("晚餐后")
	case 7:
		timePoint = string("睡前")
	case 8:
		timePoint = string("凌晨")
	}
	return timePoint
}

func (s *BloodGlucoseRecord) timePointToType(timePoint *string) uint64 {
	t := uint64(0)
	if timePoint == nil {
		return t
	}
	str := string(*timePoint)
	switch str {
	case "晨起空腹":
		t = uint64(1)
	case "早餐后":
		t = uint64(2)
	case "午餐前":
		t = uint64(3)
	case "午餐后":
		t = uint64(4)
	case "晚餐前":
		t = uint64(5)
	case "晚餐后":
		t = uint64(6)
	case "睡前":
		t = uint64(7)
	case "凌晨":
		t = uint64(8)
	}
	return t
}
