package sqldb

type ViewTlcTeamTotalPointBase struct {
}

func (s ViewTlcTeamTotalPointBase) TableName() string {
	return "viewtlcteamtotalpoint"
}

type ViewTlcTeamTotalPoint struct {
	ViewTlcTeamTotalPointBase
	TeamSno           uint64 `sql:"TeamSno"`
	TeamTotalIntegral uint64 `sql:"TeamTotalIntegral"`
	TeamNickName      string `sql:"TeamNickName"`
	BelongToGroup     string `sql:"BelongToGroup"`
}
