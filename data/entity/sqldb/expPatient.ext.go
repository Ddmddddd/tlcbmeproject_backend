package sqldb

import (
	"tlcbme_project/data/model/doctor"
	"time"
)

type ExpPatientIDFilter struct {
	ExpPatientBase
	// 用户ID
	PatientID uint64 `sql:"PatientID"`
}

type ExpPatientStatusUpdate struct {
	ExpPatientBase
	// 序号
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 管理状态，0-退出管理，1-管理中，2-管理中但有未完成的问卷，3-未完成行动计划选择
	Status uint64 `sql:"Status"`
	// 更新时间
	UpdateDateTime *time.Time `sql:"UpdateDateTime"`
	// 终止原因
	EndReason *string `sql:"endReason"`
}

type ExpPatientStatusUpdateByID struct {
	ExpPatientBase
	// 管理状态，0-退出管理，1-管理中，2-管理中但有未完成的问卷，3-未完成行动计划选择
	Status uint64 `sql:"Status"`
	// 更新时间
	UpdateDateTime *time.Time `sql:"UpdateDateTime"`
}

func (s *ExpPatient) CopyFromCreate(source *doctor.ExpPatientCreate) {
	if source == nil {
		return
	}
	s.PatientID = source.PatientID
	s.Type = source.Type
	if source.CreateDateTime == nil {
		s.CreateDateTime = nil
	} else {
		createDateTime := time.Time(*source.CreateDateTime)
		s.CreateDateTime = &createDateTime
	}
}

func (s *ExpPatientStatusUpdate) CopyFromStatusChange(source *doctor.ExpPatientStatusChange) {
	if source == nil {
		return
	}
	if source.SerialNo != 0 {
		s.SerialNo = source.SerialNo
	}
	s.Status = source.Status
	updateDateTime := time.Time(*source.UpdateDateTime)
	s.UpdateDateTime = &updateDateTime
	endReason := string(source.EndReason)
	s.EndReason = &endReason
}
