package sqldb

type ExpDietPlanRepositoryNilFilter struct {
	ExpDietPlanRepositoryBase
}

type ExpDietPlanRepositoryFilter struct {
	ExpDietPlanRepositoryBase
	//优先级
	Level []uint64 `sql:"Level" filter:"IN"`
}