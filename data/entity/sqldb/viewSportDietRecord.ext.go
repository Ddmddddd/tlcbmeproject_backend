package sqldb

import (
	"tlcbme_project/data/model/doctor"
	"time"
)

type ViewSportDietRecordOrder struct {
	ViewSportDietRecordBase

	//
	HappenDate *string `sql:"HappenDate" order:"DESC"`
}

type ViewSportDietRecordFilter struct {
	ViewSportDietRecordBase

	// 用户ID 关联患者用户登录表用户ID 例如:232442
	PatientID uint64 `sql:"PatientID"`

	HappenDateStart *time.Time `sql:"HappenDate" filter:">="`
	HappenDateEnd   *time.Time `sql:"HappenDate" filter:"<"`
}

func (s *ViewSportDietRecordFilter) CopyFrom(source *doctor.ViewSportDietRecordFilter) {
	if source == nil {
		return
	}
	s.PatientID = source.PatientID
	if source.HappenDateStart != nil {
		s.HappenDateStart = source.HappenDateStart.ToDate(0)
	}
	if source.HappenDateEnd != nil {
		s.HappenDateEnd = source.HappenDateEnd.ToDate(1)
	}
}
