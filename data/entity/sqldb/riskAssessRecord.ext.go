package sqldb

import (
	"tlcbme_project/data/enum"
	"tlcbme_project/data/model/doctor"
	"time"
)

func (s *RiskAssessRecord) CopyToEx(target *doctor.RiskAssessRecordEx) {
	if target == nil {
		return
	}
	s.CopyTo(&target.RiskAssessRecord)

	target.LevelText = enum.RiskAssessLevels.Value(s.Level)
}

type RiskAssessRecordOrder struct {
	RiskAssessRecordBase

	// 评估时间 慢病管理工作平台接收到该评估的时间 例如:2018-07-06 15:00:00
	ReceiveDateTime *time.Time `sql:"ReceiveDateTime" order:"DESC"`
}

type RiskAssessRecordFilter struct {
	RiskAssessRecordBase

	// 序号 主键，自增 例如:324
	SerialNo *uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 用户ID 关联患者用户登录表用户ID 例如:232442
	PatientID *uint64 `sql:"PatientID"`
	// 危险级别 1-低危,2-中危, 3-高危 例如:3
	Levels []uint64 `sql:"Level" filter:"in"`
}

func (s *RiskAssessRecordFilter) CopyFrom(source *doctor.RiskAssessRecordFilter) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.PatientID = source.PatientID
	if len(source.Levels) > 0 {
		s.Levels = source.Levels
	}
}
