package sqldb

type TlcHeEduAnswerFilterByQuestionId struct {
	TlcHeEduAnswerBase
	Question_id uint64 `sql:"question_id"`
}
