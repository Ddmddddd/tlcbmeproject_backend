package sqldb

import "time"

type ViewsportdietorgrecordFilter struct {
	// 管理机构代码 管理该患者的机构 例如:897798
	Orgcode *string `sql:"OrgCode"`

	RecordType string `sql:"RecordType"`

	// 发生时间 实际发生的时间 例如:2018-07-03 14:45:00
	Happendatetime *time.Time `sql:"HappenDateTime" filter:"<="`
}
