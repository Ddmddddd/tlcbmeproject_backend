package sqldb

import (
	"tlcbme_project/data/model/doctor"
	"github.com/ktpswjz/httpserver/types"
	"time"
)

type ViewDiscomfortRecordBase struct {
}

func (s ViewDiscomfortRecordBase) TableName() string {
	return "ViewDiscomfortRecord"
}

// VIEW
type ViewDiscomfortRecord struct {
	ViewDiscomfortRecordBase
	// 序号 主键，自增 例如:324
	SerialNo uint64 `sql:"SerialNo"`
	// 用户ID 关联患者用户登录表用户ID 例如:232442
	PatientID uint64 `sql:"PatientID"`
	//
	Discomfort *string `sql:"Discomfort"`
	// 备注  例如:
	Memo *string `sql:"Memo"`
	// 发生时间 不适情况发生的时间 例如:2018-07-03 14:45:00
	HappenDateTime *time.Time `sql:"HappenDateTime"`
	// 录入时间 录入时间 例如:2018-07-03 14:00:00
	InputDateTime *time.Time `sql:"InputDateTime"`
}

func (s *ViewDiscomfortRecord) CopyTo(target *doctor.ViewDiscomfortRecord) {
	if target == nil {
		return
	}
	target.SerialNo = s.SerialNo
	target.PatientID = s.PatientID
	if s.Discomfort != nil {
		target.Discomfort = string(*s.Discomfort)
	}
	if s.Memo != nil {
		target.Memo = string(*s.Memo)
	}
	if s.HappenDateTime == nil {
		target.HappenDateTime = nil
	} else {
		happenDateTime := types.Time(*s.HappenDateTime)
		target.HappenDateTime = &happenDateTime
	}
	if s.InputDateTime == nil {
		target.InputDateTime = nil
	} else {
		inputDateTime := types.Time(*s.InputDateTime)
		target.InputDateTime = &inputDateTime
	}
}

func (s *ViewDiscomfortRecord) CopyFrom(source *doctor.ViewDiscomfortRecord) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.PatientID = source.PatientID
	discomfort := string(source.Discomfort)
	s.Discomfort = &discomfort
	memo := string(source.Memo)
	s.Memo = &memo
	if source.HappenDateTime == nil {
		s.HappenDateTime = nil
	} else {
		happenDateTime := time.Time(*source.HappenDateTime)
		s.HappenDateTime = &happenDateTime
	}
	if source.InputDateTime == nil {
		s.InputDateTime = nil
	} else {
		inputDateTime := time.Time(*source.InputDateTime)
		s.InputDateTime = &inputDateTime
	}
}
