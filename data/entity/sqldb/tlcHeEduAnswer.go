package sqldb

type TlcHeEduAnswerBase struct {
}

func (s TlcHeEduAnswerBase) TableName() string {
	return "tlcheeduanswer"
}

type TlcHeEduAnswer struct {
	TlcHeEduAnswerBase
	Id          uint64 `sql:"id"`
	Content     string `sql:"content"`
	Question_id uint64 `sql:"question_id"`
	Isop        uint64 `sql:"isop"`
}
