package sqldb

import "tlcbme_project/data/model/doctor"

type ManagedPatientClassBase struct {
}

func (s ManagedPatientClassBase) TableName() string {
	return "ManagedPatientClass"
}

// 慢病管理业务
// 工作平台管理数据
// 管理患者管理分类
// 注释：该表保存已经纳入慢病管理的患者的管理分类信息。
type ManagedPatientClass struct {
	ManagedPatientClassBase
	// 用户ID 关联患者用户登录表用户ID 例如:232442
	PatientID uint64 `sql:"PatientID" primary:"true"`
	// 管理分类代码，例如：1
	ManageClassCode uint64 `sql:"ManageClassCode" primary:"true"`
}

func (s *ManagedPatientClass) CopyTo(target *doctor.ManagedPatientClass) {
	if target == nil {
		return
	}
	target.PatientID = s.PatientID
	target.ManageClassCode = s.ManageClassCode
}

func (s *ManagedPatientClass) CopyFrom(source *doctor.ManagedPatientClass) {
	if source == nil {
		return
	}
	s.PatientID = source.PatientID
	s.ManageClassCode = source.ManageClassCode
}
