package sqldb

import (
	"tlcbme_project/data/enum"
	"tlcbme_project/data/model"
	"tlcbme_project/data/model/doctor"
	"fmt"
	"time"
)

func (s *ViewManagementApplicationReview) CopyToEx(target *doctor.ViewManagementApplicationReviewEx) {
	if target == nil {
		return
	}
	s.CopyTo(&target.ViewManagementApplicationReview)

	if s.Sex != nil {
		target.SexText = enum.Sexes.Value(*s.Sex)
	}
	if s.DateOfBirth != nil {
		age := time.Now().Sub(*s.DateOfBirth)
		target.Age = int64(age.Hours() / (24 * 365))
	}
}

type ViewManagementApplicationReviewOrder struct {
	ViewManagementApplicationReviewBase

	// 注册时间  例如:2018-07-02 15:00:00
	RegistDateTime *time.Time `sql:"RegistDateTime"`
}

type ViewManagementApplicationReviewFilter struct {
	ViewManagementApplicationReviewBase

	// 序号 主键，自增 例如:324
	SerialNo *uint64 `sql:"SerialNo"`
	// 用户ID 关联患者用户登录表用户ID 例如:232442
	PatientID *uint64 `sql:"PatientID"`
	// 姓名  例如:张三
	PatientName string `sql:"PatientName" filter:"like"`
	// 来源 表示从哪里发起的申请，其值可以为：APP、微信小程序 例如:APP
	ApplicationFrom string `sql:"ApplicationFrom"`
	// 就诊机构代码 例如:123
	VisitOrgCode string `sql:"VisitOrgCode"`
	// 机构内就诊号 例如:123456
	OrgVisitID string `sql:"OrgVisitID"`
	// 健康管理师ID 患者注册时指定的健康管理师 例如:12432
	HealthManagerID *uint64 `sql:"HealthManagerID"`
	// 审核状态 0-未审核，1-审核通过，2-审核不通过 例如:0
	Statuses []uint64 `sql:"Status" filter:"IN"`
	// 身份证号 例如:330106200012129876
	IdentityCardNumber string `sql:"IdentityCardNumber"`
	// 联系电话 例如:13812344321
	Phone string `sql:"Phone"`
}

func (s *ViewManagementApplicationReviewFilter) CopyFrom(source *doctor.ViewManagementApplicationReviewFilter) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.PatientID = source.PatientID
	if len(source.PatientName) > 0 {
		s.PatientName = fmt.Sprint("%", source.PatientName, "%")
	}
	s.ApplicationFrom = source.ApplicationFrom
	s.OrgVisitID = source.OrgVisitID
	s.HealthManagerID = source.HealthManagerID
	if len(source.Statuses) > 0 {
		s.Statuses = source.Statuses
	}
	s.IdentityCardNumber = source.IdentityCardNumber
	s.Phone = source.Phone
}

func (s *ViewManagementApplicationReviewFilter) CopyFromEx(source *doctor.ViewManagementApplicationReviewFilterEx) {
	if source == nil {
		return
	}
	s.CopyFrom(&source.ViewManagementApplicationReviewFilter)

	s.VisitOrgCode = source.VisitOrgCode
}

type ViewManagementApplicationReviewOrderBase struct {
	ViewManagementApplicationReviewBase

	// 序号 主键，自增 例如:324
	SerialNo uint64 `sql:"SerialNo" index:"100"`
}

type ViewManagementApplicationReviewOrderRegistDateTimeAscending struct {
	ViewManagementApplicationReviewOrderBase

	// 注册时间  例如:2018-07-02 15:00:00
	RegistDateTime *time.Time `sql:"RegistDateTime"`
}

type ViewManagementApplicationReviewOrderRegistDateTimeDescending struct {
	ViewManagementApplicationReviewOrderBase

	// 注册时间  例如:2018-07-02 15:00:00
	RegistDateTime *time.Time `sql:"RegistDateTime" order:"DESC"`
}

var viewManagementApplicationReviewOrders = make(map[string]interface{}, 0)

func init() {
	viewManagementApplicationReviewOrders["registDateTime-ascending"] = &ViewManagementApplicationReviewOrderRegistDateTimeAscending{}
	viewManagementApplicationReviewOrders["registDateTime-descending"] = &ViewManagementApplicationReviewOrderRegistDateTimeDescending{}
}

func (s *ViewManagementApplicationReview) GetOrder(order *model.Order) interface{} {
	if order == nil {
		return &ViewManagementApplicationReviewOrder{}
	}

	key := order.Key()
	if len(key) < 1 {
		return &ViewManagementApplicationReviewOrder{}
	}

	v, ok := viewManagementApplicationReviewOrders[key]
	if ok {
		return v
	} else {
		return &ViewManagementApplicationReviewOrder{}
	}
}
