package sqldb

import (
	"tlcbme_project/data/model/doctor"
	"github.com/ktpswjz/httpserver/types"
	"time"
)

type FollowupPlanBase struct {
}

func (s FollowupPlanBase) TableName() string {
	return "FollowupPlan"
}

// 慢病管理业务
// 工作平台管理数据
// 随访计划
// 注释：该表保存所有患者的所有随访计划
type FollowupPlan struct {
	FollowupPlanBase
	// 序号 主键，自增 例如:324
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 用户ID 关联患者用户登录表用户ID 例如:232442
	PatientID uint64 `sql:"PatientID"`
	// 计划随访日期 例如:2018-07-15
	FollowupDate *time.Time `sql:"FollowupDate"`
	// 随访类型 随访的类型（原因） 例如:三个月例行随访
	FollowUpType string `sql:"FollowUpType"`
	// 随访状态 0-待随访，1-已随访，2-已忽略，3-已过期，9-已作废。 例如:0
	Status uint64 `sql:"Status"`
	// 状态备注 例如当状态为忽略时，此处填写忽略的原因。 例如:
	StatusMemo *string `sql:"StatusMemo"`
	// 创建时间 随访计划的创建时间 例如:2018-07-15 09:33:22
	CreateDateTime *time.Time `sql:"CreateDateTime"`
	// 创建者姓名 随访计划创建者的姓名 例如:张三
	CreatorName string `sql:"CreatorName"`
	// 最后更新日期 例如:2018-08-15
	UpdateTime *time.Time `sql:"UpdateTime"`
}

func (s *FollowupPlan) CopyTo(target *doctor.FollowupPlan) {
	if target == nil {
		return
	}
	target.SerialNo = s.SerialNo
	target.PatientID = s.PatientID
	if s.FollowupDate == nil {
		target.FollowupDate = nil
	} else {
		followupDate := types.Time(*s.FollowupDate)
		target.FollowupDate = &followupDate
	}
	target.FollowUpType = s.FollowUpType
	target.Status = s.Status
	if s.StatusMemo != nil {
		target.StatusMemo = string(*s.StatusMemo)
	}
	if s.CreateDateTime == nil {
		target.CreateDateTime = nil
	} else {
		createDateTime := types.Time(*s.CreateDateTime)
		target.CreateDateTime = &createDateTime
	}
	target.CreatorName = s.CreatorName
	if s.UpdateTime == nil {
		target.UpdateTime = nil
	} else {
		updateTime := types.Time(*s.UpdateTime)
		target.UpdateTime = &updateTime
	}
}

func (s *FollowupPlan) CopyFrom(source *doctor.FollowupPlan) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.PatientID = source.PatientID
	if source.FollowupDate == nil {
		s.FollowupDate = nil
	} else {
		followupDate := time.Time(*source.FollowupDate)
		s.FollowupDate = &followupDate
	}
	s.FollowUpType = source.FollowUpType
	s.Status = source.Status
	statusMemo := string(source.StatusMemo)
	s.StatusMemo = &statusMemo
	if source.CreateDateTime == nil {
		s.CreateDateTime = nil
	} else {
		createDateTime := time.Time(*source.CreateDateTime)
		s.CreateDateTime = &createDateTime
	}
	s.CreatorName = source.CreatorName
	if source.UpdateTime == nil {
		s.UpdateTime = nil
	} else {
		updateTime := time.Time(*source.UpdateTime)
		s.UpdateTime = &updateTime
	}
}
