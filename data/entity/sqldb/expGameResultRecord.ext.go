package sqldb

import (
	"tlcbme_project/data/model/doctor"
	"time"
)

type ExpGameResultRecordFilter struct {
	ExpGameResultRecordBase
	// 用户ID
	PatientID uint64 `sql:"PatientID"`
	// 起始时间
	StartDateTime *time.Time `sql:"CreateDateTime" filter:">="`
}

func (s *ExpGameResultRecordFilter) CopyFrom(source *doctor.ExpGameResultRecordFilter) {
	if source == nil {
		return
	}
	s.PatientID = source.PatientID
	if source.StartDateTime != nil {
		s.StartDateTime = source.StartDateTime.ToDate(0)
	}
}

type ExpGameResultRecordOrder struct {
	ExpGameResultRecordBase
	// 得分
	Point uint64 `sql:"Point" order:"DESC"`
	// 序号
	SerialNo uint64 `sql:"SerialNo" order:"DESC"`
}