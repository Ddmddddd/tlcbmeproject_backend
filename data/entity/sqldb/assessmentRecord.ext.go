package sqldb

import (
	"tlcbme_project/data/model/doctor"
	"encoding/json"
	"github.com/ktpswjz/httpserver/types"
	"time"
)

func (s *AssessmentRecord) CopyFromCreate(source *doctor.AssessmentRecordCreate) {
	if source == nil {
		return
	}
	s.PatientID = source.PatientID
	s.AssessmentName = source.AssessmentName
	s.AssessmentSheetCode = source.AssessmentSheetCode
	s.AssessmentSheetVersion = source.AssessmentSheetVersion
	if source.Content != nil {
		contentData, err := json.Marshal(source.Content)
		if err == nil {
			content := string(contentData)
			s.Content = &content
		}
	}

}

func (s *AssessmentRecord) CopyFromCreateEx(source *doctor.AssessmentRecordCreateEx) {
	if source == nil {
		return
	}
	s.CopyFromCreate(&source.AssessmentRecordCreate)

	s.OperatorID = source.OperatorID
	s.Summary = source.Summary
	if source.Others != nil {
		contentData, err := json.Marshal(source.Others)
		if err == nil {
			content := string(contentData)
			s.Others = &content
		}
	}
}

type AssessmentRecordUpdate struct {
	AssessmentRecordBase
	// 序号 主键，自增 例如:324
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 评估摘要 摘要规则根据所用评估表不同而不同，一般此处填入评估结果。 例如:中危
	Summary string `sql:"Summary"`
}

func (s *AssessmentRecordUpdate) CopyFrom(source *doctor.AssessmentRecordUpdate) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.Summary = source.Summary
}

type AssessmentRecordOrder struct {
	AssessmentRecordBase

	// 评估时间  例如:2018-08-01 10:00:00
	AssessDateTime *time.Time `sql:"AssessDateTime" order:"DESC"`
}

type AssessmentRecordFilter struct {
	AssessmentRecordBase

	// 序号 主键，自增 例如:324
	SerialNo *uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 用户ID 关联患者用户登录表用户ID 例如:232442
	PatientID *uint64 `sql:"PatientID"`
	// 评估时间  例如:2018-08-01 10:00:00
	AssessDateStart *time.Time `sql:"AssessDateTime" filter:">="`
	AssessDateEnd   *time.Time `sql:"AssessDateTime" filter:"<"`
	// 评估名称  例如:高血压危险分层评估
	AssessmentName string `sql:"AssessmentName"`
	// 评估摘要 摘要规则根据所用评估表不同而不同，一般此处填入评估结果。 例如:中危
	Summary string `sql:"Summary"`
}

func (s *AssessmentRecordFilter) CopyFrom(source *doctor.AssessmentRecordFilter) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.PatientID = source.PatientID
	if source.AssessDateStart != nil {
		s.AssessDateStart = source.AssessDateStart.ToDate(0)
	}
	if source.AssessDateEnd != nil {
		s.AssessDateEnd = source.AssessDateEnd.ToDate(1)
	}
	s.AssessmentName = source.AssessmentName
	s.Summary = source.Summary
}

type AssessmentRecordListItem struct {
	AssessmentRecordBase
	// 序号 主键，自增 例如:324
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 用户ID 关联患者用户登录表用户ID 例如:232442
	PatientID uint64 `sql:"PatientID"`
	// 评估时间  例如:2018-08-01 10:00:00
	AssessDateTime *time.Time `sql:"AssessDateTime"`
	// 评估名称  例如:高血压危险分层评估
	AssessmentName string `sql:"AssessmentName"`
	// 评估表代码 关联评估表字典代码字段 例如:1
	AssessmentSheetCode uint64 `sql:"AssessmentSheetCode"`
	// 评估表版本号 例如:1
	AssessmentSheetVersion uint64 `sql:"AssessmentSheetVersion"`
	// 评估摘要 摘要规则根据所用评估表不同而不同，一般此处填入评估结果。 例如:中危
	Summary string `sql:"Summary"`
	// 操作者ID 保存用户ID，如果是后台服务自动做的评估，这里填0 例如:322
	OperatorID uint64 `sql:"OperatorID"`
	// 操作者ID 保存用户ID，如果是后台服务自动做的评估，这里填0 例如:322
	Others *string `sql:"Others"`
}

func (s *AssessmentRecordListItem) CopyTo(target *doctor.AssessmentRecordListItem) {
	if target == nil {
		return
	}
	target.SerialNo = s.SerialNo
	target.PatientID = s.PatientID
	if s.AssessDateTime == nil {
		target.AssessDateTime = nil
	} else {
		assessDateTime := types.Time(*s.AssessDateTime)
		target.AssessDateTime = &assessDateTime
	}
	target.AssessmentName = s.AssessmentName
	target.AssessmentSheetCode = s.AssessmentSheetCode
	target.AssessmentSheetVersion = s.AssessmentSheetVersion
	target.Summary = s.Summary
	others := ""
	if s.Others != nil {
		others = *s.Others
	}
	if len(others) > 0 {
		json.Unmarshal([]byte(others), &target.Others)
	}

}

func (s *AssessmentRecord) CopyFromScale(source *doctor.ExpScaleRecordCreate) {
	if source == nil {
		return
	}
	s.PatientID = source.PatientID
	s.AssessmentName = source.ScaleName
	s.AssessmentSheetCode = 2
	s.AssessmentSheetVersion = source.ScaleVersion
	s.Summary = "共计0条不良饮食障碍"
	if source.CreateDateTime == nil {
		s.AssessDateTime = nil
	} else {
		assessDateTime := time.Time(*source.CreateDateTime)
		s.AssessDateTime = &assessDateTime
	}
	s.OperatorID = source.OperatorID
	if source.Result != nil {
		contentData, err := json.Marshal(source.Result)
		if err == nil {
			content := string(contentData)
			s.Content = &content
		}
	}
}