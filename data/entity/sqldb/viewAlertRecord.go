package sqldb

import (
	"tlcbme_project/data/model/doctor"
	"github.com/ktpswjz/httpserver/types"
	"time"
)

type ViewAlertRecordBase struct {
}

func (s ViewAlertRecordBase) TableName() string {
	return "ViewAlertRecord"
}

// VIEW
type ViewAlertRecord struct {
	ViewAlertRecordBase
	// 序号 主键，自增 例如:324
	SerialNo uint64 `sql:"SerialNo"`
	// 用户ID 关联患者用户登录表用户ID 例如:232442
	PatientID uint64 `sql:"PatientID"`
	// 预警代码 每类预警的唯一代码 例如:001
	AlertCode string `sql:"AlertCode"`
	// 预警类型 例如:血压
	AlertType string `sql:"AlertType"`
	// 预警名称 例如:单次血压异常偏高
	AlertName string `sql:"AlertName"`
	// 预警原因  例如:180/100mmHg
	AlertReason string `sql:"AlertReason"`
	// 预警消息 例如:消息内容
	AlertMessage string `sql:"AlertMessage"`
	// 预警发生时间  例如:2018-07-06 14:55:00
	AlertDateTime *time.Time `sql:"AlertDateTime"`
	// 处理状态 0-未处理 例如:0
	Status uint64 `sql:"Status"`
	// 处理方式 0-随访，1-忽略 例如:0
	ProcessMode *uint64 `sql:"ProcessMode"`
	// 管理机构代码 管理该患者的机构 例如:897798
	OrgCode *string `sql:"OrgCode"`
	// 医生ID 例如:232
	DoctorID *uint64 `sql:"DoctorID"`
	// 健康管理师ID 例如:232
	HealthManagerID *uint64 `sql:"HealthManagerID"`
}

func (s *ViewAlertRecord) CopyTo(target *doctor.ViewAlertRecord) {
	if target == nil {
		return
	}
	target.SerialNo = s.SerialNo
	target.PatientID = s.PatientID
	target.AlertCode = s.AlertCode
	target.AlertType = s.AlertType
	target.AlertName = s.AlertName
	target.AlertReason = s.AlertReason
	target.AlertMessage = s.AlertMessage
	if s.AlertDateTime == nil {
		target.AlertDateTime = nil
	} else {
		alertDateTime := types.Time(*s.AlertDateTime)
		target.AlertDateTime = &alertDateTime
	}
	target.Status = s.Status
	target.ProcessMode = s.ProcessMode
	if s.OrgCode != nil {
		target.OrgCode = string(*s.OrgCode)
	}
	target.DoctorID = s.DoctorID
	target.HealthManagerID = s.HealthManagerID
}

func (s *ViewAlertRecord) CopyFrom(source *doctor.ViewAlertRecord) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.PatientID = source.PatientID
	s.AlertCode = source.AlertCode
	s.AlertType = source.AlertType
	s.AlertName = source.AlertName
	s.AlertReason = source.AlertReason
	s.AlertMessage = source.AlertMessage
	if source.AlertDateTime == nil {
		s.AlertDateTime = nil
	} else {
		alertDateTime := time.Time(*source.AlertDateTime)
		s.AlertDateTime = &alertDateTime
	}
	s.Status = source.Status
	s.ProcessMode = source.ProcessMode
	orgCode := string(source.OrgCode)
	s.OrgCode = &orgCode
	s.DoctorID = source.DoctorID
	s.HealthManagerID = source.HealthManagerID
}
