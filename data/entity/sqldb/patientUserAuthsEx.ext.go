package sqldb

import (
	"tlcbme_project/data/model"
)

type PatientUserAuthsExFilter struct {
	PatientUserAuthsExBase
	// 用户ID  例如:
	UserID 		  uint64 `sql:"UserID"`
	// 账号类型 微信、QQ、微博 例如:微信
	IdentityType  string `sql:"IdentityType"`
	// 账号
	Identitier 	  string `sql:"Identitier"`
}

type WeChatLoginFilter struct {
	PatientUserAuthsExBase
	// 账号
	Identitier string `sql:"Identitier"`
	Status	   string `sql:"Status"`
}

func (s *WeChatLoginFilter) CopyFrom(source *model.WeChatLoginFilter) {
	if source == nil {
		return
	}
	s.Identitier = source.Identitier
	s.Status = source.Status
}

type PatientUserAuthsExStatusUpdate struct {
	PatientUserAuthsExBase

	// 账号状态，0-正常，1-停用
	Status string `sql:"Status"`
}

