package sqldb

import (
	"tlcbme_project/data/model/doctor"
	"encoding/json"
)

type ExpSmallScaleTaskIDFilter struct {
	ExpSmallScaleBase
	// 任务序号
	TaskID uint64 `sql:"TaskID"`
}

type ExpSmallScaleEdit struct {
	ExpSmallScaleBase
	// 序号
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 量表内容，json格式
	Content string `sql:"Content"`
}

func (s *ExpSmallScaleEdit) CopyFrom(source *doctor.ExpSmallScaleContentEdit) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	if source.Content != nil {
		contentData, err := json.Marshal(source.Content)
		if err == nil {
			content := string(contentData)
			s.Content = content
		}
	}
}

type ExpSmallScaleIDsFilter struct {
	ExpSmallScaleBase
	// 序号
	SerialNos []uint64 `sql:"SerialNo" filter:"in"`
}