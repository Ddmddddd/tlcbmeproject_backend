package sqldb

import (
	"tlcbme_project/data/model/doctor"
	"time"
)

type ExpActionPlanPatientIDAndStatusFilter struct {
	ExpActionPlanBase
	// 用户ID
	PatientID uint64 `sql:"PatientID"`
	// 状态，0-使用中，1-已取消
	Status uint64 `sql:"Status"`
}

type ExpActionPlanOrder struct {
	ExpActionPlanBase
	// 优先级，分为0-5，数值越大优先级越高
	Level uint64 `sql:"Level" order:"desc"`
}

type ExpActionPlanEdit struct {
	ExpActionPlanBase
	// 序号
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 行动计划
	ActionPlan string `sql:"ActionPlan"`
	// 状态，0-使用中，1-已取消
	Status uint64 `sql:"Status"`
	// 更新时间
	UpdateDateTime *time.Time `sql:"UpdateDateTime"`
	// 编辑者，0-引擎规则，1-医生，2-患者
	EditorType uint64 `sql:"EditorType"`
	// 优先级，分为0-5，数值越大优先级越高
	Level uint64 `sql:"Level"`
}

func (s *ExpActionPlanEdit) CopyFromEdit(source *doctor.ExpPatientActionPlanEdit) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.ActionPlan = source.ActionPlan
	s.Status = source.Status
	if source.UpdateDateTime == nil {
		s.UpdateDateTime = nil
	} else {
		updateDateTime := time.Time(*source.UpdateDateTime)
		s.UpdateDateTime = &updateDateTime
	}
	s.EditorType = source.EditorType
	s.Level = source.Level
}

type ExpActionPlanFilter struct {
	ExpActionPlanBase
	// 序号
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
}

type ExpActionPlanStatusUpdate struct {
	ExpActionPlanBase
	// 状态，0-使用中，1-已取消
	Status uint64 `sql:"Status"`
}

type ExpActionPlanRecordTimesUpdate struct {
	ExpActionPlanBase
	// 序号
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 记录次数
	RecordTimes uint64 `sql:"RecordTimes"`
}
