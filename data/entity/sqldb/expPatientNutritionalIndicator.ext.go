package sqldb

import "tlcbme_project/data/model/doctor"

func (s *ExpPatientNutritionalIndicator) CopyFromExt(source *doctor.PatientUserBaseInfoModify) {
	s.GoalEnergy = source.GoalEnergy
	s.UserID = source.UserID
	s.GoalProteinMin = source.GoalProteinMin
	s.GoalProteinMax = source.GoalProteinMax
	s.GoalFatMin = source.GoalFatMin
	s.GoalFatMax = source.GoalFatMax
	s.GoalCarbohydrateMin = source.GoalCarbohydrateMin
	s.GoalCarbohydrateMax = source.GoalCarbohydrateMax

}

type ExpPatientNutritionIndicatorFilterByUserID struct {
	ExpPatientNutritionalIndicatorBase
	UserID uint64 `sql:"UserID"`
}
