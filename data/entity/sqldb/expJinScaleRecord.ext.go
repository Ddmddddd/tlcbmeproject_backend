package sqldb

type ExpJinScaleRecordPatientFilter struct {
	ExpJinScaleRecordBase
	// 患者id
	PatientID uint64 `sql:"PatientID"`
}