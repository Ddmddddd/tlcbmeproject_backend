package sqldb

import (
	"tlcbme_project/data/model/doctor"
	"github.com/ktpswjz/httpserver/types"
	"strings"
	"time"
)

type ExpQuestionToKnowledgeBase struct {
}

func (s ExpQuestionToKnowledgeBase) TableName() string {
	return "ExpQuestionToKnowledge"
}

type ExpQuestionToKnowledge struct {
	ExpQuestionToKnowledgeBase
	// 序号
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 链接的知识
	KnowledgeID uint64 `sql:"KnowledgeID"`
	// 问题
	Question string `sql:"Question"`
	// 答案列表
	AnswerList string `sql:"AnswerList"`
	// 正确答案
	CorrectIndex uint64 `sql:"CorrectIndex"`
	// 编辑者id
	EditorID uint64 `sql:"EditorID"`
	// 创建时间
	CreateDateTime *time.Time `sql:"CreateDateTime"`
	// 状态
	Status uint64 `sql:"Status"`
	// 更新时间
	UpdateTime *time.Time `sql:"UpdateTime"`
}

func (s *ExpQuestionToKnowledge) CopyTo(target *doctor.ExpQuestionToKnowledge) {
	if target == nil {
		return
	}
	target.SerialNo = s.SerialNo
	target.KnowledgeID = s.KnowledgeID
	target.Question = s.Question
	target.AnswerList = strings.Split(s.AnswerList, ",")
	target.CorrectIndex = s.CorrectIndex
	target.EditorID = s.EditorID
	if s.CreateDateTime == nil {
		target.CreateDateTime = nil
	} else {
		createDateTme := types.Time(*s.CreateDateTime)
		target.CreateDateTime = &createDateTme
	}
	target.Status = s.Status
	if s.UpdateTime == nil {
		target.UpdateTime = nil
	} else {
		updateTime := types.Time(*s.UpdateTime)
		target.UpdateTime = &updateTime
	}
}

func (s *ExpQuestionToKnowledge) CopyFrom(source *doctor.ExpQuestionToKnowledge) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.KnowledgeID = source.KnowledgeID
	s.Question = source.Question
	s.AnswerList = strings.Join(source.AnswerList, ",")
	s.CorrectIndex = source.CorrectIndex
	s.EditorID = source.EditorID
	if source.CreateDateTime == nil {
		s.CreateDateTime = nil
	} else {
		createDateTme := time.Time(*source.CreateDateTime)
		s.CreateDateTime = &createDateTme
	}
	s.Status = source.Status
	if source.UpdateTime == nil {
		s.UpdateTime = nil
	} else {
		updateTime := time.Time(*source.UpdateTime)
		s.UpdateTime = &updateTime
	}
}

func (s *ExpQuestionToKnowledge) CopyFromAddInput(source *doctor.ExpQuestionToKnowledgeAddInput) {
	if source == nil {
		return
	}
	s.KnowledgeID = source.KnowledgeID
	s.Question = source.Question
	s.AnswerList = strings.Join(source.AnswerList, ",")
	s.CorrectIndex = source.CorrectIndex
	s.EditorID = source.EditorID
	if source.CreateDateTime == nil {
		s.CreateDateTime = nil
	} else {
		createDateTme := time.Time(*source.CreateDateTime)
		s.CreateDateTime = &createDateTme
	}
}
