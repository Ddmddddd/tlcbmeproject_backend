package sqldb

import (
	"encoding/json"
	"github.com/ktpswjz/httpserver/types"
	"time"
	"tlcbme_project/data/model/doctor"
)

type ManagedPatientIndexBase struct {
}

func (s ManagedPatientIndexBase) TableName() string {
	return "ManagedPatientIndex"
}

// 慢病管理业务
// 工作平台管理数据
// 管理患者索引
// 注释：该表保存已经纳入慢病管理的患者的管理信息索引，各慢病详细的管理信息则保存在各自的ManageDetil表中。
type ManagedPatientIndex struct {
	ManagedPatientIndexBase
	// 序号 主键，自增 例如:324
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 用户ID 关联患者用户登录表用户ID 例如:232442
	PatientID uint64 `sql:"PatientID"`
	// 患者特点 如有多个，之间用逗号分隔，例如：老年人，肥胖，残疾人 例如:老年人，肥胖
	PatientFeature *string `sql:"PatientFeature"`
	// 管理分类 如有多个，之间用逗号分隔，例如：高血压，糖尿病 例如:高血压，糖尿病
	ManageClass *string `sql:"ManageClass"`
	// 管理机构代码 管理该患者的机构 例如:897798
	OrgCode *string `sql:"OrgCode"`
	// 管理状态 0-管理中，1-迁出，2-已终止管理 例如:0
	ManageStatus uint64 `sql:"ManageStatus"`
	// 医生ID 例如:232
	DoctorID *uint64 `sql:"DoctorID"`
	// 医生姓名 例如:张三
	DoctorName *string `sql:"DoctorName"`
	// 健康管理师ID 例如:232
	HealthManagerID *uint64 `sql:"HealthManagerID"`
	// 健康管理师姓名 例如:李四
	HealthManagerName *string `sql:"HealthManagerName"`
	// 管理开始时间 最近一次管理的开始时间，如果是刚注册，则默认为审核通过时间 例如:2018-07-03 13:15:00
	ManageStartDateTime *time.Time `sql:"ManageStartDateTime"`
	// 上次随访日期 例如:2018-06-15
	LastFollowupDate *time.Time `sql:"LastFollowupDate"`
	// 累计随访次数 本次管理的累计随访次数 例如:3
	FollowupTimes *uint64 `sql:"FollowupTimes"`
	// 依从度 值为0至5，0表示依从度未知 例如:4
	ComplianceRate *uint64 `sql:"ComplianceRate"`
	// 患者活跃度 用户参与管理的活跃度，0-沉睡，1-活跃，默认值为1 例如:1
	PatientActiveDegree uint64 `sql:"PatientActiveDegree"`
	// 管理终止时间 该字段只有当ManageStatus字段为2时才有意义 例如:2018-08-03 13:15:00
	ManageEndDateTime *time.Time `sql:"ManageEndDateTime"`
	// 管理终止的原因 该字段只有当ManageStatus字段为2时才有意义 例如:亡故（2018-08-03，车祸死亡）
	ManageEndReason *string `sql:"ManageEndReason"`
	// 扩展信息 保存不同管理分类的扩展信息 例如:{htn:{}, dm:{}}
	Ext *string `sql:"Ext"`
}

func (s *ManagedPatientIndex) CopyTo(target *doctor.ManagedPatientIndex) {
	if target == nil {
		return
	}
	target.SerialNo = s.SerialNo
	target.PatientID = s.PatientID
	if s.PatientFeature != nil {
		target.PatientFeature = string(*s.PatientFeature)
	}
	if s.ManageClass != nil {
		target.ManageClass = string(*s.ManageClass)
	}
	if s.OrgCode != nil {
		target.OrgCode = string(*s.OrgCode)
	}
	target.ManageStatus = s.ManageStatus
	target.DoctorID = s.DoctorID
	if s.DoctorName != nil {
		target.DoctorName = string(*s.DoctorName)
	}
	target.HealthManagerID = s.HealthManagerID
	if s.HealthManagerName != nil {
		target.HealthManagerName = string(*s.HealthManagerName)
	}
	if s.ManageStartDateTime == nil {
		target.ManageStartDateTime = nil
	} else {
		manageStartDateTime := types.Time(*s.ManageStartDateTime)
		target.ManageStartDateTime = &manageStartDateTime
	}
	if s.LastFollowupDate == nil {
		target.LastFollowupDate = nil
	} else {
		lastFollowupDate := types.Time(*s.LastFollowupDate)
		target.LastFollowupDate = &lastFollowupDate
	}
	target.FollowupTimes = s.FollowupTimes
	target.ComplianceRate = s.ComplianceRate
	target.PatientActiveDegree = s.PatientActiveDegree
	if s.ManageEndDateTime == nil {
		target.ManageEndDateTime = nil
	} else {
		manageEndDateTime := types.Time(*s.ManageEndDateTime)
		target.ManageEndDateTime = &manageEndDateTime
	}
	if s.ManageEndReason != nil {
		target.ManageEndReason = string(*s.ManageEndReason)
	}
	ext := ""
	if s.Ext != nil {
		ext = *s.Ext
	}
	if len(ext) > 0 {
		json.Unmarshal([]byte(ext), &target.Ext)
	}

}

func (s *ManagedPatientIndex) CopyFrom(source *doctor.ManagedPatientIndex) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.PatientID = source.PatientID
	patientFeature := string(source.PatientFeature)
	s.PatientFeature = &patientFeature
	manageClass := string(source.ManageClass)
	s.ManageClass = &manageClass
	orgCode := string(source.OrgCode)
	s.OrgCode = &orgCode
	s.ManageStatus = source.ManageStatus
	s.DoctorID = source.DoctorID
	doctorName := string(source.DoctorName)
	s.DoctorName = &doctorName
	s.HealthManagerID = source.HealthManagerID
	healthManagerName := string(source.HealthManagerName)
	s.HealthManagerName = &healthManagerName
	if source.ManageStartDateTime == nil {
		s.ManageStartDateTime = nil
	} else {
		manageStartDateTime := time.Time(*source.ManageStartDateTime)
		s.ManageStartDateTime = &manageStartDateTime
	}
	if source.LastFollowupDate == nil {
		s.LastFollowupDate = nil
	} else {
		lastFollowupDate := time.Time(*source.LastFollowupDate)
		s.LastFollowupDate = &lastFollowupDate
	}
	s.FollowupTimes = source.FollowupTimes
	s.ComplianceRate = source.ComplianceRate
	s.PatientActiveDegree = source.PatientActiveDegree
	if source.ManageEndDateTime == nil {
		s.ManageEndDateTime = nil
	} else {
		manageEndDateTime := time.Time(*source.ManageEndDateTime)
		s.ManageEndDateTime = &manageEndDateTime
	}
	manageEndReason := string(source.ManageEndReason)
	s.ManageEndReason = &manageEndReason
	if source.Ext != nil {
		extData, err := json.Marshal(source.Ext)
		if err == nil {
			ext := string(extData)
			s.Ext = &ext
		}
	}

}
