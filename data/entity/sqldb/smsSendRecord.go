package sqldb

import (
	"tlcbme_project/data/model/doctor"
	"github.com/ktpswjz/httpserver/types"
	"time"
)

type SmsSendRecordBase struct {
}

func (s SmsSendRecordBase) TableName() string {
	return "SmsSendRecord"
}

// 平台运维
// 短信
// 短信发送记录
// 注释：本表保存系统发出的短信，以便回溯。
type SmsSendRecord struct {
	SmsSendRecordBase
	// 序号 主键，自增 例如:324
	SerialNo uint64 `sql:"SerialNo" primary:"true" auto:"true"`
	// 接收者手机号 例如:13812345678
	ReceiverPhoneNum string `sql:"ReceiverPhoneNum"`
	// 模板类型 0-验证码，1-短信通知，2-推广短信 例如:1
	TemplateType *uint64 `sql:"TemplateType"`
	// 模板代码 该条短信使用的短信模板代码，关联SmsTemplate表 例如:SMS_1330248924729
	TemplateCode *string `sql:"TemplateCode"`
	// 短信内容 例如:【浙大健康小微】马晓阳您好，您的复诊时间是2019-10-10，请您于近日前往医院复查。祝您健康！
	SmsContent string `sql:"SmsContent"`
	// 患者ID 例如:324
	PatientID *uint64 `sql:"PatientID"`
	// 管理机构代码 例如:4562432424
	OrgCode *string `sql:"OrgCode"`
	// 发送者用户ID 例如:998
	Sender uint64 `sql:"Sender"`
	// 短信发送时间 例如:2020-02-16 14:23:00
	SendDatetime *time.Time `sql:"SendDatetime"`
}

func (s *SmsSendRecord) CopyTo(target *doctor.SmsSendRecord) {
	if target == nil {
		return
	}
	target.SerialNo = s.SerialNo
	target.ReceiverPhoneNum = s.ReceiverPhoneNum
	target.TemplateType = s.TemplateType
	if s.TemplateCode != nil {
		target.TemplateCode = string(*s.TemplateCode)
	}
	target.SmsContent = s.SmsContent
	target.PatientID = s.PatientID
	if s.OrgCode != nil {
		target.OrgCode = string(*s.OrgCode)
	}
	target.Sender = s.Sender
	if s.SendDatetime == nil {
		target.SendDatetime = nil
	} else {
		sendDatetime := types.Time(*s.SendDatetime)
		target.SendDatetime = &sendDatetime
	}
}

func (s *SmsSendRecord) CopyFrom(source *doctor.SmsSendRecord) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.ReceiverPhoneNum = source.ReceiverPhoneNum
	s.TemplateType = source.TemplateType
	templateCode := string(source.TemplateCode)
	s.TemplateCode = &templateCode
	s.SmsContent = source.SmsContent
	s.PatientID = source.PatientID
	orgCode := string(source.OrgCode)
	s.OrgCode = &orgCode
	s.Sender = source.Sender
	if source.SendDatetime == nil {
		s.SendDatetime = nil
	} else {
		sendDatetime := time.Time(*source.SendDatetime)
		s.SendDatetime = &sendDatetime
	}
}

func (s *SmsSendRecord) CopyFromCreateExt(source *doctor.SmsParam) {
	if source == nil {
		return
	}
	s.ReceiverPhoneNum = source.PhoneNumber
	s.TemplateType = &source.TemplateType
	templateCode := string(source.TemplateCode)
	s.TemplateCode = &templateCode
	s.SmsContent = source.SmsContent
	s.PatientID = &source.PatientID
	orgCode := string(source.OrgCode)
	s.OrgCode = &orgCode
	s.Sender = source.Sender
	if source.SendDateTime == nil {
		s.SendDatetime = nil
	} else {
		sendDatetime := time.Time(*source.SendDateTime)
		s.SendDatetime = &sendDatetime
	}
}

func (s *SmsSendRecord) CopyToExt(target *doctor.SmsSendRecordExt) {
	if target == nil {
		return
	}
	target.SerialNo = s.SerialNo
	target.ReceiverPhoneNum = s.ReceiverPhoneNum
	target.TemplateType = s.TemplateType
	if s.TemplateCode != nil {
		target.TemplateCode = string(*s.TemplateCode)
	}
	target.SmsContent = s.SmsContent
	target.PatientID = s.PatientID
	if s.OrgCode != nil {
		target.OrgCode = string(*s.OrgCode)
	}
	target.Sender = s.Sender
	if s.SendDatetime == nil {
		target.SendDatetime = nil
	} else {
		sendDatetime := types.Time(*s.SendDatetime)
		target.SendDatetime = &sendDatetime
	}
}
