package sqldb

type KnowledgeToTagBase struct {
}

func (s KnowledgeToTagBase) TableName() string {
	return "tlcheeduknowledgetotag"
}

type KnowledgeToTag struct {
	KnowledgeToTagBase
	Id           uint64 `sql:"id"`
	Knowledge_id uint64 `sql:"knowledge_id"`
	Tag_id       uint64 `sql:"tag_id"`
}
