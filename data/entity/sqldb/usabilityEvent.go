package sqldb

import (
	"tlcbme_project/data/model/doctor"
	"github.com/ktpswjz/httpserver/types"
	"time"
)

type UsabilityEventBase struct {
}

func (s UsabilityEventBase) TableName() string {
	return "UsabilityEvent"
}

// 平台运维
// 用户行为统计
// 用户事件统计表
// 注释：本表记录用户在应用触发的事件
type UsabilityEvent struct {
	UsabilityEventBase
	// 序号 主键，自增 例如:1
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 用户名 关联患者用户登录表用户名 例如:aa
	UserName string `sql:"UserName"`
	// 应用标识 唯一标识应用 例如:abcdefghijklmn
	AppId string `sql:"AppId"`
	// 应用类别 Android、iOS或者微信小程序 例如:Android
	AppType string `sql:"AppType"`
	// 应用版本 当前应用版本 例如:1.0.1.0
	AppVersion string `sql:"AppVersion"`
	// 事件名称 当前事件的名称 例如:OnLoginEvent
	EventName string `sql:"EventName"`
	// 事件时间 触发当前事件的时间点 例如:2018-12-12 08:00:00
	EventTime *time.Time `sql:"EventTime"`
}

func (s *UsabilityEvent) CopyTo(target *doctor.UsabilityEvent) {
	if target == nil {
		return
	}
	target.SerialNo = s.SerialNo
	target.UserName = s.UserName
	target.AppId = s.AppId
	target.AppType = s.AppType
	target.AppVersion = s.AppVersion
	target.EventName = s.EventName
	if s.EventTime == nil {
		target.EventTime = nil
	} else {
		eventTime := types.Time(*s.EventTime)
		target.EventTime = &eventTime
	}
}

func (s *UsabilityEvent) CopyFrom(source *doctor.UsabilityEvent) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.UserName = source.UserName
	s.AppId = source.AppId
	s.AppType = source.AppType
	s.AppVersion = source.AppVersion
	s.EventName = source.EventName
	if source.EventTime == nil {
		s.EventTime = nil
	} else {
		eventTime := time.Time(*source.EventTime)
		s.EventTime = &eventTime
	}
}
