package sqldb

import (
	"tlcbme_project/data/model/doctor"
	"github.com/ktpswjz/httpserver/types"
	"time"
)

type RiskAssessRecordBase struct {
}

func (s RiskAssessRecordBase) TableName() string {
	return "RiskAssessRecord"
}

// 慢病管理业务
// 工作平台管理数据
// 危险评估记录
// 注释：该表保存慢病管理服务发过来的危险评估记录。
type RiskAssessRecord struct {
	RiskAssessRecordBase
	// 序号 主键，自增 例如:324
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 用户ID 关联患者用户登录表用户ID 例如:232442
	PatientID uint64 `sql:"PatientID"`
	// 危险级别 1-低危,2-中危, 3-高危 例如:3
	Level uint64 `sql:"Level"`
	// 评估名称 例如:高血压危险分层评估
	Name *string `sql:"Name"`
	// 备注
	Memo *string `sql:"memo"`
	// 评估时间 慢病管理工作平台接收到该评估的时间 例如:2018-07-06 15:00:00
	ReceiveDateTime *time.Time `sql:"ReceiveDateTime"`
	// 下次评估时间 例如:2018-09-03 12:00:23
	NextSchedule *time.Time `sql:"NextSchedule"`
}

func (s *RiskAssessRecord) CopyTo(target *doctor.RiskAssessRecord) {
	if target == nil {
		return
	}
	target.SerialNo = s.SerialNo
	target.PatientID = s.PatientID
	target.Level = s.Level
	if s.Name != nil {
		target.Name = string(*s.Name)
	}
	if s.Memo != nil {
		target.Memo = string(*s.Memo)
	}
	if s.ReceiveDateTime == nil {
		target.ReceiveDateTime = nil
	} else {
		receiveDateTime := types.Time(*s.ReceiveDateTime)
		target.ReceiveDateTime = &receiveDateTime
	}
	if s.NextSchedule == nil {
		target.NextSchedule = nil
	} else {
		nextSchedule := types.Time(*s.NextSchedule)
		target.NextSchedule = &nextSchedule
	}
}

func (s *RiskAssessRecord) CopyFrom(source *doctor.RiskAssessRecord) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.PatientID = source.PatientID
	s.Level = source.Level
	name := string(source.Name)
	s.Name = &name
	memo := string(source.Memo)
	s.Memo = &memo
	if source.ReceiveDateTime == nil {
		s.ReceiveDateTime = nil
	} else {
		receiveDateTime := time.Time(*source.ReceiveDateTime)
		s.ReceiveDateTime = &receiveDateTime
	}
	if source.NextSchedule == nil {
		s.NextSchedule = nil
	} else {
		nextSchedule := time.Time(*source.NextSchedule)
		s.NextSchedule = &nextSchedule
	}
}
