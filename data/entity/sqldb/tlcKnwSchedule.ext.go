package sqldb

type TlcKnwScheduleFilterBySno struct {
	TlcKnwScheduleBase
	SerialNo uint64 `sql:"SerialNo"`
}
