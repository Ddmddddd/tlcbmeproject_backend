package sqldb

import "tlcbme_project/data/model/doctor"

type SportItemBase struct {
}

func (s SportItemBase) TableName() string {
	return "SportItem"
}

type SportItem struct {
	SportItemBase
	// 序号
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 运动项目分类
	Type *string `sql:"Type"`
	// 运动项目名称
	Name *string `sql:"Name"`
	// 运动强度
	Intensity *string `sql:"Intensity"`
	// 身体活动强度，结合体重、运动时长来计算能量消耗
	Met float64 `sql:"Met"`
}

func (s *SportItem) CopyTo(target *doctor.SportItem) {
	if target == nil {
		return
	}
	target.SerialNo = s.SerialNo
	if s.Type != nil {
		target.Type = string(*s.Type)
	}
	if s.Name != nil {
		target.Name = string(*s.Name)
	}
	if s.Intensity != nil {
		target.Intensity = string(*s.Intensity)
	}
	target.Met = s.Met
}

func (s *SportItem) CopyFrom(source *doctor.SportItem) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	t := source.Type
	s.Type = &t
	name := source.Name
	s.Name = &name
	intensity := string(source.Intensity)
	s.Intensity = &intensity
	s.Met = source.Met
}
