package sqldb

import (
	"tlcbme_project/data/model/doctor"
	"github.com/ktpswjz/httpserver/types"
	"time"
)

type ExpPatientBase struct {
}

func (s ExpPatientBase) TableName() string {
	return "ExpPatient"
}

// 记录在饮食行为实验中的患者
type ExpPatient struct {
	ExpPatientBase
	// 序号
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 用户ID
	PatientID uint64 `sql:"PatientID"`
	// 管理级别，1-实验组，2-对照组
	Type uint64 `sql:"Type"`
	// 管理状态，0-退出管理，1-管理中，2-管理中但有未完成的问卷，3-未完成行动计划选择
	Status uint64 `sql:"Status"`
	// 创建时间
	CreateDateTime *time.Time `sql:"CreateDateTime"`
	// 更新时间
	UpdateDateTime *time.Time `sql:"UpdateDateTime"`
	// 终止原因
	EndReason *string `sql:"endReason"`
}

func (s *ExpPatient) CopyTo(target *doctor.ExpPatient) {
	if target == nil {
		return
	}
	target.SerialNo = s.SerialNo
	target.PatientID = s.PatientID
	target.Type = s.Type
	target.Status = s.Status
	if s.CreateDateTime == nil {
		target.CreateDateTime = nil
	} else {
		createDateTime := types.Time(*s.CreateDateTime)
		target.CreateDateTime = &createDateTime
	}
	if s.UpdateDateTime == nil {
		target.UpdateDateTime = nil
	} else {
		updateDateTime := types.Time(*s.UpdateDateTime)
		target.UpdateDateTime = &updateDateTime
	}
	if s.EndReason != nil {
		target.EndReason = string(*s.EndReason)
	}
}

func (s *ExpPatient) CopyFrom(source *doctor.ExpPatient) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.PatientID = source.PatientID
	s.Type = source.Type
	s.Status = source.Status
	if source.CreateDateTime == nil {
		s.CreateDateTime = nil
	} else {
		createDateTime := time.Time(*source.CreateDateTime)
		s.CreateDateTime = &createDateTime
	}
	if source.UpdateDateTime == nil {
		s.UpdateDateTime = nil
	} else {
		updateDateTime := time.Time(*source.UpdateDateTime)
		s.UpdateDateTime = &updateDateTime
	}
	endReason := string(source.EndReason)
	s.EndReason = &endReason
}

func (s *ExpPatient) CopyToEx(target *doctor.ExpPatientEx) {
	if target == nil {
		return
	}
	target.PatientID = s.PatientID
	target.Type = s.Type
}

func (s *ExpPatient) CopyToTypeAndStatus(target *doctor.ExpPatientTypeAndStatus) {
	if target == nil {
		return
	}
	target.PatientID = s.PatientID
	target.Status = s.Status
	target.Type = s.Type
}

func (s *ExpPatient) CopyToBaseInfo(target *doctor.ExpPatientBaseInfo) {
	if target == nil {
		return
	}
	target.PatientID = s.PatientID
	target.Status = s.Status
}