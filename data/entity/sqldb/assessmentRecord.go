package sqldb

import (
	"tlcbme_project/data/model/doctor"
	"encoding/json"
	"github.com/ktpswjz/httpserver/types"
	"time"
)

type AssessmentRecordBase struct {
}

func (s AssessmentRecordBase) TableName() string {
	return "AssessmentRecord"
}

// 慢病管理业务
// 工作平台管理数据
// 评估记录
// 注释：该表保存评估记录。
type AssessmentRecord struct {
	AssessmentRecordBase
	// 序号 主键，自增 例如:324
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 用户ID 关联患者用户登录表用户ID 例如:232442
	PatientID uint64 `sql:"PatientID"`
	// 评估时间  例如:2018-08-01 10:00:00
	AssessDateTime *time.Time `sql:"AssessDateTime"`
	// 评估名称  例如:高血压危险分层评估
	AssessmentName string `sql:"AssessmentName"`
	// 评估表代码 关联评估表字典代码字段 例如:1
	AssessmentSheetCode uint64 `sql:"AssessmentSheetCode"`
	// 评估表版本号 例如:1
	AssessmentSheetVersion uint64 `sql:"AssessmentSheetVersion"`
	// 评估摘要 摘要规则根据所用评估表不同而不同，一般此处填入评估结果。 例如:中危
	Summary string `sql:"Summary"`
	// 评估表内容 JSON格式保存 例如:
	Content *string `sql:"Content"`
	// 操作者ID 保存用户ID，如果是后台服务自动做的评估，这里填0 例如:322
	OperatorID uint64 `sql:"OperatorID"`
	// 评估表内容 JSON格式保存 例如:
	Others *string `sql:"Others"`
}

func (s *AssessmentRecord) CopyTo(target *doctor.AssessmentRecord) {
	if target == nil {
		return
	}
	target.SerialNo = s.SerialNo
	target.PatientID = s.PatientID
	if s.AssessDateTime == nil {
		target.AssessDateTime = nil
	} else {
		assessDateTime := types.Time(*s.AssessDateTime)
		target.AssessDateTime = &assessDateTime
	}
	target.AssessmentName = s.AssessmentName
	target.AssessmentSheetCode = s.AssessmentSheetCode
	target.AssessmentSheetVersion = s.AssessmentSheetVersion
	target.Summary = s.Summary
	content := ""
	if s.Content != nil {
		content = *s.Content
	}
	if len(content) > 0 {
		json.Unmarshal([]byte(content), &target.Content)
	}
	others := ""
	if s.Others != nil {
		others = *s.Others
	}
	if len(others) > 0 {
		json.Unmarshal([]byte(others), &target.Others)
	}

	target.OperatorID = s.OperatorID
}

func (s *AssessmentRecord) CopyFrom(source *doctor.AssessmentRecord) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.PatientID = source.PatientID
	if source.AssessDateTime == nil {
		s.AssessDateTime = nil
	} else {
		assessDateTime := time.Time(*source.AssessDateTime)
		s.AssessDateTime = &assessDateTime
	}
	s.AssessmentName = source.AssessmentName
	s.AssessmentSheetCode = source.AssessmentSheetCode
	s.AssessmentSheetVersion = source.AssessmentSheetVersion
	s.Summary = source.Summary
	if source.Content != nil {
		contentData, err := json.Marshal(source.Content)
		if err == nil {
			content := string(contentData)
			s.Content = &content
		}
	}
	if source.Others != nil {
		contentData, err := json.Marshal(source.Others)
		if err == nil {
			content := string(contentData)
			s.Others = &content
		}
	}

	s.OperatorID = source.OperatorID
}
