package sqldb

type UsabilityDeviceFilter struct {
	UsabilityDeviceBase

	// 用户名 关联患者用户登录表用户名 例如:aa
	UserName string `sql:"UserName"`
	// 应用标识 唯一标识应用 例如:abcdefghijklmn
	AppId string `sql:"AppId"`
}
