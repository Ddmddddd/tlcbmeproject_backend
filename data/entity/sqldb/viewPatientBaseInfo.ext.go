package sqldb

import (
	"strings"
	"time"
	"tlcbme_project/data/enum"
	"tlcbme_project/data/model/doctor"
)

func (s *ViewPatientBaseInfo) CopyToEx(target *doctor.ViewPatientBaseInfoEx) {
	if target == nil {
		return
	}
	s.CopyTo(&target.ViewPatientBaseInfo)

	if target.FollowupDate != nil {
		target.FollowupDays = target.FollowupDate.GetDays(time.Now())
	}
	if target.LastFollowupDate != nil {
		target.LastFollowupDays = target.LastFollowupDate.GetDays(time.Now())
	}
	if target.ManageStartDateTime != nil {
		target.ManageDays = -target.ManageStartDateTime.GetDays(time.Now())
	}

	if s.Sex != nil {
		target.SexText = enum.Sexes.Value(*s.Sex)
	}
	if s.DateOfBirth != nil {
		age := time.Now().Sub(*s.DateOfBirth)
		target.Age = int64(age.Hours() / (24 * 365))
	}

	tagCount := 0
	tag := &strings.Builder{}
	if s.ManageClass != nil {
		vs := strings.Split(*s.ManageClass, ",")
		for _, v := range vs {
			tv := strings.TrimSpace(v)
			if len(tv) < 1 {
				continue
			}
			tagCount++
			if tagCount > 1 {
				tag.WriteString(" ")
			}
			tag.WriteString(tv)
		}
	}
	if s.PatientFeature != nil {
		vs := strings.Split(*s.PatientFeature, ",")
		for _, v := range vs {
			tv := strings.TrimSpace(v)
			if len(tv) < 1 {
				continue
			}
			tagCount++
			if tagCount > 1 {
				tag.WriteString(" ")
			}
			tag.WriteString(tv)
		}
	}

	if target.Age >= 45 && target.Age < 60 {
		tag.WriteString(" 中年人")
	} else if target.Age >= 60 {
		tag.WriteString(" 老年人")
	}

	if s.GetBmi() != nil {
		bmi := *s.GetBmi()
		if bmi >= 24 && bmi < 27 {
			tag.WriteString(" 超重")
		} else if bmi >= 27 && bmi < 30 {
			tag.WriteString(" 肥胖")
		} else if bmi >= 30 {
			tag.WriteString(" 重度肥胖")
		}
	}

	target.Tag = tag.String()
	target.GoalEnergy = s.GoalEnergy
	target.GoalCarbohydrateMin = s.GoalCarbohydrateMin
	target.GoalCarbohydrateMax = s.GoalCarbohydrateMax
	target.GoalFatMin = s.GoalFatMin
	target.GoalFatMax = s.GoalFatMax
	target.GoalProteinMin = s.GoalProteinMin
	target.GoalProteinMax = s.GoalProteinMax

}

type ViewPatientBaseInfoFilter struct {
	ViewPatientBaseInfoBase

	// 用户ID 关联医生用户登录表用户ID 例如:232442
	PatientID *uint64 `sql:"PatientID"`
	// 管理机构代码 管理该患者的机构 例如:897798
	OrgCode string `sql:"OrgCode"`
}

type ViewPatientBaseInfoListFilter struct {

	// 用户ID 关联医生用户登录表用户ID 例如:232442
	PatientID []uint64 `sql:"PatientID" filter:"NOT IN"`
}

func (s *ViewPatientBaseInfoFilter) CopyFrom(source *doctor.ViewPatientBaseInfoFilter) {
	if source == nil {
		return
	}

	s.PatientID = source.PatientID
}

func (s *ViewPatientBaseInfoFilter) CopyFromEx(source *doctor.ViewPatientBaseInfoFilterEx) {
	if source == nil {
		return
	}
	s.CopyFrom(&source.ViewPatientBaseInfoFilter)

	s.OrgCode = source.OrgCode
}

func (s *ViewPatientBaseInfo) GetBmi() *float64 {
	if s.Weight == nil {
		return nil
	}
	if s.Height == nil {
		return nil
	}

	height := float64(*s.Height) / 100
	bmi := *s.Weight / height / height

	return &bmi
}
