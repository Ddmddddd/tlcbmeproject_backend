package sqldb

import (
	"tlcbme_project/data/model/doctor"
	"time"
)

type HtnManageDetailOrder struct {
	HtnManageDetailBase

	// 管理等级开始时间 当前管理等级开始时间 例如:2018-07-03 13:15:00
	ManageLevelStartDateTime *time.Time `sql:"ManageLevelStartDateTime"`
}

type HtnManageDetailFilter struct {
	HtnManageDetailBase

	// 序号 主键，自增 例如:324
	SerialNo *uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 用户ID 关联患者用户登录表用户ID 例如:232442
	PatientID *uint64 `sql:"PatientID"`
	// 管理等级 当前管理等级，0-新患者，1-一级管理，2-二级管理 例如:0
	ManageLevels []uint64 `sql:"ManageLevel" filter:"in"`
	// 管理等级开始时间 当前管理等级开始时间 例如:2018-07-03 13:15:00
	ManageDateStart *time.Time `sql:"ManageLevelStartDateTime" filter:">="`
	ManageDateEnd   *time.Time `sql:"ManageLevelStartDateTime" filter:"<"`
}

func (s *HtnManageDetailFilter) CopyFrom(source *doctor.HtnManageDetailFilter) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.PatientID = source.PatientID
	if len(source.ManageLevels) > 0 {
		s.ManageLevels = source.ManageLevels
	}
	if source.ManageDateStart != nil {
		s.ManageDateStart = source.ManageDateStart.ToDate(0)
	}
	if source.ManageDateEnd != nil {
		s.ManageDateEnd = source.ManageDateEnd.ToDate(1)
	}
}
