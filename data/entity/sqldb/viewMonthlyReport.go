package sqldb

import (
	"tlcbme_project/data/model/doctor"
	"github.com/ktpswjz/httpserver/types"
	"time"
)

type ViewMonthlyReportBase struct {
}

func (s ViewMonthlyReportBase) TableName() string {
	return "ViewMonthlyReport"
}

// VIEW
type ViewMonthlyReport struct {
	ViewMonthlyReportBase
	// 用户ID 关联患者用户登录表用户ID 例如:232442
	PatientID uint64 `sql:"PatientID"`
	// 月份 年份+月份（2016-09） 例如:2016-09
	YearMonth string `sql:"YearMonth"`
	// 血压实测次数  例如:12
	BpMeasuredTimes uint64 `sql:"BpMeasuredTimes"`
	// 血压应测次数  例如:90
	BpPlanedTimes uint64 `sql:"BpPlanedTimes"`
	// 体重实测次数  例如:20
	WeightMeasuredTimes uint64 `sql:"WeightMeasuredTimes"`
	// 体重应测次数  例如:30
	WeightPlanedTimes uint64 `sql:"WeightPlanedTimes"`
	// 实际服药次数  例如:20
	DrugTakenTimes uint64 `sql:"DrugTakenTimes"`
	// 计划服药次数  例如:30
	DrugPlanedTimes uint64 `sql:"DrugPlanedTimes"`
	// 心率实测次数  例如:12
	HrMeasuredTimes uint64 `sql:"HrMeasuredTimes"`
	// 加入管理天数  例如:1002
	DaysInManagement uint64 `sql:"DaysInManagement"`
	// 当前血压 当月最后一次血压 例如:132/82
	CurrentBP *string `sql:"CurrentBP"`
	// 目标血压 控制血压的目标 例如:140/90
	GoalBP *string `sql:"GoalBP"`
	// 最大收缩压 当月出现的最大收缩压 例如:156
	MaxSBP *uint64 `sql:"MaxSBP"`
	// 最大收缩压时间 当月出现最大收缩压的时间 例如:2016-09-10 12:00:00
	MaxSBPTime *time.Time `sql:"MaxSBPTime"`
	// 最小收缩压 当月出现的最小收缩压 例如:124
	MinSBP *uint64 `sql:"MinSBP"`
	// 最小收缩压时间 当月出现最小收缩压的时间 例如:2016-09-11 12:00:00
	MinSBPTime *time.Time `sql:"MinSBPTime"`
	// 最大舒张压 当月出现的最大舒张压 例如:102
	MaxDBP *uint64 `sql:"MaxDBP"`
	// 最大舒张压时间 当月出现最大舒张压的时间 例如:2016-09-12 12:00:00
	MaxDBPTime *time.Time `sql:"MaxDBPTime"`
	// 最小舒张压 当月出现的最小舒张压 例如:83
	MinDBP *uint64 `sql:"MinDBP"`
	// 最小舒张压时间 当月出现最小舒张压的时间 例如:2016-09-13 12:00:00
	MinDBPTime *time.Time `sql:"MinDBPTime"`
	// 最大脉压差 当月出现的最大脉压差 例如:48
	MaxRange *uint64 `sql:"MaxRange"`
	// 最大脉压差时间 当月出现最大脉压差的时间 例如:2016-09-14 12:00:00
	MaxRangeTime *time.Time `sql:"MaxRangeTime"`
	// 最小脉压差 当月出现的最小脉压差 例如:26
	MinRange *uint64 `sql:"MinRange"`
	// 最小脉压差时间 当月出现最小脉压差的时间 例如:2016-09-15 12:00:00
	MinRangeTime *time.Time `sql:"MinRangeTime"`
	// 平均收缩压 当月的平均收缩压 例如:135
	AvgSBP *uint64 `sql:"AvgSBP"`
	// 平均舒张压 当月的平均舒张压 例如:98
	AvgDBP *uint64 `sql:"AvgDBP"`
	// 平均脉压差 当月的平均脉压差 例如:35
	AvgRange *uint64 `sql:"AvgRange"`
	// 血压正常次数 当月正常血压的次数 例如:6
	BpNormalTimes *uint64 `sql:"BpNormalTimes"`
	// 血压偏高次数 当月偏高血压的次数 例如:4
	BpHigherTimes *uint64 `sql:"BpHigherTimes"`
	// 血压偏低次数 当月偏低血压的次数 例如:2
	BpLowerTimes *uint64 `sql:"BpLowerTimes"`
	// 最大心率 当月出现的最大心率值 例如:108
	MaxHR *uint64 `sql:"MaxHR"`
	// 最大心率时间 当月出现最大心率值的时间 例如:2016-09-10 12:00:00
	MaxHRTime *time.Time `sql:"MaxHRTime"`
	// 最小心率 当月出现的最小心率值 例如:56
	MinHR *uint64 `sql:"MinHR"`
	// 最小心率时间 当月出现最小心率值的时间 例如:2016-09-11 12:00:00
	MinHRTime *time.Time `sql:"MinHRTime"`
	// 平均心率 当月的平均心率值 例如:68
	AvgHR *uint64 `sql:"AvgHR"`
	// 心率正常次数 当月正常心率的次数 例如:6
	HrNormalTimes *uint64 `sql:"HrNormalTimes"`
	// 心率偏高次数 当月偏高心率的次数 例如:4
	HrHigherTimes *uint64 `sql:"hrHigherTimes"`
	// 心率偏低次数 当月偏低心率的次数 例如:2
	HrLowerTimes *uint64 `sql:"HrLowerTimes"`
	// 当前身高 当月的身高 例如:170
	CurrentHeight *uint64 `sql:"CurrentHeight"`
	// 当前体重 当月最后一次体重 例如:60.5
	CurrentWeight *float64 `sql:"CurrentWeight"`
	// 目标体重 当月目标的体重 例如:65
	TargetWeight *float64 `sql:"TargetWeight"`
	// 当前BMI 当月最后一次体重对应的BMI值 例如:22.6
	CurrentBMI *float64 `sql:"CurrentBMI"`
}

func (s *ViewMonthlyReport) CopyTo(target *doctor.ViewMonthlyReport) {
	if target == nil {
		return
	}
	target.PatientID = s.PatientID
	target.YearMonth = s.YearMonth
	target.BpMeasuredTimes = s.BpMeasuredTimes
	target.BpPlanedTimes = s.BpPlanedTimes
	target.WeightMeasuredTimes = s.WeightMeasuredTimes
	target.WeightPlanedTimes = s.WeightPlanedTimes
	target.DrugTakenTimes = s.DrugTakenTimes
	target.DrugPlanedTimes = s.DrugPlanedTimes
	target.HrMeasuredTimes = s.HrMeasuredTimes
	target.DaysInManagement = s.DaysInManagement
	if s.CurrentBP != nil {
		target.CurrentBP = string(*s.CurrentBP)
	}
	if s.GoalBP != nil {
		target.GoalBP = string(*s.GoalBP)
	}
	target.MaxSBP = s.MaxSBP
	if s.MaxSBPTime == nil {
		target.MaxSBPTime = nil
	} else {
		maxSBPTime := types.Time(*s.MaxSBPTime)
		target.MaxSBPTime = &maxSBPTime
	}
	target.MinSBP = s.MinSBP
	if s.MinSBPTime == nil {
		target.MinSBPTime = nil
	} else {
		minSBPTime := types.Time(*s.MinSBPTime)
		target.MinSBPTime = &minSBPTime
	}
	target.MaxDBP = s.MaxDBP
	if s.MaxDBPTime == nil {
		target.MaxDBPTime = nil
	} else {
		maxDBPTime := types.Time(*s.MaxDBPTime)
		target.MaxDBPTime = &maxDBPTime
	}
	target.MinDBP = s.MinDBP
	if s.MinDBPTime == nil {
		target.MinDBPTime = nil
	} else {
		minDBPTime := types.Time(*s.MinDBPTime)
		target.MinDBPTime = &minDBPTime
	}
	target.MaxRange = s.MaxRange
	if s.MaxRangeTime == nil {
		target.MaxRangeTime = nil
	} else {
		maxRangeTime := types.Time(*s.MaxRangeTime)
		target.MaxRangeTime = &maxRangeTime
	}
	target.MinRange = s.MinRange
	if s.MinRangeTime == nil {
		target.MinRangeTime = nil
	} else {
		minRangeTime := types.Time(*s.MinRangeTime)
		target.MinRangeTime = &minRangeTime
	}
	target.AvgSBP = s.AvgSBP
	target.AvgDBP = s.AvgDBP
	target.AvgRange = s.AvgRange
	target.BpNormalTimes = s.BpNormalTimes
	target.BpHigherTimes = s.BpHigherTimes
	target.BpLowerTimes = s.BpLowerTimes
	target.MaxHR = s.MaxHR
	if s.MaxHRTime == nil {
		target.MaxHRTime = nil
	} else {
		maxHRTime := types.Time(*s.MaxHRTime)
		target.MaxHRTime = &maxHRTime
	}
	target.MinHR = s.MinHR
	if s.MinHRTime == nil {
		target.MinHRTime = nil
	} else {
		minHRTime := types.Time(*s.MinHRTime)
		target.MinHRTime = &minHRTime
	}
	target.AvgHR = s.AvgHR
	target.HrNormalTimes = s.HrNormalTimes
	target.HrHigherTimes = s.HrHigherTimes
	target.HrLowerTimes = s.HrLowerTimes
	target.CurrentHeight = s.CurrentHeight
	target.CurrentWeight = s.CurrentWeight
	target.TargetWeight = s.TargetWeight
	target.CurrentBMI = s.CurrentBMI
}

func (s *ViewMonthlyReport) CopyFrom(source *doctor.ViewMonthlyReport) {
	if source == nil {
		return
	}
	s.PatientID = source.PatientID
	s.YearMonth = source.YearMonth
	s.BpMeasuredTimes = source.BpMeasuredTimes
	s.BpPlanedTimes = source.BpPlanedTimes
	s.WeightMeasuredTimes = source.WeightMeasuredTimes
	s.WeightPlanedTimes = source.WeightPlanedTimes
	s.DrugTakenTimes = source.DrugTakenTimes
	s.DrugPlanedTimes = source.DrugPlanedTimes
	s.HrMeasuredTimes = source.HrMeasuredTimes
	s.DaysInManagement = source.DaysInManagement
	currentBP := string(source.CurrentBP)
	s.CurrentBP = &currentBP
	goalBP := string(source.GoalBP)
	s.GoalBP = &goalBP
	s.MaxSBP = source.MaxSBP
	if source.MaxSBPTime == nil {
		s.MaxSBPTime = nil
	} else {
		maxSBPTime := time.Time(*source.MaxSBPTime)
		s.MaxSBPTime = &maxSBPTime
	}
	s.MinSBP = source.MinSBP
	if source.MinSBPTime == nil {
		s.MinSBPTime = nil
	} else {
		minSBPTime := time.Time(*source.MinSBPTime)
		s.MinSBPTime = &minSBPTime
	}
	s.MaxDBP = source.MaxDBP
	if source.MaxDBPTime == nil {
		s.MaxDBPTime = nil
	} else {
		maxDBPTime := time.Time(*source.MaxDBPTime)
		s.MaxDBPTime = &maxDBPTime
	}
	s.MinDBP = source.MinDBP
	if source.MinDBPTime == nil {
		s.MinDBPTime = nil
	} else {
		minDBPTime := time.Time(*source.MinDBPTime)
		s.MinDBPTime = &minDBPTime
	}
	s.MaxRange = source.MaxRange
	if source.MaxRangeTime == nil {
		s.MaxRangeTime = nil
	} else {
		maxRangeTime := time.Time(*source.MaxRangeTime)
		s.MaxRangeTime = &maxRangeTime
	}
	s.MinRange = source.MinRange
	if source.MinRangeTime == nil {
		s.MinRangeTime = nil
	} else {
		minRangeTime := time.Time(*source.MinRangeTime)
		s.MinRangeTime = &minRangeTime
	}
	s.AvgSBP = source.AvgSBP
	s.AvgDBP = source.AvgDBP
	s.AvgRange = source.AvgRange
	s.BpNormalTimes = source.BpNormalTimes
	s.BpHigherTimes = source.BpHigherTimes
	s.BpLowerTimes = source.BpLowerTimes
	s.MaxHR = source.MaxHR
	if source.MaxHRTime == nil {
		s.MaxHRTime = nil
	} else {
		maxHRTime := time.Time(*source.MaxHRTime)
		s.MaxHRTime = &maxHRTime
	}
	s.MinHR = source.MinHR
	if source.MinHRTime == nil {
		s.MinHRTime = nil
	} else {
		minHRTime := time.Time(*source.MinHRTime)
		s.MinHRTime = &minHRTime
	}
	s.AvgHR = source.AvgHR
	s.HrNormalTimes = source.HrNormalTimes
	s.HrHigherTimes = source.HrHigherTimes
	s.HrLowerTimes = source.HrLowerTimes
	s.CurrentHeight = source.CurrentHeight
	s.CurrentWeight = source.CurrentWeight
	s.TargetWeight = source.TargetWeight
	s.CurrentBMI = source.CurrentBMI
}
