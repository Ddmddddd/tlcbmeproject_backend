package sqldb

import "time"

type ViewDiscomfortRecordDataFilter struct {
	ViewDiscomfortRecordBase

	// 用户ID 关联患者用户登录表用户ID 例如:232442
	PatientID uint64 `sql:"PatientID"`

	// 发生时间 不适情况发生的时间 例如:2018-07-03 14:45:00
	HappenStartDate *time.Time `sql:"HappenDateTime" filter:">="`
	HappenEndDate   *time.Time `sql:"HappenDateTime" filter:"<"`
}
