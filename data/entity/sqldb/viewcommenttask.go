package sqldb

import (
	"encoding/json"
	"github.com/ktpswjz/httpserver/types"
	"time"
	"tlcbme_project/data/model/doctor"
)

type ViewCommentTaskBase struct {
}

func (s ViewCommentTaskBase) TableName() string {
	return "viewcommenttask"
}

// VIEW
type ViewCommentTask struct {
	ViewCommentTaskBase
	//
	SerialNo *uint64 `sql:"SerialNo"`
	// 患者id
	PatientID *uint64 `sql:"PatientID"`
	// 患者姓名
	Name *string `sql:"Name"`
	// 对应taskRepository中的serialNo，为0则代表该任务不是按期完成的
	TaskID *uint64 `sql:"TaskID"`
	// 父任务名
	ParentTaskName *string `sql:"ParentTaskName"`
	// 任务名
	TaskName *string `sql:"TaskName"`
	// 创建时间
	CreateDateTime *time.Time `sql:"CreateDateTime"`
	// 任务完成具体记录
	Record *string `sql:"Record"`
	// 医生ID 例如:232
	DoctorID *uint64 `sql:"DoctorID"`
	// 医生姓名 例如:张三
	DoctorName *string `sql:"DoctorName"`
	// 健康管理师ID 例如:232
	HealthManagerID *uint64 `sql:"HealthManagerID"`
	// 健康管理师姓名 例如:李四
	HealthManagerName *string `sql:"HealthManagerName"`
	// 性别 0-未知, 1-男, 2-女，9-未说明 例如:1
	Sex *uint64 `sql:"Sex"`
	// 出生日期 例如:2000-12-12
	DateOfBirth *time.Time `sql:"DateOfBirth"`
	// 患者特点 如有多个，之间用逗号分隔，例如：老年人，肥胖，残疾人 例如:老年人，肥胖
	PatientFeature *string `sql:"PatientFeature"`
	// 依从度 值为0至5，0表示依从度未知 例如:4
	ComplianceRate *uint64 `sql:"ComplianceRate"`
}

func (s *ViewCommentTask) CopyTo(target *doctor.ViewCommentTask) {
	if target == nil {
		return
	}
	target.SerialNo = s.SerialNo
	target.PatientID = s.PatientID
	if s.Name != nil {
		target.Name = string(*s.Name)
	}
	target.TaskID = s.TaskID
	if s.ParentTaskName != nil {
		target.ParentTaskName = string(*s.ParentTaskName)
	}
	if s.TaskName != nil {
		target.TaskName = string(*s.TaskName)
	}
	if s.CreateDateTime == nil {
		target.CreateDateTime = nil
	} else {
		createDateTime := types.Time(*s.CreateDateTime)
		target.CreateDateTime = &createDateTime
	}
	record := ""
	if s.Record != nil {
		record = *s.Record
	}
	if len(record) > 0 {
		json.Unmarshal([]byte(record), &target.Record)
	}

	target.DoctorID = s.DoctorID
	if s.DoctorName != nil {
		target.DoctorName = string(*s.DoctorName)
	}
	target.HealthManagerID = s.HealthManagerID
	if s.HealthManagerName != nil {
		target.HealthManagerName = string(*s.HealthManagerName)
	}
}

func (s *ViewCommentTask) CopyFrom(source *doctor.ViewCommentTask) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.PatientID = source.PatientID
	name := string(source.Name)
	s.Name = &name
	s.TaskID = source.TaskID
	parentTaskName := string(source.ParentTaskName)
	s.ParentTaskName = &parentTaskName
	taskName := string(source.TaskName)
	s.TaskName = &taskName
	if source.CreateDateTime == nil {
		s.CreateDateTime = nil
	} else {
		createDateTime := time.Time(*source.CreateDateTime)
		s.CreateDateTime = &createDateTime
	}
	if source.Record != nil {
		recordData, err := json.Marshal(source.Record)
		if err == nil {
			record := string(recordData)
			s.Record = &record
		}
	}

	s.DoctorID = source.DoctorID
	doctorName := string(source.DoctorName)
	s.DoctorName = &doctorName
	s.HealthManagerID = source.HealthManagerID
	healthManagerName := string(source.HealthManagerName)
	s.HealthManagerName = &healthManagerName
}

func (s *ViewCommentTask) CopyToApp(target *doctor.ExpPatientTaskRecordForApp) {
	if target == nil {
		return
	}
	target.SerialNo = *s.SerialNo
	target.PatientID = *s.PatientID
	target.TaskID = *s.TaskID
	if s.ParentTaskName != nil {
		parentTaskName := string(*s.ParentTaskName)
		target.ParentTaskName = &parentTaskName
	}
	if s.TaskName != nil {
		target.TaskName = string(*s.TaskName)
	}
	if s.CreateDateTime == nil {
		target.CreateDateTime = nil
	} else {
		createDateTime := types.Time(*s.CreateDateTime)
		target.CreateDateTime = &createDateTime
	}
	record := ""
	if s.Record != nil {
		record = *s.Record
	}
	if len(record) > 0 {
		json.Unmarshal([]byte(record), &target.Record)
	}

	if s.DateOfBirth != nil {
		age := time.Now().Sub(*s.DateOfBirth)
		target.Age = int64(age.Hours() / (24 * 365))
	}
	if s.PatientFeature != nil {
		target.Tag = string(*s.PatientFeature)
	}
	if *s.Sex == 1 {
		target.SexText = "男"
	} else if *s.Sex == 2 {
		target.SexText = "女"
	}
	target.ComplianceRate = s.ComplianceRate
	target.PatientName = *s.Name
}
