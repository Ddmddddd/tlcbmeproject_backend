package sqldb

import "tlcbme_project/data/model/doctor"

type ViewApiLogStatBase struct {
}

func (s ViewApiLogStatBase) TableName() string {
	return "ViewApiLogStat"
}

// VIEW
type ViewApiLogStat struct {
	ViewApiLogStatBase
	// 地址 调用接口地址 例如:/auth/info
	Uri string `sql:"Uri"`
	//
	Count uint64 `sql:"Count"`
	//
	AvgTime *float64 `sql:"AvgTime"`
	// 耗时 接口耗时，单位纳秒 例如:1808
	MaxTime *uint64 `sql:"MaxTime"`
	// 耗时 接口耗时，单位纳秒 例如:1808
	MinTime *uint64 `sql:"MinTime"`
}

func (s *ViewApiLogStat) CopyTo(target *doctor.ViewApiLogStat) {
	if target == nil {
		return
	}
	target.Uri = s.Uri
	target.Count = s.Count
	target.AvgTime = s.AvgTime
	target.MaxTime = s.MaxTime
	target.MinTime = s.MinTime
}

func (s *ViewApiLogStat) CopyFrom(source *doctor.ViewApiLogStat) {
	if source == nil {
		return
	}
	s.Uri = source.Uri
	s.Count = source.Count
	s.AvgTime = source.AvgTime
	s.MaxTime = source.MaxTime
	s.MinTime = source.MinTime
}
