package sqldb


type ExpDietProgramRepositoryFilter struct {
	ExpDietProgramRepositoryBase
	// 方案ID
	ProgramID *uint64 `sql:"ProgramID"`
}
