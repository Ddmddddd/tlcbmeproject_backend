package sqldb

import (
	"tlcbme_project/data/model/doctor"
	"encoding/json"
	"github.com/ktpswjz/httpserver/types"
	"time"
)

type ManagementPlanBase struct {
}

func (s ManagementPlanBase) TableName() string {
	return "ManagementPlan"
}

// 慢病管理业务
// 工作平台管理数据
// 管理计划
// 注释：本表保存每个患者的管理计划。同一PlanType，每个患者只能有一个计划处于使用中。
type ManagementPlan struct {
	ManagementPlanBase
	// 序号 主键，自增 例如:324
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 用户ID 关联患者用户登录表用户ID 例如:232442
	PatientID uint64 `sql:"PatientID"`
	// 计划类型 该字段用于标识计划类型，用于表示该计划用于哪种慢病。计划类型为以下值之一：全局、高血压、糖尿病、慢阻肺。其中“全局”表示该计划不仅限于单种慢病。计划类型的值在后续会根据系统需求不断增加。 例如:高血压
	PlanType string `sql:"PlanType"`
	// 计划名称 管理计划的名称，例如：管理计划、高血压管理计划 例如:管理计划
	PlanName *string `sql:"PlanName"`
	// 管理计划内容 JSON格式保存 例如:
	Content string `sql:"Content"`
	// 计划制定时间 例如:2018-07-03 14:45:00
	PlanStartDateTime *time.Time `sql:"PlanStartDateTime"`
	// 计划制定人ID 例如:234
	PlannerID *uint64 `sql:"PlannerID"`
	// 计划制定人姓名 例如:张三
	PlannerName string `sql:"PlannerName"`
	// 最后修改时间 计划最后修改的时间 例如:2018-09-03 14:45:00
	LastModifyDateTime *time.Time `sql:"LastModifyDateTime"`
	// 最后修改者ID 例如:555
	ModifierID *uint64 `sql:"ModifierID"`
	// 最后修改者姓名 例如:李四
	ModifierName *string `sql:"ModifierName"`
	// 计划状态 0-未开始，1-使用中，2-已作废 例如:0
	Status uint64 `sql:"Status"`
}

func (s *ManagementPlan) CopyTo(target *doctor.ManagementPlan) {
	if target == nil {
		return
	}
	target.SerialNo = s.SerialNo
	target.PatientID = s.PatientID
	target.PlanType = s.PlanType
	if s.PlanName != nil {
		target.PlanName = string(*s.PlanName)
	}
	content := s.Content
	if len(content) > 0 {
		json.Unmarshal([]byte(content), &target.Content)
	}

	if s.PlanStartDateTime == nil {
		target.PlanStartDateTime = nil
	} else {
		planStartDateTime := types.Time(*s.PlanStartDateTime)
		target.PlanStartDateTime = &planStartDateTime
	}
	target.PlannerID = s.PlannerID
	target.PlannerName = s.PlannerName
	if s.LastModifyDateTime == nil {
		target.LastModifyDateTime = nil
	} else {
		lastModifyDateTime := types.Time(*s.LastModifyDateTime)
		target.LastModifyDateTime = &lastModifyDateTime
	}
	target.ModifierID = s.ModifierID
	if s.ModifierName != nil {
		target.ModifierName = string(*s.ModifierName)
	}
	target.Status = s.Status
}

func (s *ManagementPlan) CopyFrom(source *doctor.ManagementPlan) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.PatientID = source.PatientID
	s.PlanType = source.PlanType
	planName := string(source.PlanName)
	s.PlanName = &planName
	if source.Content != nil {
		contentData, err := json.Marshal(source.Content)
		if err == nil {
			content := string(contentData)
			s.Content = content
		}
	}

	if source.PlanStartDateTime == nil {
		s.PlanStartDateTime = nil
	} else {
		planStartDateTime := time.Time(*source.PlanStartDateTime)
		s.PlanStartDateTime = &planStartDateTime
	}
	s.PlannerID = source.PlannerID
	s.PlannerName = source.PlannerName
	if source.LastModifyDateTime == nil {
		s.LastModifyDateTime = nil
	} else {
		lastModifyDateTime := time.Time(*source.LastModifyDateTime)
		s.LastModifyDateTime = &lastModifyDateTime
	}
	s.ModifierID = source.ModifierID
	modifierName := string(source.ModifierName)
	s.ModifierName = &modifierName
	s.Status = source.Status
}
