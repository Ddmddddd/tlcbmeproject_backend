package sqldb

import (
	"tlcbme_project/data/model/doctor"
	"tlcbme_project/data/model/manage"
	"github.com/ktpswjz/httpserver/types"
	"time"
)

type ExpPatientBarriersRecordBase struct {
}

func (s ExpPatientBarriersRecordBase) TableName() string {
	return "ExpPatientBarriersRecord"
}

// 健康饮食障碍记录表
// 用于记录各个患者的健康饮食障碍
type ExpPatientBarriersRecord struct {
	ExpPatientBarriersRecordBase
	// 序号
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 患者ID
	PatientID uint64 `sql:"PatientID"`
	// 健康饮食障碍
	Barrier string `sql:"Barrier"`
	// 障碍程度
	Level uint64 `sql:"Level"`
	// 状态，0-存在，1-删除
	Status uint64 `sql:"Status"`
	// 创建时间
	CreateDateTime *time.Time `sql:"CreateDateTime"`
	// 更新时间
	UpdateDateTime *time.Time `sql:"UpdateDateTime"`
}

func (s *ExpPatientBarriersRecord) CopyTo(target *doctor.ExpPatientBarriersRecord) {
	if target == nil {
		return
	}
	target.SerialNo = s.SerialNo
	target.PatientID = s.PatientID
	target.Barrier = s.Barrier
	target.Level = s.Level
	target.Status = s.Status
	if s.CreateDateTime == nil {
		target.CreateDateTime = nil
	} else {
		createDateTime := types.Time(*s.CreateDateTime)
		target.CreateDateTime = &createDateTime
	}
	if s.UpdateDateTime == nil {
		target.UpdateDateTime = nil
	} else {
		updateDateTime := types.Time(*s.UpdateDateTime)
		target.UpdateDateTime = &updateDateTime
	}
}

func (s *ExpPatientBarriersRecord) CopyFrom(source *doctor.ExpPatientBarriersRecord) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.PatientID = source.PatientID
	s.Barrier = source.Barrier
	s.Level = source.Level
	s.Status = source.Status
	if source.CreateDateTime == nil {
		s.CreateDateTime = nil
	} else {
		createDateTime := time.Time(*source.CreateDateTime)
		s.CreateDateTime = &createDateTime
	}
	if source.UpdateDateTime == nil {
		s.UpdateDateTime = nil
	} else {
		updateDateTime := time.Time(*source.UpdateDateTime)
		s.UpdateDateTime = &updateDateTime
	}
}

func (s *ExpPatientBarriersRecord) CopyFromManage(source *manage.InputDataBarrier) {
	if source == nil {
		return
	}
	s.Barrier = source.Barrier
	s.Level = source.Level
}
