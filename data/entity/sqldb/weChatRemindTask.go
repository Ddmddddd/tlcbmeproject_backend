package sqldb

import (
	"tlcbme_project/data/model/doctor"
	"github.com/ktpswjz/httpserver/types"
	"time"
)

type WeChatRemindTaskBase struct {
}

func (s WeChatRemindTaskBase) TableName() string {
	return "WeChatRemindTask"
}

type WeChatRemindTask struct {
	WeChatRemindTaskBase
	// 序号
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 患者id
	PatientID uint64 `sql:"PatientID"`
	// 患者openid，在微信提醒时需要用到
	OpenID *string `sql:"OpenID"`
	// 微信提醒的template_id，在微信提醒时要用到
	TemplateID *string `sql:"TemplateID"`
	// 创建时间
	CreateDateTime *time.Time `sql:"CreateDateTime"`
	// 状态，0-正常，1-弃用
	Status uint64 `sql:"Status"`
	// 提醒内容，1-血压，2-血糖
	Type uint64 `sql:"Type"`
}

func (s *WeChatRemindTask) CopyTo(target *doctor.WeChatRemindTask) {
	if target == nil {
		return
	}
	target.SerialNo = s.SerialNo
	target.PatientID = s.PatientID
	if s.OpenID != nil {
		target.OpenID = string(*s.OpenID)
	}
	if s.TemplateID != nil {
		target.TemplateID = string(*s.TemplateID)
	}
	if s.CreateDateTime == nil {
		target.CreateDateTime = nil
	} else {
		createDateTime := types.Time(*s.CreateDateTime)
		target.CreateDateTime = &createDateTime
	}
	target.Status = s.Status
	target.Type = s.Type
}

func (s *WeChatRemindTask) CopyFrom(source *doctor.WeChatRemindTask) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.PatientID = source.PatientID
	openID := string(source.OpenID)
	s.OpenID = &openID
	templateID := string(source.TemplateID)
	s.TemplateID = &templateID
	if source.CreateDateTime == nil {
		s.CreateDateTime = nil
	} else {
		createDateTime := time.Time(*source.CreateDateTime)
		s.CreateDateTime = &createDateTime
	}
	s.Status = source.Status
	s.Type = source.Type
}

func (s *WeChatRemindTask) CopyFromCreate(source *doctor.WeChatRemindTaskCreate) {
	if source == nil {
		return
	}
	s.PatientID = source.PatientID
	openID := string(source.OpenID)
	s.OpenID = &openID
	templateID := string(source.TemplateID)
	s.TemplateID = &templateID
	if source.CreateDateTime == nil {
		s.CreateDateTime = nil
	} else {
		createDateTime := time.Time(*source.CreateDateTime)
		s.CreateDateTime = &createDateTime
	}
	s.Type = source.Type
}