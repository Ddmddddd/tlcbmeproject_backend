package sqldb

import "tlcbme_project/data/model/doctor"

type DrugDictBase struct {
}

func (s DrugDictBase) TableName() string {
	return "DrugDict"
}

// 平台运维
// 基础字典
// 药品字典
// 注释：本表保存常用药
type DrugDict struct {
	DrugDictBase
	// 序号 主键，自增 例如:324
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 字典项代码  例如:1
	ItemCode uint64 `sql:"ItemCode"`
	// 字典项名称  例如:高血压
	ItemName string `sql:"ItemName"`
	// 规格  例如:10mg/粒X12
	Specification *string `sql:"Specification"`
	// 单位 该药的常用单位mg、片、粒，如有多个，用逗号分隔 例如:mg
	Units *string `sql:"Units"`
	// 功效 例如降压、降糖、调脂 例如:降压
	Effect *string `sql:"Effect"`
	// 排序 用于字典项目排序，从1开始往后排 例如:1
	ItemSortValue *uint64 `sql:"ItemSortValue"`
	// 输入码 用于快速输入，可以是拼音首字母或缩写 例如:gxy
	InputCode *string `sql:"InputCode"`
	// 有效性 用于描述本记录是否有效，0-无效，1-有效，默认值为1 例如:1
	IsValid uint64 `sql:"IsValid"`
}

func (s *DrugDict) CopyTo(target *doctor.DrugDict) {
	if target == nil {
		return
	}
	target.SerialNo = s.SerialNo
	target.ItemCode = s.ItemCode
	target.ItemName = s.ItemName
	if s.Specification != nil {
		target.Specification = string(*s.Specification)
	}
	if s.Units != nil {
		target.Units = string(*s.Units)
	}
	if s.Effect != nil {
		target.Effect = string(*s.Effect)
	}
	target.ItemSortValue = s.ItemSortValue
	if s.InputCode != nil {
		target.InputCode = string(*s.InputCode)
	}
	target.IsValid = s.IsValid
}

func (s *DrugDict) CopyFrom(source *doctor.DrugDict) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.ItemCode = source.ItemCode
	s.ItemName = source.ItemName
	specification := string(source.Specification)
	s.Specification = &specification
	units := string(source.Units)
	s.Units = &units
	effect := string(source.Effect)
	s.Effect = &effect
	s.ItemSortValue = source.ItemSortValue
	inputCode := string(source.InputCode)
	s.InputCode = &inputCode
	s.IsValid = source.IsValid
}
