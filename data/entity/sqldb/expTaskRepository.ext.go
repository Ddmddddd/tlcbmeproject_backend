package sqldb

import (
	"tlcbme_project/data/model/doctor"
	"time"
)

type ExpTaskRepositoryStatusFilter struct {
	ExpTaskRepositoryBase
	// 0-正常，1-弃用
	Status uint64 `sql:"Status"`
}

type ExpTaskRepositorySerialNosFilter struct {
	ExpTaskRepositoryBase
	//
	SerialNos []uint64 `sql:"SerialNo" filter:"in"`
}

type ExpTaskRepositorySerialNoFilter struct {
	ExpTaskRepositoryBase
	//
	SerialNo uint64 `sql:"SerialNo"`
}

type ExpTaskRepositoryTaskOrdersFilter struct {
	ExpTaskRepositoryBase
	// 0-正常，1-弃用
	Status uint64 `sql:"Status"`
	// 为空代表为周任务，否则代表日任务
	ParentTaskID uint64 `sql:"ParentTaskID"`
	// 该值为任务的顺序，例如：1，代表该任务为第一天的任务
	TaskOrders []uint64 `sql:"TaskOrder" filter:"in"`
}

type ExpTaskRepositoryParentTaskIDFilter struct {
	ExpTaskRepositoryBase
	// 0-正常，1-弃用
	Status uint64 `sql:"Status"`
	// 为空代表为周任务，否则代表日任务
	ParentTaskID uint64 `sql:"ParentTaskID"`
}

type ExpTaskRepositoryTaskOrderFilter struct {
	ExpTaskRepositoryBase
	// 0-正常，1-弃用
	Status uint64 `sql:"Status"`
	// 为空代表为周任务，否则代表日任务
	ParentTaskID uint64 `sql:"ParentTaskID"`
	// 该值为任务的顺序，例如：1，代表该任务为第一天的任务
	TaskOrder uint64 `sql:"TaskOrder"`
}

type ExpTaskRepositoryEdit struct {
	ExpTaskRepositoryBase
	//
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 任务名称，包含周任务和日任务
	TaskName string `sql:"TaskName"`
	// 为空代表周任务，否则代表日任务，该值为日任务的顺序，例如：1，代表该任务为第一天的任务
	TaskOrder uint64 `sql:"TaskOrder"`
	// 任务触发时间，为空则没有特定时间
	TaskTime *time.Time `sql:"TaskTime"`
	// 提醒任务，可选为“微信提醒”、“短信提醒”，为空则没有提醒任务
	RemindTask *string `sql:"RemindTask"`
	// 任务目标
	Target *string `sql:"Target"`
	// 编辑者id
	EditorID uint64 `sql:"EditorID"`
	// 更新时间
	UpdateTime *time.Time `sql:"UpdateTime"`
}

func (s *ExpTaskRepositoryEdit) CopyFromEditInput(source *doctor.ExpTaskRepositoryEditInput) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.TaskName = source.TaskName
	s.TaskOrder = source.TaskOrder
	if source.TaskTime == nil {
		s.TaskTime = nil
	} else {
		taskTime := time.Time(*source.TaskTime)
		s.TaskTime = &taskTime
	}
	if source.RemindTask != "" {
		remindTask := string(source.RemindTask)
		s.RemindTask = &remindTask
	}
	if source.Target != "" {
		target := string(source.Target)
		s.Target = &target
	}
	s.EditorID = source.EditorID
	if source.UpdateTime == nil {
		s.UpdateTime = nil
	} else {
		updateTime := time.Time(*source.UpdateTime)
		s.UpdateTime = &updateTime
	}
}