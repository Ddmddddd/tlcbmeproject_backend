package sqldb

import "tlcbme_project/data/model/doctor"

type ViewPatientOrgFilterBase struct {
	ViewPatientOrgBase

	// 用户ID 关联患者用户登录表用户ID 例如:232442
	PatientID *uint64 `sql:"PatientID"`
}

func (s *ViewPatientOrg) CopyToExt(target *doctor.UploadData) {
	if target == nil {
		return
	}
	if s.OrgName != nil {
		target.Hospital = string(*s.OrgName)
	}
	target.PatientName = s.Name
	target.DoctorName = s.DoctorName
}
