package sqldb

import (
	"tlcbme_project/data/model/doctor"
	"encoding/json"
	"github.com/ktpswjz/httpserver/types"
	"time"
)

type ExpScaleRecordBase struct {
}

func (s ExpScaleRecordBase) TableName() string {
	return "ExpScaleRecord"
}

// 患者量表填写记录
// 患者在进入实验后需要完成两份量表填写
// 1. 不良饮食行为量表
// 2. 健康饮食障碍量表
// 其中，不良饮食行为量表为统一量表，健康饮食障碍量表为个性化量表，由前一份量表填写情况生成。
type ExpScaleRecord struct {
	ExpScaleRecordBase
	// 序号
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 用户ID
	PatientID uint64 `sql:"PatientID"`
	// 量表属性，0-默认量表，1-普通量表，2-需要引擎生成生活计划的量表
	ScaleType uint64 `sql:"ScaleType"`
	// 量表ID
	ScaleID uint64 `sql:"ScaleID"`
	// 量表结果
	Result string `sql:"Result"`
	// 提交时间
	CreateDateTime *time.Time `sql:"CreateDateTime"`
}

func (s *ExpScaleRecord) CopyTo(target *doctor.ExpScaleRecord) {
	if target == nil {
		return
	}
	target.SerialNo = s.SerialNo
	target.PatientID = s.PatientID
	target.ScaleType = s.ScaleType
	target.ScaleID = s.ScaleID
	result := s.Result
	if len(result) > 0 {
		json.Unmarshal([]byte(result), &target.Result)
	}
	if s.CreateDateTime == nil {
		target.CreateDateTime = nil
	} else {
		createDateTime := types.Time(*s.CreateDateTime)
		target.CreateDateTime = &createDateTime
	}
}

func (s *ExpScaleRecord) CopyFrom(source *doctor.ExpScaleRecord) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.PatientID = source.PatientID
	s.ScaleType = source.ScaleType
	s.ScaleID = source.ScaleID
	if source.Result != nil {
		resultData, err := json.Marshal(source.Result)
		if err == nil {
			result := string(resultData)
			s.Result = result
		}
	}
	if source.CreateDateTime == nil {
		s.CreateDateTime = nil
	} else {
		createDateTime := time.Time(*source.CreateDateTime)
		s.CreateDateTime = &createDateTime
	}
}

func (s *ExpScaleRecord) CopyFromCreate(source *doctor.ExpScaleRecordCreate) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.PatientID = source.PatientID
	s.ScaleType = source.ScaleType
	s.ScaleID = source.ScaleID
	if source.Result != nil {
		resultData, err := json.Marshal(source.Result)
		if err == nil {
			result := string(resultData)
			s.Result = result
		}
	}
	if source.CreateDateTime == nil {
		s.CreateDateTime = nil
	} else {
		createDateTime := time.Time(*source.CreateDateTime)
		s.CreateDateTime = &createDateTime
	}
}

func (s *ExpScaleRecord) CopyFromDefaultCreate(source *doctor.ExpDefaultScaleRecordCreate) {
	if source == nil {
		return
	}
	s.PatientID = source.PatientID
	s.ScaleType = source.ScaleType
	s.ScaleID = source.ScaleID
	if source.Result != nil {
		resultData, err := json.Marshal(source.Result)
		if err == nil {
			result := string(resultData)
			s.Result = result
		}
	}
	if source.CreateDateTime == nil {
		s.CreateDateTime = nil
	} else {
		createDateTime := time.Time(*source.CreateDateTime)
		s.CreateDateTime = &createDateTime
	}
}
