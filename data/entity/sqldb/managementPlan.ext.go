package sqldb

import (
	"tlcbme_project/data/model/doctor"
	"tlcbme_project/data/model/manage"
	"encoding/json"
	"time"
)

func (s *ManagementPlan) CopyToEx(target *doctor.ManagementPlanEx) {
	if target == nil {
		return
	}
	s.CopyTo(&target.ManagementPlan)
}

func (s *ManagementPlan) CopyFromPush(source *manage.InputDataManagement, patientId uint64) {
	if source == nil {
		return
	}

	s.PatientID = patientId
	s.PlanType = source.Advise.Type
	s.PlanName = &source.Advise.Title
	contentData, err := json.Marshal(source)
	if err == nil {
		content := string(contentData)
		s.Content = content
	}

	if source.Advise.Date == nil {
		time := time.Now()
		s.PlanStartDateTime = &time
	} else {
		planStartDateTime := time.Time(*source.Advise.Date)
		s.PlanStartDateTime = &planStartDateTime
	}
	s.PlannerID = nil
	s.PlannerName = "AI"
	s.LastModifyDateTime = s.PlanStartDateTime
	s.ModifierID = nil
	s.ModifierName = &s.PlannerName
	s.Status = 1
}

type ManagementPlanFilter struct {
	ManagementPlanBase

	// 序号 主键，自增 例如:324
	SerialNo *uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 用户ID 关联患者用户登录表用户ID 例如:232442
	PatientID *uint64 `sql:"PatientID"`
	// 计划类型 该字段用于标识计划类型，用于表示该计划用于哪种慢病。计划类型为以下值之一：全局、高血压、糖尿病、慢阻肺。其中“全局”表示该计划不仅限于单种慢病。计划类型的值在后续会根据系统需求不断增加。 例如:高血压
	PlanType string `sql:"PlanType"`
	// 计划状态 0-未开始，1-使用中，2-已作废 例如:0
	Statuses []uint64 `sql:"Status" filter:"IN"`
}

type ManagementPlanAutoFilter struct {
	ManagementPlanBase

	// 用户ID 关联患者用户登录表用户ID 例如:232442
	PatientID *uint64 `sql:"PatientID"`
	// 计划类型 该字段用于标识计划类型，用于表示该计划用于哪种慢病。计划类型为以下值之一：全局、高血压、糖尿病、慢阻肺。其中“全局”表示该计划不仅限于单种慢病。计划类型的值在后续会根据系统需求不断增加。 例如:高血压
	PlanType string `sql:"PlanType"`
	// 计划状态 0-未开始，1-使用中，2-已作废 例如:0
	Status uint64 `sql:"Status" `
}

func (s *ManagementPlanFilter) CopyFrom(source *doctor.ManagementPlanFilter) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.PatientID = source.PatientID
	s.PlanType = source.PlanType
	if len(source.Statuses) > 0 {
		s.Statuses = source.Statuses
	}
}

type ManagementPlanOrder struct {
	ManagementPlanBase

	// 计划状态 0-未开始，1-使用中，2-已作废 例如:0
	Status uint64 `sql:"Status" index:"1"`

	// 计划制定时间 例如:2018-07-03 14:45:00
	PlanStartDateTime *time.Time `sql:"PlanStartDateTime" order:"DESC" index:"2"`
}

type ManagementPlanUpdateStatus struct {
	ManagementPlanBase

	// 计划状态 0-未开始，1-使用中，2-已作废 例如:0
	Status uint64 `sql:"Status"`
}
type ManagementPlanFilterEx struct {
	ManagementPlanBase

	// 序号 主键，自增 例如:324
	SerialNo *uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 用户ID 关联患者用户登录表用户ID 例如:232442
	PatientID *uint64 `sql:"PatientID"`
	//// 计划制定时间 例如:2018-07-03 14:45:00
	PlanStartDateTime *time.Time `sql:"PlanStartDateTime" `
	// 计划类型 该字段用于标识计划类型，用于表示该计划用于哪种慢病。计划类型为以下值之一：全局、高血压、糖尿病、慢阻肺。其中“全局”表示该计划不仅限于单种慢病。计划类型的值在后续会根据系统需求不断增加。 例如:高血压
	PlanType string `sql:"PlanType"`
	// 计划状态 0-未开始，1-使用中，2-已作废 例如:0
	Statuses []uint64 `sql:"Status" filter:"IN"`
}
type ManagementPlanAdjust struct {
	ManagementPlanBase
	// 用户ID 关联患者用户登录表用户ID 例如:232442
	PatientID uint64 `sql:"PatientID"`
	// 序号 主键，自增 例如:324
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 管理计划内容 JSON格式保存 例如:
	Content string `sql:"Content"`
	// 最后修改时间 计划最后修改的时间 例如:2018-09-03 14:45:00
	LastModifyDateTime *time.Time `sql:"LastModifyDateTime"`
	// 最后修改者ID 例如:555
	ModifierID *uint64 `sql:"ModifierID"`
	// 最后修改者姓名 例如:李四
	ModifierName *string `sql:"ModifierName"`
	// 计划制定人ID 例如:234
	PlannerID *uint64 `sql:"PlannerID"`
	// 计划制定人姓名 例如:张三
	PlannerName string `sql:"PlannerName"`
}

func (s *ManagementPlanAdjust) CopyFrom(source *doctor.ManagementPlanAdjust) {
	if source == nil {
		return
	}
	s.PatientID = source.PatientID
	s.SerialNo = source.SerialNo
	contentData, err := json.Marshal(source.Content)
	if err == nil {
		content := string(contentData)
		s.Content = content
	}

	if source.LastModifyDateTime == nil {
		s.LastModifyDateTime = nil
	} else {
		lastModifyDateTime := time.Time(*source.LastModifyDateTime)
		s.LastModifyDateTime = &lastModifyDateTime
	}
	s.ModifierID = source.ModifierID
	modifierName := string(source.ModifierName)
	s.ModifierName = &modifierName
	s.PlannerID = source.ModifierID
	s.PlannerName = source.ModifierName

}
