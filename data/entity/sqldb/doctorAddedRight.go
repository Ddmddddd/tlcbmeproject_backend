package sqldb

import "tlcbme_project/data/model/doctor"

type DoctorAddedRightBase struct {
}

func (s DoctorAddedRightBase) TableName() string {
	return "DoctorAddedRight"
}

// 平台运维
// 平台用户管理
// 医生用户附加权限表
// 注释：该表保存医生用户的特殊权限。
type DoctorAddedRight struct {
	DoctorAddedRightBase
	// 用户ID 关联医生用户登录表 例如:123
	UserID uint64 `sql:"UserID" primary:"true"`
	// 权限ID 附加权限，1-查看机构所有患者 例如:1
	RightID uint64 `sql:"RightID" primary:"true"`
}

func (s *DoctorAddedRight) CopyTo(target *doctor.DoctorAddedRight) {
	if target == nil {
		return
	}
	target.UserID = s.UserID
	target.RightID = s.RightID
}

func (s *DoctorAddedRight) CopyFrom(source *doctor.DoctorAddedRight) {
	if source == nil {
		return
	}
	s.UserID = source.UserID
	s.RightID = source.RightID
}
