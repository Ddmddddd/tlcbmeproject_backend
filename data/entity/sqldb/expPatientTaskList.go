package sqldb

import (
	"tlcbme_project/data/model/doctor"
	"github.com/ktpswjz/httpserver/types"
	"time"
)

type ExpPatientTaskListBase struct {
}

func (s ExpPatientTaskListBase) TableName() string {
	return "ExpPatientTaskList"
}

// 任务列表
type ExpPatientTaskList struct {
	ExpPatientTaskListBase
	// 序号
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 患者id
	PatientID uint64 `sql:"PatientID"`
	// 任务组，存放数组
	TaskList string `sql:"TaskList"`
	// 创建时间
	CreateDateTime *time.Time `sql:"CreateDateTime"`
	// 更新时间
	UpdateTime *time.Time `sql:"UpdateTime"`
	// 状态，0-正常，1-弃用，2-执行完毕
	Status uint64 `sql:"Status"`
	// 进行到的时间
	DayIndex uint64 `sql:"DayIndex"`
}

func (s *ExpPatientTaskList) CopyTo(target *doctor.ExpPatientTaskList) {
	if target == nil {
		return
	}
	target.SerialNo = s.SerialNo
	target.PatientID = s.PatientID
	target.TaskList = s.TaskList
	if s.CreateDateTime == nil {
		target.CreateDateTime = nil
	} else {
		createDateTime := types.Time(*s.CreateDateTime)
		target.CreateDateTime = &createDateTime
	}
	target.Status = s.Status
	target.DayIndex = s.DayIndex
}

func (s *ExpPatientTaskList) CopyFrom(source *doctor.ExpPatientTaskList) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.PatientID = source.PatientID
	s.TaskList = source.TaskList
	if source.CreateDateTime == nil {
		s.CreateDateTime = nil
	} else {
		createDateTime := time.Time(*source.CreateDateTime)
		s.CreateDateTime = &createDateTime
	}
	s.Status = source.Status
	s.DayIndex = source.DayIndex
}
