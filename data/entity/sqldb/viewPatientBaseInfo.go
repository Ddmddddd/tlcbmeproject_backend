package sqldb

import (
	"encoding/json"
	"github.com/ktpswjz/httpserver/types"
	"time"
	"tlcbme_project/data/model/doctor"
)

type ViewPatientBaseInfoBase struct {
}

func (s ViewPatientBaseInfoBase) TableName() string {
	return "ViewPatientBaseInfo"
}

// VIEW
type ViewPatientBaseInfo struct {
	ViewPatientBaseInfoBase
	// 序号 主键，自增 例如:324
	SerialNo uint64 `sql:"SerialNo"`
	// 用户ID 关联医生用户登录表用户ID 例如:232442
	PatientID uint64 `sql:"PatientID"`
	// 姓名  例如:张三
	Name string `sql:"Name"`
	// 性别 0-未知, 1-男, 2-女，9-未说明 例如:1
	Sex *uint64 `sql:"Sex"`
	// 出生日期 例如:2000-12-12
	DateOfBirth *time.Time `sql:"DateOfBirth"`
	// 身份证号 例如:330106200012129876
	IdentityCardNumber *string `sql:"IdentityCardNumber"`
	// 国籍 GB/T 2659-2000 世界各国和地区名称 例如:中国
	Country *string `sql:"Country"`
	// 民族 GB 3304-1991 中国各民族名称 例如:汉族
	Nation *string `sql:"Nation"`
	// 籍贯  例如:浙江杭州
	NativePlace *string `sql:"NativePlace"`
	// 婚姻状况 GB/T 2261.2-2003 例如:
	MarriageStatus *string `sql:"MarriageStatus"`
	// 文化程度 GB/T 4658-1984 文化程度 例如:大学
	EducationLevel *string `sql:"EducationLevel"`
	// 职业类别 GB/T 6565-1999 职业分类 例如:
	JobType *string `sql:"JobType"`
	// 头像照片 例如:
	Photo []byte `sql:"Photo"`
	// 昵称 例如:蓝精灵
	Nickname *string `sql:"Nickname"`
	// 个性签名 例如:在那山的那边海的那边有一群蓝精灵
	PersonalSign *string `sql:"PersonalSign"`
	// 联系电话 例如:13812344321
	Phone *string `sql:"Phone"`
	// 身高 单位：cm 例如:165
	Height *uint64 `sql:"Height"`
	// 体重 最后一次录入的体重，单位：kg 例如:60.5
	Weight *float64 `sql:"Weight"`
	// 诊断 例如:高血压，糖尿病
	Diagnosis *string `sql:"Diagnosis"`
	// 诊断备注  例如:血压 150/90 mmHg，血糖 7.9 mmol/L
	DiagnosisMemo *string `sql:"DiagnosisMemo"`
	// 患者特点 如有多个，之间用逗号分隔，例如：老年人，肥胖，残疾人 例如:老年人，肥胖
	PatientFeature *string `sql:"PatientFeature"`
	// 管理状态 0-管理中，1-迁出，2-已终止管理 例如:0
	ManageStatus *uint64 `sql:"ManageStatus"`
	// 管理分类 如有多个，之间用逗号分隔，例如：高血压，糖尿病 例如:高血压，糖尿病
	ManageClass *string `sql:"ManageClass"`
	// 扩展信息 保存不同管理分类的扩展信息 例如:{htn:{}, dm:{}}
	Ext *string `sql:"Ext"`
	// 管理机构代码 管理该患者的机构 例如:897798
	OrgCode *string `sql:"OrgCode"`
	// 医生ID 例如:232
	DoctorID *uint64 `sql:"DoctorID"`
	// 医生姓名 例如:张三
	DoctorName *string `sql:"DoctorName"`
	// 健康管理师ID 例如:232
	HealthManagerID *uint64 `sql:"HealthManagerID"`
	// 健康管理师姓名 例如:李四
	HealthManagerName *string `sql:"HealthManagerName"`
	// 管理开始时间 最近一次管理的开始时间，如果是刚注册，则默认为审核通过时间 例如:2018-07-03 13:15:00
	ManageStartDateTime *time.Time `sql:"ManageStartDateTime"`
	// 依从度 值为0至5，0表示依从度未知 例如:4
	ComplianceRate *uint64 `sql:"ComplianceRate"`
	// 管理终止时间 该字段只有当ManageStatus字段为2时才有意义 例如:2018-08-03 13:15:00
	ManageEndDateTime *time.Time `sql:"ManageEndDateTime"`
	// 管理终止的原因 该字段只有当ManageStatus字段为2时才有意义 例如:亡故（2018-08-03，车祸死亡）
	ManageEndReason *string `sql:"ManageEndReason"`
	// 累计随访次数 本次管理的累计随访次数 例如:3
	FollowupTimes *uint64 `sql:"FollowupTimes"`
	// 上次随访日期 例如:2018-06-15
	LastFollowupDate *time.Time `sql:"LastFollowupDate"`
	//
	FollowupDate *time.Time `sql:"FollowupDate"`
	// 体重 最后一次录入的体重，单位：kg 例如:60.5
	Waistline *float64 `sql:"Waistline"`

	//
	GoalEnergy          *uint64 `sql:"GoalEnergy"`
	GoalCarbohydrateMin *uint64 `sql:"GoalCarbohydrateMin"`
	GoalCarbohydrateMax *uint64 `sql:"GoalCarbohydrateMax"`
	GoalFatMin          *uint64 `sql:"GoalFatMin"`
	GoalFatMax          *uint64 `sql:"GoalFatMax"`
	GoalProteinMin      *uint64 `sql:"GoalProteinMin"`
	GoalProteinMax      *uint64 `sql:"GoalProteinMax"`
}

func (s *ViewPatientBaseInfo) CopyTo(target *doctor.ViewPatientBaseInfo) {
	if target == nil {
		return
	}
	target.SerialNo = s.SerialNo
	target.PatientID = s.PatientID
	target.Name = s.Name
	target.Sex = s.Sex
	if s.DateOfBirth == nil {
		target.DateOfBirth = nil
	} else {
		dateOfBirth := types.Time(*s.DateOfBirth)
		target.DateOfBirth = &dateOfBirth
	}
	if s.IdentityCardNumber != nil {
		target.IdentityCardNumber = string(*s.IdentityCardNumber)
	}
	if s.Country != nil {
		target.Country = string(*s.Country)
	}
	if s.Nation != nil {
		target.Nation = string(*s.Nation)
	}
	if s.NativePlace != nil {
		target.NativePlace = string(*s.NativePlace)
	}
	if s.MarriageStatus != nil {
		target.MarriageStatus = string(*s.MarriageStatus)
	}
	if s.EducationLevel != nil {
		target.EducationLevel = string(*s.EducationLevel)
	}
	if s.JobType != nil {
		target.JobType = string(*s.JobType)
	}
	target.Photo = string(s.Photo)
	if s.Nickname != nil {
		target.Nickname = string(*s.Nickname)
	}
	if s.PersonalSign != nil {
		target.PersonalSign = string(*s.PersonalSign)
	}
	if s.Phone != nil {
		target.Phone = string(*s.Phone)
	}
	target.Height = s.Height
	target.Weight = s.Weight
	target.Waistline = s.Waistline
	if s.Diagnosis != nil {
		target.Diagnosis = string(*s.Diagnosis)
	}
	if s.DiagnosisMemo != nil {
		target.DiagnosisMemo = string(*s.DiagnosisMemo)
	}
	if s.PatientFeature != nil {
		target.PatientFeature = string(*s.PatientFeature)
	}
	target.ManageStatus = s.ManageStatus
	if s.ManageClass != nil {
		target.ManageClass = string(*s.ManageClass)
	}
	ext := ""
	if s.Ext != nil {
		ext = *s.Ext
	}
	if len(ext) > 0 {
		json.Unmarshal([]byte(ext), &target.Ext)
	}

	if s.OrgCode != nil {
		target.OrgCode = string(*s.OrgCode)
	}
	target.DoctorID = s.DoctorID
	if s.DoctorName != nil {
		target.DoctorName = string(*s.DoctorName)
	}
	target.HealthManagerID = s.HealthManagerID
	if s.HealthManagerName != nil {
		target.HealthManagerName = string(*s.HealthManagerName)
	}
	if s.ManageStartDateTime == nil {
		target.ManageStartDateTime = nil
	} else {
		manageStartDateTime := types.Time(*s.ManageStartDateTime)
		target.ManageStartDateTime = &manageStartDateTime
	}
	target.ComplianceRate = s.ComplianceRate
	if s.ManageEndDateTime == nil {
		target.ManageEndDateTime = nil
	} else {
		manageEndDateTime := types.Time(*s.ManageEndDateTime)
		target.ManageEndDateTime = &manageEndDateTime
	}
	if s.ManageEndReason != nil {
		target.ManageEndReason = string(*s.ManageEndReason)
	}
	target.FollowupTimes = s.FollowupTimes
	if s.LastFollowupDate == nil {
		target.LastFollowupDate = nil
	} else {
		lastFollowupDate := types.Time(*s.LastFollowupDate)
		target.LastFollowupDate = &lastFollowupDate
	}
	if s.FollowupDate == nil {
		target.FollowupDate = nil
	} else {
		followupDate := types.Time(*s.FollowupDate)
		target.FollowupDate = &followupDate
	}
}

func (s *ViewPatientBaseInfo) CopyFrom(source *doctor.ViewPatientBaseInfo) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.PatientID = source.PatientID
	s.Name = source.Name
	s.Sex = source.Sex
	if source.DateOfBirth == nil {
		s.DateOfBirth = nil
	} else {
		dateOfBirth := time.Time(*source.DateOfBirth)
		s.DateOfBirth = &dateOfBirth
	}
	identityCardNumber := string(source.IdentityCardNumber)
	s.IdentityCardNumber = &identityCardNumber
	country := string(source.Country)
	s.Country = &country
	nation := string(source.Nation)
	s.Nation = &nation
	nativePlace := string(source.NativePlace)
	s.NativePlace = &nativePlace
	marriageStatus := string(source.MarriageStatus)
	s.MarriageStatus = &marriageStatus
	educationLevel := string(source.EducationLevel)
	s.EducationLevel = &educationLevel
	jobType := string(source.JobType)
	s.JobType = &jobType
	s.Photo = []byte(source.Photo)
	nickname := string(source.Nickname)
	s.Nickname = &nickname
	personalSign := string(source.PersonalSign)
	s.PersonalSign = &personalSign
	phone := string(source.Phone)
	s.Phone = &phone
	s.Height = source.Height
	s.Weight = source.Weight
	s.Waistline = source.Waistline
	diagnosis := string(source.Diagnosis)
	s.Diagnosis = &diagnosis
	diagnosisMemo := string(source.DiagnosisMemo)
	s.DiagnosisMemo = &diagnosisMemo
	patientFeature := string(source.PatientFeature)
	s.PatientFeature = &patientFeature
	s.ManageStatus = source.ManageStatus
	manageClass := string(source.ManageClass)
	s.ManageClass = &manageClass
	if source.Ext != nil {
		extData, err := json.Marshal(source.Ext)
		if err == nil {
			ext := string(extData)
			s.Ext = &ext
		}
	}

	orgCode := string(source.OrgCode)
	s.OrgCode = &orgCode
	s.DoctorID = source.DoctorID
	doctorName := string(source.DoctorName)
	s.DoctorName = &doctorName
	s.HealthManagerID = source.HealthManagerID
	healthManagerName := string(source.HealthManagerName)
	s.HealthManagerName = &healthManagerName
	if source.ManageStartDateTime == nil {
		s.ManageStartDateTime = nil
	} else {
		manageStartDateTime := time.Time(*source.ManageStartDateTime)
		s.ManageStartDateTime = &manageStartDateTime
	}
	s.ComplianceRate = source.ComplianceRate
	if source.ManageEndDateTime == nil {
		s.ManageEndDateTime = nil
	} else {
		manageEndDateTime := time.Time(*source.ManageEndDateTime)
		s.ManageEndDateTime = &manageEndDateTime
	}
	manageEndReason := string(source.ManageEndReason)
	s.ManageEndReason = &manageEndReason
	s.FollowupTimes = source.FollowupTimes
	if source.LastFollowupDate == nil {
		s.LastFollowupDate = nil
	} else {
		lastFollowupDate := time.Time(*source.LastFollowupDate)
		s.LastFollowupDate = &lastFollowupDate
	}
	if source.FollowupDate == nil {
		s.FollowupDate = nil
	} else {
		followupDate := time.Time(*source.FollowupDate)
		s.FollowupDate = &followupDate
	}
}

func (s *ViewPatientBaseInfo) CopyToApp(target *doctor.ExpPatientTaskRecordForApp) {
	if target == nil {
		return
	}
	target.PatientID = s.PatientID
	target.PatientName = s.Name
	if s.DateOfBirth != nil {
		age := time.Now().Sub(*s.DateOfBirth)
		target.Age = int64(age.Hours() / (24 * 365))
	}
	if s.PatientFeature != nil {
		target.Tag = string(*s.PatientFeature)
	}
	if *s.Sex == 1 {
		target.SexText = "男"
	} else if *s.Sex == 2 {
		target.SexText = "女"
	}
	target.ComplianceRate = s.ComplianceRate
}
