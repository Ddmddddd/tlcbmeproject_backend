package sqldb

import "tlcbme_project/data/model/doctor"

type ExpNutritionQuestionRepositoryBase struct {
}

func (s ExpNutritionQuestionRepositoryBase) TableName() string {
	return "ExpNutritionQuestionRepository"
}

type ExpNutritionQuestionRepository struct {
	ExpNutritionQuestionRepositoryBase
	//
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	//
	Title *string `sql:"Title"`
	//
	Option *string `sql:"Option"`
	//
	QuestionLevel *uint64 `sql:"QuestionLevel"`
}

func (s *ExpNutritionQuestionRepository) CopyTo(target *doctor.ExpNutritionQuestionRepository) {
	if target == nil {
		return
	}
	target.SerialNo = s.SerialNo
	if s.Title != nil {
		target.Title = string(*s.Title)
	}
	if s.Option != nil {
		target.Option = string(*s.Option)
	}
	target.QuestionLevel = s.QuestionLevel
}

func (s *ExpNutritionQuestionRepository) CopyFrom(source *doctor.ExpNutritionQuestionRepository) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	title := string(source.Title)
	s.Title = &title
	option := string(source.Option)
	s.Option = &option
	s.QuestionLevel = source.QuestionLevel
}
