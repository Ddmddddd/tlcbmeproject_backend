package sqldb

type TlcHeEduQuestionBase struct {
}

func (s TlcHeEduQuestionBase) TableName() string {
	return "tlcheeduquestion"
}

type TlcHeEduQuestion struct {
	TlcHeEduQuestionBase
	Id           uint64 `sql:"id" auto:"true" primary:"true"`
	Type         uint64 `sql:"type"`
	Question     string `sql:"question"`
	Reply        uint64 `sql:"reply"`
	Explanation  string `sql:"explanation"`
	Knowledge_id uint64 `sql:"knowledge_id"`
}
