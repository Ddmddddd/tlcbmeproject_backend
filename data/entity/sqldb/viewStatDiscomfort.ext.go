package sqldb

import (
	"tlcbme_project/data/model/doctor"
	"time"
)

type ViewStatDiscomfortOrder struct {
	ViewStatDiscomfortBase

	Count uint64 `sql:"Count" order:"DESC"`
}

type ViewStatDiscomfortFilter struct {
	ViewStatDiscomfortBase

	// 用户ID 关联患者用户登录表用户ID 例如:232442
	PatientID uint64 `sql:"PatientID"`
	// 发生时间 不适情况发生的时间 例如:2018-07-03 14:45:00
	HappenDateTimeStart *time.Time `sql:"HappenDateTimeStart" filter:">="`
	// 发生时间 不适情况发生的时间 例如:2018-07-03 14:45:00
	HappenDateTimeEnd *time.Time `sql:"HappenDateTimeEnd" filter:"<"`
}

func (s *ViewStatDiscomfortFilter) CopyFrom(source *doctor.ViewStatDiscomfortFilter) {
	if source == nil {
		return
	}
	s.PatientID = source.PatientID
	if source.HappenDateTimeStart != nil {
		s.HappenDateTimeStart = source.HappenDateTimeStart.ToDate(0)
	}
	if source.HappenDateTimeEnd != nil {
		s.HappenDateTimeEnd = source.HappenDateTimeEnd.ToDate(1)
	}
}
