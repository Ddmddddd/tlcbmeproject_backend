package sqldb

import "time"

type TlcTeamGroupTaskFilter struct {
	TlcTeamGroupTaskBase
	BelongToGroup string `sql:"BelongToGroup"`
	Valid         int    `sql:"Valid"`
}

type TlcTeamGroupTaskFilterByGroup struct {
	TlcTeamGroupTaskBase
	BelongToGroup string `sql:"BelongToGroup"`
}

type TlcTeamGroupTaskInsert struct {
	TlcTeamGroupTaskBase
	BelongToGroup string     `sql:"BelongToGroup"`
	TaskSno       uint64     `sql:"TaskSno"`
	Valid         int        `sql:"Valid"`
	CreateTime    *time.Time `sql:"CreateTime"`
	UpdateTime    *time.Time `sql:"UpdateTime"`
}

type TlcTeamGroupTaskStopUpdate struct {
	TlcTeamGroupTaskBase
	Valid      int        `sql:"Valid"`
	UpdateTime *time.Time `sql:"UpdateTime"`
}
