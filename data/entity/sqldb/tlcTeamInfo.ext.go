package sqldb

import "time"

type TlcTeamInfoFilterByGenerateID struct {
	TlcTeamInfoBase
	TeamGenerateID string `sql:"TeamGenerateID"`
}

type TlcTeamInfoFilterByCreatorID struct {
	TlcTeamInfoBase
	CreatorID uint64 `sql:"CreatorID"`
}

type TlcTeamInfoCreate struct {
	TlcTeamInfoBase
	TeamNickName   string     `sql:"TeamNickName"`
	BelongToGroup  string     `sql:"BelongToGroup"`
	MemNumUp       uint64     `sql:"MemNumUp"`
	TeamGenerateID string     `sql:"TeamGenerateID"`
	CreatorID      uint64     `sql:"CreatorID"`
	CreateTime     *time.Time `sql:"CreateTime"`
}

type TlcTeamInfoSerialNoOrder struct {
	TlcTeamInfoBase
	SerialNo uint64 `sql:"SerialNo" order:"desc"`
}

type TlcTeamInfoFilterByGroup struct {
	TlcTeamInfoBase
	BelongToGroup string `sql:"BelongToGroup"`
}

type TlcTeamInfoFilterBySearchInput struct {
	TlcTeamInfoBase
	TeamNickName   string `sql:"TeamNickName" filter:"like"`
	TeamGenerateID string `sql:"TeamGenerateID"`
}

type TlcTeamInfoFilterByTeamSerialNo struct {
	TlcTeamInfoBase
	SerialNo uint64 `sql:"SerialNo"`
}

type TlcTeamInfoGroup struct {
	TlcTeamInfoBase
	BelongToGroup string `sql:"BelongToGroup"`
}

type TlcTeamInfoTeamNickName struct {
	TlcTeamInfoBase
	TeamNickName string `sql:"TeamNickName"`
}

type TlcTeamInfoFilterByTime struct {
	TlcTeamInfoBase
	CreateTime *time.Time `sql:"CreateTime" filter:">="`
}
