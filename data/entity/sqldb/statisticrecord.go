package sqldb

import (
	"bytes"
	"tlcbme_project/data/model/doctor"
	"tlcbme_project/data/model/stat"
	"encoding/gob"
	"fmt"
	"github.com/ktpswjz/httpserver/types"
	"time"
)

type StatisticrecordBase struct {
}

func (s StatisticrecordBase) TableName() string {
	return "StatisticRecord"
}

func (s StatisticrecordBase) SetFilter(v interface{}) {

}

func (s *StatisticrecordBase) Clone() (interface{}, error) {
	return &StatisticrecordBase{}, nil
}

type Statisticrecord struct {
	StatisticrecordBase
	// 序号
	Serialno uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 统计类型，1-管理人数，2-活跃人数，3-血压数据，4-预警血压数据
	Type uint64 `sql:"Type"`
	// 年
	Year uint64 `sql:"Year"`
	// 月
	Month uint64 `sql:"Month"`
	// 数据量
	Count *uint64 `sql:"Count"`
	// 达标率、当日解决率
	Rate *float64 `sql:"Rate"`
	//
	Orgcode string `sql:"OrgCode"`
}

func (s *Statisticrecord) Clone() (interface{}, error) {
	t := &Statisticrecord{}

	buf := new(bytes.Buffer)
	enc := gob.NewEncoder(buf)
	dec := gob.NewDecoder(buf)
	err := enc.Encode(s)
	if err != nil {
		return nil, err
	}
	err = dec.Decode(t)
	if err != nil {
		return nil, err
	}

	return t, nil
}

func (s *Statisticrecord) CopyTo(target *doctor.Statisticrecord) {
	if target == nil {
		return
	}
	target.Serialno = s.Serialno
	target.Type = s.Type
	target.Year = s.Year
	target.Month = s.Month
	target.Count = s.Count
	target.Rate = s.Rate
	target.Orgcode = s.Orgcode
}

func (s *Statisticrecord) CopyFrom(source *doctor.Statisticrecord) {
	if source == nil {
		return
	}
	s.Serialno = source.Serialno
	s.Type = source.Type
	s.Year = source.Year
	s.Month = source.Month
	s.Count = source.Count
	s.Rate = source.Rate
	s.Orgcode = source.Orgcode
}
func (s *Statisticrecord) CopyToExt(out *stat.StatOut) {
	out.CountType = s.Type
	if s.Count != nil {
		out.Value = fmt.Sprint(*s.Count)
	} else {
		out.Value = fmt.Sprint(*s.Rate)
	}
	out.Date = types.Time(time.Date(int(s.Year), time.Month(s.Month), 1, 0, 0, 0, 0, time.Local))

}
