package sqldb

import "tlcbme_project/data/model/doctor"

type ExpFoodGIBase struct {
}

func (s ExpFoodGIBase) TableName() string {
	return "ExpFoodGI"
}

// 食物GI库
// 注释：本表只用作查询不用作修改
type ExpFoodGI struct {
	ExpFoodGIBase
	// 序号
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 食物名称
	FoodName string `sql:"FoodName"`
	// GI值
	GlycemicIndex uint64 `sql:"GlycemicIndex"`
	// 食物类型
	FoodType string `sql:"FoodType"`
}

func (s *ExpFoodGI) CopyTo(target *doctor.ExpFoodGI) {
	if target == nil {
		return
	}
	target.SerialNo = s.SerialNo
	target.FoodName = s.FoodName
	target.GlycemicIndex = s.GlycemicIndex
	target.FoodType = s.FoodType
}

func (s *ExpFoodGI) CopyFrom(source *doctor.ExpFoodGI) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.FoodName = source.FoodName
	s.GlycemicIndex = source.GlycemicIndex
	s.FoodType = source.FoodType
}
