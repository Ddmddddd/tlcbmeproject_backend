package sqldb

import (
	"tlcbme_project/data/model/doctor"
	"strings"
	"time"
)

type ExpQuestionKnowledgeIDFilter struct {
	ExpQuestionToKnowledgeBase
	// 链接的知识
	KnowledgeID uint64 `sql:"KnowledgeID"`
	// 状态
	Status uint64 `sql:"Status"`
}

type ExpQuestionToKnowledgeDelete struct {
	ExpQuestionToKnowledgeBase
	// 序号
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 编辑者id
	EditorID uint64 `sql:"EditorID"`
	// 状态
	Status uint64 `sql:"Status"`
	// 更新时间
	UpdateTime *time.Time `sql:"UpdateTime"`
}

func (s *ExpQuestionToKnowledgeDelete) CopyFromDeleteInput(source *doctor.ExpQuestionToKnowledgeDeleteInput) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.EditorID = source.EditorID
	s.Status = source.Status
	if source.UpdateTime == nil {
		s.UpdateTime = nil
	} else {
		updateTime := time.Time(*source.UpdateTime)
		s.UpdateTime = &updateTime
	}
}

type ExpQuestionToKnowledgeEdit struct {
	ExpQuestionToKnowledgeBase
	// 序号
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 问题
	Question string `sql:"Question"`
	// 答案列表
	AnswerList string `sql:"AnswerList"`
	// 正确答案
	CorrectIndex uint64 `sql:"CorrectIndex"`
	// 编辑者id
	EditorID uint64 `sql:"EditorID"`
	// 更新时间
	UpdateTime *time.Time `sql:"UpdateTime"`
}

func (s *ExpQuestionToKnowledgeEdit) CopyFromEditInput(source *doctor.ExpQuestionToKnowledgeEditInput) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.Question = source.Question
	s.AnswerList = strings.Join(source.AnswerList, ",")
	s.CorrectIndex = source.CorrectIndex
	s.EditorID = source.EditorID
	if source.UpdateTime == nil {
		s.UpdateTime = nil
	} else {
		updateTime := time.Time(*source.UpdateTime)
		s.UpdateTime = &updateTime
	}
}