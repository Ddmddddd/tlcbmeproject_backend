package sqldb

type ExpPatientBarriersRecordStatusUpdate struct {
	// 状态，0-存在，1-已克服，9-删除
	Status uint64 `sql:"Status"`
}

type ExpPatientBarriersRecordPatientIDAndStatusFilter struct {
	// 患者ID
	PatientID uint64 `sql:"PatientID"`
	// 状态，0-存在，1-已克服，9-删除
	Status uint64 `sql:"Status"`
}