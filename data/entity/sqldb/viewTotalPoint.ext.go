package sqldb

type ViewTotalPointOrder struct {
	ViewTotalPointBase
	//
	TotalPoint uint64 `sql:"totalPoint" order:"desc"`
}

type ViewTotalPointFilter struct {
	ViewTotalPointBase

	PatientID uint64 `sql:"patientID"`
}

type ViewTotalPointUpdate struct {
	ViewTotalPointBase

	TotalPoint uint64 `sql:"totalPoint"`
}
