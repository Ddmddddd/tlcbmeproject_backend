package sqldb

import "tlcbme_project/data/model/doctor"

type ExpJinScaleBase struct {
}

func (s ExpJinScaleBase) TableName() string {
	return "ExpJinScale"
}

type ExpJinScale struct {
	ExpJinScaleBase
	//
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 问卷名称
	ScaleName string `sql:"ScaleName"`
	// 跳转链接
	LinkedUrl string `sql:"LinkedUrl"`
	// 问卷描述
	Description *string `sql:"Description"`
	//电话号码对应的api code
	PhoneField string `sql:"PhoneField"`
}

func (s *ExpJinScale) CopyTo(target *doctor.ExpJinScale) {
	if target == nil {
		return
	}
	target.SerialNo = s.SerialNo
	target.ScaleName = s.ScaleName
	target.LinkedUrl = s.LinkedUrl
	if s.Description != nil {
		target.Description = string(*s.Description)
	}
	target.PhoneField = s.PhoneField
}

func (s *ExpJinScale) CopyFrom(source *doctor.ExpJinScale) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.ScaleName = source.ScaleName
	s.LinkedUrl = source.LinkedUrl
	description := string(source.Description)
	s.Description = &description
	s.PhoneField = source.PhoneField
}
