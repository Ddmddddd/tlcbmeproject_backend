package sqldb

import (
	"tlcbme_project/data/model/doctor"
	"time"
)

type ExpActionPlanRecordFilter struct {
	ExpActionPlanRecordBase
	// 序号
	SerialNo *uint64 `sql:"SerialNo"  auto:"true" primary:"true"`
}

type ExpActionPlanRecordGetFilter struct {
	ExpActionPlanRecordBase
	// 用户ID
	PatientID uint64 `sql:"PatientID"`
	// 对应ExpActionPlan的SerialNo，用于查询、修改等操作
	ActionPlanID []uint64 `sql:"ActionPlanID" filter:"in"`
	// 创建时间
	CreateDateTime *time.Time `sql:"CreateDateTime" filter:">="`
}

func (s *ExpActionPlanRecordGetFilter) CopyFromFilter(source *doctor.ExpActionPlanRecordFilter) {
	if source == nil {
		return
	}
	s.PatientID = source.PatientID
	s.ActionPlanID = source.ActionPlanIDList
	if source.StartDateTime == nil {
		s.CreateDateTime = nil
	} else {
		createDateTime := time.Time(*source.StartDateTime)
		s.CreateDateTime = &createDateTime
	}
}


type ExpActionPlanRecordOrder struct {
	ExpActionPlanRecordBase
	// 对应ExpActionPlan的SerialNo，用于查询、修改等操作
	ActionPlanID uint64 `sql:"ActionPlanID" order:"asc"`
}

type ExpActionPlanRecordEdit struct {
	ExpActionPlanRecordBase
	// 行动计划
	ActionPlan string `sql:"ActionPlan"`
	// 优先级，分为0-5，数值越大优先级越高
	Level uint64 `sql:"Level"`
}

func (s *ExpActionPlanRecordEdit) CopyFromEdit(source *doctor.ExpPatientActionPlanEdit) {
	if source == nil {
		return
	}
	s.ActionPlan = source.ActionPlan
	s.Level = source.Level
}

type ExpActionPlanRecordEditFilter struct {
	ExpActionPlanRecordBase
	// 对应ExpActionPlan的SerialNo，用于查询、修改等操作
	ActionPlanID uint64 `sql:"ActionPlanID"`
	// 创建时间
	CreateDateTime *time.Time `sql:"CreateDateTime" filter:">="`
}

type ExpActionPlanRecordEditForApp struct {
	ExpActionPlanRecordBase
	// 序号
	SerialNo uint64 `sql:"SerialNo"  auto:"true" primary:"true"`
	// 行动计划
	ActionPlan string `sql:"ActionPlan"`
	// 行动计划完成情况，0-少量完成，1-部分完成，2-全部完成
	Completion uint64 `sql:"Completion"`
	// 创建时间
	CreateDateTime *time.Time `sql:"CreateDateTime"`
	// 对应ExpActionPlan的SerialNo，用于查询、修改等操作
	ActionPlanID uint64 `sql:"ActionPlanID"`
}

func (s *ExpActionPlanRecordEditForApp) CopyFromEdit(source *doctor.ExpActionPlanRecordEditForApp) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.ActionPlan = source.ActionPlan
	s.Completion = source.Completion
	if source.CreateDateTime == nil {
		s.CreateDateTime = nil
	} else {
		createDateTime := time.Time(*source.CreateDateTime)
		s.CreateDateTime = &createDateTime
	}
	s.ActionPlanID = source.ActionPlanID
}

type ExpActionPlanRecordInFilter struct {
	ExpActionPlanRecordBase
	// 序号
	SerialNo []uint64 `sql:"SerialNo"  filter:"in"`
}

type ExpActionPlanRecordForDoctorFilter struct {
	ExpActionPlanRecordBase
	// 对应ExpActionPlan的SerialNo，用于查询、修改等操作
	ActionPlanID uint64 `sql:"ActionPlanID"`
	// 创建时间
	CreateDateTime *time.Time `sql:"CreateDateTime" filter:">="`
	// 行动计划完成情况，0-少量完成，1-部分完成，2-全部完成
	Completion uint64 `sql:"Completion"`
}