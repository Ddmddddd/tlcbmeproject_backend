package sqldb

import "tlcbme_project/data/model/doctor"

type ViewMonthlyReportFilter struct {
	ViewMonthlyReportBase

	// 用户ID 关联患者用户登录表用户ID 例如:232442
	PatientID uint64 `sql:"PatientID"`
	// 月份 年份+月份（2016-09） 例如:2016-09
	YearMonth string `sql:"YearMonth"`
}

func (s *ViewMonthlyReportFilter) CopyFrom(source *doctor.ViewMonthlyReportFilterEx) {
	if source == nil {
		return
	}
	s.PatientID = source.PatientID
	s.YearMonth = source.YearMonth
}
