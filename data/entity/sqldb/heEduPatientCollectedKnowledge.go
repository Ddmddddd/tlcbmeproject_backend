package sqldb

import "tlcbme_project/data/model/doctor"

type PatientCollectedKnowledgeBase struct {
}

func (s PatientCollectedKnowledgeBase) TableName() string {
	return "tlcheedupatientcollectedknowledge"
}

type PatientCollectedKnowledge struct {
	PatientCollectedKnowledgeBase
	Id                     uint64 `sql:"id" auto:"true" primary:"true"`
	UserID                 uint64 `sql:"UserID"`
	Collected_knowledge_id uint64 `sql:"collected_knowledge_id"`
}

func (s *PatientCollectedKnowledge) CopyFromApp(source *doctor.PatientCollectedKnowledgeAdd) {
	if source == nil {
		return
	}
	s.UserID = source.PatientID
	s.Collected_knowledge_id = source.Collected_knowledge_id
}
