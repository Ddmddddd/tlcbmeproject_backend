package sqldb

import (
	"tlcbme_project/data/model/doctor"
	"github.com/ktpswjz/httpserver/types"
	"time"
)

type DoctorPatientChatMsgFilter struct {
	DoctorPatientChatMsgBase

	// 发送者ID 关联患者或医生用户登录表用户ID 例如:232442
	SenderID uint64 `sql:"SenderID"`
	// 接受者ID 关联患者或医生用户登录表用户ID 例如:232442
	ReceiverID uint64 `sql:"ReceiverID"`
}

type DoctorPatientChatMsgWithFlagFilter struct {
	DoctorPatientChatMsgBase

	// 发送者ID 关联患者或医生用户登录表用户ID 例如:232442
	SenderID uint64 `sql:"SenderID"`
	// 接受者ID 关联患者或医生用户登录表用户ID 例如:232442
	ReceiverID uint64 `sql:"ReceiverID"`
	// 消息标识 0-未读，1-已读 例如:0
	MsgFlag uint64 `sql:"MsgFlag"`
}

type DoctorPatientChatMsgDateTimeOrder struct {
	DoctorPatientChatMsgBase

	// 消息时间 消息发生的时间 例如:2018-07-03 14:45:00
	MsgDateTime *time.Time `sql:"MsgDateTime"`
}

type DoctorPatientChatMsgDateTimeDescOrder struct {
	DoctorPatientChatMsgBase

	// 消息时间 消息发生的时间 例如:2018-07-03 14:45:00
	MsgDateTime *time.Time `sql:"MsgDateTime" order:"DESC"`
}

type DoctorPatientChatMsgBaseInfo struct {
	DoctorPatientChatMsgBase

	// 消息内容 医患沟通的消息内容 例如:你好
	MsgContent string `sql:"MsgContent"`
	// 消息时间 消息发生的时间 例如:2018-07-03 14:45:00
	MsgDateTime *time.Time `sql:"MsgDateTime"`
}

type DoctorPatientChatMsgRead struct {
	DoctorPatientChatMsgBase

	// 序号 主键，自增 例如:324
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 消息标识 0-未读，1-已读 例如:0
	MsgFlag uint64 `sql:"MsgFlag"`
}

type CountChatUnReadMsgFilter struct {
	DoctorPatientChatMsgBase

	// 接受者ID 关联患者或医生用户登录表用户ID 例如:232442
	ReceiverID uint64 `sql:"ReceiverID"`
	// 消息标识 0-未读，1-已读 例如:0
	MsgFlag uint64 `sql:"MsgFlag"`
}

type ChatUnReadMsgCount struct {
	DoctorPatientChatMsgBase

	// 发送者ID 关联患者或医生用户登录表用户ID 例如:232442
	SenderID uint64 `sql:"SenderID"`
}
type DoctorPatientLastChatMsg struct {
	// 序号 主键，自增 例如:324
	SerialNo uint64
	// 发送者ID 关联患者或医生用户登录表用户ID 例如:232442
	PatientID uint64
	// 接受者ID 关联患者或医生用户登录表用户ID 例如:232442
	PatientName string
	// 消息内容 医患沟通的消息内容 例如:你好
	MsgContent string
	// 消息时间 消息发生的时间 例如:2018-07-03 14:45:00
	MsgDateTime *time.Time
	// 消息标识 0-未读，1-已读 例如:0
	Sex *uint64
}

type DoctorMsgCount struct {
	// 未读消息总数  例如:324
	Count *uint64
	// 患者ID  例如:232442
	PatientID *uint64
	// 接受者ID 关联患者或医生用户登录表用户ID 例如:232442
	ReceiverID *uint64
}

func (s *DoctorPatientChatMsgFilter) CopyTo(target *doctor.DoctorPatientChatID) {
	if target == nil {
		return
	}
	target.SenderID = s.SenderID
	target.ReceiverID = s.ReceiverID
}

func (s *DoctorPatientLastChatMsg) CopyToExt(target *doctor.DoctorPatienLastChatMsg) {
	if target == nil {
		return
	}
	target.SerialNo = s.SerialNo
	target.PatientID = s.PatientID
	target.PatientName = s.PatientName
	target.MsgContent = s.MsgContent
	if s.MsgDateTime == nil {
		target.MsgDateTime = nil
	} else {
		msgDateTime := types.Time(*s.MsgDateTime)
		target.MsgDateTime = &msgDateTime
	}
	target.Sex = s.Sex
}
func (s *DoctorMsgCount) CopyToExt(target *doctor.DoctorMsgCount) {
	if target == nil {
		return
	}
	target.PatientID = s.PatientID
	target.Count = s.Count
	target.ReceiverID = s.ReceiverID
}
