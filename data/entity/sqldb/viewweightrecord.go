package sqldb

import (
	"bytes"
	"tlcbme_project/data/model/doctor"
	"encoding/gob"
	"github.com/ktpswjz/httpserver/types"
	"time"
)

type ViewweightrecordBase struct {
}

func (s ViewweightrecordBase) TableName() string {
	return "ViewWeightRecord"
}

func (s ViewweightrecordBase) SetFilter(v interface{}) {

}

func (s *ViewweightrecordBase) Clone() (interface{}, error) {
	return &ViewweightrecordBase{}, nil
}

// VIEW
type Viewweightrecord struct {
	ViewweightrecordBase
	// 序号 主键，自增 例如:324
	Serialno uint64 `sql:"SerialNo"`
	// 用户ID 关联患者用户登录表用户ID 例如:232442
	Patientid uint64 `sql:"PatientID"`
	// 测量时间 测量体重时的时间 例如:2018-07-03 14:45:00
	Measuredatetime *time.Time `sql:"MeasureDateTime"`
	// 管理机构代码 管理该患者的机构 例如:897798
	Orgcode *string `sql:"OrgCode"`
}

func (s *Viewweightrecord) Clone() (interface{}, error) {
	t := &Viewweightrecord{}

	buf := new(bytes.Buffer)
	enc := gob.NewEncoder(buf)
	dec := gob.NewDecoder(buf)
	err := enc.Encode(s)
	if err != nil {
		return nil, err
	}
	err = dec.Decode(t)
	if err != nil {
		return nil, err
	}

	return t, nil
}

func (s *Viewweightrecord) CopyTo(target *doctor.Viewweightrecord) {
	if target == nil {
		return
	}
	target.Serialno = s.Serialno
	target.Patientid = s.Patientid
	if s.Measuredatetime == nil {
		target.Measuredatetime = nil
	} else {
		measuredatetime := types.Time(*s.Measuredatetime)
		target.Measuredatetime = &measuredatetime
	}
	if s.Orgcode != nil {
		target.Orgcode = string(*s.Orgcode)
	}
}

func (s *Viewweightrecord) CopyFrom(source *doctor.Viewweightrecord) {
	if source == nil {
		return
	}
	s.Serialno = source.Serialno
	s.Patientid = source.Patientid
	if source.Measuredatetime == nil {
		s.Measuredatetime = nil
	} else {
		measuredatetime := time.Time(*source.Measuredatetime)
		s.Measuredatetime = &measuredatetime
	}
	orgcode := string(source.Orgcode)
	s.Orgcode = &orgcode
}
