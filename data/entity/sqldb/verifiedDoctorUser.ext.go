package sqldb

import (
	"tlcbme_project/data/enum"
	"tlcbme_project/data/model/doctor"
)

func (s *VerifiedDoctorUser) CopyToEx(target *doctor.VerifiedDoctorUserEx) {
	if target == nil {
		return
	}
	s.CopyTo(&target.VerifiedDoctorUser)

	if s.UserType != nil {
		target.UserTypeText = enum.DoctorUserTypes.Value(*s.UserType)
	}
	if s.VerifiedStatus != nil {
		target.VerifiedStatusText = enum.VerifiedStatuses.Value(*s.VerifiedStatus)
	}
}

func (s *VerifiedDoctorUser) CopyFromEdit(source *doctor.VerifiedDoctorUserEdit) {
	if source == nil {
		return
	}

	s.UserID = source.UserID
	orgCode := string(source.OrgCode)
	s.OrgCode = &orgCode
	s.UserType = source.UserType
	s.Certificate = []byte(source.Certificate)
	s.IdentityCardFront = []byte(source.IdentityCardFront)
	s.IdentityCardBack = []byte(source.IdentityCardBack)
	s.VerifiedStatus = source.VerifiedStatus
}

type VerifiedDoctorUserEdit struct {
	VerifiedDoctorUserBase

	// 执业机构代码 该医生用户所在的执业机构 例如:897798
	OrgCode *string `sql:"OrgCode"`
	// 用户类别 0-医生，1-健康管理师 例如:0
	UserType *uint64 `sql:"UserType"`
	// 执业证书照片 例如:
	Certificate []byte `sql:"Certificate"`
	// 身份证正面照片 例如:
	IdentityCardFront []byte `sql:"IdentityCardFront"`
	// 身份证背面照片 例如:
	IdentityCardBack []byte `sql:"IdentityCardBack"`
	// 是否验证通过 0-未认证，1-已认证，2-认证不通过 例如:0
	VerifiedStatus *uint64 `sql:"VerifiedStatus"`
}

func (s *VerifiedDoctorUserEdit) CopyFrom(source *doctor.VerifiedDoctorUserEdit) {
	if source == nil {
		return
	}

	orgCode := string(source.OrgCode)
	s.OrgCode = &orgCode
	s.UserType = source.UserType
	s.Certificate = []byte(source.Certificate)
	s.IdentityCardFront = []byte(source.IdentityCardFront)
	s.IdentityCardBack = []byte(source.IdentityCardBack)
	s.VerifiedStatus = source.VerifiedStatus
}

type VerifiedDoctorUserInfo struct {
	VerifiedDoctorUserBase

	// 序号 主键，自增 例如:324
	SerialNo uint64 `sql:"SerialNo" auto:"true"`
	// 用户ID 关联医生用户登录表用户ID 例如:232442
	UserID uint64 `sql:"UserID"`
	// 执业机构代码 该医生用户所在的执业机构 例如:897798
	OrgCode *string `sql:"OrgCode"`
	// 用户类别 0-医生，1-健康管理师 例如:0
	UserType *uint64 `sql:"UserType"`
	// 是否验证通过 0-未认证，1-已认证，2-认证不通过 例如:0
	VerifiedStatus *uint64 `sql:"VerifiedStatus"`
}

type VerifiedDoctorUserFilter struct {
	VerifiedDoctorUserBase

	// 用户ID 关联医生用户登录表用户ID 例如:232442
	UserID uint64 `sql:"UserID"`
}
