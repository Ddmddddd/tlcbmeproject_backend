package sqldb

type KnowledgeInfoFilterById struct {
	KnowledgeInfoBase
	Id uint64 `sql:"id"`
}

type KnowledgeInfoAddLoveNum struct {
	KnowledgeInfoBase
	LoveNum uint64 `sql:"loveNum"`
}

type KnowledgeInfoFilterByDayIndex struct {
	KnowledgeInfoBase
	Dayindex uint64 `sql:"dayindex"`
}

type KnowledgeInfoUpdateReadAlreadyNum struct {
	KnowledgeInfoBase
	ReadAlreadyNum uint64 `sql:"readAlreadyNum"`
}
