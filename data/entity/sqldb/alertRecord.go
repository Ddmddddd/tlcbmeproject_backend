package sqldb

import (
	"tlcbme_project/data/model/doctor"
	"github.com/ktpswjz/httpserver/types"
	"time"
)

type AlertRecordBase struct {
}

func (s AlertRecordBase) TableName() string {
	return "AlertRecord"
}

// 慢病管理业务
// 工作平台管理数据
// 预警记录
// 注释：该表保存慢病管理服务发过来的预警记录。
type AlertRecord struct {
	AlertRecordBase
	// 序号 主键，自增 例如:324
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 用户ID 关联患者用户登录表用户ID 例如:232442
	PatientID uint64 `sql:"PatientID"`
	// 预警代码 每类预警的唯一代码 例如:001
	AlertCode string `sql:"AlertCode"`
	// 预警类型 例如:血压
	AlertType string `sql:"AlertType"`
	// 预警名称 例如:单次血压异常偏高
	AlertName string `sql:"AlertName"`
	// 预警原因  例如:180/100mmHg
	AlertReason string `sql:"AlertReason"`
	// 预警消息 例如:消息内容
	AlertMessage string `sql:"AlertMessage"`
	// 预警发生时间  例如:2018-07-06 14:55:00
	AlertDateTime *time.Time `sql:"AlertDateTime"`
	// 接收时间 慢病管理工作平台接收到该预警的时间 例如:2018-07-06 15:00:00
	ReceiveDateTime *time.Time `sql:"ReceiveDateTime"`
	// 处理状态 0-未处理 例如:0
	Status uint64 `sql:"Status"`
	// 处理方式 0-随访，1-忽略 例如:0
	ProcessMode *uint64 `sql:"ProcessMode"`
	// 随访序号 ProcessMode为0时，这里存对应的随访记录序号。 例如:435345
	FollowUpSerialNo *uint64 `sql:"FollowUpSerialNo"`
	// 忽略预警的原因 ProcessMode为1时有效。忽略预警的原因 例如:重复预警
	IgnoreReason *string `sql:"IgnoreReason"`
	// 随访人ID ProcessMode为1时有效。 例如:111
	IgnoreDoctorID *uint64 `sql:"IgnoreDoctorID"`
	// 随访人姓名 ProcessMode为1时有效。 例如:张三
	IgnoreDoctorName *string `sql:"IgnoreDoctorName"`
	// 忽略预警的时间 ProcessMode为1时有效。 例如:2018-09-03 12:00:23
	IgnoreDateTime *time.Time `sql:"IgnoreDateTime"`
}

func (s *AlertRecord) CopyTo(target *doctor.AlertRecord) {
	if target == nil {
		return
	}
	target.SerialNo = s.SerialNo
	target.PatientID = s.PatientID
	target.AlertCode = s.AlertCode
	target.AlertType = s.AlertType
	target.AlertName = s.AlertName
	target.AlertReason = s.AlertReason
	target.AlertMessage = s.AlertMessage
	if s.AlertDateTime == nil {
		target.AlertDateTime = nil
	} else {
		alertDateTime := types.Time(*s.AlertDateTime)
		target.AlertDateTime = &alertDateTime
	}
	if s.ReceiveDateTime == nil {
		target.ReceiveDateTime = nil
	} else {
		receiveDateTime := types.Time(*s.ReceiveDateTime)
		target.ReceiveDateTime = &receiveDateTime
	}
	target.Status = s.Status
	target.ProcessMode = s.ProcessMode
	target.FollowUpSerialNo = s.FollowUpSerialNo
	if s.IgnoreReason != nil {
		target.IgnoreReason = string(*s.IgnoreReason)
	}
	target.IgnoreDoctorID = s.IgnoreDoctorID
	if s.IgnoreDoctorName != nil {
		target.IgnoreDoctorName = string(*s.IgnoreDoctorName)
	}
	if s.IgnoreDateTime == nil {
		target.IgnoreDateTime = nil
	} else {
		ignoreDateTime := types.Time(*s.IgnoreDateTime)
		target.IgnoreDateTime = &ignoreDateTime
	}
}

func (s *AlertRecord) CopyFrom(source *doctor.AlertRecord) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.PatientID = source.PatientID
	s.AlertCode = source.AlertCode
	s.AlertType = source.AlertType
	s.AlertName = source.AlertName
	s.AlertReason = source.AlertReason
	s.AlertMessage = source.AlertMessage
	if source.AlertDateTime == nil {
		s.AlertDateTime = nil
	} else {
		alertDateTime := time.Time(*source.AlertDateTime)
		s.AlertDateTime = &alertDateTime
	}
	if source.ReceiveDateTime == nil {
		s.ReceiveDateTime = nil
	} else {
		receiveDateTime := time.Time(*source.ReceiveDateTime)
		s.ReceiveDateTime = &receiveDateTime
	}
	s.Status = source.Status
	s.ProcessMode = source.ProcessMode
	s.FollowUpSerialNo = source.FollowUpSerialNo
	ignoreReason := string(source.IgnoreReason)
	s.IgnoreReason = &ignoreReason
	s.IgnoreDoctorID = source.IgnoreDoctorID
	ignoreDoctorName := string(source.IgnoreDoctorName)
	s.IgnoreDoctorName = &ignoreDoctorName
	if source.IgnoreDateTime == nil {
		s.IgnoreDateTime = nil
	} else {
		ignoreDateTime := time.Time(*source.IgnoreDateTime)
		s.IgnoreDateTime = &ignoreDateTime
	}
}
