package sqldb

type KnowledgeToTagTagIdFilter struct {
	KnowledgeToTagBase
	Tag_id uint64 `sql:"tag_id"`
}
