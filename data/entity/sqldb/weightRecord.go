package sqldb

import (
	"github.com/ktpswjz/httpserver/types"
	"time"
	"tlcbme_project/data/model/doctor"
)

type WeightRecordBase struct {
}

func (s WeightRecordBase) TableName() string {
	return "WeightRecord"
}

// 慢病管理业务
// 患者自我管理数据
// 体重记录表
// 注释：本表保存患者的体重记录。
type WeightRecord struct {
	WeightRecordBase
	// 序号 主键，自增 例如:324
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 用户ID 关联患者用户登录表用户ID 例如:232442
	PatientID uint64 `sql:"PatientID"`
	// 体重 单位：kg 例如:60.5
	Weight float64 `sql:"Weight"`
	// 腰围 单位：cm 例如:60.5
	Waistline *float64 `sql:"Waistline"`
	// 腰围 单位：cm 例如:60.5
	Water  *float64 `sql:"Water"`
	Sleep  *float64 `sql:"Sleep"`
	Height *uint64  `sql:"Height"`
	// 备注 用于说明测量时的情况 例如:有氧运动半小时后
	Memo *string `sql:"Memo"`

	// 时间点 早、中、晚 例如:早
	TimePoint *string `sql:"TimePoint"`
	Photo     *string `sql:"Photo"`
	// 测量时间 测量体重时的时间 例如:2018-07-03 14:45:00
	MeasureDateTime *time.Time `sql:"MeasureDateTime"`
	// 录入时间 将体重值录入系统的时间 例如:2018-07-03 14:00:00
	InputDateTime *time.Time `sql:"InputDateTime"`

	BloodPressureNo *uint64 `sql:"BloodPressureNo"`
	BloodGlucoseNo  *uint64 `sql:"BloodGlucoseNo"`
}

func (s *WeightRecord) CopyTo(target *doctor.WeightRecord) {
	if target == nil {
		return
	}
	target.SerialNo = s.SerialNo
	target.PatientID = s.PatientID
	target.Weight = s.Weight
	target.Waistline = s.Waistline
	target.Water = s.Water
	target.Sleep = s.Sleep
	target.Height = s.Height
	if s.Photo != nil {
		target.Photo = string(*s.Photo)
	}
	if s.Memo != nil {
		target.Memo = string(*s.Memo)
	}
	if s.TimePoint != nil {
		target.TimePoint = string(*s.TimePoint)
	}
	if s.MeasureDateTime == nil {
		target.MeasureDateTime = nil
	} else {
		measureDateTime := types.Time(*s.MeasureDateTime)
		target.MeasureDateTime = &measureDateTime
	}
	if s.InputDateTime == nil {
		target.InputDateTime = nil
	} else {
		inputDateTime := types.Time(*s.InputDateTime)
		target.InputDateTime = &inputDateTime
	}
	target.BloodPressureNo = s.BloodPressureNo
	target.BloodGlucoseNo = s.BloodGlucoseNo
}

func (s *WeightRecord) CopyFrom(source *doctor.WeightRecord) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.PatientID = source.PatientID
	s.Weight = source.Weight
	s.Waistline = source.Waistline
	s.Water = source.Water
	s.Sleep = source.Sleep
	s.Height = source.Height
	memo := string(source.Memo)
	s.Memo = &memo
	timePoint := string(source.TimePoint)
	s.TimePoint = &timePoint
	photo := string(source.Photo)
	s.Photo = &photo
	if source.MeasureDateTime == nil {
		s.MeasureDateTime = nil
	} else {
		measureDateTime := time.Time(*source.MeasureDateTime)
		s.MeasureDateTime = &measureDateTime
	}
	if source.InputDateTime == nil {
		s.InputDateTime = nil
	} else {
		inputDateTime := time.Time(*source.InputDateTime)
		s.InputDateTime = &inputDateTime
	}
	s.BloodPressureNo = source.BloodPressureNo
	s.BloodGlucoseNo = source.BloodGlucoseNo
}
