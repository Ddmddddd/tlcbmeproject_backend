package sqldb

import (
	"bytes"
	"tlcbme_project/data/model/doctor"
	"encoding/gob"
)

type DeviceDictBase struct {
}

func (s DeviceDictBase) TableName() string {
	return "DeviceDict"
}

func (s DeviceDictBase) SetFilter(v interface{}) {

}

func (s *DeviceDictBase) Clone() (interface{}, error) {
	return &DeviceDictBase{}, nil
}

// 平台运维
// 基础字典
// 设备字典
// 注释：本表保存联网设备信息
type DeviceDict struct {
	DeviceDictBase
	// 序号 主键，自增 例如:324
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 设备代码 设备代码 例如:200101
	DeviceCode string `sql:"DeviceCode"`
	// 设备名称  例如:悦奇血压计
	DeviceName *string `sql:"DeviceName"`
	// 设备类别 为以下值之一：血压计、血糖仪、身高体重测量仪 例如:血压计
	DeviceType string `sql:"DeviceType"`
	// 备注 例如:
	Memo *string `sql:"Memo"`
	// 机构代码 关联机构表的代码字段 例如:2894782947239239
	OrgCode string `sql:"OrgCode"`
	// 有效性 用于描述本记录是否有效，0-无效，1-有效，默认值为1 例如:1
	IsValid uint64 `sql:"IsValid"`
}

func (s *DeviceDict) Clone() (interface{}, error) {
	t := &DeviceDict{}

	buf := new(bytes.Buffer)
	enc := gob.NewEncoder(buf)
	dec := gob.NewDecoder(buf)
	err := enc.Encode(s)
	if err != nil {
		return nil, err
	}
	err = dec.Decode(t)
	if err != nil {
		return nil, err
	}

	return t, nil
}

func (s *DeviceDict) CopyTo(target *doctor.DeviceDict) {
	if target == nil {
		return
	}
	target.SerialNo = s.SerialNo
	target.DeviceCode = s.DeviceCode
	if s.DeviceName != nil {
		target.DeviceName = string(*s.DeviceName)
	}
	target.DeviceType = s.DeviceType
	if s.Memo != nil {
		target.Memo = string(*s.Memo)
	}
	target.OrgCode = s.OrgCode
	target.IsValid = s.IsValid
}

func (s *DeviceDict) CopyFrom(source *doctor.DeviceDict) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.DeviceCode = source.DeviceCode
	DeviceName := string(source.DeviceName)
	s.DeviceName = &DeviceName
	s.DeviceType = source.DeviceType
	memo := string(source.Memo)
	s.Memo = &memo
	s.OrgCode = source.OrgCode
	s.IsValid = source.IsValid
}
