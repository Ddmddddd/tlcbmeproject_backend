package sqldb

import (
	"time"
	"tlcbme_project/data/model/doctor"
)

type ManagementApplicationReviewMemoFilter struct {
	ManagementApplicationReviewBase

	// 用户ID 关联患者用户登录表用户ID 例如:232442
	PatientID uint64 `sql:"PatientID"`
}

type ManagementApplicationReviewMemo struct {
	ManagementApplicationReviewBase

	// 诊断备注  例如:血压 150/90 mmHg，血糖 7.9 mmol/L
	DiagnosisMemo *string `sql:"DiagnosisMemo"`
}

type ManagementApplicationReviewFilter struct {
	ManagementApplicationReviewBase

	// 用户ID 关联患者用户登录表用户ID 例如:232442
	PatientID uint64 `sql:"PatientID"`
	// 审核状态 0-未审核，1-审核通过，2-审核不通过 例如:0
	Status uint64 `sql:"Status"`
}

type ManagementApplicationReviewAuth struct {
	ManagementApplicationReviewBase

	// 用户ID 关联患者用户登录表用户ID 例如:232442
	PatientID uint64 `sql:"PatientID"  primary:"true"`
	// 审核状态 0-未审核，1-审核通过，2-审核不通过 例如:0
	Status uint64 `sql:"Status"`
	// 审核人ID 审核人对应的用户ID 例如:32423
	ReviewerID *uint64 `sql:"ReviewerID"`
	// 审核人姓名 例如:张三
	ReviewerName *string `sql:"ReviewerName"`
	// 审核时间 例如:2018-07-03 13:00:00
	ReviewDateTime time.Time `sql:"ReviewDateTime"`
	// 审核不通过原因 最近一次审核不通过的原因。只有当状态为2时有效。 例如:信息不够完善
	RefuseReason *string `sql:"RefuseReason"`
}

func (s *ManagementApplicationReviewAuth) CopyFrom(source *doctor.PatientUserAuditEx) {
	if source == nil {
		return
	}

	s.PatientID = source.PatientID
	s.Status = source.Status
	s.ReviewerID = &source.ReviewerID
	s.ReviewerName = &source.ReviewerName
	s.ReviewDateTime = time.Time(source.ReviewDateTime)
	s.RefuseReason = &source.RefuseReason
}

type ManagementApplicationReviewCountFilter struct {
	ManagementApplicationReviewBase

	// 序号 主键，自增 例如:324
	SerialNo *uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 用户ID 关联患者用户登录表用户ID 例如:232442
	PatientID *uint64 `sql:"PatientID"`
	// 就诊机构代码 例如:123
	VisitOrgCode string `sql:"VisitOrgCode"`
	// 审核状态 0-未审核，1-审核通过，2-审核不通过 例如:0
	Statuses []uint64 `sql:"Status" filter:"IN"`
	// 健康管理师ID 患者注册时指定的健康管理师 例如:12432
	HealthManagerID *uint64 `sql:"HealthManagerID"`
}

func (s *ManagementApplicationReviewCountFilter) CopyFrom(source *doctor.ManagementApplicationReviewFilter) {
	if source == nil {
		return
	}

	s.SerialNo = source.SerialNo
	s.PatientID = source.PatientID
	s.VisitOrgCode = source.VisitOrgCode
	if len(source.Statuses) > 0 {
		s.Statuses = source.Statuses
	}
	s.HealthManagerID = source.HealthManagerID
}

type ManagementApplicationExt struct {
	ManagementApplicationReviewBase
	// 诊断 例如:高血压，糖尿病
	Diagnosis *string `sql:"Diagnosis"`
	// 诊断备注  例如:血压 150/90 mmHg，血糖 7.9 mmol/L
	DiagnosisMemo *string `sql:"DiagnosisMemo"`
}

type ManagementApplicationReviewExtFilter struct {
	ManagementApplicationReviewBase
	PatientID uint64 `sql:"PatientID"`
}

func (s *ManagementApplicationExt) CopyFromExt(source *doctor.PatientUserBaseInfoModify) {
	if source == nil {
		return
	}
	diagnosis := string(source.Diagnosis)
	s.Diagnosis = &diagnosis
	diagnosisMemo := string(source.DiagnosisMemo)
	s.DiagnosisMemo = &diagnosisMemo

}

func (s *ManagementApplicationReviewMemo) CopyFrom(target *doctor.ManagementApplicationReviewMemo) {
	if target == nil {
		return
	}
	if len(target.DiagnosisMemo) == 0 {
		target.DiagnosisMemo = " "
	}
	diagnosisMemo := string(target.DiagnosisMemo)
	s.DiagnosisMemo = &diagnosisMemo
}
