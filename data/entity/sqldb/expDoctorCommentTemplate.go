package sqldb

import "tlcbme_project/data/model/doctor"

type ExpDoctorCommentTemplateBase struct {
}

func (s ExpDoctorCommentTemplateBase) TableName() string {
	return "ExpDoctorCommentTemplate"
}

type ExpDoctorCommentTemplate struct {
	ExpDoctorCommentTemplateBase
	//
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 模板类型，1-饮食评论模板
	TemplateType uint64 `sql:"TemplateType"`
	// 模板内容
	TemplateContent string `sql:"TemplateContent"`
	// 开放属性，0-所有，1-私人
	OpenType uint64 `sql:"OpenType"`
	// 编辑者id
	EditorID uint64 `sql:"EditorID"`
}

func (s *ExpDoctorCommentTemplate) CopyTo(target *doctor.ExpDoctorCommentTemplate) {
	if target == nil {
		return
	}
	target.SerialNo = s.SerialNo
	target.TemplateType = s.TemplateType
	target.TemplateContent = s.TemplateContent
	target.OpenType = s.OpenType
	target.EditorID = s.EditorID
}

func (s *ExpDoctorCommentTemplate) CopyFrom(source *doctor.ExpDoctorCommentTemplate) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.TemplateType = source.TemplateType
	s.TemplateContent = source.TemplateContent
	s.OpenType = source.OpenType
	s.EditorID = source.EditorID
}

func (s *ExpDoctorCommentTemplate) CopyFromCreate(source *doctor.ExpDoctorCommentTemplateCreate) {
	if source == nil {
		return
	}
	s.TemplateType = source.TemplateType
	s.TemplateContent = source.TemplateContent
	s.OpenType = source.OpenType
	s.EditorID = source.EditorID
}