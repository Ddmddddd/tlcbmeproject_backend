package sqldb

import (
	"tlcbme_project/data/enum"
	"tlcbme_project/data/model"
	"tlcbme_project/data/model/doctor"
	"fmt"
	"strings"
	"time"
)

func (s *ViewManagedIndex) CopyToEx(target *doctor.ViewManagedIndexEx, bmi *float64) {
	if target == nil {
		return
	}
	s.CopyTo(&target.ViewManagedIndex)

	if s.Sex != nil {
		target.SexText = enum.Sexes.Value(*s.Sex)
	}
	if s.DateOfBirth != nil {
		age := time.Now().Sub(*s.DateOfBirth)
		target.Age = int64(age.Hours() / (24 * 365))
	}

	tagCount := 0
	tag := &strings.Builder{}
	if s.ManageClass != nil {
		vs := strings.Split(*s.ManageClass, ",")
		for _, v := range vs {
			tv := strings.TrimSpace(v)
			if len(tv) < 1 {
				continue
			}
			tagCount++
			if tagCount > 1 {
				tag.WriteString(" ")
			}
			tag.WriteString(tv)
		}
	}
	if s.PatientFeature != nil {
		vs := strings.Split(*s.PatientFeature, ",")
		for _, v := range vs {
			tv := strings.TrimSpace(v)
			if len(tv) < 1 {
				continue
			}
			tagCount++
			if tagCount > 1 {
				tag.WriteString(" ")
			}
			tag.WriteString(tv)
		}
	}
	if target.Age >= 45 && target.Age < 60 {
		tag.WriteString(" 中年人")
	} else if target.Age >= 60 {
		tag.WriteString(" 老年人")
	}

	if bmi != nil {
		bmi := *bmi
		if bmi >= 24 && bmi < 27 {
			tag.WriteString(" 超重")
		} else if bmi >= 27 && bmi < 30 {
			tag.WriteString(" 肥胖")
		} else if bmi >= 30 {
			tag.WriteString(" 重度肥胖")
		}
	}

	target.Tag = tag.String()

	if s.ManageStartDateTime != nil {
		now := time.Now()
		end := time.Date(now.Year(), now.Month(), now.Day(), 0, 0, 0, 0, now.Location())
		start := time.Date(s.ManageStartDateTime.Year(), s.ManageStartDateTime.Month(), s.ManageStartDateTime.Day(), 0, 0, 0, 0, now.Location())

		days := end.Sub(start) / (time.Hour * 24)
		target.ManagedDays = int64(days)
	}
}

type ViewManagedIndexOrder struct {
	ViewManagedIndexBase

	// 管理开始时间 最近一次管理的开始时间，如果是刚注册，则默认为审核通过时间 例如:2018-07-03 13:15:00
	ManageStartDateTime *time.Time `sql:"ManageStartDateTime" order:"DESC"`
}

type ViewManagedIndexFilter struct {
	ViewManagedIndexBase

	// 序号 主键，自增 例如:324
	SerialNo *uint64 `sql:"SerialNo"`
	// 用户ID 关联患者用户登录表用户ID 例如:232442
	PatientID *uint64 `sql:"PatientID"`
	// 姓名  例如:张三
	PatientName string `sql:"PatientName" filter:"like"`
	// 管理状态 0-管理中，1-迁出，2-已终止管理 例如:0
	ManageStatus *uint64 `sql:"ManageStatus"`
	// 管理分类代码，例如：1
	ManageClassCode *uint64 `sql:"ManageClassCode"`
	// 管理机构代码 管理该患者的机构 例如:897798
	OrgCode string `sql:"OrgCode"`
	// 医生ID 例如:232
	DoctorID *uint64 `sql:"DoctorID"`
	// 健康管理师ID 例如:232
	HealthManagerID *uint64 `sql:"HealthManagerID"`
	//
	ManageLevel []byte `sql:"ManageLevel"`
	//
	DmManageLevel []byte `sql:"DmManageLevel"`
	// 活跃度 0-不活跃，1-活跃 例如:0
	PatientActiveDegree []uint64 `sql:"PatientActiveDegree" filter:"IN"`
}

func (s *ViewManagedIndexFilter) CopyFrom(source *doctor.ViewManagedIndexFilter) {
	if source == nil {
		return
	}

	s.SerialNo = source.SerialNo
	s.PatientID = source.PatientID
	if len(source.PatientName) > 0 {
		s.PatientName = fmt.Sprint("%", source.PatientName, "%")
	}
	s.ManageStatus = source.ManageStatus
	s.ManageClassCode = source.ManageClassCode
	if len(source.PatientActiveDegree) > 0 {
		s.PatientActiveDegree = source.PatientActiveDegree
	}
	if len(source.ManageLevel) > 0 {
		s.ManageLevel = []byte(source.ManageLevel)
	}
	if len(source.DmManageLevel) > 0 {
		s.DmManageLevel = []byte(source.DmManageLevel)
	}
}

func (s *ViewManagedIndexFilter) CopyFromEx(source *doctor.ViewManagedIndexFilterEx) {
	if source == nil {
		return
	}
	s.CopyFrom(&source.ViewManagedIndexFilter)

	s.OrgCode = source.OrgCode
	if !source.ViewOrgAllPatients {
		s.DoctorID = source.DoctorID
		s.HealthManagerID = source.HealthManagerID
	}
	if len(s.PatientActiveDegree) > 0 {
		s.PatientActiveDegree = source.PatientActiveDegree
	}
}

type ViewManagedIndexOrderBase struct {
	ViewManagedIndexBase

	// 序号 主键，自增 例如:324
	SerialNo uint64 `sql:"SerialNo" index:"100"`
}

type ViewManagedIndexOrderManageStartDateTimeAscending struct {
	ViewManagedIndexOrderBase

	// 管理开始时间 最近一次管理的开始时间，如果是刚注册，则默认为审核通过时间 例如:2018-07-03 13:15:00
	ManageStartDateTime *time.Time `sql:"ManageStartDateTime"`
}

type ViewManagedIndexOrderManageStartDateTimeDescending struct {
	ViewManagedIndexOrderBase

	// 管理开始时间 最近一次管理的开始时间，如果是刚注册，则默认为审核通过时间 例如:2018-07-03 13:15:00
	ManageStartDateTime *time.Time `sql:"ManageStartDateTime" order:"DESC"`
}

type ViewManagedIndexOrderComplianceRateAscending struct {
	ViewManagedIndexOrderBase

	// 依从度 值为0至5，0表示依从度未知 例如:4
	ComplianceRate *uint64 `sql:"ComplianceRate"`
}

type ViewManagedIndexOrderComplianceRateDescending struct {
	ViewManagedIndexOrderBase

	// 依从度 值为0至5，0表示依从度未知 例如:4
	ComplianceRate *uint64 `sql:"ComplianceRate" order:"DESC"`
}

type ViewManagedIndexOrderManageLevelAscending struct {
	ViewManagedIndexOrderBase

	// 管理状态 0-管理中，1-迁出，2-已终止管理 例如:0
	ManageStatus uint64 `sql:"ManageStatus" order:"DESC" index:"1"`

	ManageLevel []byte `sql:"ManageLevel" index:"2"`

	ManageLevelDays []byte `sql:"ManageLevelStartDateTime"  order:"DESC"  index:"3" `
}

type ViewManagedIndexOrderManageLevelDescending struct {
	ViewManagedIndexOrderBase

	// 管理状态 0-管理中，1-迁出，2-已终止管理 例如:0
	ManageStatus uint64 `sql:"ManageStatus" index:"1"`

	ManageLevel []byte `sql:"ManageLevel" order:"DESC" index:"2"`

	ManageLevelDays []byte `sql:"ManageLevelStartDateTime"  index:"3" `
}

type ViewManagedIndexOrderDmManageLevelAscending struct {
	ViewManagedIndexOrderBase

	// 管理状态 0-管理中，1-迁出，2-已终止管理 例如:0
	ManageStatus uint64 `sql:"ManageStatus" order:"DESC" index:"1"`

	ManageLevel []byte `sql:"DmManageLevel" index:"2"`

	ManageLevelDays []byte `sql:"DmManageLevelStartDateTime" order:"DESC" index:"3" `
}

type ViewManagedIndexOrderDmManageLevelDescending struct {
	ViewManagedIndexOrderBase

	// 管理状态 0-管理中，1-迁出，2-已终止管理 例如:0
	ManageStatus uint64 `sql:"ManageStatus" index:"1"`

	ManageLevel []byte `sql:"DmManageLevel" order:"DESC" index:"2"`

	ManageLevelDays []byte `sql:"DmManageLevelStartDateTime" index:"3" `
}

var viewManagedIndexOrders = make(map[string]interface{}, 0)

func init() {
	viewManagedIndexOrders["manageStartDateTime-ascending"] = &ViewManagedIndexOrderManageStartDateTimeAscending{}
	viewManagedIndexOrders["manageStartDateTime-descending"] = &ViewManagedIndexOrderManageStartDateTimeDescending{}

	viewManagedIndexOrders["complianceRate-ascending"] = &ViewManagedIndexOrderComplianceRateAscending{}
	viewManagedIndexOrders["complianceRate-descending"] = &ViewManagedIndexOrderComplianceRateDescending{}

	viewManagedIndexOrders["manageLevel-ascending"] = &ViewManagedIndexOrderManageLevelAscending{}
	viewManagedIndexOrders["manageLevel-descending"] = &ViewManagedIndexOrderManageLevelDescending{}

	viewManagedIndexOrders["dmManageLevel-ascending"] = &ViewManagedIndexOrderDmManageLevelAscending{}
	viewManagedIndexOrders["dmManageLevel-descending"] = &ViewManagedIndexOrderDmManageLevelDescending{}
}

func (s *ViewManagedIndex) GetOrder(order *model.Order) interface{} {
	if order == nil {
		return &ViewManagedIndexOrder{}
	}

	key := order.Key()
	if len(key) < 1 {
		return &ViewManagedIndexOrder{}
	}

	v, ok := viewManagedIndexOrders[key]
	if ok {
		return v
	} else {
		return &ViewManagedIndexOrder{}
	}
}

type ViewManagedIndexExtFilter struct {
	ViewManagedIndexBase

	//
	ManageLevel []byte `sql:"DmManageLevel"`
	// 管理机构代码 管理该患者的机构 例如:897798
	OrgCode *string `sql:"OrgCode"`
	// 管理分类代码，例如：1
	ManageClassCode *uint64 `sql:"ManageClassCode"`
	// 管理开始时间 最近一次管理的开始时间，如果是刚注册，则默认为审核通过时间 例如:2018-07-03 13:15:00
	ManageStartDateTime *time.Time `sql:"ManageStartDateTime" filter:"<"`
}

type ViewManagedIndexHtnFilter struct {
	ViewManagedIndexBase

	//
	ManageLevel []byte `sql:"ManageLevel"`
	// 管理机构代码 管理该患者的机构 例如:897798
	OrgCode *string `sql:"OrgCode"`
	// 管理分类代码，例如：1
	ManageClassCode *uint64 `sql:"ManageClassCode"`
	// 管理开始时间 最近一次管理的开始时间，如果是刚注册，则默认为审核通过时间 例如:2018-07-03 13:15:00
	ManageStartDateTime *time.Time `sql:"ManageStartDateTime" filter:"<"`
}

type ViewManagedIndexCount struct {
	ViewManagedIndexBase

	// 用户ID 关联患者用户登录表用户ID 例如:232442
	PatientID *uint64 `sql:"PatientID"`
}
