package sqldb

import (
	"github.com/ktpswjz/httpserver/types"
	"time"
	"tlcbme_project/data/model/doctor"
)

type SportDietRecordBase struct {
}

func (s SportDietRecordBase) TableName() string {
	return "SportDietRecord"
}

// 慢病管理业务
// 患者自我管理数据
// 运动饮食记录表
// 注释：本表保存患者的运动饮食情况。
type SportDietRecord struct {
	SportDietRecordBase
	// 序号 主键，自增 例如:324
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 用户ID 关联患者用户登录表用户ID 例如:232442
	PatientID uint64 `sql:"PatientID"`
	// 记录类型 用于标识是饮食还是运动，目前其值为以下两个之一：饮食、运动 例如:饮食
	RecordType string `sql:"RecordType"`
	// 时间点 早、中、晚 例如:早
	TimePoint *string `sql:"TimePoint"`
	// 项目名称 例如：米饭 例如:米饭
	ItemName string `sql:"ItemName"`
	// 项目值 例如：200 例如:200
	ItemValue *string `sql:"ItemValue"`
	// 项目单位 例如：g 例如:g
	ItemUnit *string `sql:"ItemUnit"`
	// 描述 这个字段存储可读性文本，一般是ItemName、ItemValue和ItemUnit拼接而成，例如：米饭200g 例如:米饭200g
	Description string `sql:"Description"`
	// 记录的照片
	Photo *string `sql:"Photo"`
	// 备注  例如:
	Memo *string `sql:"Memo"`
	// 运动强度  例如:强
	SportIntensity *string `sql:"SportIntensity"`
	// 发生时间 实际发生的时间 例如:2018-07-03 14:45:00
	HappenDateTime *time.Time `sql:"HappenDateTime"`
	// 录入时间 录入时间 例如:2018-07-03 14:00:00
	InputDateTime *time.Time `sql:"InputDateTime"`
	HappenPlace *string `sql:"HappenPlace"`
	DietRecordNo uint64 `sql:"DietRecordNo"`
}

func (s *SportDietRecord) CopyTo(target *doctor.SportDietRecord) {
	if target == nil {
		return
	}
	target.SerialNo = s.SerialNo
	target.PatientID = s.PatientID
	target.RecordType = s.RecordType
	if s.TimePoint != nil {
		target.TimePoint = string(*s.TimePoint)
	}
	target.ItemName = s.ItemName
	if s.ItemValue != nil {
		target.ItemValue = string(*s.ItemValue)
	}
	if s.ItemUnit != nil {
		target.ItemUnit = string(*s.ItemUnit)
	}
	target.Description = s.Description
	if s.Photo != nil {
		target.Photo = string(*s.Photo)
	}
	if s.Memo != nil {
		target.Memo = string(*s.Memo)
	}
	if s.SportIntensity != nil {
		target.SportIntensity = string(*s.SportIntensity)
	}
	if s.HappenDateTime == nil {
		target.HappenDateTime = nil
	} else {
		happenDateTime := types.Time(*s.HappenDateTime)
		target.HappenDateTime = &happenDateTime
	}
	if s.InputDateTime == nil {
		target.InputDateTime = nil
	} else {
		inputDateTime := types.Time(*s.InputDateTime)
		target.InputDateTime = &inputDateTime
	}
	if s.HappenPlace != nil {
		target.HappenPlace = string(*s.HappenPlace)
	}
}

func (s *SportDietRecord) CopyFrom(source *doctor.SportDietRecord) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.PatientID = source.PatientID
	s.RecordType = source.RecordType
	timePoint := string(source.TimePoint)
	s.TimePoint = &timePoint
	s.ItemName = source.ItemName
	itemValue := string(source.ItemValue)
	s.ItemValue = &itemValue
	itemUnit := string(source.ItemUnit)
	s.ItemUnit = &itemUnit
	s.Description = source.Description
	s.Photo = &source.Photo
	memo := string(source.Memo)
	s.Memo = &memo
	sportIntensity := string(source.SportIntensity)
	s.SportIntensity = &sportIntensity
	happenPlace := string(source.HappenPlace)
	s.HappenPlace = &happenPlace
	s.DietRecordNo = source.DietRecordNo
	if source.HappenDateTime == nil {
		s.HappenDateTime = nil
	} else {
		happenDateTime := time.Time(*source.HappenDateTime)
		s.HappenDateTime = &happenDateTime
	}
	if source.InputDateTime == nil {
		s.InputDateTime = nil
	} else {
		inputDateTime := time.Time(*source.InputDateTime)
		s.InputDateTime = &inputDateTime
	}
}
