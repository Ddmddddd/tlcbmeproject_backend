package sqldb

import "tlcbme_project/data/model/doctor"

type PatientToTagBase struct {
}

func (s PatientToTagBase) TableName() string {
	return "tlcheedupatienttotag"
}

type PatientToTag struct {
	PatientToTagBase
	Id     uint64 `sql:"id" auto:"true" primary:"true"`
	UserID uint64 `sql:"UserID"`
	Tag_id uint64 `sql:"tag_id"`
}

func (s *PatientToTag) CopyToPatientTag(target *doctor.PatientTag) {
	if target == nil {
		return
	}
	target.Tag_id = s.Tag_id
}

func (s *PatientToTag) CopyFromApp(source *doctor.PatientToTagAdd) {
	if source == nil {
		return
	}
	s.UserID = source.PatientID
	s.Tag_id = source.Tag_id
}
