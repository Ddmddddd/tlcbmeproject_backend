package sqldb

import "tlcbme_project/data/model/doctor"

type MonthlyCompletenessBase struct {
}

func (s MonthlyCompletenessBase) TableName() string {
	return "MonthlyCompleteness"
}

// 慢病管理业务
// 患者APP月报数据
// 月完成度表
// 注释：本表保存患者的自我管理月完成度记录。
type MonthlyCompleteness struct {
	MonthlyCompletenessBase
	// 序号 主键，自增 例如:324
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 用户ID 关联患者用户登录表用户ID 例如:232442
	PatientID uint64 `sql:"PatientID"`
	// 月份 年份+月份（2016-09） 例如:2016-09
	YearMonth string `sql:"YearMonth"`
	// 血压实测次数  例如:12
	BpMeasuredTimes uint64 `sql:"BpMeasuredTimes"`
	// 血压应测次数  例如:90
	BpPlanedTimes uint64 `sql:"BpPlanedTimes"`
	// 体重实测次数  例如:20
	WeightMeasuredTimes uint64 `sql:"WeightMeasuredTimes"`
	// 体重应测次数  例如:30
	WeightPlanedTimes uint64 `sql:"WeightPlanedTimes"`
	// 实际服药次数  例如:20
	DrugTakenTimes uint64 `sql:"DrugTakenTimes"`
	// 计划服药次数  例如:30
	DrugPlanedTimes uint64 `sql:"DrugPlanedTimes"`
	// 心率实测次数  例如:12
	HrMeasuredTimes uint64 `sql:"HrMeasuredTimes"`
	// 加入管理天数  例如:1002
	DaysInManagement uint64 `sql:"DaysInManagement"`
}

func (s *MonthlyCompleteness) CopyTo(target *doctor.MonthlyCompleteness) {
	if target == nil {
		return
	}
	target.SerialNo = s.SerialNo
	target.PatientID = s.PatientID
	target.YearMonth = s.YearMonth
	target.BpMeasuredTimes = s.BpMeasuredTimes
	target.BpPlanedTimes = s.BpPlanedTimes
	target.WeightMeasuredTimes = s.WeightMeasuredTimes
	target.WeightPlanedTimes = s.WeightPlanedTimes
	target.DrugTakenTimes = s.DrugTakenTimes
	target.DrugPlanedTimes = s.DrugPlanedTimes
	target.HrMeasuredTimes = s.HrMeasuredTimes
	target.DaysInManagement = s.DaysInManagement
}

func (s *MonthlyCompleteness) CopyFrom(source *doctor.MonthlyCompleteness) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.PatientID = source.PatientID
	s.YearMonth = source.YearMonth
	s.BpMeasuredTimes = source.BpMeasuredTimes
	s.BpPlanedTimes = source.BpPlanedTimes
	s.WeightMeasuredTimes = source.WeightMeasuredTimes
	s.WeightPlanedTimes = source.WeightPlanedTimes
	s.DrugTakenTimes = source.DrugTakenTimes
	s.DrugPlanedTimes = source.DrugPlanedTimes
	s.HrMeasuredTimes = source.HrMeasuredTimes
	s.DaysInManagement = source.DaysInManagement
}
