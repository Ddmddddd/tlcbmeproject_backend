package sqldb

import (
	"github.com/ktpswjz/httpserver/types"
	"time"
	"tlcbme_project/data/model/doctor"
)

type ExpNutritionInterventionRecordBase struct {
}

func (s ExpNutritionInterventionRecordBase) TableName() string {
	return "ExpNutritionInterventionRecord"
}

type ExpNutritionInterventionRecord struct {
	ExpNutritionInterventionRecordBase
	// 序列号
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 患者ID
	PatientID *uint64 `sql:"PatientID"`
	// 总结开始日期
	StartDate *time.Time `sql:"StartDate"`
	// 总结结束日期
	EndDate *time.Time `sql:"EndDate"`
	// 打卡次数统计
	RecordCount *uint64 `sql:"RecordCount"`
	// 平均能量值，含开始日期和结束日期
	Energy *float64 `sql:"Energy"`
	// 平均蛋白质摄入
	Protein *float64 `sql:"Protein"`
	// 平均脂肪摄入
	Fat *float64 `sql:"Fat"`
	// 平均碳水化合物摄入
	Carbohydrate *float64 `sql:"Carbohydrate"`
	// 平均膳食纤维摄入
	DF *float64 `sql:"DF"`
	// 饮食目标完成次数
	Habit *string `sql:"Habit"`
}

func (s *ExpNutritionInterventionRecord) CopyTo(target *doctor.ExpNutritionInterventionRecord) {
	if target == nil {
		return
	}
	target.SerialNo = s.SerialNo
	target.PatientID = s.PatientID
	if s.StartDate == nil {
		target.StartDate = nil
	} else {
		startDate := types.Time(*s.StartDate)
		target.StartDate = &startDate
	}
	if s.EndDate == nil {
		target.EndDate = nil
	} else {
		endDate := types.Time(*s.EndDate)
		target.EndDate = &endDate
	}
	target.RecordCount = s.RecordCount
	target.Energy = s.Energy
	target.Protein = s.Protein
	target.Fat = s.Fat
	target.Carbohydrate = s.Carbohydrate
	target.DF = s.DF
	target.Habit = s.Habit
	//if len(*s.Habit) > 0 {
	//	err := json.Unmarshal([]byte(*s.Habit), target.Habit)
	//	if err != nil {
	//		return
	//	}
	//}
}

func (s *ExpNutritionInterventionRecord) CopyFrom(source *doctor.ExpNutritionInterventionRecord) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.PatientID = source.PatientID
	if source.StartDate == nil {
		s.StartDate = nil
	} else {
		startDate := time.Time(*source.StartDate)
		s.StartDate = &startDate
	}
	if source.EndDate == nil {
		s.EndDate = nil
	} else {
		endDate := time.Time(*source.EndDate)
		s.EndDate = &endDate
	}
	s.RecordCount = source.RecordCount
	s.Energy = source.Energy
	s.Protein = source.Protein
	s.Fat = source.Fat
	s.Carbohydrate = source.Carbohydrate
	s.DF = source.DF
	s.Habit = source.Habit
	//if source.Habit != nil {
	//	recordData, err := json.Marshal(source.Habit)
	//	if err == nil {
	//		record := string(recordData)
	//		s.Habit = &record
	//	}
	//}
}
