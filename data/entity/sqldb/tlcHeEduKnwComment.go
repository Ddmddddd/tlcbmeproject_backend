package sqldb

import "tlcbme_project/data/model/doctor"

type TlcHeEduKnwCommentBase struct {
}

func (s TlcHeEduKnwCommentBase) TableName() string {
	return "tlcheeduknwcomment"
}

type TlcHeEduKnwComment struct {
	TlcHeEduKnwCommentBase
	Id           uint64 `sql:"id" auto:"true" primary:"true"`
	Knowledge_id uint64 `sql:"knowledge_id"`
	UserID       uint64 `sql:"UserID"`
	Content      string `sql:"content"`
}

func (s *TlcHeEduKnwComment) CopyTo(target *doctor.TlcHeEduKnwComment) {
	if target == nil {
		return
	}
	target.Id = s.Id
	target.PatientID = s.UserID
	target.Knowledge_id = s.Knowledge_id
	target.Content = s.Content
}
