package sqldb

import "time"

type WeChatRemindTaskFilter struct {
	WeChatRemindTaskBase
	// 起始时间
	StartDateTime *time.Time `sql:"CreateDateTime" filter:">="`
	//终止时间
	EndDateTime *time.Time `sql:"CreateDateTime" filter:"<="`
	// 状态，0-正常，1-弃用
	Status uint64 `sql:"Status"`
	// 提醒方式，1-微信，2-短信，3-随访（触发引擎干预随访）
	Type uint64 `sql:"Type"`
}

type WeChatRemindTaskPrimaryFilter struct {
	WeChatRemindTaskBase
	// 序号
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
}

type WeChatRemindTaskBaseFilter struct {
	WeChatRemindTaskBase
	// 患者id
	PatientID uint64 `sql:"PatientID"`
	// 微信提醒的template_id，在微信提醒时要用到
	TemplateID *string `sql:"TemplateID"`
	// 状态，0-正常，1-弃用
	Status uint64 `sql:"Status"`
}

type WeChatRemindTaskTemplateFilter struct {
	WeChatRemindTaskBase
	// 微信提醒的template_id，在微信提醒时要用到
	TemplateID *string `sql:"TemplateID"`
	// 状态，0-正常，1-弃用
	Status uint64 `sql:"Status"`
	// 提醒方式，1-微信，2-短信，3-随访（触发引擎干预随访）
	Type uint64 `sql:"Type"`
}

type WeChatRemindTaskTemplatePatientID struct {
	WeChatRemindTaskBase
	// 患者id
	PatientID uint64 `sql:"PatientID"`
}
