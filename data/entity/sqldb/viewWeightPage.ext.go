package sqldb

import (
	"time"
	"tlcbme_project/data/model/doctor"
)

type ViewWeightPageDataFilter struct {
	ViewWeightPageBase
	// 用户ID 关联患者用户登录表用户ID 例如:232442
	PatientID uint64 `sql:"PatientID"`

	// 测量时间 测量体重的时间 例如:2018-07-03 14:45:00
	MeasureStartDate *time.Time `sql:"MeasureDateTime" filter:">="`
	MeasureEndDate   *time.Time `sql:"MeasureDateTime" filter:"<"`
}

type ViewWeightPageDataOrder struct {
	ViewWeightPageBase

	// 测量时间 测量血压的时间 例如:2018-07-03 14:45:00
	MeasureDateTime *time.Time `sql:"MeasureDateTime"`
}

func (s *ViewWeightPageDataFilter) CopyFrom(source *doctor.ViewWeightPageDataFilterEx) {
	if source == nil {
		return
	}

	s.PatientID = source.PatientID
	if source.MeasureStartDate != nil {
		s.MeasureStartDate = source.MeasureStartDate.ToDate(0)
	}
}
