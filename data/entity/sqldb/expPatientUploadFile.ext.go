package sqldb

type ExpPatientUploadFileGet struct {
	ExpPatientUploadFileBase
	// 患者id
	PatientID uint64 `sql:"PatientID"`
}