package sqldb

import "tlcbme_project/data/model/doctor"

type OrgTypeDictBase struct {
}

func (s OrgTypeDictBase) TableName() string {
	return "OrgTypeDict"
}

// 平台运维
// 基础字典
// 机构类别字典
// 注释：本表保存机构类别字典项目
type OrgTypeDict struct {
	OrgTypeDictBase
	// 序号 主键，自增 例如:324
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 大类代码 例如:A
	LargeTypeCode string `sql:"LargeTypeCode"`
	// 大类名称 例如:医院
	LargeTypeName *string `sql:"LargeTypeName"`
	// 中类代码 例如:A5
	MiddleTypeCode *string `sql:"MiddleTypeCode"`
	// 中类名称 例如:专科医院
	MiddleTypeName *string `sql:"MiddleTypeName"`
	// 小类代码 例如:A513
	SmallTypeCode *string `sql:"SmallTypeCode"`
	// 小类名称 例如:耳鼻喉科医院
	SmallTypeName *string `sql:"SmallTypeName"`
	// 备注 例如:包括五官科医院
	Memo *string `sql:"Memo"`
	// 排序 用于字典项目排序，从1开始往后排 例如:1
	ItemSortValue *uint64 `sql:"ItemSortValue"`
	// 输入码 用于快速输入，可以是拼音首字母或缩写 例如:gxy
	InputCode *string `sql:"InputCode"`
	// 有效性 用于描述本记录是否有效，0-无效，1-有效，默认值为1 例如:1
	IsValid *uint64 `sql:"IsValid"`
}

func (s *OrgTypeDict) CopyTo(target *doctor.OrgTypeDict) {
	if target == nil {
		return
	}
	target.SerialNo = s.SerialNo
	target.LargeTypeCode = s.LargeTypeCode
	if s.LargeTypeName != nil {
		target.LargeTypeName = string(*s.LargeTypeName)
	}
	if s.MiddleTypeCode != nil {
		target.MiddleTypeCode = string(*s.MiddleTypeCode)
	}
	if s.MiddleTypeName != nil {
		target.MiddleTypeName = string(*s.MiddleTypeName)
	}
	if s.SmallTypeCode != nil {
		target.SmallTypeCode = string(*s.SmallTypeCode)
	}
	if s.SmallTypeName != nil {
		target.SmallTypeName = string(*s.SmallTypeName)
	}
	if s.Memo != nil {
		target.Memo = string(*s.Memo)
	}
	target.ItemSortValue = s.ItemSortValue
	if s.InputCode != nil {
		target.InputCode = string(*s.InputCode)
	}
	target.IsValid = s.IsValid
}

func (s *OrgTypeDict) CopyFrom(source *doctor.OrgTypeDict) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.LargeTypeCode = source.LargeTypeCode
	largeTypeName := string(source.LargeTypeName)
	s.LargeTypeName = &largeTypeName
	middleTypeCode := string(source.MiddleTypeCode)
	s.MiddleTypeCode = &middleTypeCode
	middleTypeName := string(source.MiddleTypeName)
	s.MiddleTypeName = &middleTypeName
	smallTypeCode := string(source.SmallTypeCode)
	s.SmallTypeCode = &smallTypeCode
	smallTypeName := string(source.SmallTypeName)
	s.SmallTypeName = &smallTypeName
	memo := string(source.Memo)
	s.Memo = &memo
	s.ItemSortValue = source.ItemSortValue
	inputCode := string(source.InputCode)
	s.InputCode = &inputCode
	s.IsValid = source.IsValid
}
