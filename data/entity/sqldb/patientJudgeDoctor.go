package sqldb

import (
	"github.com/ktpswjz/httpserver/types"
	"time"
	"tlcbme_project/data/model/doctor"
)

type PatientJudgeDoctorBase struct {
}

func (s PatientJudgeDoctorBase) TableName() string {
	return "patientjudgedoctor"
}

type PatientJudgeDoctor struct {
	PatientJudgeDoctorBase
	//
	SerialNo uint64 `sql:"Serial_No" auto:"true" primary:"true"`
	//
	PatientID *uint64 `sql:"PatientID"`
	//
	DoctorID *uint64 `sql:"DoctorID"`
	//
	Content *string `sql:"Content"`
	//
	Star *uint64 `sql:"Star"`
	//
	DateTime *time.Time `sql:"DateTime"`
}

func (s *PatientJudgeDoctor) CopyTo(target *doctor.PatientJudgeDoctor) {
	if target == nil {
		return
	}
	target.SerialNo = s.SerialNo
	target.PatientID = s.PatientID
	target.DoctorID = s.DoctorID
	if s.Content != nil {
		target.Content = string(*s.Content)
	}
	target.Star = s.Star
	if s.DateTime == nil {
		target.DateTime = nil
	} else {
		dateTime := types.Time(*s.DateTime)
		target.DateTime = &dateTime
	}
}

func (s *PatientJudgeDoctor) CopyFrom(source *doctor.PatientJudgeDoctor) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.PatientID = source.PatientID
	s.DoctorID = source.DoctorID
	content := string(source.Content)
	s.Content = &content
	s.Star = source.Star
	if source.DateTime == nil {
		s.DateTime = nil
	} else {
		dateTime := time.Time(*source.DateTime)
		s.DateTime = &dateTime
	}
}
