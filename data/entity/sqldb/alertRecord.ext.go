package sqldb

import (
	"tlcbme_project/data/enum"
	"tlcbme_project/data/model/doctor"
	"tlcbme_project/data/model/manage"
	"time"
)

type AlertRecordOrder struct {
	AlertRecordBase

	// 管理开始时间 最近一次管理的开始时间，如果是刚注册，则默认为审核通过时间 例如:2018-07-03 13:15:00
	AlertDateTime *time.Time `sql:"AlertDateTime" order:"DESC"`
}

type AlertRecordFilter struct {
	AlertRecordBase
	// 序号 主键，自增 例如:324
	SerialNo *uint64 `sql:"SerialNo" auto:"true"`
	// 用户ID 关联患者用户登录表用户ID 例如:232442
	PatientID *uint64 `sql:"PatientID"`
	// 预警代码 每类预警的唯一代码 例如:001
	AlertCode string `sql:"AlertCode"`
	// 预警名称 例如:单次血压异常偏高
	AlertName string `sql:"AlertName"`

	// 预警发生时间  例如:2018-07-06 14:55:00
	AlertStartDate *time.Time `sql:"AlertDateTime" filter:">="`
	// 预警发生时间  例如:2018-07-06 14:55:00
	AlertEndDate *time.Time `sql:"AlertDateTime" filter:"<"`

	// 预警类型 例如:血压
	AlertTypes []string `sql:"AlertType" filter:"IN"`
	// 处理状态 0-未处理 例如:0
	Statuses []uint64 `sql:"Status" filter:"IN"`
	// 处理方式 0-随访，1-忽略 例如:0
	ProcessModes []uint64 `sql:"ProcessMode" filter:"IN"`
}

func (s *AlertRecordFilter) CopyFrom(source *doctor.AlertRecordFilter) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.PatientID = source.PatientID
	s.AlertCode = source.AlertCode
	s.AlertName = source.AlertName

	if source.AlertStartDate != nil {
		s.AlertStartDate = source.AlertStartDate.ToDate(0)
	}
	if source.AlertEndDate != nil {
		s.AlertEndDate = source.AlertEndDate.ToDate(1)
	}

	if len(source.AlertTypes) > 0 {
		s.AlertTypes = source.AlertTypes
	}
	if len(source.Statuses) > 0 {
		s.Statuses = source.Statuses
	}
	if len(source.ProcessModes) > 0 {
		s.ProcessModes = source.ProcessModes
	}
}

func (s *AlertRecord) CopyToEx(target *doctor.AlertRecordEx) {
	if target == nil {
		return
	}
	s.CopyTo(&target.AlertRecord)

	target.StatusText = enum.AlertStatuses.Value(s.Status)
	if s.ProcessMode != nil {
		target.ProcessModeText = enum.AlertProcessModes.Value(*s.ProcessMode)
	}
}

func (s *AlertRecord) CopyFromManage(source *manage.InputDataWarnings) {
	if source == nil {
		return
	}

	s.AlertCode = source.Code
	s.AlertType = source.Type
	s.AlertName = source.Name
	s.AlertReason = source.Reason
	s.AlertMessage = source.Message

	now := time.Now()
	if source.DateTime != nil {
		now = time.Time(*source.DateTime)
	}
	s.AlertDateTime = &now
}

type AlertRecordIgnore struct {
	AlertRecordBase

	// 处理状态 0-未处理 例如:0
	Status uint64 `sql:"Status"`
	// 处理方式 0-随访，1-忽略 例如:0
	ProcessMode *uint64 `sql:"ProcessMode"`
	// 忽略预警的原因 ProcessMode为1时有效。忽略预警的原因 例如:重复预警
	IgnoreReason *string `sql:"IgnoreReason"`
	// 随访人ID ProcessMode为1时有效。 例如:111
	IgnoreDoctorID *uint64 `sql:"IgnoreDoctorID"`
	// 随访人姓名 ProcessMode为1时有效。 例如:张三
	IgnoreDoctorName *string `sql:"IgnoreDoctorName"`
	// 忽略预警的时间 ProcessMode为1时有效。 例如:2018-09-03 12:00:23
	IgnoreDateTime *time.Time `sql:"IgnoreDateTime"`
}

func (s *AlertRecordIgnore) CopyFrom(source *doctor.AlertRecordIgnoreEx) {
	if source == nil {
		return
	}

	s.Status = source.Status
	s.ProcessMode = source.ProcessMode
	s.IgnoreReason = &source.IgnoreReason
	s.IgnoreDoctorID = source.IgnoreDoctorID
	s.IgnoreDoctorName = &source.IgnoreDoctorName
	s.IgnoreDateTime = source.IgnoreDateTime
}

type AlertRecordIgnoreFilter struct {
	AlertRecordBase
	// 序号 主键，自增 例如:324
	SerialNos []uint64 `sql:"SerialNo" auto:"true" filter:"IN"`
}

type AlertRecordAutoIgnoreFilter struct {
	AlertRecordBase
	// 处理状态 0-未处理 例如:0
	Status uint64 `sql:"Status"`
	// 用户ID 关联患者用户登录表用户ID 例如:232442
	PatientID uint64 `sql:"PatientID"`
}

func (s *AlertRecordIgnoreFilter) CopyFrom(source *doctor.AlertRecordIgnoreEx) {
	if source == nil {
		return
	}
	s.SerialNos = source.SerialNos
}

type AlertRecordFollowup struct {
	AlertRecordBase

	// 处理状态 0-未处理 例如:0
	Status uint64 `sql:"Status"`
	// 处理方式 0-随访，1-忽略 例如:0
	ProcessMode uint64 `sql:"ProcessMode"`
	// 随访序号 ProcessMode为0时，这里存对应的随访记录序号。 例如:435345
	FollowUpSerialNo uint64 `sql:"FollowUpSerialNo"`
}

type AlertRecordFollowupFilter struct {
	AlertRecordBase

	// 用户ID 关联患者用户登录表用户ID 例如:232442
	PatientID uint64 `sql:"PatientID"`
}

func (s *AlertRecordIgnore) Init(doctorID uint64, doctorName string) {
	now := time.Now()
	processMode := enum.AlertProcessModes.Ignore().Key
	ignoreReason := "终止管理，系统自动忽略"
	s.Status = enum.AlertStatuses.Processed().Key
	s.ProcessMode = &processMode
	s.IgnoreReason = &ignoreReason
	s.IgnoreDoctorID = &doctorID
	s.IgnoreDoctorName = &doctorName
	s.IgnoreDateTime = &now
}

func (s *AlertRecord) CopyToInfo(target *doctor.AlertPatientRecord) {
	if target == nil {
		return
	}
	target.AlertCode = s.AlertCode
}
