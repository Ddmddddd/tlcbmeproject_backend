package sqldb

type ExpExerciseBaseExclude struct {
	ExpExerciseBaseBase
	//
	Target []string `sql:"Target" filter:"in"`
}

type ExpExerciseBaseSerialNo struct {
	ExpExerciseBaseBase
	//
	SerialNo uint64 `sql:"SerialNo"`
}

type EmptyExpExerciseBaseFilter struct {
	ExpExerciseBaseBase
}


type ExpExerciseBaseSnosFilter struct {
	ExpExerciseBaseBase
	//
	Snos []uint64 `sql:"SerialNo" filter:"in"`
}