package sqldb

import "time"

type TlcTeamIntegralFilterByTeamSno struct {
	TlcTeamIntegralBase
	TeamSno uint64 `sql:"TeamSno"`
}

type TlcTeamIntegralOrderByCreateTime struct {
	TlcTeamIntegralBase
	CreateTime *time.Time `sql:"CreateTime" order:"desc"`
}

type TlcTeamIntegralCheckExistenceFilter struct {
	TlcTeamIntegralBase
	TeamSno    uint64     `sql:"TeamSno"`
	TaskSno    uint64     `sql:"TaskSno"`
	Integral   uint64     `sql:"Integral"`
	CreateTime *time.Time `sql:"CreateTime" filter:">="`
}

type TlcTeamIntegralAdd struct {
	TlcTeamIntegralBase
	TeamSno    uint64     `sql:"TeamSno"`
	TaskSno    uint64     `sql:"TaskSno"`
	Integral   uint64     `sql:"Integral"`
	CreateTime *time.Time `sql:"CreateTime"`
}
