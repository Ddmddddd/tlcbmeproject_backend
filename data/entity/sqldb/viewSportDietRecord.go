package sqldb

import "tlcbme_project/data/model/doctor"

type ViewSportDietRecordBase struct {
}

func (s ViewSportDietRecordBase) TableName() string {
	return "ViewSportDietRecord"
}

// VIEW
type ViewSportDietRecord struct {
	ViewSportDietRecordBase
	// 用户ID 关联患者用户登录表用户ID 例如:232442
	PatientID uint64 `sql:"PatientID"`
	//
	HappenDate *string `sql:"HappenDate"`
	//
	DietBreakfast *string `sql:"DietBreakfast"`
	//
	DietLunch *string `sql:"DietLunch"`
	//
	DietDinner *string `sql:"DietDinner"`
	//
	DietOther *string `sql:"DietOther"`
	//
	Sport *string `sql:"Sport"`
}

func (s *ViewSportDietRecord) CopyTo(target *doctor.ViewSportDietRecord) {
	if target == nil {
		return
	}
	target.PatientID = s.PatientID
	if s.HappenDate != nil {
		target.HappenDate = string(*s.HappenDate)
	}
	if s.DietBreakfast != nil {
		target.DietBreakfast = string(*s.DietBreakfast)
	}
	if s.DietLunch != nil {
		target.DietLunch = string(*s.DietLunch)
	}
	if s.DietDinner != nil {
		target.DietDinner = string(*s.DietDinner)
	}
	if s.DietOther != nil {
		target.DietOther = string(*s.DietOther)
	}
	if s.Sport != nil {
		target.Sport = string(*s.Sport)
	}
}

func (s *ViewSportDietRecord) CopyFrom(source *doctor.ViewSportDietRecord) {
	if source == nil {
		return
	}
	s.PatientID = source.PatientID
	happenDate := string(source.HappenDate)
	s.HappenDate = &happenDate
	dietBreakfast := string(source.DietBreakfast)
	s.DietBreakfast = &dietBreakfast
	dietLunch := string(source.DietLunch)
	s.DietLunch = &dietLunch
	dietDinner := string(source.DietDinner)
	s.DietDinner = &dietDinner
	dietOther := string(source.DietOther)
	s.DietOther = &dietOther
	sport := string(source.Sport)
	s.Sport = &sport
}
