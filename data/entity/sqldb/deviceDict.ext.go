package sqldb

import (
	"tlcbme_project/data/model/doctor"
	"fmt"
)

type DeviceFilter struct {
	DeviceDictBase
	DictFilter
	// 设备代码 设备代码 例如:200101
	DeviceCode string `sql:"DeviceCode"`
	// 有效性 用于描述本记录是否有效，0-无效，1-有效，默认值为1 例如:1
	IsValid uint64 `sql:"IsValid"`
}

type DeviceDictFilter struct {
	DeviceDictBase
	DictFilter

	// 设备代码 设备代码 例如:200101
	DeviceCode string `sql:"DeviceCode"`
	// 机构代码 关联机构表的代码字段 例如:2894782947239239
	OrgCode string `sql:"OrgCode"`
	// 设备名称  例如:悦奇血压计
	DeviceName *string `sql:"DeviceName"`
}

type DeviceDictUpdate struct {
	DeviceDictBase

	// 设备代码 设备代码 例如:200101
	DeviceCode string `sql:"DeviceCode"`
	// 设备名称  例如:悦奇血压计
	DeviceName *string `sql:"DeviceName"`
	// 设备类别 为以下值之一：血压计、血糖仪、身高体重测量仪 例如:血压计
	DeviceType string `sql:"DeviceType"`
	// 机构代码 关联机构表的代码字段 例如:2894782947239239
	OrgCode string `sql:"OrgCode"`
}

type DeviceDictKeywordFilter struct {

	// 设备代码 设备代码 例如:200101
	DeviceCode string `sql:"DeviceCode"  filter:"like"`
	// 设备名称  例如:悦奇血压计
	DeviceName *string `sql:"DeviceName"  filter:"like"`
	// 设备类别 为以下值之一：血压计、血糖仪、身高体重测量仪 例如:血压计
	DeviceType string `sql:"DeviceType"  filter:"like"`
	// 机构代码 关联机构表的代码字段 例如:2894782947239239
	OrgCode string `sql:"OrgCode"  filter:"like"`
}

func (s *DeviceDictKeywordFilter) CopyFrom(source *doctor.DictKeywordFilter) {
	if source == nil {
		return
	}

	if len(source.Keyword) > 0 {
		s.DeviceCode = fmt.Sprint("%", source.Keyword, "%")
		name := fmt.Sprint("%", source.Keyword, "%")
		s.DeviceName = &name
		s.DeviceType = fmt.Sprint("%", source.Keyword, "%")
		s.OrgCode = fmt.Sprint("%", source.Keyword, "%")
	}
}

func (s *DeviceDict) CopyFromCreate(source *doctor.DeviceDictCreate) {
	if source == nil {
		return
	}
	s.DeviceCode = source.DeviceCode
	DeviceName := string(source.DeviceName)
	s.DeviceName = &DeviceName
	s.DeviceType = source.DeviceType
	s.OrgCode = source.OrgCode
	s.IsValid = 1
}

func (s *DeviceDictUpdate) CopyFrom(source *doctor.DeviceDictUpdate) {
	if source == nil {
		return
	}
	s.DeviceCode = source.DeviceCode
	DeviceName := string(source.DeviceName)
	s.DeviceName = &DeviceName
	s.DeviceType = source.DeviceType
	s.OrgCode = source.OrgCode
}
