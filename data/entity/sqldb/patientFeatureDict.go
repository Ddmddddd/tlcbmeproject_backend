package sqldb

import "tlcbme_project/data/model/doctor"

type PatientFeatureDictBase struct {
}

func (s PatientFeatureDictBase) TableName() string {
	return "PatientFeatureDict"
}

// 平台运维
// 基础字典
// 患者特点字典
// 注释：本表保存用于描述患者特点的字典项目
type PatientFeatureDict struct {
	PatientFeatureDictBase
	// 序号 主键，自增 例如:324
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 字典项代码  例如:1
	ItemCode uint64 `sql:"ItemCode"`
	// 字典项名称  例如:老年人
	ItemName string `sql:"ItemName"`
	// 排序 用于字典项目排序，从1开始往后排 例如:1
	ItemSortValue *uint64 `sql:"ItemSortValue"`
	// 输入码 用于快速输入，可以是拼音首字母或缩写 例如:lnr
	InputCode *string `sql:"InputCode"`
	// 有效性 用于描述本记录是否有效，0-无效，1-有效，默认值为1 例如:1
	IsValid *uint64 `sql:"IsValid"`
}

func (s *PatientFeatureDict) CopyTo(target *doctor.PatientFeatureDict) {
	if target == nil {
		return
	}
	target.SerialNo = s.SerialNo
	target.ItemCode = s.ItemCode
	target.ItemName = s.ItemName
	target.ItemSortValue = s.ItemSortValue
	if s.InputCode != nil {
		target.InputCode = string(*s.InputCode)
	}
	target.IsValid = s.IsValid
}

func (s *PatientFeatureDict) CopyFrom(source *doctor.PatientFeatureDict) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.ItemCode = source.ItemCode
	s.ItemName = source.ItemName
	s.ItemSortValue = source.ItemSortValue
	inputCode := string(source.InputCode)
	s.InputCode = &inputCode
	s.IsValid = source.IsValid
}
