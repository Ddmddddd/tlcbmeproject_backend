package sqldb

import "tlcbme_project/data/model/doctor"

type ExpHealthEatingBarriersBase struct {
}

func (s ExpHealthEatingBarriersBase) TableName() string {
	return "ExpHealthEatingBarriers"
}

// 健康饮食障碍表
// 用于与ExpCommonScale中的量表结果进行对应，得到相应健康饮食障碍
type ExpHealthEatingBarriers struct {
	ExpHealthEatingBarriersBase
	// 序号
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 健康饮食障碍
	BarrierName string `sql:"BarrierName"`
	// 描述
	Description *string `sql:"Description"`
}

func (s *ExpHealthEatingBarriers) CopyTo(target *doctor.ExpHealthEatingBarriers) {
	if target == nil {
		return
	}
	target.SerialNo = s.SerialNo
	target.BarrierName = s.BarrierName
	if s.Description != nil {
		target.Description = string(*s.Description)
	}
}

func (s *ExpHealthEatingBarriers) CopyFrom(source *doctor.ExpHealthEatingBarriers) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.BarrierName = source.BarrierName
	description := string(source.Description)
	s.Description = &description
}
