package sqldb

import (
	"tlcbme_project/data/model/doctor"
	"github.com/ktpswjz/httpserver/types"
	"time"
)

type UsabilityFeedBackBase struct {
}

func (s UsabilityFeedBackBase) TableName() string {
	return "UsabilityFeedBack"
}

// 平台运维
// 用户行为统计
// 用户问题反馈
// 注释：本表记录用户反馈的问题
type UsabilityFeedBack struct {
	UsabilityFeedBackBase
	// 序号 主键，自增 例如:1
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 用户名 关联患者用户登录表用户名 例如:aa
	UserName string `sql:"UserName"`
	// 应用类别 Android、iOS或者微信小程序 例如:Android
	AppType string `sql:"AppType"`
	// 问题描述 反馈问题的详细描述 例如:xxxx
	Description string `sql:"Description"`
	// 应用版本 当前应用版本 例如:1.0.1.0
	AppVersion string `sql:"AppVersion"`
	// 反馈时间 记录反馈的时间 例如:2018-12-12 08:00:00
	RecordTime *time.Time `sql:"RecordTime"`
	// 处理状态 0：待处理；1：已解决；2：其他 例如:1
	Flag uint64 `sql:"Flag"`
	// 处理结果 处理结果描述 例如:不作修改
	ResultMemo *string `sql:"ResultMemo"`
}

func (s *UsabilityFeedBack) CopyTo(target *doctor.UsabilityFeedBack) {
	if target == nil {
		return
	}
	target.SerialNo = s.SerialNo
	target.UserName = s.UserName
	target.AppType = s.AppType
	target.Description = s.Description
	target.AppVersion = s.AppVersion
	if s.RecordTime == nil {
		target.RecordTime = nil
	} else {
		recordTime := types.Time(*s.RecordTime)
		target.RecordTime = &recordTime
	}
	target.Flag = s.Flag
	if s.ResultMemo != nil {
		target.ResultMemo = string(*s.ResultMemo)
	}
}

func (s *UsabilityFeedBack) CopyFrom(source *doctor.UsabilityFeedBack) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.UserName = source.UserName
	s.AppType = source.AppType
	s.Description = source.Description
	s.AppVersion = source.AppVersion
	if source.RecordTime == nil {
		s.RecordTime = nil
	} else {
		recordTime := time.Time(*source.RecordTime)
		s.RecordTime = &recordTime
	}
	s.Flag = source.Flag
	resultMemo := string(source.ResultMemo)
	s.ResultMemo = &resultMemo
}
