package sqldb

import "time"

type TlcTeamGroupTaskBase struct {
}

func (s TlcTeamGroupTaskBase) TableName() string {
	return "tlcteamgrouptask"
}

type TlcTeamGroupTask struct {
	TlcTeamGroupTaskBase
	SerialNo      uint64     `sql:"SerialNo"`
	BelongToGroup string     `sql:"BelongToGroup"`
	TaskSno       uint64     `sql:"TaskSno"`
	Valid         int        `sql:"Valid"`
	CreateTime    *time.Time `sql:"CreateTime"`
	UpdateTime    *time.Time `sql:"UpdateTime"`
}
