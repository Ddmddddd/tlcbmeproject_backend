package sqldb

import "time"

type FollowupPlanFilter struct {
	FollowupPlanBase

	// 序号 主键，自增 例如:324
	SerialNo *uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 用户ID 关联患者用户登录表用户ID 例如:232442
	PatientID *uint64 `sql:"PatientID"`
	// 随访状态 0-待随访，1-已随访，9-已作废。 例如:0
	Status *uint64 `sql:"Status"`
	// 随访类型 随访的类型（原因） 例如:三个月例行随访
	FollowUpType *string `sql:"FollowUpType"`
}

type FollowupPlanStatusUpdate struct {
	FollowupPlanBase

	// 序号 主键，自增 例如:324
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 随访状态 0-待随访，1-已随访，2-已忽略，3-已过期，9-已作废。 例如:0
	Status uint64 `sql:"Status"`
	// 状态备注 例如当状态为忽略时，此处填写忽略的原因。 例如:
	StatusMemo *string `sql:"StatusMemo"`
	// 最后更新日期 例如:2018-08-15
	UpdateTime *time.Time `sql:"UpdateTime"`
}

type FollowupPlanAutoIgnore struct {
	FollowupPlanBase

	// 随访状态 0-待随访，1-已随访，2-已忽略，3-已过期，9-已作废。 例如:0
	Status uint64 `sql:"Status"`
	// 状态备注 例如当状态为忽略时，此处填写忽略的原因。 例如:
	StatusMemo *string `sql:"StatusMemo"`
	// 最后更新日期 例如:2018-08-15
	UpdateTime *time.Time `sql:"UpdateTime"`
}

type FollowupPlanAutoFilter struct {
	FollowupPlanBase

	// 随访状态 0-待随访，1-已随访，2-已忽略，3-已过期，9-已作废。 例如:0
	Status uint64 `sql:"Status"`
	// 用户ID 关联患者用户登录表用户ID 例如:232442
	PatientID *uint64 `sql:"PatientID"`
}

type FollowupPlanAutoTypeFilter struct {
	FollowupPlanBase

	// 随访状态 0-待随访，1-已随访，2-已忽略，3-已过期，9-已作废。 例如:0
	Status uint64 `sql:"Status"`
	// 用户ID 关联患者用户登录表用户ID 例如:232442
	PatientID *uint64 `sql:"PatientID"`
	// 随访类型 随访的类型（原因） 例如:三个月例行随访
	FollowUpType string `sql:"FollowUpType"`
}
