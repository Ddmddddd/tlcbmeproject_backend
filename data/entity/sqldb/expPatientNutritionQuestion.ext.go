package sqldb

import (
	"time"
	"tlcbme_project/data/model/doctor"
)

type ExpPatientNutritionQuestionFilter struct {
	ExpPatientNutritionQuestionBase
	PatientID uint64 `sql:"PatientID"`
}

func (s *ExpPatientNutritionQuestion) CopyFromQuestionInput(source *doctor.ExpPatientNutritionalQuestionInput) {
	if source == nil {
		return
	}
	s.PatientID = source.UserID
	//answerMap := make(map[uint64]uint64)
	//l := len(source.QuestionList)
	//i := 0
	//for i = 0; i < l; i++ {
	//	answerMap[source.QuestionList[i]] = source.AnswerList[i]
	//}
	//answerJson,_ := json.Marshal(answerMap)
	//answer := string(answerJson)
	//s.Answer = &answer
	if source.InputDate == nil {
		s.CreateDate = nil
	} else {
		createDate := time.Time(*source.InputDate)
		s.CreateDate = &createDate
	}
}