package sqldb

import (
	"tlcbme_project/data/model/doctor"
	"encoding/json"
	"github.com/ktpswjz/httpserver/types"
	"time"
)

type FollowupRecordBase struct {
}

func (s FollowupRecordBase) TableName() string {
	return "FollowupRecord"
}

// 慢病管理业务
// 工作平台管理数据
// 随访记录
// 注释：该表保存随访记录。
type FollowupRecord struct {
	FollowupRecordBase
	// 序号 主键，自增 例如:324
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 用户ID 关联患者用户登录表用户ID 例如:232442
	PatientID uint64 `sql:"PatientID"`
	// 随访时间  例如:2018-08-01 10:00:00
	FollowupDateTime *time.Time `sql:"FollowupDateTime"`
	// 随访类型  例如:三个月例行随访
	FollowupType *string `sql:"FollowupType"`
	// 随访方式 例如：电话、门诊、家庭、APP 例如:电话
	FollowupMethod string `sql:"FollowupMethod"`
	// 结果状态 用于描述本次随访是否有效，0-失访，1-进行中，2-有效，默认值为1 例如:1
	Status uint64 `sql:"Status"`
	// 失访原因 只有当Status为0时该字段才有意义，用于描述失访的原因。 例如:电话停机
	FailureReason *string `sql:"FailureReason"`
	// 死亡时间 该字段只有当失访原因为亡故时才有意义 例如:2018-08-02
	DeathTime *time.Time `sql:"DeathTime"`
	// 死亡原因 该字段只有当失访原因为亡故时才有意义 例如:车祸死亡
	CauseOfDeath *string `sql:"CauseOfDeath"`
	// 随访模板代码 1-高血压随访记录表, 2-糖尿病随访记录表, 3-慢阻肺随访记录表, 4-公卫高血压患者随访服务记录表, 5-公卫2型糖尿病患者随访服务记录表 例如:1
	FollowupTemplateCode uint64 `sql:"FollowupTemplateCode"`
	// 随访记录内容模板版本号 例如:1
	FollowupTemplateVersion uint64 `sql:"FollowupTemplateVersion"`
	// 随访记录摘要 摘要规则根据模板不同而不同。 例如:血压170/99mmHg，控制不满意，嘱患者近期复诊。
	Summary *string `sql:"Summary"`
	// 随访记录内容 JSON格式保存 例如:
	Content *string `sql:"Content"`
	// 随访人ID  例如:111
	DoctorID uint64 `sql:"DoctorID"`
	// 随访人姓名  例如:张三
	DoctorName string `sql:"DoctorName"`
	// 随访计划序号 本次随访记录关联的随访计划的序号 例如:4354
	FollowupPlanSerialNo *uint64 `sql:"FollowupPlanSerialNo"`
}

func (s *FollowupRecord) CopyTo(target *doctor.FollowupRecord) {
	if target == nil {
		return
	}
	target.SerialNo = s.SerialNo
	target.PatientID = s.PatientID
	if s.FollowupDateTime == nil {
		target.FollowupDateTime = nil
	} else {
		followupDateTime := types.Time(*s.FollowupDateTime)
		target.FollowupDateTime = &followupDateTime
	}
	if s.FollowupType != nil {
		target.FollowupType = string(*s.FollowupType)
	}
	target.FollowupMethod = s.FollowupMethod
	target.Status = s.Status
	if s.FailureReason != nil {
		target.FailureReason = string(*s.FailureReason)
	}
	if s.DeathTime == nil {
		target.DeathTime = nil
	} else {
		deathTime := types.Time(*s.DeathTime)
		target.DeathTime = &deathTime
	}
	if s.CauseOfDeath != nil {
		target.CauseOfDeath = string(*s.CauseOfDeath)
	}
	target.FollowupTemplateCode = s.FollowupTemplateCode
	target.FollowupTemplateVersion = s.FollowupTemplateVersion
	if s.Summary != nil {
		target.Summary = string(*s.Summary)
	}
	content := ""
	if s.Content != nil {
		content = *s.Content
	}
	if len(content) > 0 {
		json.Unmarshal([]byte(content), &target.Content)
	}

	target.DoctorID = s.DoctorID
	target.DoctorName = s.DoctorName
	target.FollowupPlanSerialNo = s.FollowupPlanSerialNo
}

func (s *FollowupRecord) CopyFrom(source *doctor.FollowupRecord) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.PatientID = source.PatientID
	if source.FollowupDateTime == nil {
		s.FollowupDateTime = nil
	} else {
		followupDateTime := time.Time(*source.FollowupDateTime)
		s.FollowupDateTime = &followupDateTime
	}
	followupType := string(source.FollowupType)
	s.FollowupType = &followupType
	s.FollowupMethod = source.FollowupMethod
	s.Status = source.Status
	failureReason := string(source.FailureReason)
	s.FailureReason = &failureReason
	if source.DeathTime == nil {
		s.DeathTime = nil
	} else {
		deathTime := time.Time(*source.DeathTime)
		s.DeathTime = &deathTime
	}
	causeOfDeath := string(source.CauseOfDeath)
	s.CauseOfDeath = &causeOfDeath
	s.FollowupTemplateCode = source.FollowupTemplateCode
	s.FollowupTemplateVersion = source.FollowupTemplateVersion
	summary := string(source.Summary)
	s.Summary = &summary
	if source.Content != nil {
		contentData, err := json.Marshal(source.Content)
		if err == nil {
			content := string(contentData)
			s.Content = &content
		}
	}

	s.DoctorID = source.DoctorID
	s.DoctorName = source.DoctorName
	s.FollowupPlanSerialNo = source.FollowupPlanSerialNo
}
