package sqldb

type ExpDoctorCommentTemplateOrderEditorFilter struct {
	ExpDoctorCommentTemplateOrderBase
	// 编辑者id
	EditorID uint64 `sql:"EditorID"`
}
