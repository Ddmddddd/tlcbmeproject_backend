package sqldb

import (
	"github.com/ktpswjz/httpserver/types"
	"time"
	"tlcbme_project/data/model/doctor"
)

type DietRecordFilterEx struct {

	// 用户ID
	PatientID uint64 `sql:"PatientID"`

	// 用餐日期\n2018-07-03 \nFCM\n用餐的实际日期
	EatStartDateTime *time.Time `sql:"EatDateTime" filter:">="`

	EatEndDateTime *time.Time `sql:"EatDateTime" filter:"<"`
}

type DietRecordDateFilter struct {
	// 用餐日期\n2018-07-03 \nFCM\n用餐的实际日期
	EatStartDateTime *time.Time `sql:"EatDateTime" filter:">="`

	EatEndDateTime *time.Time `sql:"EatDateTime" filter:"<"`
}

type DietRecordUser struct {
	DietRecordBase
	// 用户ID
	PatientID uint64 `sql:"PatientID"`
}

func (s *DietRecord) CopyToEx(target *doctor.DietRecordEx) {
	if target == nil {
		return
	}
	target.SerialNo = s.SerialNo
	if s.EatDateTime == nil {
		target.EatDateTime = nil
	} else {
		eatDateTime := types.Time(*s.EatDateTime)
		target.EatDateTime = &eatDateTime
	}
	if s.InputDateTime == nil {
		target.InputDateTime = nil
	} else {
		inputDateTime := types.Time(*s.InputDateTime)
		target.InputDateTime = &inputDateTime
	}
	target.DietType = s.DietType
	target.PatientID = s.PatientID
}

type DietRecordFilter struct {
	DietRecordBase
	SerialNo uint64 `sql:"SerialNo" primary:"true" auto:"true"`
}

func (s *DietRecordFilter) CopyFrom(source *doctor.DietRecordFilter) {
	s.SerialNo = source.SerialNo
}
