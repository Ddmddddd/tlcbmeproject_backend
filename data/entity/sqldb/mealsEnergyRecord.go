package sqldb

import "tlcbme_project/data/model/doctor"

type MealsEnergyRecordBase struct {
}

func (s MealsEnergyRecordBase) TableName() string {
	return "MealsEnergyRecord"
}

// 三餐能量记录表
type MealsEnergyRecord struct {
	MealsEnergyRecordBase
	//
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 用餐类型 早餐、中餐、晚餐
	DietType string `sql:"DietType"`
	// 蛋白质热量
	ProteinEnergy *float64 `sql:"ProteinEnergy"`
	// 脂肪热量
	FatEnergy *float64 `sql:"FatEnergy"`
	// 碳水化合物 热量
	CarbohydrateEnergy *float64 `sql:"CarbohydrateEnergy"`
	// 关联营养分析结论表 id
	NutritionalAnalysisSerialNo uint64 `sql:"NutritionalAnalysisSerialNo"`
	// 用户ID
	PatientID uint64 `sql:"PatientID"`
}

func (s *MealsEnergyRecord) CopyTo(target *doctor.MealsEnergyRecord) {
	if target == nil {
		return
	}
	target.SerialNo = s.SerialNo
	target.DietType = s.DietType
	target.ProteinEnergy = s.ProteinEnergy
	target.FatEnergy = s.FatEnergy
	target.CarbohydrateEnergy = s.CarbohydrateEnergy
	target.NutritionalAnalysisSerialNo = s.NutritionalAnalysisSerialNo
	target.PatientID = s.PatientID
}

func (s *MealsEnergyRecord) CopyFrom(source *doctor.MealsEnergyRecord) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.DietType = source.DietType
	s.ProteinEnergy = source.ProteinEnergy
	s.FatEnergy = source.FatEnergy
	s.CarbohydrateEnergy = source.CarbohydrateEnergy
	s.NutritionalAnalysisSerialNo = source.NutritionalAnalysisSerialNo
	s.PatientID = source.PatientID
}
