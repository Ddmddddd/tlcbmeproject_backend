package sqldb

import (
	"tlcbme_project/data/model/doctor"
	"fmt"
)

func (s *OrgDict) CopyFromCreate(source *doctor.OrgDictCreate) {
	if source == nil {
		return
	}
	s.OrgCode = source.OrgCode
	s.OrgName = source.OrgName
	if len(source.DivisionCode) > 0 {
		s.DivisionCode = &source.DivisionCode
	}
}

type OrgDictKeywordFilter struct {
	OrgDictBase

	// 机构名称  例如:第一人民医院
	OrgName string `sql:"OrgName" filter:"like"`
	// 输入码 用于快速输入，可以是拼音首字母或缩写 例如:gxy
	InputCode string `sql:"InputCode" filter:"like"`
}

func (s *OrgDictKeywordFilter) CopyFrom(source *doctor.DictKeywordFilter) {
	if source == nil {
		return
	}

	if len(source.Keyword) > 0 {
		s.OrgName = fmt.Sprint("%", source.Keyword, "%")
		s.InputCode = fmt.Sprint("%", source.Keyword, "%")
	}
}

type OrgDictFilter struct {
	OrgDictBase
	DictFilter

	// 机构代码 组织机构代码 例如:2894782947239239
	OrgCode string `sql:"OrgCode"`
	// 机构名称  例如:第一人民医院
	OrgName string `sql:"OrgName"`
	// 输入码 用于快速输入，可以是拼音首字母或缩写 例如:gxy
	InputCode *string `sql:"InputCode"`
}

func (s *OrgDictFilter) CopyFrom(source *doctor.DictFilter) {
	if source == nil {
		return
	}

	s.SerialNo = source.SerialNo
}

type OrgDictUpdate struct {
	OrgDictBase

	// 机构名称  例如:第一人民医院
	OrgName string `sql:"OrgName"`
	// 行政区划 例如:2332232
	DivisionCode *string `sql:"DivisionCode"`
}

func (s *OrgDictUpdate) CopyFrom(source *doctor.OrgDictUpdate) {
	if source == nil {
		return
	}

	s.OrgName = source.OrgName
	if len(source.DivisionCode) > 0 {
		s.DivisionCode = &source.DivisionCode
	}
}

type OrgDictName struct {
	OrgDictBase

	// 机构名称  例如:第一人民医院
	OrgName string `sql:"OrgName"`
}

type OrgDictDivisionCodeFilter struct {
	OrgDictBase

	DivisionCode string `sql:"DivisionCode"`
	// 有效性 用于描述本记录是否有效，0-无效，1-有效，默认值为1 例如:1
	IsValid uint64 `sql:"IsValid"`
}
