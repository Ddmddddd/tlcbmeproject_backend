package sqldb

import (
	"fmt"
	"strings"
	"time"
	"tlcbme_project/data/enum"
	"tlcbme_project/data/model"
	"tlcbme_project/data/model/doctor"
)

type ViewManagedPatientIndexListFilter struct {
	ViewManagedPatientIndexBase

	// 用户ID 关联患者用户登录表用户ID 例如:232442
	PatientID *uint64 `sql:"PatientID"`

	// 管理机构代码 管理该患者的机构 例如:897798
	OrgCode *string `sql:"OrgCode"`

	// 医生ID 例如:232
	DoctorID *uint64 `sql:"DoctorID"`

	// 健康管理师ID 例如:232
	HealthManagerID *uint64 `sql:"HealthManagerID"`

	HealthManagerIDList []uint64 `sql:"HealthManagerID" filter:"IN"`

	// 管理状态 0-管理中，1-迁出，2-已终止管理 例如:0
	ManageStatus []uint64 `sql:"ManageStatus" filter:"IN"`

	// 管理开始时间 最近一次管理的开始时间，如果是刚注册，则默认为审核通过时间 例如:2018-07-03 13:15:00
	ManageStartDateTime *time.Time `sql:"ManageStartDateTime" filter:">="`

	// 姓名  例如:张三
	PatientName string `sql:"PatientName" filter:"like"`

	// 活跃度 0-不活跃，1-活跃 例如:0
	PatientActiveDegree []uint64 `sql:"PatientActiveDegree" filter:"IN"`

	// 组队信息所属的大队
	GroupContentList []string `sql:"BelongToGroup" filter:"IN"`
	// 组队信息所属的小队
	TeamContentList []string `sql:"TeamNickName" filter:"IN"`
	// 所属训练营
	CountryContentList []string `sql:"Country" filter:"IN"`
}

func (s *ViewManagedPatientIndexListFilter) CopyFrom(source *doctor.ViewManagedPatientIndexListFilterEx) {
	if source == nil {
		return
	}

	s.OrgCode = &source.OrgCode
	if !source.ViewOrgAllPatients {
		s.DoctorID = source.DoctorID
		s.HealthManagerID = source.HealthManagerID
	}
	if len(source.ManageStatuses) > 0 {
		s.ManageStatus = source.ManageStatuses
	}
	if len(source.PatientActiveDegree) > 0 {
		s.PatientActiveDegree = source.PatientActiveDegree
	}
	if source.StartDaysAgo != nil {
		days := 0 - int(*source.StartDaysAgo)
		if days < 0 {
			now := time.Now().AddDate(0, 0, days)
			start := time.Date(now.Year(), now.Month(), now.Day(), 0, 0, 0, 0, now.Location())
			s.ManageStartDateTime = &start
		}
	}
	if len(source.PatientName) > 0 {
		s.PatientName = fmt.Sprint("%", source.PatientName, "%")
	}
	if len(source.HealthManagerIDList) > 0 {
		s.HealthManagerIDList = source.HealthManagerIDList
	}
	if len(source.GroupContentList) > 0 {
		s.GroupContentList = source.GroupContentList
	}
	if len(source.TeamContentList) > 0 {
		s.TeamContentList = source.TeamContentList
	}
	if len(source.CountryContentList) > 0 {
		s.CountryContentList = source.CountryContentList
	}
}

type ViewManagedPatientIndexListOrder struct {
	ViewManagedPatientIndexBase

	// 序号 主键，自增 例如:324
	SerialNo uint64 `sql:"SerialNo" index:"100"`

	// 管理开始时间 最近一次管理的开始时间，如果是刚注册，则默认为审核通过时间 例如:2018-07-03 13:15:00
	ManageStartDateTime *time.Time `sql:"ManageStartDateTime" order:"DESC"`
}

func (s *ViewManagedPatientIndex) CopyToEx(target *doctor.ViewManagedPatientIndexEx, bmi *float64) {
	if target == nil {
		return
	}

	s.CopyTo(&target.ViewManagedPatientIndex)

	if s.Sex != nil {
		target.SexText = enum.Sexes.Value(*s.Sex)
	}
	if s.DateOfBirth != nil {
		age := time.Now().Sub(*s.DateOfBirth)
		target.Age = int64(age.Hours() / (24 * 365))
	}

	tagCount := 0
	tag := &strings.Builder{}
	if s.ManageClass != nil {
		vs := strings.Split(*s.ManageClass, ",")
		for _, v := range vs {
			tv := strings.TrimSpace(v)
			if len(tv) < 1 {
				continue
			}
			tagCount++
			if tagCount > 1 {
				tag.WriteString(" ")
			}
			tag.WriteString(tv)
		}
	}
	if s.PatientFeature != nil {
		vs := strings.Split(*s.PatientFeature, ",")
		for _, v := range vs {
			tv := strings.TrimSpace(v)
			if len(tv) < 1 {
				continue
			}
			tagCount++
			if tagCount > 1 {
				tag.WriteString(" ")
			}
			tag.WriteString(tv)
		}
	}
	if target.Age >= 45 && target.Age < 60 {
		tag.WriteString(" 中年人")
	} else if target.Age >= 60 {
		tag.WriteString(" 老年人")
	}

	if bmi != nil {
		bmi := *bmi
		if bmi >= 24 && bmi < 27 {
			tag.WriteString(" 超重")
		} else if bmi >= 27 && bmi < 30 {
			tag.WriteString(" 肥胖")
		} else if bmi >= 30 {
			tag.WriteString(" 重度肥胖")
		}
	}

	target.Tag = tag.String()
}

type ViewManagedPatientIndexOrderBase struct {
	ViewManagedPatientIndexBase

	// 序号 主键，自增 例如:324
	SerialNo uint64 `sql:"SerialNo" index:"100"`
}

type ViewManagedPatientIndexOrderStartDateTimeAscending struct {
	ViewManagedPatientIndexOrderBase

	// 管理开始时间 最近一次管理的开始时间，如果是刚注册，则默认为审核通过时间 例如:2018-07-03 13:15:00
	ManageStartDateTime *time.Time `sql:"ManageStartDateTime"`
}

type ViewManagedPatientIndexOrderStartDateTimeDescending struct {
	ViewManagedPatientIndexOrderBase

	// 管理开始时间 最近一次管理的开始时间，如果是刚注册，则默认为审核通过时间 例如:2018-07-03 13:15:00
	ManageStartDateTime *time.Time `sql:"ManageStartDateTime" order:"DESC"`
}

type ViewManagedPatientIndexOrderComplianceRateAscending struct {
	ViewManagedPatientIndexOrderBase

	// 依从度 值为0至5，0表示依从度未知 例如:4
	ComplianceRate *uint64 `sql:"ComplianceRate"`
}

type ViewManagedPatientIndexOrderComplianceRateDescending struct {
	ViewManagedPatientIndexOrderBase

	// 依从度 值为0至5，0表示依从度未知 例如:4
	ComplianceRate *uint64 `sql:"ComplianceRate" order:"DESC"`
}

var viewManagedPatientIndexOrders = make(map[string]interface{}, 0)

func init() {
	viewManagedPatientIndexOrders["manageStartDateTime-ascending"] = &ViewManagedPatientIndexOrderStartDateTimeAscending{}
	viewManagedPatientIndexOrders["manageStartDateTime-descending"] = &ViewManagedPatientIndexOrderStartDateTimeDescending{}

	viewManagedPatientIndexOrders["complianceRate-ascending"] = &ViewManagedPatientIndexOrderComplianceRateAscending{}
	viewManagedPatientIndexOrders["complianceRate-descending"] = &ViewManagedPatientIndexOrderComplianceRateDescending{}
}

func (s *ViewManagedPatientIndex) GetOrder(order *model.Order) interface{} {
	if order == nil {
		return &ViewManagedPatientIndexListOrder{}
	}

	key := order.Key()
	if len(key) < 1 {
		return &ViewManagedPatientIndexListOrder{}
	}

	v, ok := viewManagedPatientIndexOrders[key]
	if ok {
		return v
	} else {
		return &ViewManagedPatientIndexListOrder{}
	}
}

type ViewManagedPatientIndexDetail struct {
	ViewManagedPatientIndexBase
	// 姓名  例如:张三
	PatientName *string `sql:"PatientName"`
	// 性别 0-未知, 1-男, 2-女，9-未说明 例如:1
	Sex *uint64 `sql:"Sex"`
	// 出生日期 例如:2000-12-12
	DateOfBirth *time.Time `sql:"DateOfBirth"`
	// 患者特点 如有多个，之间用逗号分隔，例如：老年人，肥胖，残疾人 例如:老年人，肥胖
	PatientFeature *string `sql:"PatientFeature"`
	// 依从度 值为0至5，0表示依从度未知 例如:4
	ComplianceRate *uint64 `sql:"ComplianceRate"`
}

func (s *ViewManagedPatientIndexDetail) CopyToApp(target *doctor.ExpPatientTaskRecordForApp) {
	if target == nil {
		return
	}
	if *s.Sex == 1 {
		target.SexText = "男"
	} else if *s.Sex == 2 {
		target.SexText = "女"
	}
	if s.PatientName != nil {
		target.PatientName = string(*s.PatientName)
	}
	if s.DateOfBirth != nil {
		age := time.Now().Sub(*s.DateOfBirth)
		target.Age = int64(age.Hours() / (24 * 365))
	}
	if s.PatientFeature != nil {
		target.Tag = string(*s.PatientFeature)
	}
	target.ComplianceRate = s.ComplianceRate
}

type ViewManagedPatientIndexDetailFilter struct {
	ViewManagedPatientIndexBase
	// 用户ID 关联患者用户登录表用户ID 例如:232442
	PatientID uint64 `sql:"PatientID"`
}

func (s *ViewManagedIndex) CopyToApp(target *doctor.ExpPatientTaskRecordForApp) {
	if target == nil {
		return
	}
	target.PatientID = s.PatientID
	if s.PatientName != nil {
		target.PatientName = *s.PatientName
	}
	if s.DateOfBirth != nil {
		age := time.Now().Sub(*s.DateOfBirth)
		target.Age = int64(age.Hours() / (24 * 365))
	}
	if s.PatientFeature != nil {
		target.Tag = string(*s.PatientFeature)
	}
	if *s.Sex == 1 {
		target.SexText = "男"
	} else if *s.Sex == 2 {
		target.SexText = "女"
	}
	target.ComplianceRate = s.ComplianceRate
}
