package sqldb

import (
	"tlcbme_project/data/model/doctor"
	"github.com/ktpswjz/httpserver/types"
	"time"
)

type ViewStatDiscomfortBase struct {
}

func (s ViewStatDiscomfortBase) TableName() string {
	return "ViewStatDiscomfort"
}

// VIEW
type ViewStatDiscomfort struct {
	ViewStatDiscomfortBase
	// 用户ID 关联患者用户登录表用户ID 例如:232442
	PatientID uint64 `sql:"PatientID"`
	// 不适情况 如果有多种不适，之间用逗号分隔。例如：剧烈头痛，恶心呕吐。 例如:剧烈头痛，恶心呕吐
	Discomfort string `sql:"Discomfort"`
	//
	Count uint64 `sql:"Count"`
	// 发生时间 不适情况发生的时间 例如:2018-07-03 14:45:00
	HappenDateTimeStart *time.Time `sql:"HappenDateTimeStart"`
	// 发生时间 不适情况发生的时间 例如:2018-07-03 14:45:00
	HappenDateTimeEnd *time.Time `sql:"HappenDateTimeEnd"`
}

func (s *ViewStatDiscomfort) CopyTo(target *doctor.ViewStatDiscomfort) {
	if target == nil {
		return
	}
	target.PatientID = s.PatientID
	target.Discomfort = s.Discomfort
	target.Count = s.Count
	if s.HappenDateTimeStart == nil {
		target.HappenDateTimeStart = nil
	} else {
		happenDateTimeStart := types.Time(*s.HappenDateTimeStart)
		target.HappenDateTimeStart = &happenDateTimeStart
	}
	if s.HappenDateTimeEnd == nil {
		target.HappenDateTimeEnd = nil
	} else {
		happenDateTimeEnd := types.Time(*s.HappenDateTimeEnd)
		target.HappenDateTimeEnd = &happenDateTimeEnd
	}
}

func (s *ViewStatDiscomfort) CopyFrom(source *doctor.ViewStatDiscomfort) {
	if source == nil {
		return
	}
	s.PatientID = source.PatientID
	s.Discomfort = source.Discomfort
	s.Count = source.Count
	if source.HappenDateTimeStart == nil {
		s.HappenDateTimeStart = nil
	} else {
		happenDateTimeStart := time.Time(*source.HappenDateTimeStart)
		s.HappenDateTimeStart = &happenDateTimeStart
	}
	if source.HappenDateTimeEnd == nil {
		s.HappenDateTimeEnd = nil
	} else {
		happenDateTimeEnd := time.Time(*source.HappenDateTimeEnd)
		s.HappenDateTimeEnd = &happenDateTimeEnd
	}
}
