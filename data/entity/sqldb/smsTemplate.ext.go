package sqldb

type SmsTemplateFilter struct {
	SmsTemplateBase
	// 有效性 用于描述本记录是否有效，0-无效，1-有效，默认值为1 例如:1
	IsValid uint64 `sql:"IsValid"`
}

type SmsTemplateUseFilter struct {
	// 模板名称  例如:复诊提醒短信
	TemplateName string `sql:"TemplateName"`
}
