package sqldb

import (
	"tlcbme_project/data/model/doctor"
	"github.com/ktpswjz/httpserver/types"
	"time"
)

type DoctorPatientChatMsgBase struct {
}

func (s DoctorPatientChatMsgBase) TableName() string {
	return "DoctorPatientChatMsg"
}

// 慢病管理业务
// 工作平台管理数据
// 医患沟通消息记录
// 注释：该表保存医患沟通过程中的消息记录。
type DoctorPatientChatMsg struct {
	DoctorPatientChatMsgBase
	// 序号 主键，自增 例如:324
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 发送者ID 关联患者或医生用户登录表用户ID 例如:232442
	SenderID uint64 `sql:"SenderID"`
	// 接受者ID 关联患者或医生用户登录表用户ID 例如:232442
	ReceiverID uint64 `sql:"ReceiverID"`
	// 消息内容 医患沟通的消息内容 例如:你好
	MsgContent string `sql:"MsgContent"`
	// 消息时间 消息发生的时间 例如:2018-07-03 14:45:00
	MsgDateTime *time.Time `sql:"MsgDateTime"`
	// 消息标识 0-未读，1-已读 例如:0
	MsgFlag uint64 `sql:"MsgFlag"`
}

func (s *DoctorPatientChatMsg) CopyTo(target *doctor.DoctorPatientChatMsg) {
	if target == nil {
		return
	}
	target.SerialNo = s.SerialNo
	target.SenderID = s.SenderID
	target.ReceiverID = s.ReceiverID
	target.MsgContent = s.MsgContent
	if s.MsgDateTime == nil {
		target.MsgDateTime = nil
	} else {
		msgDateTime := types.Time(*s.MsgDateTime)
		target.MsgDateTime = &msgDateTime
	}
	target.MsgFlag = s.MsgFlag
}

func (s *DoctorPatientChatMsg) CopyFrom(source *doctor.DoctorPatientChatMsg) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.SenderID = source.SenderID
	s.ReceiverID = source.ReceiverID
	s.MsgContent = source.MsgContent
	if source.MsgDateTime == nil {
		s.MsgDateTime = nil
	} else {
		msgDateTime := time.Time(*source.MsgDateTime)
		s.MsgDateTime = &msgDateTime
	}
	s.MsgFlag = source.MsgFlag
}
