package sqldb

import (
	"tlcbme_project/data/model/doctor"
	"github.com/ktpswjz/httpserver/types"
	"time"
)

type DrugUseRecordBase struct {
}

func (s DrugUseRecordBase) TableName() string {
	return "DrugUseRecord"
}

// 慢病管理业务
// 患者自我管理数据
// 用药记录
// 注释：该表保存所有患者的上传的用药记录
type DrugUseRecord struct {
	DrugUseRecordBase
	// 序号 主键，自增 例如:324
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 用户ID 关联患者用户登录表用户ID 例如:232442
	PatientID uint64 `sql:"PatientID"`
	// 时间点 早、中、晚 例如:早
	TimePoint *string `sql:"TimePoint"`
	// 药品名称  例如:氨氯地平片
	DrugName string `sql:"DrugName"`
	// 剂量 例如:5mg
	Dosage *string `sql:"Dosage"`
	// 频次 例如:1次/日
	Freq *string `sql:"Freq"`
	// 备注  例如:
	Memo *string `sql:"Memo"`
	// 用药时间 用药的实际时间 例如:2018-07-03 14:45:00
	UseDateTime *time.Time `sql:"UseDateTime"`
	// 录入时间 将用药情况录入系统的时间 例如:2018-07-03 14:00:00
	InputDateTime *time.Time `sql:"InputDateTime"`
}

func (s *DrugUseRecord) CopyTo(target *doctor.DrugUseRecord) {
	if target == nil {
		return
	}
	target.SerialNo = s.SerialNo
	target.PatientID = s.PatientID
	if s.TimePoint != nil {
		target.TimePoint = string(*s.TimePoint)
	}
	target.DrugName = s.DrugName
	if s.Dosage != nil {
		target.Dosage = string(*s.Dosage)
	}
	if s.Freq != nil {
		target.Freq = string(*s.Freq)
	}
	if s.Memo != nil {
		target.Memo = string(*s.Memo)
	}
	if s.UseDateTime == nil {
		target.UseDateTime = nil
	} else {
		useDateTime := types.Time(*s.UseDateTime)
		target.UseDateTime = &useDateTime
	}
	if s.InputDateTime == nil {
		target.InputDateTime = nil
	} else {
		inputDateTime := types.Time(*s.InputDateTime)
		target.InputDateTime = &inputDateTime
	}
}

func (s *DrugUseRecord) CopyFrom(source *doctor.DrugUseRecord) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.PatientID = source.PatientID
	timePoint := string(source.TimePoint)
	s.TimePoint = &timePoint
	s.DrugName = source.DrugName
	dosage := string(source.Dosage)
	s.Dosage = &dosage
	freq := string(source.Freq)
	s.Freq = &freq
	memo := string(source.Memo)
	s.Memo = &memo
	if source.UseDateTime == nil {
		s.UseDateTime = nil
	} else {
		useDateTime := time.Time(*source.UseDateTime)
		s.UseDateTime = &useDateTime
	}
	if source.InputDateTime == nil {
		s.InputDateTime = nil
	} else {
		inputDateTime := time.Time(*source.InputDateTime)
		s.InputDateTime = &inputDateTime
	}
}
