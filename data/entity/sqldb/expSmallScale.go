package sqldb

import (
	"tlcbme_project/data/model/doctor"
	"encoding/json"
)

type ExpSmallScaleBase struct {
}

func (s ExpSmallScaleBase) TableName() string {
	return "ExpSmallScale"
}

// 小量表
type ExpSmallScale struct {
	ExpSmallScaleBase
	// 序号
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 任务序号
	TaskID uint64 `sql:"TaskID"`
	// 量表内容，json格式
	Content string `sql:"Content"`
}

func (s *ExpSmallScale) CopyTo(target *doctor.ExpSmallScale) {
	if target == nil {
		return
	}
	target.SerialNo = s.SerialNo
	target.TaskID = s.TaskID
	content := s.Content
	if len(content) > 0 {
		json.Unmarshal([]byte(content), &target.Content)
	}

}

func (s *ExpSmallScale) CopyFrom(source *doctor.ExpSmallScale) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.TaskID = source.TaskID
	if source.Content != nil {
		contentData, err := json.Marshal(source.Content)
		if err == nil {
			content := string(contentData)
			s.Content = content
		}
	}
}
