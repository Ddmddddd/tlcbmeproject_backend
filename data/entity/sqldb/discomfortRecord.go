package sqldb

import (
	"tlcbme_project/data/model/doctor"
	"github.com/ktpswjz/httpserver/types"
	"time"
)

type DiscomfortRecordBase struct {
}

func (s DiscomfortRecordBase) TableName() string {
	return "DiscomfortRecord"
}

// 慢病管理业务
// 患者自我管理数据
// 不适情况记录表
// 注释：本表保存患者的不适情况记录。
type DiscomfortRecord struct {
	DiscomfortRecordBase
	// 序号 主键，自增 例如:324
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 用户ID 关联患者用户登录表用户ID 例如:232442
	PatientID uint64 `sql:"PatientID"`
	// 不适情况 如果有多种不适，之间用逗号分隔。例如：剧烈头痛，恶心呕吐。 例如:剧烈头痛，恶心呕吐
	Discomfort string `sql:"Discomfort"`
	// 备注  例如:
	Memo *string `sql:"Memo"`
	// 发生时间 不适情况发生的时间 例如:2018-07-03 14:45:00
	HappenDateTime *time.Time `sql:"HappenDateTime"`
	// 录入时间 录入时间 例如:2018-07-03 14:00:00
	InputDateTime *time.Time `sql:"InputDateTime"`
}

func (s *DiscomfortRecord) CopyTo(target *doctor.DiscomfortRecord) {
	if target == nil {
		return
	}
	target.SerialNo = s.SerialNo
	target.PatientID = s.PatientID
	target.Discomfort = s.Discomfort
	if s.Memo != nil {
		target.Memo = string(*s.Memo)
	}
	if s.HappenDateTime == nil {
		target.HappenDateTime = nil
	} else {
		happenDateTime := types.Time(*s.HappenDateTime)
		target.HappenDateTime = &happenDateTime
	}
	if s.InputDateTime == nil {
		target.InputDateTime = nil
	} else {
		inputDateTime := types.Time(*s.InputDateTime)
		target.InputDateTime = &inputDateTime
	}
}

func (s *DiscomfortRecord) CopyFrom(source *doctor.DiscomfortRecord) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.PatientID = source.PatientID
	s.Discomfort = source.Discomfort
	memo := string(source.Memo)
	s.Memo = &memo
	if source.HappenDateTime == nil {
		s.HappenDateTime = nil
	} else {
		happenDateTime := time.Time(*source.HappenDateTime)
		s.HappenDateTime = &happenDateTime
	}
	if source.InputDateTime == nil {
		s.InputDateTime = nil
	} else {
		inputDateTime := time.Time(*source.InputDateTime)
		s.InputDateTime = &inputDateTime
	}
}
