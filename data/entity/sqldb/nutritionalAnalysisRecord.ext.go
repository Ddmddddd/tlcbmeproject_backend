package sqldb

import (
	"encoding/json"
	"github.com/ktpswjz/httpserver/types"
	"strings"
	"time"
	"tlcbme_project/data/model/doctor"
)

type NutritionalAnalysisRecordFilter struct {
	//AnalysisDateTime *time.Time `sql:"AnalysisDateTime"`
	PatientID uint64 `sql:"PatientID"`
}

type NutritionalAnalysisRecordNoFilter struct {
	//
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
}

type NutritionalAnalysisRecordFilterEx struct {
	//AnalysisDateTime *time.Time `sql:"AnalysisDateTime"`
	PatientID     uint64     `sql:"PatientID"`
	StartDateTime *time.Time `sql:"AnalysisDateTime" filter:">="`
	EndDateTime   *time.Time `sql:"AnalysisDateTime" filter:"<"`
}

func (s *NutritionalAnalysisRecord) CopyFromEx(result *doctor.NutrientAnalysisResult, filter *doctor.NutrientAnalysisFilter) {
	if result == nil || filter == nil {
		return
	}
	s.Fat = &result.AllIntake.Fat.Actual
	s.Protein = &result.AllIntake.Protein.Actual
	s.Conclusion = strings.Join(result.Conclusion, "；")
	if filter.AnalysisDateTime != nil {
		dateTime := time.Time(*filter.AnalysisDateTime)
		s.AnalysisDateTime = &dateTime
	}
	s.Carbohydrate = &result.AllIntake.Carbohydrate.Actual
	s.PatientID = filter.PatientID
	s.Energy = result.AllIntake.Energy.Actual
	s.Na = &result.Na.Actual
	s.Se = &result.SE.Actual
	s.K = &result.POT.Actual
	s.Mg = &result.MG.Actual
	s.Bb6 = &result.VITB6.Actual
	s.Ca = &result.CA.Actual
	s.Grade = result.Grade
	s.Cholesterol = &result.CHO.Actual
	s.DietaryFiber = &result.DF.Actual
	s.I = &result.IODINE.Actual
	s.Fe = &result.FE.Actual
	s.FolicAcid = &result.USP.Actual
	s.Va = &result.VITA.Actual
	s.Vb1 = &result.VITB1.Actual
	s.Vb2 = &result.VITB2.Actual
	s.Vc = &result.VITC.Actual
	s.Vd = &result.VITD.Actual
	s.Ve = &result.VITD.Actual
	s.Zn = &result.ZN.Actual
	if result.DietPlan != nil {
		recordData, err := json.Marshal(result.DietPlan)
		if err == nil {
			record := string(recordData)
			s.DietPlan = &record
		}
	}
}

func (s *NutritionalAnalysisRecord) CopyToEnergy(grade *doctor.Grade) {
	if s == nil {
		return
	}
	grade.Value = s.Energy
	if s.AnalysisDateTime != nil {
		date := types.Time(*s.AnalysisDateTime)
		grade.DateTime = &date
	}
}

func (s *NutritionalAnalysisRecord) CopyToGrade(grade *doctor.Grade) {
	if s == nil {
		return
	}
	grade.Value = s.Grade
	if s.AnalysisDateTime != nil {
		date := types.Time(*s.AnalysisDateTime)
		grade.DateTime = &date
	}
}
