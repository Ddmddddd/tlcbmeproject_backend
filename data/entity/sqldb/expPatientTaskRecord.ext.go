package sqldb

import (
	"encoding/json"
	"github.com/ktpswjz/httpserver/types"
	"time"
	"tlcbme_project/data/model/doctor"
)

type ExpPatientTaskRecordInput struct {
	ExpPatientTaskRecordBase
	// 患者id
	PatientID uint64 `sql:"PatientID"`
	// 对应taskRepository中的serialNo，为0则代表该任务不是按期完成的
	TaskID uint64 `sql:"TaskID"`
	// 父任务名
	ParentTaskName *string `sql:"ParentTaskName"`
	// 任务名
	TaskName string `sql:"TaskName"`
	// 创建时间
	CreateDateTime *time.Time `sql:"CreateDateTime"`
	// 获取积分
	Credit uint64 `sql:"Credit"`
	// 任务完成具体记录
	Record *string `sql:"Record"`
}

func (s *ExpPatientTaskRecordInput) CopyFrom(source *doctor.ExpPatientTaskRecordInput) {
	if source == nil {
		return
	}
	s.PatientID = source.PatientID
	s.TaskID = source.TaskID
	if source.ParentTaskName == nil {
		s.ParentTaskName = nil
	} else {
		s.ParentTaskName = source.ParentTaskName
	}
	s.TaskName = source.TaskName
	if source.CreateDateTime == nil {
		s.CreateDateTime = nil
	} else {
		createDateTime := time.Time(*source.CreateDateTime)
		s.CreateDateTime = &createDateTime
	}
	s.Credit = source.Credit
	if source.Record != nil {
		recordData, err := json.Marshal(source.Record)
		if err == nil {
			record := string(recordData)
			s.Record = &record
		}
	}
}

type ExpPatientTaskRecordGetByTaskIDsAndTime struct {
	ExpPatientTaskRecordBase
	// 患者id
	PatientID uint64 `sql:"PatientID"`
	// 对应taskRepository中的serialNo，为0则代表该任务不是按期完成的
	TaskIDs []uint64 `sql:"TaskID" filter:"in"`
	// 创建时间
	CreateDateTime *time.Time `sql:"CreateDateTime" filter:">"`
}

func (s *ExpPatientTaskRecordGetByTaskIDsAndTime) CopyFrom(source *doctor.ExpPatientTaskRecordGetByTaskIDsAndTime) {
	if source == nil {
		return
	}
	s.PatientID = source.PatientID
	s.TaskIDs = source.TaskIDs
	if source.CreateDateTime == nil {
		s.CreateDateTime = nil
	} else {
		createDateTime := time.Time(*source.CreateDateTime)
		s.CreateDateTime = &createDateTime
	}
}

type ExpPatientTaskRecordGetByPatientID struct {
	ExpPatientTaskRecordBase
	// 患者id
	PatientID uint64 `sql:"PatientID"`
}

type ExpPatientTaskRecordSerialNoOrder struct {
	ExpPatientTaskRecordBase
	//
	SerialNo uint64 `sql:"SerialNo" primary:"true" order:"desc"`
}

type ExpPatientTaskRecordGetBySerialNo struct {
	ExpPatientTaskRecordBase
	//
	SerialNo uint64 `sql:"SerialNo" primary:"true"`
}

type ExpPatientTaskRecordForApp struct {
	ExpPatientTaskRecordBase
	//
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 患者id
	PatientID uint64 `sql:"PatientID"`
	// 父任务名
	ParentTaskName *string `sql:"ParentTaskName"`
	// 任务名
	TaskName string `sql:"TaskName"`
	// 对应taskRepository中的serialNo，为0则代表该任务不是按期完成的
	TaskID uint64 `sql:"TaskID"`
	// 创建时间
	CreateDateTime *time.Time `sql:"CreateDateTime"`
	// 任务完成具体记录
	Record *string `sql:"Record"`
}

func (s *ExpPatientTaskRecordForApp) CopyToApp(target *doctor.ExpPatientTaskRecordForApp) {
	if target == nil {
		return
	}
	target.SerialNo = s.SerialNo
	target.PatientID = s.PatientID
	if s.ParentTaskName == nil {
		target.ParentTaskName = nil
	} else {
		target.ParentTaskName = s.ParentTaskName
	}
	target.TaskName = s.TaskName
	target.TaskID = s.TaskID
	if s.CreateDateTime == nil {
		target.CreateDateTime = nil
	} else {
		createDateTime := types.Time(*s.CreateDateTime)
		target.CreateDateTime = &createDateTime
	}
	record := s.Record
	if record != nil {
		if len(*record) > 0 {
			json.Unmarshal([]byte(*record), &target.Record)
		}
	}

}

type ExpPatientTaskRecordGetByPatientIDAndParentTaskName struct {
	ExpPatientTaskRecordBase
	// 患者id
	PatientID uint64 `sql:"PatientID"`
	// 父任务名
	ParentTaskName *string `sql:"ParentTaskName"`
}

type ExpPatientTaskRecordTimeFilter struct {
	ExpPatientTaskRecordBase
	// 患者id
	PatientID uint64 `sql:"PatientID"`
	// 创建时间
	CreateDateTime *time.Time `sql:"CreateDateTime" filter:">="`
}

type ExpPatientTaskRecordFilter struct {
	StartDateTime *time.Time `sql:"CreateDateTime" filter:">="`
	EndDateTime   *time.Time `sql:"CreateDateTime" filter:"<"`
	TaskName      *string    `sql:"TaskName" `
}

func (s *ExpPatientTaskRecordFilter) CopyFrom(source *doctor.ExpPatientTaskRecordGetByName) {
	if s == nil {
		return
	}
	if source.StartDateTime != nil {
		startDateTime := time.Time(*source.StartDateTime)
		s.StartDateTime = &startDateTime
	}
	if source.EndDateTime != nil {
		endDateTime := time.Time(*source.EndDateTime)
		s.EndDateTime = &endDateTime
	}
	s.TaskName = &source.TaskName

}

type ExpPatientTaskRecordLastWeekFilter struct {
	ExpPatientTaskRecordBase
	// 患者id
	PatientID uint64 `sql:"PatientID"`
	// 任务名
	TaskName string `sql:"TaskName"`
	// 创建时间
	CreateDateTime *time.Time `sql:"CreateDateTime" filter:">="`
}

type ExpPatientTaskRecordCheckKnwFilter struct {
	ExpPatientTaskRecordBase
	PatientID      uint64     `sql:"PatientID"`
	ParentTaskName *string    `sql:"ParentTaskName"`
	TaskName       string     `sql:"TaskName"`
	CreateDateTime *time.Time `sql:"CreateDateTime" filter:">="`
}

type ExpPatientTaskRecordForTeamFuncCheck struct {
	ExpPatientTaskRecordBase
	PatientID      uint64     `sql:"PatientID"`
	ParentTaskName []string   `sql:"ParentTaskName" filter:"in"`
	CreateDateTime *time.Time `sql:"CreateDateTime" filter:">="`
}
