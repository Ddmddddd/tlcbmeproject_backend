package sqldb

import (
	"tlcbme_project/data/model/doctor"
	"github.com/ktpswjz/httpserver/types"
	"time"
)

type MonthlyHRInfoBase struct {
}

func (s MonthlyHRInfoBase) TableName() string {
	return "MonthlyHRInfo"
}

// 慢病管理业务
// 患者APP月报数据
// 月心率统计信息表
// 注释：本表保存患者的月心率统计记录。
type MonthlyHRInfo struct {
	MonthlyHRInfoBase
	// 序号 主键，自增 例如:324
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 用户ID 关联患者用户登录表用户ID 例如:232442
	PatientID uint64 `sql:"PatientID"`
	// 月份 年份+月份（2016-09） 例如:2016-09
	YearMonth string `sql:"YearMonth"`
	// 最大心率 当月出现的最大心率值 例如:108
	MaxHR uint64 `sql:"MaxHR"`
	// 最大心率时间 当月出现最大心率值的时间 例如:2016-09-10 12:00:00
	MaxHRTime *time.Time `sql:"MaxHRTime"`
	// 最小心率 当月出现的最小心率值 例如:56
	MinHR uint64 `sql:"MinHR"`
	// 最小心率时间 当月出现最小心率值的时间 例如:2016-09-11 12:00:00
	MinHRTime *time.Time `sql:"MinHRTime"`
	// 平均心率 当月的平均心率值 例如:68
	AvgHR uint64 `sql:"AvgHR"`
	// 心率正常次数 当月正常心率的次数 例如:6
	NormalTimes uint64 `sql:"NormalTimes"`
	// 心率偏高次数 当月偏高心率的次数 例如:4
	HigherTimes uint64 `sql:"HigherTimes"`
	// 心率偏低次数 当月偏低心率的次数 例如:2
	LowerTimes uint64 `sql:"LowerTimes"`
}

func (s *MonthlyHRInfo) CopyTo(target *doctor.MonthlyHRInfo) {
	if target == nil {
		return
	}
	target.SerialNo = s.SerialNo
	target.PatientID = s.PatientID
	target.YearMonth = s.YearMonth
	target.MaxHR = s.MaxHR
	if s.MaxHRTime == nil {
		target.MaxHRTime = nil
	} else {
		maxHRTime := types.Time(*s.MaxHRTime)
		target.MaxHRTime = &maxHRTime
	}
	target.MinHR = s.MinHR
	if s.MinHRTime == nil {
		target.MinHRTime = nil
	} else {
		minHRTime := types.Time(*s.MinHRTime)
		target.MinHRTime = &minHRTime
	}
	target.AvgHR = s.AvgHR
	target.NormalTimes = s.NormalTimes
	target.HigherTimes = s.HigherTimes
	target.LowerTimes = s.LowerTimes
}

func (s *MonthlyHRInfo) CopyFrom(source *doctor.MonthlyHRInfo) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.PatientID = source.PatientID
	s.YearMonth = source.YearMonth
	s.MaxHR = source.MaxHR
	if source.MaxHRTime == nil {
		s.MaxHRTime = nil
	} else {
		maxHRTime := time.Time(*source.MaxHRTime)
		s.MaxHRTime = &maxHRTime
	}
	s.MinHR = source.MinHR
	if source.MinHRTime == nil {
		s.MinHRTime = nil
	} else {
		minHRTime := time.Time(*source.MinHRTime)
		s.MinHRTime = &minHRTime
	}
	s.AvgHR = source.AvgHR
	s.NormalTimes = source.NormalTimes
	s.HigherTimes = source.HigherTimes
	s.LowerTimes = source.LowerTimes
}
