package sqldb

import "tlcbme_project/data/model/doctor"

type DivisionDictBase struct {
}

func (s DivisionDictBase) TableName() string {
	return "DivisionDict"
}

// 平台运维
// 基础字典
// 行政区划字典
// 注释：本表保存行政区划字典项目
type DivisionDict struct {
	DivisionDictBase
	// 序号 主键，自增 例如:324
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 行政区划代码
	ItemCode string `sql:"ItemCode"`
	// 行政区划名称
	ItemName string `sql:"ItemName"`
	// 行政区划全称
	FullName string `sql:"FullName"`
	// 上级行政区划代码
	ParentDivisionCode *string `sql:"ParentDivisionCode"`
	// 排序 用于字典项目排序，从1开始往后排 例如:1
	ItemSortValue *uint64 `sql:"ItemSortValue"`
	// 输入码 用于快速输入，可以是拼音首字母或缩写 例如:gxy
	InputCode *string `sql:"InputCode"`
	// 有效性 用于描述本记录是否有效，0-无效，1-有效，默认值为1 例如:1
	IsValid *uint64 `sql:"IsValid"`
}

func (s *DivisionDict) CopyTo(target *doctor.DivisionDict) {
	if target == nil {
		return
	}
	target.SerialNo = s.SerialNo
	target.ItemCode = s.ItemCode
	target.ItemName = s.ItemName
	target.FullName = s.FullName
	if s.ParentDivisionCode != nil {
		target.ParentDivisionCode = string(*s.ParentDivisionCode)
	}
	target.ItemSortValue = s.ItemSortValue
	if s.InputCode != nil {
		target.InputCode = string(*s.InputCode)
	}
	target.IsValid = s.IsValid
}

func (s *DivisionDict) CopyFrom(source *doctor.DivisionDict) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.ItemCode = source.ItemCode
	s.ItemName = source.ItemName
	s.FullName = source.FullName
	parentDivisionCode := string(source.ParentDivisionCode)
	s.ParentDivisionCode = &parentDivisionCode
	s.ItemSortValue = source.ItemSortValue
	inputCode := string(source.InputCode)
	s.InputCode = &inputCode
	s.IsValid = source.IsValid
}
