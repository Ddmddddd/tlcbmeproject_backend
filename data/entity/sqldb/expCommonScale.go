package sqldb

import (
	"tlcbme_project/data/model/doctor"
	"encoding/json"
)

type ExpCommonScaleBase struct {
}

func (s ExpCommonScaleBase) TableName() string {
	return "ExpCommonScale"
}

type ExpCommonScale struct {
	ExpCommonScaleBase
	// 序号
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 题目
	Title string `sql:"Title"`
	// 量表内容，json格式
	Content string `sql:"Content"`
	// 量表描述
	Description *string `sql:"Description"`
	// 量表属性，0-默认量表，仅对患者开放，1-普通量表，2-需要引擎生成生活计划的量表
	ScaleType uint64 `sql:"ScaleType"`
}

func (s *ExpCommonScale) CopyTo(target *doctor.ExpCommonScale) {
	if target == nil {
		return
	}
	target.SerialNo = s.SerialNo
	target.Title = s.Title
	content := s.Content
	if len(content) > 0 {
		json.Unmarshal([]byte(content), &target.Content)
	}

	if s.Description != nil {
		target.Description = string(*s.Description)
	}
	target.ScaleType = s.ScaleType
}

func (s *ExpCommonScale) CopyFrom(source *doctor.ExpCommonScale) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.Title = source.Title
	if source.Content != nil {
		contentData, err := json.Marshal(source.Content)
		if err == nil {
			content := string(contentData)
			s.Content = content
		}
	}

	description := string(source.Description)
	s.Description = &description
	s.ScaleType = source.ScaleType
}
