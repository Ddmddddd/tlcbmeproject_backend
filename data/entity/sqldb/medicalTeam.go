package sqldb

import (
	"tlcbme_project/data/model/doctor"
	"github.com/ktpswjz/httpserver/types"
	"time"
)

type MedicalTeamBase struct {
}

func (s MedicalTeamBase) TableName() string {
	return "MedicalTeam"
}

// 平台运维
// 平台用户管理
// 医疗团队
// 注释：该表保存医疗团队信息
type MedicalTeam struct {
	MedicalTeamBase
	// 序号 主键，自增 例如:324
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 团队代码 例如:1
	TeamCode uint64 `sql:"TeamCode"`
	// 团队名称 例如:维科专家团队
	TeamName string `sql:"TeamName"`
	// 团队描述 例如:维科专家团队创建于1996年......
	Description *string `sql:"Description"`
	// 所属级别 0-机构内，9-整个平台内 例如:0
	Level uint64 `sql:"Level"`
	// 所属机构代码 当Level为0时该字段有效，表示该医疗团队属于某个机构；当Level为1时该字段为null，表示该医疗团队是跨机构的。 例如:
	OrgCode *string `sql:"OrgCode"`
	// 有效性 用于描述本记录是否有效，0-无效，1-有效，默认值为1 例如:1
	IsValid uint64 `sql:"IsValid"`
	// 创建者ID 例如:123
	CreatorID *uint64 `sql:"CreatorID"`
	// 创建者姓名 例如:张三
	CreatorName *string `sql:"CreatorName"`
	// 创建时间 例如:2018-07-05 13:00:00
	CreateDateTime *time.Time `sql:"CreateDateTime"`
	// 最后修改者ID 例如:234
	LastModifyID *uint64 `sql:"LastModifyID"`
	// 最后修改者姓名 例如:李四
	LastModifyName *string `sql:"LastModifyName"`
	// 最后修改时间 例如:2018-07-05 14:00:00
	LastModifyDateTime *time.Time `sql:"LastModifyDateTime"`
}

func (s *MedicalTeam) CopyTo(target *doctor.MedicalTeam) {
	if target == nil {
		return
	}
	target.SerialNo = s.SerialNo
	target.TeamCode = s.TeamCode
	target.TeamName = s.TeamName
	if s.Description != nil {
		target.Description = string(*s.Description)
	}
	target.Level = s.Level
	if s.OrgCode != nil {
		target.OrgCode = string(*s.OrgCode)
	}
	target.IsValid = s.IsValid
	target.CreatorID = s.CreatorID
	if s.CreatorName != nil {
		target.CreatorName = string(*s.CreatorName)
	}
	if s.CreateDateTime == nil {
		target.CreateDateTime = nil
	} else {
		createDateTime := types.Time(*s.CreateDateTime)
		target.CreateDateTime = &createDateTime
	}
	target.LastModifyID = s.LastModifyID
	if s.LastModifyName != nil {
		target.LastModifyName = string(*s.LastModifyName)
	}
	if s.LastModifyDateTime == nil {
		target.LastModifyDateTime = nil
	} else {
		lastModifyDateTime := types.Time(*s.LastModifyDateTime)
		target.LastModifyDateTime = &lastModifyDateTime
	}
}

func (s *MedicalTeam) CopyFrom(source *doctor.MedicalTeam) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.TeamCode = source.TeamCode
	s.TeamName = source.TeamName
	description := string(source.Description)
	s.Description = &description
	s.Level = source.Level
	orgCode := string(source.OrgCode)
	s.OrgCode = &orgCode
	s.IsValid = source.IsValid
	s.CreatorID = source.CreatorID
	creatorName := string(source.CreatorName)
	s.CreatorName = &creatorName
	if source.CreateDateTime == nil {
		s.CreateDateTime = nil
	} else {
		createDateTime := time.Time(*source.CreateDateTime)
		s.CreateDateTime = &createDateTime
	}
	s.LastModifyID = source.LastModifyID
	lastModifyName := string(source.LastModifyName)
	s.LastModifyName = &lastModifyName
	if source.LastModifyDateTime == nil {
		s.LastModifyDateTime = nil
	} else {
		lastModifyDateTime := time.Time(*source.LastModifyDateTime)
		s.LastModifyDateTime = &lastModifyDateTime
	}
}
