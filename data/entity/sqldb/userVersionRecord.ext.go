package sqldb

type UserVersionRecordUserFilter struct {
	UserVersionRecordBase

	// 用户ID 关联患者用户登录表用户ID 例如:232442
	UserID uint64 `sql:"UserID"`
}
