package sqldb

import (
	"tlcbme_project/data/model/doctor"
	"encoding/json"
	"time"
)

func (s *FollowupRecord) CopyToEx(target *doctor.FollowupRecordEx) {
	if target == nil {
		return
	}
	s.CopyTo(&target.FollowupRecord)
}

func (s *FollowupRecord) CopyFromSave(source *doctor.FollowupRecordSave) {
	if source == nil {
		return
	}
	if source.SerialNo != nil {
		s.SerialNo = *source.SerialNo
	} else {
		s.SerialNo = 0
	}
	s.PatientID = source.PatientID

	followupType := string(source.FollowupType)
	s.FollowupType = &followupType
	s.FollowupMethod = source.FollowupMethod
	s.Status = source.Status
	failureReason := string(source.FailureReason)
	s.FailureReason = &failureReason
	if source.DeathTime == nil {
		s.DeathTime = nil
	} else {
		deathTime := time.Time(*source.DeathTime)
		s.DeathTime = &deathTime
	}
	causeOfDeath := string(source.CauseOfDeath)
	s.CauseOfDeath = &causeOfDeath
	s.FollowupTemplateCode = source.FollowupTemplateCode
	s.FollowupTemplateVersion = source.FollowupTemplateVersion
	summary := string(source.Summary)
	s.Summary = &summary
	if source.Content != nil {
		contentData, err := json.Marshal(source.Content)
		if err == nil {
			content := string(contentData)
			s.Content = &content
		}
	}

	s.FollowupPlanSerialNo = source.FollowupPlanSerialNo
}

func (s *FollowupRecord) CopyFromSaveEx(source *doctor.FollowupRecordSaveEx) {
	if source == nil {
		return
	}
	s.CopyFromSave(&source.FollowupRecordSave)

	if source.FollowupDateTime == nil {
		s.FollowupDateTime = nil
	} else {
		followupDateTime := time.Time(*source.FollowupDateTime)
		s.FollowupDateTime = &followupDateTime
	}

	s.DoctorID = source.DoctorID
	s.DoctorName = source.DoctorName
}

type FollowupRecordOrder struct {
	FollowupRecordBase

	// 随访时间  例如:2018-08-01 10:00:00
	FollowupDateTime *time.Time `sql:"FollowupDateTime" order:"DESC"`
}

type FollowupRecordTrendOrder struct {
	FollowupRecordBase

	// 随访时间  例如:2018-08-01 10:00:00
	FollowupDateTime *time.Time `sql:"FollowupDateTime"`
}

type FollowupRecordFilter struct {
	FollowupRecordBase

	// 序号 主键，自增 例如:324
	SerialNo *uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 用户ID 关联患者用户登录表用户ID 例如:232442
	PatientID *uint64 `sql:"PatientID"`

	// 结果状态 用于描述本次随访是否有效，0-失访，1-进行中，2-有效，默认值为1 例如:1
	Statuses []uint64 `sql:"Status" filter:"IN"`
	// 随访人ID  例如:111
	DoctorID *uint64 `sql:"DoctorID"`
	// 随访计划序号 本次随访记录关联的随访计划的序号 例如:4354
	FollowupPlanSerialNo *uint64 `sql:"FollowupPlanSerialNo"`

	// 随访时间  例如:2018-08-01 10:00:00
	FollowupDateTimeStart *time.Time `sql:"FollowupDateTime" filter:">="`
	FollowupDateTimeEnd   *time.Time `sql:"FollowupDateTime" filter:"<"`
}

func (s *FollowupRecordFilter) CopyFrom(source *doctor.FollowupRecordFilter) {
	if source == nil {
		return
	}

	s.SerialNo = source.SerialNo
	s.PatientID = source.PatientID
	if len(source.Statuses) > 0 {
		s.Statuses = source.Statuses
	}
	s.DoctorID = source.DoctorID
	s.FollowupPlanSerialNo = source.FollowupPlanSerialNo
}
