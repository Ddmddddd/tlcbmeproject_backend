package sqldb

import (
	doctor2 "tlcbme_project/data/model/doctor"
)

type ExpKnowledgeLinkBase struct {
}

func (s ExpKnowledgeLinkBase) TableName() string {
	return "ExpKnowledgeLink"
}

type ExpKnowledgeLink struct {
	ExpKnowledgeLinkBase
	//
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 知识标题
	KnowledgeTitle string `sql:"KnowledgeTitle"`
	// 知识链接
	Url string `sql:"Url"`
	// 知识类型
	KnowledgeType string `sql:"KnowledgeType"`
	// 文章来源
	From string `sql:"From"`
	// 知识封面图片
	KnowledgeImage *string `sql:"KnowledgeImage"`
}

func (s *ExpKnowledgeLink) CopyTo(target *doctor2.ExpKnowledgeLink) {
	if target == nil {
		return
	}
	target.SerialNo = s.SerialNo
	target.KnowledgeTitle = s.KnowledgeTitle
	target.Url = s.Url
	target.KnowledgeType = s.KnowledgeType
	target.From = s.From
	if s.KnowledgeImage != nil {
		target.KnowledgeImage = string(*s.KnowledgeImage)
	}
}

func (s *ExpKnowledgeLink) CopyFrom(source *doctor2.ExpKnowledgeLink) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.KnowledgeTitle = source.KnowledgeTitle
	s.Url = source.Url
	s.KnowledgeType = source.KnowledgeType
	s.From = source.From
	knowledgeImage := &source.KnowledgeImage
	s.KnowledgeImage = knowledgeImage
}
