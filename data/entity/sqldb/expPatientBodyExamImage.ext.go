package sqldb

type ExpPatientBodyExamImageFilter struct {
	ExpPatientBodyExamImageBase
	//
	PatientID uint64 `sql:"PatientID"`
}