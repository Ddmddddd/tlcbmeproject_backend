package sqldb

import (
	"github.com/ktpswjz/httpserver/types"
	"time"
	"tlcbme_project/data/model/doctor"
)

type ExpIntegralBase struct {
}

func (s ExpIntegralBase) TableName() string {
	return "ExpIntegral"
}

type ExpIntegral struct {
	ExpIntegralBase
	//
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	//
	PatientID uint64 `sql:"PatientID"`
	//
	Integral uint64 `sql:"Integral"`
	//
	Reason string `sql:"Reason"`
	//
	CreateDateTime *time.Time `sql:"CreateDateTime"`
	//
	ReadFlag uint64 `sql:"ReadFlag"`
}

func (s *ExpIntegral) CopyTo(target *doctor.ExpIntegral) {
	if target == nil {
		return
	}
	target.SerialNo = s.SerialNo
	target.PatientID = s.PatientID
	target.Integral = s.Integral
	target.Reason = s.Reason
	if s.CreateDateTime == nil {
		target.CreateDateTime = nil
	} else {
		createDateTime := types.Time(*s.CreateDateTime)
		target.CreateDateTime = &createDateTime
	}
	target.ReadFlag = s.ReadFlag
}

func (s *ExpIntegral) CopyFrom(source *doctor.ExpIntegral) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.PatientID = source.PatientID
	s.Integral = source.Integral
	s.Reason = source.Reason
	if source.CreateDateTime == nil {
		s.CreateDateTime = nil
	} else {
		createDateTime := time.Time(*source.CreateDateTime)
		s.CreateDateTime = &createDateTime
	}
	s.ReadFlag = source.ReadFlag
}
