package sqldb

import (
	"tlcbme_project/data/model/doctor"
	"github.com/ktpswjz/httpserver/types"
	"time"
)

type AppVersionHistoryCodeFilter struct {
	AppVersionHistoryBase

	// 版本代码 例如:1
	VersionCode uint64 `sql:"VersionCode"`
}

type AppVersionRecord struct {
	AppVersionHistoryBase

	// 版本代码 例如:1
	VersionCode uint64 `sql:"VersionCode"`
	// 版本名称 例如:1.0.0.1
	VersionName string `sql:"VersionName"`
	// 更新时间  例如:2018-07-06 14:55:00
	UpdateDate *time.Time `sql:"UpdateDate"`
	// 更新内容 例如:修改Bug
	UpdateContent string `sql:"UpdateContent"`
	// 是否强制更新 0-否，1-强制更新 例如:0
	IsForced uint64 `sql:"IsForced"`
	// 最小兼容版本代码 例如:1
	CompMinVersion uint64 `sql:"CompMinVersion"`
}

func (s *AppVersionRecord) CopyTo(target *doctor.AppVersionRecord) {
	if target == nil {
		return
	}
	target.VersionCode = s.VersionCode
	target.VersionName = s.VersionName
	target.UpdateContent = s.UpdateContent
	updateDate := types.Time(*s.UpdateDate)
	target.UpdateDate = &updateDate
	target.IsForced = s.IsForced
	target.CompMinVersion = s.CompMinVersion
}
