package sqldb

type TlcTeamUserFilterByUserID struct {
	TlcTeamUserBase
	UserID uint64 `sql:"UserID"`
}

type TlcTeamUserFilterByTeamSerialNo struct {
	TlcTeamUserBase
	TeamSerialNo uint64 `sql:"TeamSerialNo"`
}

type TlcTeamUserFilter struct {
	TlcTeamUserBase
	UserID       uint64 `sql:"UserID"`
	TeamSerialNo uint64 `sql:"TeamSerialNo"`
}
