package sqldb

import (
	"tlcbme_project/data/model/doctor"
	"github.com/ktpswjz/httpserver/types"
	"time"
)

type VerifiedDoctorUserBase struct {
}

func (s VerifiedDoctorUserBase) TableName() string {
	return "VerifiedDoctorUser"
}

// 平台运维
// 平台用户管理
// 认证医生用户表
// 注释：该表保存已通过认证的医生用户信息。一般来说，医生用户可以自主注册成慢病管理工作平台的用户，但需要通过本平台的认证才能正式开展慢病管理工作。
type VerifiedDoctorUser struct {
	VerifiedDoctorUserBase
	// 序号 主键，自增 例如:324
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 用户ID 关联医生用户登录表用户ID 例如:232442
	UserID uint64 `sql:"UserID"`
	// 执业机构代码 该医生用户所在的执业机构 例如:897798
	OrgCode *string `sql:"OrgCode"`
	// 用户类别 0-医生，1-健康管理师，11-医生兼健康管理师 例如:0
	UserType *uint64 `sql:"UserType"`
	// 执业证书照片 例如:
	Certificate []byte `sql:"Certificate"`
	// 身份证正面照片 例如:
	IdentityCardFront []byte `sql:"IdentityCardFront"`
	// 身份证背面照片 例如:
	IdentityCardBack []byte `sql:"IdentityCardBack"`
	// 是否验证通过 0-未认证，1-已认证，2-认证不通过 例如:0
	VerifiedStatus *uint64 `sql:"VerifiedStatus"`
	// 审核人ID 审核人对应的用户ID 例如:32423
	VerifierID *uint64 `sql:"verifierID"`
	// 审核人姓名 例如:张三
	VerifierName *string `sql:"verifierName"`
	// 审核时间 例如:2018-07-03 13:00:00
	VerifiedDateTime *time.Time `sql:"verifiedDateTime"`
	// 审核不通过原因 最近一次审核不通过的原因。只有当状态为2时有效。 例如:信息不够完善
	RefuseReason *string `sql:"RefuseReason"`
}

func (s *VerifiedDoctorUser) CopyTo(target *doctor.VerifiedDoctorUser) {
	if target == nil {
		return
	}
	target.SerialNo = s.SerialNo
	target.UserID = s.UserID
	if s.OrgCode != nil {
		target.OrgCode = string(*s.OrgCode)
	}
	target.UserType = s.UserType
	target.Certificate = string(s.Certificate)
	target.IdentityCardFront = string(s.IdentityCardFront)
	target.IdentityCardBack = string(s.IdentityCardBack)
	target.VerifiedStatus = s.VerifiedStatus
	target.VerifierID = s.VerifierID
	if s.VerifierName != nil {
		target.VerifierName = string(*s.VerifierName)
	}
	if s.VerifiedDateTime == nil {
		target.VerifiedDateTime = nil
	} else {
		verifiedDateTime := types.Time(*s.VerifiedDateTime)
		target.VerifiedDateTime = &verifiedDateTime
	}
	if s.RefuseReason != nil {
		target.RefuseReason = string(*s.RefuseReason)
	}
}

func (s *VerifiedDoctorUser) CopyFrom(source *doctor.VerifiedDoctorUser) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.UserID = source.UserID
	orgCode := string(source.OrgCode)
	s.OrgCode = &orgCode
	s.UserType = source.UserType
	s.Certificate = []byte(source.Certificate)
	s.IdentityCardFront = []byte(source.IdentityCardFront)
	s.IdentityCardBack = []byte(source.IdentityCardBack)
	s.VerifiedStatus = source.VerifiedStatus
	s.VerifierID = source.VerifierID
	verifierName := string(source.VerifierName)
	s.VerifierName = &verifierName
	if source.VerifiedDateTime == nil {
		s.VerifiedDateTime = nil
	} else {
		verifiedDateTime := time.Time(*source.VerifiedDateTime)
		s.VerifiedDateTime = &verifiedDateTime
	}
	refuseReason := string(source.RefuseReason)
	s.RefuseReason = &refuseReason
}
