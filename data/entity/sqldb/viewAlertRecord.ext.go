package sqldb

import (
	"tlcbme_project/data/model/doctor"
	"time"
)

type ViewAlertRecordFilter struct {
	ViewAlertRecordBase

	// 序号 主键，自增 例如:324
	SerialNo *uint64 `sql:"SerialNo" auto:"true"`
	// 用户ID 关联患者用户登录表用户ID 例如:232442
	PatientID *uint64 `sql:"PatientID"`
	// 预警代码 每类预警的唯一代码 例如:001
	AlertCode string `sql:"AlertCode"`
	// 预警名称 例如:单次血压异常偏高
	AlertName string `sql:"AlertName"`

	// 预警发生时间  例如:2018-07-06 14:55:00
	AlertStartDate *time.Time `sql:"AlertDateTime" filter:">="`
	// 预警发生时间  例如:2018-07-06 14:55:00
	AlertEndDate *time.Time `sql:"AlertDateTime" filter:"<"`

	// 预警类型 例如:血压
	AlertTypes []string `sql:"AlertType" filter:"IN"`
	// 处理状态 0-未处理 例如:0
	Statuses []uint64 `sql:"Status" filter:"IN"`
	// 处理方式 0-随访，1-忽略 例如:0
	ProcessModes []uint64 `sql:"ProcessMode" filter:"IN"`

	// 管理机构代码 管理该患者的机构 例如:897798
	OrgCode *string `sql:"OrgCode"`
	// 医生ID 例如:232
	DoctorID *uint64 `sql:"DoctorID"`
	// 健康管理师ID 例如:232
	HealthManagerID *uint64 `sql:"HealthManagerID"`
}

func (s *ViewAlertRecordFilter) CopyFrom(source *doctor.AlertRecordFilterEx) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.PatientID = source.PatientID
	s.AlertCode = source.AlertCode
	s.AlertName = source.AlertName

	if source.AlertStartDate != nil {
		s.AlertStartDate = source.AlertStartDate.ToDate(0)
	}
	if source.AlertEndDate != nil {
		s.AlertEndDate = source.AlertEndDate.ToDate(1)
	}

	if len(source.AlertTypes) > 0 {
		s.AlertTypes = source.AlertTypes
	}
	if len(source.Statuses) > 0 {
		s.Statuses = source.Statuses
	}
	if len(source.ProcessModes) > 0 {
		s.ProcessModes = source.ProcessModes
	}

	s.OrgCode = &source.OrgCode
	s.DoctorID = source.DoctorID
	s.HealthManagerID = source.HealthManagerID
}

type ViewAlertRecordCount struct {
	ViewAlertRecordBase
	// 用户ID 关联患者用户登录表用户ID 例如:232442
	PatientID uint64 `sql:"PatientID"`
}

type ViewAlertRecordStatFilter struct {
	ViewAlertRecordBase
	// 预警发生时间  例如:2018-07-06 14:55:00
	AlertStartDate *time.Time `sql:"AlertDateTime" filter:">="`
	// 预警发生时间  例如:2018-07-06 14:55:00
	AlertEndDate *time.Time `sql:"AlertDateTime" filter:"<"`
	// 处理状态 0-未处理 例如:0
	Statuses []uint64 `sql:"Status" filter:"IN"`
	// 管理机构代码 管理该患者的机构 例如:897798
	OrgCode *string `sql:"OrgCode"`
}
