package sqldb

import "time"

type ExpKnowledgeToPatientFilter struct {
	ExpKnowledgeToPatientBase
	// 患者id
	PatientID uint64 `sql:"PatientID"`
	// 对应计划的日期
	DayIndex uint64 `sql:"DayIndex"`
	// 创建时间
	CreateDateTime *time.Time `sql:"CreateDateTime" filter:">="`
}

type ExpKnowledgeToPatientIDFilter struct {
	ExpKnowledgeToPatientBase
	// 患者id
	PatientID uint64 `sql:"PatientID"`
}

type ExpKnowledgeToPatientStatusEdit struct {
	ExpKnowledgeToPatientBase
	//
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 记录完成情况，0-未完成，1-已阅读、未答题，2-已完成
	Status uint64 `sql:"Status"`
}

type ExpKnowledgeToPatientIDsFilter struct {
	ExpKnowledgeToPatientBase
	// 患者id
	PatientID uint64 `sql:"PatientID"`
	// 知识id
	KnowledgeID uint64 `sql:"KnowledgeID"`
}