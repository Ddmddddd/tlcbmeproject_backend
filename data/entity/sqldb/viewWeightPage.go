package sqldb

import (
	"github.com/ktpswjz/httpserver/types"
	"time"
	"tlcbme_project/data/model/doctor"
)

type ViewWeightPageBase struct {
}

func (s ViewWeightPageBase) TableName() string {
	return "ViewWeightPage"
}

// VIEW
type ViewWeightPage struct {
	ViewWeightPageBase
	// 序号 主键，自增 例如:324
	SerialNo uint64 `sql:"SerialNo"`
	// 用户ID 关联患者用户登录表用户ID 例如:232442
	PatientID uint64 `sql:"PatientID"`
	// 体重 单位：kg 例如:60.5
	Weight float64 `sql:"Weight"`
	// 腰围 单位：cm 例如:60.5
	Waistline *float64 `sql:"Waistline"`
	// 备注 用于说明测量时的情况 例如:有氧运动半小时后
	Memo *string `sql:"Memo"`
	// 测量时间 测量体重时的时间 例如:2018-07-03 14:45:00
	MeasureDateTime *time.Time `sql:"MeasureDateTime"`
	//
	Photo *string `sql:"Photo"`
	//
	Water *float64 `sql:"Water"`
	//
	Sleep *float64 `sql:"Sleep"`
	// 血糖值 单位：mmol/L 例如:6.1
	BloodGlucose *float64 `sql:"BloodGlucose"`
	// 测量时间点 空腹、早餐前、早餐后2h、午餐前、午餐后2h、晚餐前、晚餐后2h、睡前、夜间 例如:空腹
	TimePoint *string `sql:"TimePoint"`
	// 收缩压 单位：mmHg 例如:110
	SystolicPressure *uint64 `sql:"SystolicPressure"`
	// 舒张压 单位：mmHg 例如:78
	DiastolicPressure *uint64 `sql:"DiastolicPressure"`
	BloodGlucoseNo    *uint64 `sql:"BloodGlucoseNo"`
	BloodPressureNo   *uint64 `sql:"BloodPressureNo"`
}

func (s *ViewWeightPage) CopyTo(target *doctor.ViewWeightPage) {
	if target == nil {
		return
	}
	target.SerialNo = s.SerialNo
	target.PatientID = s.PatientID
	target.Weight = s.Weight
	target.Waistline = s.Waistline
	if s.Memo != nil {
		target.Memo = string(*s.Memo)
	}
	if s.MeasureDateTime == nil {
		target.MeasureDateTime = nil
	} else {
		measureDateTime := types.Time(*s.MeasureDateTime)
		target.MeasureDateTime = &measureDateTime
	}
	if s.Photo != nil {
		target.Photo = string(*s.Photo)
	}
	target.Water = s.Water
	target.Sleep = s.Sleep
	target.BloodGlucose = s.BloodGlucose
	if s.TimePoint != nil {
		target.TimePoint = string(*s.TimePoint)
	}
	target.SystolicPressure = s.SystolicPressure
	target.DiastolicPressure = s.DiastolicPressure
	target.BloodGlucoseNo = s.BloodGlucoseNo
	target.BloodPressureNo = s.BloodPressureNo
}

func (s *ViewWeightPage) CopyFrom(source *doctor.ViewWeightPage) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.PatientID = source.PatientID
	s.Weight = source.Weight
	s.Waistline = source.Waistline
	memo := string(source.Memo)
	s.Memo = &memo
	if source.MeasureDateTime == nil {
		s.MeasureDateTime = nil
	} else {
		measureDateTime := time.Time(*source.MeasureDateTime)
		s.MeasureDateTime = &measureDateTime
	}
	photo := string(source.Photo)
	s.Photo = &photo
	s.Water = source.Water
	s.Sleep = source.Sleep
	s.BloodGlucose = source.BloodGlucose
	timePoint := string(source.TimePoint)
	s.TimePoint = &timePoint
	s.SystolicPressure = source.SystolicPressure
	s.DiastolicPressure = source.DiastolicPressure
	s.BloodGlucoseNo = source.BloodGlucoseNo
	s.BloodPressureNo = source.BloodPressureNo
}
