package sqldb

type ExpExercisePlanPatientFilter struct {
	ExpExercisePlanBase
	// 患者id
	PatientID uint64 `sql:"PatientID"`
}