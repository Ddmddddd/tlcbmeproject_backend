package sqldb

import (
	"encoding/json"
	"github.com/ktpswjz/httpserver/types"
	"strings"
	"time"
	"tlcbme_project/data/model/doctor"
)

func (s *ManagedPatientIndex) CopyToExt(target *doctor.ManagedPatientIndexExt) bool {
	if target == nil {
		return false
	}

	if s.Ext == nil {
		return false
	}

	ext := *s.Ext
	if len(ext) <= 0 {
		return false
	}

	err := json.Unmarshal([]byte(ext), target)
	if err != nil {
		return false
	}

	return true
}

type ManagedPatientIndexFilterBase struct {
	ManagedPatientIndexBase

	// 序号 主键，自增 例如:324
	SerialNo *uint64 `sql:"SerialNo" auto:"true"`
	// 用户ID 关联患者用户登录表用户ID 例如:232442
	PatientID *uint64 `sql:"PatientID"`
}
type ManagedPatientIndexFilterExtBase struct {
	ManagedPatientIndexBase
	PatientID *uint64 `sql:"PatientID"`
}

type ManagedPatientIndexFilter struct {
	PatientID []uint64 `sql:"PatientID" filter:"not in"`
}

type ManagedPatientIndexDoctorIDFilter struct {
	// 医生ID 例如:232
	DoctorID *uint64 `sql:"DoctorID"`
	// 健康管理师ID 例如:232
	HealthManagerID *uint64 `sql:"HealthManagerID"`
}

type ManagedPatientIndexPatientIDFilter struct {
	ManagedPatientIndexBase
	PatientID uint64 `sql:"PatientID"`
}

type ManagedPatientIndexOrgCodeFilter struct {
	ManagedPatientIndexBase
	OrgCode *string `sql:"OrgCode"`
	// 管理开始时间 最近一次管理的开始时间，如果是刚注册，则默认为审核通过时间 例如:2018-07-03 13:15:00
	ManageStartDateTime *time.Time `sql:"ManageStartDateTime" filter:"<"`
}

type ManagedPatientIndexActiveFilter struct {
	ManagedPatientIndexBase
	OrgCode             *string `sql:"OrgCode"`
	PatientActiveDegree uint64  `sql:"PatientActiveDegree"`
	// 管理开始时间 最近一次管理的开始时间，如果是刚注册，则默认为审核通过时间 例如:2018-07-03 13:15:00
	ManageStartDateTime *time.Time `sql:"ManageStartDateTime" filter:"<"`
}

func (s *ManagedPatientIndexFilterBase) CopyFrom(source *doctor.ManagedPatientIndexFilterBase) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.PatientID = source.PatientID
}

type ManagedPatientIndexFollowupUpdate struct {
	ManagedPatientIndexBase

	// 用户ID 关联患者用户登录表用户ID 例如:232442
	PatientID uint64 `sql:"PatientID" primary:"true"`
	// 上次随访日期 例如:2018-06-15
	LastFollowupDate *time.Time `sql:"LastFollowupDate"`
	// 累计随访次数 本次管理的累计随访次数 例如:3
	FollowupTimes uint64 `sql:"FollowupTimes"`

	// 管理状态 0-管理中，1-迁出，2-已终止管理 例如:0
	ManageStatus *uint64 `sql:"ManageStatus"`
	// 管理终止时间 该字段只有当ManageStatus字段为2时才有意义 例如:2018-08-03 13:15:00
	ManageEndDateTime *time.Time `sql:"ManageEndDateTime"`
	// 管理终止的原因 该字段只有当ManageStatus字段为2时才有意义 例如:亡故（2018-08-03，车祸死亡）
	ManageEndReason *string `sql:"ManageEndReason"`
}

type ManagedPatientIndexExtUpdate struct {
	ManagedPatientIndexBase

	// 序号 主键，自增 例如:324
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`

	// 扩展信息 保存不同管理分类的扩展信息 例如:{htn:{}, dm:{}}
	Ext *string `sql:"Ext"`
}

func (s *ManagedPatientIndexExtUpdate) CopyFrom(source *doctor.ManagedPatientIndexExt) {
	if source == nil {
		return
	}

	extData, err := json.Marshal(source)
	if err == nil {
		ext := string(extData)
		s.Ext = &ext
	}
}

type ManagedPatientIndexTerminate struct {
	ManagedPatientIndexBase

	// 序号 主键，自增 例如:324
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 管理状态 0-管理中，1-迁出，2-已终止管理 例如:0
	ManageStatus uint64 `sql:"ManageStatus"`
	// 管理终止时间 该字段只有当ManageStatus字段为2时才有意义 例如:2018-08-03 13:15:00
	ManageEndDateTime *time.Time `sql:"ManageEndDateTime"`
	// 管理终止的原因 该字段只有当ManageStatus字段为2时才有意义 例如:亡故（2018-08-03，车祸死亡）
	ManageEndReason *string `sql:"ManageEndReason"`
}

type ManagedPatientIndexRecover struct {
	ManagedPatientIndexBase
	// 序号 主键，自增 例如:324
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 管理状态 0-管理中，1-迁出，2-已终止管理 例如:0
	ManageStatus uint64 `sql:"ManageStatus"`
	// 管理开始时间 最近一次管理的开始时间，如果是刚注册，则默认为审核通过时间 例如:2018-07-03 13:15:00
	ManageStartDateTime *time.Time `sql:"ManageStartDateTime"`
}

type ManagedPatientIndexStatus struct {
	ManagedPatientIndexBase

	// 管理状态 0-管理中，1-迁出，2-已终止管理 例如:0
	ManageStatus uint64 `sql:"ManageStatus"`
	// 管理终止时间 该字段只有当ManageStatus字段为2时才有意义 例如:2018-08-03 13:15:00
	ManageEndDateTime *time.Time `sql:"ManageEndDateTime"`
	// 管理终止的原因 该字段只有当ManageStatus字段为2时才有意义 例如:亡故（2018-08-03，车祸死亡）
	ManageEndReason *string `sql:"ManageEndReason"`
}

func (s *ManagedPatientIndexStatus) CopyTo(target *doctor.ManagedPatientIndexStatus) {
	if target == nil {
		return
	}
	target.ManageStatus = s.ManageStatus
	if s.ManageEndDateTime == nil {
		target.ManageEndDateTime = nil
	} else {
		manageEndDateTime := types.Time(*s.ManageEndDateTime)
		target.ManageEndDateTime = &manageEndDateTime
	}
	if s.ManageEndReason != nil {
		target.ManageEndReason = string(*s.ManageEndReason)
	}
}

type ManagedPatientIndexComplianceRateAndPatientActiveDegreeUpdate struct {
	ManagedPatientIndexBase
	// 序号 主键，自增 例如:324
	SerialNo uint64 `sql:"SerialNo" auto:"true" primary:"true"`

	// 依从度 值为0至5，0表示依从度未知 例如:4
	ComplianceRate uint64 `sql:"ComplianceRate"`
	// 活跃度 0-不活跃，1-活跃 例如:0
	PatientActiveDegree uint64 `sql:"PatientActiveDegree"`
}

type ManagedPatientIndexExt struct {
	ManagedPatientIndexBase

	// 用户ID 关联患者用户登录表用户ID 例如:232442
	PatientID uint64 `sql:"PatientID" primary:"true"`
	// 扩展信息 保存不同管理分类的扩展信息 例如:{htn:{}, dm:{}}
	Ext *string `sql:"Ext"`
}

func (s *ManagedPatientIndexExt) CopyTo(target *doctor.ManagedPatientIndexExt) {
	if target == nil {
		return
	}

	ext := ""
	if s.Ext != nil {
		ext = *s.Ext
	}
	if len(ext) > 0 {
		json.Unmarshal([]byte(ext), target)
	}

}

func (s *ManagedPatientIndexExt) CopyFrom(source *doctor.ManagedPatientIndexExt) {
	if source == nil {
		return
	}

	extData, err := json.Marshal(source)
	if err == nil {
		ext := string(extData)
		s.Ext = &ext
	}

}

type ModifyPatientIndex struct {
	ManagedPatientIndexBase
	// 患者特点 如有多个，之间用逗号分隔，例如：老年人，肥胖，残疾人 例如:老年人，肥胖
	PatientFeature *string `sql:"PatientFeature"`
	// 医生ID 例如:232
	DoctorID *uint64 `sql:"DoctorID"`
	// 医生姓名 例如:张三
	DoctorName *string `sql:"DoctorName"`
	// 健康管理师ID 例如:232
	HealthManagerID *uint64 `sql:"HealthManagerID"`
	// 健康管理师姓名 例如:李四
	HealthManagerName *string `sql:"HealthManagerName"`
	// 管理分类 如有多个，之间用逗号分隔，例如：高血压，糖尿病 例如:高血压，糖尿病
	ManageClass *string `sql:"ManageClass"`
	// 扩展信息 保存不同管理分类的扩展信息 例如:{htn:{}, dm:{}}
	Ext *string `sql:"Ext"`
	// 管理开始时间 最近一次管理的开始时间，如果是刚注册，则默认为审核通过时间 例如:2018-07-03 13:15:00
	ManageStartDateTime *time.Time `sql:"ManageStartDateTime"`
}

func (s *ModifyPatientIndex) CopyFromExt(source *doctor.PatientUserBaseInfoModify) {
	if source == nil {
		return
	}
	if len(source.PatientFeature) > 0 {
		features := strings.Join(source.PatientFeature, ",")
		s.PatientFeature = &features
	} else {
		str := " "
		s.PatientFeature = &str
	}
	s.DoctorID = source.DoctorID
	doctorName := string(source.DoctorName)
	s.DoctorName = &doctorName
	s.HealthManagerID = source.HealthManagerID
	healthManagerName := string(source.HealthManagerName)
	s.HealthManagerName = &healthManagerName
	if len(source.ManagerClasses) > 0 {
		features := strings.Join(source.ManagerClasses, ",")
		s.ManageClass = &features
	}

}
