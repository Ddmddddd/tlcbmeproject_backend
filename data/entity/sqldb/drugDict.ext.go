package sqldb

import (
	"tlcbme_project/data/model/doctor"
	"fmt"
	"strconv"
)

type DrugFilter struct {
	DrugDictBase
	DictFilter
	// 设备代码 设备代码 例如:200101
	ItemCode uint64 `sql:"itemCode"`
	// 有效性 用于描述本记录是否有效，0-无效，1-有效，默认值为1 例如:1
	IsValid uint64 `sql:"IsValid"`
}

type DrugDictKeywordFilter struct {

	// 字典项名称  例如:高血压
	ItemName string `sql:"ItemName" filter:"like"`
	// 规格  例如:10mg/粒X12
	Specification *string `sql:"Specification" filter:"like"`
	// 单位 该药的常用单位mg、片、粒，如有多个，用逗号分隔 例如:mg
	Units *string `sql:"Units" filter:"like"`
	// 功效 例如降压、降糖、调脂 例如:降压
	Effect *string `sql:"Effect" filter:"like"`
	// 输入码 用于快速输入，可以是拼音首字母或缩写 例如:gxy
	InputCode *string `sql:"InputCode" filter:"like"`
}

type DrugDictUpdate struct {
	DrugDictBase

	// 设备代码 设备代码 例如:200101
	ItemCode uint64 `sql:"itemCode"`
	// 字典项名称  例如:高血压
	ItemName string `sql:"ItemName" `
	// 规格  例如:10mg/粒X12
	Specification *string `sql:"Specification" `
	// 单位 该药的常用单位mg、片、粒，如有多个，用逗号分隔 例如:mg
	Units *string `sql:"Units" `
	// 功效 例如降压、降糖、调脂 例如:降压
	Effect *string `sql:"Effect" `
	// 输入码 用于快速输入，可以是拼音首字母或缩写 例如:gxy
	InputCode *string `sql:"InputCode" `
}

func (s *DrugDict) CopyFromCreate(source *doctor.DrugDictCreate) {
	if source == nil {
		return
	}
	int, _ := strconv.Atoi(source.ItemCode)
	s.ItemCode = uint64(int)
	s.ItemName = source.ItemName
	Specification := string(source.Specification)
	s.Specification = &Specification
	Units := string(source.Units)
	s.Units = &Units
	Effect := string(source.Effect)
	s.Effect = &Effect
	InputCode := string(source.InputCode)
	s.InputCode = &InputCode
	s.IsValid = 1
}

func (s *DrugDictKeywordFilter) CopyFrom(source *doctor.DictKeywordFilter) {
	if source == nil {
		return
	}

	if len(source.Keyword) > 0 {
		Specification := fmt.Sprint("%", source.Keyword, "%")
		s.Specification = &Specification
		Units := fmt.Sprint("%", source.Keyword, "%")
		s.Units = &Units
		Effect := fmt.Sprint("%", source.Keyword, "%")
		s.Effect = &Effect
		InputCode := fmt.Sprint("%", source.Keyword, "%")
		s.InputCode = &InputCode
		s.ItemName = fmt.Sprint("%", source.Keyword, "%")
	}
}

func (s *DrugDictUpdate) CopyFrom(source *doctor.DrugDictUpdate) {
	if source == nil {
		return
	}
	InputCode := string(source.InputCode)
	s.InputCode = &InputCode
	Effect := string(source.Effect)
	s.Effect = &Effect
	Specification := string(source.Specification)
	s.Specification = &Specification
	ItemCode, _ := strconv.Atoi(source.ItemCode)
	s.ItemCode = uint64(ItemCode)
	s.ItemName = source.ItemName
	Units := string(source.Units)
	s.Units = &Units
}
