package sqldb

type ExpFoodGIOrder struct {
	ExpFoodGIBase
	// 序号
	SerialNo uint64 `sql:"SerialNo" order:"RAND()"`
}

type ExpFoodGISerialFilter struct {
	ExpFoodGIBase
	// 序号
	SerialNo uint64 `sql:"SerialNo" order:"RAND()"`
}
