package sqldb

import "tlcbme_project/data/model/doctor"

type DoctorAddedRightFilter struct {
	DoctorAddedRightBase

	// 用户ID  例如:
	UserID uint64 `sql:"UserID" `
}

func (s *DoctorAddedRightFilter) CopyFrom(target *doctor.DoctorAddedRightFilter) {
	if target == nil {
		return
	}

	s.UserID = target.UserID
}
