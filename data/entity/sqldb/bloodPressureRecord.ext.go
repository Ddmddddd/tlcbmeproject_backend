package sqldb

import (
	"tlcbme_project/data/model/doctor"
	"github.com/ktpswjz/httpserver/types"
	"time"
)

type BloodPressureRecordFilter struct {
	BloodPressureRecordBase

	// 序号 主键，自增 例如:324
	SerialNo *uint64 `sql:"SerialNo"`
}

type BloodPressureRecordData struct {
	BloodPressureRecordBase

	// 序号 主键，自增 例如:324
	SerialNo uint64 `sql:"SerialNo"`
	// 用户ID 关联患者用户登录表用户ID 例如:232442
	// 收缩压 单位：mmHg 例如:110
	SystolicPressure uint64 `sql:"SystolicPressure"`
	// 舒张压 单位：mmHg 例如:78
	DiastolicPressure uint64 `sql:"DiastolicPressure"`
	// 心率 单位：次/分钟 例如:65
	HeartRate *uint64 `sql:"HeartRate"`
	// 测量时间 测量血压的时间 例如:2018-07-03 14:45:00
	MeasureDateTime *time.Time `sql:"MeasureDateTime"`
	// 备注 用于说明测量时的情况，例如有无服药，药品名称和剂量 例如:服用硝苯地平控释片2小时后
	Memo *string `sql:"Memo"`
}

func (s *BloodPressureRecordData) CopyTo(target *doctor.BloodPressureRecordData) {
	if target == nil {
		return
	}

	target.SystolicPressure = s.SystolicPressure
	target.DiastolicPressure = s.DiastolicPressure
	target.HeartRate = s.HeartRate
	if s.MeasureDateTime == nil {
		target.MeasureDateTime = nil
	} else {
		measureDateTime := types.Time(*s.MeasureDateTime)
		target.MeasureDateTime = &measureDateTime
	}
	if s.Memo != nil {
		target.Memo = string(*s.Memo)
	}
}

type BloodPressureRecordDataOrder struct {
	BloodPressureRecordBase

	// 测量时间 测量血压的时间 例如:2018-07-03 14:45:00
	MeasureDateTime *time.Time `sql:"MeasureDateTime"`
}

type BloodPressureRecordDataFilter struct {
	BloodPressureRecordBase

	// 用户ID 关联患者用户登录表用户ID 例如:232442
	PatientID uint64 `sql:"PatientID"`

	// 测量时间 测量血压的时间 例如:2018-07-03 14:45:00
	MeasureStartDate *time.Time `sql:"MeasureDateTime" filter:">="`
	MeasureEndDate   *time.Time `sql:"MeasureDateTime" filter:"<"`
}

func (s *BloodPressureRecordDataFilter) CopyFrom(source *doctor.BloodPressureRecordDataFilterEx) {
	if source == nil {
		return
	}

	s.PatientID = source.PatientID
	if source.MeasureStartDate != nil {
		s.MeasureStartDate = source.MeasureStartDate.ToDate(0)
	}
	if source.MeasureEndDate != nil {
		s.MeasureEndDate = source.MeasureEndDate.ToDate(1)
	}
}

func (s *BloodPressureRecord) CopyFromAppEx(source *doctor.BloodPressureRecordForAppEx) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.PatientID = source.PatientID
	s.SystolicPressure = source.SystolicPressure
	s.DiastolicPressure = source.DiastolicPressure
	s.HeartRate = source.HeartRate
	measureBodyPart := uint64(0)
	s.MeasureBodyPart = &measureBodyPart
	timePoint := TypeToTimePoint(source.Type)
	s.TimePoint = &timePoint
	memo := string(source.Memo)
	s.Memo = &memo
	measurePlace := uint64(0)
	s.MeasurePlace = &measurePlace
	if source.MeasureDateTime == nil {
		s.MeasureDateTime = nil
	} else {
		measureDateTime := time.Time(*source.MeasureDateTime)
		s.MeasureDateTime = &measureDateTime
	}
	now := time.Now()
	s.InputDateTime = &now
}

func (s *BloodPressureRecord) CopyToApp(target *doctor.BloodPressureRecordForApp) {
	if target == nil {
		return
	}
	target.SerialNo = s.SerialNo
	target.SystolicPressure = s.SystolicPressure
	target.DiastolicPressure = s.DiastolicPressure
	target.HeartRate = s.HeartRate
	target.Type = TimePointToType(s.TimePoint)
	if s.Memo != nil {
		target.Memo = string(*s.Memo)
	}
	if s.MeasureDateTime == nil {
		target.MeasureDateTime = nil
	} else {
		measureDateTime := types.Time(*s.MeasureDateTime)
		target.MeasureDateTime = &measureDateTime
	}
}

func TimePointToType(timePoint *string) uint64 {
	t := uint64(0)
	if timePoint == nil {
		return t
	}
	str := string(*timePoint)
	switch str {
	case "早":
		t = uint64(1)
	case "中":
		t = uint64(2)
	case "晚":
		t = uint64(3)
	}
	return t
}

func TypeToTimePoint(t uint64) string {
	var timePoint string
	switch t {
	case 1:
		timePoint = string("早")
	case 2:
		timePoint = string("中")
	case 3:
		timePoint = string("晚")
	default:
		timePoint = string("")
	}
	return timePoint
}

type BloodPressureRecordTrend struct {
	BloodPressureRecordBase

	// 收缩压 单位：mmHg 例如:110
	SystolicPressure uint64 `sql:"SystolicPressure"`
	// 舒张压 单位：mmHg 例如:78
	DiastolicPressure uint64 `sql:"DiastolicPressure"`
	// 心率 单位：次/分钟 例如:65
	HeartRate *uint64 `sql:"HeartRate"`
	// 测量时间 测量血压的时间 例如:2018-07-03 14:45:00
	MeasureDateTime *time.Time `sql:"MeasureDateTime"`
}

func (s *BloodPressureRecordTrend) CopyTo(target *doctor.BloodPressureRecordTrend) {
	if target == nil {
		return
	}

	target.SystolicPressure = s.SystolicPressure
	target.DiastolicPressure = s.DiastolicPressure
	target.HeartRate = s.HeartRate

	if s.MeasureDateTime == nil {
		target.MeasureDateTime = nil
	} else {
		measureDateTime := types.Time(*s.MeasureDateTime)
		target.MeasureDateTime = &measureDateTime
	}
}

type BloodPressureRecordUpOrder struct {
	BloodPressureRecordBase

	// 测量时间 测量血压的时间 例如:2018-07-03 14:45:00
	MeasureDateTime *time.Time `sql:"MeasureDateTime"`
}

type BloodPressureRecordTrendFilter struct {
	BloodPressureRecordBase

	// 用户ID 关联患者用户登录表用户ID 例如:232442
	PatientID uint64 `sql:"PatientID"`
	// 测量时间 测量血压的时间 例如:2018-07-03 14:45:00
	MeasureDateTimeStart *time.Time `sql:"MeasureDateTime" filter:">="`
	// 测量时间 测量血压的时间 例如:2018-07-03 14:45:00
	MeasureDateTimeEnd *time.Time `sql:"MeasureDateTime" filter:"<"`
}

type BloodPressureRecordPatientFilter struct {
	BloodPressureRecordBase

	// 用户ID 关联患者用户登录表用户ID 例如:232442
	PatientID uint64 `sql:"PatientID"`
}

type BloodPressureRecordLastOrder struct {
	BloodPressureRecordBase

	// 测量时间 测量血压的时间 例如:2018-07-03 14:45:00
	MeasureDateTime *time.Time `sql:"MeasureDateTime" order:"DESC"`
}

type BloodPressureRecordCreate struct {
	BloodPressureRecordBase

	// 收缩压 单位：mmHg 例如:110
	SystolicPressure uint64 `sql:"SystolicPressure"`
	// 舒张压 单位：mmHg 例如:78
	DiastolicPressure uint64 `sql:"DiastolicPressure"`
	// 心率 单位：次/分钟 例如:65
	HeartRate *uint64 `sql:"HeartRate"`
	// 测量部位 0-未知，1-左上臂，2-左手腕，3-右上臂，4-右手腕 例如:0
	MeasureBodyPart *uint64 `sql:"MeasureBodyPart"`
	// 备注 用于说明测量时的情况，例如有无服药，药品名称和剂量 例如:服用硝苯地平控释片2小时后
	Memo *string `sql:"Memo"`
	// 测量场所 0-未知，1-家里，2-医院，3-药店，9-其他场所 例如:0
	MeasurePlace *uint64 `sql:"MeasurePlace"`
	// 时间点 早、中、晚 例如:早
	TimePoint *string `sql:"TimePoint"`
	// 测量时间 测量血压的时间 例如:2018-07-03 14:45:00
	MeasureDateTime *time.Time `sql:"MeasureDateTime"`
}

func (s *BloodPressureRecordCreate) CopyTo(target *doctor.BloodPressureRecordCreate) {
	if target == nil {
		return
	}

	target.SystolicPressure = s.SystolicPressure
	target.DiastolicPressure = s.DiastolicPressure
	target.HeartRate = s.HeartRate
	if s.Memo == nil {
		target.Memo = ""
	} else {
		target.Memo = *s.Memo
	}
	target.MeasureBodyPart = s.MeasureBodyPart
	target.MeasurePlace = s.MeasurePlace

	if s.MeasureDateTime == nil {
		target.MeasureDateTime = nil
	} else {
		measureDateTime := types.Time(*s.MeasureDateTime)
		target.MeasureDateTime = &measureDateTime
	}
}
