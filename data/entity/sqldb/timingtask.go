package sqldb

import (
	"bytes"
	"tlcbme_project/data/model/doctor"
	"encoding/gob"
	"github.com/ktpswjz/httpserver/types"
	"time"
)

type TimingtaskBase struct {
}

func (s TimingtaskBase) TableName() string {
	return "TimingTask"
}

func (s TimingtaskBase) SetFilter(v interface{}) {

}

func (s *TimingtaskBase) Clone() (interface{}, error) {
	return &TimingtaskBase{}, nil
}

type Timingtask struct {
	TimingtaskBase
	// 序号\n324\nFCM\n主键，自增
	Serialno uint64 `sql:"SerialNo" auto:"true" primary:"true"`
	// 用户ID\n232442\nFCM\n关联患者用户登录表用户ID
	Patientid uint64 `sql:"PatientID"`
	// 定时任务类型：1-高血压短信，2-糖尿病短信
	Tasktype uint64 `sql:"TaskType"`
	// 操作者ID\n322\nFCM\n保存用户ID
	Operatorid uint64 `sql:"OperatorID"`
	DoctorName string `sql:"DoctorName"`
	// 开始定时时间
	Timingstartdatetime *time.Time `sql:"TimingStartDateTime"`
}

func (s *Timingtask) Clone() (interface{}, error) {
	t := &Timingtask{}

	buf := new(bytes.Buffer)
	enc := gob.NewEncoder(buf)
	dec := gob.NewDecoder(buf)
	err := enc.Encode(s)
	if err != nil {
		return nil, err
	}
	err = dec.Decode(t)
	if err != nil {
		return nil, err
	}

	return t, nil
}

func (s *Timingtask) CopyTo(target *doctor.Timingtask) {
	if target == nil {
		return
	}
	target.Serialno = s.Serialno
	target.Patientid = s.Patientid
	target.Tasktype = s.Tasktype
	target.Operatorid = s.Operatorid
	target.DoctorName = s.DoctorName
	if s.Timingstartdatetime == nil {
		target.Timingstartdatetime = nil
	} else {
		timingstartdatetime := types.Time(*s.Timingstartdatetime)
		target.Timingstartdatetime = &timingstartdatetime
	}
}

func (s *Timingtask) CopyFrom(source *doctor.Timingtask) {
	if source == nil {
		return
	}
	s.Serialno = source.Serialno
	s.Patientid = source.Patientid
	s.Tasktype = source.Tasktype
	s.Operatorid = source.Operatorid
	s.DoctorName = source.DoctorName
	if source.Timingstartdatetime == nil {
		s.Timingstartdatetime = nil
	} else {
		timingstartdatetime := time.Time(*source.Timingstartdatetime)
		s.Timingstartdatetime = &timingstartdatetime
	}
}

func (s *Timingtask) CopyFromFollow(source *doctor.FollowupRecordSaveEx) {
	if source == nil {
		return
	}
	s.Patientid = source.PatientID
	s.Operatorid = source.DoctorID
	s.DoctorName = source.DoctorName
	if source.FollowupDateTime == nil {
		s.Timingstartdatetime = nil
	} else {
		timingstartdatetime := time.Time(*source.FollowupDateTime)
		s.Timingstartdatetime = &timingstartdatetime
	}
}

func (s *Timingtask) CopyFromCreate(source *doctor.TimingTaskCreate) {
	if source == nil {
		return
	}
	s.Patientid = source.Patientid
	s.Tasktype = source.Tasktype
	s.Operatorid = source.Operatorid
	s.DoctorName = source.DoctorName
	if source.Timingstartdatetime == nil {
		s.Timingstartdatetime = nil
	} else {
		timingstartdatetime := time.Time(*source.Timingstartdatetime)
		s.Timingstartdatetime = &timingstartdatetime
	}
}