package sqldb

import (
	"tlcbme_project/data/enum"
	"tlcbme_project/data/model"
	"tlcbme_project/data/model/doctor"
	"fmt"
	"strings"
	"time"
)

func (s *ViewFollowupPlan) CopyToEx(target *doctor.ViewFollowupPlanEx) {
	if target == nil {
		return
	}
	s.CopyTo(&target.ViewFollowupPlan)

	if target.FollowupDate != nil {
		target.FollowupDays = target.FollowupDate.GetDays(time.Now())
	}

	if s.Sex != nil {
		target.SexText = enum.Sexes.Value(*s.Sex)
	}
	if s.DateOfBirth != nil {
		age := time.Now().Sub(*s.DateOfBirth)
		target.Age = int64(age.Hours() / (24 * 365))
	}

	tagCount := 0
	tag := &strings.Builder{}
	if s.ManageClass != nil {
		vs := strings.Split(*s.ManageClass, ",")
		for _, v := range vs {
			tv := strings.TrimSpace(v)
			if len(tv) < 1 {
				continue
			}
			tagCount++
			if tagCount > 1 {
				tag.WriteString(" ")
			}
			tag.WriteString(tv)
		}
	}
	if s.PatientFeature != nil {
		vs := strings.Split(*s.PatientFeature, ",")
		for _, v := range vs {
			tv := strings.TrimSpace(v)
			if len(tv) < 1 {
				continue
			}
			tagCount++
			if tagCount > 1 {
				tag.WriteString(" ")
			}
			tag.WriteString(tv)
		}
	}

	target.Tag = tag.String()
}

type ViewFollowupPlanFilter struct {
	ViewFollowupPlanBase

	// 序号 主键，自增 例如:324
	SerialNo *uint64 `sql:"SerialNo"`
	// 用户ID 关联患者用户登录表用户ID 例如:232442
	PatientID *uint64 `sql:"PatientID"`
	// 姓名  例如:张三
	PatientName string `sql:"PatientName"`
	// 性别 0-未知, 1-男, 2-女，9-未说明 例如:1
	Sex *uint64 `sql:"Sex"`
	// 身份证号 例如:330106200012129876
	IdentityCardNumber string `sql:"IdentityCardNumber"`
	// 联系电话 例如:13812344321
	Phone string `sql:"Phone"`

	// 随访类型 随访的类型（原因） 例如:三个月例行随访
	FollowUpType string `sql:"FollowUpType"`
	// 随访状态 0-待随访，1-已随访。 例如:0
	FollowStatus *uint64 `sql:"FollowStatus"`
	// 管理状态 0-管理中，1-迁出，2-已终止管理 例如:0
	ManageStatuses []uint64 `sql:"ManageStatus" filter:"IN"`

	// 管理开始时间 最近一次管理的开始时间，如果是刚注册，则默认为审核通过时间 例如:2018-07-03 13:15:00
	ManageDateStart *time.Time `sql:"ManageStartDateTime" filter:">="`
	ManageDateEnd   *time.Time `sql:"ManageStartDateTime" filter:"<"`
	// 上次随访日期 例如:2018-06-15
	LastFollowupDateStart *time.Time `sql:"LastFollowupDate" filter:">="`
	LastFollowupDateEnd   *time.Time `sql:"LastFollowupDate" filter:"<"`
	// 计划随访日期 例如:2018-07-15
	FollowupDateStart *time.Time `sql:"FollowupDate" filter:">="`
	FollowupDateEnd   *time.Time `sql:"FollowupDate" filter:"<"`

	// 管理机构代码 管理该患者的机构 例如:897798
	OrgCode string `sql:"OrgCode"`
}

func (s *ViewFollowupPlanFilter) CopyFrom(source *doctor.ViewFollowupPlanFilter) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.PatientID = source.PatientID
	if len(source.PatientName) > 0 {
		s.PatientName = fmt.Sprint("%", source.PatientName, "%")
	}
	s.Sex = source.Sex
	s.IdentityCardNumber = source.IdentityCardNumber
	s.Phone = source.Phone

	s.FollowUpType = source.FollowUpType
	s.FollowStatus = source.FollowStatus
	if len(source.ManageStatuses) > 0 {
		s.ManageStatuses = source.ManageStatuses
	}

	if source.ManageDateStart != nil {
		s.ManageDateStart = source.ManageDateStart.ToDate(0)
	}
	if source.ManageDateEnd != nil {
		s.ManageDateEnd = source.ManageDateEnd.ToDate(1)
	}

	if source.LastFollowupDateStart != nil {
		s.LastFollowupDateStart = source.LastFollowupDateStart.ToDate(0)
	}
	if source.LastFollowupDateEnd != nil {
		s.LastFollowupDateEnd = source.LastFollowupDateEnd.ToDate(1)
	}

	if source.FollowupDateStart != nil {
		s.FollowupDateStart = source.FollowupDateStart.ToDate(0)
	}
	if source.FollowupDateEnd != nil {
		s.FollowupDateEnd = source.FollowupDateEnd.ToDate(1)
	}
}

func (s *ViewFollowupPlanFilter) CopyFromEx(source *doctor.ViewFollowupPlanFilterEx) {
	if source == nil {
		return
	}
	s.CopyFrom(&source.ViewFollowupPlanFilter)

	s.OrgCode = source.OrgCode
}

type ViewFollowupPlanManagerFilter struct {
	ViewFollowupPlanBase

	// 医生ID 例如:232
	DoctorID *uint64 `sql:"DoctorID"`
	// 健康管理师ID 例如:232
	HealthManagerID *uint64 `sql:"HealthManagerID"`
}

func (s *ViewFollowupPlanManagerFilter) CopyFromEx(source *doctor.ViewFollowupPlanFilterEx) {
	if source == nil {
		return
	}

	s.DoctorID = source.DoctorID
	s.HealthManagerID = source.HealthManagerID
}

type ViewFollowupPlanOrder struct {
	ViewFollowupPlanBase

	// 计划随访日期 例如:2018-07-15
	FollowupDate *time.Time `sql:"FollowupDate" index:"1"`

	ManageLevel []byte `sql:"ManageLevel" order:"DESC" index:"2"`

	// 序号 主键，自增 例如:324
	SerialNo uint64 `sql:"SerialNo" index:"100"`
}

type ViewFollowupPlanOrderBase struct {
	ViewFollowupPlanBase

	// 序号 主键，自增 例如:324
	SerialNo uint64 `sql:"SerialNo" index:"100"`
}

type ViewFollowupPlanOrderComplianceRateAscending struct {
	ViewFollowupPlanOrderBase

	// 依从度 值为0至5，0表示依从度未知 例如:4
	ComplianceRate *uint64 `sql:"ComplianceRate"`
}

type ViewFollowupPlanOrderComplianceRateDescending struct {
	ViewFollowupPlanOrderBase

	// 依从度 值为0至5，0表示依从度未知 例如:4
	ComplianceRate *uint64 `sql:"ComplianceRate" order:"DESC"`
}

type ViewFollowupPlanOrderFollowupDateAscending struct {
	ViewFollowupPlanOrderBase

	// 计划随访日期 例如:2018-07-15
	FollowupDate *time.Time `sql:"FollowupDate" index:"1"`

	ManageLevel []byte `sql:"ManageLevel" order:"DESC" index:"2"`
}

type ViewFollowupPlanOrderFollowupDateDescending struct {
	ViewFollowupPlanOrderBase

	// 计划随访日期 例如:2018-07-15
	FollowupDate *time.Time `sql:"FollowupDate" order:"DESC" index:"1"`

	ManageLevel []byte `sql:"ManageLevel" order:"DESC" index:"2"`
}

type ViewFollowupPlanOrderManageLevelAscending struct {
	ViewFollowupPlanOrderBase

	ManageLevel []byte `sql:"maxManageLevel" index:"2"`

	ManageLevelDays []byte `sql:"minLevelStartDateTime" order:"DESC" index:"3" `
}

type ViewFollowupPlanOrderManageLevelDescending struct {
	ViewFollowupPlanOrderBase

	ManageLevel []byte `sql:"maxManageLevel" order:"DESC" index:"2"`

	ManageLevelDays []byte `sql:"minLevelStartDateTime"   index:"3" `
}

var viewFollowupPlanOrders = make(map[string]interface{}, 0)

func init() {
	viewFollowupPlanOrders["followupDate-ascending"] = &ViewFollowupPlanOrderFollowupDateAscending{}
	viewFollowupPlanOrders["followupDate-descending"] = &ViewFollowupPlanOrderFollowupDateDescending{}

	viewFollowupPlanOrders["complianceRate-ascending"] = &ViewFollowupPlanOrderComplianceRateAscending{}
	viewFollowupPlanOrders["complianceRate-descending"] = &ViewFollowupPlanOrderComplianceRateDescending{}

	viewFollowupPlanOrders["maxManageLevel-ascending"] = &ViewFollowupPlanOrderManageLevelAscending{}
	viewFollowupPlanOrders["maxManageLevel-descending"] = &ViewFollowupPlanOrderManageLevelDescending{}
}

func (s *ViewFollowupPlan) GetOrder(order *model.Order) interface{} {
	if order == nil {
		return &ViewFollowupPlanOrder{}
	}

	key := order.Key()
	if len(key) < 1 {
		return &ViewFollowupPlanOrder{}
	}

	v, ok := viewFollowupPlanOrders[key]
	if ok {
		return v
	} else {
		return &ViewFollowupPlanOrder{}
	}
}
