package model

import "fmt"

type Order struct {
	Prop  *string `json:"prop" note:"排序自断"`
	Order *string `json:"order" note:"排序顺序: ascending-升序; descending-降序"`
}

func (s *Order) Key() string {
	if s.Prop == nil {
		return ""
	}

	if s.Order == nil {
		return fmt.Sprintf("%s-ascending", *s.Prop)
	} else {
		return fmt.Sprintf("%s-%s", *s.Prop, *s.Order)
	}
}
