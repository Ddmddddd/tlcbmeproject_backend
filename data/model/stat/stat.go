package stat

import "github.com/ktpswjz/httpserver/types"

type StatInput struct {
	Mode    uint64 `json:"mode" note:"1--近三月 2-- 近六月 3-- 今年 4-- 全部"`
	OrgCode string `json:"orgCode"`
}

type StatOut struct {
	Date      types.Time `json:"date"`
	Value     string     `json:"value"`
	CountType uint64     `json:"countType"`
}
