package oauth

type Output struct {
	Token   string `json:"token"`
	Expires uint64 `json:"expires_in"`
}

type UserResult struct {
	UserName string `json:"userName"`
	UserNo   string `json:"userNo"`
}
type UserAccount struct {
	UserId   uint64 `json:"userId"`
	UserName string `json:"userName"`
}
