package oauth

type Input struct {
	Code   string `json:"code"`
	OpenId string `json:"openId"`
}
