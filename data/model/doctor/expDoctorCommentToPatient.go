package doctor

import "github.com/ktpswjz/httpserver/types"

type ExpDoctorCommentToPatient struct {
	SerialNo          uint64      `json:"serialNo" note:""`
	SportDietRecord   uint64      `json:"sportDietRecord" note:"对应sportDietRecord表中的序号"`
	Comment           string      `json:"comment" note:"医生/营养师的评论"`
	PatientReply	  *string	  `json:"patientReply" note:"用户的对于该条评论的回复"`
	ReadFlag          uint64      `json:"readFlag" note:"患者是否已读，0-未读，1-已读"`
	PatientID         uint64      `json:"patientID" note:"患者id"`
	CreateDateTime    *types.Time `json:"createDateTime" note:"创建时间"`
	UpdateTime        *types.Time `json:"updateTime" note:"修改时间"`
}
