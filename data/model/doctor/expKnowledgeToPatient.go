package doctor

import "github.com/ktpswjz/httpserver/types"

type ExpKnowledgeToPatient struct {
	SerialNo       uint64      `json:"serialNo" note:""`
	PatientID      uint64      `json:"patientID" note:"患者id"`
	Type 		   string	   `json:"type" note:"知识类型"`
	KnowledgeID    uint64      `json:"knowledgeID" note:"知识id"`
	CreateDateTime *types.Time `json:"createDateTime" note:"创建时间"`
	DayIndex       uint64      `json:"DayIndex", note:"对应计划的日期"`
	Status 		   uint64  	   `json:"status" note:"记录完成情况，0-未完成，1-已完成"`
}
