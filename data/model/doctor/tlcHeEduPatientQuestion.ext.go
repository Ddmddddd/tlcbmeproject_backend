package doctor

type TlcHeEduPatientQuestionEnter struct {
	PatientID uint64 `json:"patientID" note:"用户的id，用于说明问题发送给哪个用户"`
}

type TlcHeEduPatientIntegratedQuestionInfo struct {
	Id             uint64                        `json:"id" note:"问题的id"`
	Type           uint64                        `json:"type" note:"问题的类型,1代表是选择题，2代表是判断题"`
	Question       string                        `json:"question" note:"问题的内容"`
	Options        []*TlcHeEduQuestionOptionInfo `json:"options" note:"问题对应的选项，需要依据问题的id查出来"`
	Status         uint64                        `json:"status" note:"用户回答问题的状态， 0代表尚未回答问题， 1代表答对， 2代表答错"`
	Reply          uint64                        `json:"reply" note:"针对判断题的正否验证，若不是判断题该项为0， 若为判断题：1代表true:true, 2代表false:true"`
	Explanation    string                        `json:"explanation" note:"问题的解析"`
	TotalAnswerNum uint64                        `json:"totalAnswerNum" note:"回答该问题的总人数"`
	RightAnswerNum uint64                        `json:"rightAnswerNum" note:"正确回答该问题的人数"`
}

type TlcHeEduPatientAnswerQuestionRecord struct {
	PatientID uint64 `json:"patientID" note:"用户的id"`
	Id        uint64 `json:"question_id" note:"问题的id"`
	Status    uint64 `json:"status" note:"用户回答问题的状态"`
}

type TlcHeEduPatientQuestionGetBindWithKnw struct {
	PatientID uint64   `json:"patientID" note:"用户的id"`
	KnwIdList []uint64 `json:"knwIdList" note:"用户收到的知识的id列表"`
}
