package doctor

type KnowledgeToTag struct {
	Id           uint64 `json:"id" note:"知识和标签关联的id"`
	Knowledge_id uint64 `json:"knowledge_id" note:"知识的id"`
	Tag_id       uint64 `json:"tag_id" note:"标签的id"`
}
