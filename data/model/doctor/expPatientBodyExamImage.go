package doctor

import "github.com/ktpswjz/httpserver/types"

type ExpPatientBodyExamImage struct {
	SerialNo       uint64      `json:"serialNo" note:""`
	PatientID      uint64      `json:"patientID" note:""`
	ImgUrl         string      `json:"imgUrl" note:""`
	CreateDateTime *types.Time `json:"createDateTime" note:""`
}
