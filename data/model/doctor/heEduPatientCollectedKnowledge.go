package doctor

type PatientCollectedKnowledge struct {
	Id                     uint64 `json:"id" note:"用户收藏知识记录的id"`
	PatientID              uint64 `json:"patientID" note:"用户的id"`
	Collected_knowledge_id uint64 `json:"knowledge_id" note:"知识的id"`
}
