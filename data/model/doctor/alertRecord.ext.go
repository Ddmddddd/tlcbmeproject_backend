package doctor

import (
	"github.com/ktpswjz/httpserver/types"
	"time"
)

type AlertRecordFilter struct {
	SerialNo       *uint64     `json:"serialNo" note:"序号 主键，自增 例如:324"`
	PatientID      *uint64     `json:"patientID" note:"用户ID 关联患者用户登录表用户ID 例如:232442"`
	AlertCode      string      `json:"alertCode" note:"预警代码 每类预警的唯一代码 例如:001"`
	AlertName      string      `json:"alertName" note:"预警名称 例如:单次血压异常偏高"`
	AlertStartDate *types.Time `json:"alertStartDate" note:"预警发生开始日期  例如:2018-07-01"`
	AlertEndDate   *types.Time `json:"alertEndDate" note:"预警发生结束日期 例如:2018-07-03"`

	AlertTypes   []string `json:"alertTypes" note:"预警类型 例如:血压"`
	Statuses     []uint64 `json:"statuses" note:"处理状态 0-未处理 例如:0"`
	ProcessModes []uint64 `json:"processModes" note:"处理方式 0-随访，1-忽略 例如:0"`
}

type AlertRecordFilterEx struct {
	AlertRecordFilter

	OrgCode         string  `json:"orgCode" note:"管理机构代码 管理该患者的机构 例如:897798"`
	DoctorID        *uint64 `json:"doctorID" note:"医生ID 例如:232"`
	HealthManagerID *uint64 `json:"healthManagerID" note:"健康管理师ID 例如:232"`
}

type AlertRecordEx struct {
	AlertRecord

	StatusText      string `json:"statusText" note:"处理状态文本 0-未处理 例如:未处理"`
	ProcessModeText string `json:"processModeText" note:"处理方式文本 0-随访，1-忽略 例如:忽略"`
}

type AlertInfo struct {
	BloodPressures []AlertRecordEx `json:"bloodPressures" note:"血压预警信息"`
	BloodGlucoses  []AlertRecordEx `json:"bloodGlucoses" note:"血糖预警信息"`
	Others         []AlertRecordEx `json:"others" note:"其它预警信息"`
}

type AlertPatientInfo struct {
	PatientID   uint64      `json:"patientID" note:"用户ID 关联患者用户登录表用户ID 例如:232442"`
	PatientName string      `json:"patientName" note:"姓名  例如:张三"`
	Sex         *uint64     `json:"sex" note:"性别 0-未知, 1-男, 2-女，9-未说明 例如:1"`
	SexText     string      `json:"sexText" note:"性别 0-未知, 1-男, 2-女，9-未说明 例如:男"`
	DateOfBirth *types.Time `json:"dateOfBirth" note:"出生日期 例如:2000-12-12"`
	Age         int64       `json:"age" note:"年龄，单位岁，如60"`
	Tag         string      `json:"tag" note:"人员标签"`
	Photo       string      `json:"photo" note:"头像照片 例如:"`
	Alert       AlertInfo   `json:"alert" note:"预警信息"`
}

type AlertPatientCollection []*AlertPatientInfo

func (s AlertPatientCollection) GetInfo(patientID uint64) *AlertPatientInfo {
	count := len(s)
	for i := 0; i < count; i++ {
		item := s[i]
		if item == nil {
			continue
		}

		if item.PatientID == patientID {
			return item
		}
	}

	return nil
}

type AlertRecordIgnore struct {
	SerialNos    []uint64 `json:"serialNos" note:"序号 例如:[324, 432]"`
	IgnoreReason string   `json:"ignoreReason" note:"忽略预警的原因 ProcessMode为1时有效。忽略预警的原因 例如:重复预警"`
}

type AlertRecordIgnoreEx struct {
	AlertRecordIgnore

	Status           uint64     `json:"status" note:"处理状态 0-未处理 例如:0"`
	ProcessMode      *uint64    `json:"processMode" note:"处理方式 0-随访，1-忽略 例如:0"`
	IgnoreReason     string     `json:"ignoreReason" note:"忽略预警的原因 ProcessMode为1时有效。忽略预警的原因 例如:重复预警"`
	IgnoreDoctorID   *uint64    `json:"ignoreDoctorID" note:"随访人ID ProcessMode为1时有效。 例如:111"`
	IgnoreDoctorName string     `json:"ignoreDoctorName" note:"随访人姓名 ProcessMode为1时有效。 例如:张三"`
	IgnoreDateTime   *time.Time `json:"ignoreDateTime" note:"忽略预警的时间 ProcessMode为1时有效。 例如:2018-09-03 12:00:23"`
}

type AlertBPAndHRFeedbackForApp struct {
	BPCode  string `json:"bpCode" note:血压预警码 例如:B00"`
	BPValue string `json:"bpValue" note:血压值 例如:120/80mmHg"`
	BPTips  string `json:"bpTips" note:血压提示 例如:当前血压为轻度异常。降压治疗应缓慢进行，不能求之过急，血压达标通常需要 4-12周，请坚持测量血压哦。"`
	HRCode  string `json:"hrCode" note:心率预警码 例如:H01"`
	HRValue string `json:"hrValue" note:心率值 例如:60bpm"`
	HRTips  string `json:"hrTips" note:心率提示 例如:当前心率为正常。请持续监测一周，按医嘱服用药物。如果出现头晕头痛、耳鸣等不适症状，请立即复诊。"`
}

type AlertGLUFeedbackForApp struct {
	GLUCode  string `json:"gluCode" note:血糖预警码 例如:D01"`
	GLUValue string `json:"gluValue" note:血糖值 例如:120/80mmHg"`
	GLUTips  string `json:"gluTips" note:血糖提示 例如:本次测量量的⾎血糖正常。您⽬目前的⾎血糖已经达标，请继续保持，并坚持规律律⽤用药和⾃自测⾎血糖，警惕摄⼊入过多⾼高糖、⾟辛辣⻝⾷食品。"`
}

type AlertPatientRecord struct {
	AlertCode string `json:"alertCode" note:"预警代码 每类预警的唯一代码 例如:001"`
}
