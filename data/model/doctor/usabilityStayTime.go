package doctor

import "github.com/ktpswjz/httpserver/types"

type UsabilityStayTime struct {
	SerialNo   uint64      `json:"serialNo" note:"序号 主键，自增 例如:1"`
	UserName   string      `json:"userName" note:"用户名 关联患者用户登录表用户名 例如:aa"`
	AppId      string      `json:"appId" note:"应用标识 唯一标识应用 例如:abcdefghijklmn"`
	AppType    string      `json:"appType" note:"应用类别 Android、iOS或者微信小程序 例如:Android"`
	AppVersion string      `json:"appVersion" note:"应用版本 当前应用版本 例如:1.0.1.0"`
	PageName   string      `json:"pageName" note:"界面名称 当前界面名称 例如:MainPage"`
	EnterTime  *types.Time `json:"enterTime" note:"进入时间 进入当前界面的时间点 例如:2018-12-12 08:00:00"`
	ExitTime   *types.Time `json:"exitTime" note:"退出时间 离开当前界面的时间点 例如:2018-12-12 08:01:00"`
	StayTime   string      `json:"stayTime" note:"停留时间 停留在当前界面的时间，单位：s 例如:60"`
}
