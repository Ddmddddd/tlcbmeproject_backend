package doctor

import "github.com/ktpswjz/httpserver/types"

type InstrumentBloodPressureRecord struct {
	InstrumentPatientInfo
	SystolicPressure  uint64      `json:"systolicPressure" required:"true" note:"收缩压 单位：mmHg 例如:110"`
	DiastolicPressure uint64      `json:"diastolicPressure" required:"true" note:"舒张压 单位：mmHg 例如:78"`
	HeartRate         *uint64     `json:"heartRate" required:"true" note:"心率 单位：次/分钟 例如:65"`
	MeasureDateTime   *types.Time `json:"measureDateTime" required:"true" note:"测量时间 测量血压时间 例如:2018-07-03 14:45:00"`
	DeviceId          string      `json:"deviceId" required:"true" note:"设备Id 例如:239120170101"`
}
