package doctor

import "github.com/ktpswjz/httpserver/types"

type BloodGlucoseRecord struct {
	SerialNo        uint64      `json:"serialNo" note:"序号 主键，自增 例如:324"`
	PatientID       uint64      `json:"patientID" note:"用户ID 关联患者用户登录表用户ID 例如:232442"`
	BloodType       string      `json:"bloodType" note:"血样类型 毛细血管血、静脉血、动脉血、新生儿血 例如:毛细血管血"`
	BloodGlucose    float64     `json:"bloodGlucose" note:"血糖值 单位：mmol/L 例如:6.1"`
	BloodKetone     *float64    `json:"bloodKetone" note:"血酮值 单位：mmol/L 例如:6.1"`
	TimePoint       string      `json:"timePoint" note:"测量时间点 空腹、早餐前、早餐后2h、午餐前、午餐后2h、晚餐前、晚餐后2h、睡前、夜间 例如:空腹"`
	Memo            string      `json:"memo" note:"备注  例如:最近在口服降糖药"`
	MeasureDateTime *types.Time `json:"measureDateTime" note:"测量时间 测量血糖时的时间 例如:2018-07-03 14:45:00"`
	InputDateTime   *types.Time `json:"inputDateTime" note:"录入时间 将血糖值录入系统的时间 例如:2018-07-03 14:00:00"`
}
