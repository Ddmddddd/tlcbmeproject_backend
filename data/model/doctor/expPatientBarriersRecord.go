package doctor

import "github.com/ktpswjz/httpserver/types"

type ExpPatientBarriersRecord struct {
	SerialNo       uint64      `json:"serialNo" note:"序号"`
	PatientID      uint64      `json:"patientID" note:"患者ID"`
	Barrier        string      `json:"barrier" note:"健康饮食障碍"`
	Level 		   uint64 	   `json:"level" note:"障碍程度"`
	Status         uint64      `json:"status" note:"状态，0-存在，1-已克服，9-删除"`
	CreateDateTime *types.Time `json:"createDateTime" note:"创建时间"`
	UpdateDateTime *types.Time `json:"updateDateTime" note:"更新时间"`
}
