package doctor

import "time"

type CampList struct {
	SerialNo       uint64     `json:"serialNo" note:"序号"`
	Name           string     `json:"name" note:"训练营名称"`
	Desc           *string    `json:"desc" note:"训练营描述"`
	IsValid        uint8      `json:"IsValid" note:"是否有效"`
	CreateDateTime *time.Time `json:"createDateTime" note:"创建时间"`
	UpdateDateTime *time.Time `json:"UpdateDateTime" note:"更新时间"`
}
