package doctor

type TlcPatientToRecKnowledgeGetStar struct {
	PatientID    uint64 `json:"patientID" note:"患者id"`
	Knowledge_id uint64 `json:"knowledge_id" note:"知识id"`
}

type TlcPatientToRecKnowledgeUpdateStar struct {
	PatientID    uint64 `json:"patientID" note:"患者id"`
	Knowledge_id uint64 `json:"knowledge_id" note:"知识id"`
	Stars        uint64 `json:"stars" note:"用户对知识的评分"`
}

type TlcPatientToRecKnowledgeChangeStatus struct {
	PatientID    uint64 `json:"patientID" note:"患者id"`
	Knowledge_id uint64 `json:"knowledge_id" note:"知识id"`
}

type TlcPatientToRecKnowledgePatientID struct {
	PatientID uint64 `json:"patientID" note:"患者id"`
}
