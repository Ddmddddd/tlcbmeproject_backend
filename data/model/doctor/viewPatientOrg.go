package doctor

type ViewPatientOrg struct {
	PatientID  uint64 `json:"patientID" note:"用户ID 关联医生用户登录表用户ID 例如:232442"`
	Name       string `json:"name" note:"姓名  例如:张三"`
	OrgCode    string `json:"orgCode" note:"管理机构代码 管理该患者的机构 例如:897798"`
	DoctorName string `json:"doctorName" note:"医生姓名 例如:张三"`
	OrgName    string `json:"orgName" note:"机构名称  例如:第一人民医院"`
}
