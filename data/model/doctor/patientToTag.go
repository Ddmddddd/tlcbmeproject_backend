package doctor

type PatientToTag struct {
	Id     uint64 `json:"id" note:"关系表中自带的序号，主键"`
	UserID uint64 `json:"userID" note:"用户的id，与tag关联"`
	Tag_id uint64 `json:"tag_id" note:"标签的id"`
}
