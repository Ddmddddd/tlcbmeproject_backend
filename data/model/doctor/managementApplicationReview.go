package doctor

import "github.com/ktpswjz/httpserver/types"

type ManagementApplicationReview struct {
	SerialNo        uint64      `json:"serialNo" note:"序号 主键，自增 例如:324"`
	PatientID       uint64      `json:"patientID" note:"用户ID 关联患者用户登录表用户ID 例如:232442"`
	ApplicationFrom string      `json:"applicationFrom" note:"来源 表示从哪里发起的申请，其值可以为：APP、微信小程序 例如:APP"`
	Diagnosis       string      `json:"diagnosis" note:"诊断 例如:高血压，糖尿病"`
	DiagnosisMemo   string      `json:"diagnosisMemo" note:"诊断备注  例如:血压 150/90 mmHg，血糖 7.9 mmol/L"`
	VisitOrgCode    string      `json:"visitOrgCode" note:"就诊机构代码 例如:123"`
	OrgVisitID      string      `json:"orgVisitID" note:"机构内就诊号 例如:123456"`
	HealthManagerID *uint64     `json:"healthManagerID" note:"健康管理师ID 患者注册时指定的健康管理师 例如:12432"`
	Status          uint64      `json:"status" note:"审核状态 0-未审核，1-审核通过，2-审核不通过，3-忽略。 例如:0"`
	ReviewerID      *uint64     `json:"reviewerID" note:"审核人ID 审核人对应的用户ID 例如:32423"`
	ReviewerName    string      `json:"reviewerName" note:"审核人姓名 例如:张三"`
	ReviewDateTime  *types.Time `json:"reviewDateTime" note:"审核时间 例如:2018-07-03 13:00:00"`
	RefuseReason    string      `json:"refuseReason" note:"审核不通过原因 最近一次审核不通过的原因。只有当状态为2时有效。 例如:信息不够完善"`
}
