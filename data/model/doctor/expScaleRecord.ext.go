package doctor

import "github.com/ktpswjz/httpserver/types"

type Question struct {
	Type 	   string 			`json:"type", note:"选择类型，分为singleCheck和MultiCheck"`
	Title 	   string 			`json:"title", note:"问题标题"`
	Options    []Option	    	`json:"options", note:"选项，类型为Option"`
	Degree     []uint64			`json:"degree", note:"程度，代表问题的严重性"`
	Barriers   []uint64			`json:"barriers", note:"链接的健康饮食障碍"`
	Checked    []uint64			`json:"checked", note:"选择的选项"`
}

type NewQuestion struct {
	Type 	   string 			`json:"type", note:"选择类型，分为singleCheck和MultiCheck"`
	Title 	   string 			`json:"title", note:"问题标题"`
	Options    []Option	    	`json:"options", note:"选项，类型为Option"`
	Checked    []string			`json:"checked", note:"选择的选项"`
	Value      string           `json:"value" note:"值"`
}

type Option struct {
	Index   string `json:"index", note:"选项，例如:A"`
	Content string `json:"content", note:"选项内容"`
}

type ExpScaleRecordCreate struct {
	SerialNo  		uint64      `json:"serialNo" note:"序号"`
	ScaleName		string 		`json:"scaleName" note:"量表名"`
	PatientID 		uint64      `json:"patientID" note:"用户ID"`
	ScaleType 		uint64      `json:"scaleType" note:"量表属性，0-默认量表，1-普通量表"`
	ScaleID   		uint64      `json:"scaleID" note:"量表ID"`
	Result    		interface{} `json:"result" note:"量表结果，JSON格式存储"`
	CreateDateTime	*types.Time `json:"createDateTime" note:"提交时间"`
	OperatorID		uint64		`json:"operatorID" note:"操作者，若为患者本人操作为0"`
	//UndoneScales    []uint64	`json:"undoneScales" note:"未完成的问卷列表"`
	ScaleVersion	uint64		`json:"scaleVersion" note:"量表编码"`
}

type ExpDefaultScaleRecordCreate struct {
	PatientID 		uint64      `json:"patientID" note:"用户ID"`
	ScaleType 		uint64      `json:"scaleType" note:"量表属性，0-默认量表，1-普通量表"`
	ScaleID   		uint64      `json:"scaleID" note:"量表ID"`
	Result    		interface{} `json:"result" note:"量表结果，JSON格式存储"`
	CreateDateTime	*types.Time `json:"createDateTime" note:"提交时间"`
}

type DefaultScaleQuestion struct {
	Type 	    string 		`json:"type", note:"选择类型，分为singleCheck和MultiCheck"`
	Title 		string 		`json:"title" note:""`
	Options 	[]Option 	`json:"options"`
	Checked 	uint64 		`json:"checked"`
}