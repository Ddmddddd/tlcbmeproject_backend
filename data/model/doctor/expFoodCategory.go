package doctor

type ExpFoodCategory struct {
	SerialNo uint64 `json:"serial" note:"分类编号"`
	Name   	 string `json:"name" note:"名称"`
}
