package doctor

type UploadData struct {
	Hospital    string  `json:"hospital" note:"医院名称"`
	DoctorName  *string `json:"doctorName" note:"医生姓名  例如:12345"`
	PatientName string  `json:"patientName" note:"患者姓名 例如:李四"`
	DataType    string  `json:"type" note:"具体类型 例如：血压"`
	Time        string  `json:"time" note:"时间 例如：2018-12-12 12:12:12"`
}
