package doctor

import "github.com/ktpswjz/httpserver/types"

type ViewManagementApplicationReview struct {
	SerialNo           uint64      `json:"serialNo" note:"序号 主键，自增 例如:324"`
	PatientID          uint64      `json:"patientID" note:"用户ID 关联患者用户登录表用户ID 例如:232442"`
	PatientName        string      `json:"patientName" note:"姓名  例如:张三"`
	ApplicationFrom    string      `json:"applicationFrom" note:"来源 表示从哪里发起的申请，其值可以为：APP、微信小程序 例如:APP"`
	RegistDateTime     *types.Time `json:"registDateTime" note:"注册时间  例如:2018-07-02 15:00:00"`
	Diagnosis          string      `json:"diagnosis" note:"诊断 例如:高血压，糖尿病"`
	DiagnosisMemo      string      `json:"diagnosisMemo" note:"诊断备注  例如:血压 150/90 mmHg，血糖 7.9 mmol/L"`
	VisitOrgCode       string      `json:"visitOrgCode" note:"就诊机构代码 例如:123"`
	OrgVisitID         string      `json:"orgVisitID" note:"机构内就诊号 例如:123456"`
	HealthManagerID    *uint64     `json:"healthManagerID" note:"健康管理师ID 患者注册时指定的健康管理师 例如:12432"`
	Status             uint64      `json:"status" note:"审核状态 0-未审核，1-审核通过，2-审核不通过，3-忽略。 例如:0"`
	ReviewerID         *uint64     `json:"reviewerID" note:"审核人ID 审核人对应的用户ID 例如:32423"`
	ReviewerName       string      `json:"reviewerName" note:"审核人姓名 例如:张三"`
	ReviewDateTime     *types.Time `json:"reviewDateTime" note:"审核时间 例如:2018-07-03 13:00:00"`
	RefuseReason       string      `json:"refuseReason" note:"审核不通过原因 最近一次审核不通过的原因。只有当状态为2时有效。 例如:信息不够完善"`
	Sex                *uint64     `json:"sex" note:"性别 0-未知, 1-男, 2-女，9-未说明 例如:1"`
	DateOfBirth        *types.Time `json:"dateOfBirth" note:"出生日期 例如:2000-12-12"`
	IdentityCardNumber string      `json:"identityCardNumber" note:"身份证号 例如:330106200012129876"`
	Country            string      `json:"country" note:"国籍 GB/T 2659-2000 世界各国和地区名称 例如:中国"`
	Nation             string      `json:"nation" note:"民族 GB 3304-1991 中国各民族名称 例如:汉族"`
	NativePlace        string      `json:"nativePlace" note:"籍贯  例如:浙江杭州"`
	MarriageStatus     string      `json:"marriageStatus" note:"婚姻状况 GB/T 2261.2-2003 例如: "`
	EducationLevel     string      `json:"educationLevel" note:"文化程度 GB/T 4658-1984 文化程度 例如:大学"`
	JobType            string      `json:"jobType" note:"职业类别 GB/T 6565-1999 职业分类 例如: "`
	Nickname           string      `json:"nickname" note:"昵称 例如:蓝精灵"`
	PersonalSign       string      `json:"personalSign" note:"个性签名 例如:在那山的那边海的那边有一群蓝精灵"`
	Phone              string      `json:"phone" note:"联系电话 例如:13812344321"`
	Height             *uint64     `json:"height" note:"身高 单位：cm 例如:165"`
	Weight             *float64    `json:"weight" note:"体重 最后一次录入的体重，单位：kg 例如:60.5"`
}
