package doctor

import "github.com/ktpswjz/httpserver/types"

type FollowupRecord struct {
	SerialNo                uint64      `json:"serialNo" note:"序号 主键，自增 例如:324"`
	PatientID               uint64      `json:"patientID" note:"用户ID 关联患者用户登录表用户ID 例如:232442"`
	FollowupDateTime        *types.Time `json:"followupDateTime" note:"随访时间  例如:2018-08-01 10:00:00"`
	FollowupType            string      `json:"followupType" note:"随访类型  例如:三个月例行随访"`
	FollowupMethod          string      `json:"followupMethod" note:"随访方式 例如：电话、门诊、家庭、APP 例如:电话"`
	Status                  uint64      `json:"status" note:"结果状态 用于描述本次随访是否有效，0-失访，1-进行中，2-有效，默认值为1 例如:1"`
	FailureReason           string      `json:"failureReason" note:"失访原因 只有当Status为0时该字段才有意义，用于描述失访的原因。 例如:电话停机"`
	DeathTime               *types.Time `json:"deathTime" note:"死亡时间 该字段只有当失访原因为亡故时才有意义 例如:2018-08-02"`
	CauseOfDeath            string      `json:"causeOfDeath" note:"死亡原因 该字段只有当失访原因为亡故时才有意义 例如:车祸死亡"`
	FollowupTemplateCode    uint64      `json:"followupTemplateCode" note:"随访模板代码 1-高血压随访记录表, 2-糖尿病随访记录表, 3-慢阻肺随访记录表, 4-公卫高血压患者随访服务记录表, 5-公卫2型糖尿病患者随访服务记录表 例如:1"`
	FollowupTemplateVersion uint64      `json:"followupTemplateVersion" note:"随访记录内容模板版本号 例如:1"`
	Summary                 string      `json:"summary" note:"随访记录摘要 摘要规则根据模板不同而不同。 例如:血压170/99mmHg，控制不满意，嘱患者近期复诊。"`
	Content                 interface{} `json:"content" note:"随访记录内容 JSON格式保存 例如:"`
	DoctorID                uint64      `json:"doctorID" note:"随访人ID  例如:111"`
	DoctorName              string      `json:"doctorName" note:"随访人姓名  例如:张三"`
	FollowupPlanSerialNo    *uint64     `json:"followupPlanSerialNo" note:"随访计划序号 本次随访记录关联的随访计划的序号 例如:4354"`
}
