package doctor

import "github.com/ktpswjz/httpserver/types"

type Timingtask struct {
	Serialno            uint64      `json:"serialno" note:"序号 主键，自增"`
	Patientid           uint64      `json:"patientid" note:"用户ID 关联患者用户登录表用户ID"`
	Tasktype            uint64      `json:"tasktype" note:"定时任务类型：1-高血压短信，2-糖尿病短信"`
	Operatorid          uint64      `json:"operatorid" note:"操作者 保存用户ID"`
	DoctorName          string      `json:"DoctorName" note:"医生姓名"`
	Timingstartdatetime *types.Time `json:"timingstartdatetime" note:"开始定时时间"`
}
