package doctor

type ExpNutritionQuestionRepository struct {
	SerialNo      uint64  `json:"serialNo" note:""`
	Title         string  `json:"title" note:""`
	Option        string  `json:"option" note:""`
	QuestionLevel *uint64 `json:"questionLevel" note:""`
}
