package doctor

import "github.com/ktpswjz/httpserver/types"

type WeightRecord struct {
	SerialNo        uint64      `json:"serialNo" note:"序号 主键，自增 例如:324"`
	PatientID       uint64      `json:"patientID" note:"用户ID 关联患者用户登录表用户ID 例如:232442"`
	Weight          float64     `json:"weight" note:"体重 单位：kg 例如:60.5"`
	Waistline       *float64    `json:"weight" note:"腰围 单位：cm 例如:60.5"`
	Height          *uint64     `json:"weight" note:"身高 单位：cm 例如:60.5"`
	Water           *float64    `json:"water"`
	Sleep           *float64    `json:"sleep"`
	Memo            string      `json:"memo" note:"备注 用于说明测量时的情况 例如:有氧运动半小时后"`
	TimePoint       string      `json:"timePoint" note:"时间点 早、中、晚 例如:早"`
	Photo           string      `json:"photo" note:"拍照"`
	MeasureDateTime *types.Time `json:"measureDateTime" note:"测量时间 测量体重时的时间 例如:2018-07-03 14:45:00"`
	InputDateTime   *types.Time `json:"inputDateTime" note:"录入时间 将体重值录入系统的时间 例如:2018-07-03 14:00:00"`
	BloodPressureNo *uint64     `json:"bloodPressureNo"`
	BloodGlucoseNo  *uint64     `json:"bloodGlucoseNo"`
}
