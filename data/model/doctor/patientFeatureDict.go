package doctor

type PatientFeatureDict struct {
	SerialNo      uint64  `json:"serialNo" note:"序号 主键，自增 例如:324"`
	ItemCode      uint64  `json:"itemCode" note:"字典项代码  例如:1"`
	ItemName      string  `json:"itemName" note:"字典项名称  例如:老年人"`
	ItemSortValue *uint64 `json:"itemSortValue" note:"排序 用于字典项目排序，从1开始往后排 例如:1"`
	InputCode     string  `json:"inputCode" note:"输入码 用于快速输入，可以是拼音首字母或缩写 例如:lnr"`
	IsValid       *uint64 `json:"isValid" note:"有效性 用于描述本记录是否有效，0-无效，1-有效，默认值为1 例如:1"`
}
