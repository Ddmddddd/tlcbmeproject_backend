package doctor

import "github.com/ktpswjz/httpserver/types"

type WeightRecordData struct {
	Weight          float64     `json:"weight" note:"体重 单位：kg 例如:60.5"`
	MeasureDateTime *types.Time `json:"measureDateTime" note:"测量时间 测量体重时的时间 例如:2018-07-03 14:45:00"`
	Memo            string      `json:"memo" note:"备注 用于说明测量时的情况 例如:有氧运动半小时后"`
}

type WeightRecordForApp struct {
	SerialNo        uint64      `json:"serialNo" note:"序号 主键，自增 例如:324"`
	Weight          float64     `json:"weight" note:"体重 单位：kg 例如:60.5"`
	MeasureDateTime *types.Time `json:"measureDateTime" note:"测量时间 测量体重时的时间 例如:2018-07-03 14:45:00"`
	Memo            string      `json:"memo" note:"备注 用于说明测量时的情况 例如:有氧运动半小时后"`
	Type            uint64      `json:"type" note:"测量时刻 测量血压的时刻 例如:1早上 2中午 3晚上 0自定义"`
	Waistline       *float64    `json:"waistline" note:"腰围 单位：cm 例如:60.5"`
	Water           *float64    `json:"water" note:"饮水"`
	Sleep           *float64    `json:"sleep" note:"睡眠"`

	Photo      string   `json:"photo" note:"拍照"`
	IsUp       bool     `json:"isUp" note:"是否上升"`
	Difference *float64 `json:"difference" note:"差值"`
}

type WeightRecordForAppEx struct {
	WeightRecordForApp
	BloodPressureNo   *uint64  `json:"bloodPressureNo"`
	BloodGlucoseNo    *uint64  `json:"bloodGlucoseNo"`
	SystolicPressure  *uint64  `json:"sys"`
	DiastolicPressure *uint64  `json:"dia"`
	BloodGlucose      *float64 `json:"glu"`
	TimePoint         *string  `json:"timePoint"`
	Water             *float64 `json:"water"`
	Sleep             *float64 `json:"sleep"`
	Waistline         *float64 `json:"waistline" note:"腰围 单位：cm 例如:60.5"`
	Height            *uint64  `json:"height" note:"身高 单位：cm 例如:170"`
	PatientID         uint64   `json:"patientID" required:"true" note:"用户ID 关联患者用户登录表用户ID 例如:232442"`
}

type WeightRecordDataFilter struct {
	MeasureStartDate *types.Time `json:"measureStartDate" note:"测量开始时间 测量体重的时间 例如:2018-07-03 14:45:00"`
	MeasureEndDate   *types.Time `json:"measureEndDate" note:"测量结束时间 测量体重的时间 例如:2018-08-03 14:45:00"`
}

type WeightRecordDataFilterEx struct {
	WeightRecordDataFilter
	PatientID uint64 `json:"patientID" required:"true" note:"用户ID 关联患者用户登录表用户ID 例如:232442"`
}

type WeightRecordCreate struct {
	WeightRecordData

	PatientID uint64 `json:"patientID" required:"true" note:"用户ID 关联患者用户登录表用户ID 例如:232442"`
}

type WeightRecordForPhoto struct {
	Weight          float64     `json:"weight" note:"体重 单位：kg 例如:60.5"`
	MeasureDateTime *types.Time `json:"measureDateTime" note:"测量时间 测量体重时的时间 例如:2018-07-03 14:45:00"`
	Photo           string      `json:"photo" note:"拍照"`
}
