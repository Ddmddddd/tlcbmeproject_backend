package doctor

type OrgTypeDict struct {
	SerialNo       uint64  `json:"serialNo" note:"序号 主键，自增 例如:324"`
	LargeTypeCode  string  `json:"largeTypeCode" note:"大类代码 例如:A"`
	LargeTypeName  string  `json:"largeTypeName" note:"大类名称 例如:医院"`
	MiddleTypeCode string  `json:"middleTypeCode" note:"中类代码 例如:A5"`
	MiddleTypeName string  `json:"middleTypeName" note:"中类名称 例如:专科医院"`
	SmallTypeCode  string  `json:"smallTypeCode" note:"小类代码 例如:A513"`
	SmallTypeName  string  `json:"smallTypeName" note:"小类名称 例如:耳鼻喉科医院"`
	Memo           string  `json:"memo" note:"备注 例如:包括五官科医院"`
	ItemSortValue  *uint64 `json:"itemSortValue" note:"排序 用于字典项目排序，从1开始往后排 例如:1"`
	InputCode      string  `json:"inputCode" note:"输入码 用于快速输入，可以是拼音首字母或缩写 例如:gxy"`
	IsValid        *uint64 `json:"isValid" note:"有效性 用于描述本记录是否有效，0-无效，1-有效，默认值为1 例如:1"`
}
