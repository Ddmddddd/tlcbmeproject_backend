package doctor

import "github.com/ktpswjz/httpserver/types"

type ShareRecord struct {
	SerialNo      uint64      `json:"serialNo" note:""`
	PatientID     uint64      `json:"patientID" note:"分享人"`
	ShareDateTime *types.Time `json:"shareDateTime" note:"分享时间"`
	TimePoint     string      `json:"timePoint" note:"餐次"`
	ShareImage    string      `json:"shareImage" note:"分享图片"`
}
