package doctor

import "github.com/ktpswjz/httpserver/types"

type MedicalTeam struct {
	SerialNo           uint64      `json:"serialNo" note:"序号 主键，自增 例如:324"`
	TeamCode           uint64      `json:"teamCode" note:"团队代码 例如:1"`
	TeamName           string      `json:"teamName" note:"团队名称 例如:维科专家团队"`
	Description        string      `json:"description" note:"团队描述 例如:维科专家团队创建于1996年......"`
	Level              uint64      `json:"level" note:"所属级别 0-机构内，9-整个平台内 例如:0"`
	OrgCode            string      `json:"orgCode" note:"所属机构代码 当Level为0时该字段有效，表示该医疗团队属于某个机构；当Level为1时该字段为null，表示该医疗团队是跨机构的。 例如:"`
	IsValid            uint64      `json:"isValid" note:"有效性 用于描述本记录是否有效，0-无效，1-有效，默认值为1 例如:1"`
	CreatorID          *uint64     `json:"creatorID" note:"创建者ID 例如:123"`
	CreatorName        string      `json:"creatorName" note:"创建者姓名 例如:张三"`
	CreateDateTime     *types.Time `json:"createDateTime" note:"创建时间 例如:2018-07-05 13:00:00"`
	LastModifyID       *uint64     `json:"lastModifyID" note:"最后修改者ID 例如:234"`
	LastModifyName     string      `json:"lastModifyName" note:"最后修改者姓名 例如:李四"`
	LastModifyDateTime *types.Time `json:"lastModifyDateTime" note:"最后修改时间 例如:2018-07-05 14:00:00"`
}
