package doctor

import "github.com/ktpswjz/httpserver/types"

type PatientUserAuths struct {
	UserID            uint64      `json:"userID" note:"用户ID 主键，自增 例如:"`
	UserName          string      `json:"userName" note:"用户名 如果注册时没有指定用户名，则系统默认分配一个用户名，格式为cdmwb+userID，例如cdmwb123 例如:zhangsan"`
	MobilePhone       string      `json:"mobilePhone" note:"手机号 绑定的手机号 例如:13818765432"`
	Email             string      `json:"email" note:"电子邮箱 绑定的电子邮箱地址 例如:tlcbme_project@vico-lab.com"`
	Password          string      `json:"password" note:"登录密码 加密后存储 例如:291fakfjwi98234fsf23fjw"`
	PasswordFormat    uint64      `json:"passwordFormat" note:"登录密码格式 0-明文，1-MD5，2-SHA1，3-SHA256，4-SHA384，5-SHA512 例如:1"`
	Status            uint64      `json:"status" note:"用户状态 0-正常，1-冻结，9-已注销 例如:0"`
	RegistDateTime    *types.Time `json:"registDateTime" note:"注册时间  例如:2018-07-02 15:00:00"`
	LoginCount        uint64      `json:"loginCount" note:"登录次数 累计登录次数 例如:12"`
	LastLoginDateTime *types.Time `json:"lastLoginDateTime" note:"最后一次登录时间  例如:2018-07-02 15:10:00"`
	UseSeconds        uint64      `json:"useSeconds" note:"累计使用时长 单位：秒 例如:24342"`
}
