package doctor

type DietContent struct {
	SerialNo           uint64  `json:"serialNo" note:""`
	NutritionSerialNo  uint64  `json:"nutritionSerialNo" note:"食物在营养数据库的id"`
	NutritionSource    string  `json:"source" note:"食物在营养数据库的表来源"`
	FoodName           string  `json:"foodName" note:"食物名称"`
	MeasureValue       float64 `json:"measureValue" note:"度量值 按标准度量单位计算"`
	DietRecordSerialNo uint64  `json:"dietRecordSerialNo" note:"用餐记录关联id"`
	FoodTypeId         string  `json:"foodTypeId" note:"食物种类Id"`
	PatientID          uint64  `json:"patientID" note:"用户ID"`
	Memo               string  `json:"memo" note:"说明"`
}
