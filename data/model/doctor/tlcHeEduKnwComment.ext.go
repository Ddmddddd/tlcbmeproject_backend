package doctor

type TlcPatientSendKnwComment struct {
	Knowledge_id uint64 `json:"knowledge_id" note:"被评论的知识id"`
	PateintID    uint64 `json:"pateintID" note:"评论的用户的id"`
	Content      string `json:"content" note:"评论的内容"`
}

type TlcGetKnwComments struct {
	Knowledge_id uint64 `json:"knowledge_id" note:"被评论的知识id"`
}

type TlcGetKnwCommentsInfo struct {
	Id           uint64 `json:"id" note:"评论的id"`
	Knowledge_id uint64 `json:"knowledge_id" note:"评论的知识id"`
	PatientID    uint64 `json:"patientID" note:"评论的用户的id"`
	Content      string `json:"content" note:"评论的内容"`
	UserName     string `json:"user" note:"用户姓名"`
}
