package doctor

import "github.com/ktpswjz/httpserver/types"

type ExpPatientActionPlan struct {
	PatientID      uint64      `json:"patientID" note:"用户ID"`
	Status         uint64      `json:"status" note:"状态，0-使用中，1-已取消"`
}

type ExpPatientActionPlanEdit struct {
	SerialNo       uint64      `json:"serialNo" note:"序号"`
	ActionPlan     string      `json:"actionPlan" note:"行动计划"`
	Status         uint64      `json:"status" note:"状态，0-使用中，1-已取消"`
	UpdateDateTime *types.Time `json:"updateDateTime" note:"更新时间"`
	EditorType     uint64      `json:"editorType" note:"编辑者，0-引擎规则，1-医生，2-患者"`
	Level          uint64      `json:"level" note:"优先级，分为0-5，数值越大优先级越高"`
}

type ExpPatientActionPlanCreate struct {
	PatientID      uint64      `json:"patientID" note:"用户ID"`
	ActionPlan     string      `json:"actionPlan" note:"行动计划"`
	CreateDateTime *types.Time `json:"createDateTime" note:"更新时间"`
	EditorType     uint64      `json:"editorType" note:"编辑者，0-引擎规则，1-医生，2-患者"`
	Level          uint64      `json:"level" note:"优先级，分为0-5，数值越大优先级越高"`
}