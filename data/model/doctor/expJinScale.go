package doctor

type ExpJinScale struct {
	SerialNo    uint64 `json:"serialNo" note:""`
	ScaleName   string `json:"scaleName" note:"问卷名称"`
	LinkedUrl   string `json:"linkedUrl" note:"跳转链接"`
	Description string `json:"description" note:"问卷描述"`
	PhoneField  string `json:"phoneField" note:"电话号码对应的api code"`
}
