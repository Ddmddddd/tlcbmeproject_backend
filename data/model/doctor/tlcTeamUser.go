package doctor

import "github.com/ktpswjz/httpserver/types"

type TlcTeamUser struct {
	SerialNo     uint64      `json:"serialNo" note:"用户小队表的序列号"`
	UserID       uint64      `json:"patientID" note:"用户id"`
	TeamSerialNo uint64      `json:"teamSerialNo" note:"队伍的序列号"`
	JoinTime     *types.Time `json:"joinTime" note:"加入队伍的时间"`
}
