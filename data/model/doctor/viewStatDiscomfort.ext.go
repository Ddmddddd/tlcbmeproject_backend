package doctor

import "github.com/ktpswjz/httpserver/types"

type ViewStatDiscomfortFilter struct {
	PatientID           uint64      `json:"patientID" note:"用户ID 关联患者用户登录表用户ID 例如:232442"`
	HappenDateTimeStart *types.Time `json:"happenDateStart" note:"发生时间 不适情况发生的时间 例如:2018-07-03"`
	HappenDateTimeEnd   *types.Time `json:"happenDateEnd" note:"发生时间 不适情况发生的时间 例如:2018-07-17"`
}
