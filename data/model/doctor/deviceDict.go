package doctor

type DeviceDict struct {
	SerialNo   uint64 `json:"serialNo" note:"序号 主键，自增 例如:324"`
	DeviceCode string `json:"deviceCode" note:"设备代码 设备代码 例如:200101"`
	DeviceName string `json:"deviceName" note:"设备名称  例如:悦奇血压计"`
	DeviceType string `json:"deviceType" note:"设备类别 为以下值之一：血压计、血糖仪、身高体重测量仪 例如:血压计"`
	Memo       string `json:"memo" note:"备注 例如:"`
	OrgCode    string `json:"orgCode" note:"机构代码 关联机构表的代码字段 例如:2894782947239239"`
	IsValid    uint64 `json:"isValid" note:"有效性 用于描述本记录是否有效，0-无效，1-有效，默认值为1 例如:1"`
}