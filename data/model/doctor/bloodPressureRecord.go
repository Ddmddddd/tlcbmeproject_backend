package doctor

import "github.com/ktpswjz/httpserver/types"

type BloodPressureRecord struct {
	SerialNo          uint64      `json:"serialNo" note:"序号 主键，自增 例如:324"`
	PatientID         uint64      `json:"patientID" note:"用户ID 关联患者用户登录表用户ID 例如:232442"`
	SystolicPressure  uint64      `json:"systolicPressure" note:"收缩压 单位：mmHg 例如:110"`
	DiastolicPressure uint64      `json:"diastolicPressure" note:"舒张压 单位：mmHg 例如:78"`
	HeartRate         *uint64     `json:"heartRate" note:"心率 单位：次/分钟 例如:65"`
	MeasureBodyPart   *uint64     `json:"measureBodyPart" note:"测量部位 0-未知，1-左上臂，2-左手腕，3-右上臂，4-右手腕 例如:0"`
	Memo              string      `json:"memo" note:"备注 用于说明测量时的情况，例如有无服药，药品名称和剂量 例如:服用硝苯地平控释片2小时后"`
	MeasurePlace      *uint64     `json:"measurePlace" note:"测量场所 0-未知，1-家里，2-医院，3-药店，9-其他场所 例如:0"`
	TimePoint         string      `json:"timePoint" note:"时间点 早、中、晚 例如:早"`
	MeasureDateTime   *types.Time `json:"measureDateTime" note:"测量时间 测量血压的时间 例如:2018-07-03 14:45:00"`
	InputDateTime     *types.Time `json:"inputDateTime" note:"录入时间 将血压值录入系统的时间 例如:2018-07-03 14:00:00"`
}
