package doctor

type ViewMonthlyReportFilter struct {
	YearMonth string `json:"yearMonth" note:"月份 月报的年份和月份 例如:2018-07"`
}

type ViewMonthlyReportFilterEx struct {
	ViewMonthlyReportFilter

	PatientID uint64 `json:"patientID" note:"用户ID 关联患者用户登录表用户ID 例如:232442"`
}

type RecordDataPoint struct {
	Index float64 `json:"x" note:"曲线x坐标 例如:2"`
	Value float64 `json:"y" note:"曲线y坐标 例如:89"`
	Date  string  `json:"date" note:"数据日期 例如:2018-07-12"`
}

type MonthlyReportForApp struct {
	ViewMonthlyReport

	SBPLine []RecordDataPoint `json:"sbpLine" note:"收缩压曲线"`
	DBPLine []RecordDataPoint `json:"dbpLine" note:"舒张压曲线"`
	HRLine  []RecordDataPoint `json:"hrLine" note:"心率曲线"`
	BMILine []RecordDataPoint `json:"bmiLine" note:"bmi曲线"`
}
