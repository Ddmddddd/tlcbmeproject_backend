package doctor

import "encoding/xml"

type ExpPayToPatient struct {
	PatientID        uint64      `json:"patientID" note:""`
	OpenId           string      `json:"openId" note:""`
}

//付款订单
type WithdrawOrder struct {
	XMLName        xml.Name `xml:"xml"`
	MchAppid       string   `xml:"mch_appid"`
	Mchid          string   `xml:"mchid"`
	DeviceInfo     string   `xml:"device_info"`
	NonceStr       string   `xml:"nonce_str"`
	Sign           string   `xml:"sign"`
	PartnerTradeNo string   `xml:"partner_trade_no"`
	Openid         string   `xml:"openid"`
	CheckName      string   `xml:"check_name"`
	Amount         int      `xml:"amount"`
	Desc           string   `xml:"desc"`
	//SpbillCreateIp string   `xml:"spbill_create_ip"`
}

//付款订单结果
type WithdrawResult struct {
	ReturnCode     string `xml:"return_code"`
	ReturnMsg      string `xml:"return_msg"`
	ResultCode     string `xml:"result_code"`
	ErrCodeDes     string `xml:"err_code_des"`
	PaymentNo      string `xml:"payment_no"`
	PartnerTradeNo string `xml:"partner_trade_no"`
}