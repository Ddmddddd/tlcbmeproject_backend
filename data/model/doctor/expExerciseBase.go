package doctor

type ExpExerciseBase struct {
	SerialNo    uint64 `json:"serialNo" note:""`
	Mode        string `json:"mode" note:"训练板块，例如：热身、正式训练、拉伸活动"`
	Exercise    string `json:"exercise" note:"运动"`
	Target      string `json:"target" note:"目标肌群"`
	AdviceTimes string `json:"adviceTimes" note:"建议次数"`
	Purpose     string `json:"purpose" note:"动作目的"`
	Media       string `json:"media" note:"视频链接"`
}
