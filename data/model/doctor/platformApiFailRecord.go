package doctor

import "github.com/ktpswjz/httpserver/types"

type PlatformApiFailRecord struct {
	SerialNo     uint64      `json:"serialNo" note:"序号 主键，自增 例如:324"`
	PatientID    uint64      `json:"patientID" note:"用户ID 关联患者用户登录表用户ID 例如:232442"`
	BusinessName string      `json:"businessName" note:"业务名称 接口服务名称，如数据报送等 例如:数据报送"`
	RepeatCount  uint64      `json:"repeatCount" note:"重试次数 尝试调用次数 例如:0"`
	ErrMsg       string      `json:"errMsg" note:"错误信息 错误信息 例如:0"`
	DateTime     *types.Time `json:"dateTime" note:"调用时间 接口调用日期及时间 例如:2018-08-04 18:23:53"`
	Uri          string      `json:"uri" note:"地址 调用接口地址 例如:/1.0/patient/regpatient"`
	Input        []byte      `json:"input" note:"输入参数 输入数据 例如:{json}"`
}
