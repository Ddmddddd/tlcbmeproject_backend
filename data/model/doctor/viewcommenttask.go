package doctor

import "github.com/ktpswjz/httpserver/types"

type ViewCommentTask struct {
	SerialNo          *uint64     `json:"serialNo" note:""`
	PatientID         *uint64     `json:"patientID" note:"患者id"`
	Name 			  string      `json:"name" note:"患者姓名"`
	TaskID            *uint64     `json:"taskID" note:"对应taskRepository中的serialNo，为0则代表该任务不是按期完成的"`
	ParentTaskName    string      `json:"parentTaskName" note:"父任务名"`
	TaskName          string      `json:"taskName" note:"任务名"`
	CreateDateTime    *types.Time `json:"createDateTime" note:"创建时间"`
	Record            interface{} `json:"record" note:"任务完成具体记录"`
	DoctorID          *uint64     `json:"doctorID" note:"医生ID 例如:232"`
	DoctorName        string      `json:"doctorName" note:"医生姓名 例如:张三"`
	HealthManagerID   *uint64     `json:"healthManagerID" note:"健康管理师ID 例如:232"`
	HealthManagerName string      `json:"healthManagerName" note:"健康管理师姓名 例如:李四"`
}
