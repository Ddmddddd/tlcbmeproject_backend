package doctor

type FollowupPlanIgnore struct {
	SerialNo     uint64 `json:"serialNo" note:"序号 主键，自增 例如:324"`
	PatientID    uint64 `json:"patientID" note:"用户ID 关联患者用户登录表用户ID 例如:232442"`
	FollowUpType string `json:"followUpType" note:"随访类型 随访的类型（原因） 例如:三个月例行随访"`
	StatusMemo   string `json:"statusMemo" note:"状态备注 例如当状态为忽略时，此处填写忽略的原因。 例如:"`
}
