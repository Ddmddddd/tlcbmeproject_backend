package doctor

import "github.com/ktpswjz/httpserver/types"

type ViewMonthlyReport struct {
	PatientID           uint64      `json:"patientID" note:"用户ID 关联患者用户登录表用户ID 例如:232442"`
	YearMonth           string      `json:"yearMonth" note:"月份 年份+月份（2016-09） 例如:2016-09"`
	BpMeasuredTimes     uint64      `json:"bpMeasuredTimes" note:"血压实测次数  例如:12"`
	BpPlanedTimes       uint64      `json:"bpPlanedTimes" note:"血压应测次数  例如:90"`
	WeightMeasuredTimes uint64      `json:"weightMeasuredTimes" note:"体重实测次数  例如:20"`
	WeightPlanedTimes   uint64      `json:"weightPlanedTimes" note:"体重应测次数  例如:30"`
	DrugTakenTimes      uint64      `json:"drugTakenTimes" note:"实际服药次数  例如:20"`
	DrugPlanedTimes     uint64      `json:"drugPlanedTimes" note:"计划服药次数  例如:30"`
	HrMeasuredTimes     uint64      `json:"hrMeasuredTimes" note:"心率实测次数  例如:12"`
	DaysInManagement    uint64      `json:"daysInManagement" note:"加入管理天数  例如:1002"`
	CurrentBP           string      `json:"currentBP" note:"当前血压 当月最后一次血压 例如:132/82"`
	GoalBP              string      `json:"goalBP" note:"目标血压 控制血压的目标 例如:140/90"`
	MaxSBP              *uint64     `json:"maxSBP" note:"最大收缩压 当月出现的最大收缩压 例如:156"`
	MaxSBPTime          *types.Time `json:"maxSBPTime" note:"最大收缩压时间 当月出现最大收缩压的时间 例如:2016-09-10 12:00:00"`
	MinSBP              *uint64     `json:"minSBP" note:"最小收缩压 当月出现的最小收缩压 例如:124"`
	MinSBPTime          *types.Time `json:"minSBPTime" note:"最小收缩压时间 当月出现最小收缩压的时间 例如:2016-09-11 12:00:00"`
	MaxDBP              *uint64     `json:"maxDBP" note:"最大舒张压 当月出现的最大舒张压 例如:102"`
	MaxDBPTime          *types.Time `json:"maxDBPTime" note:"最大舒张压时间 当月出现最大舒张压的时间 例如:2016-09-12 12:00:00"`
	MinDBP              *uint64     `json:"minDBP" note:"最小舒张压 当月出现的最小舒张压 例如:83"`
	MinDBPTime          *types.Time `json:"minDBPTime" note:"最小舒张压时间 当月出现最小舒张压的时间 例如:2016-09-13 12:00:00"`
	MaxRange            *uint64     `json:"maxRange" note:"最大脉压差 当月出现的最大脉压差 例如:48"`
	MaxRangeTime        *types.Time `json:"maxRangeTime" note:"最大脉压差时间 当月出现最大脉压差的时间 例如:2016-09-14 12:00:00"`
	MinRange            *uint64     `json:"minRange" note:"最小脉压差 当月出现的最小脉压差 例如:26"`
	MinRangeTime        *types.Time `json:"minRangeTime" note:"最小脉压差时间 当月出现最小脉压差的时间 例如:2016-09-15 12:00:00"`
	AvgSBP              *uint64     `json:"avgSBP" note:"平均收缩压 当月的平均收缩压 例如:135"`
	AvgDBP              *uint64     `json:"avgDBP" note:"平均舒张压 当月的平均舒张压 例如:98"`
	AvgRange            *uint64     `json:"avgRange" note:"平均脉压差 当月的平均脉压差 例如:35"`
	BpNormalTimes       *uint64     `json:"bpNormalTimes" note:"血压正常次数 当月正常血压的次数 例如:6"`
	BpHigherTimes       *uint64     `json:"bpHigherTimes" note:"血压偏高次数 当月偏高血压的次数 例如:4"`
	BpLowerTimes        *uint64     `json:"bpLowerTimes" note:"血压偏低次数 当月偏低血压的次数 例如:2"`
	MaxHR               *uint64     `json:"maxHR" note:"最大心率 当月出现的最大心率值 例如:108"`
	MaxHRTime           *types.Time `json:"maxHRTime" note:"最大心率时间 当月出现最大心率值的时间 例如:2016-09-10 12:00:00"`
	MinHR               *uint64     `json:"minHR" note:"最小心率 当月出现的最小心率值 例如:56"`
	MinHRTime           *types.Time `json:"minHRTime" note:"最小心率时间 当月出现最小心率值的时间 例如:2016-09-11 12:00:00"`
	AvgHR               *uint64     `json:"avgHR" note:"平均心率 当月的平均心率值 例如:68"`
	HrNormalTimes       *uint64     `json:"hrNormalTimes" note:"心率正常次数 当月正常心率的次数 例如:6"`
	HrHigherTimes       *uint64     `json:"hrHigherTimes" note:"心率偏高次数 当月偏高心率的次数 例如:4"`
	HrLowerTimes        *uint64     `json:"hrLowerTimes" note:"心率偏低次数 当月偏低心率的次数 例如:2"`
	CurrentHeight       *uint64     `json:"currentHeight" note:"当前身高 当月的身高 例如:170"`
	CurrentWeight       *float64    `json:"currentWeight" note:"当前体重 当月最后一次体重 例如:60.5"`
	TargetWeight        *float64    `json:"targetWeight" note:"目标体重 当月目标的体重 例如:65"`
	CurrentBMI          *float64    `json:"currentBMI" note:"当前BMI 当月最后一次体重对应的BMI值 例如:22.6"`
}
