package doctor

import "github.com/ktpswjz/httpserver/types"

type TrendLevelDm struct {
	DateStart types.Time `json:"dateStart" note:"开始日期"`
	DateEnd   types.Time `json:"dateEnd" note:"结束日期"`

	Levels    []*TrendLevelDmLevel    `json:"levels" note:"等级记录"`
	Followups []*TrendLevelDmFollowup `json:"followups" note:"随访记录"`
}

type TrendLevelDmLevel struct {
	Level         uint64     `json:"level" note:"管理等级 当前管理等级，0-新患者，1-一级管理，2-二级管理，9-终止管理 例如:0"`
	StartDateTime types.Time `json:"startDateTime" note:"管理等级开始时间 当前管理等级开始时间 例如:2018-07-03 13:15:00"`
}

type TrendLevelDmFollowup struct {
	Status           uint64     `json:"status" note:"结果状态 用于描述本次随访是否有效，0-失访，1-进行中，2-有效，默认值为1 例如:1"`
	FollowupDateTime types.Time `json:"followupDateTime" note:"随访时间  例如:2018-08-01 10:00:00"`
}

type TrendLevelDmFilter struct {
	PatientID     uint64 `json:"patientID" note:"用户ID 关联患者用户登录表用户ID 例如:232442"`
	MonthAgo      int64  `json:"monthAgo" note:"从几个月之前开始，0表示全部"`
	AutoDateRange bool   `json:"autoDateRange" note:"是否自动计算日期范围：true开始时间从第一个数据的时间开始，false开始时间为查询的范围开始日期"`
}
