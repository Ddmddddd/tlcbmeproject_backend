package doctor

import (
	"github.com/ktpswjz/httpserver/types"
)

type DietRecordEx struct {
	SerialNo      uint64         `json:"serialNo" note:"序号\\n324\\nFCM\\n主键，自增"`
	EatDateTime   *types.Time    `json:"eatDateTime" note:"用餐日期\n2018-07-03 \nFCM\n用餐的实际日期"`
	InputDateTime *types.Time    `json:"inputDateTime" note:"录入时间\n2018-07-03 14:00:00\nFCM\n将用药情况录入系统的时间"`
	DietType      string         `json:"dietType" note:"用餐类型 早餐、中餐、晚餐"`
	PatientID     uint64         `json:"patientID" note:"用户ID"`
	DietContents  []*DietContent `json:"dietContents" note:"三餐具体内容 例如50ml 牛奶"`
}

type NutrientAnalysisFilter struct {
	PatientID        uint64      `json:"patientID" note:"用户ID"`
	AnalysisDateTime *types.Time `json:"analysisDateTime" note:"分析日期"`
}

type NutrientAnalysisFilterEx struct {
	NutrientAnalysisFilter
	DateType *uint64 `json:"type"`
}

type NutrientAnalysisResult struct {
	AllIntake       *Intake `json:"allIntake" note:"总量" `
	BreakfastIntake *Intake `json:"breakfastIntake" note:"早餐总量" `
	LunchIntake     *Intake `json:"lunchIntake" note:"午餐总量" `
	DinnerIntake    *Intake `json:"dinnerIntake" note:"晚餐总量" `

	Grade      float64  `json:"grade" note:"营养评分"`
	Conclusion []string `json:"conclusion" note:"结论"`
	// 限制性
	Na NumberCompare `json:"na" note:"钠"`

	// 推荐 g
	DF    NumberCompare `json:"DF" note:"膳食纤维"`
	CA    NumberCompare `json:"CA" note:"钙"`
	POT   NumberCompare `json:"POT" note:"钾"`
	MG    NumberCompare `json:"MG" note:"镁"`
	FE    NumberCompare `json:"FE" note:"铁"`
	ZN    NumberCompare `json:"ZN" note:"锌"`
	VITA  NumberCompare `json:"VITA" note:"维生素A"`
	VITB1 NumberCompare `json:"VITB1" note:"维生素B1"`
	VITB2 NumberCompare `json:"VITB2" note:"维生素B2"`
	VITC  NumberCompare `json:"VITC" note:"维生素C"`
	VITD  NumberCompare `json:"VITD" note:"维生素D"`

	// 其他
	CHO    NumberCompare `json:"CHO" note:"胆固醇"`
	USP    NumberCompare `json:"USP" note:"叶酸"`
	IODINE NumberCompare `json:"IODINE" note:"碘"`
	SE     NumberCompare `json:"SE" note:"硒"`
	VITB6  NumberCompare `json:"VITB6" note:"维生素B6"`
	VITE   NumberCompare `json:"VITE" note:"维生素E"`

	FoodKindWeight FoodKindWeight `json:"foodKindWeight"`

	GradeList []*Grade `json:"gradeList"`

	EnergyList []*Grade `json:"energyList"`
	// 只有推荐值
	Status bool `json:"status"`
	DietPlan *DietPlanRecord   `json:"dietPlan"`
}

type DietPlanRecord struct {
	Grain		uint64		`json:"grain" note:"这一类完成该目标的次数"`
	Vegetable	uint64 		`json:"vegetable"`
	Fruit 		uint64 		`json:"fruit"`
	Oil 		uint64		`json:"oil"`
	Sweet		uint64		`json:"sweet"`
	Drink 		uint64		`json:"drink"`
	Pickle		uint64		`json:"pickle"`
	Milk		uint64		`json:"milk"`
	Fish        uint64		`json:"fish"`
	Bean		uint64		`json:"bean"`
	Mushroom	uint64		`json:"mushroom"`
	Water       uint64		`json:"water"`
	Place       uint64		`json:"place"`
}

type Intake struct {
	Energy       NumberCompare `json:"energy" note:"能量"`
	Protein      NumberCompare `json:"protein" note:"蛋白质"`
	Fat          NumberCompare `json:"fat" note:"脂肪"`
	Carbohydrate NumberCompare `json:"carbohydrate" note:"碳水化合物"`
}

type NumberCompare struct {
	Actual       float64 `json:"actual" note:"实际摄入"`
	RecommendMin float64 `json:"recommendFatMin" note:"推荐摄入最小值"`
	RecommendMax float64 `json:"recommendFatMax" note:"推荐摄入最大值"`
}

type DietRecordFilter struct {
	SerialNo uint64 `json:"serialNo" note:"序号\\n324\\nFCM\\n主键，自增"`
}

type Grade struct {
	DateTime *types.Time `json:"dateTime"`
	Value    float64     `json:"value"`
}

type DietRecordTotalForApp struct {
	SerialNo      uint64         `json:"serialNo" note:"序号\\n324\\nFCM\\n主键，自增"`
	EatDateTime   *types.Time    `json:"happenDateTime" note:"用餐日期\n2018-07-03 \nFCM\n用餐的实际日期"`
	InputDateTime *types.Time    `json:"createDateTime" note:"录入时间\n2018-07-03 14:00:00\nFCM\n将用药情况录入系统的时间"`
	DietType      string         `json:"dietType" note:"用餐类型 早餐、中餐、晚餐"`
	PatientID     uint64         `json:"patientID" note:"用户ID"`
	DietContents  []*DietContent `json:"dietContents" note:"三餐具体内容 例如50ml 牛奶"`
	Place         string         `json:"happenPlace" note:"用餐地点"`
	Memo          string         `json:"memo" note:"用餐说明"`
	Photo         string         `json:"photo" note:"照片列表"`
	DietSerialNo  uint64		 `json:"dietSerialNo" note:"如果是修改的 用餐记录序列"`
	RecordSerialNo uint64		 `json:"recordSerialNo" note:"如果是修改的 用餐记录序列2"`
}

type DietRecordTotalDeleteForApp struct {
	PatientID     uint64         `json:"patientID" note:"用户ID"`
	DietSerialNo  uint64		 `json:"dietSerialNo" note:"用餐记录序列"`
	RecordSerialNo uint64		 `json:"recordSerialNo" note:"用餐记录序列2"`
}