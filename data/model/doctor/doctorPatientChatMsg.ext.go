package doctor

import "github.com/ktpswjz/httpserver/types"

type DoctorPatientChatMsgData struct {
	ReceiverID  uint64      `json:"receiverID" note:"接受者ID 关联患者或医生用户登录表用户ID 例如:232442"`
	MsgContent  string      `json:"msgContent" note:"消息内容 医患沟通的消息内容 例如:你好"`
	MsgDateTime *types.Time `json:"msgDateTime" note:"消息时间 消息发生的时间 例如:2018-07-03 14:45:00"`
}

type DoctorPatientChatMsgReadData struct {
	ReceiverID   uint64   `json:"receiverID" note:"接受者ID 关联患者或医生用户登录表用户ID 例如:232442"`
	MsgSerialNos []uint64 `json:"msgSerialNos" note:"序号 主键，自增 例如:[324, 423]"`
}

type DoctorPatientChatMsgRead struct {
	SenderID     uint64   `json:"senderID" note:"发送者ID 关联患者或医生用户登录表用户ID 例如:232442"`
	ReceiverID   uint64   `json:"receiverID" note:"接受者ID 关联患者或医生用户登录表用户ID 例如:232442"`
	MsgSerialNos []uint64 `json:"msgSerialNos" note:"序号 主键，自增 例如:[324, 423]"`
}

type ChatDoctorWithMsgInfoForApp struct {
	UserID          uint64      `json:"id" note:"用户ID 例如:"`
	Name            string      `json:"name" note:"姓名 例如:张三"`
	UnReadMsgList   []uint64    `json:"unReadMsgList" note:"未读消息列表 例如:[11,12]"`
	LastMsgContent  string      `json:"lastMsgContent" note:"最近一条消息的内容 例如:你好"`
	LastMsgDateTime *types.Time `json:"lastMsgDateTime" note:"最近一条消息的时间 例如:2019-06-05 08:00:00"`
}

type ChatMsgDoctorID struct {
	DoctorID uint64 `json:"doctorID" note:"医生用户ID 例如:13"`
}

type ChatMsgUserInfo struct {
	PatientID uint64 `json:"patientID" note:"患者用户ID 例如:12"`
	DoctorID  uint64 `json:"doctorID" note:"医生用户ID 例如:13"`
}

type DoctorPatientChatID struct {
	SenderID   uint64 `json:"senderID" note:"发送者ID 关联患者或医生用户登录表用户ID 例如:232442"`
	ReceiverID uint64 `json:"receiverID" note:"接受者ID 关联患者或医生用户登录表用户ID 例如:232442"`
}

type ChatMsgPatientID struct {
	PatientID uint64 `json:"patientID" note:"患者用户ID 例如:12"`
}

type ChatUnReadMsgCountFilter struct {
	ReceiverID uint64 `json:"receiverID" note:"接受者ID 关联患者或医生用户登录表用户ID 例如:232442"`
	MsgFlag    uint64 `json:"msgFlag" note:"消息标识 0-未读，1-已读 例如:0"`
}

type DoctorPatienLastChatMsg struct {
	SerialNo    uint64      `json:"serialNo" note:"序号 主键，自增 例如:324"`
	PatientName string      `json:"patientName" note:"患者姓名"`
	PatientID   uint64      `json:"patientID" note:"患者ID 登录表用户ID 例如:232442"`
	MsgContent  string      `json:"msgContent" note:"消息内容 医患沟通的消息内容 例如:你好"`
	MsgDateTime *types.Time `json:"msgDateTime" note:"消息时间 消息发生的时间 例如:2018-07-03 14:45:00"`
	Sex         *uint64     `json:"sex" note:"患者性别 1:男 2：女 例如:1"`
	Count       uint64      `json:"unReadCount" note:"未读消息数目 例如:3"`
}

type DoctorMsgCount struct {
	// 未读消息总数  例如:324
	Count *uint64
	// 患者ID  例如:232442
	PatientID *uint64
	// 接受者ID 关联患者或医生用户登录表用户ID 例如:232442
	ReceiverID *uint64
}
