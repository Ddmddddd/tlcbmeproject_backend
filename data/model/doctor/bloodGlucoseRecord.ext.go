package doctor

import "github.com/ktpswjz/httpserver/types"

type BloodGlucoseRecordEx struct {
	BloodGlucoseRecord
}

type BloodGlucoseRecordTrend struct {
	BloodType       string      `json:"bloodType" note:"血样类型 毛细血管血、静脉血、动脉血、新生儿血 例如:毛细血管血"`
	BloodGlucose    float64     `json:"bloodGlucose" note:"血糖值 单位：mmol/L 例如:6.1"`
	TimePoint       string      `json:"timePoint" note:"测量时间点 空腹、早餐前、早餐后2h、午餐前、午餐后2h、晚餐前、晚餐后2h、睡前、夜间 例如:空腹"`
	MeasureDateTime *types.Time `json:"measureDateTime" note:"测量时间 测量血糖时的时间 例如:2018-07-03 14:45:00"`
}

type BloodGlucoseRecordCreate struct {
	BloodType       string      `json:"bloodType" note:"血样类型 毛细血管血、静脉血、动脉血、新生儿血 例如:毛细血管血"`
	BloodGlucose    float64     `json:"bloodGlucose" note:"血糖值 单位：mmol/L 例如:6.1"`
	BloodKetone     *float64    `json:"bloodKetone" note:"血酮值 单位：mmol/L 例如:6.1"`
	TimePoint       string      `json:"timePoint" note:"测量时间点 空腹、早餐前、早餐后2h、午餐前、午餐后2h、晚餐前、晚餐后2h、睡前、夜间 例如:空腹"`
	Memo            string      `json:"memo" note:"备注  例如:最近在口服降糖药"`
	MeasureDateTime *types.Time `json:"measureDateTime" note:"测量时间 测量血糖时的时间 例如:2018-07-03 14:45:00"`
}

type BloodGlucoseRecordCreateEx struct {
	BloodGlucoseRecordCreate
	PatientID uint64 `json:"patientID" note:"用户ID 关联患者用户登录表用户ID 例如:232442"`
}

type BloodGlucoseRecordFilter struct {
	SerialNo         *uint64     `json:"serialNo" note:"序号 主键，自增 例如:324"`
	PatientID        *uint64     `json:"patientID" note:"用户ID 关联患者用户登录表用户ID 例如:232442"`
	BloodType        *string     `json:"bloodType" note:"血样类型 毛细血管血、静脉血、动脉血、新生儿血 例如:毛细血管血"`
	TimePoints       []*string   `json:"timePoints" note:"测量时间点 空腹、早餐前、早餐后2h、午餐前、午餐后2h、晚餐前、晚餐后2h、睡前、夜间 例如:空腹"`
	MeasureDateStart *types.Time `json:"measureDateStart" note:"测量开始日期 例如:2018-07-03"`
	MeasureDateEnd   *types.Time `json:"measureDateEnd" note:"测量结束日期 例如:2018-08-03"`
}

type BloodGlucoseRecordForApp struct {
	SerialNo        uint64      `json:"serialNo" note:"序号 主键，自增 例如:324"`
	BloodGlucose    float64     `json:"bloodGlucose" required:"true" note:"血糖值 单位：mmol/L 例如:6.1"`
	MeasureDateTime *types.Time `json:"measureDateTime" note:"测量时间 测量血糖的时间 例如:2018-07-03 14:45:00"`
	Memo            string      `json:"memo" note:"备注  例如:最近在口服降糖药"`
	TimePoint       string      `json:"timePoint" note:"测量时刻 测量血糖的时刻 例如:1晨起空腹 2早餐后 3午餐前 4午餐后 5晚餐前 6晚餐后 7睡前 8凌晨"`
}

type BloodGlucoseRecordForAppEx struct {
	BloodGlucoseRecordForApp

	PatientID uint64 `json:"patientID" required:"true" note:"用户ID 关联患者用户登录表用户ID 例如:232442"`
}

type BloodGlucoseRecordDataFilter struct {
	MeasureStartDate *types.Time `json:"measureStartDate" note:"测量开始时间 测量血糖的时间 例如:2018-07-03 14:45:00"`
	MeasureEndDate   *types.Time `json:"measureEndDate" note:"测量结束时间 测量血糖的时间 例如:2018-08-03 14:45:00"`
}

type BloodGlucoseRecordDataFilterEx struct {
	BloodGlucoseRecordDataFilter

	PatientID uint64 `json:"patientID" required:"true" note:"用户ID 关联患者用户登录表用户ID 例如:232442"`
}
