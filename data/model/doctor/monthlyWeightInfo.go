package doctor

type MonthlyWeightInfo struct {
	SerialNo      uint64  `json:"serialNo" note:"序号 主键，自增 例如:324"`
	PatientID     uint64  `json:"patientID" note:"用户ID 关联患者用户登录表用户ID 例如:232442"`
	YearMonth     string  `json:"yearMonth" note:"月份 年份+月份（2016-09） 例如:2016-09"`
	CurrentHeight *uint64 `json:"currentHeight" note:"当前身高 当月的身高 例如:170"`
	CurrentWeight float64 `json:"currentWeight" note:"当前体重 当月最后一次体重 例如:60.5"`
	TargetWeight  float64 `json:"targetWeight" note:"目标体重 当月目标的体重 例如:65"`
	CurrentBMI    float64 `json:"currentBMI" note:"当前BMI 当月最后一次体重对应的BMI值 例如:22.6"`
}
