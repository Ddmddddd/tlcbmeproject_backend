package doctor

type SportItem struct {
	SerialNo  uint64  `json:"serialNo" note:"序号"`
	Type      string  `json:"type" note:"运动项目分类"`
	Name      string  `json:"name" note:"运动项目名称"`
	Intensity string  `json:"intensity" note:"运动强度"`
	Met       float64 `json:"met" note:"身体活动强度，结合体重、运动时长来计算能量消耗"`
}
