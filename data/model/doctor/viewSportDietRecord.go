package doctor

type ViewSportDietRecord struct {
	PatientID     uint64 `json:"patientID" note:"用户ID 关联患者用户登录表用户ID 例如:232442"`
	HappenDate    string `json:"happenDate" note:""`
	DietBreakfast string `json:"dietBreakfast" note:""`
	DietLunch     string `json:"dietLunch" note:""`
	DietDinner    string `json:"dietDinner" note:""`
	DietOther     string `json:"dietOther" note:""`
	Sport         string `json:"sport" note:""`
}
