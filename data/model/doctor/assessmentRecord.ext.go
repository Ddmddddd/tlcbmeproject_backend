package doctor

import "github.com/ktpswjz/httpserver/types"

type AssessmentSheet struct {
	SheetCode    uint64 `json:"sheetCode" note:"评估表代码 关联评估表字典代码字段 例如:1"`
	SheetVersion uint64 `json:"sheetVersion" note:"评估表版本号 例如:1"`
}

type AssessmentRecordCreate struct {
	PatientID              uint64      `json:"patientID" required:"true" note:"用户ID 关联患者用户登录表用户ID 例如:232442"`
	AssessmentName         string      `json:"assessmentName" note:"评估名称  例如:高血压危险分层评估"`
	AssessmentSheetCode    uint64      `json:"assessmentSheetCode" note:"评估表代码 关联评估表字典代码字段 例如:1"`
	AssessmentSheetVersion uint64      `json:"assessmentSheetVersion" note:"评估表版本号 例如:1"`
	Content                interface{} `json:"content" required:"true" note:"评估表内容 JSON格式保存 例如:"`
	Others                 interface{} `json:"others"`
}

type AssessmentRecordCreateEx struct {
	AssessmentRecordCreate

	OperatorID uint64      `json:"operatorID" note:"操作者ID 保存用户ID，如果是后台服务自动做的评估，这里填0 例如:322"`
	Summary    string      `json:"summary" note:"评估摘要 摘要规则根据所用评估表不同而不同，一般此处填入评估结果。 例如:中危"`
	Others     interface{} `json:"others" note:"评估结果其他信息。"`
	Weight     float64     `json:"weight"`
	Height     uint64      `json:"height"`
	Waistline  float64     `json:"waistline"`
}

type AssessmentRecordCreateResult struct {
	SerialNo       uint64      `json:"serialNo" note:"序号 主键，自增 例如:324"`
	PatientID      uint64      `json:"patientID" note:"用户ID 关联患者用户登录表用户ID 例如:232442"`
	AssessDateTime *types.Time `json:"assessDateTime" note:"评估时间  例如:2018-08-01 10:00:00"`
	Summary        string      `json:"summary" note:"评估摘要 摘要规则根据所用评估表不同而不同，一般此处填入评估结果。 例如:中危"`
	Others         interface{} `json:"others"`
}

type AssessmentRecordUpdate struct {
	SerialNo uint64 `json:"serialNo" note:"序号 主键，自增 例如:324"`
	Summary  string `json:"summary" note:"评估摘要 摘要规则根据所用评估表不同而不同，一般此处填入评估结果。 例如:中危"`
}

type AssessmentRecordFilter struct {
	SerialNo        *uint64     `json:"serialNo" note:"序号 主键，自增 例如:324"`
	PatientID       *uint64     `json:"patientID" note:"用户ID 关联患者用户登录表用户ID 例如:232442"`
	AssessDateStart *types.Time `json:"assessDateStart" note:"评估开始日期  例如:2018-08-01"`
	AssessDateEnd   *types.Time `json:"assessDateEnd" note:"评估结束日期  例如:2018-09-01"`
	AssessmentName  string      `json:"assessmentName" note:"评估名称  例如:高血压危险分层评估"`
	Summary         string      `json:"summary" note:"评估摘要 摘要规则根据所用评估表不同而不同，一般此处填入评估结果。 例如:中危"`
}

type AssessmentRecordListItem struct {
	SerialNo               uint64      `json:"serialNo" note:"序号 主键，自增 例如:324"`
	PatientID              uint64      `json:"patientID" note:"用户ID 关联患者用户登录表用户ID 例如:232442"`
	AssessDateTime         *types.Time `json:"assessDateTime" note:"评估时间  例如:2018-08-01 10:00:00"`
	AssessmentName         string      `json:"assessmentName" note:"评估名称  例如:高血压危险分层评估"`
	AssessmentSheetCode    uint64      `json:"assessmentSheetCode" note:"评估表代码 关联评估表字典代码字段 例如:1"`
	AssessmentSheetVersion uint64      `json:"assessmentSheetVersion" note:"评估表版本号 例如:1"`
	Summary                string      `json:"summary" note:"评估摘要 摘要规则根据所用评估表不同而不同，一般此处填入评估结果。 例如:中危"`
	Others                 interface{} `json:"others"`
}
