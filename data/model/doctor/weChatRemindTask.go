package doctor

import "github.com/ktpswjz/httpserver/types"

type WeChatRemindTask struct {
	SerialNo       uint64      `json:"serialNo" note:"序号"`
	PatientID      uint64      `json:"patientID" note:"患者id"`
	OpenID         string      `json:"openID" note:"患者openid，在微信提醒时需要用到"`
	TemplateID     string      `json:"templateID" note:"微信提醒的template_id，在微信提醒时要用到"`
	CreateDateTime *types.Time `json:"createDateTime" note:"创建时间"`
	Status         uint64      `json:"status" note:"状态，0-正常，1-弃用"`
	Type           uint64      `json:"type" note:"提醒内容，1-血压，2-血糖"`
}
