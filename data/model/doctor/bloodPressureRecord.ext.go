package doctor

import (
	"github.com/ktpswjz/httpserver/types"
	"time"
)

type BloodPressureRecordCreate struct {
	SystolicPressure  uint64      `json:"systolicPressure" required:"true" note:"收缩压 单位：mmHg 例如:110"`
	DiastolicPressure uint64      `json:"diastolicPressure" required:"true" note:"舒张压 单位：mmHg 例如:78"`
	HeartRate         *uint64     `json:"heartRate" note:"心率 单位：次/分钟 例如:65"`
	MeasureBodyPart   *uint64     `json:"measureBodyPart" note:"测量部位 0-未知，1-左上臂，2-左手腕，3-右上臂，4-右手腕 例如:0"`
	Memo              string      `json:"memo" note:"备注 用于说明测量时的情况，例如有无服药，药品名称和剂量 例如:服用硝苯地平控释片2小时后"`
	MeasurePlace      *uint64     `json:"measurePlace" note:"测量场所 0-未知，1-家里，2-医院，3-药店，9-其他场所 例如:0"`
	MeasureDateTime   *types.Time `json:"measureDateTime" note:"测量时间 测量血压的时间 例如:2018-07-03 14:45:00"`
}

type BloodPressureRecordCreateEx struct {
	BloodPressureRecordCreate

	PatientID uint64 `json:"patientID" required:"true" note:"用户ID 关联患者用户登录表用户ID 例如:232442"`
}

type BloodPressureRecordData struct {
	SystolicPressure  uint64      `json:"systolicPressure" required:"true" note:"收缩压 单位：mmHg 例如:110"`
	DiastolicPressure uint64      `json:"diastolicPressure" required:"true" note:"舒张压 单位：mmHg 例如:78"`
	HeartRate         *uint64     `json:"heartRate" note:"心率 单位：次/分钟 例如:65"`
	MeasureDateTime   *types.Time `json:"measureDateTime" note:"测量时间 测量血压的时间 例如:2018-07-03 14:45:00"`
	Memo              string      `json:"memo" note:"备注 用于说明测量时的情况，例如有无服药，药品名称和剂量 例如:服用硝苯地平控释片2小时后"`
}

type BloodPressureRecordForApp struct {
	SerialNo          uint64      `json:"serialNo" note:"序号 主键，自增 例如:324"`
	SystolicPressure  uint64      `json:"systolicPressure" required:"true" note:"收缩压 单位：mmHg 例如:110"`
	DiastolicPressure uint64      `json:"diastolicPressure" required:"true" note:"舒张压 单位：mmHg 例如:78"`
	HeartRate         *uint64     `json:"heartRate" note:"心率 单位：次/分钟 例如:65"`
	MeasureDateTime   *types.Time `json:"measureDateTime" note:"测量时间 测量血压的时间 例如:2018-07-03 14:45:00"`
	Memo              string      `json:"memo" note:"备注 用于说明测量时的情况，例如有无服药，药品名称和剂量 例如:服用硝苯地平控释片2小时后"`
	Type              uint64      `json:"type" note:"测量时刻 测量血压的时刻 例如:1早上 2中午 3晚上 0自定义"`
}

type BloodPressureRecordForAppEx struct {
	BloodPressureRecordForApp

	PatientID uint64 `json:"patientID" required:"true" note:"用户ID 关联患者用户登录表用户ID 例如:232442"`
}

func (s *BloodPressureRecordData) GetMin(v *BloodPressureRecordData) {
	if v == nil {
		return
	}

	if s.SystolicPressure > v.SystolicPressure {
		s.SystolicPressure = v.SystolicPressure
	}
	if s.DiastolicPressure > v.DiastolicPressure {
		s.DiastolicPressure = v.DiastolicPressure
	}
	if v.HeartRate != nil {
		heartRate := *v.HeartRate
		if s.HeartRate == nil {
			s.HeartRate = &heartRate
		} else {
			if *s.HeartRate > heartRate {
				s.HeartRate = &heartRate
			}
		}
	}
	if v.MeasureDateTime != nil {
		measureDateTime := *v.MeasureDateTime
		if s.MeasureDateTime == nil {
			s.MeasureDateTime = &measureDateTime
		} else {
			vt := time.Time(measureDateTime)
			st := time.Time(*s.MeasureDateTime)
			if st.After(vt) {
				s.MeasureDateTime = &measureDateTime
			}
		}
	}
}

func (s *BloodPressureRecordData) GetMax(v *BloodPressureRecordData) {
	if v == nil {
		return
	}

	if s.SystolicPressure < v.SystolicPressure {
		s.SystolicPressure = v.SystolicPressure
	}
	if s.DiastolicPressure < v.DiastolicPressure {
		s.DiastolicPressure = v.DiastolicPressure
	}
	if v.HeartRate != nil {
		heartRate := *v.HeartRate
		if s.HeartRate == nil {
			s.HeartRate = &heartRate
		} else {
			if *s.HeartRate < heartRate {
				s.HeartRate = &heartRate
			}
		}
	}
	if v.MeasureDateTime != nil {
		measureDateTime := *v.MeasureDateTime
		if s.MeasureDateTime == nil {
			s.MeasureDateTime = &measureDateTime
		} else {
			vt := time.Time(measureDateTime)
			st := time.Time(*s.MeasureDateTime)
			if st.Before(vt) {
				s.MeasureDateTime = &measureDateTime
			}
		}
	}
}

type BloodPressureRecordDataFilter struct {
	MeasureStartDate *types.Time `json:"measureStartDate" note:"测量开始时间 测量血压的时间 例如:2018-07-03 14:45:00"`
	MeasureEndDate   *types.Time `json:"measureEndDate" note:"测量结束时间 测量血压的时间 例如:2018-08-03 14:45:00"`
}

type BloodPressureRecordDataFilterEx struct {
	BloodPressureRecordDataFilter

	PatientID uint64 `json:"patientID" required:"true" note:"用户ID 关联患者用户登录表用户ID 例如:232442"`
}

type BloodPressureRecordDataStat struct {
	Min   *BloodPressureRecordData   `json:"min" note:"最小值"`
	Max   *BloodPressureRecordData   `json:"max" note:"最大值"`
	Items []*BloodPressureRecordData `json:"items" note:"数据项"`
}

type BloodPressureRecordTrend struct {
	SystolicPressure  uint64      `json:"systolicPressure" note:"收缩压 单位：mmHg 例如:110"`
	DiastolicPressure uint64      `json:"diastolicPressure" note:"舒张压 单位：mmHg 例如:78"`
	HeartRate         *uint64     `json:"heartRate" note:"心率 单位：次/分钟 例如:65"`
	MeasureDateTime   *types.Time `json:"measureDateTime" note:"测量时间 测量血压的时间 例如:2018-07-03 14:45:00"`
}

type BloodPressureRecordTrendStat struct {
	SystolicPressureMax  uint64                      `json:"systolicPressureMax" note:"收缩压 单位：mmHg 例如:110"`
	SystolicPressureMin  uint64                      `json:"systolicPressureMin" note:"收缩压 单位：mmHg 例如:110"`
	DiastolicPressureMax uint64                      `json:"diastolicPressureMax" note:"舒张压 单位：mmHg 例如:78"`
	DiastolicPressureMin uint64                      `json:"diastolicPressureMin" note:"舒张压 单位：mmHg 例如:78"`
	HeartRateMax         uint64                      `json:"heartRateMax" note:"心率 单位：次/分钟 例如:65"`
	HeartRateMin         uint64                      `json:"heartRateMin" note:"心率 单位：次/分钟 例如:65"`
	CurrentIndex         int                         `json:"currentIndex" note:"当前测量点 例如:15"`
	TimeStart            types.Time                  `json:"timeStart" note:"开始时间, 例如:2018-07-03 00:00:00"`
	TimeEnd              types.Time                  `json:"timeEnd" note:"结束时间, 例如:2018-07-03 23:59:59"`
	Records              []*BloodPressureRecordTrend `json:"records" note:"记录值"`
}

func (s *BloodPressureRecordTrendStat) Stat() {
	recordCount := len(s.Records)
	if recordCount < 1 {
		return
	}

	timeStart := time.Now()
	timeEnd := time.Now()
	record := s.Records[0]
	s.SystolicPressureMax = record.SystolicPressure
	s.SystolicPressureMin = record.SystolicPressure
	s.DiastolicPressureMax = record.DiastolicPressure
	s.DiastolicPressureMin = record.DiastolicPressure
	if record.HeartRate != nil {
		s.HeartRateMax = *record.HeartRate
		s.HeartRateMin = *record.HeartRate
	}
	if record.MeasureDateTime != nil {
		timeStart = time.Time(*record.MeasureDateTime)
		timeEnd = time.Time(*record.MeasureDateTime)
	}

	for i := 1; i < recordCount; i++ {
		record = s.Records[i]
		if s.SystolicPressureMax < record.SystolicPressure {
			s.SystolicPressureMax = record.SystolicPressure
		}
		if s.SystolicPressureMin > record.SystolicPressure {
			s.SystolicPressureMin = record.SystolicPressure
		}
		if s.DiastolicPressureMax < record.DiastolicPressure {
			s.DiastolicPressureMax = record.DiastolicPressure
		}
		if s.DiastolicPressureMin > record.DiastolicPressure {
			s.DiastolicPressureMin = record.DiastolicPressure
		}
		if record.HeartRate != nil {
			if s.HeartRateMax < *record.HeartRate {
				s.HeartRateMax = *record.HeartRate
			}
			if s.HeartRateMin > *record.HeartRate {
				s.HeartRateMin = *record.HeartRate
			}
		}
		if record.MeasureDateTime != nil {
			measureDateTime := time.Time(*record.MeasureDateTime)
			if timeStart.After(measureDateTime) {
				timeStart = measureDateTime
			}
			if timeEnd.Before(measureDateTime) {
				timeEnd = measureDateTime
			}
		}
	}

	s.TimeStart = types.Time(timeStart)
	s.TimeEnd = types.Time(timeEnd)
}

type BloodPressureRecordTrendFilter struct {
	PatientID       uint64      `json:"patientID" note:"用户ID 关联患者用户登录表用户ID 例如:232442"`
	MeasureDateTime *types.Time `json:"measureDateTime" note:"测量时间 测量血压的时间 例如:2018-07-03 14:45:00"`
	MaxCount        int         `json:"maxCount" note:"最大记录数，必须大于1，默认15"`
}

type BloodPressureRecordPatientFilter struct {
	PatientID uint64 `json:"patientID" note:"用户ID 关联患者用户登录表用户ID 例如:232442"`
}
