package doctor

type KnowledgeInfo struct {
	Id             uint64 `json:"id" note:"知识的id"`
	Title          string `json:"title" note:"知识的标题"`
	Content        string `json:"content" note:"知识的内容"`
	ImageURL       string `json:"poster" note:"对应到前端是海报的路由地址"`
	LoveNum        uint64 `json:"loveNum" note:"点赞数"`
	Comments       uint64 `json:"comments" note:"评论数"`
	Type           uint64 `json:"type" note:"知识的类型"`
	Dayindex       uint64 `json:"dayindex" note:"非个性知识安排的天数"`
	ReadAlreadyNum uint64 `json:"readAlreadyNum" note:"知识被阅读的次数"`
}
