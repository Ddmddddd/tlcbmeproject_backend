package doctor

type DrugDict struct {
	SerialNo      uint64  `json:"serialNo" note:"序号 主键，自增 例如:324"`
	ItemCode      uint64  `json:"itemCode" note:"字典项代码  例如:1"`
	ItemName      string  `json:"itemName" note:"字典项名称  例如:高血压"`
	Specification string  `json:"specification" note:"规格  例如:10mg/粒X12"`
	Units         string  `json:"units" note:"单位 该药的常用单位mg、片、粒，如有多个，用逗号分隔 例如:mg"`
	Effect        string  `json:"effect" note:"功效 例如降压、降糖、调脂 例如:降压"`
	ItemSortValue *uint64 `json:"itemSortValue" note:"排序 用于字典项目排序，从1开始往后排 例如:1"`
	InputCode     string  `json:"inputCode" note:"输入码 用于快速输入，可以是拼音首字母或缩写 例如:gxy"`
	IsValid       uint64  `json:"isValid" note:"有效性 用于描述本记录是否有效，0-无效，1-有效，默认值为1 例如:1"`
}
