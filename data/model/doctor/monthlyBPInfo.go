package doctor

import "github.com/ktpswjz/httpserver/types"

type MonthlyBPInfo struct {
	SerialNo     uint64      `json:"serialNo" note:"序号 主键，自增 例如:324"`
	PatientID    uint64      `json:"patientID" note:"用户ID 关联患者用户登录表用户ID 例如:232442"`
	YearMonth    string      `json:"yearMonth" note:"月份 年份+月份（2016-09） 例如:2016-09"`
	CurrentBP    string      `json:"currentBP" note:"当前血压 当月最后一次血压 例如:132/82"`
	GoalBP       string      `json:"goalBP" note:"目标血压 控制血压的目标 例如:140/90"`
	MaxSBP       uint64      `json:"maxSBP" note:"最大收缩压 当月出现的最大收缩压 例如:156"`
	MaxSBPTime   *types.Time `json:"maxSBPTime" note:"最大收缩压时间 当月出现最大收缩压的时间 例如:2016-09-10 12:00:00"`
	MinSBP       uint64      `json:"minSBP" note:"最小收缩压 当月出现的最小收缩压 例如:124"`
	MinSBPTime   *types.Time `json:"minSBPTime" note:"最小收缩压时间 当月出现最小收缩压的时间 例如:2016-09-11 12:00:00"`
	MaxDBP       uint64      `json:"maxDBP" note:"最大舒张压 当月出现的最大舒张压 例如:102"`
	MaxDBPTime   *types.Time `json:"maxDBPTime" note:"最大舒张压时间 当月出现最大舒张压的时间 例如:2016-09-12 12:00:00"`
	MinDBP       uint64      `json:"minDBP" note:"最小舒张压 当月出现的最小舒张压 例如:83"`
	MinDBPTime   *types.Time `json:"minDBPTime" note:"最小舒张压时间 当月出现最小舒张压的时间 例如:2016-09-13 12:00:00"`
	MaxRange     uint64      `json:"maxRange" note:"最大脉压差 当月出现的最大脉压差 例如:48"`
	MaxRangeTime *types.Time `json:"maxRangeTime" note:"最大脉压差时间 当月出现最大脉压差的时间 例如:2016-09-14 12:00:00"`
	MinRange     uint64      `json:"minRange" note:"最小脉压差 当月出现的最小脉压差 例如:26"`
	MinRangeTime *types.Time `json:"minRangeTime" note:"最小脉压差时间 当月出现最小脉压差的时间 例如:2016-09-15 12:00:00"`
	AvgSBP       uint64      `json:"avgSBP" note:"平均收缩压 当月的平均收缩压 例如:135"`
	AvgDBP       uint64      `json:"avgDBP" note:"平均舒张压 当月的平均舒张压 例如:98"`
	AvgRange     uint64      `json:"avgRange" note:"平均脉压差 当月的平均脉压差 例如:35"`
	NormalTimes  uint64      `json:"normalTimes" note:"血压正常次数 当月正常血压的次数 例如:6"`
	HigherTimes  uint64      `json:"higherTimes" note:"血压偏高次数 当月偏高血压的次数 例如:4"`
	LowerTimes   uint64      `json:"lowerTimes" note:"血压偏低次数 当月偏低血压的次数 例如:2"`
}
