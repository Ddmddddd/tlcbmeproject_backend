package doctor

type DoctorAddedRight struct {
	UserID  uint64 `json:"userID" note:"用户ID 关联医生用户登录表 例如:123"`
	RightID uint64 `json:"rightID" note:"权限ID 附加权限，1-查看机构所有患者 例如:1"`
}
