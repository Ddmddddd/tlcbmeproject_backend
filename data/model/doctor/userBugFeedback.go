package doctor

import "github.com/ktpswjz/httpserver/types"

type UserBugFeedback struct {
	SerialNo  uint64      `json:"serialNo" note:""`
	PatientID *uint64     `json:"patientID" note:""`
	Content   string      `json:"content" note:""`
	Photo     string      `json:"photo" note:""`
	DateTime  *types.Time `json:"dateTime" note:""`
}
