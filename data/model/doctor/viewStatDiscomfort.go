package doctor

import "github.com/ktpswjz/httpserver/types"

type ViewStatDiscomfort struct {
	PatientID           uint64      `json:"patientID" note:"用户ID 关联患者用户登录表用户ID 例如:232442"`
	Discomfort          string      `json:"discomfort" note:"不适情况 如果有多种不适，之间用逗号分隔。例如：剧烈头痛，恶心呕吐。 例如:剧烈头痛，恶心呕吐"`
	Count               uint64      `json:"count" note:""`
	HappenDateTimeStart *types.Time `json:"happenDateTimeStart" note:"发生时间 不适情况发生的时间 例如:2018-07-03 14:45:00"`
	HappenDateTimeEnd   *types.Time `json:"happenDateTimeEnd" note:"发生时间 不适情况发生的时间 例如:2018-07-03 14:45:00"`
}
