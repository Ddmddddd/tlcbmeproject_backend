package doctor

import "github.com/ktpswjz/httpserver/types"

type SmsParam struct {
	TemplateCode   string      `json:"templateCode" note:"模板代码 阿里云分配的短信模板代码 例如:SMS_1330248924729"`
	TemplateParam  string      `json:"templateParam" note:"模板参数json字符串 例如{code:123}"`
	PhoneNumber    string      `json:"phoneNumber" note:"电话号码 例如123456789"`
	OutId          string      `json:"-" note:"外部流水扩展字段,例如：abcdefgh"`
	SignatureNonce string      `json:"-" note:"用于请求的防重放攻击，每次请求唯一，JAVA语言建议用：java.util.UUID.randomUUID()生成即可"`
	TemplateType   uint64      `json:"templateType" note:"模板类型0-验证码，1-短信通知，2-推广短信"`
	SmsContent     string      `json:"smsContent" note:"短信内容"`
	PatientID      uint64      `json:"patientID" note:"病人id"`
	OrgCode        string      `json:"orgCode" note:"机构Code"`
	Sender         uint64      `json:"-" note:"发送者id"`
	SendDateTime   *types.Time `json:"-" note:"发送时间"`
}
