package doctor

type TlcTeamTaskForSelf struct {
	TaskID            uint64 `json:"task_id" note:"对应任务模板的id"`
	TaskName          string `json:"tag" note:"任务的名称"`
	Content           string `json:"content" note:"任务说明"`
	DescriptionPicUrl string `json:"desImgUrl" note:"任务描述图片地址"`
	Status            uint64 `json:"status" note:"描述用户个人任务是否完成"`
}

type TlcTeamTaskData struct {
	TaskID            uint64 `json:"task_id" note:"对应任务模板的id"`
	TaskName          string `json:"tag" note:"任务的名称"`
	Content           string `json:"content" note:"任务说明"`
	DescriptionPicUrl string `json:"desImgUrl" note:"任务描述图片地址"`
	Status            uint64 `json:"status" note:"描述用户个人任务是否完成，组队任务则无需提醒，这一项设定为1"`
	CompMemNum        uint64 `json:"compMemNum" note:"组队任务完成的人数"`
}

type TlcTeamTaskMapWithIntegralTem struct {
	RuleDescription  string `json:"ruleDescription" note:"任务的描述"`
	IntegralTemplate string `json:"integralTemplate" note:"任务对应的积分规则模板"`
}
