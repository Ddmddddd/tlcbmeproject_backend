package doctor

import "github.com/ktpswjz/httpserver/types"

type TlcTeamIntegralRecord struct {
	Content    string      `json:"content" note:"队伍积分记录的内容"`
	Integral   uint64      `json:"integral" note:"记录对应的积分"`
	CreateTime *types.Time `json:"time" note:"记录的创建时间"`
}
