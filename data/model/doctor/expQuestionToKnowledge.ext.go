package doctor

import "github.com/ktpswjz/httpserver/types"

type ExpQuestionToKnowledgeAddInput struct {
	KnowledgeID    uint64      `json:"knowledgeID" note:"链接的知识"`
	Question       string      `json:"question" note:"问题"`
	AnswerList     []string    `json:"answerList" note:"答案列表"`
	CorrectIndex   uint64      `json:"correctIndex" note:"正确答案"`
	EditorID       uint64      `json:"editorID" note:"编辑者id"`
	CreateDateTime *types.Time `json:"createDateTme" note:"创建时间"`
}

type ExpKnowledgeIDInput struct {
	KnowledgeID    uint64      `json:"knowledgeID" note:"链接的知识"`
	Status 		   uint64	   `json:"status" note:"状态"`
}

type ExpQuestionToKnowledgeDeleteInput struct {
	SerialNo       uint64      `json:"serialNo" note:"序号"`
	EditorID       uint64      `json:"editorID" note:"编辑者id"`
	Status         uint64      `json:"status" note:"状态"`
	UpdateTime     *types.Time `json:"updateTime" note:"更新时间"`
}

type ExpQuestionToKnowledgeEditInput struct {
	SerialNo       uint64      `json:"serialNo" note:"序号"`
	Question       string      `json:"question" note:"问题"`
	AnswerList     []string    `json:"answerList" note:"答案列表"`
	CorrectIndex   uint64      `json:"correctIndex" note:"正确答案"`
	EditorID       uint64      `json:"editorID" note:"编辑者id"`
	UpdateTime     *types.Time `json:"updateTime" note:"更新时间"`
}