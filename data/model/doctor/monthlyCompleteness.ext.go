package doctor

import (
	"github.com/ktpswjz/httpserver/types"
	"time"
)

type MonthlyReportDateInfo struct {
	StartDate *types.Time `json:"startDate" note:"开始时间 月报统计的时间 例如:2018-07-01 00:00:00"`
	EndDate   *types.Time `json:"endDate" note:"结束时间 月报统计的时间 例如:2018-07-31 00:00:00"`
	DayNum    uint64      `json:"dayNum" note:"天数 月报统计的当月天数 例如:31"`
}

type MonthlyReportCreateData struct {
	MonthlyReportDateInfo

	PatientID uint64  `json:"patientID" note:"用户ID 关联患者用户登录表用户ID 例如:232442"`
	YearMonth string  `json:"yearMonth" note:"月份 月报的年份和月份 例如:2018-07"`
	Height    *uint64 `json:"height" note:"身高 患者身高 例如:160"`
}

func ParseMonthlyReportDateInfo(year, month int) MonthlyReportDateInfo {
	days := [12]int{31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31}
	dayNum := days[month]
	if month == 2 {
		if (year%4 == 0 && year%100 != 0) || year%400 == 0 {
			dayNum = 29
		}
	}
	startDate := types.Time(time.Date(year, time.Month(month), 1, 0, 0, 0, 0, time.Local))
	endDate := types.Time(time.Date(year, time.Month(month), dayNum, 23, 59, 59, 0, time.Local))
	return MonthlyReportDateInfo{StartDate: &startDate, EndDate: &endDate, DayNum: uint64(dayNum)}
}

type MonthlyReportYearMonth struct {
	YearMonth string `json:"yearMonth" note:"月份 月报的年份和月份 例如:2018-07"`
}
