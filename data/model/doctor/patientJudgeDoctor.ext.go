package doctor

import "github.com/ktpswjz/httpserver/types"

type PatientJudgeDoctorApp struct {
	PatientID uint64     `json:"patientID" note:""`
	DoctorID  uint64     `json:"doctorID" note:""`
	Content   string      `json:"content" note:""`
	Star      uint64     `json:"star" note:""`
	DateTime  *types.Time `json:"dateTime" note:""`
}