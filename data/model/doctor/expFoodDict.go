package doctor

type ExpFoodDict struct {
	SerialNo              uint64   `json:"serialNo" note:"食物编号"`
	Category              uint64   `json:"category" note:"分类"`
	Name                  string   `json:"name" note:"名字"`
	ImageUrl              string   `json:"imageUrl" note:"图片地址"`
	Calorie               float64  `json:"calorie" note:"每100g卡路里"`
	Recommended           uint64   `json:"recommended" note:"推荐性,10最好,1不推荐,0未知"`
	Comment               string   `json:"comment" note:"评价"`
	NutritionCarbohydrate *float64 `json:"nutritionCarbohydrate" note:"碳水化合物(克)"`
	NutritionFat          *float64 `json:"nutritionFat" note:"脂肪(克)"`
	NutritionProtein      *float64 `json:"nutritionProtein" note:"蛋白质(克)"`
	NutritionFibre        *float64 `json:"nutritionFibre" note:"纤维素(克)"`
	NutritionVitaminA     *float64 `json:"nutritionVitaminA" note:"维生素A(微克)"`
	NutritionVitaminC     *float64 `json:"nutritionVitaminC" note:"维生素C(毫克)"`
	NutritionVitaminE     *float64 `json:"nutritionVitaminE" note:"维生素E(毫克)"`
	NutritionCarotene     *float64 `json:"nutritionCarotene" note:"胡萝卜素(微克)"`
	NutritionThiamine     *float64 `json:"nutritionThiamine" note:"硫胺素(毫克)"`
	NutritionRiboflavin   *float64 `json:"nutritionRiboflavin" note:"核黄素(毫克)"`
	NutritionNiacin       *float64 `json:"nutritionNiacin" note:"烟酸(毫克)"`
	NutritionCholesterol  *float64 `json:"nutritionCholesterol" note:"胆固醇(毫克)"`
	NutritionMagnesium    *float64 `json:"nutritionMagnesium" note:"镁(毫克)"`
	Nutrition_calcium     *float64 `json:"nutrition_calcium" note:"钙(毫克)"`
	NutritionIron         *float64 `json:"nutritionIron" note:"铁(毫克)"`
	NutritionZinc         *float64 `json:"nutritionZinc" note:"锌(毫克)"`
	NutritionCopper       *float64 `json:"nutritionCopper" note:"铜(毫克)"`
	NutritionManganese    *float64 `json:"nutritionManganese" note:"锰(毫克)"`
	NutritionPotassium    *float64 `json:"nutritionPotassium" note:"钾(毫克)"`
	NutritionPhosphorus   *float64 `json:"nutritionPhosphorus" note:"磷(毫克)"`
	NutritionSodium       *float64 `json:"nutritionSodium" note:"钠(毫克)"`
	NutritionSelenium     *float64 `json:"nutritionSelenium" note:"硒(微克)"`
}
