package doctor

type ExpDietPlanRepository struct {
	SerialNo uint64 `json:"serialNo" note:""`
	Plan     string `json:"plan" note:"饮食目标"`
	PicUrl   string `json:"picUrl" note:"图片url"`
	Type     string `json:"type" note:"周or天"`
	Level    uint64 `json:"level" note:"优先级"`
}
