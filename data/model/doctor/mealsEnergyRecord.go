package doctor

type MealsEnergyRecord struct {
	SerialNo                    uint64   `json:"serialNo" note:""`
	DietType                    string   `json:"dietType" note:"用餐类型 早餐、中餐、晚餐"`
	ProteinEnergy               *float64 `json:"proteinEnergy" note:"蛋白质热量"`
	FatEnergy                   *float64 `json:"fatEnergy" note:"脂肪热量"`
	CarbohydrateEnergy          *float64 `json:"carbohydrateEnergy" note:"碳水化合物 热量"`
	NutritionalAnalysisSerialNo uint64   `json:"nutritionalAnalysisSerialNo" note:"关联营养分析结论表 id"`
	PatientID                   uint64   `json:"patientID" note:"用户ID"`
}
