package doctor

import "github.com/ktpswjz/httpserver/types"

type ExpPay struct {
	SerialNo         uint64      `json:"serialNo" note:""`
	PatientID        uint64      `json:"patientID" note:""`
	OpenId           string      `json:"openId" note:""`
	Amount           float64     `json:"amount" note:""`
	PartnerTradeNo   string      `json:"parternerTradeNo" note:""`
	Desc             string      `json:"desc" note:""`
	Result           interface{} `json:"result" note:""`
	CreateDateTime   *types.Time `json:"createDateTime" note:""`
}
