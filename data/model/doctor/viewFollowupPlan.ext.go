package doctor

import (
	"tlcbme_project/data/model"
	"github.com/ktpswjz/httpserver/types"
)

type ViewFollowupPlanEx struct {
	ViewFollowupPlan

	SexText      string `json:"sexText" note:"性别 0-未知, 1-男, 2-女，9-未说明 例如:男"`
	Age          int64  `json:"age" note:"年龄，单位岁，如60"`
	Tag          string `json:"tag" note:"人员标签"`
	FollowupDays int64  `json:"followupDays" note:"计划随访日期与当天相差天数，例如:0表示今天，1表示明天， -1表示昨天"`
}

type ViewFollowupPlanFilter struct {
	SerialNo           *uint64 `json:"serialNo" note:"序号 主键，自增 例如:324"`
	PatientID          *uint64 `json:"patientID" note:"用户ID 关联患者用户登录表用户ID 例如:232442"`
	PatientName        string  `json:"patientName" note:"姓名  例如:张三"`
	Sex                *uint64 `json:"sex" note:"性别 0-未知, 1-男, 2-女，9-未说明 例如:1"`
	IdentityCardNumber string  `json:"identityCardNumber" note:"身份证号 例如:330106200012129876"`
	Phone              string  `json:"phone" note:"联系电话 例如:13812344321"`

	FollowUpType   string   `json:"followUpType" note:"随访类型 随访的类型（原因） 例如:三个月例行随访"`
	FollowStatus   *uint64  `json:"followStatus" note:"随访状态 0-待随访，1-已随访。 例如:0"`
	ManageStatuses []uint64 `json:"manageStatuses" note:"管理状态 0-管理中，1-迁出，2-已终止管理 例如:0"`

	ManageDateStart       *types.Time  `json:"manageDateStart" note:"管理开始日期 例如:2018-07-03"`
	ManageDateEnd         *types.Time  `json:"manageDateEnd" note:"管理结束日期 例如:2018-07-03"`
	LastFollowupDateStart *types.Time  `json:"lastFollowupDateStart" note:"上次随访开始日期 例如:2018-06-15"`
	LastFollowupDateEnd   *types.Time  `json:"lastFollowupDateEnd" note:"上次随访结束日期 例如:2018-06-15"`
	FollowupDateStart     *types.Time  `json:"followupDateStart" note:"计划随访开始日期 例如:2018-07-15"`
	FollowupDateEnd       *types.Time  `json:"followupDateEnd" note:"计划随访结束日期 例如:2018-07-15"`
	Order                 *model.Order `json:"order" note:"排序"`

	Today bool `json:"today" note:"是否今天"`
}

type ViewFollowupPlanFilterEx struct {
	ViewFollowupPlanFilter

	OrgCode         string  `json:"orgCode" note:"管理机构代码 管理该患者的机构 例如:897798"`
	DoctorID        *uint64 `json:"doctorID" note:"医生ID 例如:232"`
	HealthManagerID *uint64 `json:"healthManagerID" note:"健康管理师ID 例如:232"`
}

type ViewFollowupPlanCount struct {
	Total    uint64 `json:"total" note:"计划随访"`
	Waiting  uint64 `json:"waiting" note:"待随访"`
	Finished uint64 `json:"finished" note:"已随访"`
	Ignored  uint64 `json:"ignored" note:"已忽略"`
}
