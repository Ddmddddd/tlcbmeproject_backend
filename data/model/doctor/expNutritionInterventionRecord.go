package doctor

import "github.com/ktpswjz/httpserver/types"

type ExpNutritionInterventionRecord struct {
	SerialNo     uint64      `json:"serialNo" note:"序列号"`
	PatientID    *uint64     `json:"patientID" note:"患者ID"`
	StartDate    *types.Time `json:"startDate" note:"总结开始日期"`
	EndDate      *types.Time `json:"endDate" note:"总结结束日期"`
	RecordCount  *uint64     `json:"recordCount" note:"打卡次数统计"`
	Energy       *float64    `json:"energy" note:"平均能量值，含开始日期和结束日期"`
	Protein      *float64    `json:"protein" note:"平均蛋白质摄入"`
	Fat          *float64    `json:"fat" note:"平均脂肪摄入"`
	Carbohydrate *float64    `json:"carbohydrate" note:"平均碳水化合物摄入"`
	DF           *float64    `json:"dF" note:"平均膳食纤维摄入"`
	Habit        *string     `json:"habit" note:"饮食目标完成次数"`
}
