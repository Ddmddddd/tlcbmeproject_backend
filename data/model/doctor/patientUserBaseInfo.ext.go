package doctor

import (
	"fmt"
	"github.com/ktpswjz/httpserver/types"
)

type PatientInfoBase struct {
	PatientID uint64 `json:"patientID" note:"患者ID 关联患者用户登录表用户ID 例如:232442"`
}
type DoctorInfoBase struct {
	DoctorID uint64 `json:"doctorID" note:"医生ID 关联医生用户登录表用户ID 例如:232442"`
}
type PatientListInfoBase struct {
	DoctorID      uint64   `json:"doctorID" note:"医生ID 关联医生用户登录表用户ID 例如:232442"`
	PatientIDList []uint64 `json:"patientIDList" note:"患者id列表 关联患者用户登录表用户ID 例如:232442"`
}

type PatientUserInfoUpdate struct {
	PatientID   uint64      `json:"patientID" note:"患者ID 关联患者用户登录表用户ID 例如:232442"`
	Name        string      `json:"name" note:"姓名  例如:张三"`
	Sex         string      `json:"sex" note:"性别 例如:男"`
	DateOfBirth *types.Time `json:"dateOfBirth" note:"出生日期 例如:1999-09-09"`
	Height      uint64      `json:"height" note:"身高 单位:cm 例如:165"`
	Weight      float64     `json:"weight" note:"身高 单位:kg 例如:65.0"`
	JobType     string      `json:"profession" note:"职业类别 GB/T 6565-1999 职业分类 例如: "`
	Nickname    string      `json:"nickname" note:"姓名  例如:小张"`
}

type PatientUserInfoForApp struct {
	PatientID          uint64      `json:"patientID" note:"患者ID 关联患者用户登录表用户ID 例如:232442"`
	UserName           string      `json:"userName" note:"用户名 患者用户登录表用户名 例如:cdm_app_007"`
	Name               string      `json:"name" note:"姓名  例如:张三"`
	Sex                string      `json:"sex" note:"性别 例如:男"`
	DateOfBirth        *types.Time `json:"dateOfBirth" note:"出生日期 例如:1999-09-09"`
	IdentityCardNumber string      `json:"identityCardNumber" note:"身份证号 例如:330106199909099876"`
	PhoneNumber        string      `json:"phoneNumber" note:"联系电话 例如:13812344321"`
	Photo              string      `json:"photo" note:"头像照片 例如:"`
	NewestHeight       uint64      `json:"newestHeight" note:"身高 单位:cm 例如:165"`
	NewestWeight       float64     `json:"newestWeight" note:"身高 单位:kg 例如:65.0"`
	Nickname           string      `json:"nickname" note:"姓名  例如:小张"`
	RegistDate         *types.Time `json:"registDate" note:"注册日期 例如:2018-08-01"`
	UUID               string      `json:"uuid" note:"宁夏第三方挂号预约平台凭证 例如:"`
	Country            string      `json:"country" note:"所属训练营"`
}

type PatientUserInfoCreateForApp struct {
	UserName           string      `json:"userName" note:"用户名 患者用户登录表用户名 例如:cdm_app_007"`
	Name               string      `json:"name" note:"姓名  例如:张三"`
	Diagnosis          string      `json:"diagnosis" note:"诊断的疾病  例如:高血压，糖尿病"`
	Sex                string      `json:"sex" note:"性别 例如:男"`
	DateOfBirth        *types.Time `json:"dateOfBirth" note:"出生日期 例如:1999-09-09"`
	IdentityCardNumber string      `json:"identityCardNumber" note:"身份证号 例如:330106199909099876"`
	PhoneNumber        string      `json:"phoneNumber" note:"联系电话 例如:13812344321"`
	EducationLevel     string      `json:"education" note:"文化水平 例如:本科"`
	JobType            string      `json:"profession" note:"职业类别 单位:科技"`
	Height             uint64      `json:"height" note:"身高 单位：cm 例如:170"`
	Weight             float64     `json:"weight" note:"身高 单位：kg 例如:65.0"`
	Nickname           string      `json:"nickname" note:"姓名  例如:小张"`
	HospitalCode       string      `json:"hospital" note:"就诊医院(机构代码) 例如:"`
	HealthManagerID    uint64      `json:"healthManager" note:"健康管理师(ID) 例如:"`
	HealthManagerName  string      `json:"healthManagerName" note:"健康管理师（姓名）"`
	Password           string      `json:"password" note:"密码 例如:123"`
	SourceFrom         string      `json:"sourceFrom" note:"来源 例如:APP、微信小程序"`
	Country            string      `json:"country" note:"训练营"`
}

type PatientUserHeightAndWeight struct {
	Height    *uint64  `json:"height" note:"身高 单位：cm 例如:165"`
	Weight    *float64 `json:"weight" note:"体重 最后一次录入的体重，单位：kg 例如:60.5"`
	Waistline *float64 `json:"waistline" note:"腰围 最后一次录入的腰围，单位：cm 例如:60.5"`
}
type PatientUserHeight struct {
	Height    *uint64 `json:"height" note:"身高 单位：cm 例如:165"`
	PatientID uint64  `json:"patientID" note:"患者ID 关联患者用户登录表用户ID 例如:232442"`
}
type PatientUserBaseInfoModify struct {
	SerialNo            uint64      `json:"serialNo" note:"序号 主键，自增 例如:324"`
	UserID              uint64      `json:"userID" note:"用户ID 关联医生用户登录表用户ID 例如:232442"`
	Name                string      `json:"name" note:"姓名  例如:张三"`
	Sex                 *uint64     `json:"sex" note:"性别 0-未知, 1-男, 2-女，9-未说明 例如:1"`
	DateOfBirth         *types.Time `json:"dateOfBirth" note:"出生日期 例如:2000-12-12"`
	IdentityCardNumber  string      `json:"identityCardNumber" note:"身份证号 例如:330106200012129876"`
	Country             string      `json:"country" note:"所属训练营 例如：20220530训练营"`
	Phone               string      `json:"phone" note:"联系电话 例如:13812344321"`
	EducationLevel      string      `json:"education" note:"文化水平 例如:本科"`
	JobType             string      `json:"profession" note:"职业类别 单位:科技"`
	Height              *uint64     `json:"height" note:"身高 单位：cm 例如:165"`
	Weight              *float64    `json:"weight" note:"体重 最后一次录入的体重，单位：kg 例如:60.5"`
	Waistline           *float64    `json:"waistline" note:"腰围 最后一次录入的腰围，单位：cm 例如:60.5"`
	PatientFeature      []string    `json:"patientFeature" note:"患者特点 如有多个，之间用逗号分隔，例如：老年人，肥胖，残疾人 例如:老年人，肥胖"`
	ManagerClasses      []string    `json:"managerClasses" note:"管理分类 如有多个，之间用逗号分隔，例如：高血压，糖尿病 例如:高血压，糖尿病"`
	DoctorID            *uint64     `json:"doctorID" note:"医生ID 例如:232"`
	DoctorName          string      `json:"doctorName" note:"医生姓名 例如:张三"`
	HealthManagerID     *uint64     `json:"healthManagerID" note:"健康管理师ID 例如:232"`
	HealthManagerName   string      `json:"healthManagerName" note:"健康管理师姓名 例如:李四"`
	Diagnosis           string      `json:"diagnosis" note:"诊断 例如:高血压，糖尿病"`
	DiagnosisMemo       string      `json:"diagnosisMemo" note:"诊断备注  例如:血压 150/90 mmHg，血糖 7.9 mmol/L"`
	VisitOrgCode        string      `json:"visitOrgCode" note:"就诊机构代码 例如:123"`
	Status              uint64      `json:"status" note:"审核状态 0-未审核，1-审核通过，2-审核不通过 例如:0"`
	ManageChangeType    uint64      `json:"manageChangeType" note:"管理分类改变的类型 0-不改变，1-高血压 变 糖尿病，2-高血压 变 高血压，糖尿病，3-糖尿病 变 高血压，4-糖尿病 变 高血压，糖尿病，5-糖尿病，高血压 变 高血压，6-糖尿病，高血压 变 糖尿病"`
	DmType              string      `json:"dmType" note:"糖尿病类型，1型糖尿病、2型糖尿病、妊娠糖尿病、其他类型， 例如:1型糖尿病"`
	DmMedications       []string    `json:"dmMedications" note:"糖尿病当前用药，口服降糖药、注射胰岛素， 例如:口服降糖药"`
	GoalEnergy          *float64    `json:"goalEnergy" note:"目标热量值"`
	GoalCarbohydrateMax *float64    `json:"goalCarbohydrateMax" note:"碳水化合物提供能量占比最大值"`
	GoalCarbohydrateMin *float64    `json:"goalCarbohydrateMin" note:"碳水化合物提供能量占比最小值"`
	GoalFatMin          *float64    `json:"goalFatMin" note:"脂肪提供能量占比最小值"`
	GoalFatMax          *float64    `json:"goalFatMax" note:"脂肪提供能量占比最大值"`
	GoalProteinMin      *float64    `json:"goalProteinMin" note:"蛋白质提供能量占比最小值"`
	GoalProteinMax      *float64    `json:"goalProteinMax" note:"蛋白质提供能量占比最大值"`
}

func (s *PatientUserBaseInfoModify) Verify() error {
	//if s.IdentityCardNumber == "" {
	//	return fmt.Errorf("身份证号为空")
	//}
	//if s.Name == "" {
	//	return fmt.Errorf("姓名为空")
	//}
	//if s.Sex == nil {
	//	return fmt.Errorf("性别为空")
	//}
	//if s.DateOfBirth == nil {
	//	return fmt.Errorf("出生日期为空")
	//}
	//if s.Phone == "" {
	//	return fmt.Errorf("联系电话为空")
	//}
	//if s.DoctorID == nil {
	//	return fmt.Errorf("医生为空")
	//}
	if s.HealthManagerID == nil {
		return fmt.Errorf("管理师为空")
	}

	return nil
}

type PatientUserBaseInfoChangeNickname struct {
	Nickname string `json:"nickname" note:"昵称 例如:蓝精灵"`
}

type TlcPatientNameQuery struct {
	PatientID uint64 `json:"patientID" note:"用户的id"`
}

type TlcPatientNameQueryResult struct {
	Name string `json:"name" note:"查询得到的用户名称"`
}
