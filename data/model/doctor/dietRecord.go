package doctor

import "github.com/ktpswjz/httpserver/types"

type DietRecord struct {
	SerialNo      uint64      `json:"serialNo" note:"序号\\n324\\nFCM\\n主键，自增"`
	EatDateTime   *types.Time `json:"eatDateTime" note:"用餐日期\n2018-07-03 \nFCM\n用餐的实际日期"`
	InputDateTime *types.Time `json:"inputDateTime" note:"录入时间\n2018-07-03 14:00:00\nFCM\n将用药情况录入系统的时间"`
	DietType      string      `json:"dietType" note:"用餐类型 早餐、中餐、晚餐"`
	PatientID     uint64      `json:"patientID" note:"用户ID"`
}
