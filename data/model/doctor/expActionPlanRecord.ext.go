package doctor

import (
	"github.com/ktpswjz/httpserver/types"
)

type ExpActionPlanRecordForApp struct {
	PatientID      uint64      `json:"patientID" note:"用户ID"`
	ActionPlan     string      `json:"actionPlan" note:"行动计划"`
	Completion     uint64      `json:"completion" note:"行动计划完成情况，0-少量完成，1-部分完成，2-全部完成"`
	Level          uint64      `json:"level" note:"优先级，分为0-5，数值越大优先级越高"`
	CreateDateTime *types.Time `json:"createDateTime" note:"创建时间"`
	SeqActionPlan  uint64	   `json:"seqActionPlan" note:"对应引擎端生活计划的id，用于提交、修改等操作"`
	ActionPlanID   uint64	   `json:"actionPlanID" note:"生活计划ID，对应ExpActionPlan中的SerialNo"`
}

type ExpActionPlanRecordFilter struct {
	PatientID      		uint64      	`json:"patientID" note:"用户ID"`
	StartDateTime  		*types.Time 	`json:"startDateTime" note:"起始时间"`
	ActionPlanIDList   	[]uint64	   	`json:"actionPlanIDList" note:"生活计划ID列表，对应ExpActionPlan中的SerialNo"`
}

type ExpActionPlanRecordCompletionEdit struct {
	SerialNo       uint64      `json:"serialNo" note:"序号"`
	Completion     uint64      `json:"completion" note:"行动计划完成情况，0-少量完成，1-部分完成，2-全部完成"`
	CreateDateTime *types.Time `json:"createDateTime" note:"创建时间"`
	SeqActionPlan  uint64	   `json:"seqActionPlan" note:"对应引擎端生活计划的id，用于提交、修改等操作"`
	ActionPlanID   uint64	   `json:"actionPlanID" note:"生活计划ID，对应ExpActionPlan中的SerialNo"`
}

type ExpActionPlanRecordEditForApp struct {
	SerialNo       uint64      `json:"serialNo" note:"序号"`
	ActionPlan     string      `json:"actionPlan" note:"行动计划"`
	Completion     uint64      `json:"completion" note:"行动计划完成情况，0-少量完成，1-部分完成，2-全部完成"`
	CreateDateTime *types.Time `json:"createDateTime" note:"创建时间"`
	ActionPlanID   uint64	   `json:"actionPlanID" note:"生活计划ID，对应ExpActionPlan中的SerialNo"`
}

type ExpActionPlanRecordTimeFilter struct {
	PatientID      		uint64      	`json:"patientID" note:"用户ID"`
	StartDateTime  		*types.Time 	`json:"startDateTime" note:"起始时间"`
}

type ExpActionPlanRecordForDoctor struct {
	ActionPlan     	string      `json:"actionPlan" note:"行动计划"`
	CompleteInfo   	string	   	`json:"completeInfo" note:"完成情况"`
	Editor		   	string		`json:"editor" note:"计划编辑者"`
	Level 			uint64		`json:"level" note:"优先级"`
	Status          uint64      `json:"status" note:"状态，0-使用中，1-已取消"`
	ActionPlanID   	uint64	   	`json:"actionPlanID" note:"生活计划ID，对应ExpActionPlan中的SerialNo"`
}