package doctor

import "github.com/ktpswjz/httpserver/types"

type Viewweightrecord struct {
	Serialno        uint64      `json:"serialno" note:"序号 主键，自增 例如:324"`
	Patientid       uint64      `json:"patientid" note:"用户ID 关联患者用户登录表用户ID 例如:232442"`
	Measuredatetime *types.Time `json:"measuredatetime" note:"测量时间 测量体重时的时间 例如:2018-07-03 14:45:00"`
	Orgcode         string      `json:"orgcode" note:"管理机构代码 管理该患者的机构 例如:897798"`
}
