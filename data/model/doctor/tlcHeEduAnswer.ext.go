package doctor

type TlcHeEduQuestionOptionInfo struct {
	Id      uint64 `json:"id" note:"设置的问题的答案的id"`
	Content string `json:"content" note:"答案的内容,需要写选项"`
	Isop    uint64 `json:"isop" note:"是否被设置为是正确答案，0是错误答案，1是正确答案 "`
}
