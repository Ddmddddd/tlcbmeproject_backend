package doctor

type MonthlyCompleteness struct {
	SerialNo            uint64 `json:"serialNo" note:"序号 主键，自增 例如:324"`
	PatientID           uint64 `json:"patientID" note:"用户ID 关联患者用户登录表用户ID 例如:232442"`
	YearMonth           string `json:"yearMonth" note:"月份 年份+月份（2016-09） 例如:2016-09"`
	BpMeasuredTimes     uint64 `json:"bpMeasuredTimes" note:"血压实测次数  例如:12"`
	BpPlanedTimes       uint64 `json:"bpPlanedTimes" note:"血压应测次数  例如:90"`
	WeightMeasuredTimes uint64 `json:"weightMeasuredTimes" note:"体重实测次数  例如:20"`
	WeightPlanedTimes   uint64 `json:"weightPlanedTimes" note:"体重应测次数  例如:30"`
	DrugTakenTimes      uint64 `json:"drugTakenTimes" note:"实际服药次数  例如:20"`
	DrugPlanedTimes     uint64 `json:"drugPlanedTimes" note:"计划服药次数  例如:30"`
	HrMeasuredTimes     uint64 `json:"hrMeasuredTimes" note:"心率实测次数  例如:12"`
	DaysInManagement    uint64 `json:"daysInManagement" note:"加入管理天数  例如:1002"`
}
