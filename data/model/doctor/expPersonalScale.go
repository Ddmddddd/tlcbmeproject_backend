package doctor

import "github.com/ktpswjz/httpserver/types"

type ExpPersonalScale struct {
	SerialNo       uint64      `json:"serialNo" note:"序号"`
	PatientID      uint64      `json:"patientID" note:"患者ID"`
	ScaleContent   interface{} `json:"scaleContent" note:"量表内容"`
	Status         uint64      `json:"status" note:"状态，0-未完成，1-已完成"`
	CreateDateTime *types.Time `json:"createDateTime" note:"创建时间"`
}
