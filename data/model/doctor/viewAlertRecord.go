package doctor

import "github.com/ktpswjz/httpserver/types"

type ViewAlertRecord struct {
	SerialNo        uint64      `json:"serialNo" note:"序号 主键，自增 例如:324"`
	PatientID       uint64      `json:"patientID" note:"用户ID 关联患者用户登录表用户ID 例如:232442"`
	AlertCode       string      `json:"alertCode" note:"预警代码 每类预警的唯一代码 例如:001"`
	AlertType       string      `json:"alertType" note:"预警类型 例如:血压"`
	AlertName       string      `json:"alertName" note:"预警名称 例如:单次血压异常偏高"`
	AlertReason     string      `json:"alertReason" note:"预警原因  例如:180/100mmHg"`
	AlertMessage    string      `json:"alertMessage" note:"预警消息 例如:消息内容"`
	AlertDateTime   *types.Time `json:"alertDateTime" note:"预警发生时间  例如:2018-07-06 14:55:00"`
	Status          uint64      `json:"status" note:"处理状态 0-未处理 例如:0"`
	ProcessMode     *uint64     `json:"processMode" note:"处理方式 0-随访，1-忽略 例如:0"`
	OrgCode         string      `json:"orgCode" note:"管理机构代码 管理该患者的机构 例如:897798"`
	DoctorID        *uint64     `json:"doctorID" note:"医生ID 例如:232"`
	HealthManagerID *uint64     `json:"healthManagerID" note:"健康管理师ID 例如:232"`
}
