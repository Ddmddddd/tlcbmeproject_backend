package doctor

import "github.com/ktpswjz/httpserver/types"

type TlcTeamInfo struct {
	SerialNo       uint64      `json:"serialNo" note:"主键"`
	TeamNickName   string      `json:"teamNickName" note:"队伍名称"`
	BelongToGroup  string      `json:"belongToGroup" note:"所属大队"`
	MemNumUp       uint64      `json:"memNumUp" note:"团队成员人数上限"`
	TeamGenerateID string      `json:"teamGenerateID" note:"小队生成的id"`
	CreatorID      uint64      `json:"creatorID" note:"小队创建者的id"`
	CreateTime     *types.Time `json:"createTime" note:"小队创建时间"`
}
