package doctor

import "github.com/ktpswjz/httpserver/types"

type ExpPatientTaskRecord struct {
	SerialNo       uint64      `json:"serialNo" note:""`
	PatientID      uint64      `json:"patientID" note:"患者id"`
	TaskID		   uint64  	   `json:"taskID" note:"对应taskRepository中的serialNo，为0则代表该任务不是按期完成的"`
	ParentTaskName *string      `json:"parentTaskName" note:"父任务名"`
	TaskName       string      `json:"taskName" note:"任务名"`
	CreateDateTime *types.Time `json:"createDateTime" note:"创建时间"`
	Credit         uint64      `json:"credit" note:"获取积分"`
	Record         *interface{} `json:"record" note:"任务完成具体记录"`
}
