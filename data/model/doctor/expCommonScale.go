package doctor

type ExpCommonScale struct {
	SerialNo    uint64      `json:"serialNo" note:"序号"`
	Title       string      `json:"title" note:"题目"`
	Content     interface{} `json:"content" note:"量表内容，json格式"`
	Description string      `json:"description" note:"量表描述"`
	ScaleType   uint64      `json:"scaleType" note:"量表属性，0-默认量表，1-普通量表，2-需要引擎生成生活计划的量表"`
}
