package doctor

type UserVersionRecord struct {
	SerialNo    uint64 `json:"serialNo" note:"序号 例如:1"`
	UserID      uint64 `json:"userID" note:"用户ID 关联患者用户登录表用户ID 例如:232442"`
	VersionCode uint64 `json:"versionCode" note:"版本代码 例如:1"`
	VersionName string `json:"versionName" note:"版本名称 例如:1.0.0.1"`
}
