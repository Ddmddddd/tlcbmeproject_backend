package doctor

type WeChatRemindDict struct {
	SerialNo   			uint64      `json:"serialNo" note:"序号"`
	TemplateID 			string      `json:"templateID" note:"对应微信订阅消息的template_id"`
	Page       			string      `json:"page" note:"点击卡片后的跳转页面，例如：index?foo=bar"`
	Data       			interface{} `json:"data" note:"模板内容，格式如：{ "key1": { "value": any }, "key2": { "value": any } }"`
	Lang       			string      `json:"lang" note:"进入小程序的语言类型，支持zh_CN(简体中文)、en_US(英文)、zh_HK(繁体中文)、zh_TW(繁体中文)，默认为zh_CN"`
	MiniprogramState	string	`json:"miniprogram_state" note:"跳转小程序类型：developer为开发版；trial为体验版；formal为正式版；默认为正式版"`
}
