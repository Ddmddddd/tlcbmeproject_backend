package doctor

type DictFilter struct {
	SerialNo *uint64 `json:"serialNo" note:"序号 主键，自增 例如:324"`
}

type DictKeywordFilter struct {
	Keyword string `json:"keyword" note:"关键字，名称或输入码"`
}

type DictCodeItem struct {
	Code uint64 `json:"code" note:"代码"`
	Name string `json:"name" note:"名称"`
}

type DictCodeItemFilter struct {
	InputCode string `json:"inputCode" note:"输入码"`
}
