package doctor

type ExpSmallScale struct {
	SerialNo uint64      `json:"serialNo" note:"序号"`
	TaskID 	 uint64 	 `json:"taskID" note:"任务序号"`
	Content  interface{} `json:"content" note:"量表内容，json格式"`
}
