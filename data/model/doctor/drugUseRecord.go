package doctor

import "github.com/ktpswjz/httpserver/types"

type DrugUseRecord struct {
	SerialNo      uint64      `json:"serialNo" note:"序号 主键，自增 例如:324"`
	PatientID     uint64      `json:"patientID" note:"用户ID 关联患者用户登录表用户ID 例如:232442"`
	TimePoint     string      `json:"timePoint" note:"时间点 早、中、晚 例如:早"`
	DrugName      string      `json:"drugName" note:"药品名称  例如:氨氯地平片"`
	Dosage        string      `json:"dosage" note:"剂量 例如:5mg"`
	Freq          string      `json:"freq" note:"频次 例如:1次/日"`
	Memo          string      `json:"memo" note:"备注  例如:"`
	UseDateTime   *types.Time `json:"useDateTime" note:"用药时间 用药的实际时间 例如:2018-07-03 14:45:00"`
	InputDateTime *types.Time `json:"inputDateTime" note:"录入时间 将用药情况录入系统的时间 例如:2018-07-03 14:00:00"`
}
