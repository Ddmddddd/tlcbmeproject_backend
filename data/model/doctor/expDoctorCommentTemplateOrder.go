package doctor

type ExpDoctorCommentTemplateOrder struct {
	SerialNo      uint64       `json:"serialNo" note:""`
	EditorID      uint64       `json:"editorID" note:"编辑者id"`
	TemplateOrder *interface{} `json:"templateOrder" note:"评论模板顺序"`
}
