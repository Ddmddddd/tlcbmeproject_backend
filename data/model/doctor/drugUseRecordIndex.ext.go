package doctor

import (
	"fmt"
	"github.com/ktpswjz/httpserver/types"
)

type DrugUseRecordIndexEx struct {
	DrugUseRecordIndex

	UsedDays int64 `json:"usedDays" note:"已使用天数，0表示刚开始使用，例如：3"`
}

type DrugUseRecordIndexFilter struct {
	SerialNo   *uint64 `json:"serialNo" note:"序号 主键，自增 例如:324"`
	PatientID  *uint64 `json:"patientID" note:"用户ID 关联患者用户登录表用户ID 例如:232442"`
	Status     *uint64 `json:"status" note:"状态 0-使用中，1-已停用 例如:0"`
	IsModified *uint64 `json:"isModified" note:"是否调整过用法 0-未调整过，1-调整过 例如:0"`
}
type DrugRecordForDoctorList struct {
	DrugList []*DrugUseRecordForDoctor `json:"drugList"`
}

type DrugUseRecordForDoctor struct {
	DrugName         string      `json:"drugName" note:"药品名称  例如:氨氯地平片"`
	Freq             string      `json:"freq" note:"频次 例如:1次/日"`
	FirstUseDateTime *types.Time `json:"firstUseDateTime" note:"首次使用时间 例如:2018-01-01 08:50:00"`
	Unit             string      `json:"unit" note:"单位  例如:mg"`
	Dosage           string      `json:"dosage" note:"剂量 例如:5"`
	PatientID        uint64      `json:"patientID" note:"用户ID 关联患者用户登录表用户ID 例如:232442"`
	CreateBy         string      `json:"createBy" note:"这条记录的创建者 如果是医生录入的，这里填医生姓名；如果是系统整理出来的，这里为空。 例如:张三"`
}

func (s *DrugUseRecordForDoctor) CopyTo(target *DrugUseRecordIndex) {
	if s == nil {
		return
	}
	target.PatientID = s.PatientID
	target.Freq = fmt.Sprintf("%s次/日", s.Freq)
	target.Dosage = fmt.Sprintf("%s%s", s.Dosage, s.Unit)
	target.DrugName = s.DrugName
	target.CreateBy = s.CreateBy
	if s.FirstUseDateTime == nil {
		target.FirstUseDateTime = nil
		target.LastUseDateTime = nil
	} else {
		firstUseDateTime := types.Time(*s.FirstUseDateTime)
		target.FirstUseDateTime = &firstUseDateTime
	}
}
