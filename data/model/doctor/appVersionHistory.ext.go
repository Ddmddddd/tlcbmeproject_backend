package doctor

import (
	"encoding/json"
	"fmt"
	"github.com/ktpswjz/httpserver/types"
	"io/ioutil"
	"os"
	"path/filepath"
)

type AppVersionRecordBase struct {
	VersionCode uint64 `json:"versionCode" required:"true" note:"版本代码 例如:1"`
}

type AppVersionRecord struct {
	AppVersionRecordBase

	VersionName    string      `json:"versionName" note:"版本名称 例如:1.0.0.1"`
	UpdateDate     *types.Time `json:"updateDate" note:"更新时间  例如:2018-07-06 14:55:00"`
	UpdateContent  string      `json:"updateContent" note:"更新内容 例如:修改Bug"`
	IsForced       uint64      `json:"isForced" note:"是否强制更新 0-否，1-强制更新 例如:0"`
	CompMinVersion uint64      `json:"compMinVersion" note:"最小兼容版本代码 例如:1"`
}

type AppVersionRecordFromUser struct {
	VersionCode uint64 `json:"versionCode" note:"版本代码 例如:1"`
	PatientID   uint64 `json:"patientID" required:"true" note:"用户ID 关联患者用户登录表用户ID 例如:232442"`
}

func (s *AppVersionHistory) SaveToFile(filePath string) error {
	bytes, err := json.MarshalIndent(s, "", "    ")
	if err != nil {
		return err
	}

	fileFolder := filepath.Dir(filePath)
	_, err = os.Stat(fileFolder)
	if os.IsNotExist(err) {
		os.MkdirAll(fileFolder, 0777)
	}

	file, err := os.Create(filePath)
	if err != nil {
		return err
	}
	defer file.Close()

	_, err = fmt.Fprint(file, string(bytes[:]))

	return err
}

type AppVersionHistoryEx struct {
	AppVersionHistory

	Url     string `json:"url" note:"下载路径"`
	UrlCode string `json:"urlCode" note:"下载路径二维码"`
}

func (s *AppVersionHistoryEx) LoadFromFile(filePath string) error {
	bytes, err := ioutil.ReadFile(filePath)
	if err != nil {
		return err
	}

	return json.Unmarshal(bytes, s)
}
