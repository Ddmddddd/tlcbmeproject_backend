package doctor

type ExpNutritionInterventionHabitReport struct {
	Habit1      string `json:"habit1"`
	Habit1Value uint64 `json:"habit1value"`
	Habit2      string `json:"habit2"`
	Habit2Value uint64 `json:"habit2value"`
	Habit3      string `json:"habit3"`
	Habit3Value uint64 `json:"habit3value"`
	Habit4      string `json:"habit4"`
	Habit4Value uint64 `json:"habit4value"`
}

type ExpNutritionInterventionHabitReportHabitUpdate struct {
	SerialNo uint64  `json:"serialNo" note:"序列号"`
	Habit    *string `json:"habit" note:"饮食目标完成次数"`
}

type ExpNutritionInterventionReportGet struct {
	PatientID uint64 `json:"patientID" note:"患者ID"`
}
