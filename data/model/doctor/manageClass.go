package doctor

type ManageClass struct {
	ItemCode uint64 `json:"itemCode" note:"字典项代码  例如:1"`
	ItemName string `json:"itemName" note:"字典项名称  例如:高血压"`
}
