package doctor

type CampListAddInput struct {
	Name string  `json:"name" note:"训练营名称"`
	Desc *string `json:"desc" note:"训练营描述"`
}

type CampListInfo struct {
	SerialNo       uint64     `json:"serialNo" note:"序号"`
	Name           string     `json:"name" note:"训练营名称"`
	Desc           *string    `json:"desc" note:"训练营描述"`
}

type CampListUpdateInput struct {
	SerialNo       uint64     `json:"serialNo" note:"序号"`
	Name           string     `json:"name" note:"训练营名称"`
	Desc           *string    `json:"desc" note:"训练营描述"`
}

type CampListSerialNo struct {
	SerialNo       uint64     `json:"serialNo" note:"序号"`
}

type CampListDict struct {
	Text  string `json:"text" note:"对应前端过滤器所需的字典格式"`
	Value string `json:"value" note:"对应前端过滤器所需的字典格式"`
}