package doctor

type DivisionBaseInfo struct {
	ItemCode string `json:"code" note:"行政区划代码"`
	ItemName string `json:"name" note:"行政区划名称"`
	FullName string `json:"fullName" note:"行政区划全称"`
}

type DivisionDictTreeItem struct {
	SerialNo           uint64 `json:"serialNo" note:"序号 主键，自增 例如:324"`
	ItemCode           string `json:"code" note:"行政区划代码"`
	ItemName           string `json:"name" note:"行政区划名称"`
	FullName           string `json:"fullName" note:"行政区划全称"`
	ParentDivisionCode string `json:"parent" note:"上级行政区划代码"`
	Disabled           bool   `json:"disabled" note:"是否禁用"`
}

type DivisionDictTree struct {
	DivisionDictTreeItem

	Children []*DivisionDictTree `json:"children,omitempty" note:"下属区划"`
}

type DivisionDictTreeAdmin struct {
	DivisionDictTreeItem

	Children []*DivisionDictTreeAdmin `json:"children" note:"下属区划"`
}

type DivisionDictTreeFilter struct {
	Admin bool `json:"admin" note:"是否用于管理"`
}

type DivisionDictFilter struct {
	SerialNo *uint64 `json:"serialNo" note:"序号 主键，自增 例如:324"`
	ItemCode string  `json:"code" note:"行政区划代码"`
	FullName string  `json:"fullName" note:"行政区划全称"`
}

type DivisionDictDeleteFilter struct {
	ItemCode string `json:"code" required:"true" note:"行政区划代码"`
}

type DivisionDictCreate struct {
	Parent *string             `json:"parent" note:"上级区划代码 12位统计用区划代码，空表示省/直辖市 例如:450000000000"`
	Items  []*DivisionBaseInfo `json:"items" required:"true" note:"行政区划"`
}
