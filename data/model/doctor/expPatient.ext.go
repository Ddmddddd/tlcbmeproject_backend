package doctor

import "github.com/ktpswjz/httpserver/types"

type ExpPatientCreate struct {
	PatientID 		uint64 		`json:"patientID" note:"用户ID"`
	Type      		uint64 		`json:"type" note:"管理级别，1-实验组，2-对照组"`
	CreateDateTime 	*types.Time `json:"createDateTime" note:"创建时间 例如:2000-12-12 00:00:00"`
}

type ExpPatientEx struct {
	PatientID 		uint64 		`json:"patientID" note:"用户ID"`
	Type      		uint64 		`json:"type" note:"管理级别，1-实验组，2-对照组"`
}

type ExpPatientStatusChange struct {
	SerialNo			uint64		`json:"serialNo" note:"序号，主键"`
	PatientID 			uint64 		`json:"patientID" note:"用户ID"`
	Status      		uint64 		`json:"status" note:"管理状态，0-退出管理，1-管理中"`
	UpdateDateTime 	    *types.Time `json:"updateDateTime" note:"创建时间 例如:2000-12-12 00:00:00"`
	EndReason	   		string      `json:"endReason" note:"终止原因"`
}

type ExpPatientTypeAndStatus struct {
	PatientID 			uint64 		`json:"patientID" note:"用户ID"`
	Type      			uint64 		`json:"type" note:"管理级别，1-实验组，2-对照组"`
	Status      		uint64 		`json:"status" note:"管理状态，0-退出管理，1-管理中"`
}

type ExpPatientInfoGet struct {
	PatientID 			uint64 		`json:"patientID" note:"用户ID"`
}

type ExpPatientBaseInfo struct {
	PatientID 			uint64 		`json:"patientID" note:"用户ID"`
	Status      		uint64 		`json:"status" note:"管理状态，0-退出管理，1-管理中"`
	//UndoneScales		[]uint64 	`json:"undoneScales" note:"未完成量表的序号"`
}