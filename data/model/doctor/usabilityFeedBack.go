package doctor

import "github.com/ktpswjz/httpserver/types"

type UsabilityFeedBack struct {
	SerialNo    uint64      `json:"serialNo" note:"序号 主键，自增 例如:1"`
	UserName    string      `json:"userName" note:"用户名 关联患者用户登录表用户名 例如:aa"`
	AppType     string      `json:"appType" note:"应用类别 Android、iOS或者微信小程序 例如:Android"`
	Description string      `json:"description" note:"问题描述 反馈问题的详细描述 例如:xxxx"`
	AppVersion  string      `json:"appVersion" note:"应用版本 当前应用版本 例如:1.0.1.0"`
	RecordTime  *types.Time `json:"recordTime" note:"反馈时间 记录反馈的时间 例如:2018-12-12 08:00:00"`
	Flag        uint64      `json:"flag" note:"处理状态 0：待处理；1：已解决；2：其他 例如:1"`
	ResultMemo  string      `json:"resultMemo" note:"处理结果 处理结果描述 例如:不作修改"`
}
