package doctor

import "github.com/ktpswjz/httpserver/types"

type WeChatRemindTaskCreate struct {
	PatientID      uint64      `json:"patientID" note:"患者id"`
	OpenID         string      `json:"openID" note:"患者openid，在微信提醒时需要用到"`
	TemplateID     string      `json:"templateID" note:"微信提醒的template_id，在微信提醒时要用到"`
	CreateDateTime *types.Time `json:"createDateTime" note:"创建时间"`
	Type           uint64      `json:"type" note:"提醒内容，1-血压，2-血糖，3-饮食"`
}

type WeChatRemindTaskSend struct {
	PatientID      uint64      `json:"patientID" note:"患者id"`
	TemplateID     string      `json:"templateID" note:"微信提醒的template_id，在微信提醒时要用到"`
}