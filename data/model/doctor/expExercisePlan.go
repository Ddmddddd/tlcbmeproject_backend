package doctor

type ExpExercisePlan struct {
	SerialNo     uint64 `json:"serialNo" note:""`
	PatientID    uint64 `json:"patientID" note:"患者id"`
	ExercisePlan string `json:"exercisePlan" note:"训练计划"`
	Counts       uint64 `json:"counts" note:"完成次数"`
}
