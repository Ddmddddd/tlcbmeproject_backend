package doctor

type MonthlyAnalysis struct {
	SerialNo             uint64 `json:"serialNo" note:"序号 主键，自增 例如:324"`
	PatientID            uint64 `json:"patientID" note:"用户ID 关联患者用户登录表用户ID 例如:232442"`
	YearMonth            string `json:"yearMonth" note:"月份 年份+月份（2016-09） 例如:2016-09"`
	CompletenessAnalysis string `json:"completenessAnalysis" note:"完成度分析 对当月总体管理情况的分析 例如:记录血压、体重保持得很好，记录服药有所放松"`
	BpTrendAnalysis      string `json:"bpTrendAnalysis" note:"血压趋势分析 对当月血压趋势图的分析 例如:血压平稳下降，达标仍需努力"`
	BpAnalysis           string `json:"bpAnalysis" note:"血压数据解读 对当月血压数据的解读 例如:一半以上测量达标，高血压值不可忽略"`
	HrTrendAnalysis      string `json:"hrTrendAnalysis" note:"心率趋势分析 对当月心率趋势图的分析 例如:心率曲线相对平稳，达标仍需努力"`
	HrAnalysis           string `json:"hrAnalysis" note:"心率数据解读 对当月心率数据的解读 例如:心率值基本正常，偶尔偏快"`
	WeightTrendAnalysis  string `json:"weightTrendAnalysis" note:"体重趋势分析 对当月体重趋势图的分析 例如:体重基本没变，偷懒了吧？"`
	WeightAnalysis       string `json:"weightAnalysis" note:"体重数据解读 对当月体重数据的解读 例如:需要控制饮食，加强锻炼了"`
	AdviceFromDoctor     string `json:"adviceFromDoctor" note:"健康管理师建议 根据本月的总体情况健康管理师给出的建议 例如:继续保持，坚持管理"`
}
