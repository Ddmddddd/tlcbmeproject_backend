package doctor

type KnowledgeInfoId struct {
	Id uint64 `json:"id" note:"对应前端传入的知识的id"`
}

type KnowledgeInfoDateString struct {
	DateString string `json:"dateString" note:"对应前端传入的dateString参数"`
	PatientID  uint64 `json:"patientID"`
}

type KnowledgeInfoIncludingComments struct {
	Id             uint64                   `json:"id" note:"知识的id"`
	Title          string                   `json:"title" note:"知识的标题"`
	Content        string                   `json:"content" note:"知识的内容"`
	ImageURL       string                   `json:"poster" note:"对应到前端是海报的路由地址"`
	LoveNum        uint64                   `json:"loveNum" note:"点赞数"`
	Comments       uint64                   `json:"comments" note:"评论数"`
	Type           uint64                   `json:"type" note:"知识的类型"`
	Dayindex       uint64                   `json:"dayindex" note:"非个性知识安排的天数"`
	ReadAlreadyNum uint64                   `json:"readAlreadyNum" note:"知识被阅读的次数"`
	CommentsList   []*TlcGetKnwCommentsInfo `json:"commentsList" note:"评论列表"`
}

func (s *KnowledgeInfoIncludingComments) GetAttributeFromKnwInfo(source *KnowledgeInfo) {
	if source == nil {
		return
	}
	s.Id = source.Id
	s.Title = source.Title
	s.Content = source.Content
	s.ImageURL = source.ImageURL
	s.LoveNum = source.LoveNum
	s.Comments = source.Comments
	s.Type = source.Type
	s.Dayindex = source.Dayindex
	s.ReadAlreadyNum = source.ReadAlreadyNum
}
