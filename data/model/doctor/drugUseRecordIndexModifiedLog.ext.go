package doctor

type DrugUseRecordIndexModifiedLogFilter struct {
	SerialNo     *uint64 `json:"serialNo" note:"序号 主键，自增 例如:324"`
	DURISerialNo *uint64 `json:"dURISerialNo" note:"用药记录索引表序号 关联DrugUseRecordIndex表的SerialNo 例如:232442"`
	PatientID    *uint64 `json:"patientID" note:"用户ID 关联患者用户登录表用户ID 例如:232442"`
	DrugName     string  `json:"drugName" note:"药品名称  例如:氨氯地平片"`
}

type DrugUseRecordIndexModifiedLogCreate struct {
	DURISerialNo *uint64 `json:"dURISerialNo" note:"用药记录索引表序号 关联DrugUseRecordIndex表的SerialNo 例如:232442"`
	PatientID    uint64  `json:"patientID" required:"true" note:"用户ID 关联患者用户登录表用户ID 例如:232442"`
	DrugName     string  `json:"drugName" required:"true" note:"药品名称  例如:氨氯地平片"`
	Dosage       string  `json:"dosage" note:"剂量 例如:5mg"`
	Freq         string  `json:"freq" note:"频次 例如:1次/日"`
	ModifyType   string  `json:"modifyType" required:"true" note:"修改类型 值为以下三种：开始使用，调整用法，停止使用 例如:开始使用"`
}
