package doctor

import "github.com/ktpswjz/httpserver/types"

type TlcTeamTaskRepository struct {
	SerialNo          uint64      `json:"serialNo" note:"任务的序列号"`
	TaskName          string      `json:"taskName" note:"任务的名称"`
	Content           string      `json:"content" note:"任务说明"`
	DescriptionPicUrl string      `json:"desImgUrl" note:"任务描述图片地址"`
	Level             uint        `json:"level" note:"任务评级"`
	CreateTime        *types.Time `json:"createTime" note:"任务创建时间"`
}
