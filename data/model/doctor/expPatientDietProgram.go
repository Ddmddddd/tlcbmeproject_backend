package doctor

import "github.com/ktpswjz/httpserver/types"

type ExpPatientDietProgram struct {
	SerialNo   uint64      `json:"serialNo" note:""`
	PatientID  *uint64     `json:"patientID" note:""`
	MealTime   string      `json:"mealTime" note:""`
	Recipe     string      `json:"recipe" note:""`
	Food       string      `json:"food" note:""`
	Quantity   string      `json:"quantity" note:""`
	Estimate   string      `json:"estimate" note:""`
	Memo       string      `json:"memo" note:""`
	UpdateDate *types.Time `json:"updateDate" note:""`
}
