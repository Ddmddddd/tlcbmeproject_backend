package doctor

import "github.com/ktpswjz/httpserver/types"

type ViewPatientBaseInfo struct {
	SerialNo            uint64      `json:"serialNo" note:"序号 主键，自增 例如:324"`
	PatientID           uint64      `json:"patientID" note:"用户ID 关联医生用户登录表用户ID 例如:232442"`
	Name                string      `json:"name" note:"姓名  例如:张三"`
	Sex                 *uint64     `json:"sex" note:"性别 0-未知, 1-男, 2-女，9-未说明 例如:1"`
	DateOfBirth         *types.Time `json:"dateOfBirth" note:"出生日期 例如:2000-12-12"`
	IdentityCardNumber  string      `json:"identityCardNumber" note:"身份证号 例如:330106200012129876"`
	Country             string      `json:"country" note:"国籍 GB/T 2659-2000 世界各国和地区名称 例如:中国"`
	Nation              string      `json:"nation" note:"民族 GB 3304-1991 中国各民族名称 例如:汉族"`
	NativePlace         string      `json:"nativePlace" note:"籍贯  例如:浙江杭州"`
	MarriageStatus      string      `json:"marriageStatus" note:"婚姻状况 GB/T 2261.2-2003 例如: "`
	EducationLevel      string      `json:"educationLevel" note:"文化程度 GB/T 4658-1984 文化程度 例如:大学"`
	JobType             string      `json:"jobType" note:"职业类别 GB/T 6565-1999 职业分类 例如: "`
	Photo               string      `json:"photo" note:"头像照片 例如:"`
	Nickname            string      `json:"nickname" note:"昵称 例如:蓝精灵"`
	PersonalSign        string      `json:"personalSign" note:"个性签名 例如:在那山的那边海的那边有一群蓝精灵"`
	Phone               string      `json:"phone" note:"联系电话 例如:13812344321"`
	Height              *uint64     `json:"height" note:"身高 单位：cm 例如:165"`
	Weight              *float64    `json:"weight" note:"体重 最后一次录入的体重，单位：kg 例如:60.5"`
	Waistline           *float64    `json:"waistline" note:"腰围 最后一次录入的腰围，单位：cm 例如:60.5"`
	Diagnosis           string      `json:"diagnosis" note:"诊断 例如:高血压，糖尿病"`
	DiagnosisMemo       string      `json:"diagnosisMemo" note:"诊断备注  例如:血压 150/90 mmHg，血糖 7.9 mmol/L"`
	PatientFeature      string      `json:"patientFeature" note:"患者特点 如有多个，之间用逗号分隔，例如：老年人，肥胖，残疾人 例如:老年人，肥胖"`
	ManageStatus        *uint64     `json:"manageStatus" note:"管理状态 0-管理中，1-迁出，2-已终止管理 例如:0"`
	ManageClass         string      `json:"manageClass" note:"管理分类 如有多个，之间用逗号分隔，例如：高血压，糖尿病 例如:高血压，糖尿病"`
	Ext                 interface{} `json:"ext" note:"扩展信息 保存不同管理分类的扩展信息 例如:{htn:{}, dm:{}}"`
	OrgCode             string      `json:"orgCode" note:"管理机构代码 管理该患者的机构 例如:897798"`
	DoctorID            *uint64     `json:"doctorID" note:"医生ID 例如:232"`
	DoctorName          string      `json:"doctorName" note:"医生姓名 例如:张三"`
	HealthManagerID     *uint64     `json:"healthManagerID" note:"健康管理师ID 例如:232"`
	HealthManagerName   string      `json:"healthManagerName" note:"健康管理师姓名 例如:李四"`
	ManageStartDateTime *types.Time `json:"manageStartDateTime" note:"管理开始时间 最近一次管理的开始时间，如果是刚注册，则默认为审核通过时间 例如:2018-07-03 13:15:00"`
	ComplianceRate      *uint64     `json:"complianceRate" note:"依从度 值为0至5，0表示依从度未知 例如:4"`
	ManageEndDateTime   *types.Time `json:"manageEndDateTime" note:"管理终止时间 该字段只有当ManageStatus字段为2时才有意义 例如:2018-08-03 13:15:00"`
	ManageEndReason     string      `json:"manageEndReason" note:"管理终止的原因 该字段只有当ManageStatus字段为2时才有意义 例如:亡故（2018-08-03，车祸死亡）"`
	FollowupTimes       *uint64     `json:"followupTimes" note:"累计随访次数 本次管理的累计随访次数 例如:3"`
	LastFollowupDate    *types.Time `json:"lastFollowupDate" note:"上次随访日期 例如:2018-06-15"`
	FollowupDate        *types.Time `json:"followupDate" note:""`
	GoalEnergy          *uint64     `json:"goalEnergy" note:"目标热量值"`
	GoalCarbohydrateMin *uint64     `json:"goalCarbohydrateMin" note:"碳水化合物提供能量占比最小值"`
	GoalCarbohydrateMax *uint64     `json:"goalCarbohydrateMax" note:"碳水化合物提供能量占比最大值"`
	GoalFatMin          *uint64     `json:"goalFatMin" note:"脂肪提供能量占比最小值"`
	GoalFatMax          *uint64     `json:"goalFatMax" note:"脂肪提供能量占比最大值"`
	GoalProteinMin      *uint64     `json:"goalProteinMin" note:"蛋白质提供能量占比最小值"`
	GoalProteinMax      *uint64     `json:"goalProteinMax" note:"蛋白质提供能量占比最大值"`
}
