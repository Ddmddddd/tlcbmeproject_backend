package doctor

type ViewApiLogStat struct {
	Uri     string   `json:"uri" note:"地址 调用接口地址 例如:/auth/info"`
	Count   uint64   `json:"count" note:""`
	AvgTime *float64 `json:"avgTime" note:""`
	MaxTime *uint64  `json:"maxTime" note:"耗时 接口耗时，单位纳秒 例如:1808"`
	MinTime *uint64  `json:"minTime" note:"耗时 接口耗时，单位纳秒 例如:1808"`
}
