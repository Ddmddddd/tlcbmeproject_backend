package doctor

type DoctorAddedRightFilter struct {
	UserID uint64 `json:"userID" required:"true" note:"用户ID 关联医生附加权限表用户ID 例如:232442"`
}
