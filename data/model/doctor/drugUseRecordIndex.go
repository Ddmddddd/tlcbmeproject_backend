package doctor

import "github.com/ktpswjz/httpserver/types"

type DrugUseRecordIndex struct {
	SerialNo           uint64      `json:"serialNo" note:"序号 主键，自增 例如:324"`
	PatientID          uint64      `json:"patientID" note:"用户ID 关联患者用户登录表用户ID 例如:232442"`
	DrugName           string      `json:"drugName" note:"药品名称  例如:氨氯地平片"`
	Dosage             string      `json:"dosage" note:"剂量 例如:5mg"`
	Freq               string      `json:"freq" note:"频次 例如:1次/日"`
	Status             uint64      `json:"status" note:"状态 0-使用中，1-已停用 例如:0"`
	FirstUseDateTime   *types.Time `json:"firstUseDateTime" note:"首次使用时间 例如:2018-01-01 08:50:00"`
	LastUseDateTime    *types.Time `json:"lastUseDateTime" note:"最近开始使用时间 例如:2018-01-01 08:50:00"`
	IsModified         uint64      `json:"isModified" note:"是否调整过用法 0-未调整过，1-调整过 例如:0"`
	CreateBy           string      `json:"createBy" note:"这条记录的创建者 如果是医生录入的，这里填医生姓名；如果是系统整理出来的，这里为空。 例如:张三"`
	LastModifyDateTime *types.Time `json:"lastModifyDateTime" note:"最后一次调整用法的时间 该字段只有当IsModified为1时才有意义 例如:2018-08-08 13:30:33"`
}
