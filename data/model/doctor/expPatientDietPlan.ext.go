package doctor

import "github.com/ktpswjz/httpserver/types"

type ExpPatientDietPlanGet struct {
	PatientID uint64 `json:"patientID" note:"患者id"`
}

type ExpPatientDietPlanDelete struct {
	SerialNo uint64 `json:"serialNo" note:"删除的序列号"`
}

type ExpPatientDietPlanSave struct {
	Plans []ExpPatientDietPlan `json:"plans"`
}

type ExpPatientDietPlanGetByLevel struct {
	PatientID uint64 `json:"patientID" note:"患者id"`
	Level     string `json:"level" note:"重要程度"`
}

type ExpPatientDietPlanRead struct {
	SerialNos []uint64 `json:"snos" note:""`
}

type ExpPatientDietPlanFour struct {
	SerialNo  uint64      `json:"serialNo" note:""`
	PatientID uint64      `json:"patientID" note:"患者id"`
	DietPlan  string      `json:"dietPlan" note:"饮食计划"`
	PicUrl    string      `json:"picUrl" note:"图片"`
	StartDate *types.Time `json:"startDate" note:"开始时间"`
}
