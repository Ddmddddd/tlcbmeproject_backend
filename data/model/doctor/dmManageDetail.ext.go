package doctor

import "github.com/ktpswjz/httpserver/types"

type DmManageDetailFilter struct {
	SerialNo        *uint64     `json:"serialNo" note:"序号 主键，自增 例如:324"`
	PatientID       *uint64     `json:"patientID" note:"用户ID 关联患者用户登录表用户ID 例如:232442"`
	ManageLevels    []uint64    `json:"manageLevels" note:"管理等级 当前管理等级，0-新患者，1-一级管理，2-二级管理 例如:0"`
	ManageDateStart *types.Time `json:"manageDateStart" note:"管理等级开始时间 例如:2018-07-03"`
	ManageDateEnd   *types.Time `json:"manageDateEnd" note:"管理等级结束时间 例如:2018-08-03"`
}
