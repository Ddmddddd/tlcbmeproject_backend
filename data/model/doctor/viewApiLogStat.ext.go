package doctor

type ViewApiLogStatFilter struct {
	Uri string `json:"uri" note:"地址 调用接口地址 例如:/auth/info"`
}

type ViewApiLogStatEx struct {
	ViewApiLogStat

	AvgTimeText string `json:"avgTimeText" note:"平均耗时文本信息"`
	MaxTimeText string `json:"maxTimeText" note:"最大耗时文本信息"`
	MinTimeText string `json:"minTimeText" note:"最小耗时文本信息"`
}
