package doctor

import "github.com/ktpswjz/httpserver/types"

type DoctorUserAuthsCreate struct {
	UserName    string `json:"userName" required:"true" note:"用户名 例如cdmwb123"`
	MobilePhone string `json:"mobilePhone" note:"手机号 绑定的手机号 例如:13818765432"`
	Email       string `json:"email" note:"电子邮箱 绑定的电子邮箱地址 例如:tlcbme_project@vico-lab.com"`
	Password    string `json:"password" required:"true" note:"登录密码"`
}

type DoctorUserAuthsInfo struct {
	UserID            uint64      `json:"userID" note:"用户ID 主键，自增 例如:"`
	UserName          string      `json:"userName" note:"用户名 如果注册时没有指定用户名，则系统默认分配一个用户名，格式为cdmwb+userID，例如cdmwb123 例如:zhangsan"`
	MobilePhone       string      `json:"mobilePhone" note:"手机号 绑定的手机号 例如:13818765432"`
	Email             string      `json:"email" note:"电子邮箱 绑定的电子邮箱地址 例如:tlcbme_project@vico-lab.com"`
	Status            uint64      `json:"status" note:"用户状态 0-正常，1-冻结，9-已注销 例如:0"`
	StatusText        string      `json:"statusText" note:"用户状态文本 0-正常，1-冻结，9-已注销 例如:0"`
	RegistDateTime    *types.Time `json:"registDateTime" note:"注册时间  例如:2018-07-02 15:00:00"`
	LoginCount        uint64      `json:"loginCount" note:"登录次数 累计登录次数 例如:12"`
	LastLoginDateTime *types.Time `json:"lastLoginDateTime" note:"最后一次登录时间  例如:2018-07-02 15:10:00"`
	UseSeconds        uint64      `json:"useSeconds" note:"累计使用时长 单位：秒 例如:24342"`
}

type DoctorUserAuthsInfoFilter struct {
	UserID      *uint64 `json:"userID" note:"用户ID 主键，自增 例如:"`
	UserName    *string `json:"userName" note:"用户名 如果注册时没有指定用户名，则系统默认分配一个用户名，格式为cdmwb+userID，例如cdmwb123 例如:zhangsan"`
	MobilePhone *string `json:"mobilePhone" note:"手机号 绑定的手机号 例如:13818765432"`
	Email       *string `json:"email" note:"电子邮箱 绑定的电子邮箱地址 例如:tlcbme_project@vico-lab.com"`
	Status      *uint64 `json:"status" note:"用户状态 0-正常，1-冻结，9-已注销 例如:0"`
}

type DoctorUserAuthsInfoLikeFilter struct {
	Account  string   `json:"account" note:"用户名或手机号或电子邮箱"`
	Statuses []uint64 `json:"statuses" note:"用户状态 0-正常，1-冻结，9-已注销 例如:0"`
}

type DoctorUserAuthsPassword struct {
	UserID   uint64 `json:"userID" required:"true" note:"用户ID"`
	Password string `json:"password" required:"true" note:"登录密码"`
}

type DoctorUserAuthsEdit struct {
	UserID      uint64 `json:"userID" required:"true" note:"用户ID 例如:111"`
	UserName    string `json:"userName" note:"用户名 如果注册时没有指定用户名，则系统默认分配一个用户名，格式为cdmwb+userID，例如cdmwb123 例如:zhangsan"`
	MobilePhone string `json:"mobilePhone" note:"手机号 绑定的手机号 例如:13818765432"`
	Email       string `json:"email" note:"电子邮箱 绑定的电子邮箱地址 例如:tlcbme_project@vico-lab.com"`
	Status      uint64 `json:"status" note:"用户状态 0-正常，1-冻结，9-已注销 例如:0"`
}

type DoctorUserAuthsChangePassword struct {
	OldPassword string `json:"oldPassword" note:"旧密码"`
	NewPassword string `json:"newPassword" note:"新密码"`
	Encryption  string `json:"encryption" note:"密码加密方法: 空-明文(默认); rsa-RSA密文(公钥通过调用获取RSA公钥接口获取)"`
}

type DoctorUserAuthsBaseInfo struct {
	UserID   uint64 `json:"id" note:"用户ID 例如:"`
	UserName string `json:"userName" note:"用户名 如果注册时没有指定用户名，则系统默认分配一个用户名，格式为cdmwb+userID，例如cdmwb123 例如:zhangsan"`
	Name     string `json:"name" note:"姓名 例如:张三"`
}
type DoctorUserAuthsCreateOneTime struct {
	UserName       string  `json:"userName" required:"true" note:"用户名 例如cdmwb123"`
	MobilePhone    string  `json:"mobilePhone" note:"手机号 绑定的手机号 例如:13818765432"`
	Password       string  `json:"password" required:"true" note:"登录密码"`
	Status         uint64  `json:"status" note:"用户状态 0-正常，1-冻结，9-已注销 例如:0"`
	Name           string  `json:"name" note:"姓名 例如:张三"`
	OrgCode        string  `json:"orgCode" note:"执业机构代码 该医生用户所在的执业机构 例如:897798"`
	UserType       *uint64 `json:"userType" note:"用户类别 0-医生，1-健康管理师 例如:0"`
	VerifiedStatus *uint64 `json:"verifiedStatus" note:"是否验证通过 0-未认证，1-已认证，2-认证不通过 例如:0"`
}
