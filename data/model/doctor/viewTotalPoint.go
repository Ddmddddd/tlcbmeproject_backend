package doctor

type ViewTotalPoint struct {
	PatientID   uint64 `json:"patientID" note:""`
	PatientName string `json:"patientName" note:""`
	TotalPoint  uint64 `json:"totalPoint" note:""`
}
