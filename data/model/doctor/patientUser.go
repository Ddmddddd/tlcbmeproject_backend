package doctor

import (
	"fmt"
	"github.com/ktpswjz/httpserver/types"
)

type PatientUserCreate struct {
	IdentityCardNumber string      `json:"identityCardNumber" required:"true" note:"身份证号 例如:330106200012129876"`
	Name               string      `json:"name" required:"true" note:"姓名  例如:张三"`
	Sex                *uint64     `json:"sex" required:"true" note:"性别 0-未知, 1-男, 2-女，9-未说明 例如:1"`
	DateOfBirth        *types.Time `json:"dateOfBirth" required:"true" note:"出生日期 例如:2000-12-12"`
	Phone              string      `json:"phone" required:"true" note:"联系电话 例如:13812344321"`
	EducationLevel     string      `json:"educationLevel" note:"文化程度 GB/T 4658-1984 文化程度 例如:大学"`
	JobType            string      `json:"jobType" note:"职业类别 GB/T 6565-1999 职业分类 例如: "`

	PatientFeatures []string       `json:"patientFeatures" note:"患者特点 例如：老年人，肥胖，残疾人 例如:老年人，肥胖"`
	ManageClasses   []*ManageClass `json:"manageClasses" required:"true" note:"管理分类 例如：高血压，糖尿病 例如:高血压，糖尿病"`

	DoctorID          *uint64 `json:"doctorID" required:"true" note:"医生ID 例如:232"`
	DoctorName        string  `json:"doctorName" note:"医生姓名 例如:张三"`
	HealthManagerID   *uint64 `json:"healthManagerID" required:"true" note:"健康管理师ID 例如:232"`
	HealthManagerName string  `json:"healthManagerName" note:"健康管理师姓名 例如:李四"`

	UserName string `json:"userName" note:"用户名(APP账号) 如果注册时没有指定用户名，则系统默认分配一个用户名，格式为cdmwb+随机数， 例如:cdmwb1808062217340002"`
	Password string `json:"password" note:"登录密码，如果注册时没有指定，系统默认设置为123456"`

	DmType        string   `json:"dmType" note:"糖尿病类型，1型糖尿病、2型糖尿病、妊娠糖尿病、其他类型， 例如:1型糖尿病"`
	DmMedications []string `json:"dmMedications" note:"糖尿病当前用药，口服降糖药、注射胰岛素， 例如:口服降糖药"`
	DietManage    string   `json:"dietManage" note:"患者是否纳入饮食管理，例如：纳入"`
}

func (s *PatientUserCreate) Verify() error {
	if s.IdentityCardNumber == "" {
		return fmt.Errorf("身份证号为空")
	}
	if s.Name == "" {
		return fmt.Errorf("姓名为空")
	}
	if s.Sex == nil {
		return fmt.Errorf("性别为空")
	}
	if s.DateOfBirth == nil {
		return fmt.Errorf("出生日期为空")
	}
	if s.Phone == "" {
		return fmt.Errorf("联系电话为空")
	}
	if len(s.ManageClasses) < 1 {
		return fmt.Errorf("管理分类为空")
	}
	if s.DoctorID == nil {
		return fmt.Errorf("医生为空")
	}
	if s.HealthManagerID == nil {
		return fmt.Errorf("管理师为空")
	}

	return nil
}

type PatientUserCreateEx struct {
	PatientUserCreate
	Actor

	OrgCode   string   `json:"orgCode" note:"管理机构代码 管理该患者的机构 例如:897798"`
	Height    *uint64  `json:"height" note:"身高 单位：cm 例如:165"`
	Weight    *float64 `json:"weight" note:"体重 最后一次录入的体重，单位：kg 例如:60.5"`
	Waistline *float64 `json:"waistline" note:"腰围 单位：cm 例如:165"`
}

type PatientUserCreateSuccess struct {
	UserID   uint64 `json:"userID" note:"用户ID 主键，自增 例如:"`
	UserName string `json:"userName" note:"用户名(APP账号) 如果注册时没有指定用户名，则系统默认分配一个用户名，格式为cdmwb+随机数， 例如:cdmwb1808062217340002"`
	Password string `json:"password" note:"登录密码，如果注册时没有指定，系统默认设置为123456"`
}

type PatientUserAudit struct {
	PatientID uint64 `json:"patientID" required:"true" note:"用户ID 关联患者用户登录表用户ID 例如:232442"`

	Status       uint64 `json:"status" required:"true" note:"审核状态 0-未审核，1-审核通过，2-审核不通过 例如:0"`
	RefuseReason string `json:"refuseReason" note:"审核不通过原因 最近一次审核不通过的原因。只有当状态为2时有效。 例如:信息不够完善"`

	PatientFeatures []string       `json:"patientFeatures" note:"患者特点 例如：老年人，肥胖，残疾人 例如:老年人，肥胖"`
	ManageClasses   []*ManageClass `json:"manageClasses" required:"true" note:"管理分类 例如：高血压，糖尿病 例如:高血压，糖尿病"`

	DoctorID          *uint64 `json:"doctorID" required:"true" note:"医生ID 例如:232"`
	DoctorName        string  `json:"doctorName" note:"医生姓名 例如:张三"`
	HealthManagerID   *uint64 `json:"healthManagerID" required:"true" note:"健康管理师ID 例如:232"`
	HealthManagerName string  `json:"healthManagerName" note:"健康管理师姓名 例如:李四"`

	DmType        string   `json:"dmType" note:"糖尿病类型，1型糖尿病、2型糖尿病、妊娠糖尿病、其他类型， 例如:1型糖尿病"`
	DmMedications []string `json:"dmMedications" note:"糖尿病当前用药，口服降糖药、注射胰岛素， 例如:口服降糖药"`
	DietManage    string   `json:"dietManage" note:"是否纳入饮食管理，例如:纳入"`
}
type PatientInfo struct {
	IdentityCardNumber string      `json:"identityCardNumber" required:"true" note:"身份证号 例如:330106200012129876"`
	Name               string      `json:"patientName" required:"true" note:"姓名  例如:张三"`
	Sex                *uint64     `json:"sex" required:"true" note:"性别 0-未知, 1-男, 2-女，9-未说明 例如:1"`
	DateOfBirth        *types.Time `json:"dateOfBirth" required:"true" note:"出生日期 例如:2000-12-12"`
	Phone              string      `json:"phone" required:"true" note:"联系电话 例如:13812344321"`
	EducationLevel     string      `json:"educationLevel" note:"文化程度 GB/T 4658-1984 文化程度 例如:大学"`
	JobType            string      `json:"jobType" note:"职业类别 GB/T 6565-1999 职业分类 例如: "`
	Height             *uint64     `json:"height" note:"身高 单位：cm 例如:165"`
	Weight             *float64    `json:"weight" note:"体重 最后一次录入的体重，单位：kg 例如:60.5"`
	Waistline          *float64    `json:"waistline" note:"腰围 最后一次录入的腰围，单位：cm 例如:60.5"`
	Diagnosis          string      `json:"diagnosis" note:"诊断 例如:高血压，糖尿病"`
	DiagnosisMemo      string      `json:"diagnosisMemo" note:"诊断备注  例如:血压 150/90 mmHg，血糖 7.9 mmol/L"`
	Country            string      `json:"country"`
}
type PatientUserAuditEx struct {
	PatientUserAudit
	PatientInfo     `json:"patientInfo"`
	ReviewerOrgCode string     `json:"reviewerOrgCode" note:"审核人执业机构代码 例如:897798"`
	ReviewerID      uint64     `json:"reviewerID" note:"审核人ID 审核人对应的用户ID 例如:32423"`
	ReviewerName    string     `json:"reviewerName" note:"审核人姓名 例如:张三"`
	ReviewDateTime  types.Time `json:"reviewDateTime" note:"审核时间 例如:2018-07-03 13:00:00"`
}

func (s *PatientUserAuditEx) CopyToEx(target *PatientUserBaseInfoModify) {
	if target == nil {
		return
	}
	target.UserID = s.PatientID
	target.Height = s.Height
	target.Weight = s.Weight
	target.Name = s.Name
	target.DateOfBirth = s.DateOfBirth
	target.Diagnosis = s.Diagnosis
	target.DiagnosisMemo = s.DiagnosisMemo
	target.Sex = s.Sex
	target.IdentityCardNumber = s.IdentityCardNumber
	target.Phone = s.Phone
	target.EducationLevel = s.EducationLevel
	target.JobType = s.JobType
	target.VisitOrgCode = s.ReviewerOrgCode
	target.HealthManagerID = s.HealthManagerID
	target.Waistline = s.Waistline
}
