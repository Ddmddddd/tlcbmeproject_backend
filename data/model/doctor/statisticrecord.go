package doctor

type Statisticrecord struct {
	Serialno uint64   `json:"serialno" note:"序号"`
	Type     uint64   `json:"type" note:"统计类型，1-管理人数，2-活跃人数，3-血压数据，4-预警血压数据"`
	Year     uint64   `json:"year" note:"年"`
	Month    uint64   `json:"month" note:"月"`
	Count    *uint64  `json:"count" note:"数据量"`
	Rate     *float64 `json:"rate" note:"达标率、当日解决率"`
	Orgcode  string   `json:"orgcode" note:""`
}
