package doctor

import (
	"bytes"
	"github.com/ktpswjz/httpserver/types"
	"strconv"
	"strings"
	"time"
)

type SportDietRecordDataFilter struct {
	HappenStartDate *types.Time `json:"measureStartDate" note:"记录开始时间 记录运动或饮食的时间 例如:2018-07-03 14:45:00"`
	HappenEndDate   *types.Time `json:"measureEndDate" note:"记录结束时间 记录运动或饮食的时间 例如:2018-08-03 14:45:00"`
}

type SportDietRecordDataFilterEx struct {
	SportDietRecordDataFilter

	PatientID uint64 `json:"patientID" required:"true" note:"用户ID 关联患者用户登录表用户ID 例如:232442"`
}

type StepRecordDataFilter struct {
	HappenDate *types.Time `json:"measureDate" note:"记录时间 记录计步的时间 例如:2018-07-03 14:45:00"`
}

type StepRecordData struct {
	SerialNo       uint64      `json:"serialNo" note:"序号 主键，自增 例如:324"`
	StepCount      uint64      `json:"stepCount" note:"步数 仅用于步行 例如：3699"`
	Memo           string      `json:"memo" note:"备注  例如:"`
	HappenDateTime *types.Time `json:"happenDateTime" note:"发生时间 实际发生的时间 例如:2018-07-03 14:45:00"`
}

type SportRecordData struct {
	StepCount      uint64      `json:"stepCount" note:"步数 仅用于步行 例如：3699"`
	Memo           string      `json:"memo" note:"备注  例如:"`
	HappenDateTime *types.Time `json:"happenDateTime" note:"发生时间 实际发生的时间 例如:2018-07-03 14:45:00"`
	SportsType     string      `json:"sportsType" required:"true" note:"运动类型 目前其值为以下之一：步行 慢跑 爬楼梯 跳绳 做操 其他"`
	DurationTime   uint64      `json:"durationTime" note:"持续时间 例如：60 单位默认为min"`
	Intensity      string      `json:"intensity" note:"运动强度 例如：低 中 高"`
}

type SportRecordForApp struct {
	SerialNo       uint64      `json:"serialNo" note:"序号 主键，自增 例如:324"`
	StepCount      uint64      `json:"stepCount" note:"步数 仅用于步行 例如：3699"`
	Photo          string      `json:"photo" note:"运动照片 例如:"`
	Memo           string      `json:"memo" note:"备注  例如:"`
	HappenDateTime *types.Time `json:"happenDateTime" note:"发生时间 实际发生的时间 例如:2018-07-03 14:45:00"`
	SportsType     string      `json:"sportsType" required:"true" note:"运动类型 目前其值为以下之一：步行 慢跑 爬楼梯 跳绳 做操 其他"`
	DurationTime   uint64      `json:"durationTime" note:"持续时间 例如：60 单位默认为min"`
	Intensity      string      `json:"intensity" note:"运动强度 例如：低 中 高"`
	Description    string      `json:"description" note:"记录运动项目"`
}

type SportRecordForAppEx struct {
	SportRecordForApp

	PatientID uint64 `json:"patientID" note:"用户ID 关联患者用户登录表用户ID 例如:232442"`
}

type DietRecordData struct {
	Kinds          string      `json:"kinds" required:"true" note:"食物种类 多种类用逗号分开 例如：主食,肉类,蔬菜,水果,蛋类"`
	Appetite       string      `json:"appetite" required:"true" note:"食量 对应上述食物种类 例如：200,60,80,120,40 单位默认为g"`
	Photo          string      `json:"photo" note:"食物照片 例如:"`
	Memo           string      `json:"memo" note:"备注  例如:"`
	HappenDateTime *types.Time `json:"happenDateTime" note:"发生时间 实际发生的时间 例如:2018-07-03 14:45:00"`
}

type DietRecordForApp struct {
	SerialNo       string      `json:"serialNo" note:"序号组 例如:324,248"`
	Kinds          string      `json:"kinds" required:"true" note:"食物种类 多种类用逗号分开 例如：主食,肉类,蔬菜,水果,蛋类"`
	Appetite       string      `json:"appetite" required:"true" note:"食量 对应上述食物种类 例如：200,60,80,120,40 单位默认为g"`
	Photo          string      `json:"photo" note:"食物照片 例如:"`
	Memo           string      `json:"memo" note:"备注  例如:"`
	HappenDateTime *types.Time `json:"happenDateTime" note:"发生时间 实际发生的时间 例如:2018-07-03 14:45:00"`
	HappenPlace    string      `json:"happenPlace" note:"发生地点"`
	Type           uint64      `json:"type" note:"记录时刻 记录饮食的时刻 例如:1早上 2中午 3晚上 0自定义"`
	DietRecordNo   uint64      `json:"dietRecordNo" note:"记录序号"`
}

type DietRecordForAppEx struct {
	//DietRecordForApp
	SerialNo       string      `json:"serialNo" note:"序号组 例如:324,248"`
	Kinds          string      `json:"kinds" required:"true" note:"食物种类 多种类用逗号分开 例如：主食,肉类,蔬菜,水果,蛋类"`
	Appetite       string      `json:"appetite" required:"true" note:"食量 对应上述食物种类 例如：200,60,80,120,40 单位默认为g"`
	Photo          string      `json:"photo" note:"食物照片 例如:"`
	Memo           string      `json:"memo" note:"备注  例如:"`
	HappenDateTime *types.Time `json:"happenDateTime" note:"发生时间 实际发生的时间 例如:2018-07-03 14:45:00"`
	HappenPlace    string      `json:"happenPlace" note:"发生地点"`
	Type           uint64      `json:"type" note:"记录时刻 记录饮食的时刻 例如:1早上 2中午 3晚上 0自定义"`
	DietRecordNo   uint64      `json:"dietRecordNo" note:"记录序号"`
	PatientID      uint64      `json:"patientID" note:"用户ID 关联患者用户登录表用户ID 例如:232442"`
}

func (s *DietRecordForAppEx) ConvertToDietRecordList() []*SportDietRecord {
	kinds := strings.Split(s.Kinds, ",")
	appetite := strings.Split(s.Appetite, ",")
	count := len(kinds)
	var timePoint string
	switch s.Type {
	case 1:
		timePoint = string("早")
	case 2:
		timePoint = string("中")
	case 3:
		timePoint = string("晚")
	default:
		timePoint = string("")
	}
	var buf bytes.Buffer
	arrDietRecord := make([]*SportDietRecord, 0)
	now := types.Time(time.Now())
	for i := 0; i < count; i++ {
		dietRecord := &SportDietRecord{
			SerialNo:       uint64(0),
			PatientID:      s.PatientID,
			RecordType:     string("饮食"),
			TimePoint:      timePoint,
			ItemName:       kinds[i],
			ItemValue:      appetite[i],
			ItemUnit:       "",
			Photo:          s.Photo,
			Memo:           s.Memo,
			HappenDateTime: s.HappenDateTime,
			InputDateTime:  &now,
		}
		if dietRecord.ItemValue != "" {
			dietRecord.ItemUnit = "g"
		}
		buf.WriteString(dietRecord.ItemName)
		buf.WriteString(dietRecord.ItemValue)
		buf.WriteString(dietRecord.ItemUnit)
		dietRecord.Description = buf.String()
		buf.Reset()
		arrDietRecord = append(arrDietRecord, dietRecord)
	}
	return arrDietRecord
}

func (s *DietRecordForAppEx) ConvertToDietRecord() *SportDietRecord {
	kinds := strings.Split(s.Kinds, ",")
	appetite := strings.Split(s.Appetite, ",")
	var timePoint string
	switch s.Type {
	case 1:
		timePoint = string("早")
	case 2:
		timePoint = string("中")
	case 3:
		timePoint = string("晚")
	default:
		timePoint = string("")
	}
	var buf bytes.Buffer
	now := types.Time(time.Now())
	dietRecord := &SportDietRecord{
		SerialNo:       uint64(0),
		PatientID:      s.PatientID,
		RecordType:     string("饮食"),
		TimePoint:      timePoint,
		ItemName:       kinds[0],
		ItemValue:      appetite[0],
		ItemUnit:       "",
		Photo:          s.Photo,
		Memo:           s.Memo,
		HappenDateTime: s.HappenDateTime,
		HappenPlace:    s.HappenPlace,
		InputDateTime:  &now,
		DietRecordNo:   s.DietRecordNo,
	}
	if dietRecord.ItemValue != "" {
		dietRecord.ItemUnit = "g"
	}
	buf.WriteString(dietRecord.ItemName)
	buf.WriteString(dietRecord.ItemValue)
	buf.WriteString(dietRecord.ItemUnit)
	dietRecord.Description = buf.String()
	buf.Reset()

	if s.SerialNo != "" {
		serialNo := strings.Split(s.SerialNo, ",")
		dietRecord.SerialNo, _ = strconv.ParseUint(serialNo[0], 10, 64)
	}

	return dietRecord
}

type WeRunCryptDataInput struct {
	EncryptedData string `json:"encryptedData" note:""`
	Iv            string `json:"iv" note:""`
	SessionKey    string `json:"sessionKey" note:""`
	PatientID     uint64 `json:"patientID" required:"true" note:"用户ID 关联患者用户登录表用户ID 例如:232442"`
}

type WeRunData struct {
	TimeStamp uint64 `json:"timestamp" note:"时间"`
	Step      uint64 `json:"step" note:"步数"`
}

type WeRunDataList struct {
	StepInfoList []*WeRunData `json:"stepInfoList"`
}

type SportDietRecordFilter struct {
	SerialNo uint64 `json:"serialNo" note:"序号 主键，自增 例如:324"`
}
