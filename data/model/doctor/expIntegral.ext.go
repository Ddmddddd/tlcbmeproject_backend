package doctor

type ExpIntegralUnreadGet struct {
	PatientID uint64 `json:"patientID" note:""`
	ReadFlag  uint64 `json:"readFlag" note:""`
}

type ExpIntegralRead struct {
	Snos []uint64 `json:"snos"`
}

type Integral struct {
	Diet  uint64 `json:"diet"`
	Weigh uint64 `json:"weigh"`
	Sport uint64 `json:"sport"`
	Other uint64 `json:"other"`
}
