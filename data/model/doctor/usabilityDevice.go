package doctor

import "github.com/ktpswjz/httpserver/types"

type UsabilityDevice struct {
	SerialNo      uint64      `json:"serialNo" note:"序号 主键，自增 例如:1"`
	UserName      string      `json:"userName" note:"用户名 关联患者用户登录表用户名 例如:aa"`
	AppId         string      `json:"appId" note:"应用标识 唯一标识应用 例如:abcdefghijklmn"`
	DeviceIMEI    string      `json:"deviceIMEI" note:"设备序列号 当前设备的序列号 例如:cn64wksj3sm8e"`
	PhoneBrand    string      `json:"phoneBrand" note:"手机品牌 当前手机的品牌 例如:vivo x9 plus"`
	SystemVersion string      `json:"systemVersion" note:"系统版本 手机操作系统版本 例如:Android 7.0"`
	RecordTime    *types.Time `json:"recordTime" note:"记录时间 最后一次记录的时间 例如:2018-12-12 08:00:00"`
}
