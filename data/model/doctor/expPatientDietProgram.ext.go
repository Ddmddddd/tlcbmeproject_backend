package doctor

type ExpPatientDietProgramUpdate struct {
	DietProgramList interface{} `json:"dietProgramList"`
}

type ExpPatientDietProgramWholeUpdate struct {
	DietProgramList []*ExpPatientDietProgram `json:"dietProgramList"`
}
