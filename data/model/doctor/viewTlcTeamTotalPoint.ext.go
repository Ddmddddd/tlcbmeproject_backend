package doctor

type TlcTeamRankWholeInfo struct {
	Percent       float64         `json:"percent" note:"队伍的积分超过大队内的比例"`
	TeamRank      uint64          `json:"teamRank" note:"队伍所处排名"`
	GroupRankData []*TeamRankData `json:"groupRankData" note:"大队内的队伍的积分排名数据"`
}

type TeamRankData struct {
	TeamNickName      string `json:"teamNickName" note:"队伍名称"`
	TeamTotalIntegral uint64 `json:"teamTotalIntegral" note:"队伍积分"`
	TeamRank          uint64 `json:"teamRank" note:"队伍所处大队内的排名"`
	Flag              int    `json:"flag" note:"大队内是否为自己小队的标识，0为非自己小队，1为自己小队"`
}
