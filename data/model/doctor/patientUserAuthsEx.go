package doctor

type PatientUserAuthsEx struct {
	SerialNo     uint64 `json:"serialNo" note:"序号 主键，自增 例如:324"`
	UserID       uint64 `json:"userID" note:"用户ID 关联患者用户登录表用户ID 例如:232442"`
	IdentityType string `json:"identityType" note:"账号类型 微信、QQ、微博 例如:微信"`
	Identitier   string `json:"identitier" note:"账号 例如微信unionID 例如:32424243"`
	Credential   string `json:"credential" note:"凭证 例如access_token 例如:3rf3242445fewfw34345f54"`
	Status       string `json:"status" note:"状态 0-正常，1-禁用 例如:0"`
}
