package doctor

import "github.com/ktpswjz/httpserver/types"

type ExpKnowledgeTypeInput struct {
	Type           string      `json:"type" note:"知识类型，例如：“粗粮”"`
	Status 		   uint64      `json:"status" note:"状态"`
	DayIndex	   uint64	   `json:"dayIndex" note:"对应计划的日期"`
}

type ExpKnowledgeEditInput struct {
	SerialNo       uint64      `json:"serialNo" note:""`
	KnoTitle       string      `json:"knoTitle" note:"知识标题"`
	Content        string      `json:"content" note:"知识内容，富文本形式"`
	ImageUrl       *string     `json:"imageUrl" note:"知识配图"`
	EditorID       uint64      `json:"editorID" note:"编辑者id"`
	UpdateTime     *types.Time `json:"updateTime" note:"更新时间"`
}


type ExpKnowledgeAddInput struct {
	Type           string      `json:"type" note:"知识类型，例如：“粗粮”"`
	KnoTitle       string      `json:"knoTitle" note:"知识标题"`
	Content        string      `json:"content" note:"知识内容，富文本形式"`
	ImageUrl       *string     `json:"imageUrl" note:"知识配图"`
	CreateDateTime *types.Time `json:"createDateTime" note:"创建时间"`
	EditorID       uint64      `json:"editorID" note:"编辑者id"`
}

type ExpKnowledgeDeleteInput struct {
	SerialNo       uint64      `json:"serialNo" note:""`
	EditorID       uint64      `json:"editorID" note:"编辑者id"`
	Status 		   uint64      `json:"status" note:"状态"`
	UpdateTime     *types.Time `json:"updateTime" note:"更新时间"`
}

type ExpKnowledgeSerialNoInput struct {
	SerialNo       uint64      `json:"serialNo" note:""`
}