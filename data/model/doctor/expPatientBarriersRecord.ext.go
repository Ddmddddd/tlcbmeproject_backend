package doctor

type ExpPatientBarriersRecordFilter struct {
	SerialNo       uint64      `json:"serialNo" note:"序号"`
	PatientID      uint64      `json:"patientID" note:"患者ID"`
}