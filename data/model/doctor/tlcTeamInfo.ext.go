package doctor

import "github.com/ktpswjz/httpserver/types"

type TlcTeamCreateInfo struct {
	TeamNickName   string `json:"teamNickName" note:"队伍名称"`
	BelongToGroup  string `json:"belongToGroup" note:"所属大队"`
	MemNumUp       uint64 `json:"memNumUp" note:"团队成员人数上限"`
	TeamGenerateID string `json:"teamGenerateID" note:"小队生成的id"`
	CreatorID      uint64 `json:"creatorID" note:"小队创建者的id"`
}

type TlcTeamCreatePreInfoToApp struct {
	TeamGenerateID string `json:"teamGenerateID" note:"小队生成的id"`
	PatientName    string `json:"patientName" note:"创建小队的用户的昵称"`
}

type TlcTeamQueryByGroup struct {
	BelongToGroup string `json:"belongToGroup" note:"所属大队"`
}

type TlcTeamQueryBySearch struct {
	SearchInput string `json:"searchInput"`
}

type TlcTeamInfoIncMemNum struct {
	SerialNo       uint64      `json:"serialNo" note:"主键"`
	TeamNickName   string      `json:"teamNickName" note:"队伍名称"`
	BelongToGroup  string      `json:"belongToGroup" note:"所属大队"`
	MemNum         uint64      `json:"memNum" note:"队伍当前人数"`
	MemNumUp       uint64      `json:"memNumUp" note:"团队成员人数上限"`
	TeamGenerateID string      `json:"teamGenerateID" note:"小队生成的id"`
	CreatorID      uint64      `json:"creatorID" note:"小队创建者的id"`
	CreateTime     *types.Time `json:"createTime" note:"小队创建时间"`
}

type TlcTeamInfoToAppFuncArea struct {
	CreatorID     uint64                           `json:"creatorID" note:"小队创建者的id"`
	TeamNickName  string                           `json:"teamNickName" note:"队伍名称"`
	BelongToGroup string                           `json:"belongToGroup" note:"所属大队"`
	MemNum        uint64                           `json:"memNum" note:"队伍当前人数"`
	TaskRules     []*TlcTeamTaskMapWithIntegralTem `json:"taskRules" note:"队伍收到的任务对应的积分规则（映射后）"`
	Point         uint64                           `json:"point" note:"队伍积分"`
	PointList     []*TlcTeamIntegralRecord         `json:"pointList" note:"队伍积分记录"`
}

type TlcTeamInfoFromAppFuncArea struct {
	SerialNo uint64 `json:"serialNo" note:"主键"`
}

type TlcTeamInfoGroupDict struct {
	Text  string `json:"text" note:"对应前端过滤器所需的字典格式"`
	Value string `json:"value" note:"对应前端过滤器所需的字典格式"`
}

type TlcTeamInfoTeamDict struct {
	Text  string `json:"text" note:"对应前端过滤器所需的字典格式"`
	Value string `json:"value" note:"对应前端过滤器所需的字典格式"`
}
