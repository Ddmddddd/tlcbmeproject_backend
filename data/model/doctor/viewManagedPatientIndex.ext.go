package doctor

import "tlcbme_project/data/model"

type ViewManagedPatientIndexListFilter struct {
	StartDaysAgo        *uint64      `json:"startDaysAgo" note:"几天前开始管理"`
	ManageStatuses      []uint64     `json:"manageStatuses" note:"管理状态"`
	PatientName         string       `json:"patientName" note:"姓名  例如:张三"`
	Order               *model.Order `json:"order" note:"排序"`
	PatientActiveDegree []uint64     `json:"patientActiveDegree" note:"活跃度"`
}

type ViewManagedPatientIndexListFilterEx struct {
	ViewManagedPatientIndexListFilter

	OrgCode            string  `json:"orgCode" note:"管理机构代码 管理该患者的机构 例如:897798"`
	DoctorID           *uint64 `json:"doctorID" note:"医生ID 例如:232"`
	HealthManagerID    *uint64 `json:"healthManagerID" note:"健康管理师ID 例如:232"`
	ViewOrgAllPatients bool    `json:"viewOrgAllPatients" note:"是否查看机构所有患者 例如:true"`

	HealthManagerIDList []uint64 `json:"healthManagerIDList"`
	GroupContentList    []string `json:"groupContentList"`
	TeamContentList     []string `json:"teamContentList"`
	CountryContentList     []string `json:"countryContentList"`
}

type ViewManagedPatientIndexEx struct {
	ViewManagedPatientIndex

	SexText string `json:"sexText" note:"性别 0-未知, 1-男, 2-女，9-未说明 例如:男"`
	Age     int64  `json:"age" note:"年龄，单位岁，如60"`
	Tag     string `json:"tag" note:"人员标签"`
	//BelongToGroup string `json:"belongToGroup" note:"患者参与组队管理时的所属大队"`
	//TeamNickName  string `json:"teamNickName" note:"患者参与组队管理时所属的小队"`
}
