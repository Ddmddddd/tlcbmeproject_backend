package doctor

import "github.com/ktpswjz/httpserver/types"

type ManagementPlan struct {
	SerialNo           uint64      `json:"serialNo" note:"序号 主键，自增 例如:324"`
	PatientID          uint64      `json:"patientID" note:"用户ID 关联患者用户登录表用户ID 例如:232442"`
	PlanType           string      `json:"planType" note:"计划类型 该字段用于标识计划类型，用于表示该计划用于哪种慢病。计划类型为以下值之一：全局、高血压、糖尿病、慢阻肺。其中“全局”表示该计划不仅限于单种慢病。计划类型的值在后续会根据系统需求不断增加。 例如:高血压"`
	PlanName           string      `json:"planName" note:"计划名称 管理计划的名称，例如：管理计划、高血压管理计划 例如:管理计划"`
	Content            interface{} `json:"content" note:"管理计划内容 JSON格式保存 例如:"`
	PlanStartDateTime  *types.Time `json:"planStartDateTime" note:"计划制定时间 例如:2018-07-03 14:45:00"`
	PlannerID          *uint64     `json:"plannerID" note:"计划制定人ID 例如:234"`
	PlannerName        string      `json:"plannerName" note:"计划制定人姓名 例如:张三"`
	LastModifyDateTime *types.Time `json:"lastModifyDateTime" note:"最后修改时间 计划最后修改的时间 例如:2018-09-03 14:45:00"`
	ModifierID         *uint64     `json:"modifierID" note:"最后修改者ID 例如:555"`
	ModifierName       string      `json:"modifierName" note:"最后修改者姓名 例如:李四"`
	Status             uint64      `json:"status" note:"计划状态 0-未开始，1-使用中，2-已作废 例如:0"`
}
