package doctor

import "github.com/ktpswjz/httpserver/types"

type ExpIntegral struct {
	SerialNo       uint64      `json:"serialNo" note:""`
	PatientID      uint64 	   `json:"patientID" note:""`
	Integral       uint64      `json:"integral" note:""`
	Reason         string      `json:"reason" note:""`
	CreateDateTime *types.Time `json:"createDateTime" note:""`
	ReadFlag       uint64      `json:"readFlag" note:""`
}
