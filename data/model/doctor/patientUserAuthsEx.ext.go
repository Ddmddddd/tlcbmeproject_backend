package doctor

type PatientUserBindWeChat struct {
	UserID   string `json:"UserID" note:"用户名 例如:123456"`
	OpenID   string `json:"openID" note:"微信open-id 例如:32424243"`
}

type PatientWeChatStatus struct {
	Status   uint64 `json:"status" note:"用户与微信的绑定状态 0-正常，1-禁用，例如:0"`
}

type WeChatCode2SessionResult struct {
	SessionKey	string	`json:"session_key" note:"会话密匙"`
	OpenID 	 	string 	`json:"openID" note:"微信open-id 例如:32424243"`
	UnionID		string 	`json:"unionid" note:"用户在开放平台的唯一标识符，在满足 UnionID 下发条件的情况下会返回"`
	ErrCode		uint64	`json:"errcode" note:"错误码"`
	ErrMsg		string  `json:"errmsg" note:"错误信息"`
}