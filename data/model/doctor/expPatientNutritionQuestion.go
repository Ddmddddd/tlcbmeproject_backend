package doctor

import "github.com/ktpswjz/httpserver/types"

type ExpPatientNutritionQuestion struct {
	SerialNo   uint64      `json:"serialNo" note:""`
	PatientID  *uint64     `json:"patientID" note:""`
	Answer     string      `json:"answer" note:""`
	CreateDate *types.Time `json:"createDate" note:""`
}
