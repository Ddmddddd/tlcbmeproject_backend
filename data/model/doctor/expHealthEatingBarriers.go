package doctor

type ExpHealthEatingBarriers struct {
	SerialNo    uint64 `json:"serialNo" note:"序号"`
	BarrierName string `json:"barrierName" note:"健康饮食障碍"`
	Description string `json:"description" note:"描述"`
}
