package doctor

type PatientTagInfo struct {
	PatientID uint64 `json:"patientID"  note:"患者id,用于与标签关联"`
}

type PatientTag struct {
	Tag_id uint64 `json:"tag_id" note:"标签的id"`
}

type PatientToTagDelete struct {
	PatientID uint64 `json:"patientID" note:"患者id，用于与标签关联"`
	Tag_id    uint64 `json:"tag_id" note:"标签的id"`
}

type PatientToTagAdd struct {
	PatientID uint64 `json:"patientID" note:"患者id,用于与标签关联"`
	Tag_id    uint64 `json:"tag_id" note:"标签的id"`
}
