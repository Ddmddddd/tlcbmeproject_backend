package doctor

import "github.com/ktpswjz/httpserver/types"

type NutritionalAnalysisRecord struct {
	SerialNo         uint64      `json:"serialNo" note:""`
	Conclusion       string      `json:"conclusion" note:"分析结论"`
	Grade            float64     `json:"grade" note:"营养评分分数"`
	GradeLevel       uint64      `json:"gradeLevel" note:"营养评分等级 1- 一般，2- 良好，3-优秀"`
	FoodKinds        string      `json:"foodKinds" note:"食物种类，主食，蔬菜等"`
	Na               *float64    `json:"na" note:"限制性营养素 钠"`
	Sugar            *float64    `json:"sugar" note:"限制性营养素 添加糖"`
	Fat              *float64    `json:"fat" note:"限制性营养素 脂肪"`
	Protein          *float64    `json:"protein" note:"推荐营养素 蛋白质"`
	DietaryFiber     *float64    `json:"dietaryFiber" note:"推荐营养素 蛋白质"`
	Ca               *float64    `json:"ca" note:"推荐营养素 钙"`
	K                *float64    `json:"K" note:"推荐营养素 钙"`
	Mg               *float64    `json:"mg" note:"推荐营养素 镁"`
	Fe               *float64    `json:"fe" note:"推荐营养素 铁"`
	Va               *float64    `json:"va" note:"推荐营养素 维生素A"`
	Vb1              *float64    `json:"vb1" note:"推荐营养素 维生素B1"`
	Vb2              *float64    `json:"vb2" note:"推荐营养素 维生素B2"`
	Vc               *float64    `json:"vc" note:"推荐营养素 维生素C"`
	Vd               *float64    `json:"vd" note:"推荐营养素 维生素D"`
	Zn               *float64    `json:"zn" note:"推荐营养素 锌"`
	Cholesterol      *float64    `json:"cholesterol" note:"其他营养素 胆固醇"`
	FolicAcid        *float64    `json:"folicAcid" note:"其他营养素 叶酸"`
	I                *float64    `json:"I" note:"其他营养素 碘"`
	Se               *float64    `json:"se" note:"其他营养素 硒"`
	Bb6              *float64    `json:"bb6" note:"其他营养素 维生素B6"`
	Ve               *float64    `json:"ve" note:"其他营养素 维生素E"`
	PatientID        uint64      `json:"patientID" note:"用户ID"`
	AnalysisDateTime *types.Time `json:"analysisDateTime"`
	Energy           float64     `json:"energy"`
	Carbohydrate     *float64     `json:"carbohydrate"`
	DietPlan         DietPlanRecord  `json:"dietPlan" note:"饮食目标完成情况"`
}
