package doctor

import "github.com/ktpswjz/httpserver/types"

type AppVersionHistory struct {
	SerialNo       uint64      `json:"serialNo" note:"序号 例如:1"`
	VersionCode    uint64      `json:"versionCode" note:"版本代码 例如:1"`
	VersionName    string      `json:"versionName" note:"版本名称 例如:1.0.0.1"`
	UpdateDate     *types.Time `json:"updateDate" note:"更新时间  例如:2018-07-06 14:55:00"`
	UpdateContent  string      `json:"updateContent" note:"更新内容 例如:修改Bug"`
	IsForced       uint64      `json:"isForced" note:"是否强制更新 0-否，1-强制更新 例如:0"`
	CompMinVersion uint64      `json:"compMinVersion" note:"最小兼容版本代码 例如:1"`
}
