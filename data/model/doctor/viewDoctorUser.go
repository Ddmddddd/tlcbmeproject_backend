package doctor

type ViewDoctorUser struct {
	UserID         uint64  `json:"userID" note:"用户ID 主键，自增 例如:"`
	UserName       string  `json:"userName" note:"用户名 如果注册时没有指定用户名，则系统默认分配一个用户名，格式为cdmwb+userID，例如cdmwb123 例如:zhangsan"`
	MobilePhone    string  `json:"mobilePhone" note:"手机号 绑定的手机号 例如:13818765432"`
	Email          string  `json:"email" note:"电子邮箱 绑定的电子邮箱地址 例如:tlcbme_project@vico-lab.com"`
	Status         uint64  `json:"status" note:"用户状态 0-正常，1-冻结，9-已注销 例如:0"`
	LoginCount     uint64  `json:"loginCount" note:"登录次数 累计登录次数 例如:12"`
	Name           string  `json:"name" note:"姓名  例如:张三"`
	Sex            *uint64 `json:"sex" note:"性别 0-未知, 1-男, 2-女，9-未说明 例如:1"`
	Phone          string  `json:"phone" note:"联系电话 例如:13812344321"`
	Photo          string  `json:"photo" note:"头像照片 例如:"`
	OrgCode        string  `json:"orgCode" note:"执业机构代码 该医生用户所在的执业机构 例如:897798"`
	UserType       *uint64 `json:"userType" note:"用户类别 0-医生，1-健康管理师，11-医生兼健康管理师 例如:0"`
	VerifiedStatus *uint64 `json:"verifiedStatus" note:"是否验证通过 0-未认证，1-已认证，2-认证不通过 例如:0"`
	OrgName        string  `json:"orgName" note:"机构名称  例如:第一人民医院"`
}
