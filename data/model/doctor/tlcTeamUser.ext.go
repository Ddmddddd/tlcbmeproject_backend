package doctor

import (
	"github.com/ktpswjz/httpserver/types"
	"time"
)

type TlcUserJoinTeamInfo struct {
	UserID       uint64 `json:"patientID" note:"用户id"`
	TeamSerialNo uint64 `json:"teamSerialNo" note:"队伍的序列号"`
}

type TlcTeamUserFuncFromApp struct {
	UserID       uint64 `json:"patientID" note:"用户id"`
	TeamSerialNo uint64 `json:"teamSerialNo" note:"队伍的序列号"`
}

type TlcTeamUserFuncFromAppDiets struct {
	TlcTeamUserFuncFromApp
	TimePoint string `json:"dietKind" note:"筛选的餐次"`
}

type DietRecordForTeamUserFunc struct {
	SportDietRecord
	PatientName string                     `json:"patientName"`
	DietComment *ExpDoctorCommentToPatient `json:"dietComment"`
}

// DietRecordForTeamUserFunc Implements Sort Interface

type DietRecordsForTeamUserFunc []*DietRecordForTeamUserFunc

func (s DietRecordsForTeamUserFunc) Len() int {
	return len(s)
}

func (s DietRecordsForTeamUserFunc) Less(i, j int) bool {
	return (time.Time(*s[i].HappenDateTime)).After(time.Time(*s[j].HappenDateTime))
}

func (s DietRecordsForTeamUserFunc) Swap(i, j int) {
	s[i], s[j] = s[j], s[i]
}

func (s *DietRecordForTeamUserFunc) CopyFrom(source *SportDietRecord) {
	if source == nil {
		return
	}
	s.SerialNo = source.SerialNo
	s.PatientID = source.PatientID
	s.RecordType = source.RecordType
	s.TimePoint = source.TimePoint
	s.ItemName = source.ItemName
	s.ItemValue = source.ItemValue
	s.ItemUnit = source.ItemUnit
	s.Description = source.Description
	s.Photo = source.Photo
	s.Memo = source.Memo
	s.SportIntensity = source.SportIntensity
	s.HappenPlace = source.HappenPlace
	s.DietRecordNo = source.DietRecordNo
	s.HappenDateTime = source.HappenDateTime
	s.InputDateTime = source.InputDateTime
}

type TlcTeamUserFuncDietsData struct {
	SelfDietsData  []*DietRecordForTeamUserFunc `json:"selfDietsData" note:"选定餐次后用户自己的数据"`
	TeamDietsData  []*DietRecordForTeamUserFunc `json:"teamDietsData" note:"选定餐次后小队的数据"`
	GroupDietsData []*DietRecordForTeamUserFunc `json:"groupDietsData" note:"选定餐次后大队的数据"`
}

type TlcTeamUserFuncWeightsData struct {
	TimeSeries       []*types.Time `json:"timeSeries" note:"时间序列"`
	SelfWeightData   []float64     `json:"selfWeightData"`
	TeamWeightsData  []float64     `json:"teamWeightsData"`
	GroupWeightsData []float64     `json:"groupWeightsData"`
}

type TlcTeamUserFuncSportStepsData struct {
	TimeSeries     []*types.Time `json:"timeSeries" note:"时间序列"`
	SelfStepsData  []uint64      `json:"selfStepsData" note:"依照时间序列的自己的步数列表"`
	TeamStepsData  []float64     `json:"teamStepsData" note:"依照时间序列的小队的平均步数列表"`
	GroupStepsData []float64     `json:"groupStepsData" note:"依照时间序列的大队的平均步数列表"`
}

type TlcTeamUserFuncTaskInfo struct {
	SelfTaskDataList []*TlcTeamTaskForSelf `json:"selfTaskDataList" note:"用户的个人任务"`
	TeamTaskDataList []*TlcTeamTaskData    `json:"teamTaskDataList" note:"队伍的任务信息"`
}

type TlcTeamUserFuncMemsInfo struct {
	ReqUserID    uint64                 `json:"reqUserID" note:"发起请求的用户ID"`
	MemsInfoList []*PatientUserBaseInfo `json:"memsInfoList" note:"组队队员的信息"`
}
