package doctor

import "github.com/ktpswjz/httpserver/types"

type InstrumentPatientInfo struct {
	IdentityCardNumber string      `json:"identityCardNumber" required:"true" note:"身份证号 例如:330106200012129876"`
	Name               string      `json:"patientName" required:"true" note:"姓名  例如:张三"`
	Sex                string      `json:"sex" note:"性别 例如:男"`
	DateOfBirth        *types.Time `json:"dateOfBirth"  note:"出生日期 例如:2000-12-12"`
}
