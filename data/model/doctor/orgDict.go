package doctor

type OrgDict struct {
	SerialNo              uint64  `json:"serialNo" note:"序号 主键，自增 例如:324"`
	OrgCode               string  `json:"orgCode" note:"机构代码 组织机构代码 例如:2894782947239239"`
	OrgName               string  `json:"orgName" note:"机构名称  例如:第一人民医院"`
	OrgLogo               string  `json:"orgLogo" note:"机构LOGO 例如:"`
	OrgType               string  `json:"orgType" note:"机构类别 对应机构类别字典的SmallTypeCode 例如:23232"`
	HospitalClass         *uint64 `json:"hospitalClass" note:"医院等级 0-未知级别; 1-一级丙等; 2-一级乙等; 3-一级甲等; 4-二级丙等; 5-二级乙等; 6-二级甲等; 7-三级丙等; 8-三级乙等; 9-三级甲等;  例如:9"`
	HospitalClass2        *uint64 `json:"hospitalClass2" note:"医院专业分类 0-综合医院，1-专科医院，3-诊所 例如:0"`
	HospitalClass3        *uint64 `json:"hospitalClass3" note:"医院所有制分类 0-公办医院，1-民营医院，2-私人医院 例如:0"`
	HospitalClass4        *uint64 `json:"hospitalClass4" note:"医院所属地区级别 0-省级，1-市级，2-区县，3-乡，4-村室 例如:0"`
	ParentOrgCode         string  `json:"parentOrgCode" note:"上级机构代码 如无上级机构，该字段为空；如有上级机构，字段中填写上级机构的OrgCode。 例如:232342"`
	DivisionCode          string  `json:"divisionCode" note:"行政区划 例如:2332232"`
	Address               string  `json:"address" note:"地址 例如:杭州市余杭区文一西路998号"`
	ContactName           string  `json:"contactName" note:"联系人姓名 例如:张三"`
	ContactPhone          string  `json:"contactPhone" note:"联系电话 例如:057187654321"`
	ResponsiblePersonName string  `json:"responsiblePersonName" note:"负责人姓名 例如:李四"`
	Memo                  string  `json:"memo" note:"备注 例如:"`
	ItemSortValue         *uint64 `json:"itemSortValue" note:"排序 用于字典项目排序，从1开始往后排 例如:1"`
	InputCode             string  `json:"inputCode" note:"输入码 用于快速输入，可以是拼音首字母或缩写 例如:gxy"`
	IsValid               *uint64 `json:"isValid" note:"有效性 用于描述本记录是否有效，0-无效，1-有效，默认值为1 例如:1"`
}
