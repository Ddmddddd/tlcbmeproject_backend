package doctor

type PatientCollectedKnowledgePatientId struct {
	PatientID uint64 `json:"patientID"`
}

type PatientCollectedKnowledgeKnowledgeId struct {
	Collected_knowledge_id uint64 `json:"knowledge_id"`
}

type PatientCollectedKnowledgeAdd struct {
	PatientID              uint64 `json:"patientID"`
	Collected_knowledge_id uint64 `json:"knowledge_id"`
}

type PatientCollectedKnowledgeDelete struct {
	PatientID              uint64 `json:"patientID"`
	Collected_knowledge_id uint64 `json:"knowledge_id"`
}

type PatientCollectedKnowledgeIdList struct {
	CollectedKnowledgeIdList []uint64 `json:"collectedKnowledgeIdList"`
}
