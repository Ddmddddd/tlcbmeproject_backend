package doctor

import "github.com/ktpswjz/httpserver/types"

type ApiLog struct {
	SerialNo       uint64      `json:"serialNo" note:"请求序号  例如:1808041823390094"`
	Method         string      `json:"method" note:"调用方法 调用接口使用的方法，如GET、 POST等 例如:POST"`
	Schema         string      `json:"schema" note:"协议 调用接口使用的协议，http或https 例如:http"`
	Uri            string      `json:"uri" note:"地址 调用接口地址 例如:/auth/info"`
	StartTime      *types.Time `json:"startTime" note:"开始时间 接口收到请求时间 例如:2018-08-04 18:23:53"`
	EndTime        *types.Time `json:"endTime" note:"结束时间 接口收到请求后返回时间 例如:2018-08-04 18:23:55"`
	ElapseTime     uint64      `json:"elapseTime" note:"耗时 接口耗时，单位纳秒 例如:1808"`
	ElapseTimeText string      `json:"elapseTimeText" note:"耗时文本 接口耗时显示文本 例如:199.305µs"`
	RIP            string      `json:"rIP" note:"调用者IP地址 接口调用者的远程IP地址 例如:192.168.1.101"`
	Result         uint64      `json:"result" note:"结果 接口调用结果，0表示成功，其它表示失败 例如:0"`
	Input          []byte      `json:"input" note:"输入参数"`
	Output         []byte      `json:"output" note:"输出参数"`
	Param          []byte      `json:"param" note:"地址栏参数"`
	ResponseID     string      `json:"responseID" note:"响应标识ID 例如:hbp-1222-8-9c5e06b9-7a6f-4715-a341-f5428a2d0ffb"`
}
