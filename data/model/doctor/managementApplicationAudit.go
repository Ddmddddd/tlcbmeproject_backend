package doctor

import "github.com/ktpswjz/httpserver/types"

type ManagementApplicationAudit struct {
	SerialNo      uint64      `json:"serialNo" note:"序号 主键，自增 例如:324"`
	PatientID     uint64      `json:"patientID" note:"用户ID 关联患者用户登录表用户ID 例如:232442"`
	Diagnosis     string      `json:"diagnosis" note:"诊断 例如:高血压，糖尿病"`
	DiagnosisMemo string      `json:"diagnosisMemo" note:"诊断备注  例如:血压 150/90 mmHg，血糖 7.9 mmol/L"`
	VisitOrgCode  string      `json:"visitOrgCode" note:"就诊机构代码 例如:123"`
	OrgVisitID    string      `json:"orgVisitID" note:"机构内就诊号 例如:123456"`
	Status        uint64      `json:"status" note:"审核状态 0-未审核，1-审核通过，2-审核不通过 例如:0"`
	AuditID       uint64      `json:"auditID" note:"审核人ID 审核人对应的用户ID 例如:32423"`
	AuditName     string      `json:"auditName" note:"审核人姓名 例如:张三"`
	AuditDateTime *types.Time `json:"auditDateTime" note:"审核时间 例如:2018-07-03 13:00:00"`
	RefuseReason  string      `json:"refuseReason" note:"审核不通过原因 最近一次审核不通过的原因。只有当状态为2时有效。 例如:信息不够完善"`
}
