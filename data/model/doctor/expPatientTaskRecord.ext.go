package doctor

import "github.com/ktpswjz/httpserver/types"

type ExpPatientTaskRecordInput struct {
	PatientID      uint64      `json:"patientID" note:"患者id"`
	TaskID         uint64      `json:"taskID" note:"对应taskRepository中的serialNo，为0则代表该任务不是按期完成的"`
	ParentTaskName *string     `json:"parentTaskName" note:"父任务名"`
	TaskName       string      `json:"taskName" note:"任务名"`
	CreateDateTime *types.Time `json:"createDateTime" note:"创建时间"`
	Credit         uint64      `json:"credit" note:"获取积分"`
	Record         interface{} `json:"record" note:"任务完成具体记录"`
}

type ExpPatientTaskRecordGetByTaskIDsAndTime struct {
	PatientID      uint64       `json:"patientID" note:"患者id"`
	TaskIDs        []uint64     `json:"taskIDs" note:"对应taskRepository中的serialNo，为0则代表该任务不是按期完成的"`
	CreateDateTime *types.Time  `json:"createDateTime" note:"创建时间"`
	Record         *interface{} `json:"record" note:"任务完成具体记录"`
}

type ExpPatientTaskCreditHistory struct {
	TaskName       string      `json:"taskName" note:"任务名"`
	CreateDateTime *types.Time `json:"createDateTime" note:"创建时间"`
	Credit         uint64      `json:"credit" note:"获取积分"`
}

type ExpPatientTaskRecordGetListInput struct {
	PatientID uint64 `json:"patientID" note:"患者id"`
}

type ExpPatientTaskRecords struct {
	ParentTaskName string   `json:"parentTaskName" note:"行动名称"`
	RecordTask     []string `json:"recordTask" note:"执行任务"`
	RecordTaskSnos []uint64 `json:"recordTaskSnos" note:"执行任务序号"`
}

type ExpPatientTaskRecordGetInput struct {
	SerialNo uint64 `json:"serialNo" note:""`
}

type ExpPatientTaskRecordGetByName struct {
	TaskName        string      `json:"taskName" note:"任务名"`
	HealthManagerID uint64      `json:"healthManagerID"`
	DoctorID        uint64      `json:"doctorID"`
	Name            string      `json:"patientName" note:"患者姓名"`
	ParentTaskName  *string     `json:"parentTaskName" note:"父任务名"`
	StartDateTime   *types.Time `json:"startDateTime" note:"创建时间"`
	EndDateTime     *types.Time `json:"endDateTime" note:"创建时间"`
}

type ExpPatientTaskRecordGetByNameAll struct {
	TaskName       string      `json:"taskName" note:"任务名"`
	PatientID      uint64      `json:"patientID" note:"患者id"`
	ParentTaskName *string     `json:"parentTaskName" note:"父任务名"`
	StartDateTime  *types.Time `json:"startDateTime" note:"创建时间"`
	EndDateTime    *types.Time `json:"endDateTime" note:"创建时间"`
}

type ExpPatientTaskRecordForApp struct {
	SerialNo        uint64      `json:"serialNo" note:""`
	PatientID       uint64      `json:"patientID" note:"患者id"`
	PatientName     string      `json:"patientName" note:"患者姓名"`
	Age             int64       `json:"age" note:"患者年龄"`
	SexText         string      `json:"sexText" note:"患者性别"`
	ComplianceRate  *uint64     `json:"complianceRate" note:"依从度"`
	Tag             string      `json:"tag" note:"患者特征"`
	ParentTaskName  *string     `json:"parentTaskName" note:"父任务名"`
	TaskName        string      `json:"taskName" note:"任务名"`
	TaskID          uint64      `json:"taskID" note:"对应taskRepository中的serialNo，为0则代表该任务不是按期完成的"`
	CreateDateTime  *types.Time `json:"createDateTime" note:"创建时间"`
	Record          interface{} `json:"record" note:"任务完成具体记录"`
	Comment         string      `json:"comment" note:"该餐的营养师评价"`
	ReadFlag        uint64      `json:"readFlag" note:"读取状态 例如 已读，未读"`
	CommentSerialNo *uint64     `json:"commentSerialNo"`
}

type ExpPatientTaskRecordGetCheckDataInput struct {
	PatientID      uint64   `json:"patientID" note:"患者id"`
	ParentTaskName []string `json:"parentTaskName" note:"父任务名"`
}

type ExpPatientTaskRecordCheckData struct {
	PatientID      uint64 `json:"patientID" note:"患者id"`
	ParentTaskName string `json:"parentTaskName" note:"父任务名"`
	Count          uint64 `json:"count"`
}

type ExpPatientTaskRecordClock struct {
	CreateDateTime *types.Time `json:"createDateTime" note:"创建时间"`
}

type ExpPatientTaskRecordCheckKnwRecord struct {
	PatientID uint64 `json:"patientID" note:"患者id"`
}

type ExpPatientTaskRecordDelete struct {
	SerialNo uint64 `json:"serialNo" note:"删除的序列号"`
}

type ExpPatientTaskRecordJson struct {
	SportDietRecord ExpPatientTaskRecordDelete `json:"sportDietRecord" note:"运动饮食信息"`
}
