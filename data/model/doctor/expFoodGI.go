package doctor

type ExpFoodGI struct {
	SerialNo      uint64 `json:"serialNo" note:"序号"`
	FoodName      string `json:"foodName" note:"食物名称"`
	GlycemicIndex uint64 `json:"glycemicIndex" note:"GI值"`
	FoodType      string `json:"foodType" note:"食物类型"`
}
