package doctor

import "tlcbme_project/data/model"

type ViewManagedIndexEx struct {
	ViewManagedIndex

	SexText     string `json:"sexText" note:"性别 0-未知, 1-男, 2-女，9-未说明 例如:男"`
	Age         int64  `json:"age" note:"年龄，单位岁，如60"`
	Tag         string `json:"tag" note:"人员标签"`
	ManagedDays int64  `json:"managedDays" note:"管理天数"`
}

type ViewManagedIndexFilter struct {
	SerialNo            *uint64      `json:"serialNo" note:"序号 主键，自增 例如:324"`
	PatientID           *uint64      `json:"patientID" note:"用户ID 关联患者用户登录表用户ID 例如:232442"`
	PatientName         string       `json:"patientName" note:"姓名  例如:张三"`
	ManageClassCode     *uint64      `json:"manageClassCode" note:"管理分类代码，例如：1"`
	ManageStatus        *uint64      `json:"manageStatus" note:"管理状态 0-管理中，1-迁出，2-已终止管理 例如:0"`
	ManageLevel         string       `json:"manageLevel" note:"管理等级"`
	DmManageLevel       string       `json:"DmManageLevel" note:"糖尿病管理等级"`
	PatientActiveDegree []uint64     `json:"patientActiveDegree" note:"活跃度"`
	Order               *model.Order `json:"order" note:"排序"`
}

type ViewManagedIndexFilterEx struct {
	ViewManagedIndexFilter

	OrgCode            string  `json:"orgCode" note:"管理机构代码 管理该患者的机构 例如:897798"`
	DoctorID           *uint64 `json:"doctorID" note:"医生ID 例如:232"`
	HealthManagerID    *uint64 `json:"healthManagerID" note:"健康管理师ID 例如:232"`
	ViewOrgAllPatients bool    `json:"viewOrgAllPatients" note:"是否查看机构所有患者 例如:true"`
}

type ViewManagedIndexCountManageLevel struct {
	Total      uint64 `json:"total" note:"总数"`
	Terminated uint64 `json:"terminated" note:"已终止数量"`
	Level0     uint64 `json:"level0" note:"一级管理数量"`
	Level1     uint64 `json:"level1" note:"二级管理数量"`
	Level2     uint64 `json:"level2" note:"三级管理数量"`
}

type ViewDmManagedIndexCountManageLevel struct {
	Total      uint64 `json:"total" note:"总数"`
	Terminated uint64 `json:"terminated" note:"已终止数量"`
	Level0     uint64 `json:"level0" note:"新患者数量"`
	Level10    uint64 `json:"level10" note:"常规达标"`
	Level20    uint64 `json:"level20" note:"常规不达标"`
	Level30    uint64 `json:"level30" note:"强化管理"`
}

type ViewManagedIndexCountDmType struct {
	Total      uint64 `json:"total" note:"总数"`
	Terminated uint64 `json:"terminated" note:"已终止数量"`
	Type1      uint64 `json:"type1" note:"1型糖尿病数量"`
	Type2      uint64 `json:"type2" note:"2型糖尿病数量"`
	Gestation  uint64 `json:"gestation" note:"妊娠糖尿病数量"`
	Other      uint64 `json:"other" note:"其他类型糖尿病数量"`
}
