package doctor

import "github.com/ktpswjz/httpserver/types"

type ClientLog struct {
	SerialNo       uint64      `json:"serialNo" note:"序号  例如:1808041823390094"`
	Method         string      `json:"method" note:"调用方法 调用接口使用的方法，如GET、 POST等 例如:POST"`
	Schema         string      `json:"schema" note:"协议 调用接口使用的协议，http或https 例如:http"`
	Host           string      `json:"host" note:"主机或IP 接口地址主机或IP 例如:example.com"`
	Port           string      `json:"port" note:"端口号 接口地址中的端口号 例如:80"`
	Uri            string      `json:"uri" note:"地址 接口地址路径 例如:/auth/info"`
	StartTime      *types.Time `json:"startTime" note:"开始时间 接口收到请求时间 例如:2018-08-04 18:23:53"`
	EndTime        *types.Time `json:"endTime" note:"结束时间 接口收到请求后返回时间 例如:2018-08-04 18:23:55"`
	ElapseTime     uint64      `json:"elapseTime" note:"耗时 接口耗时，单位纳秒 例如:1808"`
	ElapseTimeText string      `json:"elapseTimeText" note:"耗时文本 接口耗时显示文本 例如:199.305µs"`
	Result         uint64      `json:"result" note:"结果(http状态码) 接口调用结果，0表示未知失败，其它为http状态码 例如:0"`
	ErrorMessage   string      `json:"errorMessage" note:"错误信息 接口调用错误时的信息 例如:异常"`
	Input          []byte      `json:"input" note:"输入参数"`
	Output         []byte      `json:"output" note:"输出参数"`
	Param          []byte      `json:"param" note:"地址栏参数"`
	ResponseID     string      `json:"responseID" note:"响应标识ID 例如:hbp-1222-8-9c5e06b9-7a6f-4715-a341-f5428a2d0fea"`
}
