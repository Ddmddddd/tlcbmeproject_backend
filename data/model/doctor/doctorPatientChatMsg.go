package doctor

import "github.com/ktpswjz/httpserver/types"

type DoctorPatientChatMsg struct {
	SerialNo    uint64      `json:"serialNo" note:"序号 主键，自增 例如:324"`
	SenderID    uint64      `json:"senderID" note:"发送者ID 关联患者或医生用户登录表用户ID 例如:232442"`
	ReceiverID  uint64      `json:"receiverID" note:"接受者ID 关联患者或医生用户登录表用户ID 例如:232442"`
	MsgContent  string      `json:"msgContent" note:"消息内容 医患沟通的消息内容 例如:你好"`
	MsgDateTime *types.Time `json:"msgDateTime" note:"消息时间 消息发生的时间 例如:2018-07-03 14:45:00"`
	MsgFlag     uint64      `json:"msgFlag" note:"消息标识 0-未读，1-已读 例如:0"`
}
