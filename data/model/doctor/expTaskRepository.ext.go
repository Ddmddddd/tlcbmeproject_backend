package doctor

import "github.com/ktpswjz/httpserver/types"

type ExpTaskRepositoryTree struct {
	SerialNo       uint64      `json:"serialNo" note:""`
	TaskName       string      `json:"taskName" note:"任务名称，包含周任务和日任务"`
	TaskOrder      uint64      `json:"taskOrder" note:"该值为任务的顺序，例如：1，代表该任务为第一天的任务"`
	TaskTime       *types.Time     `json:"taskTime" note:"任务触发时间，为空则没有特定时间"`
	RemindTask     string      `json:"remindTask" note:"提醒任务，可选为“微信提醒”、“短信提醒”，为空则没有提醒任务"`
	Target         string      `json:"target" note:"任务目标"`

	Children		TreeCollection `json:"children"`
}

type TreeCollection []*ExpTaskRepositoryTree

// 实现sort.Interface接口的获取元素数量方法
func (t TreeCollection) Len() int {
	return len(t)
}
// 实现sort.Interface接口的比较元素方法
func (t TreeCollection) Less(i, j int) bool {
	return t[i].TaskOrder < t[j].TaskOrder
}
// 实现sort.Interface接口的交换元素方法
func (t TreeCollection) Swap(i, j int) {
	t[i], t[j] = t[j], t[i]
}

type ExpTaskRepositoryAddInput struct {
	TaskName       string      `json:"taskName" note:"任务名称，包含周任务和日任务"`
	ParentTaskID   uint64      `json:"parentTaskID" note:"为0代表为周任务，否则代表日任务"`
	TaskOrder      uint64      `json:"taskOrder" note:"该值为任务的顺序，例如：1，代表该任务为第一天的任务"`
	TaskTime      *types.Time    `json:"taskTime" note:"任务触发时间，为空则没有特定时间"`
	RemindTask     string      `json:"remindTask" note:"提醒任务，可选为“微信提醒”、“短信提醒”，为空则没有提醒任务"`
	Target         string      `json:"target" note:"任务目标"`
	EditorID       uint64      `json:"editorID" note:"编辑者id"`
	CreateDateTime *types.Time `json:"createDateTime" note:"创建时间"`
}

type ExpTaskRepositoryEditInput struct {
	SerialNo       uint64      `json:"serialNo" note:""`
	TaskName       string      `json:"taskName" note:"任务名称，包含周任务和日任务"`
	TaskOrder      uint64      `json:"taskOrder" note:"该值为任务的顺序，例如：1，代表该任务为第一天的任务"`
	TaskTime       *types.Time    `json:"taskTime" note:"任务触发时间，为空则没有特定时间"`
	RemindTask     string      `json:"remindTask" note:"提醒任务，可选为“微信提醒”、“短信提醒”，为空则没有提醒任务"`
	Target         string      `json:"target" note:"任务目标"`
	EditorID       uint64      `json:"editorID" note:"编辑者id"`
	UpdateTime     *types.Time `json:"updateTime" note:"更新时间"`
}

type ExpTaskRepositoryDeleteInput struct {
	SerialNo       uint64      `json:"serialNo" note:""`
	EditorID       uint64      `json:"editorID" note:"编辑者id"`
	Status         uint64      `json:"status" note:"0-正常，1-弃用，此处为删除，应为1"`
	UpdateTime     *types.Time `json:"updateTime" note:"更新时间"`
}

type ExpTaskRepositoryOutput struct {
	Snos 			 []uint64    `json:"snos" note:"任务的序号"`
	TaskName       	 []string    `json:"taskName" note:"任务名称，包含周任务和日任务"`
	Target           []string    `json:"target" note:"任务目标"`
	TaskOrder      	 uint64      `json:"taskOrder" note:"该值为任务的顺序，例如：1，代表该任务为第一天的任务"`
	TaskTime        *types.Time   `json:"taskTime" note:"任务触发时间，为空则没有特定时间"`
	RemindTask       string      `json:"remindTask" note:"提醒任务，可选为“微信提醒”、“短信提醒”，为空则没有提醒任务"`
	ParentTaskName   string      `json:"parentTaskName" note:"父任务名称"`
	ParentTaskTarget string      `json:"parentTaskTarget" note:"任务目标"`
	DayIndex	     uint64      `json:"dayIndex" note:"进行的日期"`
}
