package doctor

import "github.com/ktpswjz/httpserver/types"

type DiscomfortRecordData struct {
	Discomfort     string      `json:"discomfort" note:"不适情况 如果有多种不适，之间用逗号分隔。例如：剧烈头痛，恶心呕吐。 例如:剧烈头痛，恶心呕吐"`
	Memo           string      `json:"memo" note:"备注  例如:"`
	HappenDateTime *types.Time `json:"happenDateTime" note:"发生时间 不适情况发生的时间 例如:2018-07-03 14:45:00"`
}

type DiscomfortRecordForApp struct {
	SerialNo       uint64      `json:"serialNo" note:"序号 主键，自增 例如:324"`
	Discomfort     string      `json:"discomfort" note:"不适情况 如果有多种不适，之间用逗号分隔。例如：剧烈头痛，恶心呕吐。 例如:剧烈头痛，恶心呕吐"`
	Memo           string      `json:"memo" note:"备注  例如:"`
	HappenDateTime *types.Time `json:"happenDateTime" note:"发生时间 不适情况发生的时间 例如:2018-07-03 14:45:00"`
}

type DiscomfortRecordForAppEx struct {
	DiscomfortRecordForApp

	PatientID uint64 `json:"patientID" note:"用户ID 关联患者用户登录表用户ID 例如:232442"`
}

type DiscomfortRecordDataFilter struct {
	HappenStartDate *types.Time `json:"measureStartDate" note:"测量开始时间 测量体重的时间 例如:2018-07-03 14:45:00"`
	HappenEndDate   *types.Time `json:"measureEndDate" note:"测量结束时间 测量体重的时间 例如:2018-08-03 14:45:00"`
}

type DiscomfortRecordDataFilterEx struct {
	DiscomfortRecordDataFilter

	PatientID uint64 `json:"patientID" required:"true" note:"用户ID 关联患者用户登录表用户ID 例如:232442"`
}

type UserRecordCollection struct {
	BloodPressureRecords []*BloodPressureRecordForApp `json:"bpRecordList" note:"血压数据 例如:"`
	WeightRecords        []*WeightRecordForApp        `json:"weightRecordList" note:"血压数据 例如:"`
	DrugRecords          []*DrugRecordForApp          `json:"drugRecordList" note:"服药数据 例如:"`
	DietRecords          []*DietRecordForApp          `json:"dietRecordList" note:"饮食数据 例如:"`
	SportRecords         []*SportRecordForApp         `json:"sportsRecordList" note:"运动数据 例如:"`
	DiscomfortRecords    []*DiscomfortRecordForApp    `json:"discomfortRecordList" note:"不适记录 例如:"`
	BloodGlucoseRecords  []*BloodGlucoseRecordForApp  `json:"bloodGlucoseRecordList" note:"血糖记录 例如:"`
}

type LoginedDataForApp struct {
	ManagementPlan ManagementPlanForApp           `json:"managementPlan" note:"管理计划 例如:"`
	LoginUserInfo  PatientUserInfoForApp          `json:"loginUserInfo" note:"手机端登录用户信息 例如:"`
	TodayRecords   UserRecordCollection           `json:"todayRecords" note:"用户今日记录数据 例如:"`
	ChatDoctorList []*ChatDoctorWithMsgInfoForApp `json:"chatDoctorList" note:"可用于医患沟通的医生列表 例如:"`
	DietPlanChangeList []*ExpPatientDietPlan      `json:"dietPlanChangeList" note:"被医生修改的状态，用于提醒患者查看"`
}
