package doctor

type ManagementApplicationReviewFilter struct {
	SerialNo        *uint64  `json:"serialNo" note:"序号 主键，自增 例如:324"`
	PatientID       *uint64  `json:"patientID" note:"用户ID 关联患者用户登录表用户ID 例如:232442"`
	VisitOrgCode    string   `json:"visitOrgCode" note:"就诊机构代码 例如:123"`
	Statuses        []uint64 `json:"statuses" note:"审核状态 0-未审核，1-审核通过，2-审核不通过 例如:0"`
	HealthManagerID *uint64  `json:"healthManagerID" note:"健康管理师ID 患者注册时指定的健康管理师 例如:12432"`
}

type ManagementApplicationReviewMemo struct {
	PatientID       uint64      `json:"patientID" note:"用户ID 关联患者用户登录表用户ID 例如:232442"`
	DiagnosisMemo   string      `json:"diagnosisMemo" note:"诊断备注  例如:血压 150/90 mmHg，血糖 7.9 mmol/L"`
}
