package doctor

type ExpKnowledgeLink struct {
	SerialNo       uint64 `json:"serialNo" note:""`
	KnowledgeTitle string `json:"knowledgeTitle" note:"知识标题"`
	Url            string `json:"url" note:"知识链接"`
	KnowledgeType  string `json:"knowledgeType" note:"知识类型"`
	From           string `json:"from" note:"文章来源"`
	KnowledgeImage string `json:"knowledgeImage" note:"知识封面图片"`
}
