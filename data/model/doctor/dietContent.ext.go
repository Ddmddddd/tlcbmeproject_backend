package doctor

type FoodKindWeight struct {
	Oil              float64  `json:"oil" note:"油"`
	Salt             float64  `json:"salt" note:"盐"`
	Sugar            float64  `json:"sugar" note:"糖"`
	Dairy            float64  `json:"dairy" note:"乳制品"`
	Soybean          float64  `json:"soybean" note:"大豆"`
	Nut              float64  `json:"nut" note:"坚果"`
	LivestockPoultry float64  `json:"livestockPoultry" note:"畜禽类"`
	Fish             float64  `json:"fish" note:"鱼虾类"`
	Egg              float64  `json:"egg" note:"蛋类"`
	Vegetable        float64  `json:"vegetable" note:"蔬菜类"`
	Fruits           float64  `json:"fruits" note:"水果类"`
	Potatoes         float64  `json:"potatoes" note:"谷薯类和杂豆"`
	KindNum          []string `json:"kindNum" note:"食材种类"`
}

type DietContentFilter struct {
	DietRecordSerialNo uint64  `json:"dietRecordSerialNo" note:"用餐记录关联id"`
}