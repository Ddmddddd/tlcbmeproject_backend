package doctor

import "github.com/ktpswjz/httpserver/types"

type ExpScaleRecord struct {
	SerialNo  		uint64      `json:"serialNo" note:"序号"`
	PatientID 		uint64      `json:"patientID" note:"用户ID"`
	ScaleType 		uint64      `json:"scaleType" note:"量表属性，0-默认量表，1-普通量表，2-需要引擎生成生活计划的量表"`
	ScaleID   		uint64      `json:"scaleID" note:"量表ID"`
	Result    		interface{} `json:"result" note:"量表结果，JSON格式存储"`
	CreateDateTime	*types.Time `json:"createDateTime" note:"提交时间"`
}
