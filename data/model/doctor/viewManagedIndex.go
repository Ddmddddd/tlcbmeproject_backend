package doctor

import "github.com/ktpswjz/httpserver/types"

type ViewManagedIndex struct {
	SerialNo            uint64      `json:"serialNo" note:"序号 主键，自增 例如:324"`
	PatientID           uint64      `json:"patientID" note:"用户ID 关联患者用户登录表用户ID 例如:232442"`
	PatientName         string      `json:"patientName" note:"姓名  例如:张三"`
	Sex                 *uint64     `json:"sex" note:"性别 0-未知, 1-男, 2-女，9-未说明 例如:1"`
	DateOfBirth         *types.Time `json:"dateOfBirth" note:"出生日期 例如:2000-12-12"`
	PatientFeature      string      `json:"patientFeature" note:"患者特点 如有多个，之间用逗号分隔，例如：老年人，肥胖，残疾人 例如:老年人，肥胖"`
	ManageStatus        uint64      `json:"manageStatus" note:"管理状态 0-管理中，1-迁出，2-已终止管理 例如:0"`
	ManageClass         string      `json:"manageClass" note:"管理分类 如有多个，之间用逗号分隔，例如：高血压，糖尿病 例如:高血压，糖尿病"`
	ManageClassCode     *uint64     `json:"manageClassCode" note:"管理分类代码，例如：1"`
	OrgCode             string      `json:"orgCode" note:"管理机构代码 管理该患者的机构 例如:897798"`
	DoctorID            *uint64     `json:"doctorID" note:"医生ID 例如:232"`
	DoctorName          string      `json:"doctorName" note:"医生姓名 例如:张三"`
	HealthManagerID     *uint64     `json:"healthManagerID" note:"健康管理师ID 例如:232"`
	HealthManagerName   string      `json:"healthManagerName" note:"健康管理师姓名 例如:李四"`
	ManageStartDateTime *types.Time `json:"manageStartDateTime" note:"管理开始时间 最近一次管理的开始时间，如果是刚注册，则默认为审核通过时间 例如:2018-07-03 13:15:00"`
	ComplianceRate      *uint64     `json:"complianceRate" note:"依从度 值为0至5，0表示依从度未知 例如:4"`
	PatientActiveDegree uint64      `json:"patientActiveDegree" note:"患者活跃度 用户参与管理的活跃度，0-沉睡，1-活跃，默认值为1 例如:1"`
	ManageEndDateTime   *types.Time `json:"manageEndDateTime" note:"管理终止时间 该字段只有当ManageStatus字段为2时才有意义 例如:2018-08-03 13:15:00"`
	ManageEndReason     string      `json:"manageEndReason" note:"管理终止的原因 该字段只有当ManageStatus字段为2时才有意义 例如:亡故（2018-08-03，车祸死亡）"`
	ManageLevel         string      `json:"manageLevel" note:""`
	DmManageLevel       string      `json:"dmManageLevel" note:""`
	Ext                 interface{} `json:"ext" note:"扩展信息 保存不同管理分类的扩展信息 例如:{htn:{}, dm:{}}"`
}
