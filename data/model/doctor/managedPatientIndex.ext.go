package doctor

import (
	"encoding/json"
	"github.com/ktpswjz/httpserver/types"
)

type ManagedPatientIndexFilterBase struct {
	SerialNo  *uint64 `json:"serialNo" note:"序号 主键，自增 例如:324"`
	PatientID *uint64 `json:"patientID" note:"用户ID 关联患者用户登录表用户ID 例如:232442"`
}

type ManagedPatientIndexExt struct {
	Htn  ManagedPatientIndexExtHtn  `json:"htn" note:"高血压"`
	Dm   ManagedPatientIndexExtDm   `json:"dm" note:"糖尿病"`
	Copd ManagedPatientIndexExtCopd `json:"copd" note:"慢阻肺"`
}

func (s *ManagedPatientIndexExt) Init(v interface{}) error {
	data, err := json.Marshal(v)
	if err != nil {
		return err
	}

	return json.Unmarshal(data, s)
}

type ManagedPatientIndexExtData struct {
	ManageLevel              uint64     `json:"manageLevel" note:"管理等级 当前管理等级，0-新患者，1-一级管理，2-二级管理 例如:0"`
	ManageLevelStartDateTime types.Time `json:"manageLevelStartDateTime" note:"管理等级开始时间 当前管理等级开始时间 例如:2018-07-03 13:15:00"`
}

type ManagedPatientIndexExtHtn struct {
	ManagedPatientIndexExtData
}

type ManagedPatientIndexExtDm struct {
	ManagedPatientIndexExtData

	Type        ManagedPatientIndexExtDmType `json:"type" note:"糖尿病类型"`
	Medications []string                     `json:"medications" note:"当前用药"`

	Fpg   *float64 `json:"fpg"`
	Ppg   *float64 `json:"ppg"`
	HbA1c *float64 `json:"hbA1c"`
}

type ManagedPatientIndexExtDmType struct {
	Code uint64 `json:"code" note:"代码， 1-1型糖尿病，2-2型糖尿病，3-妊娠糖尿病"`
	Name string `json:"name" note:"名称 例如:1型糖尿病"`
}

type ManagedPatientIndexExtCopd struct {
	IPAG      *uint64 `json:"ipag" note:"IPAG量表"`
	CAT       *uint64 `json:"cat" note:"CAT量表"`
	Depressed *uint64 `json:"depressed" note:"抑郁量表"`
	Anxious   *uint64 `json:"anxious" note:"焦虑量表"`
	PEF       *uint64 `json:"pef" note:"呼气峰流速"`
}

type ManagedPatientIndexTerminate struct {
	PatientID       uint64 `json:"patientID" required:"true" note:"用户ID 关联患者用户登录表用户ID 例如:232442"`
	ManageEndReason string `json:"manageEndReason" note:"管理终止的原因 该字段只有当ManageStatus字段为2时才有意义 例如:亡故（2018-08-03，车祸死亡）"`
}

type ManagedPatientIndexStatus struct {
	ManageStatus      uint64      `json:"manageStatus" note:"管理状态 0-管理中，1-迁出，2-已终止管理 例如:0"`
	ManageEndDateTime *types.Time `json:"manageEndDateTime" note:"管理终止时间 该字段只有当ManageStatus字段为2时才有意义 例如:2018-08-03 13:15:00"`
	ManageEndReason   string      `json:"manageEndReason" note:"管理终止的原因 该字段只有当ManageStatus字段为2时才有意义 例如:亡故（2018-08-03，车祸死亡）"`
}
