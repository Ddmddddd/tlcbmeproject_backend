package doctor

import "github.com/ktpswjz/httpserver/types"

type ExpActionPlan struct {
	SerialNo       uint64      `json:"serialNo" note:"序号"`
	PatientID      uint64      `json:"patientID" note:"用户ID"`
	Title          string	   `json:"title" note:"行动计划标题"`
	ActionPlan     string      `json:"actionPlan" note:"行动计划"`
	Description	   string	   `json:"description" note:"计划描述"`
	Type  		   string 	   `json:"type" note:"计划类型"`
	Status         uint64      `json:"status" note:"状态，0-使用中，1-已取消"`
	CreateDateTime *types.Time `json:"createDateTime" note:"创建时间"`
	UpdateDateTime *types.Time `json:"updateDateTime" note:"更新时间"`
	EditorType     uint64      `json:"editorType" note:"编辑者，0-引擎规则，1-医生，2-患者"`
	Level          uint64      `json:"level" note:"优先级，分为0-5，数值越大优先级越高"`
	SeqActionPlan  uint64	   `json:"seqActionPlan" note:"对应引擎端生活计划的id，用于提交、修改等操作"`
	RecordTimes	   uint64	   `json:"recordTimes" note:"记录次数"`
	RequireTimes   uint64	   `json:"requireTimes" note:"需求次数"`
}
