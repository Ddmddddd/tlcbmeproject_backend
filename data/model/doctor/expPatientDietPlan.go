package doctor

import "github.com/ktpswjz/httpserver/types"

type ExpPatientDietPlan struct {
	SerialNo     uint64      `json:"serialNo" note:""`
	PatientID    uint64      `json:"patientID" note:"患者id"`
	DietPlan     string      `json:"dietPlan" note:"饮食计划"`
	PicUrl       string      `json:"picUrl" note:"图片"`
	StartDate    *types.Time `json:"startDate" note:"开始时间"`
	EndDate      *types.Time `json:"endDate" note:"结束时间"`
	Level        string      `json:"level" note:"重要程度"`
	Complete     uint64		 `json:"complete" note:"是否完成，0-未完成，1-已完成"`
	ChangeStatus uint64      `json:"changeStatus" note:"改变状态，0-正常，1-被修改，需要患者查看，给予提示"`
	CreateDateTime *types.Time `json:"createDateTime" note:"创建时间"`
	UpdateDateTime *types.Time `json:"updateDateTime" note:"更新时间"`
}
