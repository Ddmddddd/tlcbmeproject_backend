package doctor

import (
	"github.com/ktpswjz/httpserver/types"
	"strings"
	"time"
)

type DrugRecordData struct {
	DrugName    string      `json:"drugName" note:"药品名称  例如:氨氯地平片,依普纳利"`
	Dosage      string      `json:"dosage" note:"剂量 例如:5mg,10mg"`
	Memo        string      `json:"memo" note:"备注  例如:"`
	UseDateTime *types.Time `json:"useDateTime" note:"用药时间 用药的实际时间 例如:2018-07-03 14:45:00"`
}

type DrugRecordForApp struct {
	SerialNo    string      `json:"serialNo" note:"序号组 例如:324,248"`
	DrugName    string      `json:"drugName" note:"药品名称  例如:氨氯地平片,依普纳利"`
	Dosage      string      `json:"dosage" note:"剂量 例如:5mg,10mg"`
	Memo        string      `json:"memo" note:"备注  例如:"`
	UseDateTime *types.Time `json:"useDateTime" note:"用药时间 用药的实际时间 例如:2018-07-03 14:45:00"`
	Type        uint64      `json:"type" note:"记录时刻 记录服药的时刻 例如:1早上 2中午 3晚上 0自定义"`
}

type DrugRecordForAppEx struct {
	DrugRecordForApp

	PatientID uint64 `json:"patientID" required:"true" note:"用户ID 关联患者用户登录表用户ID 例如:232442"`
}

type DrugRecordDataFilter struct {
	UseStartDate *types.Time `json:"measureStartDate" note:"记录开始时间 记录服药的时间 例如:2018-07-03 14:45:00"`
	UseEndDate   *types.Time `json:"measureEndDate" note:"记录结束时间 记录服药的时间 例如:2018-08-03 14:45:00"`
}

type DrugRecordDataFilterEx struct {
	DrugRecordDataFilter

	PatientID uint64 `json:"patientID" required:"true" note:"用户ID 关联患者用户登录表用户ID 例如:232442"`
}

func (s *DrugRecordForAppEx) ConvertToDrugRecordList() []*DrugUseRecord {
	arrDrugName := strings.Split(s.DrugName, ",")
	arrDosage := strings.Split(s.Dosage, ",")
	count := len(arrDrugName)
	var timePoint string
	switch s.Type {
	case 1:
		timePoint = string("早")
	case 2:
		timePoint = string("中")
	case 3:
		timePoint = string("晚")
	default:
		timePoint = string("")
	}
	now := types.Time(time.Now())
	arrDrugRecord := make([]*DrugUseRecord, 0)
	for i := 0; i < count; i++ {
		drugRecord := &DrugUseRecord{
			SerialNo:      uint64(0),
			PatientID:     s.PatientID,
			TimePoint:     timePoint,
			DrugName:      arrDrugName[i],
			Dosage:        arrDosage[i],
			Memo:          s.Memo,
			UseDateTime:   s.UseDateTime,
			InputDateTime: &now,
		}
		arrDrugRecord = append(arrDrugRecord, drugRecord)
	}
	return arrDrugRecord
}

type DrugUseRecordFilter struct {
	SerialNo     *uint64     `json:"serialNo" note:"序号 主键，自增 例如:324"`
	PatientID    *uint64     `json:"patientID" note:"用户ID 关联患者用户登录表用户ID 例如:232442"`
	TimePoint    string      `json:"timePoint" note:"时间点 早、中、晚 例如:早"`
	DrugName     string      `json:"drugName" note:"药品名称  例如:氨氯地平片"`
	UseDateStart *types.Time `json:"useDateStart" note:"开始日期 例如:2018-07-03"`
	UseDateEnd   *types.Time `json:"useDateEnd" note:"结束日期 例如:2018-07-17"`
}

type DrugUseRecordMonthGroup struct {
	Year  int `json:"year" note:"年份"`
	Month int `json:"month" note:"月份"`

	Records []*DrugUseRecord `json:"records" note:"用药记录"`
}

type DrugUseRecordMonthGroupCollection []*DrugUseRecordMonthGroup

func (s DrugUseRecordMonthGroupCollection) GetGroup(year, month int) *DrugUseRecordMonthGroup {
	count := len(s)
	for i := 0; i < count; i++ {
		item := s[i]
		if item.Year != year {
			continue
		}
		if item.Month != month {
			continue
		}

		return item
	}

	return nil
}
