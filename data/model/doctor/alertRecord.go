package doctor

import "github.com/ktpswjz/httpserver/types"

type AlertRecord struct {
	SerialNo         uint64      `json:"serialNo" note:"序号 主键，自增 例如:324"`
	PatientID        uint64      `json:"patientID" note:"用户ID 关联患者用户登录表用户ID 例如:232442"`
	AlertCode        string      `json:"alertCode" note:"预警代码 每类预警的唯一代码 例如:001"`
	AlertType        string      `json:"alertType" note:"预警类型 例如:血压"`
	AlertName        string      `json:"alertName" note:"预警名称 例如:单次血压异常偏高"`
	AlertReason      string      `json:"alertReason" note:"预警原因  例如:180/100mmHg"`
	AlertMessage     string      `json:"alertMessage" note:"预警消息 例如:消息内容"`
	AlertDateTime    *types.Time `json:"alertDateTime" note:"预警发生时间  例如:2018-07-06 14:55:00"`
	ReceiveDateTime  *types.Time `json:"receiveDateTime" note:"接收时间 慢病管理工作平台接收到该预警的时间 例如:2018-07-06 15:00:00"`
	Status           uint64      `json:"status" note:"处理状态 0-未处理 例如:0"`
	ProcessMode      *uint64     `json:"processMode" note:"处理方式 0-随访，1-忽略 例如:0"`
	FollowUpSerialNo *uint64     `json:"followUpSerialNo" note:"随访序号 ProcessMode为0时，这里存对应的随访记录序号。 例如:435345"`
	IgnoreReason     string      `json:"ignoreReason" note:"忽略预警的原因 ProcessMode为1时有效。忽略预警的原因 例如:重复预警"`
	IgnoreDoctorID   *uint64     `json:"ignoreDoctorID" note:"随访人ID ProcessMode为1时有效。 例如:111"`
	IgnoreDoctorName string      `json:"ignoreDoctorName" note:"随访人姓名 ProcessMode为1时有效。 例如:张三"`
	IgnoreDateTime   *types.Time `json:"ignoreDateTime" note:"忽略预警的时间 ProcessMode为1时有效。 例如:2018-09-03 12:00:23"`
}
