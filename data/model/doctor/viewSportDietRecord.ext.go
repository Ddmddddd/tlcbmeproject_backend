package doctor

import "github.com/ktpswjz/httpserver/types"

type ViewSportDietRecordFilter struct {
	PatientID       uint64      `json:"patientID" requied:"true" note:"用户ID 关联患者用户登录表用户ID 例如:232442"`
	HappenDateStart *types.Time `json:"happenDateStart" note:"开始日期"`
	HappenDateEnd   *types.Time `json:"happenDateEnd" note:"结束日期"`
}
