package doctor

import "github.com/ktpswjz/httpserver/types"

type FollowupPlan struct {
	SerialNo       uint64      `json:"serialNo" note:"序号 主键，自增 例如:324"`
	PatientID      uint64      `json:"patientID" note:"用户ID 关联患者用户登录表用户ID 例如:232442"`
	FollowupDate   *types.Time `json:"followupDate" note:"计划随访日期 例如:2018-07-15"`
	FollowUpType   string      `json:"followUpType" note:"随访类型 随访的类型（原因） 例如:三个月例行随访"`
	Status         uint64      `json:"status" note:"随访状态 0-待随访，1-已随访，2-已忽略，3-已过期，9-已作废。 例如:0"`
	StatusMemo     string      `json:"statusMemo" note:"状态备注 例如当状态为忽略时，此处填写忽略的原因。 例如:"`
	CreateDateTime *types.Time `json:"createDateTime" note:"创建时间 随访计划的创建时间 例如:2018-07-15 09:33:22"`
	CreatorName    string      `json:"creatorName" note:"创建者姓名 随访计划创建者的姓名 例如:张三"`
	UpdateTime     *types.Time `json:"updateTime" note:"最后更新日期 例如:2018-08-15"`
}
