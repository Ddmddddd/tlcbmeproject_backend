package doctor

type Tag struct {
	Id  uint64 `json:"id" note:"主键、自增"`
	Tag string `json:"tag" note:"标签"`
}
