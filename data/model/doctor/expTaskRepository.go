package doctor

import "github.com/ktpswjz/httpserver/types"

type ExpTaskRepository struct {
	SerialNo       uint64      `json:"serialNo" note:""`
	TaskName       string      `json:"taskName" note:"任务名称，包含周任务和日任务"`
	ParentTaskID   uint64      `json:"parentTaskID" note:"为空代表为周任务，否则代表日任务"`
	TaskOrder      uint64      `json:"taskOrder" note:"为空代表周任务，否则代表日任务，该值为日任务的顺序，例如：1，代表该任务为第一天的任务"`
	TaskTime       *types.Time    `json:"taskTime" note:"任务触发时间，为空则没有特定时间"`
	RemindTask     string      `json:"remindTask" note:"提醒任务，可选为“微信提醒”、“短信提醒”，为空则没有提醒任务"`
	Target         string      `json:"target" note:"任务目标"`
	EditorID       uint64      `json:"editorID" note:"编辑者id"`
	CreateDateTime *types.Time `json:"createDateTime" note:"创建时间"`
	Status         uint64      `json:"status" note:"0-正常，1-弃用"`
	UpdateTime     *types.Time `json:"updateTime" note:"更新时间"`
}