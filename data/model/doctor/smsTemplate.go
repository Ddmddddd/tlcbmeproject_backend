package doctor

type SmsTemplate struct {
	SerialNo        uint64 `json:"serialNo" note:"序号 主键，自增 例如:324"`
	TemplateType    uint64 `json:"templateType" note:"模板类型 0-验证码，1-短信通知，2-推广短信 例如:1"`
	TemplateCode    string `json:"templateCode" note:"模板代码 阿里云分配的短信模板代码 例如:SMS_1330248924729"`
	TemplateName    string `json:"templateName" note:"模板名称  例如:复诊提醒短信"`
	TemplateContent string `json:"templateContent" note:"短信内容  例如:【浙大健康小微】${患者姓名}您好，您的复诊时间是${复诊时间}，请您于近日前往医院复查。祝您健康！"`
	IsValid         uint64 `json:"isValid" note:"有效性 用于描述本记录是否有效，0-无效，1-有效，默认值为1 例如:1"`
}
