package doctor

import "github.com/ktpswjz/httpserver/types"

type ExpFeatureToTask struct {
	SerialNo       uint64      `json:"serialNo" note:"序号"`
	Feature        string      `json:"feature" note:"患者特点"`
	TaskID         uint64      `json:"taskID" note:"该特征下，患者所需要的任务id"`
	EditorID       uint64      `json:"editorID" note:"编辑者id"`
	CreateDateTime *types.Time `json:"createDateTime" note:"创建时间"`
	Status         uint64      `json:"status" note:"状态，0-正常，1-删除"`
	UpdateTime     *types.Time `json:"updateTime" note:"更新时间"`
}
