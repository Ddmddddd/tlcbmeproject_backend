package doctor

import "github.com/ktpswjz/httpserver/types"

type UsabilityEvent struct {
	SerialNo   uint64      `json:"serialNo" note:"序号 主键，自增 例如:1"`
	UserName   string      `json:"userName" note:"用户名 关联患者用户登录表用户名 例如:aa"`
	AppId      string      `json:"appId" note:"应用标识 唯一标识应用 例如:abcdefghijklmn"`
	AppType    string      `json:"appType" note:"应用类别 Android、iOS或者微信小程序 例如:Android"`
	AppVersion string      `json:"appVersion" note:"应用版本 当前应用版本 例如:1.0.1.0"`
	EventName  string      `json:"eventName" note:"事件名称 当前事件的名称 例如:OnLoginEvent"`
	EventTime  *types.Time `json:"eventTime" note:"事件时间 触发当前事件的时间点 例如:2018-12-12 08:00:00"`
}
