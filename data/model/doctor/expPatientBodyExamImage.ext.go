package doctor

import "github.com/ktpswjz/httpserver/types"

type ExpPatientBodyExamImageCreate struct {
	PatientID      uint64      `json:"patientID" note:""`
	ImgUrlList         []string     `json:"imgUrlList" note:""`
	CreateDateTime *types.Time `json:"createDateTime" note:""`
}

type ExpPatientBodyExamImageGet struct {
	PatientID      uint64      `json:"patientID" note:""`
}