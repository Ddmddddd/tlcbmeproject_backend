package doctor

type ExpDietProgramRepository struct {
	SerialNo  uint64   `json:"serialNo" note:"序列号"`
	ProgramID *uint64  `json:"programID" note:"方案ID"`
	MealTime  string   `json:"mealTime" note:"餐次"`
	Recipe    string   `json:"recipe" note:"菜品"`
	Food      string   `json:"food" note:"组成食物"`
	Quantity  string   `json:"quantity" note:"组成食物份量"`
	Estimate  string   `json:"estimate" note:"组成食物估计份量"`
	Memo      string   `json:"memo" note:"组成食物备注"`
	Energy    *float64 `json:"energy" note:"方案标记目标能量"`
}
