package doctor

import (
	"github.com/ktpswjz/httpserver/types"
)

type Viewfollowuprecord struct {
	Serialno     uint64 `json:"serialno" note:"序号 主键，自增 例如:324"`
	Patientid    uint64 `json:"patientid" note:"用户ID 关联患者用户登录表用户ID 例如:232442"`
	Followuptype string `json:"followuptype" note:"随访类型  例如:三个月例行随访"`
	Orgcode      string `json:"orgcode" note:"管理机构代码 管理该患者的机构 例如:897798"`
	// 随访时间  例如:2018-08-01 10:00:00
	FollowupDateTime *types.Time `json:"followupDateTime"`
}
