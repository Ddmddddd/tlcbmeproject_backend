package doctor

import "github.com/ktpswjz/httpserver/types"

type ViewPatient struct {
	PatientID          uint64      `json:"patientID" note:"用户ID 关联医生用户登录表用户ID 例如:232442"`
	PatientName        string      `json:"patientName" note:"姓名  例如:张三"`
	Sex                *uint64     `json:"sex" note:"性别 0-未知, 1-男, 2-女，9-未说明 例如:1"`
	DateOfBirth        *types.Time `json:"dateOfBirth" note:"出生日期 例如:2000-12-12"`
	IdentityCardNumber string      `json:"identityCardNumber" note:"身份证号 例如:330106200012129876"`
	Phone              string      `json:"phone" note:"联系电话 例如:13812344321"`
	PatientFeature     string      `json:"patientFeature" note:"患者特点 如有多个，之间用逗号分隔，例如：老年人，肥胖，残疾人 例如:老年人，肥胖"`
	ManageClass        string      `json:"manageClass" note:"管理分类 如有多个，之间用逗号分隔，例如：高血压，糖尿病 例如:高血压，糖尿病"`
	OrgCode            string      `json:"orgCode" note:"管理机构代码 管理该患者的机构 例如:897798"`
	DoctorID           *uint64     `json:"doctorID" note:"医生ID 例如:232"`
	DoctorName         string      `json:"doctorName" note:"医生姓名 例如:张三"`
	HealthManagerID    *uint64     `json:"healthManagerID" note:"健康管理师ID 例如:232"`
	HealthManagerName  string      `json:"healthManagerName" note:"健康管理师姓名 例如:李四"`
}
