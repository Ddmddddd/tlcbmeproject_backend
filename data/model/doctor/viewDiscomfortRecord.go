package doctor

import "github.com/ktpswjz/httpserver/types"

type ViewDiscomfortRecord struct {
	SerialNo       uint64      `json:"serialNo" note:"序号 主键，自增 例如:324"`
	PatientID      uint64      `json:"patientID" note:"用户ID 关联患者用户登录表用户ID 例如:232442"`
	Discomfort     string      `json:"discomfort" note:""`
	Memo           string      `json:"memo" note:"备注  例如:"`
	HappenDateTime *types.Time `json:"happenDateTime" note:"发生时间 不适情况发生的时间 例如:2018-07-03 14:45:00"`
	InputDateTime  *types.Time `json:"inputDateTime" note:"录入时间 录入时间 例如:2018-07-03 14:00:00"`
}
