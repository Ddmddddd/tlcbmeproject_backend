package doctor

type ExpExercisePlanCreate struct {
	PatientID    uint64 `json:"patientID" note:"患者id"`
	ChoosePlan   []Plan `json:"choosePlan"`
}

type Plan struct {
	SerialNo uint64 `json:"serialNo"`
	Target   string `json:"target"`
	Mode     string `json:"mode"`
}