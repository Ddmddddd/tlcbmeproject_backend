package doctor

import (
	"github.com/ktpswjz/httpserver/types"
)

type ExpPatientTaskList struct {
	SerialNo       uint64      `json:"serialNo" note:"序号"`
	PatientID      uint64      `json:"patientID" note:"患者id"`
	TaskList       string      `json:"taskList" note:"任务组，存放数组"`
	CreateDateTime *types.Time `json:"createDateTime" note:"创建时间"`
	UpdateTime 	   *types.Time `json:"updateTime" note:"更新时间"`
	Status         uint64      `json:"status" note:"状态，0-正常，1-弃用，2-执行完毕"`
	DayIndex 	   uint64 	   `json:"dayIndex" note:"进行到的时间"`
}
