package doctor

import "github.com/ktpswjz/httpserver/types"

type ExpPatientFeatureToTaskCreate struct {
	TaskID         uint64      `json:"taskID" note:"任务序号"`
	TaskName 	   string      `json:"taskName" note:"任务名称"`
	Feature        string      `json:"feature" note:"对应特征"`
	EditorID       uint64      `json:"editorID" note:"编辑者id"`
	CreateDateTime *types.Time `json:"createDateTime" note:"创建时间"`
}

