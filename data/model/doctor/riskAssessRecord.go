package doctor

import "github.com/ktpswjz/httpserver/types"

type RiskAssessRecord struct {
	SerialNo        uint64      `json:"serialNo" note:"序号 主键，自增 例如:324"`
	PatientID       uint64      `json:"patientID" note:"用户ID 关联患者用户登录表用户ID 例如:232442"`
	Level           uint64      `json:"level" note:"危险级别 1-低危,2-中危, 3-高危 例如:3"`
	Name            string      `json:"name" note:"评估名称 例如:高血压危险分层评估"`
	Memo            string      `json:"memo" note:"备注"`
	ReceiveDateTime *types.Time `json:"receiveDateTime" note:"评估时间 慢病管理工作平台接收到该评估的时间 例如:2018-07-06 15:00:00"`
	NextSchedule    *types.Time `json:"nextSchedule" note:"下次评估时间 例如:2018-09-03 12:00:23"`
}
