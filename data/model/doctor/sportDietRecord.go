package doctor

import "github.com/ktpswjz/httpserver/types"

type SportDietRecord struct {
	SerialNo       uint64      `json:"serialNo" note:"序号 主键，自增 例如:324"`
	PatientID      uint64      `json:"patientID" note:"用户ID 关联患者用户登录表用户ID 例如:232442"`
	RecordType     string      `json:"recordType" note:"记录类型 用于标识是饮食还是运动，目前其值为以下两个之一：饮食、运动 例如:饮食"`
	TimePoint      string      `json:"timePoint" note:"时间点 早、中、晚 例如:早"`
	ItemName       string      `json:"itemName" note:"项目名称 例如：米饭 例如:米饭"`
	ItemValue      string      `json:"itemValue" note:"项目值 例如：200 例如:200"`
	ItemUnit       string      `json:"itemUnit" note:"项目单位 例如：g 例如:g"`
	Description    string      `json:"description" note:"描述 这个字段存储可读性文本，一般是ItemName、ItemValue和ItemUnit拼接而成，例如：米饭200g 例如:米饭200g"`
	Photo          string      `json:"photo" note:"食物照片 例如:"`
	Memo           string      `json:"memo" note:"备注  例如:"`
	SportIntensity string      `json:"sportIntensity" note:"运动强度  例如:强"`
	HappenDateTime *types.Time `json:"happenDateTime" note:"发生时间 实际发生的时间 例如:2018-07-03 14:45:00"`
	InputDateTime  *types.Time `json:"inputDateTime" note:"录入时间 录入时间 例如:2018-07-03 14:00:00"`
	HappenPlace    string      `json:"happenPlace" note:"发生地点"`
	DietRecordNo   uint64		`json:"dietRecordNo" note:"对应dietrecord表序号"`
}
