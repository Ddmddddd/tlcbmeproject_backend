package doctor

type ViewDoctorUserEx struct {
	ViewDoctorUser

	StatusText         string `json:"statusText" note:"用户状态文本 0-正常，1-冻结，9-已注销 例如:正常"`
	UserTypeText       string `json:"userTypeText" note:"用户类别文本 0-医生，1-健康管理师 例如:医生"`
	VerifiedStatusText string `json:"verifiedStatusText" note:"是否验证通过文本 0-未认证，1-已认证，2-认证不通过 例如:未认证"`
}

type ViewDoctorUserFilter struct {
	UserID uint64 `json:"userID" note:"用户ID 主键，自增 例如:"`
}

type ViewDoctorUserDict struct {
	UserID uint64 `json:"userID" note:"用户ID 主键，自增 例如:"`
	Name   string `json:"name" note:"姓名  例如:张三"`
}

type ViewDoctorUserDictFilter struct {
	UserType *uint64 `json:"userType" note:"用户类别 0-医生，1-健康管理师 例如:0"`
}
