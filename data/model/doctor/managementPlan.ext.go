package doctor

import (
	"github.com/ktpswjz/httpserver/types"
	"strconv"
)

type ManagementPlanSource struct {
	Rank   uint64           `json:"rank" note:"管理等级 例如:1"`
	Memo   string           `json:"memo" note:"分级说明 例如:"`
	Advise ManagementAdvise `json:"advise" note:"具体建议 例如:"`
}

type ManagementAdvise struct {
	Date           *types.Time      `json:"date" note:"生成时间 例如:2018-09-28 11:00:00"`
	Title          string           `json:"title" note:"标题 例如:一级管理计划"`
	Plans          []PlanFrequency  `json:"measureFrequencies" note:"管理计划内容及频次 例如:"`
	Goals          []ControlGoal    `json:"controlGoals" note:"管理计划目标 例如:"`
	DrugSuggestion TextSuggestion   `json:"drugSuggestion" note:"服药建议 例如:"`
	LifeSuggestion LifeIntervention `json:"lifeIntervention" note:"生活处方 例如:"`
	Revisit        bool             `json:"revisit" note:"是否建议复诊 例如:false"`
}

type PlanFrequency struct {
	Name       string   `json:"name" note:"名称 例如:血压/体重/血糖"`
	Interval   uint64   `json:"interval" note:"间隔 例如:1"`
	Unit       string   `json:"unit" note:"间隔单位 例如:天"`
	Times      uint64   `json:"times" note:"次数 例如:3"`
	TimePoints []string `json:"timePoints" note:"对应次数的code 例如:1(早) 2(中) 3(晚)"`
	Memo       []string `json:"memo" note:"说明"`
}

type ControlGoal struct {
	Name     string `json:"name" note:"名称 例如:收缩压/舒张压/体质指数/血糖"`
	Operator string `json:"operator" note:"关系 例如:> < ="`
	Value    string `json:"value" note:"值 例如:24.5"`
	Unit     string `json:"unit" note:"值单位 例如:kg/m^2"`
}

type LifeIntervention struct {
	Title        string           `json:"title" note:"标题 例如:生活处方"`
	Prescription []TextSuggestion `json:"prescription" note:"处方列表 例如:"`
}

type TextSuggestion struct {
	Title   string  `json:"title" note:"建议名称 例如:用药指导/饮食处方/运动处方"`
	Value   string  `json:"value" note:"建议内容 例如:低盐低脂饮食，食盐摄入量<6g，控制主食，晚餐 5-7 分饱，新鲜蔬菜 500g 水果 1 个（苹果、梨、桃子等）"`
	Details Details `json:"details" note:"详细信息"`
}

type ManagementPlanForApp struct {
	GoalSBP           string             `json:"goalSBP" note:"收缩压控制目标 例如:140"`
	GoalDBP           string             `json:"goalDBP" note:"舒张压控制目标 例如:90"`
	GoalBMI           string             `json:"goalBMI" note:"BMI（体重）控制目标，例如:24"`
	BPTasks           []BPTask           `json:"bpTasks" note:"测量血压计划表 例如:"`
	DrugTasks         []DrugTask         `json:"drugTasks" note:"服药计划表 例如:"`
	WeightTasks       []WeightTask       `json:"weightTasks" note:"测量体重计划表 例如:"`
	DrugSuggestion    string             `json:"drugSuggestion" note:"服药建议 例如:"`
	DietSuggestion    string             `json:"dietSuggestion" note:"饮食建议 例如:低糖少盐"`
	SportsSuggestion  string             `json:"sportsSuggestion" note:"运动建议 例如:每周运动一次"`
	CreateDate        *types.Time        `json:"createDate" note:"管理计划创建时间 例如:2018-08-01 00:00:00"`
	BloodGlucoseTasks []BloodGlucoseTask `json:"bloodGlucoseTasks" note:"测量血糖计划表 例如:"`
	GoalGLU           string             `json:"goalGLU" note:"血糖控制目标 例如:6.7"`
}

type BPTask struct {
	Type uint64 `json:"type" note:"测量血压的时刻 例如:1早 2中 3晚"`
}

type WeightTask struct {
	Type uint64 `json:"type" note:"测量体重的时刻 例如:1早 2中 3晚"`
}

type DrugTask struct {
	DrugName string `json:"drugName" note:"药品名称  例如:氨氯地平片,依普纳利"`
	Dosage   string `json:"dosage" note:"剂量 例如:5mg,10mg"`
	Type     uint64 `json:"type" note:"用药时刻 例如:1早 2中 3晚"`
}

type BloodGlucoseTask struct {
	Type uint64 `json:"type" note:"测量血糖的时刻 例如:1晨起空腹 2早餐后 3午餐前 4午餐后 5晚餐前 6晚餐后 7睡前 8凌晨"`
}

type Details struct {
	Code          string         `json:"code" note:"方案编码"`
	Name          string         `json:"name" note:"方案名称"`
	Prescriptions []Prescription `json:"prescriptions" note:"单药方案下的具体处方列表"`
}

type Prescription struct {
	Code string `json:"code"`
	Desc string `json:"desc"`
}

func (s *ManagementPlanForApp) InitializeDefault() {
	s.GoalSBP = "140"
	s.GoalDBP = "90"
	s.GoalBMI = "24"
	s.BPTasks = []BPTask{{1}}
	s.WeightTasks = []WeightTask{{1}}
	s.DrugTasks = make([]DrugTask, 0)
	s.DietSuggestion = "低糖少盐"
	s.SportsSuggestion = "每周运动一次"
	s.BloodGlucoseTasks = make([]BloodGlucoseTask, 0)
	s.GoalGLU = "6.7"
}

func (s *ManagementPlanForApp) InitializeFrom(source *ManagementPlanSource) {
	if source == nil {
		return
	}

	s.BPTasks = make([]BPTask, 0)
	s.WeightTasks = make([]WeightTask, 0)
	s.DrugTasks = make([]DrugTask, 0)
	s.BloodGlucoseTasks = make([]BloodGlucoseTask, 0)

	s.CreateDate = source.Advise.Date
	for _, plan := range source.Advise.Plans {
		switch plan.Name {
		case "血压":
			s.BPTasks = plan.ToBPTasks()
		case "体重":
			s.WeightTasks = plan.ToWeightTasks()
		case "血糖":
			s.BloodGlucoseTasks = plan.ToGLUTasks()
		}
	}

	for _, goal := range source.Advise.Goals {
		switch goal.Name {
		case "收缩压":
			s.GoalSBP = goal.Value
		case "舒张压":
			s.GoalDBP = goal.Value
		case "体质指数":
			s.GoalBMI = goal.Value
		case "血糖":
			s.GoalGLU = goal.Value
		}
	}

	s.DrugSuggestion = source.Advise.DrugSuggestion.Value
	for _, lifeSuggestion := range source.Advise.LifeSuggestion.Prescription {
		switch lifeSuggestion.Title {
		case "饮食处方":
			s.DietSuggestion = lifeSuggestion.Value
		case "运动处方":
			s.SportsSuggestion = lifeSuggestion.Value
		}
	}
}

func (s *ManagementPlanForApp) AddGLUPlan(source *ManagementPlanSource) {
	if source == nil {
		return
	}

	weightTaskTimes := int(0)
	s.BloodGlucoseTasks = make([]BloodGlucoseTask, 0)
	for _, plan := range source.Advise.Plans {
		switch plan.Name {
		case "血糖":
			s.BloodGlucoseTasks = plan.ToGLUTasks()
		case "体重":
			weightTaskTimes = len(plan.ToWeightTasks())
		}
	}
	if weightTaskTimes > 0 && len(s.WeightTasks) < 3 {
		weightTaskTimes += len(s.WeightTasks)
		switch weightTaskTimes {
		case 1:
			s.WeightTasks = []WeightTask{{1}}
		case 2:
			s.WeightTasks = []WeightTask{{1}, {3}}
		case 3:
			s.WeightTasks = []WeightTask{{1}, {2}, {3}}
		}
	}

	for _, goal := range source.Advise.Goals {
		switch goal.Name {
		case "血糖":
			s.GoalGLU = goal.Value
		}
	}
}

func (s *PlanFrequency) ToBPTasks() []BPTask {
	bpPlans := make([]BPTask, 0)
	if len(s.TimePoints) > 0 {
		for _, v := range s.TimePoints {
			t, err := strconv.ParseUint(v, 10, 64)
			if err != nil {
				continue
			}
			bpPlans = append(bpPlans, BPTask{Type: t})
		}
	}
	return bpPlans
}

func (s *PlanFrequency) ToWeightTasks() []WeightTask {
	weightPlans := make([]WeightTask, 0)
	if len(s.TimePoints) > 0 {
		for _, v := range s.TimePoints {
			t, err := strconv.ParseUint(v, 10, 64)
			if err != nil {
				continue
			}
			weightPlans = append(weightPlans, WeightTask{Type: t})
		}
	}
	return weightPlans
}

func (s *PlanFrequency) ToGLUTasks() []BloodGlucoseTask {
	gluPlans := make([]BloodGlucoseTask, 0)
	if len(s.TimePoints) > 0 {
		for _, v := range s.TimePoints {
			t, err := strconv.ParseUint(v, 10, 64)
			if err != nil {
				continue
			}
			gluPlans = append(gluPlans, BloodGlucoseTask{Type: t})
		}
	}
	return gluPlans
}

type ManagementPlanEx struct {
	ManagementPlan
}

type ManagementPlanFilter struct {
	SerialNo  *uint64  `json:"serialNo" note:"序号 主键，自增 例如:324"`
	PatientID *uint64  `json:"patientID" note:"用户ID 关联患者用户登录表用户ID 例如:232442"`
	PlanType  string   `json:"planType" note:"计划类型 该字段用于标识计划类型，用于表示该计划用于哪种慢病。计划类型为以下值之一：全局、高血压、糖尿病、慢阻肺。其中“全局”表示该计划不仅限于单种慢病。计划类型的值在后续会根据系统需求不断增加。 例如:高血压"`
	Statuses  []uint64 `json:"statuses" note:"计划状态 0-未开始，1-使用中，2-已作废 例如:0"`
}
type ManagementPlanAdjust struct {
	SerialNo           uint64      `json:"serialNo" note:"序号 主键，自增 例如:324"`
	PatientID          uint64      `json:"patientID" note:"用户ID 关联患者用户登录表用户ID 例如:232442"`
	Content            Content     `json:"content" note:"管理计划内容 JSON格式保存 例如:"`
	LastModifyDateTime *types.Time `json:"lastModifyDateTime" note:"最后修改时间 计划最后修改的时间 例如:2018-09-03 14:45:00"`
	ModifierID         *uint64     `json:"modifierID" note:"最后修改者ID 例如:555"`
	ModifierName       string      `json:"modifierName" note:"最后修改者姓名 例如:李四"`
	PlanType           string      `json:"planType" note:"计划类型 例如:高血压"`
	PlannerID          *uint64     `json:"plannerID" note:"计划制定人ID 例如:234"`
	PlannerName        string      `json:"plannerName" note:"计划制定人姓名 例如:张三"`
}
type InputManageInfo struct {
	PatientID string          `json:"patientId" note:"用户ID 关联患者用户登录表用户ID 例如:232442"`
	Plans     []PlanFrequency `json:"measureFrequency" note:"管理计划内容及频次 例如:"`
	Goals     []ControlGoal   `json:"controlGoals" note:"管理计划目标 例如:"`
}
type Content struct {
	Rank   uint64 `json:"rank" note:"管理等级 例如:1"`
	Memo   string `json:"memo" note:"分级说明 例如:"`
	Advise Advise `json:"advise" note:"具体建议 例如:"`
}
type Advise struct {
	Date           *types.Time      `json:"date" note:"生成时间 例如:2018-09-28 11:00:00"`
	Title          string           `json:"title" note:"标题 例如:一级管理计划"`
	Plans          []PlanFrequency  `json:"measureFrequencies" note:"管理计划内容及频次 例如:"`
	Goals          []ControlGoal    `json:"controlGoals" note:"管理计划目标 例如:"`
	DrugSuggestion TextSuggestion   `json:"drugSuggestion" note:"服药建议 例如:"`
	LifeSuggestion LifeIntervention `json:"lifeIntervention" note:"生活处方 例如:"`
	Revisit        bool             `json:"revisit" note:"是否建议复诊 例如:false"`
}
