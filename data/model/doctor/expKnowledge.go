package doctor

import "github.com/ktpswjz/httpserver/types"

type ExpKnowledge struct {
	SerialNo       uint64      `json:"serialNo" note:""`
	Type           string      `json:"type" note:"知识类型，例如：“粗粮”"`
	KnoTitle       string      `json:"knoTitle" note:"知识标题"`
	Content        string      `json:"content" note:"知识内容，富文本形式"`
	ImageUrl       *string     `json:"imageUrl" note:"知识配图"`
	CreateDateTime *types.Time `json:"createDateTime" note:"创建时间"`
	EditorID       uint64      `json:"editorID" note:"编辑者id"`
	Status 		   uint64      `json:"status" note:"状态"`
	UpdateTime     *types.Time `json:"updateTime" note:"更新时间"`
}
