package doctor

type SmsTemplateParam struct {
	SerialNo          uint64 `json:"serialNo" note:"序号 主键，自增 例如:324"`
	TemplateParamName string `json:"templateParamName" note:"参数名称 参数名称不能重名 例如:复诊时间"`
	ParamDataType     uint64 `json:"paramDataType" note:"参数数据类型 0-文本，1-整数，3-日期，4-日期时间 例如:0"`
	Editable          uint64 `json:"editable" note:"是否可编辑 0-不可编辑，使用后台给的值，例如姓名；1-允许编辑，例如复诊时间，用户可以编辑 例如:0"`
	InputCode         string `json:"inputCode" note:"输入码 用于快速输入，可以是拼音首字母或缩写 例如:gxy"`
	IsValid           uint64 `json:"isValid" note:"有效性 用于描述本记录是否有效，0-无效，1-有效，默认值为1 例如:1"`
}
