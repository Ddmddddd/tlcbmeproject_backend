package doctor

import "github.com/ktpswjz/httpserver/types"

type TlcTeamGroupTask struct {
	SerialNo      uint64      `json:"serialNo" note:"大队任务记录表序列号，主键"`
	BelongToGroup string      `json:"belongToGroup" note:"大队的名称"`
	TaskSno       uint64      `json:"taskSno" note:"对应的任务的序列号，通常不会在前端用到"`
	Valid         int         `json:"valid" note:"任务是否有效，0：无效；1：有效"`
	CreateTime    *types.Time `json:"createTime" note:"任务创建的时间"`
	UpdateTime    *types.Time `json:"updateTime" note:"任务更新的时间"`
}
