package doctor

type TlcHeEduKnwComment struct {
	Id           uint64 `json:"id" note:"评论的id"`
	Knowledge_id uint64 `json:"knowledge_id" note:"评论的知识id"`
	PatientID    uint64 `json:"patientID" note:"评论的用户的id"`
	Content      string `json:"content" note:"评论的内容"`
}
