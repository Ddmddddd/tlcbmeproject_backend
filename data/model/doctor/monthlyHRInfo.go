package doctor

import "github.com/ktpswjz/httpserver/types"

type MonthlyHRInfo struct {
	SerialNo    uint64      `json:"serialNo" note:"序号 主键，自增 例如:324"`
	PatientID   uint64      `json:"patientID" note:"用户ID 关联患者用户登录表用户ID 例如:232442"`
	YearMonth   string      `json:"yearMonth" note:"月份 年份+月份（2016-09） 例如:2016-09"`
	MaxHR       uint64      `json:"maxHR" note:"最大心率 当月出现的最大心率值 例如:108"`
	MaxHRTime   *types.Time `json:"maxHRTime" note:"最大心率时间 当月出现最大心率值的时间 例如:2016-09-10 12:00:00"`
	MinHR       uint64      `json:"minHR" note:"最小心率 当月出现的最小心率值 例如:56"`
	MinHRTime   *types.Time `json:"minHRTime" note:"最小心率时间 当月出现最小心率值的时间 例如:2016-09-11 12:00:00"`
	AvgHR       uint64      `json:"avgHR" note:"平均心率 当月的平均心率值 例如:68"`
	NormalTimes uint64      `json:"normalTimes" note:"心率正常次数 当月正常心率的次数 例如:6"`
	HigherTimes uint64      `json:"higherTimes" note:"心率偏高次数 当月偏高心率的次数 例如:4"`
	LowerTimes  uint64      `json:"lowerTimes" note:"心率偏低次数 当月偏低心率的次数 例如:2"`
}
