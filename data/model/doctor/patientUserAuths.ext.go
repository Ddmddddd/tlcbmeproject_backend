package doctor

type PatientUserAuthsChangePassword struct {
	OldPassword string `json:"oldPassword" note:"旧密码"`
	NewPassword string `json:"newPassword" note:"新密码"`
	Encryption  string `json:"encryption" note:"密码加密方法: 空-明文(默认); rsa-RSA密文(公钥通过调用获取RSA公钥接口获取)"`
}

type PatientUserAuthsChangePhone struct {
	PhoneNumber string `json:"phoneNumber" note:"手机号"`
}

type PatientUserAuthsUpdateTime struct {
	UserID      uint64 `json:"userID" note:"用户ID 主键，自增 例如:"`
	UseSeconds        uint64      `json:"useSeconds" note:"累计使用时长 单位：秒 例如:24342"`
}
