package doctor

import "github.com/ktpswjz/httpserver/types"

type ExpPatient struct {
	SerialNo       uint64      `json:"serialNo" note:"序号"`
	PatientID      uint64      `json:"patientID" note:"用户ID"`
	Type           uint64      `json:"type" note:"管理级别，1-实验组，2-对照组"`
	Status         uint64      `json:"status" note:"管理状态，0-退出管理，1-管理中"`
	CreateDateTime *types.Time `json:"createDateTime" note:"创建时间"`
	UpdateDateTime *types.Time `json:"updateDateTime" note:"更新时间"`
	EndReason	   string      `json:"endReason" note:"终止原因"`
}
