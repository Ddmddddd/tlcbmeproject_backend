package doctor

import "github.com/ktpswjz/httpserver/types"

type ExpPatientNutritionalQuestionFilter struct {
	PatientID      uint64      `json:"patientID" note:"用户ID 关联医生用户登录表用户ID 例如:232442"`
}

type ExpPatientNutritionalQuestionInput struct {
	UserID             uint64      `json:"userID" note:"用户ID 关联医生用户登录表用户ID 例如:232442"`
	Sex                *uint64     `json:"sex" note:"性别 0-未知, 1-男, 2-女，9-未说明 例如:1"`
	DateOfBirth        *types.Time `json:"dateOfBirth" note:"出生日期 例如:2000-12-12"`
	JobType            string      `json:"jobType" note:"职业类别 GB/T 6565-1999 职业分类 例如: "`
	Height             *uint64     `json:"height" note:"身高 单位：cm 例如:165"`
	Weight             *float64    `json:"weight" note:"体重 最后一次录入的体重，单位：kg 例如:60.5"`
	QuestionList       []uint64		`json:"questionList" note:"问题列表"`
	AnswerList		   []uint64		`json:"answerList" note:"答案列表"`
	InputDate		   *types.Time	`json:"time" note:"输入时间"`
}