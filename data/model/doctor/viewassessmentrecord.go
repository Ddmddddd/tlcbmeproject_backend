package doctor

import "github.com/ktpswjz/httpserver/types"

type Viewassessmentrecord struct {
	Serialno       uint64      `json:"serialno" note:"序号 主键，自增 例如:324"`
	Patientid      uint64      `json:"patientid" note:"用户ID 关联患者用户登录表用户ID 例如:232442"`
	Assessdatetime *types.Time `json:"assessdatetime" note:"评估时间  例如:2018-08-01 10:00:00"`
	Summary        string      `json:"Summary" note:"评估摘要 摘要规则根据所用评估表不同而不同，一般此处填入评估结果。 例如:中危"`
	Assessmentname string      `json:"assessmentname" note:"评估名称  例如:高血压危险分层评估"`
	Orgcode        string      `json:"orgcode" note:"管理机构代码 管理该患者的机构 例如:897798"`
}
