package doctor

import "github.com/ktpswjz/httpserver/types"

type ExpScaleToPatient struct {
	SerialNo       uint64      `json:"serialNo" note:"序号"`
	PatientID      uint64      `json:"patientID" note:"患者ID"`
	TaskID 		   *uint64 	   `json:"taskID" note:"任务id"`
	ScaleID        *uint64     `json:"scaleID" note:"量表id"`
	SmallScaleID   *uint64     `json:"smallScaleID" note:"量表id"`
	Status         uint64      `json:"status" note:"状态，0-未完成，1-已完成，2-放弃完成"`
	CreateDateTime *types.Time `json:"createDateTime" note:"创建时间"`
	UpdateTime     *types.Time `json:"updateTime" note:"更新时间"`
	RecordID       *uint64     `json:"recordID" note:"如果任务status=1，该字段标明问卷record中的序号"`
}
