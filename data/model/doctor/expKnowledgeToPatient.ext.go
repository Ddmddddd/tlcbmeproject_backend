package doctor

type ExpKnowledgeToPatientStatusEdit struct {
	KnowledgeID    uint64      `json:"knowledgeID" note:"知识id"`
	Status 		   uint64  	   `json:"status" note:"记录完成情况，0-未完成，1-已完成"`
	ParentTaskName *string     `json:"parentTaskName" note:"父任务名"`
	Credit 		   uint64  	   `json:"credit" note:"积分，第一周为10分，第二周为6分，第三周为2分，不是在任务当天完成仅获得1分"`
	TaskID		   uint64      `json:"taskID" note:"对应taskRepository中的serialNo，为0则代表该任务不是按期完成的"`
}

type KnowledgeID struct {
	KnowledgeID    uint64      `json:"knowledgeID" note:"知识id"`
}

type ExpKnowledgeToPatientDayIndex struct {
	PatientID      uint64      `json:"patientID" note:"患者id"`
	DayIndex       uint64      `json:"DayIndex", note:"对应计划的日期"`
}

