package doctor

import "github.com/ktpswjz/httpserver/types"

type TlcTeamIntegral struct {
	SerialNo   uint64      `json:"serialNo" note:"队伍积分条目序列号"`
	TeamSno    uint64      `json:"teamSno" note:"队伍序列号"`
	TaskSno    uint64      `json:"taskSno" note:"任务的序列号"`
	Integral   uint64      `json:"integral" note:"获得的积分"`
	CreateTime *types.Time `json:"createTime" note:"积分获得的的时间"`
}
