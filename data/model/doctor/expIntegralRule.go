package doctor

type ExpIntegralRule struct {
	SerialNo         uint64  `json:"serialNo" note:"序号\n324\nFCM\n主键，自增"`
	Weigh            *uint64 `json:"weigh" note:"每日称重打卡：积分加x"`
	FirstThreeMeals  *uint64 `json:"firstThreeMeals" note:"每日三餐拍照打卡：完成第一次用餐打卡积分加x"`
	SecondThreeMeals *uint64 `json:"secondThreeMeals" note:"完成第二次用餐打卡积分加x"`
	ThirdThreeMeals  *uint64 `json:"thirdThreeMeals" note:"完成第三次用餐打卡积分加x"`
	FirstSteps       *uint64 `json:"firstSteps" note:"每日步数：每日运动步数6000-7999步,积分加x"`
	SecondSteps      *uint64 `json:"secondSteps" note:"每日步数8000-9999步，积分加x"`
	ThirdSteps       *uint64 `json:"thirdSteps" note:"10000步以上，积分加x"`
	Sport            *uint64 `json:"sport" note:"每日运动打卡：运动时长≥20分钟，并且上传照片，积分加x。"`
	LearnKnw         *uint64 `json:"learnKnw" note:"每日健康教育打卡, 积分加x"`
	OrgCode          string  `json:"orgCode" note:""`
}
