package doctor

type ViewPatientEx struct {
	ViewPatient

	SexText string `json:"sexText" note:"性别 0-未知, 1-男, 2-女，9-未说明 例如:男"`
	Age     int64  `json:"age" note:"年龄，单位岁，如60"`
	Tag     string `json:"tag" note:"人员标签"`
}

type ViewPatientFilter struct {
	PatientID          *uint64 `json:"patientID" note:"用户ID 关联医生用户登录表用户ID 例如:232442"`
	IdentityCardNumber string  `json:"identityCardNumber" note:"身份证号 例如:330106200012129876"`
}
