package doctor

import "github.com/ktpswjz/httpserver/types"

type ExpDoctorCommentToPatientSportDietRecordFilter struct {
	SportDietRecord   uint64      `json:"sportDietRecord" note:"对应sportDietRecord表中的序号"`
}

type ExpDoctorCommentToPatientUnreadGet struct {
	ReadFlag          uint64      `json:"readFlag" note:"患者是否已读，0-未读，1-已读"`
	PatientID         uint64      `json:"patientID" note:"患者id"`
}

type ExpDoctorCommentToPatientRead struct {
	SerialNo          uint64      `json:"serialNo" note:""`
	ReadFlag          uint64      `json:"readFlag" note:"患者是否已读，0-未读，1-已读"`
}

type ExpDoctorCommentToPatientFilter struct {
	SerialNo          uint64      `json:"serialNo" note:""`
}

type ExpDoctorCommentToPatientAllUnreadGet struct {
	ReadFlag          uint64      `json:"readFlag" note:"患者是否已读，0-未读，1-已读"`
}

type ExpDoctorCommentToPatientCreate struct {
	SerialNo          uint64      `json:"serialNo" note:""`
	SportDietRecord   uint64      `json:"sportDietRecord" note:"对应sportDietRecord表中的序号"`
	Comment           string      `json:"comment" note:"医生/营养师的评论"`
	ReadFlag          uint64      `json:"readFlag" note:"患者是否已读，0-未读，1-已读"`
	PatientID         uint64      `json:"patientID" note:"患者id"`
	CreateDateTime    *types.Time `json:"createDateTime" note:"创建时间"`
	UpdateTime        *types.Time `json:"updateTime" note:"修改时间"`
	TemplateID     	  string      `json:"templateID" note:"微信提醒的template_id，在微信提醒时要用到"`
}

type ExpDoctorCommentToPatientSportDietSnosFilter struct {
	SportDietRecordList   []uint64      `json:"sportDietRecordList" note:"对应sportDietRecord表中的序号"`
}

type ExpDoctorCommentToPatientWithDietInfo struct {
	SerialNo          uint64      `json:"serialNo" note:""`
	SportDietRecord   uint64      `json:"sportDietRecord" note:"对应sportDietRecord表中的序号"`
	Comment           string      `json:"comment" note:"医生/营养师的评论"`
	ReadFlag          uint64      `json:"readFlag" note:"患者是否已读，0-未读，1-已读"`
	PatientID         uint64      `json:"patientID" note:"患者id"`
	CreateDateTime    *types.Time `json:"createDateTime" note:"创建时间"`
	UpdateTime        *types.Time `json:"updateTime" note:"修改时间"`
	DietInfo 		  *SportDietRecord `json:"dietInfo"`
}


type ExpDoctorCommentToPatientTodayUnreadGet struct {
	ReadFlag          uint64      `json:"readFlag" note:"患者是否已读，0-未读，1-已读"`
	PatientID         uint64      `json:"patientID" note:"患者id"`
	CreateDateTime    *types.Time `json:"createDateTime" note:"创建时间"`
}

type ExpDoctorCommentToPatientReply struct {
	SerialNo          uint64      `json:"serialNo" note:""`
	PatientReply      *string      `json:"PatientReply" note:"用户的回复"`
}