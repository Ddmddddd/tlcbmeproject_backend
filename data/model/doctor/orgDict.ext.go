package doctor

type OrgDictCreate struct {
	OrgCode      string `json:"orgCode" note:"机构代码 组织机构代码 例如:2894782947239239"`
	OrgName      string `json:"orgName" note:"机构名称  例如:第一人民医院"`
	DivisionCode string `json:"divisionCode" note:"行政区划 例如:2332232"`
}

type OrgDictUpdate struct {
	SerialNo     uint64 `json:"serialNo" note:"序号 主键，自增 例如:324"`
	OrgName      string `json:"orgName" note:"机构名称  例如:第一人民医院"`
	DivisionCode string `json:"divisionCode" note:"行政区划 例如:2332232"`
}

type OrgAndDoctorInfo struct {
	OrgCode      string                    `json:"code" note:"机构代码 组织机构代码 例如:2894782947239239"`
	OrgName      string                    `json:"name" note:"机构名称  例如:第一人民医院"`
	DivisionCode string                    `json:"divisionCode" note:"行政区划 例如:2332232"`
	Doctors      []DoctorUserAuthsBaseInfo `json:"doctors" note:"医生列表  例如:"`
}

type OrgAndDoctorFilter struct {
	DivisionCode string `json:"divisionCode" note:"机构代码 组织机构代码 例如:2894782947239239"`
	UserType     uint64 `json:"userType" note:"用户类别 0-医生，1-健康管理师 例如:1"`
}
