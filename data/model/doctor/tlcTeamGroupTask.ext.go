package doctor

type TlcTeamGroupTaskGroupKindsInput struct {
	GroupKinds []string `json:"groupKinds" note:"输入的大队类型，营养师前端固定输入"`
}

type TlcTeamGroupTaskIntegratedInfo struct {
	BelongToGroup string   `json:"belongToGroup" note:"大队的名称"`
	GroupTaskList []string `json:"groupTaskList" note:"队伍的任务名称列表"`
	Flag          int      `json:"flag" note:"当前是否存在有效任务"`
}

type TlcTeamGroupTaskCreateInfo struct {
	BelongToGroup string   `json:"belongToGroup"`
	GroupTaskList []string `json:"groupTaskList"`
}

type TlcTeamGroupTaskStopInfo struct {
	BelongToGroup string   `json:"belongToGroup"`
	GroupTaskList []string `json:"groupTaskList"`
}
