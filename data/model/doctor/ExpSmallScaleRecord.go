package doctor

import "github.com/ktpswjz/httpserver/types"

type ExpSmallScaleRecord struct {
	SerialNo  		uint64      `json:"serialNo" note:"序号"`
	PatientID 		uint64      `json:"patientID" note:"用户ID"`
	SmallScaleID   	uint64      `json:"smallScaleID" note:"小量表ID"`
	Result    		interface{} `json:"result" note:"量表结果，JSON格式存储"`
	CreateDateTime	*types.Time `json:"createDateTime" note:"提交时间"`
}