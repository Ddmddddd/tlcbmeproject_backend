package doctor

import "github.com/ktpswjz/httpserver/types"

type VerifiedDoctorUser struct {
	SerialNo          uint64      `json:"serialNo" note:"序号 主键，自增 例如:324"`
	UserID            uint64      `json:"userID" note:"用户ID 关联医生用户登录表用户ID 例如:232442"`
	OrgCode           string      `json:"orgCode" note:"执业机构代码 该医生用户所在的执业机构 例如:897798"`
	UserType          *uint64     `json:"userType" note:"用户类别 0-医生，1-健康管理师，11-医生兼健康管理师 例如:0"`
	Certificate       string      `json:"certificate" note:"执业证书照片 例如:"`
	IdentityCardFront string      `json:"identityCardFront" note:"身份证正面照片 例如:"`
	IdentityCardBack  string      `json:"identityCardBack" note:"身份证背面照片 例如:"`
	VerifiedStatus    *uint64     `json:"verifiedStatus" note:"是否验证通过 0-未认证，1-已认证，2-认证不通过 例如:0"`
	VerifierID        *uint64     `json:"verifierID" note:"审核人ID 审核人对应的用户ID 例如:32423"`
	VerifierName      string      `json:"verifierName" note:"审核人姓名 例如:张三"`
	VerifiedDateTime  *types.Time `json:"verifiedDateTime" note:"审核时间 例如:2018-07-03 13:00:00"`
	RefuseReason      string      `json:"refuseReason" note:"审核不通过原因 最近一次审核不通过的原因。只有当状态为2时有效。 例如:信息不够完善"`
}
