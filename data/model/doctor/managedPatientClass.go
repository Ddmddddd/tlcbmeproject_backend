package doctor

type ManagedPatientClass struct {
	PatientID       uint64 `json:"patientID" note:"用户ID 关联患者用户登录表用户ID 例如:232442"`
	ManageClassCode uint64 `json:"manageClassCode" note:"管理分类代码，例如：1"`
}
