package doctor

type TlcPatientToRecKnowledge struct {
	Id           uint64 `json:"id" note:"患者推荐知识列表的id，主键"`
	PatientID    uint64 `json:"patientID" note:"患者id"`
	Knowledge_id uint64 `json:"knowledge_id" note:"知识id"`
	Dayindex     uint64 `json:"dayindex" note:"非个性知识安排的天数"`
	Status       uint64 `json:"status" note:"知识是否被读 0代表未读，1代表已读"`
	Stars        uint64 `json:"stars" note:"用户对知识的评分"`
}
