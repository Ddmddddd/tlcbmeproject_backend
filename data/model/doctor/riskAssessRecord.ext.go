package doctor

type RiskAssessRecordEx struct {
	RiskAssessRecord

	LevelText string `json:"levelText" note:"危险级别 1-低危,2-中危, 3-高危 例如:高危"`
}

type RiskAssessRecordFilter struct {
	SerialNo  *uint64  `json:"serialNo" note:"序号 主键，自增 例如:324"`
	PatientID *uint64  `json:"patientID" note:"用户ID 关联患者用户登录表用户ID 例如:232442"`
	Levels    []uint64 `json:"levels" note:"危险级别 1-低危,2-中危, 3-高危 例如:3"`
}
