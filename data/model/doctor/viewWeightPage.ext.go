package doctor

import "github.com/ktpswjz/httpserver/types"

type ViewWeightPageDataFilter struct {
	MeasureStartDate *types.Time `json:"measureStartDate" note:"测量开始时间 测量体重的时间 例如:2018-07-03 14:45:00"`
	MeasureEndDate   *types.Time `json:"measureEndDate" note:"测量结束时间 测量体重的时间 例如:2018-08-03 14:45:00"`
}

type ViewWeightPageDataFilterEx struct {
	ViewWeightPageDataFilter
	PatientID uint64 `json:"patientID" required:"true" note:"用户ID 关联患者用户登录表用户ID 例如:232442"`
}
