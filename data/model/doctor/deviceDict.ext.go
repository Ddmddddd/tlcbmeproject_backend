package doctor

type DeviceDictCreate struct {
	DeviceCode string `json:"deviceCode" note:"设备代码 设备代码 例如:200101"`
	DeviceName string `json:"deviceName" note:"设备名称  例如:悦奇血压计"`
	DeviceType string `json:"deviceType" note:"设备类别 为以下值之一：血压计、血糖仪、身高体重测量仪 例如:血压计"`
	OrgCode    string `json:"orgCode" note:"机构代码 关联机构表的代码字段 例如:2894782947239239"`
}

type DeviceDictUpdate struct {
	SerialNo   uint64 `json:"serialNo" note:"序号 主键，自增 例如:324"`
	DeviceCode string `json:"deviceCode" note:"设备代码 设备代码 例如:200101"`
	DeviceName string `json:"deviceName" note:"设备名称  例如:悦奇血压计"`
	DeviceType string `json:"deviceType" note:"设备类别 为以下值之一：血压计、血糖仪、身高体重测量仪 例如:血压计"`
	OrgCode    string `json:"orgCode" note:"机构代码 关联机构表的代码字段 例如:2894782947239239"`
}
