package doctor

type ExpFoodDictCategoryFilter struct {
	Category uint64 `json:"category" note:"分类"`
}

type FoodSearchFilter struct {
	Name string `json:"name" note:"名字"`
}

type FoodMultiSearchFilter struct {
	Name string `json:"name" note:"搜索词"`
	Type uint64 `json:"type" note:"0-viewfood, 1-food"`
}

type FoodMeasureUnitFilter struct {
	Serial_No uint64 `json:"serial_No" note:"食品在膳食数据库的id"`
	Source    string `json:"source"`
}


