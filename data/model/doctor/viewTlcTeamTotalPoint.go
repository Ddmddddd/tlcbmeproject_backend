package doctor

type TlcTeamTotalPoint struct {
	TeamSno           uint64 `json:"teamSno" note:"队伍序列号"`
	TeamTotalIntegral uint64 `json:"teamTotalIntegral" note:"队伍得到的总积分"`
	TeamNickName      string `json:"teamNickName" note:"小队的名称"`
	BelongToGroup     string `json:"belongToGroup" note:"小队所归属的大队"`
}
