package doctor

type DivisionDict struct {
	SerialNo           uint64  `json:"serialNo" note:"序号 主键，自增 例如:324"`
	ItemCode           string  `json:"itemCode" note:"行政区划代码"`
	ItemName           string  `json:"itemName" note:"行政区划名称"`
	FullName           string  `json:"fullName" note:"行政区划全称"`
	ParentDivisionCode string  `json:"parentDivisionCode" note:"上级行政区划代码"`
	ItemSortValue      *uint64 `json:"itemSortValue" note:"排序 用于字典项目排序，从1开始往后排 例如:1"`
	InputCode          string  `json:"inputCode" note:"输入码 用于快速输入，可以是拼音首字母或缩写 例如:gxy"`
	IsValid            *uint64 `json:"isValid" note:"有效性 用于描述本记录是否有效，0-无效，1-有效，默认值为1 例如:1"`
}
