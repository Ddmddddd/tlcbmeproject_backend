package doctor

import "tlcbme_project/data/model"

type ViewManagementApplicationReviewEx struct {
	ViewManagementApplicationReview

	SexText string `json:"sexText" note:"性别 0-未知, 1-男, 2-女，9-未说明 例如:男"`
	Age     int64  `json:"age" note:"年龄，单位岁，如60"`
}

type ViewManagementApplicationReviewFilter struct {
	SerialNo        *uint64  `json:"serialNo" note:"序号 主键，自增 例如:324"`
	PatientID       *uint64  `json:"patientID" note:"用户ID 关联患者用户登录表用户ID 例如:232442"`
	PatientName     string   `json:"patientName" note:"姓名  例如:张三"`
	ApplicationFrom string   `json:"applicationFrom" note:"来源 表示从哪里发起的申请，其值可以为：APP、微信小程序 例如:APP"`
	OrgVisitID      string   `json:"orgVisitID" note:"机构内就诊号 例如:123456"`
	HealthManagerID *uint64  `json:"healthManagerID" note:"健康管理师ID 患者注册时指定的健康管理师 例如:12432"`
	Statuses        []uint64 `json:"statuses" note:"审核状态 0-未审核，1-审核通过，2-审核不通过 例如:0"`

	IdentityCardNumber string       `json:"identityCardNumber" note:"身份证号 例如:330106200012129876"`
	Phone              string       `json:"phone" note:"联系电话 例如:13812344321"`
	Order              *model.Order `json:"order" note:"排序"`
}

type ViewManagementApplicationReviewFilterEx struct {
	ViewManagementApplicationReviewFilter

	VisitOrgCode string `json:"visitOrgCode" note:"就诊机构代码 例如:123"`
}
