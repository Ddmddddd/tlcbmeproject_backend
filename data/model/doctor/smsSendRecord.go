package doctor

import "github.com/ktpswjz/httpserver/types"

type SmsSendRecord struct {
	SerialNo         uint64      `json:"serialNo" note:"序号 主键，自增 例如:324"`
	ReceiverPhoneNum string      `json:"receiverPhoneNum" note:"接收者手机号 例如:13812345678"`
	TemplateType     *uint64     `json:"templateType" note:"模板类型 0-验证码，1-短信通知，2-推广短信 例如:1"`
	TemplateCode     string      `json:"templateCode" note:"模板代码 该条短信使用的短信模板代码，关联SmsTemplate表 例如:SMS_1330248924729"`
	SmsContent       string      `json:"smsContent" note:"短信内容 例如:【浙大健康小微】马晓阳您好，您的复诊时间是2019-10-10，请您于近日前往医院复查。祝您健康！"`
	PatientID        *uint64     `json:"patientID" note:"患者ID 例如:324"`
	OrgCode          string      `json:"orgCode" note:"管理机构代码 例如:4562432424"`
	Sender           uint64      `json:"sender" note:"发送者用户ID 例如:998"`
	SendDatetime     *types.Time `json:"sendDatetime" note:"短信发送时间 例如:2020-02-16 14:23:00"`
}
