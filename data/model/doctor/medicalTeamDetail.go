package doctor

type MedicalTeamDetail struct {
	SerialNo     uint64 `json:"serialNo" note:"序号 主键，自增 例如:324"`
	TeamCode     uint64 `json:"teamCode" note:"团队代码 对应医疗团队的团队ID 例如:1"`
	UserID       uint64 `json:"userID" note:"医生用户ID 对应医生用户表的用户ID字段 例如:123"`
	IsTeamLeader uint64 `json:"isTeamLeader" note:"是否团队负责人 0-普通成员，1-团队负责人 例如:0"`
}
