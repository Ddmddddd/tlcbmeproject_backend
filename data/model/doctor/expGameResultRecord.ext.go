package doctor

import "github.com/ktpswjz/httpserver/types"

type ExpGameResultRecordFilter struct {
	PatientID      uint64      `json:"patientID" note:"用户ID"`
	StartDateTime *types.Time `json:"startDateTime" note:"起始时间"`
}

type ExpGameResultRecordOrderFilter struct {
	StartDateTime *types.Time `json:"startDateTime" note:"完成时间"`
}