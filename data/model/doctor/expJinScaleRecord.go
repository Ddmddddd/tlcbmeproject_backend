package doctor

import "github.com/ktpswjz/httpserver/types"

type ExpJinScaleRecord struct {
	SerialNo       uint64      `json:"serialNo" note:""`
	PatientID      uint64      `json:"patientID" note:"患者id"`
	ScaleName      string      `json:"scaleName" note:"问卷名称"`
	SerialNumber   float64     `json:"serialNumber" note:"对应金数据里的serial_number"`
	Record         interface{} `json:"record" note:"问卷内容"`
	CreateDateTime *types.Time `json:"createDateTime" note:""`
}
