package doctor

type Actor struct {
	ActorID   uint64 `json:"actorID" note:"标识ID"`
	ActorName string `json:"actorName" note:"姓名"`
}
