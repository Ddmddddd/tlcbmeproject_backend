package doctor

type ViewPatientBaseInfoEx struct {
	ViewPatientBaseInfo

	SexText string `json:"sexText" note:"性别 0-未知, 1-男, 2-女，9-未说明 例如:男"`
	Age     int64  `json:"age" note:"年龄，单位岁，如60"`
	Tag     string `json:"tag" note:"人员标签"`

	FollowupDays     int64 `json:"followupDays" note:"计划随访日期与当天相差天数，例如:0表示今天，1表示明天， -1表示昨天"`
	LastFollowupDays int64 `json:"lastFollowupDays" note:"上次随访日期与当天相差天数，例如:0表示今天，1表示明天， -1表示昨天"`
	ManageDays       int64 `json:"manageDays" note:"已管理天数"`
}

type ViewPatientBaseInfoFilter struct {
	PatientID *uint64 `json:"patientID" note:"用户ID 关联医生用户登录表用户ID 例如:232442"`
}

type ViewPatientBaseInfoFilterEx struct {
	ViewPatientBaseInfoFilter

	OrgCode string `json:"orgCode" note:"管理机构代码 管理该患者的机构 例如:897798"`
}
