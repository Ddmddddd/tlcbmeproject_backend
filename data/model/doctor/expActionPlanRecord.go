package doctor

import "github.com/ktpswjz/httpserver/types"

type ExpActionPlanRecord struct {
	SerialNo       uint64      `json:"serialNo" note:"序号"`
	PatientID      uint64      `json:"patientID" note:"用户ID"`
	ActionPlan     string      `json:"actionPlan" note:"行动计划"`
	Completion     uint64      `json:"completion" note:"行动计划完成情况，0-少量完成，1-部分完成，2-全部完成"`
	Level          uint64      `json:"level" note:"优先级，分为0-5，数值越大优先级越高"`
	CreateDateTime *types.Time `json:"createDateTime" note:"创建时间"`
	SeqActionPlan  uint64	   `json:"seqActionPlan" note:"对应引擎端生活计划的id，用于提交、修改等操作"`
	ActionPlanID   uint64	   `json:"actionPlanID" note:"生活计划ID，对应ExpActionPlan中的SerialNo"`
}
