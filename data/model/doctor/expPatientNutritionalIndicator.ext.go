package doctor

type ExpPatientNutritionInterventionTotal struct {
	GoalEnergy          *float64                  `json:"goalEnergy" note:"目标热量值 单位千卡"`
	GoalCarbohydrateMin *float64                  `json:"goalCarbohydrateMin" note:"目标碳水化合物摄入范围下限"`
	GoalCarbohydrateMax *float64                  `json:"goalCarbohydrateMax" note:"目标碳水化合物摄入范围上限"`
	GoalFatMin          *float64                  `json:"goalFatMin" note:"目标脂肪摄入范围下限"`
	GoalFatMax          *float64                  `json:"goalFatMax" note:"目标脂肪摄入范围上限"`
	GoalProteinMin      *float64                  `json:"goalProteinMin" note:"目标蛋白质摄入范围下限"`
	GoalProteinMax      *float64                  `json:"goalProteinMax" note:"目标蛋白质摄入范围上限"`
	GoalWeight          *float64                  `json:"goalWeight" note:"目标体重"`
	GoalProtein         *float64                  `json:"goalProtein" note:"目标蛋白质摄入量"`
	GoalDF              *float64                  `json:"goalDF" note:"目标膳食纤维摄入量"`
	DietProgramID       *uint64                   `json:"dietProgramID" note:"膳食方案"`
	DietPlanList        []*ExpPatientDietPlanFour `json:"dietPlanList" note:"饮食习惯列表"`
	DietProgramList     []*ExpPatientDietProgram  `json:"dietProgramList" note:"饮食方案内容"`
}
