package doctor

type FollowupTemplateDict struct {
	SerialNo        uint64 `json:"serialNo" note:"序号 主键，自增 例如:324"`
	ItemCode        uint64 `json:"itemCode" note:"字典项代码  例如:1"`
	ItemName        string `json:"itemName" note:"字典项名称  例如:高血压随访模板"`
	ManageClassCode uint64 `json:"manageClassCode" note:"模板所属管理分类代码 表示当前模板属于哪类慢病 例如:2323"`
	InputCode       string `json:"inputCode" note:"输入码 用于快速输入，可以是拼音首字母或缩写 例如:gxy"`
	IsValid         uint64 `json:"isValid" note:"有效性 用于描述本记录是否有效，0-无效，1-有效，默认值为1 例如:1"`
}
