package doctor

import "github.com/ktpswjz/httpserver/types"

type TlcHeEduPatientQuestion struct {
	Id          uint64      `json:"id" note:"用户问题卡片记录的id，主键"`
	PatientID   uint64      `json:"patientID" note:"用户的id,表明该条记录对应的用户"`
	Question_id uint64      `json:"question_id" note:"对应的问题的id"`
	Status      uint64      `json:"status" note:"回答问题的状态"`
	Answer_time *types.Time `json:"answer_time" note:"用户回答问题的时间"`
}
