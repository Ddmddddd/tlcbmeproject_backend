package doctor

type ExpDoctorCommentTemplateType struct {
	TemplateType uint64 `json:"templateType" note:"模板类型，1-饮食评论模板"`
	EditorID     uint64 `json:"editorID" note:"编辑者id"`
}

type ExpDoctorCommentTemplateCreate struct {
	TemplateType    uint64 `json:"templateType" note:"模板类型，1-饮食评论模板"`
	TemplateContent string `json:"templateContent" note:"模板内容"`
	OpenType        uint64 `json:"openType" note:"开放属性，0-所有，1-私人"`
	EditorID        uint64 `json:"editorID" note:"编辑者id"`
}

type ExpDoctorCommentTemplateDelete struct {
	SerialNo uint64 `json:"serialNo" note:""`
	EditorID uint64 `json:"editorID" note:"编辑者id"`
}
