package doctor

type ExpPatientUploadFile struct {
	SerialNo                 uint64 `json:"serialNo" note:""`
	PatientID                uint64 `json:"patientID" note:"患者id"`
	HealthReport             string `json:"healthReport" note:"健康方案地址"`
	HealthReportName         string `json:"healthReportName" note:"健康方案文档名称"`
	DietPrescription         string `json:"dietPrescription" note:"饮食处方地址"`
	DietPrescriptionName     string `json:"dietPrescriptionName" note:"饮食处方文档名称"`
	ExercisePrescription     string `json:"exercisePrescription" note:"运动处方地址"`
	ExercisePrescriptionName string `json:"exercisePrescriptionName" note:"运动处方地址"`
}
