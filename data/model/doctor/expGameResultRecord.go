package doctor

import "github.com/ktpswjz/httpserver/types"

type ExpGameResultRecord struct {
	SerialNo       uint64      `json:"serialNo" note:"序号"`
	PatientID      uint64      `json:"patientID" note:"用户ID"`
	PatientName    string      `json:"patientName" note:"用户姓名，涉及隐私，可考虑让用户选择录入一个昵称"`
	Point          uint64      `json:"point" note:"得分"`
	CreateDateTime *types.Time `json:"createDateTime" note:"完成时间"`
}
