package doctor

type ExpCommonScaleSerialNoFilter struct {
	SerialNo    uint64      `json:"serialNo" note:"序号"`
}

type ExpCommonScaleSerialNoListFilter struct {
	SerialNos	[]uint64	`json:"serialNos" note:"序号组"`
}