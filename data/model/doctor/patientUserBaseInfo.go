package doctor

import "github.com/ktpswjz/httpserver/types"

type PatientUserBaseInfo struct {
	SerialNo           uint64      `json:"serialNo" note:"序号 主键，自增 例如:324"`
	UserID             uint64      `json:"userID" note:"用户ID 关联医生用户登录表用户ID 例如:232442"`
	Name               string      `json:"name" note:"姓名  例如:张三"`
	Sex                *uint64     `json:"sex" note:"性别 0-未知, 1-男, 2-女，9-未说明 例如:1"`
	DateOfBirth        *types.Time `json:"dateOfBirth" note:"出生日期 例如:2000-12-12"`
	IdentityCardNumber string      `json:"identityCardNumber" note:"身份证号 例如:330106200012129876"`
	Country            string      `json:"country" note:"国籍 GB/T 2659-2000 世界各国和地区名称 例如:中国"`
	Nation             string      `json:"nation" note:"民族 GB 3304-1991 中国各民族名称 例如:汉族"`
	NativePlace        string      `json:"nativePlace" note:"籍贯  例如:浙江杭州"`
	MarriageStatus     string      `json:"marriageStatus" note:"婚姻状况 GB/T 2261.2-2003 例如: "`
	EducationLevel     string      `json:"educationLevel" note:"文化程度 GB/T 4658-1984 文化程度 例如:大学"`
	JobType            string      `json:"jobType" note:"职业类别 GB/T 6565-1999 职业分类 例如: "`
	Photo              string      `json:"photo" note:"头像照片 例如:"`
	Nickname           string      `json:"nickname" note:"昵称 例如:蓝精灵"`
	PersonalSign       string      `json:"personalSign" note:"个性签名 例如:在那山的那边海的那边有一群蓝精灵"`
	Phone              string      `json:"phone" note:"联系电话 例如:13812344321"`
	Height             *uint64     `json:"height" note:"身高 单位：cm 例如:165"`
	Weight             *float64    `json:"weight" note:"体重 最后一次录入的体重，单位：kg 例如:60.5"`
	Waistline          *float64    `json:"waistline" note:"腰围 最后一次录入的腰围，单位：cm 例如:60.5"`
}
