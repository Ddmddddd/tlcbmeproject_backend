package doctor

type VerifiedDoctorUserEx struct {
	VerifiedDoctorUser

	UserTypeText       string `json:"userTypeText" note:"用户类别文本 0-医生，1-健康管理师 例如:医生"`
	VerifiedStatusText string `json:"verifiedStatusText" note:"认证状态文本 0-未认证，1-已认证，2-认证不通过 例如:未认证"`
	OrgName            string `json:"orgName" note:"执业机构名称 例如:第一人民医院"`
}

type VerifiedDoctorUserEdit struct {
	UserID            uint64  `json:"userID" required:"true" note:"用户ID 关联医生用户登录表用户ID 例如:232442"`
	OrgCode           string  `json:"orgCode" note:"执业机构代码 该医生用户所在的执业机构 例如:897798"`
	UserType          *uint64 `json:"userType" note:"用户类别 0-医生，1-健康管理师 例如:0"`
	Certificate       string  `json:"certificate" note:"执业证书照片 例如:"`
	IdentityCardFront string  `json:"identityCardFront" note:"身份证正面照片 例如:"`
	IdentityCardBack  string  `json:"identityCardBack" note:"身份证背面照片 例如:"`
	VerifiedStatus    *uint64 `json:"verifiedStatus" note:"是否验证通过 0-未认证，1-已认证，2-认证不通过 例如:0"`
}

type VerifiedDoctorUserFilter struct {
	UserID uint64 `json:"userID" required:"true" note:"用户ID 关联医生用户登录表用户ID 例如:232442"`
}
