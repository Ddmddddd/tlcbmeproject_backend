package doctor

import "github.com/ktpswjz/httpserver/types"

type WeChatRemindRecord struct {
	SerialNo       uint64      `json:"serialNo" note:"序号"`
	PatientID      uint64      `json:"patientID" note:"患者ID"`
	ReceiverOpenID string      `json:"receiverOpenID" note:"接收患者的openid"`
	TemplateID     string      `json:"templateID" note:"发送消息对应的订阅消息模板"`
	RemindContent  interface{} `json:"remindContent" note:"发送消息的具体内容"`
	SendDateTime   *types.Time `json:"sendDateTime" note:"发送时间"`
	Response 	   interface{}     `json:"response" note:"请求响应"`
}
