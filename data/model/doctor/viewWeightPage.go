package doctor

import "github.com/ktpswjz/httpserver/types"

type ViewWeightPage struct {
	SerialNo          uint64      `json:"serialNo" note:"序号 主键，自增 例如:324"`
	PatientID         uint64      `json:"patientID" note:"用户ID 关联患者用户登录表用户ID 例如:232442"`
	Weight            float64     `json:"weight" note:"体重 单位：kg 例如:60.5"`
	Waistline         *float64    `json:"waistline" note:"腰围 单位：cm 例如:60.5"`
	Memo              string      `json:"memo" note:"备注 用于说明测量时的情况 例如:有氧运动半小时后"`
	MeasureDateTime   *types.Time `json:"measureDateTime" note:"测量时间 测量体重时的时间 例如:2018-07-03 14:45:00"`
	Photo             string      `json:"photo" note:""`
	Water             *float64    `json:"water" note:""`
	Sleep             *float64    `json:"sleep" note:""`
	BloodGlucose      *float64    `json:"bloodGlucose" note:"血糖值 单位：mmol/L 例如:6.1"`
	TimePoint         string      `json:"timePoint" note:"测量时间点 空腹、早餐前、早餐后2h、午餐前、午餐后2h、晚餐前、晚餐后2h、睡前、夜间 例如:空腹"`
	SystolicPressure  *uint64     `json:"systolicPressure" note:"收缩压 单位：mmHg 例如:110"`
	DiastolicPressure *uint64     `json:"diastolicPressure" note:"舒张压 单位：mmHg 例如:78"`
	BloodPressureNo   *uint64     `json:"bloodPressureNo""`
	BloodGlucoseNo    *uint64     `json:"bloodGlucoseNo"`
}
