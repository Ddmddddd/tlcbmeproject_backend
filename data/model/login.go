package model

import (
	"errors"
	"strings"
)

type Login struct {
	Token       string   `json:"token" note:"接口访问凭证" example:"7faf10b0bde847c9905c93966594c82b"`
	Account     string   `json:"account" required:"true" note:"账号名称"`
	RightIDList []uint64 `json:"rightIDList"  note:"用户权限列表"`
	Host        string   `json:"host"`
}

type LoginFilter struct {
	Account      string `json:"account" required:"true" note:"账号名称"`
	Password     string `json:"password" required:"true" note:"账号密码"`
	CaptchaId    string `json:"captchaId" required:"true" note:"验证码ID"`
	CaptchaValue string `json:"captchaValue" required:"true" note:"验证码"`
	Encryption   string `json:"encryption" note:"密码加密方法: 空-明文(默认); rsa-RSA密文(公钥通过调用获取验证码接口获取)"`
}

type WeChatLoginFilter struct {
	Identitier string `json:"identitier" required:"true" note:"微信open-id"`
	Status     string `json:"status" required:"true" note:"绑定微信状态"'`
}

func (s *LoginFilter) Check(captchaRequired, passwordRequired bool) error {
	if strings.TrimSpace(s.Account) == "" {
		return errors.New("账号为空")
	}
	if passwordRequired {
		if strings.TrimSpace(s.Password) == "" {
			return errors.New("密码为空")
		}
	}
	if strings.TrimSpace(s.CaptchaId) == "" {
		return errors.New("验证码ID为空")
	}
	if captchaRequired {
		if strings.TrimSpace(s.CaptchaValue) == "" {
			return errors.New("验证码为空")
		}
	}

	return nil
}

type UserNameValidate struct {
	UserName string `json:"userName" note:"用户名 用户登录表用户名 例如:cdm_user_007"`
}

type WeChatCodeFilter struct {
	Code string `json:"code" note:"微信code，用于获取微信openid"`
	Env  uint64 `json:"env" note:"微信小程序环境,对应枚举类appEnv 例如:0-测试服,1-浙大健康小微,2-慢病微管家"`
}

type SendCodePhone struct {
	Phone string `json:"phone"`
}

type NewChangePasswordWithPhone struct {
	UserName string `json:"userName" note:"用户名 用户登录表用户名 例如:cdm_user_007"`
	Password string `json:"password"`
}
