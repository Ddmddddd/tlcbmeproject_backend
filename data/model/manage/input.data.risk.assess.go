package manage

import "github.com/ktpswjz/httpserver/types"

type InputDataRiskAssess struct {
	Level        uint64      `json:"level" note:"危险级别, 例如：3"`
	Name         string      `json:"memo" note:"名称, 例如：高血压危险分层评估"`
	Memo         string      `json:"memo" note:"备注, 例如：高危"`
	NextSchedule *types.Time `json:"nextSchedule" note:"下次评估时间, 例如：2018-08-22 00:00:00"`
}
