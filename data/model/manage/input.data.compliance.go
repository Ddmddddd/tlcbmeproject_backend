package manage

type InputDataCompliance struct {
	Name         string  `json:"name" note:"状态说明, 例如：测量依从度"`
	Code         int     `json:"code" note:"状态说明, 例如：1"`
	Value        float64 `json:"value" note:"状态说明, 例如：5.0"`
	NoActiveDays int     `json:"noActiveDays" note:"连续多少天没有提交数据, 例如：14天"`
}
