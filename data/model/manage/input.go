package manage

import (
	"github.com/ktpswjz/httpserver/types"
)

type Input struct {
	Data InputData `json:"data" note:"数据"`
}

type PatientInfo struct {
	Name               string      `json:"name" note:"姓名"`
	Gender             string      `json:"gender" note:"性别"`
	Birthday           *types.Time `json:"birthday" note:"出生日期"`
	Phone              string      `json:"tel" note:"电话"`
	IdentityCardNumber string      `json:"idNum" note:"身份证号"`
}

type DiseaseInfo struct {
	Name string `json:"name" note:"名称, 高血压、糖尿病，例如:高血压"`
	Code uint64 `json:"code" note:"代码， 1-1型糖尿病，2-2型糖尿病，3-妊娠糖尿病"`
	Type string `json:"type" note:"名称 例如:1型糖尿病"`
}

type BloodPressureRecord struct {
	SerialNo          string      `json:"dataId" note:" 数据Id"`
	SystolicPressure  uint64      `json:"sbp" note:"收缩压 单位：mmHg 例如:110"`
	DiastolicPressure uint64      `json:"dbp" note:"舒张压 单位：mmHg 例如:78"`
	HeartRate         *uint64     `json:"hr" note:"心率 单位：次/分钟 例如:65"`
	MeasureDateTime   *types.Time `json:"measuredTime" note:"测量时间 测量血压的时间 例如:2018-07-03 14:45:00"`
}

type BloodGlucoseRecord struct {
	SerialNo        string      `json:"dataId" note:"序号 主键，自增 例如:324"`
	BloodType       uint64      `json:"code" note:"测量时间点代码 1-晨起空腹、2-早餐后、3-午餐前、4-午餐后、5-晚餐前、6-晚餐后、7-睡前、8-凌晨 例如:1"`
	BloodGlucose    float64     `json:"value" note:"血糖值 单位：mmol/L 例如:6.1"`
	Other           Other       `json:"other" `
	TimePoint       *string     `json:"flag" note:"测量时间点 晨起空腹、早餐后、午餐前、午餐后、晚餐前、晚餐后、睡前、凌晨 例如:晨起空腹"`
	MeasureDateTime *types.Time `json:"measuredTime" note:"测量时间 测量血糖时的时间 例如:2018-07-03 14:45:00"`
}

type Other struct {
	BloodKetone float64 `json:"bloodKetone"`
}

func (s *BloodGlucoseRecord) ToCode() uint64 {
	if s.TimePoint == nil {
		return 0
	}

	text := *s.TimePoint
	if text == "晨起空腹" {
		return 1
	} else if text == "早餐后" {
		return 2
	} else if text == "午餐前" {
		return 3
	} else if text == "午餐后" {
		return 4
	} else if text == "晚餐前" {
		return 5
	} else if text == "晚餐后" {
		return 6
	} else if text == "睡前" {
		return 7
	} else if text == "凌晨" {
		return 8
	}

	return 0
}

type DiscomfortRecord struct {
	SerialNo       string      `json:"dataId" note:"序号 主键，自增 例如:324"`
	Discomfort     []string    `json:"symptoms" note:"不适情况的数组"`
	HappenDateTime *types.Time `json:"occurrenceTime" note:"发生时间 不适记录发生的时间 例如:2018-07-03 14:45:00"`
}

type DiseaseInfoInput struct {
	PatientID string `json:"patientId" note:"用户ID  例如:232442"`

	Info DiseaseInfo `json:"disease" note:"疾病信息"`
}

type BloodPressureInput struct {
	PatientID string `json:"patientId" note:"用户ID  例如:232442"`

	Record BloodPressureRecord `json:"bpHr" note:"血压数据"`
}

type BloodGlucoseInput struct {
	PatientID string `json:"patientId" note:"用户ID  例如:232442"`

	Record BloodGlucoseRecord `json:"glu" note:"血糖数据"`
}

type WeightInput struct {
	PatientID string `json:"patientId" note:"用户ID  例如:232442"`

	Weight WeightData  `json:"weight"`
	Height *HeightData `json:"height"`
	Wc     *WcData     `json:"wc"`
}

type DiscomfortInput struct {
	PatientID string `json:"patientId" note:"用户ID  例如:232442"`

	Record DiscomfortRecord `json:"discomforts" note:"不适记录数据"`
}
type DeleteDiscomfortInput struct {
	PatientID string `json:"patientId" note:"用户ID  例如:232442"`

	DataId string `json:"dataId" note:"不适记录数据编号"`
}

type BodyInfo struct {
	Height *uint64  `json:"height"`
	Weight *float64 `json:"weight"`
	Wc     *float64 `json:"wc"`
}

type PatientInfoInput struct {
	PatientID string `json:"patientId" note:"用户ID  例如:232442"`

	Info     PatientInfo `json:"socialInfo" note:"基本信息"`
	BodyInfo BodyInfo    `json:"baseInfo" note:"身高 体重信息"`
}

type PatientHbpAssessmentInput struct {
	PatientID string `json:"patientId" note:"用户ID  例如:232442"`

	Data interface{} `json:"data" note:"诊断相关数据"`
}

type DeleteBloodPressureRecordInput struct {
	PatientID string `json:"patientId" note:"用户ID  例如:232442"`
	SerialNo  string `json:"dataId" note:"序号 主键，自增 例如:324"`
}

type DeleteBloodGlucoseRecordInput struct {
	PatientID string `json:"patientId" note:"用户ID  例如:232442"`
	SerialNo  string `json:"dataId" note:"序号 主键，自增 例如:324"`
}

type FollowupFailureInfo struct {
	FailureReason *string     `json:"failureReason" note:"失访原因 只有当Status为0时该字段才有意义，用于描述失访的原因。 例如:电话停机"`
	DeathTime     *types.Time `json:"deathTime" note:"死亡时间 该字段只有当失访原因为亡故时才有意义 例如:2018-08-02"`
	CauseOfDeath  *string     `json:"causeOfDeath" note:"死亡原因 该字段只有当失访原因为亡故时才有意义 例如:车祸死亡"`
}

type FollowupInfo struct {
	FollowupMethod   string      `json:"followMethod" note:"随访方式 例如：电话、门诊、家庭、APP 例如:电话"`
	FollowupDateTime *types.Time `json:"followupTime" note:"随访时间  例如:2018-08-01 10:00:00"`
	Status           uint64      `json:"status" note:"结果状态 用于描述本次随访是否有效，0-失访，1-进行中，2-有效，默认值为1 例如:1"`

	FailureInfo FollowupFailureInfo `json:"failureInfo" note:"失访原因"`
	Content     interface{}         `json:"content" note:"随访内容"`
}

type FollowupInfoInput struct {
	PatientID string `json:"patientId" note:"用户ID  例如:232442"`

	Info FollowupInfo `json:"record" note:"随访记录"`
}
type EnableManageInput struct {
	PatientID string `json:"patientId" note:"用户ID  例如:232442"`
}

type ExpPatientInput struct {
	PatientID 	  string `json:"patientId" note:"用户ID  例如:232442"`
	ManageType 	  uint64 `json:"manageType" note:"用户实验分组，1-实验组，2-对照组"`
	Type          uint64 `json:"type" note:"用户管理状态，0-停止实验，1-实验中"`
}

type ActionPlanInput struct {
	PatientID 	      string 			`json:"patientId" note:"用户ID  例如:232442"`
	ActionPlanRecord  ActionPlanRecord  `json:"actionPlan" note:"行动计划信息"`
}

type ActionPlanRecord struct {
	SerialNo       string      `json:"dataId" note:"序号 主键，自增 例如:324"`
	SeqActionPlan  uint64 	   `json:"seqActionPlan" note:"生活计划，具体内容"`
	Completion	   uint64	   `json:"completion" note:"完成度，0-部分完成，1-全部完成"`
	ActionTime	   *types.Time `json:"actionTime" note:"行动时间"`
}

type ExpScaleRecordInput struct {
	PatientID 	    string 		  	`json:"patientId" note:"用户ID  例如:232442"`
	ExpScaleRecord  ExpScaleRecord 	`json:"scaleRecord" note:"量表信息"`
	Flag 			bool			`json:"flag" note:"标志位，true-需要生活计划，false-不需要生活计划"`
}

type ExpScaleRecord struct {
	SerialNo       			string        	`json:"dataId" note:"序号 主键，自增 例如:324"`
	BarrierInfo    			[]*BarrierInfo 	`json:"barrierInfos" note:"健康饮食障碍信息"`
	AssessmentSheetVersion  uint64			`json:"assessmentSheetVersion" note:"量表编号"`
}

type BarrierInfo struct {
	CheckedType   uint64	`json:"checkedType" note:"选择类型，0-单选，1-多选"`
	Degree		  []uint64  `json:"degree" note:"每个选项对应的程度"`
	Barriers	  []uint64	`json:"barriers" note:"关联的健康饮食障碍序号"`
	Checked		  []uint64	`json:"checked" note:"选择的序号"`
}

type ExpActionPlanEditInput struct {
	PatientID 	    	string 		  		`json:"patientId" note:"用户ID  例如:232442"`
	ActionPlanEditInfo 	ActionPlanEditInfo 	`json:"actionPlanEditInfo" note:"修改信息"`
}

type ActionPlanEditInfo struct {
	SeqActionPlan	uint64	`json:"seqActionPlan" note:"生活计划序号，对应引擎端数据库的主键"`
	ActionPlan      string	`json:"actionPlan" note:"生活计划具体内容"`
	EditType		uint64	`json:"editType" note:"编辑者，0-引擎，1-非引擎"`
	Status 			uint64  `json:"status" note:"计划状态，0-正常，1-停用"`
	Level 			uint64  `json:"level" note:"计划优先级"`
}

type ActionPlanListInput struct {
	PatientID 	      string 				`json:"patientId" note:"用户ID  例如:232442"`
	ActionPlanRecord  []*ActionPlanRecord   `json:"actionPlanList" note:"行动计划列表"`
}

type ExpActionPlanRecordEditInput struct {
	PatientID 	    			string 		  				`json:"patientId" note:"用户ID  例如:232442"`
	ActionPlanRecordEditInfo 	ActionPlanRecordEditInfo 	`json:"actionPlanRecordEdit" note:"修改信息"`
}

type ActionPlanRecordEditInfo struct {
	SerialNo	    string		`json:"dataId" note:"生活计划序号，对应引擎端数据库的主键"`
	Completion	   	uint64	   	`json:"completion" note:"完成度，0-部分完成，1-全部完成"`
	ActionTime	   	*types.Time `json:"actionTime" note:"行动时间"`
}

type HeightData struct {
	DataId       *string     `json:"dataId"`
	Value        *uint64     `json:"value"`
	MeasuredTime *types.Time `json:"measuredTime" note:""`
}

type WeightData struct {
	DataId       *string     `json:"dataId"`
	Value        *float64    `json:"value"`
	MeasuredTime *types.Time `json:"measuredTime" note:""`
}

type WcData struct {
	DataId       *string     `json:"dataId"`
	Value        *float64    `json:"value"`
	MeasuredTime *types.Time `json:"measuredTime" note:""`
}
