package manage

type InputDataActionPlan struct {
	SeqActionPlan  	uint64  `json:"seqActionPlan" note:"生活计划序号"`
	Title			string	`json:"title" note:"生活计划标题"`
	ActionPlan  	string 	`json:"actionPlan" note:"生活计划内容"`
	Description 	string 	`json:"description" note:"生活计划的具体描述"`
	SeqBarrier  	uint64 	`json:"seqBarrier" note:"链接的障碍"`
	Level 			uint64 	`json:"level" note:"生活计划的优先级"`
	Type 			string 	`json:"type" note:"计划的类型"`
}

