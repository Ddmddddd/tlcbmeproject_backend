package manage

import "strconv"

type OutputData struct {
	PatientID string            `json:"patientId" required:"true" note:"患者编号"`
	Alerts    []OutputDataAlert `json:"warnings" note:"危险预警"`
}

func (s *OutputData) GetPatientID() (uint64, error) {
	return strconv.ParseUint(s.PatientID, 10, 64)
}

func (s *OutputData) GetAlerts() []OutputDataAlert {
	alerts := make([]OutputDataAlert, 0)

	if len(s.Alerts) > 0 {
		for _, item := range s.Alerts {
			alerts = append(alerts, item)
		}
	}

	return alerts
}
