package manage

import "github.com/ktpswjz/httpserver/types"

type OutputDataAlert struct {
	Code     string      `json:"code" required:"true" note:"预警代码"`
	Type     string      `json:"type" required:"true" note:"预警类型"`
	Name     string      `json:"name" required:"true" note:"预警名称"`
	Reason   string      `json:"reason" note:"预警原因"`
	Message  string      `json:"message" note:"预警消息"`
	DateTime *types.Time `json:"dateTime" required:"true" note:"预警发生时间"`
}
