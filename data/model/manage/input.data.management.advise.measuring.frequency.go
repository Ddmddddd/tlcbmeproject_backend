package manage

type InputDataManagementAdviseMeasuringFrequency struct {
	Name       string   `json:"name" note:"名称, 例如：血压测量"`
	Interval   int      `json:"interval" note:"间隔, 例如：1"`
	Unit       string   `json:"unit" note:"单位, 例如：天,周,月"`
	Times      uint64   `json:"times" note:"次数, 例如：3"`
	TimePoints []string `json:"timePoints" note:"时间点，1-早；2-中；3-晚"`
	Memo       []string `json:"memo" note:"每次测2~3遍，取平均值"`
}
