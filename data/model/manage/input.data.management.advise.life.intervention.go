package manage

type InputDataManagementAdviseLifeIntervention struct {
	Title string `json:"title" note:"标题, 例如：生活处方"`

	Prescription []InputDataManagementAdviseLifeInterventionPrescription `json:"prescription" note:"处方"`
}
