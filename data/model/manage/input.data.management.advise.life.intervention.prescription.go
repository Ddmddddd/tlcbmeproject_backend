package manage

type InputDataManagementAdviseLifeInterventionPrescription struct {
	Title string `json:"title" note:"标题, 例如：饮食处方"`
	Value string `json:"value" note:"内容, 例如：低盐低脂饮食，食盐摄入量<6g，控制主食，晚餐 5-7 分饱，新鲜蔬菜 500g 水果 1 个（苹果、梨、桃子等）"`
}
