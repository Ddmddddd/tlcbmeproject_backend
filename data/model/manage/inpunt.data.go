package manage

import "strconv"

const (
	FlagRiskAssess int = 0x01 // 危险评估
	FlagManagement int = 0x02 // 管理计划
	FlagFollowup   int = 0x04 // 随访排期
	FlagWarnings   int = 0x08 // 危险预警
	FlagCompliance int = 0x10 // 依从度
	FlagBarrier    int = 0x80 //健康饮食障碍（饮食实验）
	FlagActionPlan int = 0x100 // 生活计划（饮食实验）
)

type InputData struct {
	Flag      int    `json:"flag" note:"0x01-危险评估(1-RiskAssess); 0x02-管理计划(2-Management); 0x04-随访排期(4-Followup); 0x08-危险预警(8-Warnings); 0x10-依从度(16-Compliance); 0x80-生活计划(128-ActionPlans); 0x100-健康饮食障碍(256-barriers)"`
	PatientID string `json:"patientId" note:"患者编号"`

	RiskAssess  *InputDataRiskAssess  `json:"riskAssess" note:"危险评估"`
	Management  *InputDataManagement  `json:"management" note:"管理计划"`
	Followup    *InputDataFollowup    `json:"followup" note:"随访排期"`
	Warnings    []InputDataWarnings   `json:"warnings" note:"危险预警"`
	Compliance  []InputDataCompliance `json:"compliance" note:"依从度"`
	Barriers    BarrierResult	  `json:"barriers" note:"健康饮食障碍"`
	ActionPlans []InputDataActionPlan `json:"actionPlans" note:"生活计划"`
}

func (s *InputData) ContainFlag(flag int) bool {
	if (s.Flag & flag) == 0 {
		return false
	} else {
		return true
	}
}

func (s *InputData) GetPatientID() (uint64, error) {
	return strconv.ParseUint(s.PatientID, 10, 64)
}
