package manage

type InputDataManagement struct {
	Rank int    `json:"rank" note:"等级, 例如：1"`
	Memo string `json:"memo" note:"分级说明, 例如：管理初期"`

	Advise *InputDataManagementAdvise `json:"advise" note:"建议"`
}
