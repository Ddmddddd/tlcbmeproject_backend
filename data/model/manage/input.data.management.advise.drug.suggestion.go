package manage

type InputDataManagementAdviseDrugSuggestion struct {
	Title *string `json:"title" note:"标题, 例如：用药指导"`
	Value *string `json:"value" note:"内容, 例如：ACEI 和 ARB / β 受体阻滞剂 / 钙通道阻滞剂 / 利尿剂"`
}
