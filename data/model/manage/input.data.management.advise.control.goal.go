package manage

type InputDataManagementAdviseControlGoal struct {
	Name     string `json:"name" note:"名称, 例如：SBP"`
	Operator string `json:"operator" note:"符号, 例如：>,<,>=,<="`
	Value    string `json:"value" note:"值, 例如：130"`
	Unit     string `json:"unit" note:"单位, 例如：mmHg"`
}
