package manage

import "github.com/ktpswjz/httpserver/types"

type InputDataFollowup struct {
	ScheduledDate *types.Time `json:"scheduledDate" note:"排期, 例如：2018-08-24 00:00:00"`
	Memo          string      `json:"memo" note:"状态说明, 例如："`
}
