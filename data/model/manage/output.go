package manage

type Output struct {
	Data *OutputData `json:"data" required:"true" note:"数据"`
}

type InputDataRiskAssessOutput struct {
	RiskAssess InputDataRiskAssess `json:"riskAssess" note:"危险评估结果"`
	Others     interface{}         `json:"others" note:"危险评估结果其他信息"`
}
