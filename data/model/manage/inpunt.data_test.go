package manage

import (
	"fmt"
	"testing"
)

func TestInputData_ContainFlag(t *testing.T) {
	testHexToDec()

	data := &InputData{
		Flag: FlagRiskAssess | FlagFollowup | FlagWarnings,
	}

	if !data.ContainFlag(FlagRiskAssess) {
		t.Fatal(FlagRiskAssess)
	}
	if data.ContainFlag(FlagManagement) {
		t.Fatal(FlagManagement)
	}
	if !data.ContainFlag(FlagFollowup) {
		t.Fatal(FlagFollowup)
	}
	if !data.ContainFlag(FlagWarnings) {
		t.Fatal(FlagWarnings)
	}
	if data.ContainFlag(FlagCompliance) {
		t.Fatal(FlagCompliance)
	}
}

func testHexToDec() {
	fmt.Println("0x01:", 0x01)
	fmt.Println("0x02:", 0x02)
	fmt.Println("0x04:", 0x04)
	fmt.Println("0x08:", 0x08)
	fmt.Println("0x10:", 0x10)

	fmt.Println("0x01 | 0x02:", 0x01|0x02)
	fmt.Println("0x01 | 0x04:", 0x01|0x04)
	fmt.Println("0x01 | 0x08:", 0x01|0x08)
	fmt.Println("0x01 | 0x10:", 0x01|0x10)

	fmt.Println("0x02 | 0x04:", 0x02|0x04)
	fmt.Println("0x02 | 0x08:", 0x02|0x08)
	fmt.Println("0x02 | 0x10:", 0x02|0x10)

	fmt.Println("0x04 | 0x08:", 0x04|0x08)
	fmt.Println("0x04 | 0x10:", 0x04|0x10)

	fmt.Println("0x10 | 0x10:", 0x10|0x10)
}
