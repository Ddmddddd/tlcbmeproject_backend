package manage

type Test struct {
	ProviderType int         `json:"providerType" required:"true" note:"提供者类型，1-高血压；2-糖尿病"`
	Uri          string      `json:"uri" note:"路径"`
	Argument     interface{} `json:"argument" note:"参数"`
}
