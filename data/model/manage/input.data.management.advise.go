package manage

import "github.com/ktpswjz/httpserver/types"

type InputDataManagementAdvise struct {
	Date  *types.Time `json:"date" note:"产生时间, 例如：2018-09-28 11:00:00"`
	Title string      `json:"title" note:"名称, 例如：一级管理计划"`
	Type  string      `json:"type" note:"类型, 例如：高血压、糖尿病、慢阻肺"`

	MeasuringFrequency []InputDataManagementAdviseMeasuringFrequency `json:"measureFrequencies" note:"测量频率"`
	DrugSuggestion     InputDataManagementAdviseDrugSuggestion       `json:"drugSuggestion" note:"用药建议"`
	ControlGoal        []InputDataManagementAdviseControlGoal        `json:"controlGoals" note:"控制目标"`
	LifeIntervention   InputDataManagementAdviseLifeIntervention     `json:"lifeIntervention" note:"生活干预"`

	Revisit bool `json:"revisit" note:"是否建议复诊, 例如：false"`
}
