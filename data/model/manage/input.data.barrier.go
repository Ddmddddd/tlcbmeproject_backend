package manage

type BarrierResult struct {
	AssessmentSheetVersion uint64 				`json:"assessmentSheetVersion" note:"量表编号"`
	Barriers 			   []InputDataBarrier	`json:"barriers" note:"健康饮食障碍列表"`
}

type InputDataBarrier struct {
	SeqBarrier  uint64  `json:"seqBarrier" note:"健康障碍序号"`
	Barrier 	string 	`json:"barrier" note:"健康障碍内容"`
	Level       uint64  `json:"level" note:"障碍程度"`
	Description string 	`json:"description" note:"健康障碍的具体描述"`
}
