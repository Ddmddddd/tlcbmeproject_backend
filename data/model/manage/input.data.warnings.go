package manage

import "github.com/ktpswjz/httpserver/types"

type InputDataWarnings struct {
	Code     string      `json:"code" required:"true" note:"预警代码, 例如：B04"`
	Type     string      `json:"type" required:"true" note:"预警类型, 例如：血压"`
	Name     string      `json:"name" required:"true" note:"预警名称, 例如：单次血压轻度异常"`
	Reason   string      `json:"reason" note:"预警原因, 例如：100/90mmHg"`
	Message  string      `json:"message" note:"预警消息, 例如：当前血压为轻度异常。降压治疗应缓慢进行，不能求之过急，血压达标通常需要 4-12周，请坚持测量血压哦。"`
	DateTime *types.Time `json:"dateTime" required:"true" note:"预警发生时间, 例如：2018-04-19 13:30:10"`
}
