package model

type Page struct {
	Index uint64 `json:"index" note:"当前页码，从1开始，默认1"`
	Size  uint64 `json:"size" note:"每页条数，默认15"`
}

type PageFilter struct {
	Page

	Filter interface{} `json:"filter" note:"过滤条件"`
}

type PageResult struct {
	Page

	Total  uint64      `json:"total" note:"总条数"`
	Count  uint64      `json:"count" note:"页数"`
	Data   interface{} `json:"data" note:"页面数据"`
	Extend interface{} `json:"extend,omitempty" note:"扩展信息"`
}
