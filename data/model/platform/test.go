package platform

type Test struct {
	Uri      string      `json:"uri" note:"路径"`
	Argument interface{} `json:"argument" note:"参数"`
}
