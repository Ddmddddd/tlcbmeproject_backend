package model

type Control struct {
	Timestamp string `json:"timestamp" note:"时间戳"`
	Nonce     string `json:"nonce" note:"随机字符串"`
	Sign      string `json:"sign" note:"校验值"`
}
