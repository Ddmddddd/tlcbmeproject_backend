package model

// https://tools.ietf.org/html/rfc7519#section-4.1
type JwtPayload struct {
	Issuer         string `json:"iss" note:"签发者(管理服务标识ID)"`
	Subject        string `json:"sub" note:"发送方(订阅者: 医生端机构代码)"`
	Audience       string `json:"aud" note:"接收方(管理服务标识ID)"`
	ExpirationTime int64  `json:"exp" note:"过期时间(UNIX)"`
	NotBefore      int64  `json:"nbf" note:"生效时间(UNIX)"`
	IssuedAt       int64  `json:"iat" note:"签发时间(UNIX)"`
	ID             string `json:"jti" note:"标识ID(UUID，仅使用一次)"`
}

type JWT struct {
	JWT       string      `json:"jwt" note:"JWT凭证"`
	Header    interface{} `json:"header" note:"头部"`
	Payload   *JwtPayload `json:"payload" note:"载荷"`
	Signature string      `json:"signature" note:"签名"`
}
