package jinscale

import (
	"time"
)

type BaseScale struct {
	Form     string `json:"form"`
	FormName string `json:"form_name"`
	Entry    *struct {
		SerialNumber float64  `json:"serial_number"`
		Field26      string   `json:"field_26"`
		Field35      string   `json:"field_35"`
		Field2       string   `json:"field_2"`
		Field3       string   `json:"field_3"`
		Field4       string   `json:"field_4"`
		Field33      []string `json:"field_33"`
		Field34      []string `json:"field_34"`
		Field28      []struct {
			Statement string   `json:"statement"`
			Choices   []string `json:"choices"`
		} `json:"field_28"`
		Field6       string    `json:"field_6"`
		Field7       string    `json:"field_7"`
		Field8       string    `json:"field_8"`
		Field9       string    `json:"field_9"`
		Field10      string    `json:"field_10"`
		Field11      string    `json:"field_11"`
		Field12      string    `json:"field_12"`
		Field13      string    `json:"field_13"`
		Field14      string    `json:"field_14"`
		Field15      string    `json:"field_15"`
		Field16      []string  `json:"field_16"`
		Field17      string    `json:"field_17"`
		Field18      string    `json:"field_18"`
		Field19      string    `json:"field_19"`
		Field20      string    `json:"field_20"`
		Field21      string    `json:"field_21"`
		Field22      string    `json:"field_22"`
		Field23      string    `json:"field_23"`
		CreatorName  string    `json:"creator_name"`
		CreatedAt    time.Time `json:"created_at"`
		UpdatedAt    time.Time `json:"updated_at"`
		InfoRemoteIP string    `json:"info_remote_ip"`
	} `json:"entry"`
}

type WeightScale struct {
	Form     string `json:"form"`
	FormName string `json:"form_name"`
	Entry    *struct {
		SerialNumber float64   `json:"serial_number"`
		Field1       string    `json:"field_1"`
		Field26      string    `json:"field_26"`
		Field2       string    `json:"field_2"`
		Field3       string    `json:"field_3"`
		Field4       string    `json:"field_4"`
		Field23      string    `json:"field_23"`
		Field24      string    `json:"field_24"`
		Field5       []string  `json:"field_5"`
		Field6       string    `json:"field_6"`
		Field7       []string  `json:"field_7"`
		Field8       string    `json:"field_8"`
		Field9       string    `json:"field_9"`
		Field10      string    `json:"field_10"`
		Field25      string    `json:"field_25"`
		Field11      []string  `json:"field_11"`
		Field12      string    `json:"field_12"`
		Field13      string    `json:"field_13"`
		Field14      string    `json:"field_14"`
		Field15      string    `json:"field_15"`
		Field16      string    `json:"field_16"`
		Field17      string    `json:"field_17"`
		Field18      string    `json:"field_18"`
		Field19      string    `json:"field_19"`
		Field20      string    `json:"field_20"`
		Field21      string    `json:"field_21"`
		CreatorName  string    `json:"creator_name"`
		CreatedAt    time.Time `json:"created_at"`
		UpdatedAt    time.Time `json:"updated_at"`
		InfoRemoteIP string    `json:"info_remote_ip"`
	} `json:"entry"`
}

type DietScale struct {
	Form     string `json:"form"`
	FormName string `json:"form_name"`
	Entry    *struct {
		SerialNumber float64   `json:"serial_number"`
		Field1       string    `json:"field_1"`
		Field17      string    `json:"field_17"`
		Field2       []string  `json:"field_2"`
		Field3       string    `json:"field_3"`
		Field4       []string  `json:"field_4"`
		Field5       string    `json:"field_5"`
		Field6       string    `json:"field_6"`
		Field7       string    `json:"field_7"`
		Field8       []string  `json:"field_8"`
		Field9       string    `json:"field_9"`
		Field10      string    `json:"field_10"`
		Field11      string    `json:"field_11"`
		Field12      []string  `json:"field_12"`
		Field13      string    `json:"field_13"`
		Field14      []string  `json:"field_14"`
		Field15      string    `json:"field_15"`
		CreatorName  string    `json:"creator_name"`
		CreatedAt    time.Time `json:"created_at"`
		UpdatedAt    time.Time `json:"updated_at"`
		InfoRemoteIP string    `json:"info_remote_ip"`
	} `json:"entry"`
}

type ExerciseScale struct {
	Form     string `json:"form"`
	FormName string `json:"form_name"`
	Entry    *struct {
		SerialNumber float64   `json:"serial_number"`
		Field11      string    `json:"field_11"`
		Field12      string    `json:"field_12"`
		Field1       string    `json:"field_1"`
		Field2       string    `json:"field_2"`
		Field3       string    `json:"field_3"`
		Field4       string    `json:"field_4"`
		Field5       []string  `json:"field_5"`
		Field6       []string  `json:"field_6"`
		Field7       string    `json:"field_7"`
		Field8       string    `json:"field_8"`
		Field9       []string  `json:"field_9"`
		Field10      string    `json:"field_10"`
		CreatorName  string    `json:"creator_name"`
		CreatedAt    time.Time `json:"created_at"`
		UpdatedAt    time.Time `json:"updated_at"`
		InfoRemoteIP string    `json:"info_remote_ip"`
	} `json:"entry"`
}

type FitnessScale struct {
	Form     string `json:"form"`
	FormName string `json:"form_name"`
	Entry    *struct {
		SerialNumber float64   `json:"serial_number"`
		Field24      string    `json:"field_24"`
		Field25      string    `json:"field_25"`
		Field2       string    `json:"field_2"`
		Field3       string    `json:"field_3"`
		Field4       string    `json:"field_4"`
		Field26      string    `json:"field_26"`
		Field5       string    `json:"field_5"`
		Field6       string    `json:"field_6"`
		Field7       string    `json:"field_7"`
		Field8       string    `json:"field_8"`
		Field9       string    `json:"field_9"`
		Field10      string    `json:"field_10"`
		Field11      string    `json:"field_11"`
		Field12      string    `json:"field_12"`
		Field13      string    `json:"field_13"`
		Field14      string    `json:"field_14"`
		Field15      string    `json:"field_15"`
		Field16      string    `json:"field_16"`
		Field17      string    `json:"field_17"`
		Field18      string    `json:"field_18"`
		Field19      string    `json:"field_19"`
		CreatorName  string    `json:"creator_name"`
		CreatedAt    time.Time `json:"created_at"`
		UpdatedAt    time.Time `json:"updated_at"`
		InfoRemoteIP string    `json:"info_remote_ip"`
	} `json:"entry"`
}

type PostureScale struct {
	Form     string `json:"form"`
	FormName string `json:"form_name"`
	Entry    *struct {
		SerialNumber float64   `json:"serial_number"`
		Field5       string    `json:"field_5"`
		Field6       string    `json:"field_6"`
		Field7       string    `json:"field_7"`
		Field1       []string  `json:"field_1"`
		Field2       []string  `json:"field_2"`
		Field3       []string  `json:"field_3"`
		Field4       []string  `json:"field_4"`
		CreatorName  string    `json:"creator_name"`
		CreatedAt    time.Time `json:"created_at"`
		UpdatedAt    time.Time `json:"updated_at"`
		InfoRemoteIP string    `json:"info_remote_ip"`
	} `json:"entry"`
}

type FitnessEndScale struct {
	Form     string `json:"form"`
	FormName string `json:"form_name"`
	Entry    *struct {
		SerialNumber float64   `json:"serial_number"`
		Field1       string    `json:"field_1"`
		Field2       string    `json:"field_2"`
		Field3       string    `json:"field_3"`
		Field4       string    `json:"field_4"`
		Field5       string    `json:"field_5"`
		Field6       string    `json:"field_6"`
		Field8       string    `json:"field_8"`
		Field9       string    `json:"field_9"`
		Field10      string    `json:"field_10"`
		Field11      string    `json:"field_11"`
		Field13      string    `json:"field_13"`
		Field14      string    `json:"field_14"`
		Field15      string    `json:"field_15"`
		Field16      string    `json:"field_16"`
		Field17      string    `json:"field_17"`
		Field19      string    `json:"field_19"`
		Field20      string    `json:"field_20"`
		Field21      string    `json:"field_21"`
		Field22      string    `json:"field_22"`
		Field23      string    `json:"field_23"`
		Field24      string    `json:"field_24"`
		CreatorName  string    `json:"creator_name"`
		CreatedAt    time.Time `json:"created_at"`
		UpdatedAt    time.Time `json:"updated_at"`
		InfoRemoteIP string    `json:"info_remote_ip"`
	} `json:"entry"`
}

type PostureEndScale struct {
	Form     string `json:"form"`
	FormName string `json:"form_name"`
	Entry    *struct {
		SerialNumber float64   `json:"serial_number"`
		Field1       string    `json:"field_1"`
		Field2       string    `json:"field_2"`
		Field3       string    `json:"field_3"`
		Field4       []string  `json:"field_4"`
		Field5       []string  `json:"field_5"`
		Field6       []string  `json:"field_6"`
		Field7       []string  `json:"field_7"`
		CreatorName  string    `json:"creator_name"`
		CreatedAt    time.Time `json:"created_at"`
		UpdatedAt    time.Time `json:"updated_at"`
		InfoRemoteIP string    `json:"info_remote_ip"`
	} `json:"entry"`
}

type EndScale struct {
	Form     string `json:"form"`
	FormName string `json:"form_name"`
	Entry    *struct {
		SerialNumber float64   `json:"serial_number"`
		Field2       string    `json:"field_2"`
		Field3       string    `json:"field_3"`
		Field4       string    `json:"field_4"`
		Field5       string    `json:"field_5"`
		Field7       int       `json:"field_7"`
		Field8       int       `json:"field_8"`
		Field9       int       `json:"field_9"`
		Field10      int       `json:"field_10"`
		Field11      int       `json:"field_11"`
		Field12      int       `json:"field_12"`
		Field13      string    `json:"field_13"`
		Field14      string    `json:"field_14"`
		Field15      string    `json:"field_15"`
		Field16      string    `json:"field_16"`
		Field17      string    `json:"field_17"`
		Field18      string    `json:"field_18"`
		Field19      string    `json:"field_19"`
		Field20      int       `json:"field_20"`
		Field21      []string  `json:"field_21"`
		Field22      int       `json:"field_22"`
		Field23      []string  `json:"field_23"`
		Field24      int       `json:"field_24"`
		Field26      int       `json:"field_26"`
		Field27      []string  `json:"field_27"`
		Field28      string    `json:"field_28"`
		Field29      string    `json:"field_29"`
		Field30      string    `json:"field_30"`
		Field31      string    `json:"field_31"`
		CreatorName  string    `json:"creator_name"`
		CreatedAt    time.Time `json:"created_at"`
		UpdatedAt    time.Time `json:"updated_at"`
		InfoRemoteIP string    `json:"info_remote_ip"`
	} `json:"entry"`
}

type NewEndScale struct {
	Form     string `json:"form"`
	FormName string `json:"form_name"`
	Entry    *struct {
		SerialNumber float64   `json:"serial_number"`
		Field2       string    `json:"field_2"`
		Field3       string    `json:"field_3"`
		Field4       string    `json:"field_4"`
		Field5       string    `json:"field_5"`
		Field7       int       `json:"field_7"`
		Field8       int       `json:"field_8"`
		Field9       int       `json:"field_9"`
		Field10      int       `json:"field_10"`
		Field11      int       `json:"field_11"`
		Field12      int       `json:"field_12"`
		Field13      string    `json:"field_13"`
		Field14      string    `json:"field_14"`
		Field15      string    `json:"field_15"`
		Field16      string    `json:"field_16"`
		Field17      string    `json:"field_17"`
		Field19      string    `json:"field_19"`
		Field20      int       `json:"field_20"`
		Field21      []string  `json:"field_21"`
		Field22      int       `json:"field_22"`
		Field23      []string  `json:"field_23"`
		Field24      int       `json:"field_24"`
		Field26      int       `json:"field_26"`
		Field27      []string  `json:"field_27"`
		Field28      string    `json:"field_28"`
		Field29      string    `json:"field_29"`
		Field30      string    `json:"field_30"`
		Field31      string    `json:"field_31"`
		CreatorName  string    `json:"creator_name"`
		CreatedAt    time.Time `json:"created_at"`
		UpdatedAt    time.Time `json:"updated_at"`
		InfoRemoteIP string    `json:"info_remote_ip"`
	} `json:"entry"`
}

type BaseHealthScale struct {
	Form     string `json:"form"`
	FormName string `json:"form_name"`
	Entry    *struct {
		SerialNumber float64   `json:"serial_number"`
		Field3       string    `json:"field_3"`
		Field7       string    `json:"field_7"`
		Field147     int       `json:"field_177"`
		Field154     int       `json:"field_154"`
		Field176     string    `json:"field_176"`
		Field158     int       `json:"field_158"`
		Field168     string    `json:"field_168"`
		Field157     int       `json:"field_157"`
		Field162     string    `json:"field_162"`
		Field163     string    `json:"field_163"`
		Field170     string    `json:"field_170"`
		Field83      string    `json:"field_83"`
		Field142     string    `json:"field_142"`
		Field143     string    `json:"field_143"`
		Field166     []string  `json:"field_166"`
		Field25      []string  `json:"field_25"`
		Field35      string    `json:"field_35"`
		Field148     string    `json:"field_148"`
		Field39      []Field39 `json:"field_39"`
		Field149     string    `json:"field_149"`
		Field150     string    `json:"field_150"`
		Field146     string    `json:"field_146"`
		Field50      string    `json:"field_50"`
		Field113     string    `json:"field_113"`
		Field129     []string  `json:"field_129"`
		Field151     []string  `json:"field_151"`
		Field167     []string  `json:"field_167"`
		Field153     []string  `json:"field_153"`
		Field54      string    `json:"field_54"`
		Field159     string    `json:"field_159"`
		Field160     string    `json:"field_160"`
		Field114     string    `json:"field_114"`
		Field161     string    `json:"field_161"`
		Field55      []string  `json:"field_55"`
		Field56      int       `json:"field_56"`
		Field57      []string  `json:"field_57"`
		Field58      string    `json:"field_58"`
		Field109     string    `json:"field_109"`
		Field63      string    `json:"field_63"`
		Field104     []string  `json:"field_104"`
		Field119     string    `json:"field_119"`
		Field65      []string  `json:"field_65"`
		Field67      string    `json:"field_67"`
		Field68      string    `json:"field_68"`
		Field105     []string  `json:"field_105"`
		Field70      int       `json:"field_70"`
		Field71      string    `json:"field_71"`
		Field72      []string  `json:"field_72"`
		Field106     string    `json:"field_106"`
		Field145     string    `json:"field_145"`
		Field73      string    `json:"field_73"`
		Field75      string    `json:"field_75"`
		Field76      string    `json:"field_76"`
		Field77      string    `json:"field_77"`
		Field79      string    `json:"field_79"`
		Field80      string    `json:"field_80"`
		Field81      string    `json:"field_81"`
		Field136     []string  `json:"field_136"`
		Field122     string    `json:"field_122"`
		Field124     string    `json:"field_124"`
		Field125     string    `json:"field_125"`
		Field126     string    `json:"field_126"`
		Field165     string    `json:"field_165"`
		CreatorName  string    `json:"creator_name"`
		CreatedAt    time.Time `json:"created_at"`
		UpdatedAt    time.Time `json:"updated_at"`
		InfoRemoteIP string    `json:"info_remote_ip"`
	} `json:"entry"`
}
type Field39 struct {
	Statement string   `json:"statement"`
	Choices   []string `json:"choices"`
}

type SportsTeamScale struct {
	Form     string `json:"form"`
	FormName string `json:"form_name"`
	Entry    *struct {
		SerialNumber float64   `json:"serial_number"`
		Field1       string    `json:"field_1"`
		Field2       string    `json:"field_2"`
		Field3       string    `json:"field_3"`
		Field4       string    `json:"field_4"`
		Field5       string    `json:"field_5"`
		Field6       string    `json:"field_6"`
		Field7       string    `json:"field_7"`
		Field8       string    `json:"field_8"`
		Field9       string    `json:"field_9"`
		Field10      string    `json:"field_10"`
		Field11      string    `json:"field_11"`
		Field12      string    `json:"field_12"`
		Field13      string    `json:"field_13"`
		Field14      string    `json:"field_14"`
		Field15      string    `json:"field_15"`
		Field16      string    `json:"field_16"`
		Field17      string    `json:"field_17"`
		Field18      string    `json:"field_18"`
		Field19      string    `json:"field_19"`
		Field20      string    `json:"field_20"`
		Field21      []string  `json:"field_21"`
		Field22      string    `json:"field_22"`
		Field23      string    `json:"field_23"`
		Field24      string    `json:"field_24"`
		Field25      string    `json:"field_25"`
		Field26      string    `json:"field_26"`
		Field27      string    `json:"field_27"`
		Field28      string    `json:"field_28"`
		Field29      string    `json:"field_29"`
		Field30      string    `json:"field_30"`
		Field31      string    `json:"field_31"`
		Field32      []string  `json:"field_32"`
		Field33      []string  `json:"field_33"`
		Field34      []string  `json:"field_34"`
		Field35      string    `json:"field_35"`
		Field36      string    `json:"field_36"`
		Field37      string    `json:"field_37"`
		Field38      string    `json:"field_38"`
		Field39      string    `json:"field_39"`
		Field40      []string  `json:"field_40"`
		Field41      string    `json:"field_41"`
		Field42      string    `json:"field_42"`
		Field43      string    `json:"field_43"`
		Field44      string    `json:"field_44"`
		Field45      string    `json:"field_45"`
		Field46      string    `json:"field_46"`
		Field47      string    `json:"field_47"`
		Field48      string    `json:"field_48"`
		Field49      string    `json:"field_49"`
		Field50      []string  `json:"field_50"`
		Field51      string    `json:"field_51"`
		Field52      string    `json:"field_52"`
		Field53      string    `json:"field_53"`
		Field54      string    `json:"field_54"`
		Field55      string    `json:"field_55"`
		Field56      string    `json:"field_56"`
		CreatorName  string    `json:"creator_name"`
		CreatedAt    time.Time `json:"created_at"`
		UpdatedAt    time.Time `json:"updated_at"`
		InfoRemoteIP string    `json:"info_remote_ip"`
	} `json:"entry"`
}

type TestWangScale struct {
	AppKey    string `json:"app_key"`
	Timestamp string `json:"timestamp"`
	Signature string `json:"signature"`
	Params    *struct {
		EventType string `json:"event_type"`
		EventTime string `json:"event_time"`
		EventData *struct {
			ShortId          string `json:"short_id"`
			Seq              int    `json:"seq"`
			Source           string `json:"source"`
			RespondentStatus int    `json:"respondent_status"`
		} `json:"event_data"
		`
	} `json:"params"`
}

type TestResultWangScale struct {
	//Result *struct{
	ErrCode int    `json:"err_code"`
	ErrMsg  string `json:"err_msg"`
	Data    *struct {
		Field1 *struct {
			Answer   string `json:"answer"`
			TypeDesc string `json:"type_desc"`
			Title    string `json:"title"`
		} `json:"Q1"`
		Seq              int    `json:"seq"`
		Source           string `json:"source"`
		RespondentStatus int    `json:"respondent_status"`
		TimeUsed         string `json:"time_used"`
		Ip               string `json:"ip"`
		Finish           string `json:"finish"`
		Start            string `json:"start"`
		WxAddr           string `json:"weixin_addr"`
		WxSex            string `json:"weixin_sex"`
		WxNickname       string `json:"weixin_nickname"`
	} `json:"data"`
	//} `json:"result"`
}
