package nutrient

type Packagedfood struct {
	Serial_No       uint64   `json:"serial_No" note:"序号"`
	Quality         *uint64  `json:"quality" note:"1-固体，2-液体"`
	Resource        string   `json:"resource" note:"来源"`
	Code            string   `json:"code" note:"条形码"`
	Name            string   `json:"name" note:"名称"`
	Brand           string   `json:"brand" note:"品牌"`
	Manufaturer     string   `json:"manufaturer" note:"制造商"`
	FOOD_TYPE_ID    string   `json:"fOOD_TYPE_ID" note:"类型ID，对应类型表"`
	Flavor          string   `json:"flavor" note:"口味"`
	PackingSizeNum  *float64 `json:"packingSizeNum" note:"包装单位的克数/毫升数"`
	PackingSizeUnit string   `json:"packingSizeUnit" note:"包装单位"`
	Portion         string   `json:"portion" note:"营养素对应份量"`
	ENERGY_KJ       *float64 `json:"eNERGY_KJ" note:"热量，千焦单位"`
	ENERGY_KC       *float64 `json:"eNERGY_KC" note:"热量，千卡单位"`
	FETT            *float64 `json:"fETT" note:"脂肪"`
	PROTEIN         *float64 `json:"pROTEIN" note:"蛋白质"`
	CARBOHYDRATE    *float64 `json:"cARBOHYDRATE" note:"碳水化合物"`
	SUGAR           *float64 `json:"sUGAR" note:"糖类"`
	DF              *float64 `json:"dF" note:"纤维素"`
	NA              *float64 `json:"nA" note:"钠"`
}
