package nutrient

type NewRecipeComposeFromApp struct {
	FoodName   string  `json:"foodName" note:"组成食物名称 菜肴的一种组成食物"`
	FoodID     string  `json:"foodID" note:"食物成分序号 "`
	WEIGHT     float64 `json:"weight" note:"实际每份净重"`
}
