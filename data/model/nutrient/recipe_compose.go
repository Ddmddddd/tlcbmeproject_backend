package nutrient

type RecipeCompose struct {
	SerialNo  uint64  `json:"serial_No" note:"序号 主键"`
	RecipeID  string  `json:"rECIPE_ID" note:"菜肴编号 recipe.ID"`
	FoodName  string  `json:"fOOD_NAME" note:"组成食物名称 菜肴的一种组成食物"`
	FoodID    string  `json:"fOOD_ID" note:"食物成分序号 "`
	WEIGHT    float64 `json:"wEIGHT" note:"实际每份净重"`
	NetWeight string  `json:"nET_WEIGHT" note:"净重"`
}
