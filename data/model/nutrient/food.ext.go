package nutrient

type Foods struct {
	ENERGY_KC    float64 `json:"eNERGY_KC" note:"能量(千卡)"`
	CARBOHYDRATE float64 `json:"cARBOHYDRATE" note:"碳水化合物 g"`
	// 限制性营养素
	FETT float64 `json:"fETT" note:"脂肪 g"`
	NA   float64 `json:"nA" note:"钠 g"`
	// 推荐营养素
	PROTEIN float64 `json:"pROTEIN" note:"蛋白质 g"`
	DF      float64 `json:"dF" note:"膳食纤维 g"`
	CA      float64 `json:"cA" note:"钙 g"`
	POT     float64 `json:"pOT" note:"钾 g"`
	MG      float64 `json:"mG" note:"镁 g"`
	FE      float64 `json:"fE" note:"铁 g"`
	VITA    float64 `json:"vITA" note:"维生素A g"`
	VITB1   float64 `json:"vITB1" note:"维生素B1 g"`
	VITB2   float64 `json:"vITB2" note:"维生素B2 g"`
	VITC    float64 `json:"vITC" note:"维生素C g"`
	VITD    float64 `json:"vITD" note:"维生素D g"`
	ZN      float64 `json:"zN" note:"锌"`
	// 其他营养素
	CHO    float64 `json:"cHO" note:"胆固醇 g"`
	USP    float64 `json:"uSP" note:"叶酸 g"`
	IODINE float64 `json:"iODINE" note:"碘 g"`
	SE     float64 `json:"sE" note:"硒 g"`
	VITB6  float64 `json:"vITB6" note:"维生素B6 g"`
	VITE   float64 `json:"vITE" note:"维生素E g"`
	Grade  float64 `json:"grade"`
}

type FoodSearchResult struct {
	Serial_No    uint64 `json:"serial_No" note:""`
	NAME         string `json:"name" note:"食物名称"`
	FOOD_TYPE_ID string `json:"food_type_id" note:"类别"`
	Source       string `json:"source" note:"食物来源表"`
	ID           string   `json:"id" note:"食物ID"`
}
