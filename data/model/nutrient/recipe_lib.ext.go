package nutrient

type NewRecipeFromApp struct {
	Name        string                     `json:"name" note:"名称 例如：莲藕炒肉"`
	List        []*NewRecipeComposeFromApp `json:"list"`
	Source      string                     `json:"source" note:"临时 菜肴来源的ID"`
	BelongDepID string                     `json:"dep_id" note:"来自recipe_dep表"`
}


