package nutrient

type Food_measure_unit struct {
	Serial_No      uint64   `json:"serial_No" note:"唯一序号，主键"`
	Food_Serial_No uint64   `json:"food_Serial_No" note:"食品唯一序号，外键，关联food表"`
	Number_Part    float64  `json:"number_Part" note:"数值部分，对应“1只(大)鸡蛋(70.0克，可食部分62.0克)”里面的“1”"`
	Unit_Part      string   `json:"unit_Part" note:"单位部分，对应“1只(大)鸡蛋(70.0克，可食部分62.0克)”里面的“只”"`
	Unit_Name_Part string   `json:"unit_Name_Part" note:"单位名称部分，比如单位“g”，对应的名称为“克”"`
	Level_Part     string   `json:"level_Part" note:"程度部分，对应“1只(大)鸡蛋(70.0克，可食部分62.0克)”里面的“大”"`
	Allup_Part     *float64 `json:"allup_Part" note:"总重部分，对应“1只(大)鸡蛋(70.0克，可食部分62.0克)”里面的“70.0”"`
	Edible_Part    float64  `json:"edible_Part" note:"可食部分，对应“1只(大)鸡蛋(70.0克，可食部分62.0克)”里面的“62.0”"`
	Flag           uint64   `json:"flag" note:"0 - 非标准度量单位，1 - 标准度量单位"`
}
