package nutrient

type Recipe_lib struct {
	Serial_No          uint64   `json:"serial_No" note:"序号 主键"`
	ID                 string   `json:"iD" note:"唯一标识 编号"`
	NAME               string   `json:"nAME" note:"名称 例如：莲藕炒肉"`
	SPELL              string   `json:"sPELL" note:"例如：locr"`
	WEIGHT_SOUP        *float64 `json:"wEIGHT_SOUP" note:"实际每份熟重（含汤） 单位 克"`
	WEIGHT_NO_SOUP     *float64 `json:"wEIGHT_NO_SOUP" note:"实际每份熟重（去汤）  单位 克"`
	ALL_WEIGHT_SOUP    *float64 `json:"aLL_WEIGHT_SOUP" note:"总熟重（含汤）"`
	ALL_WEIGHT_NO_SOUP *float64 `json:"aLL_WEIGHT_NO_SOUP" note:"总熟重（不含汤）"`
	COOKED_WEIGHT      *float64 `json:"cOOKED_WEIGHT" note:"每份生重 单位 克"`
	RAW_COOKED         *float64 `json:"rAW_COOKED" note:"生熟比  100代表全生，0代表全熟，单位 %"`
	FREQUENCY_FLAG     *uint64  `json:"fREQUENCY_FLAG" note:"常见程度 越大越常见"`
	PHOTO              string `json:"pHOTO" note:"照片"`
	ALIAS              string   `json:"aLIAS" note:"别名"`
	COST               *float64 `json:"cOST" note:"成本 单位 元"`
	PRICE              *float64 `json:"pRICE" note:"价格 单位 元"`
	BELONG_DEP_ID      string   `json:"bELONG_DEP_ID" note:"来自recipe_dep表"`
	DESCS              string   `json:"dESCS" note:"说明"`
	CUISINE_FLAG       string   `json:"cUISINE_FLAG" note:"菜系标签  鲁菜、川菜…,来自recipe_flag_dict表"`
	FOOD_FLAG          string   `json:"fOOD_FLAG" note:"食材标签 新鲜、冷冻、冷藏、腌制…"`
	FOOD_TYPE          string   `json:"fOOD_TYPE" note:"性状标签"`
	COOK_METHOD        string   `json:"cOOK_METHOD" note:"烹饪方法标签 炒、爆、熘、炸、烹、煎…"`
	MEAL_FLAG          string   `json:"mEAL_FLAG" note:"餐次类别标签 早餐…"`
	DISH_FLAG          string   `json:"dISH_FLAG" note:"菜肴类别标签  主食…"`
	SEASON_FLAG        string   `json:"sEASON_FLAG" note:"季节标签 春，夏…"`
	SUITABLE_RICE_FLAG string   `json:"sUITABLE_RICE_FLAG" note:"适宜饭种标签  普通饭、软饭、半流质…"`
	TEXTURE_FLAG       string   `json:"tEXTURE_FLAG" note:"口感标签 脆、绵、软…"`
	FLAVOR_FLAG        string   `json:"fLAVOR_FLAG" note:"口味标签 麻、辣、酸、甜…"`
	DISIEASE_FLAG      string   `json:"dISIEASE_FLAG" note:"疾病标签 高血糖、高血压…"`
	NUTRITION_FLAG     string   `json:"nUTRITION_FLAG" note:"营养标签 高热量、低热量…"`
	HEALTH_SCENE_FLAG  string   `json:"hEALTH_SCENE_FLAG" note:"健康标签"`
	SORT_PERSON_FLAG   string   `json:"sORT_PERSON_FLAG" note:"适用年龄标签"`
	PHI_FLAG           string   `json:"pHI_FLAG" note:"红绿灯标识（PHI指数） 标签  红、黄…"`
	COLOUR_FLAG        string   `json:"cOLOUR_FLAG" note:"颜色标签 白，红，橙，黄…"`
	DISH_PRECTICE      string   `json:"dISH_PRECTICE" note:"菜肴做法"`
	USAGE_RATE_FLAG    string   `json:"uSAGE_RATE_FLAG" note:"价格 使用频率标签 常用、一般、较少使用…"`
	SOURCE             string   `json:"sOURCE" note:"临时 菜肴来源的ID"`
}
