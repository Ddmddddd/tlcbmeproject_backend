package nutrient

type Viewfood struct {
	Serial_No      uint64   `json:"serial_No" note:""`
	Version        uint64   `json:"version" note:""`
	Name           string   `json:"name" note:""`
	ENERGY_KC      *float64 `json:"eNERGY_KC" note:""`
	PROTEIN        *float64 `json:"pROTEIN" note:""`
	FETT           *float64 `json:"fETT" note:""`
	CARBOHYDRATE   *float64 `json:"cARBOHYDRATE" note:""`
	NA             *float64 `json:"nA" note:""`
	DF             *float64 `json:"dF" note:""`
	ZN             *float64 `json:"zN" note:""`
	MG             *float64 `json:"mG" note:""`
	FE             *float64 `json:"fE" note:""`
	CA             *float64 `json:"cA" note:""`
	POT            *float64 `json:"pOT" note:""`
	VITA           *float64 `json:"vITA" note:""`
	VITB1          *float64 `json:"vITB1" note:""`
	VITB2          *float64 `json:"vITB2" note:""`
	VITC           *float64 `json:"vITC" note:""`
	VITD           *float64 `json:"vITD" note:""`
	CHO            *float64 `json:"cHO" note:""`
	USP            *float64 `json:"uSP" note:""`
	IODINE         *float64 `json:"iODINE" note:""`
	SE             *float64 `json:"sE" note:""`
	VITB6          *float64 `json:"vITB6" note:""`
	VITE           *float64 `json:"vITE" note:""`
	Frequency_Flag uint64   `json:"frequency_Flag" note:""`
	Alias          string   `json:"alias" note:""`
	Source         string   `json:"source" note:""`
	FOOD_TYPE_ID   string   `json:"food_type_id"`
}
