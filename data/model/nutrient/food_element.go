package nutrient

type Food_element struct {
	Serial_No uint64 `json:"serial_No" note:""`
	ID        string `json:"iD" note:""`
	NAME      string `json:"nAME" note:"元素名称"`
	FIELD     string `json:"fIELD" note:"元素字段"`
	UNIT      string `json:"uNIT" note:"单位英文"`
	UNIT_NAME string `json:"uNIT_NAME" note:"单位中文"`
	PARENT_ID string `json:"pARENT_ID" note:""`
}
