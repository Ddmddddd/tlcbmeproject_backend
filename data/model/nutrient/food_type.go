package nutrient

type Food_type struct {
	Serial_No uint64  `json:"serial_No" note:""`
	Version   uint64  `json:"version" note:"版本"`
	ID        string  `json:"iD" note:""`
	NAME      string  `json:"nAME" note:""`
	PARENT_ID string  `json:"pARENT_ID" note:""`
	SORT      *uint64 `json:"sORT" note:""`
	DESCS     string  `json:"dESCS" note:""`
}
