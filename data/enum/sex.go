package enum

var Sexes = newSex()

type Sex interface {
	Number

	Unknown() NumberEnum
	Male() NumberEnum
	Female() NumberEnum
	Unexplained() NumberEnum
}

func newSex() Sex {
	instance := &sex{}
	instance.init()

	return instance
}

type sex struct {
	numberEnum

	unknown     NumberEnum
	male        NumberEnum
	female      NumberEnum
	unexplained NumberEnum
}

func (s *sex) init() {
	s.numberEnum.init()

	s.male = s.newItem(1, "男")
	s.female = s.newItem(2, "女")
	s.unknown = s.newItem(0, "未知")
	s.unexplained = s.newItem(9, "未说明")
}

func (s *sex) Unknown() NumberEnum {
	return s.unknown
}
func (s *sex) Male() NumberEnum {
	return s.male
}
func (s *sex) Female() NumberEnum {
	return s.female
}
func (s *sex) Unexplained() NumberEnum {
	return s.unexplained
}
