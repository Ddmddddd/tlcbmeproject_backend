package enum


var BindWeChatStatuses = newBindWeChatStatus()

type BindWeChatStatus interface {
	Number

	Normal() NumberEnum
	Stop() NumberEnum
}

func newBindWeChatStatus() BindWeChatStatus {
	instance := &bindWeChatStatus{}
	instance.init()

	return instance
}

type bindWeChatStatus struct {
	numberEnum

	normal NumberEnum
	stop   NumberEnum
}

func (s *bindWeChatStatus) init() {
	s.numberEnum.init()

	s.normal = s.newItem(0, "正常")
	s.stop = s.newItem(1, "禁用")
}

func (s *bindWeChatStatus) Normal() NumberEnum {
	return s.normal
}

func (s *bindWeChatStatus) Stop() NumberEnum {
	return s.stop
}
