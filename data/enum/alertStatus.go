package enum

var AlertStatuses = newAlertStatus()

type AlertStatus interface {
	Number

	Unprocessed() NumberEnum
	Processed() NumberEnum
}

func newAlertStatus() AlertStatus {
	instance := &alertStatus{}
	instance.init()

	return instance
}

type alertStatus struct {
	numberEnum

	unprocessed NumberEnum
	processed   NumberEnum
}

func (s *alertStatus) init() {
	s.numberEnum.init()

	s.unprocessed = s.newItem(0, "未处理")
	s.processed = s.newItem(1, "已处理")
}

func (s *alertStatus) Unprocessed() NumberEnum {
	return s.unprocessed
}

func (s *alertStatus) Processed() NumberEnum {
	return s.processed
}
