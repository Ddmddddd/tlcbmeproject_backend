package enum

var TaskNames = newTaskName()

type TaskName interface {
	Number

	Read() NumberEnum
	Answer() NumberEnum
	Photo() NumberEnum
	Timing() NumberEnum
	Game() NumberEnum
	Scale() NumberEnum
}

func newTaskName() TaskName {
	instance := &taskName{}
	instance.init()

	return instance
}

type taskName struct {
	numberEnum

	read        	NumberEnum
	answer          NumberEnum
	photo 			NumberEnum
	timing 			NumberEnum
	game			NumberEnum
	scale			NumberEnum
}

func (s *taskName) init() {
	s.numberEnum.init()

	s.read = s.newItem(0, "阅读")
	s.answer = s.newItem(1, "答题")
	s.photo = s.newItem(2, "拍照")
	s.timing = s.newItem(3, "计时")
	s.game = s.newItem(4, "小游戏")
	s.scale = s.newItem(5, "问卷")
}

func (s *taskName) Read() NumberEnum {
	return s.read
}
func (s *taskName) Answer() NumberEnum {
	return s.answer
}
func (s *taskName) Photo() NumberEnum {
	return s.photo
}
func (s *taskName) Timing() NumberEnum {
	return s.timing
}
func (s *taskName) Game() NumberEnum {
	return s.game
}
func (s *taskName) Scale() NumberEnum {
	return s.scale
}