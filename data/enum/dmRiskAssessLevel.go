package enum

var DmRiskAssessLevels = newDmRiskAssessLevel()

type DmRiskAssessLevel interface {
	Number

	Low() NumberEnum
	Middle() NumberEnum
	High() NumberEnum
}

type dmRiskAssessLevel struct {
	numberEnum

	low    NumberEnum
	middle NumberEnum
	high   NumberEnum
}

func newDmRiskAssessLevel() DmRiskAssessLevel {
	instance := &dmRiskAssessLevel{}
	instance.init()

	return instance
}

func (s *dmRiskAssessLevel) init() {
	s.numberEnum.init()
	s.low = s.newItem(1, "宽松")
	s.middle = s.newItem(2, "一般")
	s.high = s.newItem(3, "严格")
}

func (s *dmRiskAssessLevel) Low() NumberEnum {
	return s.low
}

func (s *dmRiskAssessLevel) Middle() NumberEnum {
	return s.middle
}

func (s *dmRiskAssessLevel) High() NumberEnum {
	return s.high
}
