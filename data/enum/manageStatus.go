package enum

var ManageStatuses = newManageStatus()

type ManageStatus interface {
	Number

	InManagement() NumberEnum
	MoveOut() NumberEnum
	TerminatedManagement() NumberEnum
}

func newManageStatus() ManageStatus {
	instance := &manageStatus{}
	instance.init()

	return instance
}

type manageStatus struct {
	numberEnum

	inManagement         NumberEnum
	moveOut              NumberEnum
	terminatedManagement NumberEnum
}

func (s *manageStatus) init() {
	s.numberEnum.init()

	s.inManagement = s.newItem(0, "管理中")
	s.moveOut = s.newItem(1, "迁出")
	s.terminatedManagement = s.newItem(2, "已终止管理")
}

func (s *manageStatus) InManagement() NumberEnum {
	return s.inManagement
}
func (s *manageStatus) MoveOut() NumberEnum {
	return s.moveOut
}
func (s *manageStatus) TerminatedManagement() NumberEnum {
	return s.terminatedManagement
}
