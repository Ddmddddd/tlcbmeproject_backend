package enum


var AppEnvs = newAppEnv()

type AppEnv interface {
	Number

	Test() NumberEnum
	Zj() NumberEnum
	Nx() NumberEnum
	WeightTest() NumberEnum
	Weight() NumberEnum
}

func newAppEnv() AppEnv {
	instance := &appEnv{}
	instance.init()

	return instance
}

type appEnv struct {
	numberEnum

	test NumberEnum
	zj   NumberEnum
	nx	 NumberEnum
	weightTest NumberEnum
	weight NumberEnum
}

func (s *appEnv) init() {
	s.numberEnum.init()

	s.test = s.newItem(0, "测试服")
	s.zj = s.newItem(1, "浙江")
	s.nx = s.newItem(2, "宁夏")
	s.weightTest = s.newItem(3, "体重测试")
	s.weight = s.newItem(4, "体重")
}

func (s *appEnv) Test() NumberEnum {
	return s.test
}

func (s *appEnv) Zj() NumberEnum {
	return s.zj
}

func (s *appEnv) Nx() NumberEnum {
	return s.nx
}

func (s *appEnv) WeightTest() NumberEnum {
	return s.weightTest
}

func (s *appEnv) Weight() NumberEnum {
	return s.weight
}