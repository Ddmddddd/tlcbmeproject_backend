package enum

var RiskAssessLevels = newRiskAssessLevel()

type RiskAssessLevel interface {
	Number

	Low() NumberEnum
	Middle() NumberEnum
	High() NumberEnum
}

type riskAssessLevel struct {
	numberEnum

	low    NumberEnum
	middle NumberEnum
	high   NumberEnum
}

func newRiskAssessLevel() RiskAssessLevel {
	instance := &riskAssessLevel{}
	instance.init()

	return instance
}

func (s *riskAssessLevel) init() {
	s.numberEnum.init()
	s.low = s.newItem(1, "低危")
	s.middle = s.newItem(2, "中危")
	s.high = s.newItem(3, "高危")
}

func (s *riskAssessLevel) Low() NumberEnum {
	return s.low
}

func (s *riskAssessLevel) Middle() NumberEnum {
	return s.middle
}

func (s *riskAssessLevel) High() NumberEnum {
	return s.high
}
