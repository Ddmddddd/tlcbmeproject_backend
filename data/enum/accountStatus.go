package enum

var AccountStatuses = newAccountStatus()

type AccountStatus interface {
	Number

	Normal() NumberEnum
	Frozen() NumberEnum
	Canceled() NumberEnum
}

func newAccountStatus() AccountStatus {
	instance := &accountStatus{}
	instance.init()

	return instance
}

type accountStatus struct {
	numberEnum

	normal   NumberEnum
	frozen   NumberEnum
	canceled NumberEnum
}

func (s *accountStatus) init() {
	s.numberEnum.init()

	s.normal = s.newItem(0, "正常")
	s.frozen = s.newItem(1, "冻结")
	s.canceled = s.newItem(9, "已注销")
}

func (s *accountStatus) Normal() NumberEnum {
	return s.normal
}
func (s *accountStatus) Frozen() NumberEnum {
	return s.frozen
}
func (s *accountStatus) Canceled() NumberEnum {
	return s.canceled
}
