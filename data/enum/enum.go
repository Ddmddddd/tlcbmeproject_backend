package enum

type NumberEnum struct {
	Key   uint64 `json:"key" note:"健值"`
	Value string `json:"value" note:"内容"`
}

type NumberEnums []NumberEnum

func (s NumberEnums) Value(key uint64) string {
	count := len(s)
	for i := 0; i < count; i++ {
		if key == s[i].Key {
			return s[i].Value
		}
	}

	return ""
}

type Number interface {
	All() NumberEnums
	Value(key uint64) string
}

type numberEnum struct {
	items NumberEnums
}

func (s *numberEnum) init() {
	s.items = make(NumberEnums, 0)
}

func (s *numberEnum) newItem(key uint64, value string) NumberEnum {
	item := NumberEnum{
		Key:   key,
		Value: value,
	}
	s.items = append(s.items, item)

	return item
}

func (s *numberEnum) All() NumberEnums {
	return s.items
}

func (s *numberEnum) Value(key uint64) string {
	return s.items.Value(key)
}

type StringEnum struct {
	Key   string `json:"key" note:"健值"`
	Value string `json:"value" note:"内容"`
}

type StringEnums []StringEnum

func (s StringEnums) Value(key string) string {
	count := len(s)
	for i := 0; i < count; i++ {
		if key == s[i].Key {
			return s[i].Value
		}
	}

	return ""
}
