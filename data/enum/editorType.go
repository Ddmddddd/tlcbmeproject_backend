package enum

var EditorTypes = newEditorType()

type EditorType interface {
	Number

	Eng() NumberEnum
	Doctor() NumberEnum
	Patient() NumberEnum
}

func newEditorType() EditorType {
	instance := &editorType{}
	instance.init()

	return instance
}

type editorType struct {
	numberEnum

	eng 	NumberEnum
	doctor  NumberEnum
	patient NumberEnum
}

func (s *editorType) init() {
	s.numberEnum.init()

	s.eng = s.newItem(0, "引擎")
	s.doctor = s.newItem(1, "医生")
	s.eng = s.newItem(2, "患者本人")
}

func (s *editorType) Eng() NumberEnum {
	return s.eng
}

func (s *editorType) Doctor() NumberEnum {
	return s.doctor
}

func (s *editorType) Patient() NumberEnum {
	return s.patient
}