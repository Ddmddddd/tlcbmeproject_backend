package enum

var ScaleTypes = newScaleType()

type ScaleType interface {
	Number

	Default() NumberEnum
	Normal() NumberEnum
	Action() NumberEnum
}

func newScaleType() ScaleType {
	instance := &scaleType{}
	instance.init()

	return instance
}

type scaleType struct {
	numberEnum

	_default         NumberEnum
	normal           NumberEnum
	action  		 NumberEnum
}

func (s *scaleType) init() {
	s.numberEnum.init()

	s._default = s.newItem(0, "默认量表，只对患者开放")
	s.normal = s.newItem(1, "普通量表")
	s.action = s.newItem(2, "需要行动计划的量表")
}

func (s *scaleType) Default() NumberEnum {
	return s._default
}
func (s *scaleType) Normal() NumberEnum {
	return s.normal
}
func (s *scaleType) Action() NumberEnum {
	return s.action
}