package enum

var AccountRights = newAccountRight()

type AccountRight interface {
	Number

	ViewOrgAllPatients() NumberEnum
}

func newAccountRight() AccountRight {
	instance := &accountRight{}
	instance.init()

	return instance
}

type accountRight struct {
	numberEnum

	viewOrgAllPatients NumberEnum
}

func (s *accountRight) init() {
	s.numberEnum.init()

	s.viewOrgAllPatients = s.newItem(1, "查看该机构全部患者")
}

func (s *accountRight) ViewOrgAllPatients() NumberEnum {
	return s.viewOrgAllPatients
}
