package enum

var FollowupStatuses = newFollowupStatus()

type FollowupStatus interface {
	Number

	Loss() NumberEnum
	Following() NumberEnum
	Valid() NumberEnum
}

func newFollowupStatus() FollowupStatus {
	instance := &followupStatus{}
	instance.init()

	return instance
}

type followupStatus struct {
	numberEnum

	loss      NumberEnum
	following NumberEnum
	valid     NumberEnum
}

func (s *followupStatus) init() {
	s.numberEnum.init()

	s.loss = s.newItem(0, "失访")
	s.following = s.newItem(1, "进行中")
	s.valid = s.newItem(2, "有效")
}

func (s *followupStatus) Loss() NumberEnum {
	return s.loss
}
func (s *followupStatus) Following() NumberEnum {
	return s.following
}
func (s *followupStatus) Valid() NumberEnum {
	return s.valid
}
