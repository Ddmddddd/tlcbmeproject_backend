package enum

var FollowupPlanStatuses = newFollowupPlanStatus()

type FollowupPlanStatus interface {
	Number

	Waiting() NumberEnum
	Finished() NumberEnum
	Ignored() NumberEnum
}

func newFollowupPlanStatus() FollowupPlanStatus {
	instance := &followupPlanStatus{}
	instance.init()

	return instance
}

type followupPlanStatus struct {
	numberEnum

	waiting  NumberEnum
	finished NumberEnum
	ignored  NumberEnum
}

func (s *followupPlanStatus) init() {
	s.numberEnum.init()

	s.waiting = s.newItem(0, "待随访")
	s.finished = s.newItem(1, "已随访")
	s.ignored = s.newItem(2, "已忽略")
}

func (s *followupPlanStatus) Waiting() NumberEnum {
	return s.waiting
}
func (s *followupPlanStatus) Finished() NumberEnum {
	return s.finished
}
func (s *followupPlanStatus) Ignored() NumberEnum {
	return s.ignored
}
