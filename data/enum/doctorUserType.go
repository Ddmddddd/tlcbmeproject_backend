package enum

var DoctorUserTypes = newDoctorUserType()

type DoctorUserType interface {
	Number

	Doctor() NumberEnum
	HealthManager() NumberEnum
	DoctorAndHealthManager() NumberEnum
}

func newDoctorUserType() DoctorUserType {
	instance := &doctorUserType{}
	instance.init()

	return instance
}

type doctorUserType struct {
	numberEnum

	doctor                 NumberEnum
	healthManager          NumberEnum
	doctorAndHealthManager NumberEnum
}

func (s *doctorUserType) init() {
	s.numberEnum.init()

	s.doctor = s.newItem(0, "医生")
	s.healthManager = s.newItem(1, "健康管理师")
	s.doctorAndHealthManager = s.newItem(11, "医生兼健康管理师")
}

func (s *doctorUserType) Doctor() NumberEnum {
	return s.doctor
}
func (s *doctorUserType) HealthManager() NumberEnum {
	return s.healthManager
}
func (s *doctorUserType) DoctorAndHealthManager() NumberEnum {
	return s.doctorAndHealthManager
}
