package enum

var TlcTeamTaskIntegralRuleTems = newTlcTeamTaskIntegralRuleTem()

type TlcTeamTaskIntegralRuleTem interface {
	Number
	FirstLevParticipation() NumberEnum
	SecondLevParticipation() NumberEnum
	ThirdLevParticipation() NumberEnum
}

func newTlcTeamTaskIntegralRuleTem() TlcTeamTaskIntegralRuleTem {
	instance := &tlcTeamTaskIntegralRuleTem{}
	instance.init()
	return instance
}

type tlcTeamTaskIntegralRuleTem struct {

	/**
	Create Team Task Integral Rule Template Enumerate
	---
	Attribution Type NumberEnum -> (key is number,value is string)
	--|--
	key : Basic Integral Assignment
	value : Explanation of Participation Level

	*/

	numberEnum
	firstLevParticipation  NumberEnum
	secondLevParticipation NumberEnum
	thirdLevParticipation  NumberEnum
}

func (s *tlcTeamTaskIntegralRuleTem) init() {
	s.numberEnum.init()

	s.firstLevParticipation = s.newItem(1, "完成率>0%，")

	s.secondLevParticipation = s.newItem(3, "完成率>=50%，")

	s.thirdLevParticipation = s.newItem(6, "完成率=100%，")
}

func (s *tlcTeamTaskIntegralRuleTem) FirstLevParticipation() NumberEnum {
	return s.firstLevParticipation
}

func (s *tlcTeamTaskIntegralRuleTem) SecondLevParticipation() NumberEnum {
	return s.secondLevParticipation
}

func (s *tlcTeamTaskIntegralRuleTem) ThirdLevParticipation() NumberEnum {
	return s.thirdLevParticipation
}
