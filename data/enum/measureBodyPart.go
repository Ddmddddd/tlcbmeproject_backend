package enum

var MeasureBodyParts = newMeasureBodyPart()

type MeasureBodyPart interface {
	Number

	Unknown() NumberEnum
	LeftUpperArm() NumberEnum
	LeftWrist() NumberEnum
	RightUpperArm() NumberEnum
}

type measureBodyPart struct {
	numberEnum

	unknown       NumberEnum
	leftUpperArm  NumberEnum
	leftWrist     NumberEnum
	rightUpperArm NumberEnum
	rightWrist    NumberEnum
}

func newMeasureBodyPart() MeasureBodyPart {
	instance := &measureBodyPart{}
	instance.init()

	return instance
}

func (s *measureBodyPart) init() {
	s.numberEnum.init()

	s.unknown = s.newItem(10, "未知")
	s.leftUpperArm = s.newItem(1, "左上臂")
	s.leftWrist = s.newItem(2, "左手腕")
	s.rightUpperArm = s.newItem(3, "右手腕")
	s.rightWrist = s.newItem(4, "右上臂")
}

func (s *measureBodyPart) Unknown() NumberEnum {
	return s.unknown
}

func (s *measureBodyPart) LeftUpperArm() NumberEnum {
	return s.leftUpperArm
}

func (s *measureBodyPart) LeftWrist() NumberEnum {
	return s.leftWrist
}

func (s *measureBodyPart) RightUpperArm() NumberEnum {
	return s.rightUpperArm
}

func (s *measureBodyPart) RightWrist() NumberEnum {
	return s.rightWrist
}
