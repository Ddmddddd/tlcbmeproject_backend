package enum

var PracticeCampInfos = newPracticeCampInfo()

type PracticeCampInfo interface {
	Number
	CampStartDate() NumberEnum
	CampHoldDays() NumberEnum
}

func newPracticeCampInfo() PracticeCampInfo {
	instance := &practiceCampInfo{}
	instance.init()
	return instance
}

type practiceCampInfo struct {
	/**
	Create Practice Camp Information Enumerate
	---
	Attribution Type NumberEnum -> (key is number,value is string)
	--|--
	key : Serial Number of Information
	value : Information in string formation
	--|--
	key|explanation
	1 -> Camp Start Date
	2 -> Camp Hold Days
	*/

	numberEnum
	campStartDate NumberEnum
	campHoldDays  NumberEnum
}

func (s *practiceCampInfo) init() {
	s.numberEnum.init()
	s.campStartDate = s.newItem(1, "2022-10-24")
	s.campHoldDays = s.newItem(2, "7")
}

func (s *practiceCampInfo) CampStartDate() NumberEnum {
	return s.campStartDate
}

func (s *practiceCampInfo) CampHoldDays() NumberEnum {
	return s.campHoldDays
}
