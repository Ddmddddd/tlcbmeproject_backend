package enum

var ExpStatuses = newExpStatus()

//状态scale（需要完成问卷）,已经被弃用
type ExpStatus interface {
	Number

	StopExp() NumberEnum
	InExp() NumberEnum
	Scale() NumberEnum
	Task() NumberEnum
}

func newExpStatus() ExpStatus {
	instance := &expStatus{}
	instance.init()

	return instance
}

type expStatus struct {
	numberEnum

	stopExp         NumberEnum
	inExp           NumberEnum
	scale  			NumberEnum
	task 			NumberEnum
}

func (s *expStatus) init() {
	s.numberEnum.init()

	s.stopExp = s.newItem(0, "退出饮食管理")
	s.inExp = s.newItem(1, "饮食管理中")
	s.scale = s.newItem(2, "需要完成问卷")
	s.task = s.newItem(3, "未完成行动计划选择")
}

func (s *expStatus) StopExp() NumberEnum {
	return s.stopExp
}
func (s *expStatus) InExp() NumberEnum {
	return s.inExp
}
func (s *expStatus) Scale() NumberEnum {
	return s.scale
}
func (s *expStatus) Task() NumberEnum {
	return s.task
}