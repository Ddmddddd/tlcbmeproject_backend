package enum

var AlertProcessModes = newAlertProcessMode()

type AlertProcessMode interface {
	Number

	Followup() NumberEnum
	Ignore() NumberEnum
}

func newAlertProcessMode() AlertProcessMode {
	instance := &alertProcessMode{}
	instance.init()

	return instance
}

type alertProcessMode struct {
	numberEnum

	followup NumberEnum
	ignore   NumberEnum
}

func (s *alertProcessMode) init() {
	s.numberEnum.init()

	s.followup = s.newItem(0, "随访")
	s.ignore = s.newItem(1, "忽略")
}

func (s *alertProcessMode) Followup() NumberEnum {
	return s.followup
}

func (s *alertProcessMode) Ignore() NumberEnum {
	return s.ignore
}
