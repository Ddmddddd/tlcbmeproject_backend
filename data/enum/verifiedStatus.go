package enum

var VerifiedStatuses = newVerifiedStatus()

type VerifiedStatus interface {
	Number

	Unverified() NumberEnum
	Verified() NumberEnum
	NotPass() NumberEnum
}

func newVerifiedStatus() VerifiedStatus {
	instance := &verifiedStatus{}
	instance.init()

	return instance
}

type verifiedStatus struct {
	numberEnum

	unverified NumberEnum
	verified   NumberEnum
	notPass    NumberEnum
}

func (s *verifiedStatus) init() {
	s.numberEnum.init()

	s.unverified = s.newItem(0, "未认证")
	s.verified = s.newItem(1, "已认证")
	s.notPass = s.newItem(2, "认证不通过")
}

func (s *verifiedStatus) Unverified() NumberEnum {
	return s.unverified
}
func (s *verifiedStatus) Verified() NumberEnum {
	return s.verified
}
func (s *verifiedStatus) NotPass() NumberEnum {
	return s.notPass
}
