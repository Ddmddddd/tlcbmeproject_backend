package enum

var MeasurePlaces = newMeasurePlace()

type MeasurePlace interface {
	Number

	Unknown() NumberEnum
	Home() NumberEnum
	Hospital() NumberEnum
	Drugstore() NumberEnum
	Other() NumberEnum
}

type measurePlace struct {
	numberEnum

	unknown   NumberEnum
	home      NumberEnum
	hospital  NumberEnum
	drugstore NumberEnum
	other     NumberEnum
}

func newMeasurePlace() MeasurePlace {
	instance := &measurePlace{}
	instance.init()

	return instance
}

func (s *measurePlace) init() {
	s.numberEnum.init()

	s.unknown = s.newItem(10, "未知")
	s.home = s.newItem(1, "家里")
	s.hospital = s.newItem(2, "医院")
	s.drugstore = s.newItem(3, "药店")
	s.other = s.newItem(9, "其他场所")
}

func (s *measurePlace) Unknown() NumberEnum {
	return s.unknown
}

func (s *measurePlace) Home() NumberEnum {
	return s.home
}

func (s *measurePlace) Hospital() NumberEnum {
	return s.hospital
}

func (s *measurePlace) Drugstore() NumberEnum {
	return s.drugstore
}

func (s *measurePlace) Other() NumberEnum {
	return s.other
}
