package router

import (
	"github.com/ktpswjz/httpserver/router"
	"github.com/ktpswjz/httpserver/types"
	"net/http"
	"tlcbme_project/server/controller/patient"
)

type patientController struct {
	patientEnum    *patient.Enum
	patientData    *patient.Data
	patientSocket  *patient.Socket
	patientAccount *patient.Account
	patientExpData *patient.ExpData
}

func (s *innerRouter) mapPatientApi(path types.Path, router *router.Router) {
	s.patientEnum = patient.NewEnum()
	s.patientData = patient.NewData(s.cfg, s.GetLog(), s.patientToken, s.doctorBusiness, s.patientBusiness)
	s.patientSocket = patient.NewSocket(s.cfg, s.GetLog(), s.patientToken, s.doctorBusiness, s.patientBusiness, s.notifyChannels)
	s.patientAccount = patient.NewAccount(s.cfg, s.GetLog(), s.patientToken, s.doctorBusiness, s.patientBusiness)
	s.patientExpData = patient.NewExpData(s.cfg, s.GetLog(), s.patientToken, s.doctorBusiness, s.patientBusiness)

	// WebSocket
	router.GET(path.Path("/socket/notify/subscribe"), s.patientSocket.NotifySubscribe, s.patientSocket.NotifySubscribeDoc)

	// 用户管理
	router.POST(path.Path("/account/password/change"), s.patientAccount.ChangePassword, s.patientAccount.ChangePasswordDoc)
	router.POST(path.Path("/account/phone/change"), s.patientAccount.ChangePhone, s.patientAccount.ChangePhoneDoc)
	router.POST(path.Path("/account/usability/stay/time/commit"), s.patientAccount.CommitUsabilityStayTimeInfo, s.patientAccount.CommitUsabilityStayTimeInfoDoc)
	router.POST(path.Path("/account/usability/event/commit"), s.patientAccount.CommitUsabilityEventInfo, s.patientAccount.CommitUsabilityEventInfoDoc)
	router.POST(path.Path("/account/usability/device/commit"), s.patientAccount.CommitUsabilityDeviceInfo, s.patientAccount.CommitUsabilityDeviceInfoDoc)
	router.POST(path.Path("/account/usability/feedback/commit"), s.patientAccount.CommitUsabilityFeedbackInfo, s.patientAccount.CommitUsabilityFeedbackInfoDoc)
	router.POST(path.Path("/account/bind/wx"), s.patientAccount.BindWeChat, s.patientAccount.BindWeChatDoc)
	router.POST(path.Path("/account/release/wx"), s.patientAccount.ReleaseWeChat, s.patientAccount.ReleaseWeChatDoc)
	router.POST(path.Path("/account/check/wx"), s.patientAccount.CheckWeChatStatus, s.patientAccount.CheckWeChatStatusDoc)
	router.POST(path.Path("/account/nickname/change"), s.patientAccount.ChangeNickname, s.patientAccount.ChangeNicknameDoc)

	// 枚举信息
	router.POST(path.Path("/enum/account/status"), s.patientEnum.ListAccountStatus, s.patientEnum.ListAccountStatusDoc)
	router.POST(path.Path("/enum/sex"), s.patientEnum.ListSex, s.patientEnum.ListSexDoc)
	router.POST(path.Path("/enum/user/type"), s.patientEnum.ListUserType, s.patientEnum.ListUserTypeDoc)
	router.POST(path.Path("/enum/verify/status"), s.patientEnum.ListVerifiedStatus, s.patientEnum.ListVerifiedStatusDoc)
	router.POST(path.Path("/enum/manage/status"), s.patientEnum.ListManageStatus, s.patientEnum.ListManageStatusDoc)
	router.POST(path.Path("/enum/measure/body/part"), s.patientEnum.ListMeasureBodyPart, s.patientEnum.ListMeasureBodyPartDoc)
	router.POST(path.Path("/enum/measure/place"), s.patientEnum.ListMeasurePlace, s.patientEnum.ListMeasurePlaceDoc)

	// 数据管理
	router.POST(path.Path("/data/blood/pressure/record/create"), s.patientData.CreateBloodPressureRecord, s.patientData.CreateBloodPressureRecordDoc)
	router.POST(path.Path("/data/blood/pressure/record/commit"), s.patientData.CommitBloodPressureRecord, s.patientData.CommitBloodPressureRecordDoc)
	router.POST(path.Path("/data/blood/glucose/record/commit"), s.patientData.CommitBloodGlucoseRecord, s.patientData.CommitBloodGlucoseRecordDoc)
	router.POST(path.Path("/data/weight/record/commit"), s.patientData.CommitWeightRecord, s.patientData.CommitWeightRecordDoc)
	router.POST(path.Path("/data/drug/record/commit"), s.patientData.CommitDrugRecord, s.patientData.CommitDrugRecordDoc)
	router.POST(path.Path("/data/diet/record/commit"), s.patientData.CommitDietRecord, s.patientData.CommitDietRecordDoc)
	router.POST(path.Path("/data/sport/record/commit"), s.patientData.CommitSportRecord, s.patientData.CommitSportRecordDoc)
	router.POST(path.Path("/data/discomfort/record/commit"), s.patientData.CommitDiscomfortRecord, s.patientData.CommitDiscomfortRecordDoc)
	router.POST(path.Path("/data/blood/pressure/record/delete"), s.patientData.DeleteBloodPressureRecord, s.patientData.DeleteBloodPressureRecordDoc)
	router.POST(path.Path("/data/blood/glucose/record/delete"), s.patientData.DeleteBloodGlucoseRecord, s.patientData.DeleteBloodGlucoseRecordDoc)
	router.POST(path.Path("/data/weight/record/delete"), s.patientData.DeleteWeightRecord, s.patientData.DeleteWeightRecordDoc)
	router.POST(path.Path("/data/drug/record/delete"), s.patientData.DeleteDrugRecord, s.patientData.DeleteDrugRecordDoc)
	router.POST(path.Path("/data/diet/record/delete"), s.patientData.DeleteDietRecord, s.patientData.DeleteDietRecordDoc)
	router.POST(path.Path("/data/sport/record/delete"), s.patientData.DeleteSportRecord, s.patientData.DeleteSportRecordDoc)
	router.POST(path.Path("/data/discomfort/record/delete"), s.patientData.DeleteDiscomfortRecord, s.patientData.DeleteDiscomfortRecordDoc)
	router.POST(path.Path("/data/blood/pressure/record/list"), s.patientData.ListBloodPressureRecord, s.patientData.ListBloodPressureRecordDoc)
	router.POST(path.Path("/data/blood/glucose/record/list"), s.patientData.ListBloodGlucoseRecord, s.patientData.ListBloodGlucoseRecordDoc)
	router.POST(path.Path("/data/weight/record/list"), s.patientData.ListWeightRecord, s.patientData.ListWeightRecordDoc)
	router.POST(path.Path("/data/weight/blood/record/list"), s.patientData.ListWeightBloodRecord, s.patientData.ListWeightBloodRecordDoc)
	router.POST(path.Path("/data/drug/record/list"), s.patientData.ListDrugRecord, s.patientData.ListDrugRecordDoc)
	router.POST(path.Path("/data/diet/record/list"), s.patientData.ListDietRecord, s.patientData.ListDietRecordDoc)
	router.POST(path.Path("/data/diet/record/get"), s.patientData.GetDietRecord, s.patientData.GetDietRecordDoc)
	router.POST(path.Path("/data/sport/record/list"), s.patientData.ListSportRecord, s.patientData.ListSportRecordDoc)
	router.POST(path.Path("/data/discomfort/record/list"), s.patientData.ListDiscomfortRecord, s.patientData.ListDiscomfortRecordDoc)
	router.POST(path.Path("/data/sport/step/record/list"), s.patientData.ListStepRecord, s.patientData.ListStepRecordDoc)
	router.POST(path.Path("/data/sport/step/record/oneday"), s.patientData.OneDayStepRecord, s.patientData.OneDayStepRecordDoc)
	router.POST(path.Path("/data/sport/step/we/data/decrypt"), s.patientData.DecryptWeRunData, s.patientData.DecryptWeRunDataDoc)
	router.POST(path.Path("/data/today/record"), s.patientData.TodayRecord, s.patientData.TodayRecordDoc)
	router.POST(path.Path("/data/monthly/report"), s.patientData.MonthlyReport, s.patientData.MonthlyReportDoc)
	router.POST(path.Path("/data/patient/logined"), s.patientData.PatientLogined, s.patientData.PatientLoginedDoc)
	router.POST(path.Path("/app/version/update/check"), s.patientData.CheckAppUpdate, s.patientData.CheckAppUpdateDoc)
	router.POST(path.Path("/data/doctor/patient/chat/msg/send"), s.patientData.SendChatMessage, s.patientData.SendChatMessageDoc)
	router.POST(path.Path("/data/doctor/patient/chat/msg/read"), s.patientData.SendChatMessageRead, s.patientData.SendChatMessageReadDoc)
	router.POST(path.Path("/data/doctor/patient/chat/msg/list"), s.patientData.GetChatMessageList, s.patientData.GetChatMessageListDoc)
	router.POST(path.Path("/data/doctor/patient/chat/doctor/list"), s.patientData.GetChatDoctorList, s.patientData.GetChatDoctorListDoc)

	//exp相关
	router.POST(path.Path("/exp/baseinfo/get"), s.patientExpData.GetPatientBaseInfo, s.patientExpData.GetPatientBaseInfoDoc)
	router.POST(path.Path("/exp/join"), s.patientExpData.IncludePatientExp, s.patientExpData.IncludePatientExpDoc)
	router.POST(path.Path("/exp/default/task/scale/commit"), s.patientExpData.CommitDefaultTaskScale, s.patientExpData.CommitDefaultTaskScaleDoc)
	router.POST(path.Path("/exp/default/task/create"), s.patientExpData.CreateDefaultTask, s.patientExpData.CreateDefaultTaskDoc)
	router.POST(path.Path("/exp/scale/get/by/sno/list"), s.patientExpData.GetScaleBySnoList, s.patientExpData.GetScaleBySnoListDoc)
	router.POST(path.Path("/exp/scale/get/by/sno"), s.patientExpData.GetScaleBySno, s.patientExpData.GetScaleBySnoDoc)
	router.POST(path.Path("/exp/scale/record/commit"), s.patientExpData.CommitScaleRecord, s.patientExpData.CommitScaleRecordDoc)
	router.POST(path.Path("/exp/action/plan/get"), s.patientExpData.GetActionPlan, s.patientExpData.GetActionPlanDoc)
	router.POST(path.Path("/exp/action/plan/edit"), s.patientExpData.EditActionPlan, s.patientExpData.EditActionPlanDoc)
	router.POST(path.Path("/exp/action/plan/record/commit"), s.patientExpData.CommitActionPlanRecord, s.patientExpData.CommitActionPlanRecordDoc)
	router.POST(path.Path("/exp/action/plan/record/edit"), s.patientExpData.EditActionPlanRecord, s.patientExpData.EditActionPlanRecordDoc)
	router.POST(path.Path("/exp/action/plan/record/get"), s.patientExpData.GetActionPlanRecord, s.patientExpData.GetActionPlanRecordDoc)
	router.POST(path.Path("/exp/game/get"), s.patientExpData.GetOneGame, s.patientExpData.GetOneGameDoc)
	router.POST(path.Path("/exp/game/result/commit"), s.patientExpData.CommitGameResult, s.patientExpData.CommitGameResultDoc)
	router.POST(path.Path("/exp/game/result/get"), s.patientExpData.GetGameResult, s.patientExpData.GetGameResultDoc)
	router.POST(path.Path("/exp/game/result/order/get"), s.patientExpData.GetGameResultOrder, s.patientExpData.GetGameResultOrderDoc)
	router.POST(path.Path("/exp/patient/task/get"), s.patientExpData.GetPatientTodayTask, s.patientExpData.GetPatientTodayTaskDoc)
	router.POST(path.Path("/exp/today/knowledge/by/type/get"), s.patientExpData.GetTodayKnowledgeByType, s.patientExpData.GetTodayKnowledgeByTypeDoc)
	router.POST(path.Path("/exp/knowledge/by/id/get"), s.patientExpData.GetKnowledgeByID, s.patientExpData.GetKnowledgeByIDDoc)
	//router.POST(path.Path("/exp/get/knowledge/status"), s.patientExpData.GetKnowledgeStatus, s.patientExpData.GetKnowledgeStatusDoc)
	router.POST(path.Path("/exp/knowledge/commit"), s.patientExpData.CommitKnowledge, s.patientExpData.CommitKnowledgeDoc)
	router.POST(path.Path("/exp/history/knowledge/get"), s.patientExpData.GetHistoryKnowledge, s.patientExpData.GetHistoryKnowledgeDoc)
	router.POST(path.Path("/exp/question/by/day/index/get"), s.patientExpData.GetQuestionByDayIndex, s.patientExpData.GetQuestionByDayIndexDoc)
	router.POST(path.Path("/exp/question/answer/commit"), s.patientExpData.CommitQuestionAnswer, s.patientExpData.CommitQuestionAnswerDoc)
	router.POST(path.Path("/exp/undone/small/scale/get"), s.patientExpData.GetUndoneSmallScale, s.patientExpData.GetUndoneSmallScaleDoc)
	router.POST(path.Path("/exp/small/scale/get"), s.patientExpData.GetSmallScale, s.patientExpData.GetSmallScaleDoc)
	router.POST(path.Path("/exp/small/scale/commit"), s.patientExpData.CommitSmallScaleRecord, s.patientExpData.CommitSmallScaleRecordDoc)
	router.POST(path.Path("/exp/task/record/commit"), s.patientExpData.CommitTaskRecord, s.patientExpData.CommitTaskRecordDoc)
	router.POST(path.Path("/exp/task/record/get"), s.patientExpData.GetTaskRecord, s.patientExpData.GetTaskRecordDoc)
	router.POST(path.Path("/exp/task/credit/history/get"), s.patientExpData.GetTaskCreditHistory, s.patientExpData.GetTaskCreditHistoryDoc)
	router.POST(path.Path("/exp/comment/list/get"), s.patientExpData.GetUnreadDoctorCommentList, s.patientExpData.GetUnreadDoctorCommentListDoc)
	router.POST(path.Path("/exp/comment/read"), s.patientExpData.ReadComment, s.patientExpData.ReadCommentDoc)
	router.POST(path.Path("/exp/comment/list/by/snos/get"), s.patientExpData.GetCommentListBySnos, s.patientExpData.GetCommentListBySnosDoc)
	router.POST(path.Path("/exp/comment/diet/info/get"), s.patientExpData.GetDietAndCommentInfo, s.patientExpData.GetDietAndCommentInfoDoc)
	router.POST(path.Path("/exp/today/unread/comment/count/get"), s.patientExpData.GetTodayUnreadCommentCount, s.patientExpData.GetTodayUnreadCommentCountDoc)
	router.POST(path.Path("/exp/body/exam/image/upload"), s.patientExpData.UploadBodyExamImage, s.patientExpData.UploadBodyExamImageDoc)
	router.POST(path.Path("/exp/body/exam/image/get"), s.patientExpData.GetBodyExamImage, s.patientExpData.GetBodyExamImageDoc)
	router.POST(path.Path("/exp/file/get"), s.patientExpData.GetFile, nil)
	router.POST(path.Path("/exp/jin/scale/get"), s.patientExpData.GetJinScale, nil)
	router.POST(path.Path("/exp/finished/jin/scale/get"), s.patientExpData.GetFinishedJinScale, nil)
	router.POST(path.Path("/exp/exercise/plan/get"), s.patientExpData.GetExercisePlan, nil)
	router.POST(path.Path("/exp/exercise/plan/create"), s.patientExpData.CreateExercisePlan, nil)
	router.POST(path.Path("/exp/patient/integral/history/get"), s.patientExpData.GetPatientIntegralHistory, nil)
	router.POST(path.Path("/exp/patient/integral/unread/get"), s.patientExpData.GetPatientUnreadIntegral, nil)
	router.POST(path.Path("/exp/patient/integral/read"), s.patientExpData.ReadPatientIntegral, nil)
	router.POST(path.Path("/exp/patient/setting/dietitian/get"), s.patientExpData.GetSettingDietitian, nil)
	router.POST(path.Path("/exp/patient/integral/all/get"), s.patientExpData.GetPatientIntegralAll, nil)
	router.POST(path.Path("/exp/patient/diet/plan/get"), s.patientExpData.GetPatientDietPlan, nil)
	router.POST(path.Path("/exp/patient/diet/plan/level/get"), s.patientExpData.GetPatientDietPlanByLevel, nil)
	router.POST(path.Path("/exp/patient/diet/plan/read"), s.patientExpData.ReadPatientDietPlan, nil)
	router.POST(path.Path("/exp/patient/reply/commit"), s.patientExpData.CommitPatientReply, nil)
	router.POST(path.Path("/exp/patient/clock/get"), s.patientExpData.GetPatientClock, nil)
	router.POST(path.Path("/exp/commit/judge"), s.patientExpData.CommitJudge, nil)
	router.POST(path.Path("/exp/commit/bug"), s.patientExpData.CommitBug, nil)
	router.POST(path.Path("/exp/patient/info/get"), s.patientExpData.GetPatientInfo, nil)
	router.POST(path.Path("/exp/patient/info/update"), s.patientExpData.UpdatePatientInfo, nil)
	router.POST(path.Path("/exp/patient/point/rule/get"), s.patientExpData.GetPointRule, nil)

	router.POST(path.Path("/exp/patient/food/total/commit"), s.patientExpData.CommitTotalFood, nil)
	router.POST(path.Path("/exp/patient/food/total/delete"), s.patientExpData.DeleteTotalFood, nil)
	router.POST(path.Path("/exp/patient/use/time/update"), s.patientExpData.UpdateUseTime, nil)

	//健康教育相关
	router.POST(path.Path("/exp/patient/tag/info"), s.patientExpData.GetTagInfo, nil)
	router.POST(path.Path("/exp/patient/tag/get"), s.patientExpData.GetPatientTag, nil)
	router.POST(path.Path("/exp/patient/tag/delete"), s.patientExpData.DeletePatientTag, nil)
	router.POST(path.Path("/exp/patient/tag/add"), s.patientExpData.AddPatientTag, nil)
	router.POST(path.Path("/exp/patient/knowledge/get"), s.patientExpData.GetKnowledgeList, nil)
	router.POST(path.Path("/exp/patient/knowledge/addlovenum"), s.patientExpData.AddKnowledgeLoveNum, nil)
	router.POST(path.Path("/exp/patient/collections/getknowledgeidlist"), s.patientExpData.GetKnowledgeIdList, nil)
	router.POST(path.Path("/exp/patient/knowledge/getsingleknowledgebyid"), s.patientExpData.GetTlcKnowledgeById, nil)
	router.POST(path.Path("/exp/patient/collections/addcollectedknowledge"), s.patientExpData.AddCollectedKnowledge, nil)
	router.POST(path.Path("/exp/patient/collections/deletecollectedknowledge"), s.patientExpData.DeleteCollectedKnowledge, nil)
	router.POST(path.Path("/exp/patient/collections/getcollectedknowledgelist"), s.patientExpData.GetCollectedKnowledgeList, nil)
	router.POST(path.Path("/exp/patient/datetoback/givedatetobackgetrcknw"), s.patientExpData.GiveDateToGetRcKnw, nil)
	router.POST(path.Path("/exp/patient/knowledge/tlcgettodayknowledge"), s.patientExpData.TlcGetTodayKnowledge, nil)
	router.POST(path.Path("/exp/patient/knowledge/tlcgetknowledgestar"), s.patientExpData.TlcGetKnowledgeStar, nil)
	router.POST(path.Path("/exp/patient/knowledge/tlcupdateknowledgestar"), s.patientExpData.TlcUpdateKnowledgeStar, nil)
	router.POST(path.Path("/exp/patient/knowledge/tlcchangeknowledgestatus"), s.patientExpData.TlcChangeKnowledgeStatus, nil)
	router.POST(path.Path("/exp/patient/knowledge/tlcgethistoryknowledge"), s.patientExpData.TlcGetHistoryKnowledge, nil)
	//评论功能
	router.POST(path.Path("/exp/patient/comment/tlcsendknwcomment"), s.patientExpData.TlcSendKnwComment, nil)
	router.POST(path.Path("/exp/patient/comment/tlcgetknwcommmentsinfo"), s.patientExpData.TlcGetKnwCommentsInfo, nil)
	//健康教育与积分系统对接
	router.POST(path.Path("/exp/patient/task/tlchasknwcommitedastask"), s.patientExpData.TlcHasKnwCommitedAsTask, nil)
	//用户小卡片回答问题相关
	router.POST(path.Path("/exp/patient/heeduquestion/tlcprerequestofquestion"), s.patientExpData.TlcPreRequestOfQuestion, nil)
	router.POST(path.Path("/exp/patient/heeduquestion/tlcgetintegratedquestioninfo"), s.patientExpData.TlcGetIntegratedQuestionInfo, nil)
	router.POST(path.Path("/exp/patient/heeduquestion/tlcrecordpatientanswerquestion"), s.patientExpData.TlcRecordPatientAnswerQuestion, nil)
	//获取用户的基本信息
	router.POST(path.Path("/exp/patient/patientbaseinfo/tlcgetpatientname"), s.patientExpData.TlcGetPatientName, nil)
	//提醒相关
	router.POST(path.Path("/remind/wx/create"), s.patientExpData.WxRemindCreate, s.patientExpData.WxRemindCreateDoc)
	router.POST(path.Path("/remind/sms/create"), s.patientExpData.SmsRemindCreate, s.patientExpData.SmsRemindCreateDoc)
	router.POST(path.Path("/remind/photo/feedback/create"), s.patientExpData.PhotoFeedbackRemindCreate, s.patientExpData.PhotoFeedbackRemindCreateDoc)

	//红包
	router.POST(path.Path("/exp/transfers"), s.patientExpData.Transfer, nil)

	//营养分析相关
	router.POST(path.Path("/exp/commit/meals"), s.patientExpData.CommitThreeMeals, s.patientExpData.CommitThreeMealsDoc)
	router.POST(path.Path("/exp/get/meal"), s.patientExpData.GetMealContent, s.patientExpData.GetMealContentDoc)
	router.POST(path.Path("/exp/delete/meal"), s.patientExpData.DeleteMealContent, s.patientExpData.DeleteMealContentDoc)
	router.POST(path.Path("/exp/nutrient/analysis/save"), s.patientExpData.SaveNutrientAnalysis, s.patientExpData.SaveNutrientAnalysisDoc)
	router.POST(path.Path("/exp/nutrient/history/energy"), s.patientExpData.GetHistoryEnergy, s.patientExpData.GetHistoryEnergyDoc)
	router.POST(path.Path("/exp/nutrient/history/grade"), s.patientExpData.GetHistoryGrade, s.patientExpData.GetHistoryGradeDoc)
	router.POST(path.Path("/exp/food/search"), s.patientExpData.FoodSearch, s.patientExpData.FoodSearchDoc)
	router.POST(path.Path("/exp/food/measure/unit"), s.patientExpData.GetFoodMeasureUnit, s.patientExpData.GetFoodMeasureUnitDoc)
	router.POST(path.Path("/exp/food/multi/search"), s.patientExpData.FoodMultiSearch, s.patientExpData.FoodMultiSearchDoc)
	router.POST(path.Path("/exp/food/basic/search"), s.patientExpData.FoodBasicSearch, s.patientExpData.FoodBasicSearchDoc)
	router.POST(path.Path("/exp/recipe/add"), s.patientExpData.AddPatientRecipe, s.patientExpData.AddPatientRecipeDoc)

	//营养干预相关
	router.POST(path.Path("/exp/patient/nutrition/intervention/get"), s.patientExpData.GetNutritionIntervention, nil)
	router.POST(path.Path("/exp/patient/nutrition/intervention/update"), s.patientExpData.UpdateNutritionIntervention, nil)
	router.POST(path.Path("/exp/patient/nutrition/intervention/question/get"), s.patientExpData.GetNutritionInterventionQuestion, nil)
	router.POST(path.Path("/exp/patient/nutrition/intervention/question/result"), s.patientExpData.ResultNutritionInterventionQuestion, nil)
	router.POST(path.Path("/exp/patient/nutrition/intervention/question/if"), s.patientExpData.IfNutritionInterventionQuestion, nil)
	router.POST(path.Path("/exp/patient/nutrition/intervention/report/get"), s.patientExpData.GetWeeklyReport, nil)
	router.POST(path.Path("/exp/patient/weekly/habit/update"), s.patientExpData.UpdateWeeklyHabitReport, nil)

	//社群群组相关
	router.POST(path.Path("/exp/patient/team/createteam"), s.patientExpData.CreateTeam, nil)
	router.POST(path.Path("/exp/patient/team/create/pre"), s.patientExpData.CreateTeamPre, nil)
	router.POST(path.Path("/exp/patient/team/query/group"), s.patientExpData.TeamQueryByGroup, nil)
	router.POST(path.Path("/exp/patient/team/query/search"), s.patientExpData.TeamQuerySearch, nil)
	router.POST(path.Path("/exp/patient/team/join"), s.patientExpData.JoinTeam, nil)
	router.POST(path.Path("/exp/patient/team/isjoined"), s.patientExpData.IsJoinedTeam, nil)
	router.POST(path.Path("/exp/patient/team/mems/info"), s.patientExpData.GetTeamMemInfo, nil)
	router.POST(path.Path("/exp/patient/team/mem/delete"), s.patientExpData.DeleteTeamMem, nil)
	//社群功能相关
	router.POST(path.Path("/exp/patient/team/func/diets/get"), s.patientExpData.GetWholeTeamDietsData, nil)
	router.POST(path.Path("/exp/patient/team/func/weights/get"), s.patientExpData.GetTeamWeightsData, nil)
	router.POST(path.Path("/exp/patient/team/func/sport/steps/get"), s.patientExpData.GetTeamSportStepsData, nil)
	router.POST(path.Path("/exp/patient/team/func/task/get"), s.patientExpData.GetWholeTeamTaskInfo, nil)
	router.POST(path.Path("/exp/patient/team/func/info/get"), s.patientExpData.GetTeamInfo, nil)
	router.POST(path.Path("/exp/patient/team/func/integral/rank"), s.patientExpData.GetTeamRankWholeInfo, nil)
	router.POST(path.Path("/exp/patient/team/func/integral/insert"), s.patientExpData.InsertTeamIntegral, nil)
	//运动打卡可选项目
	router.POST(path.Path("/exp/patient/sport/item/get"), s.patientExpData.GetSportItem, nil)

}

func (s *innerRouter) mapPatientSite(path types.Path, router *router.Router, root string) {
	router.ServeFiles(path.Path("/*filepath"), http.Dir(root), nil)
}
