package router

import (
	"github.com/ktpswjz/httpserver/router"
	"github.com/ktpswjz/httpserver/types"
	"net/http"
	"tlcbme_project/server/authentication"
	"tlcbme_project/server/controller/auth"
)

type authController struct {
	adminAuthentication authentication.Admin

	authDoctor  *auth.Doctor
	authAdmin   *auth.Admin
	authPatient *auth.Patient
}

func (s *innerRouter) mapAuthApi(path types.Path, router *router.Router) {
	s.adminAuthentication.Config = &s.cfg.Site.Admin

	s.authDoctor = auth.NewDoctor(s.cfg, s.GetLog(), s.defaultToken, s.doctorBusiness)
	s.authDoctor.Authenticate = s.doctorBusiness.Authentication().Authenticate()
	s.authDoctor.Logined = s.doctorBusiness.Authentication().Logined()

	s.authPatient = auth.NewPatient(s.cfg, s.GetLog(), s.patientToken)
	s.authPatient.Authenticate = s.patientBusiness.Authentication().Authenticate()
	s.authPatient.Logined = s.patientBusiness.Authentication().Logined()
	s.authPatient.ValidateUser = s.patientBusiness.Authentication().ValidateUser()
	s.authPatient.CreateUser = s.patientBusiness.Authentication().CreateUser()
	s.authPatient.RegisterPatient = s.patientBusiness.Authentication().RegisterPatient()
	s.authPatient.GetWeChatOpenID = s.patientBusiness.Authentication().GetWeChatOpenID()
	s.authPatient.GetPatientByOpenID = s.patientBusiness.Authentication().GetPatientByOpenID()
	s.authPatient.AuditPatient = s.patientBusiness.Authentication().AuditPatient()
	s.authPatient.CreateDietPlan = s.patientBusiness.Authentication().CreateDietPlan()
	s.authPatient.SendCodeWithPhone = s.patientBusiness.Authentication().SendCodeWithPhone()
	s.authPatient.NewChangePasswordWithPhone = s.patientBusiness.Authentication().NewChangePasswordWithPhone()

	s.authAdmin = auth.NewAdmin(s.cfg, s.GetLog(), s.adminToken)
	s.authAdmin.Authenticate = s.adminAuthentication.Authenticate

	// 获取服务信息
	router.POST(path.Path("/info"), s.authDoctor.GetInfo, s.authDoctor.GetInfoDoc)
	router.POST(path.Path("/info/patient"), s.authPatient.GetInfo, s.authPatient.GetInfoDoc)
	router.POST(path.Path("/info/admin"), s.authAdmin.GetInfo, s.authAdmin.GetInfoDoc)

	// 获取RSA公钥
	router.POST(path.Path("/rsa/key/public"), s.authDoctor.GetRsaPublicKey, s.authDoctor.GetRsaPublicKeyDoc)
	router.POST(path.Path("/rsa/key/public/patient"), s.authPatient.GetRsaPublicKey, s.authPatient.GetRsaPublicKeyDoc)
	router.POST(path.Path("/rsa/key/public/admin"), s.authAdmin.GetRsaPublicKey, s.authAdmin.GetRsaPublicKeyDoc)

	// 获取验证码
	router.POST(path.Path("/captcha"), s.authDoctor.GetCaptcha, s.authDoctor.GetCaptchaDoc)
	router.POST(path.Path("/captcha/patient"), s.authPatient.GetCaptcha, s.authPatient.GetCaptchaDoc)
	router.POST(path.Path("/captcha/admin"), s.authAdmin.GetCaptcha, s.authAdmin.GetCaptchaDoc)

	// 用户登陆
	router.POST(path.Path("/login"), s.authDoctor.Login, s.authDoctor.LoginDoc)
	router.POST(path.Path("/login/patient"), s.authPatient.Login, s.authPatient.LoginDoc)
	router.POST(path.Path("/login/admin"), s.authAdmin.Login, s.authAdmin.LoginDoc)
	router.POST(path.Path("/login/app/code/visible"), s.authDoctor.IsShowAppDownloadCode, s.authDoctor.IsShowAppDownloadCodeDoc)

	// 患者端微信登录相关
	router.POST(path.Path("/login/wx/openid/get"), s.authPatient.GetWeChatOpenIDByCode, s.authPatient.GetWeChatOpenIDByCodeDoc)
	router.POST(path.Path("/login/patient/wx"), s.authPatient.LoginByWeChat, s.authPatient.LoginByWeChatDoc)

	// 用户名验证
	router.POST(path.Path("/validate/user/name/patient"), s.authPatient.ValidateUserName, s.authPatient.ValidateUserNameDoc)

	// 注册用户
	router.POST(path.Path("/register/user/patient"), s.authPatient.RegisterUser, s.authPatient.RegisterUserDoc)

	//忘记密码获取验证码
	router.POST(path.Path("/login/code/send"), s.authPatient.SendCode, nil)
	router.POST(path.Path("/login/password/change"), s.authPatient.NewChangePassword, nil)
}

func (s *innerRouter) mapAuthSite(path types.Path, router *router.Router, root string) {
	router.ServeFiles(path.Path("/*filepath"), http.Dir(root), nil)
}
