package router

import (
	"tlcbme_project/server/controller/callback"
	"github.com/ktpswjz/httpserver/router"
	"github.com/ktpswjz/httpserver/types"
	"net/http"
)

type callbackController struct {
	callbackManage *callback.Manage
}

func (s *innerRouter) mapCallbackApi(path types.Path, router *router.Router) {
	s.callbackManage = callback.NewManage(s.cfg, s.GetLog(), s.doctorBusiness, s.patientBusiness)

	router.POST(path.Path("/manage/subscribe/receive"), s.callbackManage.SubscribeReceive, s.callbackManage.SubscribeReceiveDoc)
}

func (s *innerRouter) mapCallbackSite(path types.Path, router *router.Router, root string) {
	router.ServeFiles(path.Path("/*filepath"), http.Dir(root), nil)
}
