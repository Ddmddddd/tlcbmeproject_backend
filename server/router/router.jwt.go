package router

import (
	"tlcbme_project/data/model"
	"tlcbme_project/server/config"
	"fmt"
	"github.com/ktpswjz/httpserver/security/jwt"
)

func (s *innerRouter) decodeJwt(value string, payload *model.JwtPayload) error {
	header := &jwt.Header{}
	_, err := jwt.Decode(value, payload, header)
	if err != nil {
		return err
	}

	secret := ""
	audience := ""
	providerType := s.cfg.Management.Providers.GetType(payload.Subject)
	if providerType == config.ManagementProviderTypeHbp {
		secret = s.cfg.Management.Providers.Hbp.Auth.Secret
		audience = s.cfg.Management.Providers.Hbp.Auth.ID
	} else if providerType == config.ManagementProviderTypeDm {
		secret = s.cfg.Management.Providers.Dm.Auth.Secret
		audience = s.cfg.Management.Providers.Dm.Auth.ID
	} else {
		return fmt.Errorf("发送方(sub=%s)无效", payload.Subject)
	}
	if audience != payload.Audience {
		return fmt.Errorf("接收方(aud=%s)无效", payload.Audience)
	}

	return jwt.Verify(value, secret)
}
