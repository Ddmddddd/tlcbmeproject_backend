package router

import (
	"github.com/ktpswjz/httpserver/router"
	"github.com/ktpswjz/httpserver/types"
	"tlcbme_project/server/controller/public"
)

type publicController struct {
	publicDict *public.Dict
	publicApp  *public.App
}

func (s *innerRouter) mapPublicApi(path types.Path, router *router.Router) {
	s.publicDict = public.NewDict(s.cfg, s.GetLog(), s.doctorBusiness, s.patientBusiness)
	s.publicApp = public.NewApp(s.cfg, s.GetLog(), s.doctorBusiness, s.patientBusiness, DownloadSite)

	// 字典信息
	router.POST(path.Path("/dict/division/tree"), s.publicDict.SearchDivisionTree, s.publicDict.SearchDivisionTreeDoc)
	router.POST(path.Path("/dict/division/base/info"), s.publicDict.ListDivision, s.publicDict.ListDivisionDoc)
	router.POST(path.Path("/dict/org/and/doctor/info"), s.publicDict.ListOrgAndDoctor, s.publicDict.ListOrgAndDoctorDoc)
	router.POST(path.Path("/dict/drug"), s.publicDict.ListDictDrug, s.publicDict.ListDictDrugDoc)

	// 移动应用
	router.POST(path.Path("/app/android/install/info"), s.publicApp.GetAndroidInfo, s.publicApp.GetAndroidInfoDoc)
	router.POST(path.Path("/app/wechat/install/info"), s.publicApp.GetWechatInfo, s.publicApp.GetWechatInfoDoc)

	//食物库字典信息
	router.POST(path.Path("/dict/food/category/get"), s.publicDict.ListDictFoodCategory, s.publicDict.ListDictFoodCategoryDoc)
	router.POST(path.Path("/dict/food/get/page/by/category"), s.publicDict.ListDictFoodPageByCategory, s.publicDict.ListDictFoodPageByCategoryDoc)
	router.POST(path.Path("/dict/food/search"), s.publicDict.FoodSearch, s.publicDict.FoodSearchDoc)

	//金数据问卷回调
	router.POST(path.Path("/jin/scale/base/upload"), s.publicDict.UploadBaseJinScale, nil)
	router.POST(path.Path("/jin/scale/weight/upload"), s.publicDict.UploadWeightJinScale, nil)
	router.POST(path.Path("/jin/scale/diet/upload"), s.publicDict.UploadDietJinScale, nil)
	router.POST(path.Path("/jin/scale/exercise/upload"), s.publicDict.UploadExerciseJinScale, nil)
	router.POST(path.Path("/jin/scale/fitness/upload"), s.publicDict.UploadFitnessJinScale, nil)
	router.POST(path.Path("/jin/scale/posture/upload"), s.publicDict.UploadPostureJinScale, nil)
	router.POST(path.Path("/jin/scale/fitness/end/upload"), s.publicDict.UploadFitnessEndJinScale, nil)
	router.POST(path.Path("/jin/scale/posture/end/upload"), s.publicDict.UploadPostureEndJinScale, nil)
	router.POST(path.Path("/jin/scale/end/upload"), s.publicDict.UploadEndJinScale, nil)
	router.POST(path.Path("/jin/scale/new/end/upload"), s.publicDict.UploadNewEndJinScale, nil)
	router.POST(path.Path("/jin/scale/base/health/upload"), s.publicDict.UploadBaseHealthJinScale, nil)
	router.POST(path.Path("/jin/scale/sportsteam/upload"), s.publicDict.UploadSportsTeamJinScale, nil)

	//问卷网问卷回调
	router.GET(path.Path("/wang/scale/test/upload"), s.publicDict.UploadTestWangScale, nil)
	//健康知识
	router.POST(path.Path("/knowledge/link/list"), s.publicDict.GetKnowledgeLinkList, nil)
	//训练营列表
	router.POST(path.Path("/dict/camp/list"), s.publicDict.GetCampList, nil)
}
