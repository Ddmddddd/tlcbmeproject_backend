package router

import (
	"tlcbme_project/data/entity/sqldb"
	"encoding/json"
	"github.com/ktpswjz/httpserver/router"
	"time"
)

func (s *innerRouter) saveApiLog(a router.Assistant) {
	if a == nil {
		return
	}
	if !a.GetRecord() {
		return
	}

	if s.sqlDatabase == nil {
		return
	}

	enterTime := a.EnterTime()
	leaveTime := a.LeaveTime()
	elapse := leaveTime.Sub(enterTime)

	dbEntity := &sqldb.ApiLog{
		SerialNo:       a.RID(),
		Method:         a.Method(),
		Schema:         a.Schema(),
		Uri:            a.Path(),
		StartTime:      &enterTime,
		EndTime:        &leaveTime,
		ElapseTime:     uint64(elapse),
		ElapseTimeText: elapse.String(),
		RIP:            a.RIP(),
		Input:          a.GetInput(),
		Output:         a.GetOutput(),
		Param:          a.GetParam(),
		Result:         0,
	}
	if a.IsError() {
		dbEntity.Result = 1
	}
	responseUID := &ResponseUID{}
	err := responseUID.init(dbEntity.Input)
	if err == nil {
		dbEntity.ResponseID = &responseUID.ResponseID
	}

	s.sqlDatabase.InsertSelective(dbEntity)
}

func (s *innerRouter) saveClientLog(dbEntity *sqldb.ClientLog) {
	if dbEntity == nil {
		return
	}
	now := time.Now()
	dbEntity.EndTime = &now
	elapse := now.Sub(*dbEntity.StartTime)
	dbEntity.ElapseTime = uint64(elapse)
	dbEntity.ElapseTimeText = elapse.String()

	responseUID := &ResponseUID{}
	err := responseUID.init(dbEntity.Output)
	if err == nil {
		dbEntity.ResponseID = &responseUID.ResponseID
	}

	s.sqlDatabase.InsertSelective(dbEntity)
}

type ResponseUID struct {
	ResponseID string `json:"uid"`
}

func (s *ResponseUID) init(data []byte) error {
	s.ResponseID = ""
	return json.Unmarshal(data, s)
}
