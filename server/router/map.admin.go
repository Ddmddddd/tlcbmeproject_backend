package router

import (
	"tlcbme_project/server/controller/admin"
	"github.com/ktpswjz/httpserver/router"
	"github.com/ktpswjz/httpserver/types"
	"net/http"
)

type adminController struct {
	adminSys     *admin.Sys
	adminLogout  *admin.Logout
	adminService *admin.Service
	adminSite    *admin.Site
	adminDoctor  *admin.Doctor
	adminConfig  *admin.Config
	adminLog     *admin.Log
	adminInstall *admin.Install
}

func (s *innerRouter) mapAdminApi(path types.Path, router *router.Router) {
	s.adminSys = admin.NewSys()
	s.adminLogout = admin.NewLogout(s.cfg, s.GetLog(), s.adminToken)
	s.adminService = admin.NewService(s.cfg, s.GetLog(), s.adminToken)
	s.adminSite = admin.NewSite(s.cfg, s.GetLog(), s.adminToken, AdminSite, DocSite)
	s.adminDoctor = admin.NewDoctor(s.cfg, s.GetLog(), s.adminToken, s.doctorBusiness)
	s.adminConfig = admin.NewConfig(s.cfg, s.GetLog(), s.adminToken, s.sqlDatabase, s, s.doctorBusiness.Sms(), s.patientBusiness.Data())
	s.adminLog = admin.NewLog(s.cfg, s.GetLog(), s.adminToken, s.sqlDatabase, s)
	s.adminInstall = admin.NewInstall(s.cfg, s.GetLog(), s.adminToken, s.sqlDatabase, DownloadSite)

	// 注销登陆
	router.POST(path.Path("/logout"), s.adminLogout.Logout, s.adminLogout.LogoutDoc)

	// 系统信息
	router.POST(path.Path("/sys/host"), s.adminSys.GetHost, s.adminSys.GetHostDoc)
	router.POST(path.Path("/sys/network/interfaces"), s.adminSys.GetNetworkInterfaces, s.adminSys.GetNetworkInterfacesDoc)
	router.POST(path.Path("/sys/disk/partitions"), s.adminSys.GetDiskPartitions, s.adminSys.GetDiskPartitionsDoc)

	// 日志信息
	router.POST(path.Path("/log/api/list"), s.adminLog.ListApi, s.adminLog.ListApiDoc)
	router.POST(path.Path("/log/api/page"), s.adminLog.ListApiPage, s.adminLog.ListApiPageDoc)
	router.POST(path.Path("/log/api/argument"), s.adminLog.GetApiArgument, s.adminLog.GetApiArgumentDoc)
	router.POST(path.Path("/log/api/list/stat"), s.adminLog.ListApiStat, s.adminLog.ListApiStatDoc)
	router.POST(path.Path("/log/client/page"), s.adminLog.ListClientPage, s.adminLog.ListClientPageDoc)
	router.POST(path.Path("/log/client/argument"), s.adminLog.GetClientArgument, s.adminLog.GetClientArgumentDoc)
	router.POST(path.Path("/log/manage/fail/record/page"), s.adminLog.SearchManageFailRecordPage, s.adminLog.SearchManageFailRecordPageDoc)
	router.POST(path.Path("/log/manage/fail/record/retry"), s.adminLog.RetryManageFailRecord, s.adminLog.RetryManageFailRecordDoc)
	router.POST(path.Path("/log/manage/fail/record/delete"), s.adminLog.DeleteManageFailRecord, s.adminLog.DeleteManageFailRecordDoc)
	router.POST(path.Path("/log/platform/fail/record/page"), s.adminLog.SearchPlatformFailRecordPage, s.adminLog.SearchPlatformFailRecordPageDoc)
	router.POST(path.Path("/log/platform/fail/record/retry"), s.adminLog.RetryPlatformFailRecord, s.adminLog.RetryPlatformFailRecordDoc)
	router.POST(path.Path("/log/platform/fail/record/delete"), s.adminLog.RetryPlatformFailRecord, s.adminLog.RetryPlatformFailRecordDoc)

	// 服务管理
	router.POST(path.Path("/svc/info"), s.adminService.GetInfo, s.adminService.GetInfoDoc)
	router.POST(path.Path("/svc/cfg/info"), s.adminService.GetConfig, s.adminService.GetConfigDoc)
	router.POST(path.Path("/svc/restart/enable"), s.adminService.CanRestart, s.adminService.CanRestartDoc)
	router.POST(path.Path("/svc/restart"), s.adminService.Restart, s.adminService.RestartDoc)
	router.POST(path.Path("/svc/update/enable"), s.adminService.CanUpdate, s.adminService.CanUpdateDoc)
	router.POST(path.Path("/svc/update"), s.adminService.Update, nil)

	// 配置管理
	router.POST(path.Path("/svc/cfg/db/mysql/info"), s.adminConfig.GetMySql, s.adminConfig.GetMySqlDoc)
	router.POST(path.Path("/svc/cfg/db/mysql/save"), s.adminConfig.SetMySql, s.adminConfig.SetMySqlDoc)
	router.POST(path.Path("/svc/cfg/db/mysql/test"), s.adminConfig.TestMySql, s.adminConfig.TestMySqlDoc)
	router.GET(path.Path("/svc/cfg/db/mysql/schema/download"), s.adminConfig.DownloadMysqlSchema, s.adminConfig.DownloadMysqlSchemaDoc)
	router.POST(path.Path("/svc/cfg/manage/provide/info"), s.adminConfig.GetManageProvider, s.adminConfig.GetManageProviderDoc)
	router.POST(path.Path("/svc/cfg/manage/provide/save"), s.adminConfig.SetManageProvider, s.adminConfig.SetManageProviderDoc)
	router.POST(path.Path("/svc/cfg/platform/provide/info"), s.adminConfig.GetPlatformProvider, s.adminConfig.GetPlatformProviderDoc)
	router.POST(path.Path("/svc/cfg/platform/provide/save"), s.adminConfig.SetPlatformProvider, s.adminConfig.SetPlatformProviderDoc)
	router.POST(path.Path("/svc/cfg/manage/jwt/create"), s.adminConfig.CreateJwt, s.adminConfig.CreateJwtDoc)
	router.POST(path.Path("/svc/cfg/platform/control/create"), s.adminConfig.CreateControl, s.adminConfig.CreateControlDoc)
	router.POST(path.Path("/svc/cfg/manage/provide/api/info"), s.adminConfig.GetManageProviderApi, s.adminConfig.GetManageProviderApiDoc)
	router.POST(path.Path("/svc/cfg/manage/provide/api/save"), s.adminConfig.SetManageProviderApi, s.adminConfig.SetManageProviderApiDoc)
	router.POST(path.Path("/svc/cfg/manage/provide/api/test"), s.adminConfig.TestManageProviderApi, s.adminConfig.TestManageProviderApiDoc)
	router.POST(path.Path("/svc/cfg/platform/provide/api/info"), s.adminConfig.GetPlatformProviderApi, s.adminConfig.GetPlatformProviderApiDoc)
	router.POST(path.Path("/svc/cfg/platform/provide/api/save"), s.adminConfig.SetPlatformProviderApi, s.adminConfig.SetPlatformProviderApiDoc)
	router.POST(path.Path("/svc/cfg/platform/provide/api/test"), s.adminConfig.TestPlatformProviderApi, s.adminConfig.TestPlatformProviderApiDoc)
	router.POST(path.Path("/svc/cfg/site/doctor/app/download/login/visible"), s.adminConfig.IsShowDoctorAppDownloadCode, s.adminConfig.IsShowDoctorAppDownloadCodeDoc)
	router.POST(path.Path("/svc/cfg/site/doctor/app/download/login/show"), s.adminConfig.ShowDoctorAppDownloadCode, s.adminConfig.ShowDoctorAppDownloadCodeDoc)
	router.POST(path.Path("/svc/cfg/sms/info"), s.adminConfig.GetSms, s.adminConfig.GetSmsDoc)
	router.POST(path.Path("/svc/cfg/sms/save"), s.adminConfig.SetSms, s.adminConfig.SetSmsDoc)
	router.POST(path.Path("/svc/cfg/task/info"), s.adminConfig.GetTask, s.adminConfig.GetTaskDoc)
	router.POST(path.Path("/svc/cfg/task/save"), s.adminConfig.SetTask, s.adminConfig.SetTaskDoc)
	router.POST(path.Path("/svc/cfg/statical/task/info"), s.adminConfig.GetStaticalTask, s.adminConfig.GetStaticalTaskDoc)
	router.POST(path.Path("/svc/cfg/statical/task/save"), s.adminConfig.SetStaticalTask, s.adminConfig.SetStaticalTaskDoc)
	router.POST(path.Path("/svc/cfg/statical/history/info"), s.adminConfig.GetHistoryConfig, s.adminConfig.GetHistoryConfigDoc)
	router.POST(path.Path("/svc/cfg/statical/history/save"), s.adminConfig.SetHistoryConfig, s.adminConfig.SetHistoryConfigDoc)

	// 网站管理
	router.POST(path.Path("/site/default/info"), s.adminSite.GetDefaultInfo, s.adminSite.GetDefaultInfoDoc)
	router.POST(path.Path("/site/default/upload"), s.adminSite.UploadDefault, nil)
	router.POST(path.Path("/site/admin/info"), s.adminSite.GetAdminInfo, s.adminSite.GetAdminInfoDoc)
	router.POST(path.Path("/site/admin/upload"), s.adminSite.UploadAdmin, nil)
	router.POST(path.Path("/site/doc/info"), s.adminSite.GetDocInfo, s.adminSite.GetDocInfoDoc)
	router.POST(path.Path("/site/doc/upload"), s.adminSite.UploadDoc, nil)

	router.POST(path.Path("/site/app/upload"), s.adminSite.UploadApp, nil)
	router.POST(path.Path("/site/app/tree"), s.adminSite.TreeApp, s.adminSite.TreeAppDoc)
	router.POST(path.Path("/site/app/delete"), s.adminSite.DeleteApp, s.adminSite.DeleteAppDoc)

	// 安装包管理
	router.POST(path.Path("/install/android/upload"), s.adminInstall.UploadAndroid, nil)
	router.POST(path.Path("/install/wechat/upload"), s.adminInstall.UploadWeChat, nil)
	router.POST(path.Path("/install/android/info"), s.adminInstall.GetAndroidInfo, s.adminInstall.GetAndroidInfoDoc)
	router.POST(path.Path("/install/wechat/info"), s.adminInstall.GetWechatInfo, s.adminInstall.GetWechatInfoDoc)
	router.POST(path.Path("/install/browser/chrome/upload"), s.adminInstall.UploadBrowserSetupForChrome, nil)
	router.POST(path.Path("/install/browser/chrome/info"), s.adminInstall.GetBrowserSetupInfoForChrome, s.adminInstall.GetBrowserSetupInfoForChromeDoc)

	// 医生端网站
	router.POST(path.Path("/site/doctor/account/create"), s.adminDoctor.CreateAccount, s.adminDoctor.CreateAccountDoc)
	router.POST(path.Path("/site/doctor/account/create/onetime"), s.adminDoctor.CreateAccountOneTime, s.adminDoctor.CreateAccountOneTimeDoc)
	router.POST(path.Path("/site/doctor/account/update"), s.adminDoctor.EditAccount, s.adminDoctor.EditAccountDoc)
	router.POST(path.Path("/site/doctor/account/password/reset"), s.adminDoctor.ResetAccountPassword, s.adminDoctor.ResetAccountPasswordDoc)
	router.POST(path.Path("/site/doctor/account/list"), s.adminDoctor.ListAccount, s.adminDoctor.ListAccountDoc)
	router.POST(path.Path("/site/doctor/account/list/like"), s.adminDoctor.ListAccountLike, s.adminDoctor.ListAccountLikeDoc)
	router.POST(path.Path("/site/doctor/account/basic/info/save"), s.adminDoctor.SaveBasicInfo, s.adminDoctor.SaveBasicInfoDoc)
	router.POST(path.Path("/site/doctor/account/basic/info/detail"), s.adminDoctor.GetBasicInfo, s.adminDoctor.GetBasicInfoDoc)
	router.POST(path.Path("/site/doctor/account/verified/info/save"), s.adminDoctor.SaveVerifiedInfo, s.adminDoctor.SaveVerifiedInfoDoc)
	router.POST(path.Path("/site/doctor/account/verified/info/detail"), s.adminDoctor.GetVerifiedInfo, s.adminDoctor.GetVerifiedInfoDoc)
	router.POST(path.Path("/site/doctor/account/right/list"), s.adminDoctor.GetRightIDList, s.adminDoctor.GetRightIDListDoc)
	router.POST(path.Path("/site/doctor/account/right/save"), s.adminDoctor.SaveRightIDList, s.adminDoctor.SaveRightIDListDoc)
	// 枚举信息
	router.POST(path.Path("/site/doctor/enum/sex"), s.adminDoctor.ListSex, s.adminDoctor.ListSexDoc)
	router.POST(path.Path("/site/doctor/enum/user/type"), s.adminDoctor.ListUserType, s.adminDoctor.ListUserTypeDoc)
	router.POST(path.Path("/site/doctor/enum/account/status"), s.adminDoctor.ListAccountStatus, s.adminDoctor.ListAccountStatusDoc)
	router.POST(path.Path("/site/doctor/enum/verified/status"), s.adminDoctor.ListVerifiedStatus, s.adminDoctor.ListVerifiedStatusDoc)
	router.POST(path.Path("/site/doctor/enum/right/list"), s.adminDoctor.ListAccountRights, s.adminDoctor.ListAccountRightsDoc)
	// 字典信息
	router.POST(path.Path("/site/doctor/dict/org/create"), s.adminDoctor.CreateDictOrg, s.adminDoctor.CreateDictOrgDoc)
	router.POST(path.Path("/site/doctor/dict/org/delete"), s.adminDoctor.DeleteDictOrg, s.adminDoctor.UDeleteDictOrgDoc)
	router.POST(path.Path("/site/doctor/dict/org/update"), s.adminDoctor.UpdateDictOrg, s.adminDoctor.UpdateDictOrgDoc)
	router.POST(path.Path("/site/doctor/dict/org/list"), s.adminDoctor.ListDictOrg, s.adminDoctor.ListDictOrgDoc)

	router.POST(path.Path("/site/doctor/dict/device/create"), s.adminDoctor.CreateDictDevice, s.adminDoctor.CreateDictDeviceDoc)
	router.POST(path.Path("/site/doctor/dict/device/delete"), s.adminDoctor.DeleteDictDevice, s.adminDoctor.UDeleteDictDeviceDoc)
	router.POST(path.Path("/site/doctor/dict/device/update"), s.adminDoctor.UpdateDictDevice, s.adminDoctor.UpdateDictDeviceDoc)
	router.POST(path.Path("/site/doctor/dict/device/list"), s.adminDoctor.ListDictDevice, s.adminDoctor.ListDictDeviceDoc)

	router.POST(path.Path("/site/doctor/dict/drug/create"), s.adminDoctor.CreateDictDrug, s.adminDoctor.CreateDictDrugDoc)
	router.POST(path.Path("/site/doctor/dict/drug/delete"), s.adminDoctor.DeleteDictDrug, s.adminDoctor.UDeleteDictDrugDoc)
	router.POST(path.Path("/site/doctor/dict/drug/update"), s.adminDoctor.UpdateDictDrug, s.adminDoctor.UpdateDictDrugDoc)
	router.POST(path.Path("/site/doctor/dict/drug/list"), s.adminDoctor.ListDictDrug, s.adminDoctor.ListDictDrugDoc)

	router.POST(path.Path("/site/doctor/dict/division/create"), s.adminDoctor.CreateDivision, s.adminDoctor.CreateDivisionDoc)
	router.POST(path.Path("/site/doctor/dict/division/delete"), s.adminDoctor.DeleteDivision, s.adminDoctor.DeleteDivisionDoc)
	router.POST(path.Path("/site/doctor/dict/division/tree"), s.adminDoctor.SearchDivisionTree, s.adminDoctor.SearchDivisionTreeDoc)
	router.POST(path.Path("/site/doctor/dict/division/parent/codes"), s.adminDoctor.SearchDivisionParentCodes, s.adminDoctor.SearchDivisionParentCodesDoc)
}

func (s *innerRouter) mapAdminSite(path types.Path, router *router.Router, root string) {
	router.ServeFiles(path.Path("/*filepath"), http.Dir(root), nil)
}
