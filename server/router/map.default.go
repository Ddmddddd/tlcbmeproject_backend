package router

import (
	"github.com/ktpswjz/httpserver/router"
	"github.com/ktpswjz/httpserver/types"
	"net/http"
	"tlcbme_project/server/controller/doctor"
)

type defaultController struct {
	doctorAccount    *doctor.Account
	doctorEnum       *doctor.Enum
	doctorDict       *doctor.Dict
	doctorPatient    *doctor.Patient
	doctorSocket     *doctor.Socket
	doctorFollowup   *doctor.Followup
	doctorManage     *doctor.Manage
	doctorAssess     *doctor.Assess
	doctorBlood      *doctor.Blood
	doctorGlucose    *doctor.Glucose
	doctorDrug       *doctor.Drug
	doctorAssessment *doctor.Assessment
	doctorChat       *doctor.Chat
	doctorSms        *doctor.Sms
	doctorOauth      *doctor.Oauth
	doctorInstrument *doctor.Instrument
	doctorExp        *doctor.Exp
	doctorStat       *doctor.Stat
}

func (s *innerRouter) mapDefaultApi(path types.Path, router *router.Router) {
	s.doctorAccount = doctor.NewAccount(s.cfg, s.GetLog(), s.defaultToken, s.doctorBusiness, DownloadSite)
	s.doctorEnum = doctor.NewEnum()
	s.doctorDict = doctor.NewDict(s.cfg, s.GetLog(), s.defaultToken, s.doctorBusiness)
	s.doctorPatient = doctor.NewPatient(s.cfg, s.GetLog(), s.defaultToken, s.doctorBusiness, s.patientBusiness)
	s.doctorSocket = doctor.NewSocket(s.cfg, s.GetLog(), s.defaultToken, s.doctorBusiness, s.patientBusiness, s.notifyChannels)
	s.doctorFollowup = doctor.NewFollowup(s.cfg, s.GetLog(), s.defaultToken, s.doctorBusiness, s.patientBusiness)
	s.doctorManage = doctor.NewManage(s.cfg, s.GetLog(), s.defaultToken, s.doctorBusiness, s.patientBusiness)
	s.doctorAssess = doctor.NewAssess(s.cfg, s.GetLog(), s.defaultToken, s.doctorBusiness, s.patientBusiness)
	s.doctorBlood = doctor.NewBlood(s.cfg, s.GetLog(), s.defaultToken, s.doctorBusiness, s.patientBusiness)
	s.doctorGlucose = doctor.NewGlucose(s.cfg, s.GetLog(), s.defaultToken, s.doctorBusiness, s.patientBusiness)
	s.doctorDrug = doctor.NewDrug(s.cfg, s.GetLog(), s.defaultToken, s.doctorBusiness, s.patientBusiness)
	s.doctorAssessment = doctor.NewAssessment(s.cfg, s.GetLog(), s.defaultToken, s.doctorBusiness, s.patientBusiness)
	s.doctorChat = doctor.NewChat(s.cfg, s.GetLog(), s.defaultToken, s.doctorBusiness)
	s.doctorSms = doctor.NewSms(s.cfg, s.GetLog(), s.defaultToken, s.doctorBusiness)
	s.doctorInstrument = doctor.NewInstrument(s.cfg, s.GetLog(), s.defaultToken, s.doctorBusiness, s.patientBusiness)
	s.doctorOauth = doctor.NewOauth(s.cfg, s.GetLog(), s.defaultToken, s.doctorBusiness, s.doctorBusiness.Authentication().Logined())
	s.doctorExp = doctor.NewExp(s.cfg, s.GetLog(), s.defaultToken, s.doctorBusiness, s.patientBusiness)
	s.doctorStat = doctor.NewStat(s.cfg, s.GetLog(), s.defaultToken, s.doctorBusiness)

	// 注销登陆
	router.POST(path.Path("/logout"), s.doctorAccount.Logout, s.doctorAccount.LogoutDoc)

	// WebSocket
	router.GET(path.Path("/socket/notify/subscribe"), s.doctorSocket.NotifySubscribe, s.doctorSocket.NotifySubscribeDoc)

	// 账号信息
	router.POST(path.Path("/account/basic/info/detail"), s.doctorAccount.GetBasicInfo, s.doctorAccount.GetBasicInfoDoc)
	router.POST(path.Path("/account/password/change"), s.doctorAccount.ChangePassword, s.doctorAccount.ChangePasswordDoc)
	router.POST(path.Path("/account/app/android/info"), s.doctorAccount.GetAndroidInfo, s.doctorAccount.GetAndroidInfoDoc)

	// 枚举信息
	router.POST(path.Path("/enum/account/status"), s.doctorEnum.ListAccountStatus, s.doctorEnum.ListAccountStatusDoc)
	router.POST(path.Path("/enum/sex"), s.doctorEnum.ListSex, s.doctorEnum.ListSexDoc)
	router.POST(path.Path("/enum/user/type"), s.doctorEnum.ListUserType, s.doctorEnum.ListUserTypeDoc)
	router.POST(path.Path("/enum/verify/status"), s.doctorEnum.ListVerifiedStatus, s.doctorEnum.ListVerifiedStatusDoc)
	router.POST(path.Path("/enum/manage/status"), s.doctorEnum.ListManageStatus, s.doctorEnum.ListManageStatusDoc)
	router.POST(path.Path("/enum/measure/body/part"), s.doctorEnum.ListMeasureBodyPart, s.doctorEnum.ListMeasureBodyPartDoc)
	router.POST(path.Path("/enum/measure/place"), s.doctorEnum.ListMeasurePlace, s.doctorEnum.ListMeasurePlaceDoc)
	router.POST(path.Path("/enum/alert/process/status"), s.doctorEnum.ListAlertStatus, s.doctorEnum.ListAlertStatusDoc)
	router.POST(path.Path("/enum/alert/process/mode"), s.doctorEnum.ListAlertProcessMode, s.doctorEnum.ListAlertProcessModeDoc)

	// 字典信息
	router.POST(path.Path("/dict/doctor"), s.doctorDict.ListDoctor, s.doctorDict.ListDoctorDoc)
	router.POST(path.Path("/dict/patient/feature"), s.doctorDict.ListPatientFeature, s.doctorDict.ListPatientFeatureDoc)
	router.POST(path.Path("/dict/manager/class"), s.doctorDict.ListManageClass, s.doctorDict.ListManageClassDoc)
	router.POST(path.Path("/dict/aliyunsms/template/param"), s.doctorDict.ListDictSmsParam, s.doctorDict.ListDictSmsParamDoc)
	router.POST(path.Path("/dict/aliyunsms/template/content"), s.doctorDict.ListDictSmsContent, s.doctorDict.ListDictSmsContentDoc)

	// 患者管理
	router.POST(path.Path("/patient/create"), s.doctorPatient.CreatePatient, s.doctorPatient.CreatePatientDoc)
	router.POST(path.Path("/patient/audit"), s.doctorPatient.AuditPatient, s.doctorPatient.AuditPatientDoc)
	router.POST(path.Path("/patient/detail"), s.doctorPatient.SearchPatientOne, s.doctorPatient.SearchPatientOneDoc)
	router.POST(path.Path("/patient/manage/index/page"), s.doctorPatient.ListManagedPatientIndexPage, s.doctorPatient.ListManagedPatientIndexPageDoc)
	router.POST(path.Path("/patient/manage/count/hbp"), s.doctorPatient.SearchViewManagedIndexMangeLevelCount, s.doctorPatient.SearchViewManagedIndexMangeLevelCountDoc)
	router.POST(path.Path("/patient/manage/count/dm"), s.doctorPatient.SearchViewManagedIndexDmTypeCount, s.doctorPatient.SearchViewManagedIndexDmTypeCountDoc)
	router.POST(path.Path("/patient/manage/page"), s.doctorPatient.SearchViewManagedIndexPage, s.doctorPatient.SearchViewManagedIndexPageDoc)
	router.POST(path.Path("/patient/blood/pressure/record/create"), s.doctorPatient.CreateBloodPressureRecord, s.doctorPatient.CreateBloodPressureRecordDoc)
	router.POST(path.Path("/patient/blood/pressure/record/data/list"), s.doctorPatient.ListBloodPressureRecord, s.doctorPatient.ListBloodPressureRecordDoc)
	router.POST(path.Path("/patient/blood/pressure/record/data/stat"), s.doctorPatient.StatBloodPressureRecord, s.doctorPatient.StatBloodPressureRecordDoc)
	router.POST(path.Path("/patient/info/basic"), s.doctorPatient.SearchPatientBasicInfoOne, s.doctorPatient.SearchPatientBasicInfoOneDoc)
	router.POST(path.Path("/patient/manage/plan/list"), s.doctorPatient.SearchManagementPlanList, s.doctorPatient.SearchManagementPlanListDoc)
	router.POST(path.Path("/patient/manage/register/page"), s.doctorPatient.SearchManagementApplicationReviewPage, s.doctorPatient.SearchManagementApplicationReviewPageDoc)
	router.POST(path.Path("/patient/weight/record/create"), s.doctorPatient.CreateWeightRecord, s.doctorPatient.CreateWeightRecordDoc)
	router.POST(path.Path("/patient/weight/height/last"), s.doctorPatient.SearchPatientHeightAndWeight, s.doctorPatient.SearchPatientHeightAndWeightDoc)
	router.POST(path.Path("/patient/weight/record/list"), s.doctorPatient.SearchWeightRecordList, s.doctorPatient.SearchWeightRecordListDoc)
	router.POST(path.Path("/patient/weight/bmi"), s.doctorPatient.SearchPatientBodyMassIndex, s.doctorPatient.SearchPatientBodyMassIndexDoc)
	router.POST(path.Path("/patient/sport/diet/page"), s.doctorPatient.SearchViewSportDietRecordPage, s.doctorPatient.SearchViewSportDietRecordPageDoc)
	router.POST(path.Path("/patient/discomfort/record/page"), s.doctorPatient.SearchDiscomfortRecordPage, s.doctorPatient.SearchDiscomfortRecordPageDoc)
	router.POST(path.Path("/patient/discomfort/count/stat/list"), s.doctorPatient.SearchViewStatDiscomfortList, s.doctorPatient.SearchViewStatDiscomfortListDoc)
	router.POST(path.Path("/patient/height/update"), s.doctorPatient.UpdatePatientHeight, s.doctorPatient.UpdatePatientHeightDoc)
	router.POST(path.Path("/patient/basic/info/update"), s.doctorPatient.UpdatePatientUserInfo, s.doctorPatient.UpdatePatientUserInfoDoc)
	router.POST(path.Path("/patient/diagnosismemo/update"), s.doctorPatient.UpdatePatientDiagnosismemo, s.doctorPatient.UpdatePatientDiagnosismemoDoc)
	router.POST(path.Path("/patient/weight/photo/list"), s.doctorPatient.ListPatientWeightPhoto, s.doctorPatient.ListPatientWeightPhotoDoc)
	router.POST(path.Path("/patient/clock/get"), s.doctorPatient.GetPatientClockDay, s.doctorPatient.GetPatientClockDayDoc)

	// 预警记录
	router.POST(path.Path("/patient/alert/record/page"), s.doctorPatient.ListAlertRecordPage, s.doctorPatient.ListAlertRecordPageDoc)
	router.POST(path.Path("/patient/alert/record/list"), s.doctorPatient.ListAlert, s.doctorPatient.ListAlertDoc)
	router.POST(path.Path("/patient/alert/record/count"), s.doctorPatient.CountAlert, s.doctorPatient.CountAlertDoc)
	router.POST(path.Path("/patient/alert/record/ignore"), s.doctorPatient.IgnoreAlert, s.doctorPatient.IgnoreAlertDoc)
	router.POST(path.Path("/patient/alert/code/list"), s.doctorPatient.SearchAlert, s.doctorPatient.SearchAlertDoc)

	// 医患交流
	router.POST(path.Path("/patient/chat/msg/send"), s.doctorChat.SendChatMessageFromDoctor, s.doctorChat.SendChatMessageFromDoctorDoc)
	router.POST(path.Path("/patient/chat/msg/read"), s.doctorChat.SendChatMessageReadFromDoctor, s.doctorChat.SendChatMessageReadFromDoctorDoc)
	router.POST(path.Path("/patient/chat/msg/list"), s.doctorChat.GetChatMessageList, s.doctorChat.GetChatMessageListDoc)
	router.POST(path.Path("/patient/last/chat/msg/list"), s.doctorChat.GetChatPatientLastMsgList, s.doctorChat.GetChatPatientLastMsgListDoc)

	// 随访管理
	router.POST(path.Path("/followup/plan/page"), s.doctorFollowup.SearchFollowupPlanPage, s.doctorFollowup.SearchFollowupPlanPageDoc)
	router.POST(path.Path("/followup/plan/ignore"), s.doctorFollowup.IgnoreFollowupPlan, s.doctorFollowup.IgnoreFollowupPlanDoc)
	router.POST(path.Path("/followup/record/save"), s.doctorFollowup.SaveFollowupRecord, s.doctorFollowup.SaveFollowupRecordDoc)
	router.POST(path.Path("/followup/record/page"), s.doctorFollowup.SearchFollowupRecordPage, s.doctorFollowup.SearchFollowupRecordPageDoc)

	// 管理等级
	router.POST(path.Path("/manage/htn/detail/list"), s.doctorManage.SearchHtnDetailList, s.doctorManage.SearchHtnDetailListDoc)
	router.POST(path.Path("/manage/htn/level/trend"), s.doctorManage.SearchHtnLevelTrend, s.doctorManage.SearchHtnLevelTrendDoc)
	router.POST(path.Path("/manage/dm/level/trend"), s.doctorManage.SearchDmLevelTrend, s.doctorManage.SearchDmLevelTrendDoc)
	router.POST(path.Path("/manage/status"), s.doctorManage.SearchManageStatus, s.doctorManage.SearchManageStatusDoc)
	router.POST(path.Path("/manage/terminate"), s.doctorManage.TerminateManage, s.doctorManage.TerminateManageDoc)
	router.POST(path.Path("/manage/recover"), s.doctorManage.RecoverManage, s.doctorManage.RecoverManageDoc)
	router.POST(path.Path("/manage/update"), s.doctorManage.UpdateManagementPlan, s.doctorManage.UpdateManagementPlanDoc)

	// 评估管理
	router.POST(path.Path("/assessment/record/create"), s.doctorAssessment.CreateAssessmentRecord, s.doctorAssessment.CreateAssessmentRecordDoc)
	router.POST(path.Path("/assessment/record/detail"), s.doctorAssessment.SearchAssessmentRecordOne, s.doctorAssessment.SearchAssessmentRecordOneDoc)
	router.POST(path.Path("/assessment/record/page"), s.doctorAssessment.SearchAssessmentRecordPage, s.doctorAssessment.SearchAssessmentRecordPageDoc)

	// 血压信息
	router.POST(path.Path("/blood/trend/single/list"), s.doctorBlood.SearchTrendSingleList, s.doctorBlood.SearchTrendSingleListDoc)
	router.POST(path.Path("/blood/trend/single/range"), s.doctorBlood.SearchTrendSingleRange, s.doctorBlood.SearchTrendSingleRangeDoc)
	router.POST(path.Path("/blood/table/single/range"), s.doctorBlood.SearchTableSingleRange, s.doctorBlood.SearchTableSingleRangeDoc)
	router.POST(path.Path("/blood/record/last"), s.doctorBlood.SearchRecordLastOne, s.doctorBlood.SearchRecordLastOneDoc)

	// 血糖信息
	router.POST(path.Path("/glucose/record/create"), s.doctorGlucose.CreateRecord, s.doctorGlucose.CreateRecordDoc)
	router.POST(path.Path("/glucose/record/trend/list"), s.doctorGlucose.SearchRecordTrendList, s.doctorGlucose.SearchRecordTrendListDoc)
	router.POST(path.Path("/glucose/record/table/list"), s.doctorGlucose.SearchRecordTableList, s.doctorGlucose.SearchRecordTableListDoc)

	// 用药管理
	router.POST(path.Path("/drug/record/index/list"), s.doctorDrug.SearchDrugUseRecordIndexList, s.doctorDrug.SearchDrugUseRecordIndexListDoc)
	router.POST(path.Path("/drug/record/group/month/list"), s.doctorDrug.SearchDrugUseRecordMonthGroupList, s.doctorDrug.SearchDrugUseRecordMonthGroupListDoc)
	router.POST(path.Path("/drug/record/index/log/list"), s.doctorDrug.SearchDrugUseRecordIndexModifiedLogList, s.doctorDrug.SearchDrugUseRecordIndexModifiedLogListDoc)
	router.POST(path.Path("/drug/record/index/log/create"), s.doctorDrug.CreateDrugUseRecordIndexModifiedLog, s.doctorDrug.CreateDrugUseRecordIndexModifiedLogDoc)
	router.POST(path.Path("/drug/record/index/create"), s.doctorDrug.CreateDrugUseRecordIndex, s.doctorDrug.CreateDrugUseRecordIndexDoc)

	//  短信管理
	router.POST(path.Path("/patient/sms/send"), s.doctorSms.SendSms, s.doctorSms.SendSmsDoc)
	router.POST(path.Path("/patient/sms/record/list"), s.doctorSms.GetSmsRecordList, s.doctorSms.GetSmsRecordListDoc)

	router.POST(path.Path("/oauth/user/info"), s.doctorOauth.GetOauthCode, s.doctorOauth.GetOauthCodeDoc)
	router.POST(path.Path("/server/time"), s.doctorDict.GetServerTime, s.doctorDict.GetServerTimeDoc)

	// 第三方仪器对接
	router.POST(path.Path("/instrument/blood/pressure/record/create"), s.doctorInstrument.CreateBloodPressureRecord, s.doctorInstrument.CreateBloodPressureRecordDoc)

	// 饮食实验相关
	router.POST(path.Path("/exp/patient/info/get"), s.doctorExp.GetPatientExpInfo, s.doctorExp.GetPatientExpInfoDoc)
	router.POST(path.Path("/exp/patient/include"), s.doctorExp.IncludePatientInExp, s.doctorExp.IncludePatientInExpDoc)
	router.POST(path.Path("/exp/patient/remove"), s.doctorExp.RemovePatientFromExp, s.doctorExp.RemovePatientFromExpDoc)
	router.POST(path.Path("/exp/scale/common/get/all"), s.doctorExp.GetAllExpCommonScale, s.doctorExp.GetAllExpCommonScaleDoc)
	router.POST(path.Path("/exp/scale/common/get"), s.doctorExp.GetExpCommonScale, s.doctorExp.GetExpCommonScaleDoc)
	router.POST(path.Path("/exp/patient/scale/common/commit"), s.doctorExp.CommitPatientExpScaleRecord, s.doctorExp.CommitPatientExpScaleRecordDoc)
	router.POST(path.Path("/exp/patient/action/plan/get"), s.doctorExp.GetPatientActionPlan, s.doctorExp.GetPatientActionPlanDoc)
	router.POST(path.Path("/exp/patient/action/plan/create"), s.doctorExp.CreatePatientActionPlan, s.doctorExp.CreatePatientActionPlanDoc)
	router.POST(path.Path("/exp/patient/action/plan/edit"), s.doctorExp.EditPatientActionPlan, s.doctorExp.EditPatientActionPlanDoc)
	router.POST(path.Path("/exp/patient/action/plan/commit"), s.doctorExp.CommitPatientActionPlan, s.doctorExp.CommitPatientActionPlanDoc)
	router.POST(path.Path("/exp/patient/action/plan/record/get"), s.doctorExp.GetPatientActionPlanRecord, s.doctorExp.GetPatientActionPlanRecordDoc)
	router.POST(path.Path("/exp/patient/task/record/list/get"), s.doctorExp.GetPatientTaskRecordList, s.doctorExp.GetPatientTaskRecordListDoc)
	router.POST(path.Path("/exp/patient/task/record/get"), s.doctorExp.GetPatientTaskRecord, s.doctorExp.GetPatientTaskRecordDoc)
	router.POST(path.Path("/exp/patient/diet/record/get"), s.doctorExp.GetPatientDietRecord, s.doctorExp.GetPatientDietRecordDoc)
	router.POST(path.Path("/exp/patient/diet/record/comment/get"), s.doctorExp.GetPatientDietRecordComment, s.doctorExp.GetPatientDietRecordCommentDoc)
	router.POST(path.Path("/exp/patient/diet/record/comment/commit"), s.doctorExp.CommitPatientDietRecordComment, s.doctorExp.CommitPatientDietRecordCommentDoc)
	router.POST(path.Path("/exp/patient/comment/template/get"), s.doctorExp.GetCommentTemplate, s.doctorExp.GetCommentTemplateDoc)
	router.POST(path.Path("/exp/patient/comment/template/add"), s.doctorExp.AddCommentTemplate, s.doctorExp.AddCommentTemplateDoc)
	router.POST(path.Path("/exp/patient/comment/template/delete"), s.doctorExp.DeleteCommentTemplate, s.doctorExp.DeleteCommentTemplateDoc)
	router.POST(path.Path("/exp/patient/comment/template/top"), s.doctorExp.TopCommentTemplate, nil)
	router.POST(path.Path("/exp/un/comment/task/count"), s.doctorExp.GetUnCommentTaskCount, s.doctorExp.GetUnCommentTaskCountDoc)
	router.POST(path.Path("/exp/un/comment/task/page"), s.doctorExp.GetUnCommentTaskPage, s.doctorExp.GetUnCommentTaskPageDoc)
	router.POST(path.Path("/exp/current/day/without/clocking/in/page"), s.doctorExp.GetCurrentDayWithoutClockingInPage, s.doctorExp.GetCurrentDayWithoutClockingInPageDoc)
	router.POST(path.Path("/exp/comment/record/page"), s.doctorExp.GetCommentRecordPage, nil)
	router.POST(path.Path("/exp/comment/record/delete"), s.doctorExp.DeleteCommentRecord, nil)
	router.POST(path.Path("/exp/patient/check/data/get"), s.doctorExp.GetPatientCheckData, nil)
	router.POST(path.Path("/exp/patient/body/exam/image/list/get"), s.doctorExp.GetPatientBodyExamImageList, nil)
	router.POST(path.Path("/exp/patient/file/upload"), s.doctorExp.UploadPatientFile, nil)
	router.POST(path.Path("/exp/patient/file/get"), s.doctorExp.GetPatientFile, nil)
	router.POST(path.Path("/exp/patient/diet/plan/get"), s.doctorExp.GetPatientDietPlan, nil)
	router.POST(path.Path("/exp/patient/diet/plan/delete"), s.doctorExp.DeletePatientDietPlan, nil)
	router.POST(path.Path("/exp/patient/diet/plan/save"), s.doctorExp.SavePatientDietPlan, nil)
	router.POST(path.Path("/exp/patient/diet/plan/delay/save"), s.doctorExp.SaveAndDelayPatientDietPlan, nil)
	router.POST(path.Path("/exp/patient/diet/plan/start"), s.doctorExp.StartPatientDietPlan, nil)

	// 任务编辑工具
	router.POST(path.Path("/exp/task/tree"), s.doctorExp.GetTaskTree, nil)
	router.POST(path.Path("/exp/task/add"), s.doctorExp.AddTask, nil)
	router.POST(path.Path("/exp/task/edit"), s.doctorExp.EditTask, nil)
	router.POST(path.Path("/exp/task/delete"), s.doctorExp.DeleteTask, nil)
	router.POST(path.Path("/exp/get/knowledge/by/type"), s.doctorExp.GetKnowledgeByType, nil)
	router.POST(path.Path("/exp/edit/knowledge"), s.doctorExp.EditKnowledge, nil)
	router.POST(path.Path("/exp/add/knowledge"), s.doctorExp.AddKnowledge, nil)
	router.POST(path.Path("/exp/delete/knowledge"), s.doctorExp.DeleteKnowledge, nil)
	router.POST(path.Path("/exp/get/question/by/knowledge/id"), s.doctorExp.GetQuestionByKnowledgeID, nil)
	router.POST(path.Path("/exp/add/question"), s.doctorExp.AddQuestion, nil)
	router.POST(path.Path("/exp/delete/question"), s.doctorExp.DeleteQuestion, nil)
	router.POST(path.Path("/exp/edit/question"), s.doctorExp.EditQuestion, nil)
	router.POST(path.Path("/exp/get/scale"), s.doctorExp.GetScale, nil)
	router.POST(path.Path("/exp/edit/scale"), s.doctorExp.EditScale, nil)
	router.POST(path.Path("/exp/feature/edit"), s.doctorExp.EditTaskFeature, nil)
	router.POST(path.Path("/exp/all/feature/get"), s.doctorExp.EditScale, nil)

	// 营养分析
	router.POST(path.Path("/exp/nutrient/analysis"), s.doctorExp.NutrientAnalysis, s.doctorExp.NutrientAnalysisDoc)
	router.POST(path.Path("/exp/nutrient/analysis/save"), s.doctorExp.SaveNutrientAnalysis, s.doctorExp.SaveNutrientAnalysisDoc)
	router.POST(path.Path("/exp/nutrient/history/grade"), s.doctorExp.GetHistoryGrade, s.doctorExp.GetHistoryGradeDoc)
	router.POST(path.Path("/exp/nutrient/history/energy"), s.doctorExp.GetHistoryEnergy, s.doctorExp.GetHistoryEnergyDoc)

	//饮食方案
	router.POST(path.Path("/exp/diet/program/data/get"), s.doctorExp.GetDietProgramData, s.doctorExp.GetDietProgramDataDoc)
	router.POST(path.Path("/exp/nutrition/indicator/update"), s.doctorExp.UpdateNutritionalIndicator, s.doctorExp.UpdateNutritionalIndicatorDoc)
	router.POST(path.Path("/exp/diet/program/update"), s.doctorExp.UpdateDietProgram, s.doctorExp.UpdateDietProgramDoc)
	router.POST(path.Path("/exp/diet/plan/template/get"), s.doctorExp.GetDietPlanTemplate, nil)
	//饮食方案的上周回顾
	router.POST(path.Path("/exp/nutrition/intervention/report/get"), s.doctorExp.GetNutritionWeeklyReport, nil)

	// 数据统计
	router.POST(path.Path("/stat/manage/count"), s.doctorStat.GetPatientManageCount, s.doctorStat.GetPatientManageCountDoc)
	router.POST(path.Path("/stat/blood/pressure/level/count"), s.doctorStat.GetBloodPressureLevelCount, s.doctorStat.GetBloodPressureLevelCountDoc)
	router.POST(path.Path("/stat/blood/glucose/level/count"), s.doctorStat.GetBloodGlucoseLevelCount, s.doctorStat.GetBloodGlucoseLevelCountDoc)
	router.POST(path.Path("/stat/blood/pressure/count"), s.doctorStat.GetBloodPressureCount, s.doctorStat.GetBloodPressureCountDoc)
	router.POST(path.Path("/stat/blood/pressure/rate"), s.doctorStat.GetBloodPressureRate, s.doctorStat.GetBloodPressureRateDoc)
	router.POST(path.Path("/stat/blood/glucose/count"), s.doctorStat.GetBloodGlucoseCount, s.doctorStat.GetBloodGlucoseCountDoc)
	router.POST(path.Path("/stat/blood/glucose/rate"), s.doctorStat.GetBloodGlucoseRate, s.doctorStat.GetBloodGlucoseRateDoc)
	router.POST(path.Path("/stat/other/data/count"), s.doctorStat.GetOtherData, s.doctorStat.GetOtherDataDoc)
	router.POST(path.Path("/stat/followup/data/count"), s.doctorStat.GetFollowupData, s.doctorStat.GetFollowupDataDoc)
	router.POST(path.Path("/stat/alert/data"), s.doctorStat.GetAlertData, s.doctorStat.GetAlertDataDoc)
	router.POST(path.Path("/stat/blood/pressure/risk/data"), s.doctorStat.GetBloodPressureRiskData, s.doctorStat.GetBloodPressureRiskDataDoc)
	router.POST(path.Path("/stat/blood/glucose/risk/data"), s.doctorStat.GetBloodGlucoseRiskData, s.doctorStat.GetBloodGlucoseRiskDataDoc)

	router.POST(path.Path("/exp/integral/get"), s.doctorExp.GetIntegral, s.doctorExp.GetIntegralDoc)
	router.POST(path.Path("/exp/grade/rule/get"), s.doctorExp.GetGradeRule, s.doctorExp.GetGradeRuleDoc)
	router.POST(path.Path("/exp/grade/rule/update"), s.doctorExp.UpdateGradeRule, s.doctorExp.UpdateGradeRuleDoc)

	// 组队功能-营养师端
	router.POST(path.Path("/exp/team/group/task/get"), s.doctorExp.GetAllGroupTasks, nil)
	router.POST(path.Path("/exp/team/group/task/create"), s.doctorExp.CreateGroupTask, nil)
	router.POST(path.Path("/exp/team/group/task/stop"), s.doctorExp.StopGroupTask, nil)
	router.POST(path.Path("/exp/team/manage/group/dict"), s.doctorExp.ListGroupDict, nil)
	router.POST(path.Path("/exp/team/manage/team/dict"), s.doctorExp.ListTeamDict, nil)

	// 训练营
	router.POST(path.Path("/exp/camp/add"), s.doctorExp.AddCamp, nil)
	router.POST(path.Path("/exp/camp/list/page"), s.doctorExp.ListCampPage, nil)
	router.POST(path.Path("/exp/camp/dict"), s.doctorExp.ListCampDict, nil)
	router.POST(path.Path("/exp/camp/get"), s.doctorExp.GetCamp, nil)
	router.POST(path.Path("/exp/camp/update"), s.doctorExp.UpdateCamp, nil)
	router.POST(path.Path("/exp/camp/delete"), s.doctorExp.DeleteCamp, nil)

}

func (s *innerRouter) mapDefaultSite(path types.Path, router *router.Router, root string) {
	router.ServeFiles(path.Path("/*filepath"), http.Dir(root), nil)
}
