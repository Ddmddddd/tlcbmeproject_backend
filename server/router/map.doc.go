package router

import (
	"tlcbme_project/server/controller/doc"
	"github.com/ktpswjz/httpserver/router"
	"github.com/ktpswjz/httpserver/types"
	"net/http"
)

type docController struct {
	adminDoc *doc.Doc
}

func (s *innerRouter) mapDocApi(path types.Path, router *router.Router) {
	s.adminDoc = doc.NewDoc(s.cfg, s.GetLog(), s.defaultToken, router.Doc, s)
	s.adminDoc.AdminToken = s.adminToken
	s.adminDoc.PatientToken = s.patientToken
	s.adminDoc.AdminAuthenticate = s.adminAuthentication.Authenticate
	s.adminDoc.DefaultAuthenticate = s.doctorBusiness.Authentication().Authenticate()
	s.adminDoc.DefaultLogined = s.doctorBusiness.Authentication().Logined()
	s.adminDoc.PateintAuthenticate = s.patientBusiness.Authentication().Authenticate()
	s.adminDoc.PatientLogined = s.patientBusiness.Authentication().Logined()

	// 获取接口目录信息
	router.POST(path.Path("/catalog/tree"), s.adminDoc.GetCatalogTree, nil)

	// 获取接口定义信息
	router.POST(path.Path("/function/:id"), s.adminDoc.GetFunction, nil)

	// 创建接口访问凭证
	router.POST(path.Path("/token/create"), s.adminDoc.CreateToken, nil)
	router.POST(path.Path("/jwt/create"), s.adminDoc.CreateJwt, nil)
}

func (s *innerRouter) mapDocSite(path types.Path, router *router.Router, root string) {
	router.ServeFiles(path.Path("/*filepath"), http.Dir(root), nil)
}
