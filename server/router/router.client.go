package router

import (
	"tlcbme_project/data/entity/sqldb"
	"tlcbme_project/data/model"
	"tlcbme_project/data/model/manage"
	"tlcbme_project/data/model/platform"
	"tlcbme_project/server/config"
	"crypto/rand"
	"encoding/json"
	"fmt"
	"github.com/ktpswjz/httpserver/http/client"
	"github.com/ktpswjz/httpserver/security/hash"
	"github.com/ktpswjz/httpserver/security/jwt"
	"github.com/ktpswjz/httpserver/types"
	"github.com/satori/go.uuid"
	"io"
	"net/url"
	"time"
)

func (s *innerRouter) Post(address string, argument interface{}, headers ...client.Header) ([]byte, int, error) {
	now := time.Now()
	logEntity := &sqldb.ClientLog{
		Method:    "POST",
		StartTime: &now,
		Result:    0,
	}
	u, err := url.Parse(address)
	if err == nil {
		logEntity.Schema = u.Scheme
		logEntity.Host = u.Host
		logEntity.Port = u.Port()
		logEntity.Uri = u.Path

		query := u.Query()
		if len(query) > 0 {
			params := make([]*types.Query, 0)
			for k, v := range query {
				param := &types.Query{Key: k}
				if len(v) > 0 {
					param.Value = v[0]
				}
				params = append(params, param)
			}
			logEntity.Param, _ = json.Marshal(params)
		}
	}

	defer func(entity *sqldb.ClientLog) {
		go func(entity *sqldb.ClientLog) {
			s.saveClientLog(entity)
		}(entity)
	}(logEntity)
	clt := &client.Client{}
	input, output, _, code, err := clt.PostJson(address, argument, headers...)
	logEntity.Output = output
	logEntity.Input = input
	if err != nil {
		errMsg := err.Error()
		logEntity.ErrorMessage = &errMsg
		return nil, code, err
	}
	logEntity.Result = uint64(code)

	return output, code, nil
}

func (s *innerRouter) ManagePost(providerType config.ManagementProviderType, uri string, argument interface{}) (*manage.Result, error) {
	baseUrl := ""
	secret := ""
	algorithm := ""
	expirationSeconds := int64(60)
	jwtPayload := &model.JwtPayload{
		ID: s.generateGuid(),
	}
	if providerType == config.ManagementProviderTypeHbp {
		baseUrl = s.cfg.Management.Providers.Hbp.Api.BaseUrl
		secret = s.cfg.Management.Providers.Hbp.Auth.Secret
		algorithm = s.cfg.Management.Providers.Hbp.Auth.Algorithm
		expirationSeconds = 60 * s.cfg.Management.Providers.Hbp.Auth.Expiration
		jwtPayload.Issuer = s.cfg.Management.Providers.Hbp.ID
		jwtPayload.Subject = s.cfg.Management.Providers.Hbp.Auth.ID
		jwtPayload.Audience = s.cfg.Management.Providers.Hbp.ID
	} else if providerType == config.ManagementProviderTypeDm {
		baseUrl = s.cfg.Management.Providers.Dm.Api.BaseUrl
		secret = s.cfg.Management.Providers.Dm.Auth.Secret
		algorithm = s.cfg.Management.Providers.Dm.Auth.Algorithm
		expirationSeconds = 60 * s.cfg.Management.Providers.Dm.Auth.Expiration
		jwtPayload.Issuer = s.cfg.Management.Providers.Dm.ID
		jwtPayload.Subject = s.cfg.Management.Providers.Dm.Auth.ID
		jwtPayload.Audience = s.cfg.Management.Providers.Dm.ID
	} else {
		return nil, fmt.Errorf("提供者类型(providerType=%d)无效", providerType)
	}
	s.LogTrace("ProviderType:", providerType, "; baseUrl:", baseUrl, "; uri:", uri)

	now := time.Now()
	jwtPayload.ExpirationTime = now.Unix() + expirationSeconds
	jwtPayload.NotBefore = now.Unix() - expirationSeconds
	jwtPayload.IssuedAt = now.Unix()

	jwtEncoding := jwt.NewEncoding(algorithm, secret)
	token, err := jwtEncoding.Encode(jwtPayload)
	if err != nil {
		return nil, err
	}

	address := fmt.Sprintf("%s%s?jwt=%s", baseUrl, uri, token)
	data, code, err := s.Post(address, argument)
	if err != nil {
		return nil, err
	}

	result := &manage.Result{
		Code: code,
	}
	if code == 200 {
		err = json.Unmarshal(data, result)
		if err != nil {
			return nil, err
		}
	} else {
		result.Message = string(data)
		return result, fmt.Errorf("response code: %d", code)
	}

	return result, nil
}

func (s *innerRouter) ManageBusinessPost(businessName string, patientId uint64, providerType config.ManagementProviderType, uri string, argument interface{}) (*manage.Result, error) {
	errMsg := ""
	result, err := s.ManagePost(providerType, uri, argument)
	if err == nil {
		if result.Code == 0 {
			return result, nil
		} else {
			errMsg = fmt.Sprintf("%d-%s", result.Code, result.Message)
		}
	} else {
		errMsg = err.Error()
	}

	go func(providerType config.ManagementProviderType, patientId uint64, errMsg, businessName, uri string, argument interface{}) {
		now := time.Now()
		dbEntity := &sqldb.ManageApiFailRecord{
			PatientID:    patientId,
			ProviderType: uint64(providerType),
			BusinessName: businessName,
			ErrMsg:       errMsg,
			DateTime:     &now,
			Uri:          uri,
		}
		if argument != nil {
			dbEntity.Input, _ = json.Marshal(argument)
		}
		s.sqlDatabase.InsertSelective(dbEntity)
	}(providerType, patientId, errMsg, businessName, uri, argument)

	return result, err
}

func (s *innerRouter) PlatformPost(uri string, argument interface{}) (*platform.Result, error) {
	baseUrl := s.cfg.Platform.Providers.Api.BaseUrl
	address := fmt.Sprintf("%s%s", baseUrl, uri)

	headers := make([]client.Header, 0)
	authValue := fmt.Sprintf("Basic %s", s.cfg.Platform.Providers.Token)
	timestamp := fmt.Sprintf("%d", time.Now().Unix())
	nonce, _ := uuid.NewV4()
	nonce2 := fmt.Sprintf("%s", nonce)
	sign := fmt.Sprintf("timestamp=%s&nonce=%s", timestamp, nonce2)
	sign2, _ := hash.Hash(sign, uint64(21))

	headers = append(headers, client.Header{"Authorization", authValue})
	headers = append(headers, client.Header{"timestamp", timestamp})
	headers = append(headers, client.Header{"nonce", nonce2})
	headers = append(headers, client.Header{"sign", sign2})
	data, code, err := s.Post(address, argument, headers...)
	if err != nil {
		return nil, err
	}

	result := &platform.Result{
		Code: code,
	}
	if code == 200 {
		err = json.Unmarshal(data, result)
		if err != nil {
			return nil, err
		}
	} else {
		result.Msg = string(data)
		return result, fmt.Errorf("response code: %d", code)
	}

	return result, nil
}

func (s *innerRouter) PlatformBusinessPost(businessName string, patientId uint64, uri string, argument interface{}) (*platform.Result, error) {
	errMsg := ""
	result, err := s.PlatformPost(uri, argument)
	if err == nil {
		if result.Code == 200 && result.Msg == "" {
			return result, nil
		} else {
			errMsg = fmt.Sprintf("%d-%s", result.Code, result.Msg)
		}
	} else {
		errMsg = err.Error()
	}

	go func(patientId uint64, errMsg, datatype string, uri string, argument interface{}) {
		now := time.Now()
		dbEntity := &sqldb.PlatformApiFailRecord{
			PatientID:    patientId,
			BusinessName: businessName,
			ErrMsg:       errMsg,
			DateTime:     &now,
			Uri:          uri,
		}
		if argument != nil {
			dbEntity.Input, _ = json.Marshal(argument)
		}
		s.sqlDatabase.InsertSelective(dbEntity)
	}(patientId, errMsg, businessName, uri, argument)

	return result, err
}

func (s *innerRouter) Get(address string, argument interface{}) ([]byte, int, error) {
	now := time.Now()
	logEntity := &sqldb.ClientLog{
		Method:    "Get",
		StartTime: &now,
		Result:    0,
	}
	u, err := url.Parse(address)
	if err == nil {
		logEntity.Schema = u.Scheme
		logEntity.Host = u.Host
		logEntity.Port = u.Port()
		logEntity.Uri = u.Path

		query := u.Query()
		if len(query) > 0 {
			params := make([]*types.Query, 0)
			for k, v := range query {
				param := &types.Query{Key: k}
				if len(v) > 0 {
					param.Value = v[0]
				}
				params = append(params, param)
			}
			logEntity.Param, _ = json.Marshal(params)
		}
	}

	defer func(entity *sqldb.ClientLog) {
		go func(entity *sqldb.ClientLog) {
			s.saveClientLog(entity)
		}(entity)
	}(logEntity)

	clt := &client.Client{}

	input, output, _, code, err := clt.Get(address, argument)
	logEntity.Output = output
	logEntity.Input = input
	if err != nil {
		errMsg := err.Error()
		logEntity.ErrorMessage = &errMsg
		return nil, code, err
	}
	logEntity.Result = uint64(code)

	return output, code, nil
}

func (s *innerRouter) generateGuid() string {
	uuid2 := make([]byte, 16)
	n, err := io.ReadFull(rand.Reader, uuid2)
	if n != len(uuid2) || err != nil {
		return ""
	}

	uuid2[8] = uuid2[8]&^0xc0 | 0x80
	uuid2[6] = uuid2[6]&^0xf0 | 0x40

	return fmt.Sprintf("%x%x%x%x%x", uuid2[0:4], uuid2[4:6], uuid2[6:8], uuid2[8:10], uuid2[10:])
}
