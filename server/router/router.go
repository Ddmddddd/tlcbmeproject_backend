package router

import (
	"fmt"
	"github.com/ktpswjz/httpserver/document"
	"github.com/ktpswjz/httpserver/router"
	"github.com/ktpswjz/httpserver/types"
	"net/http"
	"strings"
	"time"
	"tlcbme_project/business/doctor"
	"tlcbme_project/business/patient"
	"tlcbme_project/data/model"
	"tlcbme_project/database"
	"tlcbme_project/database/memory"
	"tlcbme_project/database/mysql"
	"tlcbme_project/errors"
	"tlcbme_project/server/config"
	"tlcbme_project/server/notify"
)

const (
	AuthApi      = "/auth"
	DefaultSite  = "/doctor"
	DefaultApi   = "/api"
	DocSite      = "/doc"
	DocApi       = "/doc.api"
	AdminSite    = "/admin"
	AdminApi     = "/admin.api"
	AppSite      = "/app"
	PatientSite  = "/patient"
	PatientApi   = "/patient.api"
	CallbackSite = "/callback"
	CallbackApi  = "/callback.api"
	PublicApi    = "/public"
	DownloadSite = "/download"
	OauthApi     = "/api/oauth/user/info"
	Instrument   = "/api/instrument/blood/pressure/record/create"
)

type Router interface {
	Map(router *router.Router)
	PreRouting(w http.ResponseWriter, r *http.Request, a router.Assistant) bool
	PostRouting(a router.Assistant)
}

func NewRouter(cfg *config.Config, log types.Log) (Router, error) {
	adminToken, err := memory.NewToken(cfg.Site.Admin.Api.Token.Expiration, log)
	if err != nil {
		return nil, err
	}
	defaultToken, err := memory.NewToken(cfg.Site.Doctor.Api.Token.Expiration, log)
	if err != nil {
		return nil, err
	}
	patientToken, err := memory.NewToken(cfg.Site.Patient.Api.Token.Expiration, log)
	if err != nil {
		return nil, err
	}
	sqlDatabase := mysql.NewDatabase(&cfg.Database)
	sqlDbVer, err := sqlDatabase.Test()
	if err != nil {
		log.Warning("connect to sql database fail: ", err)
	} else {
		log.Info("sql database version: ", sqlDbVer)
	}

	nSqlDatabase := mysql.NewDatabase(&cfg.NutritionDatabase)
	nSqlDbVer, err := nSqlDatabase.Test()
	if err != nil {
		log.Warning("connect to sql database fail: ", err)
	} else {
		log.Info("sql database version: ", nSqlDbVer)
	}

	instance := &innerRouter{cfg: cfg}
	instance.SetLog(log)
	instance.adminToken = adminToken
	instance.defaultToken = defaultToken
	instance.patientToken = patientToken
	instance.callbackJwt = memory.NewJwt(cfg.Management.Providers.Hbp.Auth.Expiration, log)
	instance.sqlDatabase = sqlDatabase
	instance.notifyChannels = notify.NewChannelCollection()

	instance.doctorBusiness = doctor.NewBusiness(cfg, log, sqlDatabase, nSqlDatabase, defaultToken, instance, instance.notifyChannels)
	instance.patientBusiness = patient.NewBusiness(cfg, log, sqlDatabase, nSqlDatabase, patientToken, instance, instance.notifyChannels)

	return instance, nil
}

type innerRouter struct {
	types.Base

	cfg            *config.Config
	adminToken     memory.Token
	defaultToken   memory.Token
	patientToken   memory.Token
	callbackJwt    memory.Jwt
	sqlDatabase    database.SqlDatabase
	notifyChannels notify.ChannelCollection

	doctorBusiness  doctor.Business
	patientBusiness patient.Business

	// controllers
	authController
	defaultController
	adminController
	docController
	patientController
	callbackController
	publicController
}

func (s *innerRouter) Map(router *router.Router) {
	router.Doc = document.NewDocument(s.cfg.Site.Doc.Enable, s.GetLog())
	router.NotFound2 = s

	s.mapPublicApi(types.Path{Prefix: PublicApi}, router)
	s.mapAuthApi(types.Path{Prefix: AuthApi}, router)
	s.mapDefaultApi(types.Path{Prefix: DefaultApi}, router)
	s.mapDocSite(types.Path{Prefix: DefaultSite}, router, s.cfg.Site.Doctor.Root)
	s.mapAppSite(types.Path{Prefix: AppSite}, router, s.cfg.Site.App.Root)

	s.mapPatientApi(types.Path{Prefix: PatientApi}, router)
	s.mapPatientSite(types.Path{Prefix: PatientSite}, router, s.cfg.Site.Patient.Root)
	s.mapCallbackApi(types.Path{Prefix: CallbackApi}, router)
	s.mapCallbackSite(types.Path{Prefix: CallbackSite}, router, s.cfg.Site.Patient.Root)
	s.mapDownloadSite(types.Path{Prefix: DownloadSite}, router, s.cfg.Install.Root)

	if s.cfg.Site.Admin.Enable {
		s.mapAdminApi(types.Path{Prefix: AdminApi}, router)
		s.mapAdminSite(types.Path{Prefix: AdminSite}, router, s.cfg.Site.Admin.Root)
		s.LogInfo("admin is enabled")
	}

	if s.cfg.Site.Doc.Enable {
		s.mapDocApi(types.Path{Prefix: DocApi}, router)
		s.mapDocSite(types.Path{Prefix: DocSite}, router, s.cfg.Site.Doc.Root)
		s.LogInfo("doc is enabled")
		router.Doc.GenerateCatalogTree()
	}
}

func (s *innerRouter) PreRouting(w http.ResponseWriter, r *http.Request, a router.Assistant) bool {
	path := r.URL.Path
	if s.IsApi(path) {
		// enable across access
		if r.Method == "OPTIONS" {
			w.Header().Add("Access-Control-Allow-Origin", "*")
			w.Header().Set("Access-Control-Allow-Headers", "content-type,token")
			return true
		}
	}
	if path == OauthApi {
		return false
	}
	if path == Instrument {
		a.SetRecord(true)
		return false
	}
	if s.IsDefaultApi(path) {
		token := a.Token()
		if token == "" {
			a.Error(errors.AuthNoToken)
			return true
		}
		tokenEntity, err := s.defaultToken.Get(token)
		if err != nil || tokenEntity == nil {
			a.Error(errors.AuthTokenInvalid, err)
			return true
		}
		if a.RIP() != tokenEntity.LoginIP {
			a.Error(errors.AuthTokenIllegal)
			return true
		}
		tokenEntity.ActiveTime = time.Now()
		a.SetRecord(true)
	} else if s.IsPatientApi(path) {
		token := a.Token()
		if token == "" {
			a.Error(errors.AuthNoToken)
			return true
		}
		tokenEntity, err := s.patientToken.Get(token)
		if err != nil || tokenEntity == nil {
			a.Error(errors.AuthTokenInvalid, err)
			return true
		}
		if a.RIP() != tokenEntity.LoginIP {
			a.Error(errors.AuthTokenIllegal)
			return true
		}
		tokenEntity.ActiveTime = time.Now()
		a.SetRecord(true)
	} else if s.IsAdminApi(path) {
		token := a.Token()
		if token == "" {
			a.Error(errors.AuthNoToken)
			return true
		}
		tokenEntity, err := s.adminToken.Get(token)
		if err != nil || tokenEntity == nil {
			a.Error(errors.AuthTokenInvalid, err)
			return true
		}
		if a.RIP() != tokenEntity.LoginIP {
			a.Error(errors.AuthTokenIllegal)
			return true
		}
		tokenEntity.ActiveTime = time.Now()
	} else if s.IsCallbackApi(path) {
		a.SetRecord(true)
		jwt := a.JsonWebToken()
		if jwt == "" {
			a.Error(errors.AuthNoToken)
			return true
		}

		payload := &model.JwtPayload{}
		err := s.decodeJwt(jwt, payload)
		if err != nil {
			a.Error(errors.AuthTokenInvalid, err)
			return true
		}
		now := time.Now()
		if time.Unix(payload.ExpirationTime, 0).Before(now) {
			a.Error(errors.AuthTokenInvalid, fmt.Errorf("已过期"))
			return true
		}
		if time.Unix(payload.NotBefore, 0).After(now) {
			a.Error(errors.AuthTokenInvalid, fmt.Errorf("未生效"))
			return true
		}
		if len(payload.ID) < 1 {
			a.Error(errors.AuthTokenInvalid, fmt.Errorf("缺少jti"))
			return true
		}
		err = s.callbackJwt.Add(payload.ID, payload.ExpirationTime)
		if err != nil {
			a.Error(errors.AuthTokenInvalid, err)
			return true
		}
		a.Set("jwt.payload", payload)
	}

	// default to default site
	if "/" == r.URL.Path || "" == r.URL.Path || DefaultSite == r.URL.Path {
		//.URL.Path = fmt.Sprint(DefaultSite, "/")
		redirectUrl := fmt.Sprintf("%s://%s%s/", a.Schema(), r.Host, DefaultSite)
		http.Redirect(w, r, redirectUrl, http.StatusMovedPermanently)
		return true
	} else if r.Method == "GET" {
		if r.URL.Path == AdminSite {
			redirectUrl := fmt.Sprintf("%s://%s%s/", a.Schema(), r.Host, AdminSite)
			http.Redirect(w, r, redirectUrl, http.StatusMovedPermanently)
			return true
		} else if r.URL.Path == DocSite {
			redirectUrl := fmt.Sprintf("%s://%s%s/", a.Schema(), r.Host, DocSite)
			http.Redirect(w, r, redirectUrl, http.StatusMovedPermanently)
			return true
		}
	}

	return false
}

func (s *innerRouter) PostRouting(a router.Assistant) {
	s.saveApiLog(a)
}

func (s *innerRouter) IsApi(path string) bool {
	return strings.HasPrefix(path, AuthApi) ||
		strings.HasPrefix(path, DefaultApi) ||
		strings.HasPrefix(path, AdminApi) ||
		strings.HasPrefix(path, DocApi) ||
		strings.HasPrefix(path, PatientApi) ||
		strings.HasPrefix(path, CallbackApi) ||
		strings.HasPrefix(path, PublicApi)
}

func (s *innerRouter) IsDefaultApi(path string) bool {
	return strings.HasPrefix(path, DefaultApi)
}

func (s *innerRouter) IsAdminApi(path string) bool {
	return strings.HasPrefix(path, AdminApi)
}

func (s *innerRouter) IsPatientApi(path string) bool {
	return strings.HasPrefix(path, PatientApi)
}

func (s *innerRouter) IsCallbackApi(path string) bool {
	return strings.HasPrefix(path, CallbackApi)
}
