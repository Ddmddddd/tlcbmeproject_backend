package authentication

import (
	"tlcbme_project/server/config"
	"errors"
	"strings"
)

type Admin struct {
	Config *config.SiteAdmin
}

func (s *Admin) Authenticate(account, password string) (uint64, error) {
	if s.Config == nil {
		return 0, errors.New("internal error: nil config for admin site")
	}

	var user *config.SiteAdminUser = nil
	userCount := len(s.Config.Users)
	for index := 0; index < userCount; index++ {
		if account == strings.ToLower(s.Config.Users[index].Account) {
			user = &s.Config.Users[index]
			break
		}
	}

	if user != nil {
		if user.Password != password {
			return 0, errors.New("invalid password")
		} else {
			return 0, nil
		}
	}

	if !s.Config.Ldap.Enable {
		return 0, errors.New("account not exist")
	}

	ldap := &Ldap{
		Host: s.Config.Ldap.Host,
		Port: s.Config.Ldap.Port,
		Base: s.Config.Ldap.Base,
	}

	return 0, ldap.Authenticate(account, password)
}
