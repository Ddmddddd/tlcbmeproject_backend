package notify

import "github.com/ktpswjz/httpserver/types"

type Message struct {
	BusinessID int         `json:"businessID"`
	NotifyID   int         `json:"notifyID"`
	Time       types.Time  `json:"time"`
	Actor      Actor       `json:"actor"`
	Data       interface{} `json:"data"`
}
