package notify

const (
	BusinessAdmin   = 1
	BusinessDoctor  = 2
	BusinessPatient = 3
)

const (
	AdminNewDoctor = 1
)

const (
	DoctorNewPatient            = 1
	DoctorNewPatientAlert       = 2
	DoctorAuditPatient          = 3
	DoctorUpdateBloodPressure   = 4
	DoctorUpdateBloodGlucose    = 5
	DoctorUpdateWeight          = 6
	DoctorUpdateDrug            = 7
	DoctorUpdateMangeStatus     = 8
	DoctorUpdateAssessment      = 9
	DoctorUpdateFollowup        = 10
	DoctorNewChatMessage        = 11
	DoctorChatMessageRead       = 12
	DoctorCountUnReadMessage    = 13
	DoctorUpdatePatientBaseInfo = 14
	DoctorNewSmsRecord          = 15
	DoctorExpBarrierInfo        = 16
	DoctorExpActionPlanInfo     = 17
	DoctorExpCommentRead        = 18
)

const (
	PatientNewAlert        = 1
	PatientNewChatMessage  = 2
	PatientChatMessageRead = 3
)
