package controller

import (
	"tlcbme_project/data/enum"
	"fmt"
	"github.com/ktpswjz/httpserver/document"
	"github.com/ktpswjz/httpserver/router"
	"net/http"
)

type Enum struct {
	GetCatalog func(a document.Assistant) document.Catalog
}

func (s *Enum) getDoc(a document.Assistant, catalog document.Catalog, name string, example enum.NumberEnum) document.Function {
	function := a.CreateFunction(fmt.Sprintf("获取%s列表", name))
	function.SetNote(fmt.Sprintf("获取所有%s信息", name))
	function.SetOutputExample([]enum.NumberEnum{
		example,
	})

	if catalog != nil {
		catalog.SetFunction(function)
	}

	return function
}
func (s *Enum) getCatalog(a document.Assistant) document.Catalog {
	if s.GetCatalog != nil {
		return s.GetCatalog(a)
	} else {
		return a.CreateCatalog("枚举信息", "枚举值相关接口")
	}
}

func (s *Enum) ListAccountStatus(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	a.Success(enum.AccountStatuses.All())
}
func (s *Enum) ListAccountStatusDoc(a document.Assistant) document.Function {
	return s.getDoc(a, s.getCatalog(a), "账号状态", enum.AccountStatuses.Normal())
}
func (s *Enum) ListAccountRights(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	a.Success(enum.AccountRights.All())
}
func (s *Enum) ListAccountRightsDoc(a document.Assistant) document.Function {
	return s.getDoc(a, s.getCatalog(a), "查看机构全部患者", enum.AccountRights.ViewOrgAllPatients())
}

func (s *Enum) ListSex(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	a.Success(enum.Sexes.All())
}
func (s *Enum) ListSexDoc(a document.Assistant) document.Function {
	return s.getDoc(a, s.getCatalog(a), "性别", enum.Sexes.Male())
}

func (s *Enum) ListUserType(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	a.Success(enum.DoctorUserTypes.All())
}
func (s *Enum) ListUserTypeDoc(a document.Assistant) document.Function {
	return s.getDoc(a, s.getCatalog(a), "用户类别", enum.DoctorUserTypes.Doctor())
}

func (s *Enum) ListVerifiedStatus(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	a.Success(enum.VerifiedStatuses.All())
}
func (s *Enum) ListVerifiedStatusDoc(a document.Assistant) document.Function {
	return s.getDoc(a, s.getCatalog(a), "认证状态", enum.VerifiedStatuses.NotPass())
}

func (s *Enum) ListManageStatus(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	a.Success(enum.ManageStatuses.All())
}
func (s *Enum) ListManageStatusDoc(a document.Assistant) document.Function {
	return s.getDoc(a, s.getCatalog(a), "管理状态", enum.ManageStatuses.InManagement())
}

func (s *Enum) ListMeasurePlace(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	a.Success(enum.MeasurePlaces.All())
}
func (s *Enum) ListMeasurePlaceDoc(a document.Assistant) document.Function {
	return s.getDoc(a, s.getCatalog(a), "测量场所", enum.MeasurePlaces.Unknown())
}

func (s *Enum) ListMeasureBodyPart(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	a.Success(enum.MeasureBodyParts.All())
}
func (s *Enum) ListMeasureBodyPartDoc(a document.Assistant) document.Function {
	return s.getDoc(a, s.getCatalog(a), "测量部位", enum.MeasureBodyParts.Unknown())
}

func (s *Enum) ListAlertStatus(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	a.Success(enum.AlertStatuses.All())
}
func (s *Enum) ListAlertStatusDoc(a document.Assistant) document.Function {
	return s.getDoc(a, s.getCatalog(a), "预警处理状态", enum.AlertStatuses.Unprocessed())
}

func (s *Enum) ListAlertProcessMode(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	a.Success(enum.AlertProcessModes.All())
}
func (s *Enum) ListAlertProcessModeDoc(a document.Assistant) document.Function {
	return s.getDoc(a, s.getCatalog(a), "预警处理方式", enum.AlertProcessModes.Followup())
}
