package patient

import (
	"github.com/ktpswjz/httpserver/document"
	"github.com/ktpswjz/httpserver/router"
	"github.com/ktpswjz/httpserver/types"
	"net/http"
	"time"
	doctorModel "tlcbme_project/data/model/doctor"
	"tlcbme_project/errors"
)

func (s *Data) DeleteBloodPressureRecord(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.BloodPressureRecordForAppEx{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}

	now := types.Time(time.Now())
	if argument.MeasureDateTime == nil {
		argument.MeasureDateTime = &now
	}
	argument.PatientID = s.getLoginUserId(a)

	be := s.patientBusiness.Data().DeleteBloodPressureRecord(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(nil)
}

func (s *Data) DeleteBloodPressureRecordDoc(a document.Assistant) document.Function {
	heartRate := uint64(65)
	now := types.Time(time.Now())

	function := a.CreateFunction("删除血压记录")
	function.SetNote("删除患者血压记录信息")
	function.SetInputExample(&doctorModel.BloodPressureRecordForApp{
		SerialNo:          11,
		SystolicPressure:  110,
		DiastolicPressure: 78,
		HeartRate:         &heartRate,
		Memo:              "服用硝苯地平控释片2小时后",
		MeasureDateTime:   &now,
		Type:              1,
	})
	function.SetOutputExample(nil)

	s.setDocFun(a, function)

	return function
}

func (s *Data) DeleteBloodGlucoseRecord(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.BloodGlucoseRecordForAppEx{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}

	now := types.Time(time.Now())
	if argument.MeasureDateTime == nil {
		argument.MeasureDateTime = &now
	}
	argument.PatientID = s.getLoginUserId(a)

	be := s.patientBusiness.Data().DeleteBloodGlucoseRecord(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(nil)
}

func (s *Data) DeleteBloodGlucoseRecordDoc(a document.Assistant) document.Function {
	now := types.Time(time.Now())

	function := a.CreateFunction("删除血糖记录")
	function.SetNote("删除患者血糖记录信息")
	function.SetInputExample(&doctorModel.BloodGlucoseRecordForApp{
		SerialNo:        11,
		BloodGlucose:    7.8,
		Memo:            "",
		MeasureDateTime: &now,
		TimePoint:       "晨起空腹",
	})
	function.SetOutputExample(nil)

	s.setDocFun(a, function)

	return function
}

func (s *Data) DeleteWeightRecord(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.WeightRecordForAppEx{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}

	now := types.Time(time.Now())
	if argument.MeasureDateTime == nil {
		argument.MeasureDateTime = &now
	}
	argument.PatientID = s.getLoginUserId(a)

	be := s.patientBusiness.Data().DeleteWeightRecord(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(nil)
}

func (s *Data) DeleteWeightRecordDoc(a document.Assistant) document.Function {
	now := types.Time(time.Now())

	function := a.CreateFunction("删除体重记录")
	function.SetNote("删除患者体重记录信息")
	function.SetInputExample(&doctorModel.WeightRecordForApp{
		SerialNo:        11,
		Weight:          60.5,
		Memo:            "有氧运动半小时后",
		MeasureDateTime: &now,
		Type:            1,
	})
	function.SetOutputExample(nil)

	s.setDocFun(a, function)

	return function
}

func (s *Data) DeleteDrugRecord(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.DrugRecordForAppEx{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}

	now := types.Time(time.Now())
	if argument.UseDateTime == nil {
		argument.UseDateTime = &now
	}
	argument.PatientID = s.getLoginUserId(a)

	be := s.patientBusiness.Data().DeleteDrugRecord(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(nil)
}

func (s *Data) DeleteDrugRecordDoc(a document.Assistant) document.Function {
	now := types.Time(time.Now())

	function := a.CreateFunction("删除服药记录")
	function.SetNote("删除患者服药记录信息")
	function.SetInputExample(&doctorModel.DrugRecordForApp{
		SerialNo:    "11,12",
		DrugName:    "硝苯地平,依普纳利",
		Dosage:      "5mg,10mg",
		Memo:        "",
		UseDateTime: &now,
		Type:        1,
	})
	function.SetOutputExample("11,12")

	s.setDocFun(a, function)

	return function
}

func (s *Data) DeleteDietRecord(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.DietRecordForAppEx{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}

	now := types.Time(time.Now())
	if argument.HappenDateTime == nil {
		argument.HappenDateTime = &now
	}
	argument.PatientID = s.getLoginUserId(a)

	be := s.patientBusiness.Data().DeleteDietRecord(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(nil)
}

func (s *Data) DeleteDietRecordDoc(a document.Assistant) document.Function {
	now := types.Time(time.Now())

	function := a.CreateFunction("删除饮食记录")
	function.SetNote("删除患者饮食记录信息")
	function.SetInputExample(&doctorModel.DietRecordForApp{
		SerialNo:       "11,12",
		Kinds:          "主食,肉类",
		Appetite:       "150,60",
		Memo:           "",
		HappenDateTime: &now,
		Type:           1,
	})
	function.SetOutputExample(nil)

	s.setDocFun(a, function)

	return function
}

func (s *Data) DeleteSportRecord(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.SportRecordForAppEx{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}

	now := types.Time(time.Now())
	if argument.HappenDateTime == nil {
		argument.HappenDateTime = &now
	}
	argument.PatientID = s.getLoginUserId(a)

	be := s.patientBusiness.Data().DeleteSportRecord(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(nil)
}

func (s *Data) DeleteSportRecordDoc(a document.Assistant) document.Function {
	now := types.Time(time.Now())

	function := a.CreateFunction("删除运动记录")
	function.SetNote("删除患者运动记录信息")
	function.SetInputExample(&doctorModel.SportRecordForApp{
		SerialNo:       11,
		SportsType:     "步行",
		DurationTime:   0,
		Intensity:      "低",
		StepCount:      3699,
		Memo:           "",
		HappenDateTime: &now,
	})
	function.SetOutputExample(nil)

	s.setDocFun(a, function)

	return function
}

func (s *Data) DeleteDiscomfortRecord(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.DiscomfortRecordForAppEx{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}

	now := types.Time(time.Now())
	if argument.HappenDateTime == nil {
		argument.HappenDateTime = &now
	}
	argument.PatientID = s.getLoginUserId(a)

	be := s.patientBusiness.Data().DeleteDiscomfortRecord(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(nil)
}

func (s *Data) DeleteDiscomfortRecordDoc(a document.Assistant) document.Function {
	now := types.Time(time.Now())

	function := a.CreateFunction("删除不适记录")
	function.SetNote("删除患者不适记录信息")
	function.SetInputExample(&doctorModel.DiscomfortRecordForApp{
		SerialNo:       11,
		Discomfort:     "剧烈头痛,恶心呕吐",
		Memo:           "",
		HappenDateTime: &now,
	})
	function.SetOutputExample(nil)

	s.setDocFun(a, function)

	return function
}
