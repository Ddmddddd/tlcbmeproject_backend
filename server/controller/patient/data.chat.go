package patient

import (
	doctorModel "tlcbme_project/data/model/doctor"
	"tlcbme_project/errors"
	"github.com/ktpswjz/httpserver/document"
	"github.com/ktpswjz/httpserver/router"
	"github.com/ktpswjz/httpserver/types"
	"net/http"
	"time"
)

func (s *Data) SendChatMessage(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.DoctorPatientChatMsg{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}

	now := types.Time(time.Now())
	if argument.MsgDateTime == nil {
		argument.MsgDateTime = &now
	}
	argument.SenderID = s.getLoginUserId(a)

	data, be := s.patientBusiness.Data().SendChatMessage(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Data) SendChatMessageDoc(a document.Assistant) document.Function {
	now := types.Time(time.Now())

	function := a.CreateFunction("发送医患沟通聊天信息")
	function.SetNote("发送医患沟通的聊天信息,成功时返回信息序列号")
	function.SetInputExample(&doctorModel.DoctorPatientChatMsgData{
		ReceiverID:  1342,
		MsgContent:  "你好",
		MsgDateTime: &now,
	})
	function.SetOutputExample(11)

	s.setDocFun(a, function)

	return function
}

func (s *Data) SendChatMessageRead(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.DoctorPatientChatMsgRead{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}
	argument.SenderID = s.getLoginUserId(a)

	be := s.patientBusiness.Data().SendChatMessageRead(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(nil)
}

func (s *Data) SendChatMessageReadDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("发送医患沟通信息已阅读")
	function.SetNote("发送医患沟通的聊天信息已被阅读的消息")
	function.SetInputExample(&doctorModel.DoctorPatientChatMsgReadData{
		ReceiverID: 1342,
		MsgSerialNos: []uint64{
			11,
			12,
		},
	})
	function.SetOutputExample(nil)

	s.setDocFun(a, function)

	return function
}

func (s *Data) GetChatMessageList(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.ChatMsgUserInfo{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}
	argument.PatientID = s.getLoginUserId(a)

	data, be := s.patientBusiness.Data().GetChatMessageList(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Data) GetChatMessageListDoc(a document.Assistant) document.Function {
	now := types.Time(time.Now())

	function := a.CreateFunction("获取医患沟通的信息")
	function.SetNote("获取患者和选择医生的聊天记录")
	function.SetInputExample(&doctorModel.ChatMsgDoctorID{
		DoctorID: 13,
	})
	function.SetOutputExample([]*doctorModel.DoctorPatientChatMsg{
		{
			SerialNo:    1,
			SenderID:    11,
			ReceiverID:  12,
			MsgContent:  "你好",
			MsgDateTime: &now,
			MsgFlag:     1,
		},
		{
			SerialNo:    1,
			SenderID:    12,
			ReceiverID:  11,
			MsgContent:  "你好，too",
			MsgDateTime: &now,
			MsgFlag:     0,
		},
	})

	s.setDocFun(a, function)

	return function
}

func (s *Data) GetChatDoctorList(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.PatientInfoBase{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InputError, err)
		return
	}
	argument.PatientID = s.getLoginUserId(a)

	data, be := s.patientBusiness.Data().GetChatDoctorList(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Data) GetChatDoctorListDoc(a document.Assistant) document.Function {
	now := types.Time(time.Now())

	function := a.CreateFunction("获取医患沟通的医生列表")
	function.SetNote("获取医患可进行聊天的医生列表")
	function.SetInputExample(nil)
	function.SetOutputExample([]*doctorModel.ChatDoctorWithMsgInfoForApp{
		{
			UserID: 21,
			Name:   "李老师",
			UnReadMsgList: []uint64{
				11,
				12,
			},
			LastMsgContent:  "你好",
			LastMsgDateTime: &now,
		},
		{
			UserID: 22,
			Name:   "王老师",
			UnReadMsgList: []uint64{
				11,
				12,
			},
		},
	})

	s.setDocFun(a, function)

	return function
}
