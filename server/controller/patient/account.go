package patient

import (
	"tlcbme_project/business/doctor"
	"tlcbme_project/business/patient"
	"tlcbme_project/data/enum"
	doctorModel "tlcbme_project/data/model/doctor"
	"tlcbme_project/database/memory"
	"tlcbme_project/errors"
	"tlcbme_project/server/config"
	"github.com/ktpswjz/httpserver/document"
	"github.com/ktpswjz/httpserver/router"
	"github.com/ktpswjz/httpserver/types"
	"net/http"
	"strconv"
	"strings"
	"time"
)

type Account struct {
	doc
}

func NewAccount(cfg *config.Config, log types.Log, dbToken memory.Token, doctorBusiness doctor.Business, patientBusiness patient.Business) *Account {
	instance := &Account{}
	instance.SetLog(log)
	instance.Config = cfg
	instance.DbToken = dbToken
	instance.doctorBusiness = doctorBusiness
	instance.patientBusiness = patientBusiness

	return instance
}

func (s *Account) setDocFun(a document.Assistant, fun document.Function) {
	catalog := s.rootCatalog(a).CreateChild("用户管理", "患者信息相关接口")
	catalog.SetFunction(fun)
}

func (s *Account) ChangePassword(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.PatientUserAuthsChangePassword{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InputError, err)
		return
	}

	oldPassword := argument.OldPassword
	newPassword := argument.NewPassword
	if strings.ToLower(argument.Encryption) == "rsa" {
		decryptedOldPassword, err := a.RandKey().DecryptData(argument.OldPassword)
		if err != nil {
			a.Error(errors.LoginPasswordInvalid, err)
			return
		}
		oldPassword = string(decryptedOldPassword)

		decryptedOldNewPassword, err := a.RandKey().DecryptData(argument.NewPassword)
		if err != nil {
			a.Error(errors.LoginPasswordInvalid, err)
			return
		}
		newPassword = string(decryptedOldNewPassword)
	}

	be := s.patientBusiness.Authentication().ChangePassword(s.getLoginUserId(a), oldPassword, newPassword)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(true)
}

func (s *Account) ChangePasswordDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("修改当前登录用户密码")
	function.SetNote("修改当前登录用户的账号密码")
	function.SetInputExample(&doctorModel.PatientUserAuthsChangePassword{
		OldPassword: "old",
		NewPassword: "new",
		Encryption:  "rsa",
	})
	function.SetOutputExample(true)

	s.setDocFun(a, function)

	return function
}

func (s *Account) ChangePhone(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.PatientUserAuthsChangePhone{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InputError, err)
		return
	}

	be := s.patientBusiness.Authentication().ChangePhoneNumber(s.getLoginUserId(a), argument.PhoneNumber)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(true)
}

func (s *Account) ChangePhoneDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("修改当前登录用户手机号")
	function.SetNote("修改当前登录用户绑定的手机号")
	function.SetInputExample(&doctorModel.PatientUserAuthsChangePhone{
		PhoneNumber: "13768768324",
	})
	function.SetOutputExample(true)
	s.setDocFun(a, function)

	return function
}

func (s *Account) CommitUsabilityStayTimeInfo(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.UsabilityStayTime{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InputError, err)
		return
	}

	data, be := s.patientBusiness.Data().CommitUsabilityStayTimeInfo(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Account) CommitUsabilityStayTimeInfoDoc(a document.Assistant) document.Function {
	now := types.Time(time.Now())
	function := a.CreateFunction("统计用户界面停留时间")
	function.SetNote("提交用户界面停留时间的统计信息")
	function.SetInputExample(&doctorModel.UsabilityStayTime{
		UserName:   "useTest",
		AppId:      "Hy123456789",
		AppType:    "Android",
		AppVersion: "1.0.1.0",
		PageName:   "p_Main",
		EnterTime:  &now,
		ExitTime:   &now,
		StayTime:   "60",
	})
	function.SetOutputExample(11)
	s.setDocFun(a, function)

	return function
}

func (s *Account) CommitUsabilityEventInfo(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.UsabilityEvent{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InputError, err)
		return
	}

	if argument.EventTime == nil {
		now := types.Time(time.Now())
		argument.EventTime = &now
	}

	data, be := s.patientBusiness.Data().CommitUsabilityEventInfo(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Account) CommitUsabilityEventInfoDoc(a document.Assistant) document.Function {
	now := types.Time(time.Now())
	function := a.CreateFunction("统计用户触发事件")
	function.SetNote("提交用户触发事件的统计信息")
	function.SetInputExample(&doctorModel.UsabilityEvent{
		UserName:   "useTest",
		AppId:      "Hy123456789",
		AppType:    "Android",
		AppVersion: "1.0.1.0",
		EventName:  "e_Login",
		EventTime:  &now,
	})
	function.SetOutputExample(11)
	s.setDocFun(a, function)

	return function
}

func (s *Account) CommitUsabilityDeviceInfo(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.UsabilityDevice{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InputError, err)
		return
	}

	if argument.RecordTime == nil {
		now := types.Time(time.Now())
		argument.RecordTime = &now
	}

	data, be := s.patientBusiness.Data().CommitUsabilityDeviceInfo(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Account) CommitUsabilityDeviceInfoDoc(a document.Assistant) document.Function {
	now := types.Time(time.Now())
	function := a.CreateFunction("统计用户使用设备")
	function.SetNote("提交用户使用设备的统计信息")
	function.SetInputExample(&doctorModel.UsabilityDevice{
		UserName:      "useTest",
		AppId:         "Hy123456789",
		DeviceIMEI:    "123UH2938JNNKKLL78293021",
		PhoneBrand:    "XiaoMI",
		SystemVersion: "4.4.4",
		RecordTime:    &now,
	})
	function.SetOutputExample(11)
	s.setDocFun(a, function)

	return function
}

func (s *Account) CommitUsabilityFeedbackInfo(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.UsabilityFeedBack{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InputError, err)
		return
	}

	if argument.RecordTime == nil {
		now := types.Time(time.Now())
		argument.RecordTime = &now
	}

	data, be := s.patientBusiness.Data().CommitUsabilityFeedbackInfo(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Account) CommitUsabilityFeedbackInfoDoc(a document.Assistant) document.Function {
	now := types.Time(time.Now())
	function := a.CreateFunction("统计用户反馈")
	function.SetNote("提交用户反馈的问题")
	function.SetInputExample(&doctorModel.UsabilityFeedBack{
		UserName:    "useTest",
		AppType:     "Android",
		Description: "xxxxxxxx",
		AppVersion:  "1.0.1.0",
		RecordTime:  &now,
		Flag:        0,
		ResultMemo:  "",
	})
	function.SetOutputExample(true)
	s.setDocFun(a, function)

	return function
}

func (s *Account) BindWeChat(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.PatientUserAuthsEx{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InputError, err)
		return
	}

	argument.UserID = s.getLoginUserId(a)
	argument.Status = strconv.FormatUint(enum.BindWeChatStatuses.Normal().Key, 10)

	sno, be := s.patientBusiness.Authentication().PatientBindWeChat(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(sno)
}

func (s *Account) BindWeChatDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("绑定微信")
	function.SetNote("绑定微信")
	function.SetInputExample(&doctorModel.PatientUserAuthsEx{
		IdentityType: "weChat",
		Identitier: "open-id",
	})
	function.SetOutputExample(true)

	s.setDocFun(a, function)

	return function
}

func (s *Account) ReleaseWeChat(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.PatientUserAuthsEx{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InputError, err)
		return
	}

	userID := s.getLoginUserId(a)

	be := s.patientBusiness.Authentication().PatientReleaseWeChat(userID, argument.IdentityType, argument.Identitier)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(true)
}

func (s *Account) ReleaseWeChatDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("解除绑定微信")
	function.SetNote("解除绑定微信")
	function.SetInputExample(&doctorModel.PatientUserAuthsEx{
		IdentityType: "weChat",
		Identitier: "open-id",
	})
	function.SetOutputExample(true)

	s.setDocFun(a, function)

	return function
}

func (s *Account) CheckWeChatStatus(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.PatientUserAuthsEx{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InputError, err)
		return
	}

	argument.UserID = s.getLoginUserId(a)

	data, be := s.patientBusiness.Data().PatientCheckWeChatStatus(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Account) CheckWeChatStatusDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("检查绑定微信状态")
	function.SetNote("检查绑定微信状态")
	function.SetInputExample(&doctorModel.PatientUserAuthsEx{
		IdentityType: "weChat",
		Identitier: "open-id",
	})
	function.SetOutputExample(true)

	s.setDocFun(a, function)

	return function
}

func (s *Account) ChangeNickname(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.PatientUserBaseInfoChangeNickname{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InputError, err)
		return
	}

	nickname := argument.Nickname

	be := s.patientBusiness.Authentication().ChangeNickname(s.getLoginUserId(a), nickname)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(true)
}

func (s *Account) ChangeNicknameDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("修改当前登录用户昵称")
	function.SetNote("修改当前登录用户的昵称")
	function.SetInputExample(&doctorModel.PatientUserBaseInfoChangeNickname{
		Nickname:"测试",
	})
	function.SetOutputExample(true)

	s.setDocFun(a, function)

	return function
}