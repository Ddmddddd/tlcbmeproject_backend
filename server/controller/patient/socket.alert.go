package patient

import (
	"tlcbme_project/data/model"
	"tlcbme_project/data/model/doctor"
	"tlcbme_project/data/model/manage"
)

type socketAlert struct {
}

func newSocketAlert(token *model.Token) NotifyData {
	instance := &socketAlert{}

	return instance
}

func (s *socketAlert) Analysis(msg interface{}) (interface{}, bool) {

	warnings, be := msg.([]manage.InputDataWarnings)
	if !be {
		return nil, false
	}

	bBPWarning, bHRWarning, bGLUWarning := false, false, false
	bpAndHrFeedBack := doctor.AlertBPAndHRFeedbackForApp{}
	gluFeedBack := doctor.AlertGLUFeedbackForApp{}
	for _, v := range warnings {
		switch v.Type {
		case "血压":
			bpAndHrFeedBack.BPCode = v.Code
			bpAndHrFeedBack.BPValue = v.Reason
			bpAndHrFeedBack.BPTips = v.Message
			bBPWarning = true
		case "心率":
			bpAndHrFeedBack.HRCode = v.Code
			bpAndHrFeedBack.HRValue = v.Reason
			bpAndHrFeedBack.HRTips = v.Message
			bHRWarning = true
		case "血糖":
			gluFeedBack.GLUCode = v.Code
			gluFeedBack.GLUValue = v.Reason
			gluFeedBack.GLUTips = v.Message
			bGLUWarning = true
		}
	}

	if bBPWarning && bHRWarning {
		return bpAndHrFeedBack, true
	} else if bGLUWarning {
		return gluFeedBack, true
	} else {
		return nil, false
	}
}
