package patient

import (
	"tlcbme_project/business/doctor"
	"tlcbme_project/business/patient"
	"tlcbme_project/server/controller"
	"github.com/ktpswjz/httpserver/document"
	"github.com/ktpswjz/httpserver/router"
)

type doc struct {
	controller.Controller

	doctorBusiness  doctor.Business
	patientBusiness patient.Business
}

func (s *doc) rootCatalog(a document.Assistant) document.Catalog {
	return a.CreateCatalog("患者平台接口", "患者移动平台相关接口")
}

func (s *doc) getLoginUserId(a router.Assistant) uint64 {
	token, err := s.DbToken.Get(a.Token())
	if err != nil {
		return 0
	}
	if token == nil {
		return 0
	}

	return token.UserID
}

func (s *doc) getLoginUserOrgCode(a router.Assistant) string {
	token, err := s.DbToken.Get(a.Token())
	if err != nil {
		return ""
	}
	if token == nil {
		return ""
	}

	return token.OrgCode
}
