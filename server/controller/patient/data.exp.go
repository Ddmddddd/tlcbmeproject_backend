package patient

import (
	"github.com/ktpswjz/httpserver/document"
	"github.com/ktpswjz/httpserver/router"
	"github.com/ktpswjz/httpserver/types"
	"net/http"
	"strconv"
	"time"
	"tlcbme_project/business/doctor"
	"tlcbme_project/business/patient"
	"tlcbme_project/data/enum"
	doctorModel "tlcbme_project/data/model/doctor"
	nutrient "tlcbme_project/data/model/nutrient"
	"tlcbme_project/database/memory"
	"tlcbme_project/errors"
	"tlcbme_project/server/config"
	"tlcbme_project/server/notify"
)

type ExpData struct {
	doc
}

func NewExpData(cfg *config.Config, log types.Log, dbToken memory.Token, doctorBusiness doctor.Business, patientBusiness patient.Business) *ExpData {
	instance := &ExpData{}
	instance.SetLog(log)
	instance.Config = cfg
	instance.DbToken = dbToken
	instance.doctorBusiness = doctorBusiness
	instance.patientBusiness = patientBusiness

	return instance
}

func (s *ExpData) setDocFun(a document.Assistant, fun document.Function) {
	catalog := s.rootCatalog(a).CreateChild("饮食实验数据管理", "患者自我管理饮食实验数据相关接口")
	catalog.SetFunction(fun)
}

func (s *ExpData) GetPatientBaseInfo(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.ExpPatientInfoGet{}

	argument.PatientID = s.getLoginUserId(a)
	data, be := s.patientBusiness.Data().GetExpPatientBaseInfo(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *ExpData) GetPatientBaseInfoDoc(a document.Assistant) document.Function {
	patientID := uint64(100)
	function := a.CreateFunction("获取患者饮食管理的基本信息")
	function.SetNote("获取患者饮食管理的基本信息")
	function.SetInputExample(&doctorModel.ExpPatientInfoGet{
		PatientID: patientID,
	})
	function.SetOutputExample(&doctorModel.ExpPatientBaseInfo{
		PatientID: patientID,
		Status:    2,
		//UndoneScales: []uint64{1, 2},
	})
	s.setDocFun(a, function)

	return function
}

func (s *ExpData) IncludePatientExp(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.ExpPatientCreate{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}

	argument.PatientID = s.getLoginUserId(a)
	be := s.doctorBusiness.Exp().IncludePatientInExp(argument, false)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(true)
}

func (s *ExpData) IncludePatientExpDoc(a document.Assistant) document.Function {
	patientID := uint64(100)
	now := types.Time(time.Now())
	function := a.CreateFunction("将患者纳入饮食管理")
	function.SetNote("将患者纳入饮食管理")
	function.SetInputExample(&doctorModel.ExpPatientCreate{
		PatientID:      patientID,
		Type:           0,
		CreateDateTime: &now,
	})
	function.SetOutputExample(nil)
	s.setDocFun(a, function)

	return function
}

func (s *ExpData) CommitDefaultTaskScale(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.ExpDefaultScaleRecordCreate{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}

	argument.PatientID = s.getLoginUserId(a)
	be := s.patientBusiness.Data().CommitDefaultScaleRecord(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	// 提交默认行动计划量表后，修改患者的状态
	statusChangeArgument := &doctorModel.ExpPatientStatusChange{
		PatientID:      argument.PatientID,
		Status:         enum.ExpStatuses.InExp().Key,
		UpdateDateTime: argument.CreateDateTime,
	}
	be = s.doctorBusiness.Exp().ChangePatientStatus(statusChangeArgument, false)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(true)
}

func (s *ExpData) CommitDefaultTaskScaleDoc(a document.Assistant) document.Function {
	patientID := uint64(100)
	now := types.Time(time.Now())
	function := a.CreateFunction("提交默认行动量表")
	function.SetNote("提交默认行动量表")
	function.SetInputExample(&doctorModel.ExpDefaultScaleRecordCreate{
		PatientID:      patientID,
		ScaleID:        1,
		ScaleType:      1,
		Result:         nil,
		CreateDateTime: &now,
	})
	function.SetOutputExample(nil)
	s.setDocFun(a, function)

	return function
}

func (s *ExpData) CreateDefaultTask(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	patientID := s.getLoginUserId(a)
	be := s.patientBusiness.Data().CreateDefaultTask(patientID)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	now := types.Time(time.Now())
	// 提交默认行动计划量表后，修改患者的状态
	statusChangeArgument := &doctorModel.ExpPatientStatusChange{
		PatientID:      patientID,
		Status:         enum.ExpStatuses.InExp().Key,
		UpdateDateTime: &now,
	}
	be = s.doctorBusiness.Exp().ChangePatientStatus(statusChangeArgument, false)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(true)
}

func (s *ExpData) CreateDefaultTaskDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("生成默认行动计划")
	function.SetNote("生成默认行动计划")
	function.SetInputExample(nil)
	function.SetOutputExample(nil)
	s.setDocFun(a, function)

	return function
}

func (s *ExpData) GetScaleBySnoList(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.ExpCommonScaleSerialNoListFilter{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}

	data, be := s.patientBusiness.Data().GetScaleBySnoList(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *ExpData) GetScaleBySnoListDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("根据序号列表获取量表信息")
	function.SetNote("根据序号列表获取量表信息")
	function.SetInputExample(&doctorModel.ExpCommonScaleSerialNoListFilter{
		SerialNos: []uint64{1, 2},
	})
	function.SetOutputExample([]*doctorModel.ExpCommonScale{{}})
	s.setDocFun(a, function)

	return function
}

func (s *ExpData) GetScaleBySno(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.ExpCommonScaleSerialNoFilter{}
	a.GetArgument(r, argument)

	data, be := s.doctorBusiness.Exp().GetExpCommonScale(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *ExpData) GetScaleBySnoDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("获取实验量表")
	function.SetNote("从数据库中获取实验量表")
	function.SetInputExample(&doctorModel.ExpCommonScaleSerialNoFilter{
		SerialNo: 1,
	})
	function.SetOutputExample(doctorModel.ExpCommonScale{
		SerialNo:    1,
		Title:       "test",
		Content:     "",
		Description: "无",
		ScaleType:   0,
	})
	s.setDocFun(a, function)

	return function
}

func (s *ExpData) CommitScaleRecord(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.ExpScaleRecordCreate{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InternalError, err)
	}

	if argument.CreateDateTime == nil {
		now := types.Time(time.Now())
		argument.CreateDateTime = &now
	}

	argument.PatientID = s.getLoginUserId(a)
	//argument.UndoneScales, err = s.patientBusiness.Data().GetUndoneScales(argument.PatientID)
	//if err != nil {
	//	a.Error(errors.InternalError, err)
	//}

	data, be := s.doctorBusiness.Exp().CommitPatientExpScaleRecord(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *ExpData) CommitScaleRecordDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("提交量表")
	function.SetNote("提交量表")
	function.SetInputExample(&doctorModel.ExpCommonScaleSerialNoListFilter{
		SerialNos: []uint64{1, 2},
	})
	function.SetOutputExample([]*doctorModel.ExpCommonScale{{}})
	s.setDocFun(a, function)

	return function
}

func (s *ExpData) GetActionPlan(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.ExpPatientActionPlan{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}

	argument.PatientID = s.getLoginUserId(a)
	data, be := s.patientBusiness.Data().GetPatientExpActionPlanList(argument, false)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *ExpData) GetActionPlanDoc(a document.Assistant) document.Function {
	patientID := uint64(100)
	now := types.Time(time.Now())
	function := a.CreateFunction("获取行动计划")
	function.SetNote("获取相应的行动计划")
	function.SetInputExample(&doctorModel.ExpPatientActionPlan{
		PatientID: patientID,
		Status:    0,
	})
	function.SetOutputExample([]*doctorModel.ExpActionPlan{
		{
			SerialNo:       11,
			PatientID:      patientID,
			ActionPlan:     "细嚼慢咽",
			Status:         0,
			CreateDateTime: &now,
			UpdateDateTime: nil,
			EditorType:     0,
		},
	})
	s.setDocFun(a, function)

	return function
}

func (s *ExpData) EditActionPlan(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.ExpPatientActionPlanEdit{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}

	now := types.Time(time.Now())
	argument.UpdateDateTime = &now

	be := s.doctorBusiness.Exp().EditPatientExpActionPlan(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(nil)
}

func (s *ExpData) EditActionPlanDoc(a document.Assistant) document.Function {
	now := types.Time(time.Now())
	function := a.CreateFunction("调整行动计划")
	function.SetNote("调整行动计划，包括计划内容")
	function.SetInputExample(&doctorModel.ExpPatientActionPlanEdit{
		SerialNo:       11,
		Status:         0,
		ActionPlan:     "测试修改",
		UpdateDateTime: &now,
		EditorType:     1,
		Level:          3,
	})
	function.SetOutputExample([]*doctorModel.ExpActionPlan{
		{
			SerialNo:       11,
			PatientID:      100,
			ActionPlan:     "细嚼慢咽",
			Status:         0,
			CreateDateTime: &now,
			UpdateDateTime: &now,
			EditorType:     0,
		},
	})
	s.setDocFun(a, function)

	return function
}

func (s *ExpData) CommitActionPlanRecord(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.ExpActionPlanRecordForApp{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}

	argument.PatientID = s.getLoginUserId(a)
	sno, be := s.patientBusiness.Data().CommitExpActionPlanRecord(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(sno)
}

func (s *ExpData) CommitActionPlanRecordDoc(a document.Assistant) document.Function {
	now := types.Time(time.Now())
	function := a.CreateFunction("提交行动计划")
	function.SetNote("患者提交行动计划完成情况")
	function.SetInputExample(&doctorModel.ExpActionPlanRecordForApp{
		ActionPlan:     "测试提交",
		Completion:     2,
		CreateDateTime: &now,
	})
	function.SetOutputExample(11)
	s.setDocFun(a, function)

	return function
}

func (s *ExpData) EditActionPlanRecord(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.ExpActionPlanRecordEditForApp{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}

	be := s.patientBusiness.Data().EditExpActionPlanRecord(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(nil)
}

func (s *ExpData) EditActionPlanRecordDoc(a document.Assistant) document.Function {
	now := types.Time(time.Now())
	function := a.CreateFunction("编辑行动计划")
	function.SetNote("患者编辑行动计划")
	function.SetInputExample(&doctorModel.ExpActionPlanRecordEditForApp{
		ActionPlan:     "测试提交",
		Completion:     2,
		CreateDateTime: &now,
	})
	function.SetOutputExample(11)
	s.setDocFun(a, function)

	return function
}

func (s *ExpData) GetActionPlanRecord(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.ExpActionPlanRecordFilter{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}

	argument.PatientID = s.getLoginUserId(a)
	data, be := s.patientBusiness.Data().GetExpActionPlanRecord(argument, false) //无感记录-1.11更新，list无感记录取消
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *ExpData) GetActionPlanRecordDoc(a document.Assistant) document.Function {
	now := types.Time(time.Now())
	function := a.CreateFunction("获取行动计划完成情况")
	function.SetNote("获取患者行动计划的完成情况")
	function.SetInputExample(&doctorModel.ExpActionPlanRecordFilter{
		StartDateTime:    &now,
		ActionPlanIDList: []uint64{1, 2},
	})
	function.SetOutputExample(nil)
	s.setDocFun(a, function)

	return function
}

func (s *ExpData) GetOneGame(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	data, be := s.patientBusiness.Data().GetOneGame()
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *ExpData) GetOneGameDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("获取一次饮食游戏")
	function.SetNote("患者端获取一次饮食游戏")
	function.SetInputExample(nil)
	function.SetOutputExample([]*doctorModel.ExpFoodGI{
		{
			SerialNo:      1,
			FoodName:      "葡萄糖",
			GlycemicIndex: 100,
			FoodType:      "糖类",
		},
	})
	s.setDocFun(a, function)

	return function
}

func (s *ExpData) CommitGameResult(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.ExpGameResultRecord{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}

	argument.PatientID = s.getLoginUserId(a)
	sno, be := s.patientBusiness.Data().CommitGameResultRecord(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(sno)
}

func (s *ExpData) CommitGameResultDoc(a document.Assistant) document.Function {
	now := types.Time(time.Now())
	function := a.CreateFunction("提交饮食游戏结果")
	function.SetNote("患者端提交一次饮食游戏的结果")
	function.SetInputExample(doctorModel.ExpGameResultRecord{
		PatientName:    "测试",
		Point:          100,
		CreateDateTime: &now,
	})
	function.SetOutputExample(1)
	s.setDocFun(a, function)

	return function
}

func (s *ExpData) GetGameResult(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	filter := &doctorModel.ExpGameResultRecordFilter{}
	err := a.GetArgument(r, filter)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}

	filter.PatientID = s.getLoginUserId(a)
	sno, be := s.patientBusiness.Data().ListGameResultRecord(filter)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(sno)
}

func (s *ExpData) GetGameResultDoc(a document.Assistant) document.Function {
	now := types.Time(time.Now())
	function := a.CreateFunction("获取饮食游戏结果")
	function.SetNote("患者端获取近期饮食游戏的结果")
	function.SetInputExample(doctorModel.ExpGameResultRecordFilter{
		PatientID:     100,
		StartDateTime: &now,
	})
	function.SetOutputExample([]*doctorModel.ExpGameResultRecord{
		{
			SerialNo:       1,
			PatientID:      11215203,
			PatientName:    "测试002",
			Point:          100,
			CreateDateTime: nil,
		},
	})
	s.setDocFun(a, function)

	return function
}

func (s *ExpData) GetGameResultOrder(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	filter := &doctorModel.ExpGameResultRecordOrderFilter{}
	err := a.GetArgument(r, filter)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}

	sno, be := s.patientBusiness.Data().ListGameResultRecordOrder(filter)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(sno)
}

func (s *ExpData) GetGameResultOrderDoc(a document.Assistant) document.Function {
	now := types.Time(time.Now())
	function := a.CreateFunction("获取饮食游戏排行榜")
	function.SetNote("患者端获取近期饮食游戏的排行榜")
	function.SetInputExample(doctorModel.ExpGameResultRecordOrderFilter{
		StartDateTime: &now,
	})
	function.SetOutputExample([]*doctorModel.ExpGameResultRecord{
		{
			SerialNo:       1,
			PatientID:      11215203,
			PatientName:    "测试002",
			Point:          100,
			CreateDateTime: nil,
		},
	})
	s.setDocFun(a, function)

	return function
}

func (s *ExpData) GetPatientTodayTask(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	patientID := s.getLoginUserId(a)

	data, be := s.patientBusiness.Data().GetPatientTodayTask(patientID)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *ExpData) GetPatientTodayTaskDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("获取患者今日的行动计划")
	function.SetNote("患者端获取今日行动计划")
	function.SetInputExample(nil)
	function.SetOutputExample(doctorModel.ExpTaskRepositoryOutput{
		TaskName:         []string{"阅读"},
		TaskOrder:        1,
		Target:           []string{"无"},
		ParentTaskName:   "粗粮主食",
		ParentTaskTarget: "多吃粗粮",
	})
	s.setDocFun(a, function)

	return function
}

func (s *ExpData) GetTodayKnowledgeByType(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	filter := &doctorModel.ExpKnowledgeTypeInput{}
	err := a.GetArgument(r, filter)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}
	patientID := s.getLoginUserId(a)

	data, be := s.patientBusiness.Data().GetPatientTodayKnowledge(patientID, filter)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *ExpData) GetTodayKnowledgeByTypeDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("获取患者今日的知识")
	function.SetNote("患者端获取今日知识")
	function.SetInputExample(doctorModel.ExpKnowledgeTypeInput{
		Type: "粗粮主食",
	})
	function.SetOutputExample(doctorModel.ExpKnowledge{})
	s.setDocFun(a, function)

	return function
}

func (s *ExpData) GetKnowledgeByID(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	filter := &doctorModel.ExpKnowledgeSerialNoInput{}
	err := a.GetArgument(r, filter)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}
	patientID := s.getLoginUserId(a)

	data, be := s.patientBusiness.Data().GetKnowledgeByID(patientID, filter)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *ExpData) GetKnowledgeByIDDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("获取患者今日的知识")
	function.SetNote("患者端获取今日知识")
	function.SetInputExample(doctorModel.ExpKnowledgeSerialNoInput{
		SerialNo: 1,
	})
	function.SetOutputExample(doctorModel.ExpKnowledge{})
	s.setDocFun(a, function)

	return function
}

func (s *ExpData) CommitKnowledge(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.ExpKnowledgeToPatientStatusEdit{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}
	patientID := s.getLoginUserId(a)

	be := s.patientBusiness.Data().CommitKnowledge(patientID, argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(nil)
}

func (s *ExpData) CommitKnowledgeDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("患者完成知识阅读")
	function.SetNote("患者完成知识阅读")
	function.SetInputExample(doctorModel.ExpKnowledgeToPatientStatusEdit{
		KnowledgeID: 1,
		Status:      1,
	})
	function.SetOutputExample(nil)
	s.setDocFun(a, function)

	return function
}

func (s *ExpData) GetHistoryKnowledge(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	patientID := s.getLoginUserId(a)

	data, be := s.patientBusiness.Data().GetHistoryKnowledge(patientID)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *ExpData) GetHistoryKnowledgeDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("获取患者历史的知识")
	function.SetNote("患者端获取历史知识")
	function.SetInputExample(nil)
	function.SetOutputExample([]doctorModel.ExpKnowledge{})
	s.setDocFun(a, function)

	return function
}

func (s *ExpData) GetQuestionByDayIndex(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.ExpKnowledgeToPatientDayIndex{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}
	argument.PatientID = s.getLoginUserId(a)

	data, be := s.patientBusiness.Data().GetQuestionByDayIndex(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *ExpData) GetQuestionByDayIndexDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("获取患者需要回答的问题")
	function.SetNote("患者端获取上次学习知识相关的问题")
	function.SetInputExample(&doctorModel.ExpKnowledgeToPatientDayIndex{
		PatientID: 0,
		DayIndex:  0,
	})
	function.SetOutputExample(doctorModel.ExpKnowledge{})
	s.setDocFun(a, function)

	return function
}

func (s *ExpData) CommitQuestionAnswer(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.ExpPatientTaskRecordInput{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}
	argument.PatientID = s.getLoginUserId(a)

	data, be := s.patientBusiness.Data().CommitTaskRecord(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *ExpData) CommitQuestionAnswerDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("提交问题回答")
	function.SetNote("患者端提交问题回答")
	function.SetInputExample(&doctorModel.ExpPatientTaskRecordInput{
		PatientID: 0,
	})
	function.SetOutputExample(nil)
	s.setDocFun(a, function)

	return function
}

func (s *ExpData) GetUndoneSmallScale(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	patientID := s.getLoginUserId(a)

	data, be := s.patientBusiness.Data().GetUndoneSmallScale(patientID)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *ExpData) GetUndoneSmallScaleDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("获取患者未完成的小量表")
	function.SetNote("患者获取自己未完成的小量表")
	function.SetInputExample(nil)
	function.SetOutputExample([]*doctorModel.ExpSmallScale{
		{
			SerialNo: 1,
			TaskID:   1,
			Content:  "",
		},
	})
	s.setDocFun(a, function)

	return function
}

func (s *ExpData) GetSmallScale(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.ExpSmallScaleTaskIDInput{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}

	data, be := s.patientBusiness.Data().GetSmallScale(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *ExpData) GetSmallScaleDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("获取相应的小量表")
	function.SetNote("患者获取当天的小量表任务内容")
	function.SetInputExample(&doctorModel.ExpSmallScaleTaskIDInput{
		TaskID: 1,
	})
	function.SetOutputExample(doctorModel.ExpSmallScale{
		SerialNo: 1,
		TaskID:   1,
		Content:  "",
	})
	s.setDocFun(a, function)

	return function
}

func (s *ExpData) CommitSmallScaleRecord(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.ExpSmallScaleRecordInput{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}
	argument.PatientID = s.getLoginUserId(a)

	be := s.patientBusiness.Data().CommitSmallScaleRecord(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(true)
}

func (s *ExpData) CommitSmallScaleRecordDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("提交小量表结果")
	function.SetNote("患者端提交小量表结果")
	function.SetInputExample(&doctorModel.ExpSmallScaleRecordInput{})
	function.SetOutputExample(nil)
	s.setDocFun(a, function)

	return function
}

func (s *ExpData) CommitTaskRecord(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.ExpPatientTaskRecordInput{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}
	argument.PatientID = s.getLoginUserId(a)

	data, be := s.patientBusiness.Data().CommitTaskRecord(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *ExpData) CommitTaskRecordDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("记录任务")
	function.SetNote("患者端记录任务")
	function.SetInputExample(&doctorModel.ExpPatientTaskRecordInput{})
	function.SetOutputExample(nil)
	s.setDocFun(a, function)

	return function
}

func (s *ExpData) GetTaskRecord(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.ExpPatientTaskRecordGetByTaskIDsAndTime{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}
	argument.PatientID = s.getLoginUserId(a)

	data, be := s.patientBusiness.Data().GetTaskRecord(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *ExpData) GetTaskRecordDoc(a document.Assistant) document.Function {
	now := types.Time(time.Now())
	function := a.CreateFunction("提交问题回答")
	function.SetNote("患者端提交问题回答")
	function.SetInputExample(&doctorModel.ExpPatientTaskRecordGetByTaskIDsAndTime{
		PatientID:      100,
		TaskIDs:        []uint64{1},
		CreateDateTime: &now,
	})
	function.SetOutputExample([]*doctorModel.ExpPatientTaskRecord{
		{},
	})
	s.setDocFun(a, function)

	return function
}

func (s *ExpData) GetTaskCreditHistory(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	patientID := s.getLoginUserId(a)

	data, be := s.patientBusiness.Data().GetTaskCreditHistory(patientID)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *ExpData) GetTaskCreditHistoryDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("患者获取任务积分历史")
	function.SetNote("患者获取任务积分历史")
	function.SetInputExample(nil)
	function.SetOutputExample([]*doctorModel.ExpPatientTaskRecord{
		{},
	})
	s.setDocFun(a, function)

	return function
}

func (s *ExpData) GetUnreadDoctorCommentList(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.ExpDoctorCommentToPatientUnreadGet{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}
	argument.PatientID = s.getLoginUserId(a)

	data, be := s.patientBusiness.Data().GetUnreadDoctorCommentList(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *ExpData) GetUnreadDoctorCommentListDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("患者获取任务积分历史")
	function.SetNote("患者获取任务积分历史")
	function.SetInputExample(&doctorModel.ExpDoctorCommentToPatientUnreadGet{
		ReadFlag:  0,
		PatientID: 100,
	})
	function.SetOutputExample([]*doctorModel.ExpDoctorCommentToPatient{
		{},
	})
	s.setDocFun(a, function)

	return function
}

func (s *ExpData) ReadComment(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.ExpDoctorCommentToPatientRead{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}

	be := s.patientBusiness.Data().ReadComment(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}
	s.WriteNotify(&notify.Message{
		BusinessID: notify.BusinessDoctor,
		NotifyID:   notify.DoctorExpCommentRead,
		Data:       argument,
		Time:       types.Time(time.Now()),
	})

	a.Success(true)
}

func (s *ExpData) ReadCommentDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("患者获取任务积分历史")
	function.SetNote("患者获取任务积分历史")
	function.SetInputExample(&doctorModel.ExpDoctorCommentToPatientRead{
		SerialNo: 1,
		ReadFlag: 1,
	})
	function.SetOutputExample(nil)
	s.setDocFun(a, function)

	return function
}

func (s *ExpData) GetCommentListBySnos(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.ExpDoctorCommentToPatientSportDietSnosFilter{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}

	data, be := s.patientBusiness.Data().GetCommentListBySnos(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *ExpData) GetCommentListBySnosDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("患者根据饮食序号列表获取相应的评论列表")
	function.SetNote("患者根据饮食序号列表获取相应的评论列表")
	function.SetInputExample(&doctorModel.ExpDoctorCommentToPatientSportDietSnosFilter{
		SportDietRecordList: []uint64{1},
	})
	function.SetOutputExample(nil)
	s.setDocFun(a, function)

	return function
}

func (s *ExpData) GetDietAndCommentInfo(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.ExpDoctorCommentToPatientFilter{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}

	data, be := s.patientBusiness.Data().GetDietAndCommentInfo(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *ExpData) GetDietAndCommentInfoDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("微信服务号提醒跳转用，获取相应的饮食信息和评论信息")
	function.SetNote("微信服务号提醒跳转用，获取相应的饮食信息和评论信息")
	function.SetInputExample(&doctorModel.ExpDoctorCommentToPatientFilter{
		SerialNo: 0,
	})
	function.SetOutputExample(&doctorModel.ExpDoctorCommentToPatientWithDietInfo{})
	s.setDocFun(a, function)

	return function
}

func (s *ExpData) GetTodayUnreadCommentCount(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.ExpDoctorCommentToPatientTodayUnreadGet{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}
	argument.PatientID = s.getLoginUserId(a)

	data, be := s.patientBusiness.Data().GetTodayUnreadCommentCount(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *ExpData) GetTodayUnreadCommentCountDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("获取今日未读的评论数量，用于首页")
	function.SetNote("获取今日未读的评论数量，用于首页")
	function.SetInputExample(&doctorModel.ExpDoctorCommentToPatientTodayUnreadGet{})
	function.SetOutputExample(0)
	s.setDocFun(a, function)

	return function
}

func (s *ExpData) UploadBodyExamImage(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.ExpPatientBodyExamImageCreate{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}
	argument.PatientID = s.getLoginUserId(a)

	be := s.patientBusiness.Data().UploadPatientBodyExamImage(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(true)
}

func (s *ExpData) UploadBodyExamImageDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("上传体检报告图片")
	function.SetNote("上传体检报告图片")
	function.SetInputExample(&doctorModel.ExpPatientBodyExamImageCreate{})
	function.SetOutputExample(0)
	s.setDocFun(a, function)

	return function
}

func (s *ExpData) GetBodyExamImage(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	patientID := s.getLoginUserId(a)

	data, be := s.doctorBusiness.Exp().GetPatientBodyExamImage(patientID)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *ExpData) GetBodyExamImageDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("上传体检报告图片")
	function.SetNote("上传体检报告图片")
	function.SetInputExample(nil)
	function.SetOutputExample(0)
	s.setDocFun(a, function)

	return function
}

func (s *ExpData) GetFile(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.ExpPatientUploadFileGet{}
	argument.PatientID = s.getLoginUserId(a)

	data, be := s.doctorBusiness.Exp().GetPatientFile(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *ExpData) GetJinScale(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.ExpJinScaleCampFilter{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}

	data, be := s.patientBusiness.Data().GetJinScale(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *ExpData) GetFinishedJinScale(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	patientID := s.getLoginUserId(a)

	data, be := s.patientBusiness.Data().GetFinishedJinScale(patientID)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *ExpData) GetExercisePlan(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	patientID := s.getLoginUserId(a)

	data, be := s.patientBusiness.Data().GetExercisePlan(patientID)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *ExpData) CreateExercisePlan(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	patientID := s.getLoginUserId(a)
	argument := &doctorModel.ExpExercisePlanCreate{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}
	argument.PatientID = patientID

	be := s.patientBusiness.Data().CreateExercisePlan(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(true)
}

func (s *ExpData) GetPatientIntegralHistory(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	patientID := s.getLoginUserId(a)

	data, be := s.patientBusiness.Data().GetPatientIntegralHistory(patientID)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *ExpData) GetPatientUnreadIntegral(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	patientID := s.getLoginUserId(a)
	argument := &doctorModel.ExpIntegralUnreadGet{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}
	argument.PatientID = patientID

	data, be := s.patientBusiness.Data().GetPatientUnreadIntegral(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *ExpData) ReadPatientIntegral(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.ExpIntegralRead{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}

	be := s.patientBusiness.Data().ReadPatientIntegral(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(true)
}

func (s *ExpData) GetSettingDietitian(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	patientID := s.getLoginUserId(a)

	data, be := s.patientBusiness.Data().GetSettingDietitian(patientID)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *ExpData) GetPatientDietPlan(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.ExpPatientDietPlanGet{}
	patientID := s.getLoginUserId(a)
	argument.PatientID = patientID

	data, be := s.patientBusiness.Data().GetDietPlan(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *ExpData) GetPatientIntegralAll(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	//patientID := s.getLoginUserId(a)

	data, be := s.patientBusiness.Data().GetPatientIntegralAll()
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *ExpData) GetPatientDietPlanByLevel(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.ExpPatientDietPlanGetByLevel{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}
	patientID := s.getLoginUserId(a)
	argument.PatientID = patientID

	data, be := s.patientBusiness.Data().GetDietPlanByLevel(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *ExpData) ReadPatientDietPlan(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.ExpPatientDietPlanRead{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}

	be := s.patientBusiness.Data().ReadDietPlan(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(true)
}

func (s *ExpData) WxRemindCreate(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.WeChatRemindTaskCreate{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}

	argument.PatientID = s.getLoginUserId(a)
	sno, be := s.patientBusiness.Data().WxRemindCreate(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(sno)
}

func (s *ExpData) WxRemindCreateDoc(a document.Assistant) document.Function {
	now := types.Time(time.Now())
	function := a.CreateFunction("患者请求一次血糖记录提醒")
	function.SetNote("患者请求一次血糖记录提醒")
	function.SetInputExample(doctorModel.WeChatRemindTaskCreate{
		PatientID:      100,
		OpenID:         "",
		TemplateID:     "",
		CreateDateTime: &now,
		Type:           1,
	})
	function.SetOutputExample(1)
	s.setDocFun(a, function)

	return function
}

func (s *ExpData) SmsRemindCreate(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.TimingTaskCreate{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}

	argument.Patientid = s.getLoginUserId(a)
	sno, be := s.patientBusiness.Data().SmsRemindCreate(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(sno)
}

func (s *ExpData) SmsRemindCreateDoc(a document.Assistant) document.Function {
	now := types.Time(time.Now())
	function := a.CreateFunction("患者请求一次血糖记录提醒")
	function.SetNote("患者请求一次血糖记录提醒")
	function.SetInputExample(doctorModel.TimingTaskCreate{
		Patientid:           100,
		Tasktype:            2,
		Operatorid:          11,
		DoctorName:          "测试",
		Timingstartdatetime: &now,
	})
	function.SetOutputExample(1)
	s.setDocFun(a, function)

	return function
}

func (s *ExpData) PhotoFeedbackRemindCreate(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.WeChatRemindTaskCreate{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}

	argument.PatientID = s.getLoginUserId(a)
	sno, be := s.patientBusiness.Data().PhotoFeedbackRemindCreate(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(sno)
}

func (s *ExpData) PhotoFeedbackRemindCreateDoc(a document.Assistant) document.Function {
	now := types.Time(time.Now())
	function := a.CreateFunction("患者提交饮食，授权获取营养师评价反馈订阅消息")
	function.SetNote("患者提交饮食，授权获取营养师评价反馈订阅消息")
	function.SetInputExample(doctorModel.WeChatRemindTaskCreate{
		PatientID:      100,
		OpenID:         "",
		TemplateID:     "",
		CreateDateTime: &now,
		Type:           3,
	})
	function.SetOutputExample(1)
	s.setDocFun(a, function)

	return function
}

func (s *ExpData) Transfer(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	patientID := s.getLoginUserId(a)
	argument := &doctorModel.ExpPayToPatient{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}
	argument.PatientID = patientID

	data, be := s.patientBusiness.Data().Transfer(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *ExpData) CommitThreeMeals(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.DietRecordEx{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}

	now := types.Time(time.Now())
	if argument.EatDateTime == nil {
		argument.EatDateTime = &now
	}
	argument.InputDateTime = &now
	argument.PatientID = s.getLoginUserId(a)

	data, be := s.patientBusiness.Data().CommitThreeMeals(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *ExpData) CommitThreeMealsDoc(a document.Assistant) document.Function {
	now := types.Time(time.Now())

	function := a.CreateFunction("提交饮食营养信息")
	function.SetNote("提交饮食营养信息,成功时返回记录序列号")
	function.SetInputExample(&doctorModel.DietRecordEx{
		EatDateTime:   &now,
		InputDateTime: &now,
		DietType:      "午餐",
		DietContents: append(make([]*doctorModel.DietContent, 0), &doctorModel.DietContent{
			NutritionSerialNo: 59,
		}),
	})
	function.SetOutputExample(11)

	s.setDocFun(a, function)

	return function
}

func (s *ExpData) CommitPatientReply(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.ExpDoctorCommentToPatientReply{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}

	be := s.patientBusiness.Data().CommitPatientReply(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(true)
}

func (s *ExpData) GetPatientClock(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	patientID := s.getLoginUserId(a)
	argument := &doctorModel.ExpPatientTaskRecordGetListInput{
		PatientID: patientID,
	}

	data, be := s.patientBusiness.Data().GetPatientClock(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)

}

func (s *ExpData) GetMealContent(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.DietContentFilter{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}

	data, be := s.patientBusiness.Data().GetMealContent(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *ExpData) GetMealContentDoc(a document.Assistant) document.Function {

	function := a.CreateFunction("前端展示打卡饮食信息")
	function.SetNote("前端展示打卡饮食信息")
	function.SetInputExample(&doctorModel.DietContentFilter{
		DietRecordSerialNo: 1,
	})
	function.SetOutputExample(11)

	s.setDocFun(a, function)

	return function
}

func (s *ExpData) DeleteMealContent(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.DietRecordFilter{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}

	data, be := s.patientBusiness.Data().DeleteMealContent(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *ExpData) DeleteMealContentDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("删除打卡记录")
	function.SetNote("删除打卡记录")
	function.SetInputExample(&doctorModel.DietRecordFilter{
		SerialNo: 1,
	})
	function.SetOutputExample(11)

	s.setDocFun(a, function)

	return function
}

func (s *ExpData) SaveNutrientAnalysis(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	//argument := &doctorModel.PatientJudgeDoctorApp{}
	//err := a.GetArgument(r, argument)
	//if err != nil {
	//	a.Error(errors.InternalError, err)
	//	return
	//}
	//
	//argument.PatientID = s.getLoginUserId(a)
	//now := types.Time(time.Now())
	//argument.DateTime = &now
	//now := types.Time(time.Now())
	//argument := &doctorModel.NutrientAnalysisFilter{
	//	PatientID:        s.getLoginUserId(a),
	//	AnalysisDateTime: &now,
	//}

	argument := &doctorModel.NutrientAnalysisFilter{}

	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}
	argument.PatientID = s.getLoginUserId(a)

	data, be := s.patientBusiness.Data().NutrientAnalysis(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *ExpData) SaveNutrientAnalysisDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("营养分析")
	function.SetNote("营养分析")
	now := types.Time(time.Now())
	function.SetInputExample(&doctorModel.NutrientAnalysisFilter{
		PatientID:        10000,
		AnalysisDateTime: &now,
	})
	function.SetOutputExample(&doctorModel.NutrientAnalysisResult{})
	s.setDocFun(a, function)

	return function
}

func (s *ExpData) GetHistoryEnergy(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	now := types.Time(time.Now())
	filter := &doctorModel.NutrientAnalysisFilter{
		PatientID:        s.getLoginUserId(a),
		AnalysisDateTime: &now,
	}
	argument := &doctorModel.NutrientAnalysisFilterEx{NutrientAnalysisFilter: *filter}
	//a.GetArgument(r, argument)
	data, be := s.patientBusiness.Data().GetHistoryEnergy(argument)

	//s.patientBusiness.Data().CreateNutrientReportRecord(data, argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *ExpData) GetHistoryEnergyDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("营养分析历史能量")
	function.SetNote("营养分析历史能量")
	now := types.Time(time.Now())
	function.SetInputExample(&doctorModel.NutrientAnalysisFilter{
		PatientID:        10000,
		AnalysisDateTime: &now,
	})
	function.SetOutputExample(nil)
	s.setDocFun(a, function)

	return function
}

func (s *ExpData) GetHistoryGrade(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	now := types.Time(time.Now())
	filter := &doctorModel.NutrientAnalysisFilter{
		PatientID:        s.getLoginUserId(a),
		AnalysisDateTime: &now,
	}
	argument := &doctorModel.NutrientAnalysisFilterEx{NutrientAnalysisFilter: *filter}

	data, be := s.patientBusiness.Data().GetHistoryGrade(argument)

	//s.patientBusiness.Data().CreateNutrientReportRecord(data, argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *ExpData) GetHistoryGradeDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("营养分析历史分数")
	function.SetNote("营养分析历史分数")
	now := types.Time(time.Now())
	function.SetInputExample(&doctorModel.NutrientAnalysisFilter{
		PatientID:        10000,
		AnalysisDateTime: &now,
	})
	function.SetOutputExample(&doctorModel.NutrientAnalysisResult{})
	s.setDocFun(a, function)

	return function
}

func (s *ExpData) CommitJudge(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.PatientJudgeDoctorApp{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}

	argument.PatientID = s.getLoginUserId(a)
	now := types.Time(time.Now())
	argument.DateTime = &now

	be := s.patientBusiness.Data().CommitJudge(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(true)

}

func (s *ExpData) CommitBug(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.UserBugFeedbackApp{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}

	argument.PatientID = s.getLoginUserId(a)
	now := types.Time(time.Now())
	argument.DateTime = &now

	be := s.patientBusiness.Data().CommitBug(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(true)

}

func (s *ExpData) GetPatientInfo(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.PatientInfoBase{}
	argument.PatientID = s.getLoginUserId(a)

	data, be := s.patientBusiness.Data().GetPatientInfo(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)

}

func (s *ExpData) UpdatePatientInfo(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.PatientUserInfoUpdate{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}
	argument.PatientID = s.getLoginUserId(a)
	be := s.patientBusiness.Data().UpdatePatientInfo(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}
	a.Success(true)

}

func (s *ExpData) FoodSearch(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.FoodSearchFilter{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}
	data, be := s.patientBusiness.Data().FoodSearch(argument)

	//s.patientBusiness.Data().CreateNutrientReportRecord(data, argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *ExpData) FoodSearchDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("食品搜索")
	function.SetNote("食品搜索")
	str := "鸡蛋"
	function.SetInputExample(&doctorModel.FoodSearchFilter{
		Name: str,
	})
	function.SetOutputExample(nil)
	s.setDocFun(a, function)

	return function
}

func (s *ExpData) GetFoodMeasureUnit(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.FoodMeasureUnitFilter{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}
	data, be := s.patientBusiness.Data().GetFoodMeasureUnit(argument)

	//s.patientBusiness.Data().CreateNutrientReportRecord(data, argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *ExpData) GetFoodMeasureUnitDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("获取食品度量单位")
	function.SetNote("获取食品度量单位")
	function.SetInputExample(&doctorModel.FoodMeasureUnitFilter{
		Serial_No: 0,
	})
	function.SetOutputExample(nil)
	s.setDocFun(a, function)

	return function
}

func (s *ExpData) GetPointRule(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {

	data, be := s.patientBusiness.Data().GetGradeRule("20210422")

	//s.patientBusiness.Data().CreateNutrientReportRecord(data, argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *ExpData) FoodMultiSearch(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.FoodMultiSearchFilter{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}
	data, be := s.patientBusiness.Data().FoodMultiSearch(argument)

	//s.patientBusiness.Data().CreateNutrientReportRecord(data, argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}
	a.Success(data)
}

//健康教育相关
func (s *ExpData) GetTagInfo(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	data, be := s.patientBusiness.Data().GetTagInfo()
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}
	a.Success(data)
}

func (s *ExpData) FoodMultiSearchDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("多个食品搜索")
	function.SetNote("多个食品搜索")
	str := [...]string{"鸡蛋", "苹果"}
	function.SetInputExample(&doctorModel.FoodSearchFilter{
		Name: str[0],
	})
	function.SetOutputExample(nil)
	s.setDocFun(a, function)

	return function
}

func (s *ExpData) CommitTotalFood(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.DietRecordTotalForApp{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}
	now := types.Time(time.Now())
	if argument.InputDateTime == nil {
		argument.InputDateTime = &now
	}
	argument.PatientID = s.getLoginUserId(a)
	data, be := s.patientBusiness.Data().CommitTotalFood(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}
	a.Success(data)
}

func (s *ExpData) GetPatientTag(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.PatientTagInfo{}
	argument.PatientID = s.getLoginUserId(a)
	data, be := s.patientBusiness.Data().GetPatientTag(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}
	a.Success(data)
}

func (s *ExpData) DeletePatientTag(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.PatientToTagDelete{} //再model的ext中定义一下该数据结构，包括patientID,tag_id
	//想要拿到与系统自带的无关的前端参数
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}
	argument.PatientID = s.getLoginUserId(a)
	//从前端拿到参数准备完成
	data, be := s.patientBusiness.Data().DeletePatientTag(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}
	a.Success(data)
}

func (s *ExpData) DeleteTotalFood(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.DietRecordTotalDeleteForApp{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}
	argument.PatientID = s.getLoginUserId(a)
	data, be := s.patientBusiness.Data().DeleteTotalFood(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}
	a.Success(data)
}

func (s *ExpData) AddPatientTag(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.PatientToTagAdd{} //在model的ext中定义一下该数据结构，包括patientID,tag_id
	//拿到前端传入的tag_id
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}
	argument.PatientID = s.getLoginUserId(a)
	//从前端拿到参数准备完成
	data, be := s.patientBusiness.Data().AddPatientTag(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}
	a.Success(data)

}

func (s *ExpData) GetNutritionIntervention(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	PatientID := s.getLoginUserId(a)
	data, be := s.patientBusiness.Data().GetNutritionIntervention(PatientID)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)

}

func (s *ExpData) UpdateNutritionIntervention(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.ExpPatientNutritionalIndicator{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}
	now := types.Time(time.Now())
	if argument.UpdateTime == nil {
		argument.UpdateTime = &now
	}
	argument.UserID = s.getLoginUserId(a)
	be := s.patientBusiness.Data().UpdateNutritionIntervention(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}
	a.Success(true)
}

func (s *ExpData) GetKnowledgeList(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.SelectedTags{} //前端传过来的参数是SelectedTags,这个数据结构是json->selectedTags
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}
	data, be := s.patientBusiness.Data().GetKnowledgeList(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}
	a.Success(data)
}

func (s *ExpData) UpdateUseTime(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.PatientUserAuthsUpdateTime{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}
	argument.UserID = s.getLoginUserId(a)
	be := s.patientBusiness.Data().UpdateUseTime(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}
	a.Success(true)

}

func (s *ExpData) GetNutritionInterventionQuestion(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {

	data, be := s.patientBusiness.Data().GetNutritionInterventionQuestion()
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}
	a.Success(data)
}

func (s *ExpData) ResultNutritionInterventionQuestion(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.ExpPatientNutritionalQuestionInput{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}
	now := types.Time(time.Now())
	if argument.InputDate == nil {
		argument.InputDate = &now
	}
	argument.UserID = s.getLoginUserId(a)
	be := s.patientBusiness.Data().ResultNutritionInterventionQuestion(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}
	a.Success(true)
}

func (s *ExpData) AddKnowledgeLoveNum(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.KnowledgeInfoId{} //前端传过来的参数id,这个数据结构去model/ext中定义
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}
	data, be := s.patientBusiness.Data().AddKnowledgeLoveNum(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}
	a.Success(data)
}

func (s *ExpData) IfNutritionInterventionQuestion(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.ExpPatientNutritionalQuestionFilter{}
	argument.PatientID = s.getLoginUserId(a)
	data, be := s.patientBusiness.Data().IfNutritionInterventionQuestion(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}
	a.Success(data)
}

func (s *ExpData) GetWeeklyReport(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	PatientID := s.getLoginUserId(a)
	data, be := s.patientBusiness.Data().GetWeeklyReport(PatientID)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}
	a.Success(data)
}

func (s *ExpData) UpdateWeeklyHabitReport(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.ExpNutritionInterventionHabitReportHabitUpdate{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}
	be := s.patientBusiness.Data().UpdateWeeklyHabitReport(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}
	a.Success(true)
}

func (s *ExpData) GetKnowledgeIdList(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.PatientCollectedKnowledgePatientId{}
	argument.PatientID = s.getLoginUserId(a)
	data, be := s.patientBusiness.Data().GetKnowledgeIdList(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}
	a.Success(data)
}

func (s *ExpData) GetTlcKnowledgeById(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.KnowledgeInfoId{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}
	data, be := s.patientBusiness.Data().GetTlcKnowledgeById(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}
	a.Success(data)
}

func (s *ExpData) AddCollectedKnowledge(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.PatientCollectedKnowledgeAdd{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}
	argument.PatientID = s.getLoginUserId(a)
	data, be := s.patientBusiness.Data().AddCollectedKnowledge(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}
	a.Success(data)
}

func (s *ExpData) DeleteCollectedKnowledge(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.PatientCollectedKnowledgeDelete{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}
	argument.PatientID = s.getLoginUserId(a)
	data, be := s.patientBusiness.Data().DeleteCollectedKnowledge(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}
	a.Success(data)
}

func (s *ExpData) GetCollectedKnowledgeList(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.PatientCollectedKnowledgeIdList{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}
	data, be := s.patientBusiness.Data().GetCollectedKnowledgeList(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}
	a.Success(data)
}

func (s *ExpData) GiveDateToGetRcKnw(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.KnowledgeInfoDateString{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}
	argument.PatientID = s.getLoginUserId(a)
	data, be := s.patientBusiness.Data().GiveDateToGetRcKnw(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}
	a.Success(data)
}

func (s *ExpData) TlcGetTodayKnowledge(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.KnowledgeInfoDateString{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}
	argument.PatientID = s.getLoginUserId(a)
	data, be := s.patientBusiness.Data().TlcGetTodayKnowledge(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}
	a.Success(data)
}

func (s *ExpData) TlcGetKnowledgeStar(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.TlcPatientToRecKnowledgeGetStar{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}
	argument.PatientID = s.getLoginUserId(a)
	data, be := s.patientBusiness.Data().TlcGetKnowledgeStar(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}
	a.Success(data)
}

func (s *ExpData) TlcUpdateKnowledgeStar(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.TlcPatientToRecKnowledgeUpdateStar{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}
	argument.PatientID = s.getLoginUserId(a)
	data, be := s.patientBusiness.Data().TlcUpdateKnowledgeStar(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}
	a.Success(data)
}

func (s *ExpData) TlcChangeKnowledgeStatus(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.TlcPatientToRecKnowledgeChangeStatus{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}
	argument.PatientID = s.getLoginUserId(a)
	data, be := s.patientBusiness.Data().TlcChangeKnowledgeStatus(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}
	a.Success(data)
}

func (s *ExpData) TlcGetHistoryKnowledge(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.TlcPatientToRecKnowledgePatientID{}
	argument.PatientID = s.getLoginUserId(a)
	data, be := s.patientBusiness.Data().TlcGetHistoryKnowledge(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}
	a.Success(data)
}

//comment function
func (s *ExpData) TlcSendKnwComment(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.TlcPatientSendKnwComment{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}
	argument.PateintID = s.getLoginUserId(a)
	data, be := s.patientBusiness.Data().TlcSendKnwComment(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}
	a.Success(data)
}

func (s *ExpData) TlcGetKnwCommentsInfo(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.TlcGetKnwComments{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}
	data, be := s.patientBusiness.Data().TlcGetKnwCommentsInfo(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}
	a.Success(data)
}

func (s *ExpData) TlcGetPatientName(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.TlcPatientNameQuery{}
	argument.PatientID = s.getLoginUserId(a)
	data, be := s.patientBusiness.Data().TlcGetPatientName(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}
	a.Success(data)
}

func (s *ExpData) TlcPreRequestOfQuestion(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.TlcHeEduPatientQuestionGetBindWithKnw{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}
	argument.PatientID = s.getLoginUserId(a)
	data, be := s.patientBusiness.Data().TlcPreRequestOfQuestion(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}
	a.Success(data)
}

func (s *ExpData) TlcGetIntegratedQuestionInfo(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.TlcHeEduPatientQuestionGetBindWithKnw{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}
	argument.PatientID = s.getLoginUserId(a)
	data, be := s.patientBusiness.Data().TlcGetIntegratedQuestionInfo(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}
	a.Success(data)
}

func (s *ExpData) TlcRecordPatientAnswerQuestion(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.TlcHeEduPatientAnswerQuestionRecord{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}
	argument.PatientID = s.getLoginUserId(a)
	data, be := s.patientBusiness.Data().TlcRecordPatientAnswerQuestion(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}
	a.Success(data)
}

func (s *ExpData) FoodBasicSearch(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.FoodSearchFilter{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}
	data, be := s.patientBusiness.Data().FoodBasicSearch(argument)
	//s.patientBusiness.Data().CreateNutrientReportRecord(data, argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}
	a.Success(data)
}

func (s *ExpData) FoodBasicSearchDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("只从food表进行的食品搜索")
	function.SetNote("食品搜索")
	str := "鸡蛋"
	function.SetInputExample(&doctorModel.FoodSearchFilter{
		Name: str,
	})
	function.SetOutputExample(nil)
	s.setDocFun(a, function)

	return function
}

func (s *ExpData) AddPatientRecipe(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &nutrient.NewRecipeFromApp{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}
	argument.Source = strconv.FormatInt(int64(s.getLoginUserId(a)), 10)
	argument.BelongDepID = "8"
	data, be := s.patientBusiness.Data().AddPatientRecipe(argument)

	//s.patientBusiness.Data().CreateNutrientReportRecord(data, argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *ExpData) AddPatientRecipeDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("小程序用户自创菜肴")
	function.SetNote("小程序用户自创菜肴")
	str := "鸡蛋"
	function.SetInputExample(&nutrient.NewRecipeFromApp{
		Name: str,
	})
	function.SetOutputExample(nil)
	s.setDocFun(a, function)

	return function
}

func (s *ExpData) TlcHasKnwCommitedAsTask(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.ExpPatientTaskRecordCheckKnwRecord{}
	argument.PatientID = s.getLoginUserId(a)
	data, be := s.patientBusiness.Data().TlcHasKnwCommitedAsTask(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}
	a.Success(data)
}

func (s *ExpData) CreateTeam(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.TlcTeamCreateInfo{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}

	argument.CreatorID = s.getLoginUserId(a)
	data, be := s.patientBusiness.Data().CreateTeam(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}
	a.Success(data)
}
func (s *ExpData) GetSportItem(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.SportItemType{}

	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}

	data, be := s.patientBusiness.Data().GetSportItem(argument)

	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}
	a.Success(data)

}

func (s *ExpData) CreateTeamPre(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	patientID := s.getLoginUserId(a)
	data, be := s.patientBusiness.Data().CreateTeamPre(patientID)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}
	a.Success(data)
}

func (s *ExpData) TeamQueryByGroup(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.TlcTeamQueryByGroup{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}
	data, be := s.patientBusiness.Data().TeamQueryByGroup(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}
	a.Success(data)
}

func (s *ExpData) TeamQuerySearch(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.TlcTeamQueryBySearch{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}
	data, be := s.patientBusiness.Data().TeamQuerySearch(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}
	a.Success(data)
}

func (s *ExpData) JoinTeam(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.TlcUserJoinTeamInfo{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}
	argument.UserID = s.getLoginUserId(a)
	data, be := s.patientBusiness.Data().JoinTeam(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}
	a.Success(data)
}

func (s *ExpData) IsJoinedTeam(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	patientID := s.getLoginUserId(a)
	data, be := s.patientBusiness.Data().IsJoinedTeam(patientID)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}
	a.Success(data)
}

func (s *ExpData) GetWholeTeamDietsData(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.TlcTeamUserFuncFromAppDiets{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}
	argument.UserID = s.getLoginUserId(a)
	data, be := s.patientBusiness.Data().GetWholeTeamDietsData(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}
	a.Success(data)
}

func (s *ExpData) GetTeamWeightsData(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.TlcTeamUserFuncFromApp{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}
	argument.UserID = s.getLoginUserId(a)
	data, be := s.patientBusiness.Data().GetTeamWeightsData(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}
	a.Success(data)
}

func (s *ExpData) GetTeamSportStepsData(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.TlcTeamUserFuncFromApp{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}
	argument.UserID = s.getLoginUserId(a)
	data, be := s.patientBusiness.Data().GetTeamSportStepsData(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}
	a.Success(data)
}

func (s *ExpData) GetWholeTeamTaskInfo(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.TlcTeamUserFuncFromApp{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}
	argument.UserID = s.getLoginUserId(a)
	data, be := s.patientBusiness.Data().GetWholeTeamTaskInfo(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}
	a.Success(data)
}

func (s *ExpData) GetTeamInfo(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.TlcTeamInfoFromAppFuncArea{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}
	data, be := s.patientBusiness.Data().GetTeamInfo(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}
	a.Success(data)
}

func (s *ExpData) GetTeamRankWholeInfo(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.TlcTeamInfoFromAppFuncArea{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}
	data, be := s.patientBusiness.Data().GetTeamRankWholeInfo(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}
	a.Success(data)
}

func (s *ExpData) InsertTeamIntegral(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.TlcTeamInfoFromAppFuncArea{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}
	data, be := s.patientBusiness.Data().InsertTeamIntegral(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}
	a.Success(data)
}

func (s *ExpData) GetTeamMemInfo(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.TlcTeamUserFuncFromApp{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}
	argument.UserID = s.getLoginUserId(a)
	data, be := s.patientBusiness.Data().GetTeamMemInfo(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}
	a.Success(data)
}

func (s *ExpData) DeleteTeamMem(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.TlcTeamUserFuncFromApp{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}
	data, be := s.patientBusiness.Data().DeleteTeamMem(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}
	a.Success(data)
}
