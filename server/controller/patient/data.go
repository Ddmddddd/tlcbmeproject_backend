package patient

import (
	"github.com/ktpswjz/httpserver/document"
	"github.com/ktpswjz/httpserver/router"
	"github.com/ktpswjz/httpserver/types"
	"net/http"
	"time"
	"tlcbme_project/business/doctor"
	"tlcbme_project/business/patient"
	doctorModel "tlcbme_project/data/model/doctor"
	"tlcbme_project/database/memory"
	"tlcbme_project/errors"
	"tlcbme_project/server/config"
)

type Data struct {
	doc
}

func NewData(cfg *config.Config, log types.Log, dbToken memory.Token, doctorBusiness doctor.Business, patientBusiness patient.Business) *Data {
	instance := &Data{}
	instance.SetLog(log)
	instance.Config = cfg
	instance.DbToken = dbToken
	instance.doctorBusiness = doctorBusiness
	instance.patientBusiness = patientBusiness

	return instance
}

func (s *Data) setDocFun(a document.Assistant, fun document.Function) {
	catalog := s.rootCatalog(a).CreateChild("数据管理", "患者自我管理数据相关接口")
	catalog.SetFunction(fun)
}

func (s *Data) CreateBloodPressureRecord(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.BloodPressureRecord{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}

	now := types.Time(time.Now())
	argument.InputDateTime = &now
	if argument.MeasureDateTime == nil {
		argument.MeasureDateTime = &now
	}
	argument.PatientID = s.getLoginUserId(a)

	data, be := s.patientBusiness.Data().CreateBloodPressureRecord(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Data) CreateBloodPressureRecordDoc(a document.Assistant) document.Function {
	heartRate := uint64(65)
	measureBodyPart := uint64(0)
	MeasurePlace := uint64(0)
	now := types.Time(time.Now())

	function := a.CreateFunction("新增血压记录")
	function.SetNote("新增血压记录患者血压记录信息,成功时返回记录序列号")
	function.SetInputExample(&doctorModel.BloodPressureRecordCreate{
		SystolicPressure:  110,
		DiastolicPressure: 78,
		HeartRate:         &heartRate,
		MeasureBodyPart:   &measureBodyPart,
		Memo:              "服用硝苯地平控释片2小时后",
		MeasurePlace:      &MeasurePlace,
		MeasureDateTime:   &now,
	})
	function.SetOutputExample(11)

	s.setDocFun(a, function)

	return function
}

func (s *Data) CommitBloodPressureRecord(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.BloodPressureRecordForAppEx{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}

	now := types.Time(time.Now())
	if argument.MeasureDateTime == nil {
		argument.MeasureDateTime = &now
	}
	argument.PatientID = s.getLoginUserId(a)

	data, be := s.patientBusiness.Data().CommitBloodPressureRecord(argument, false)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Data) CommitBloodPressureRecordDoc(a document.Assistant) document.Function {
	heartRate := uint64(65)
	now := types.Time(time.Now())

	function := a.CreateFunction("提交血压记录")
	function.SetNote("提交血压记录患者血压记录信息,成功时返回记录序列号")
	function.SetInputExample(&doctorModel.BloodPressureRecordForApp{
		SerialNo:          11,
		SystolicPressure:  110,
		DiastolicPressure: 78,
		HeartRate:         &heartRate,
		Memo:              "服用硝苯地平控释片2小时后",
		MeasureDateTime:   &now,
		Type:              1,
	})
	function.SetOutputExample(11)

	s.setDocFun(a, function)

	return function
}

func (s *Data) CommitBloodGlucoseRecord(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.BloodGlucoseRecordForAppEx{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}

	now := types.Time(time.Now())
	if argument.MeasureDateTime == nil {
		argument.MeasureDateTime = &now
	}
	argument.PatientID = s.getLoginUserId(a)

	data, be := s.patientBusiness.Data().CommitBloodGlucoseRecord(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Data) CommitBloodGlucoseRecordDoc(a document.Assistant) document.Function {
	now := types.Time(time.Now())

	function := a.CreateFunction("提交血糖记录")
	function.SetNote("提交血糖记录患者血糖记录信息,成功时返回记录序列号")
	function.SetInputExample(&doctorModel.BloodGlucoseRecordForApp{
		SerialNo:        11,
		BloodGlucose:    7.8,
		Memo:            "",
		MeasureDateTime: &now,
		TimePoint:       "晨起空腹",
	})
	function.SetOutputExample(11)

	s.setDocFun(a, function)

	return function
}

func (s *Data) CommitWeightRecord(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.WeightRecordForAppEx{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}

	now := types.Time(time.Now())
	if argument.MeasureDateTime == nil {
		argument.MeasureDateTime = &now
	}
	argument.PatientID = s.getLoginUserId(a)

	data, be := s.patientBusiness.Data().CommitWeightRecord(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Data) CommitWeightRecordDoc(a document.Assistant) document.Function {
	now := types.Time(time.Now())

	function := a.CreateFunction("提交体重记录")
	function.SetNote("提交体重记录患者体重记录信息,成功时返回记录序列号")
	function.SetInputExample(&doctorModel.WeightRecordForApp{
		SerialNo:        11,
		Weight:          60.5,
		Memo:            "有氧运动半小时后",
		MeasureDateTime: &now,
		Type:            1,
	})
	function.SetOutputExample(11)

	s.setDocFun(a, function)

	return function
}

func (s *Data) CommitDrugRecord(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.DrugRecordForAppEx{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}

	now := types.Time(time.Now())
	if argument.UseDateTime == nil {
		argument.UseDateTime = &now
	}
	argument.PatientID = s.getLoginUserId(a)

	data, be := s.patientBusiness.Data().CommitDrugRecord(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Data) CommitDrugRecordDoc(a document.Assistant) document.Function {
	now := types.Time(time.Now())

	function := a.CreateFunction("提交服药记录")
	function.SetNote("提交饮食记录患者服药记录信息,成功时返回记录序列号")
	function.SetInputExample(&doctorModel.DrugRecordForApp{
		SerialNo:    "",
		DrugName:    "硝苯地平,依普纳利",
		Dosage:      "5mg,10mg",
		Memo:        "",
		UseDateTime: &now,
		Type:        1,
	})
	function.SetOutputExample("11,12")

	s.setDocFun(a, function)

	return function
}

func (s *Data) CommitDietRecord(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.DietRecordForAppEx{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}

	now := types.Time(time.Now())
	if argument.HappenDateTime == nil {
		argument.HappenDateTime = &now
	}
	argument.PatientID = s.getLoginUserId(a)

	data, be := s.patientBusiness.Data().CommitDietRecord(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Data) CommitDietRecordDoc(a document.Assistant) document.Function {
	now := types.Time(time.Now())

	function := a.CreateFunction("提交饮食记录")
	function.SetNote("提交饮食记录患者饮食记录信息,成功时返回记录序列号")
	function.SetInputExample(&doctorModel.DietRecordForApp{
		SerialNo:       "",
		Kinds:          "主食,肉类",
		Appetite:       "150,60",
		Memo:           "",
		HappenDateTime: &now,
		Type:           1,
	})
	function.SetOutputExample("11,12")

	s.setDocFun(a, function)

	return function
}

func (s *Data) CommitSportRecord(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.SportRecordForAppEx{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}

	now := types.Time(time.Now())
	if argument.HappenDateTime == nil {
		argument.HappenDateTime = &now
	}
	argument.PatientID = s.getLoginUserId(a)

	data, be := s.patientBusiness.Data().CommitSportRecord(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Data) CommitSportRecordDoc(a document.Assistant) document.Function {
	now := types.Time(time.Now())

	function := a.CreateFunction("提交运动记录")
	function.SetNote("提交运动录患者运动记录信息,成功时返回记录序列号")
	function.SetInputExample(&doctorModel.SportRecordForApp{
		SerialNo:       11,
		SportsType:     "步行",
		DurationTime:   0,
		Intensity:      "低",
		StepCount:      3699,
		Memo:           "",
		HappenDateTime: &now,
	})
	function.SetOutputExample(11)

	s.setDocFun(a, function)

	return function
}

func (s *Data) CommitDiscomfortRecord(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.DiscomfortRecordForAppEx{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}

	now := types.Time(time.Now())
	if argument.HappenDateTime == nil {
		argument.HappenDateTime = &now
	}
	argument.PatientID = s.getLoginUserId(a)

	data, be := s.patientBusiness.Data().CommitDiscomfortRecord(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Data) CommitDiscomfortRecordDoc(a document.Assistant) document.Function {
	now := types.Time(time.Now())

	function := a.CreateFunction("提交不适记录")
	function.SetNote("提交不适记录患者不适记录信息,成功时返回记录序列号")
	function.SetInputExample(&doctorModel.DiscomfortRecordForApp{
		SerialNo:       11,
		Discomfort:     "剧烈头痛,恶心呕吐",
		Memo:           "",
		HappenDateTime: &now,
	})
	function.SetOutputExample(11)

	s.setDocFun(a, function)

	return function
}

func (s *Data) ListBloodPressureRecord(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.BloodPressureRecordDataFilterEx{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InputError, err)
		return
	}
	argument.PatientID = s.getLoginUserId(a)

	data, be := s.patientBusiness.Data().ListBloodPressureRecord(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Data) ListBloodPressureRecordDoc(a document.Assistant) document.Function {
	heartRate := uint64(65)
	now := types.Time(time.Now())
	function := a.CreateFunction("获取血压数据列表")
	function.SetNote("获取患者血压及心率数据列表")
	function.SetInputExample(&doctorModel.BloodPressureRecordDataFilter{
		MeasureStartDate: &now,
		MeasureEndDate:   &now,
	})
	function.SetOutputExample([]*doctorModel.BloodPressureRecordForApp{
		{
			SerialNo:          11,
			SystolicPressure:  110,
			DiastolicPressure: 78,
			HeartRate:         &heartRate,
			MeasureDateTime:   &now,
			Memo:              "服用硝苯地平控释片2小时后",
			Type:              1,
		},
	})

	s.setDocFun(a, function)

	return function
}

func (s *Data) ListBloodGlucoseRecord(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.BloodGlucoseRecordDataFilterEx{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InputError, err)
		return
	}
	argument.PatientID = s.getLoginUserId(a)

	data, be := s.patientBusiness.Data().ListBloodGlucoseRecord(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Data) ListBloodGlucoseRecordDoc(a document.Assistant) document.Function {
	now := types.Time(time.Now())
	function := a.CreateFunction("获取血糖数据列表")
	function.SetNote("获取患者血糖数据列表")
	function.SetInputExample(&doctorModel.BloodGlucoseRecordDataFilter{
		MeasureStartDate: &now,
		MeasureEndDate:   &now,
	})
	function.SetOutputExample([]*doctorModel.BloodGlucoseRecordForApp{
		{
			SerialNo:        11,
			BloodGlucose:    7.8,
			Memo:            "",
			MeasureDateTime: &now,
			TimePoint:       "晨起空腹",
		},
	})

	s.setDocFun(a, function)

	return function
}

func (s *Data) ListWeightRecord(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.WeightRecordDataFilterEx{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InputError, err)
		return
	}
	argument.PatientID = s.getLoginUserId(a)

	data, be := s.patientBusiness.Data().ListWeightRecord(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Data) ListWeightRecordDoc(a document.Assistant) document.Function {
	now := types.Time(time.Now())
	function := a.CreateFunction("获取体重数据列表")
	function.SetNote("获取患者体重数据列表")
	function.SetInputExample(&doctorModel.WeightRecordDataFilter{
		MeasureStartDate: &now,
		MeasureEndDate:   &now,
	})
	function.SetOutputExample([]*doctorModel.WeightRecordForApp{
		{
			SerialNo:        11,
			Weight:          60.5,
			MeasureDateTime: &now,
			Memo:            "有氧运动半小时后",
			Type:            1,
		},
	})

	s.setDocFun(a, function)

	return function
}

func (s *Data) ListWeightBloodRecord(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.ViewWeightPageDataFilterEx{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InputError, err)
		return
	}
	argument.PatientID = s.getLoginUserId(a)

	data, be := s.patientBusiness.Data().ListWeightBloodRecord(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Data) ListWeightBloodRecordDoc(a document.Assistant) document.Function {
	now := types.Time(time.Now())
	function := a.CreateFunction("获取体重和血压血糖数据列表")
	function.SetNote("获取患者体重和血压血糖数据列表")
	function.SetInputExample(&doctorModel.ViewWeightPageDataFilter{
		MeasureStartDate: &now,
		MeasureEndDate:   &now,
	})
	function.SetOutputExample([]*doctorModel.ViewWeightPage{
		{
			SerialNo:        11,
			Weight:          60.5,
			MeasureDateTime: &now,
			Memo:            "有氧运动半小时后",
		},
	})

	s.setDocFun(a, function)

	return function
}

func (s *Data) ListDrugRecord(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.DrugRecordDataFilterEx{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InputError, err)
		return
	}
	argument.PatientID = s.getLoginUserId(a)

	data, be := s.patientBusiness.Data().ListDrugRecord(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Data) ListDrugRecordDoc(a document.Assistant) document.Function {
	now := types.Time(time.Now())
	function := a.CreateFunction("获取服药数据列表")
	function.SetNote("获取患者服药数据列表")
	function.SetInputExample(&doctorModel.DrugRecordDataFilter{
		UseStartDate: &now,
		UseEndDate:   &now,
	})
	function.SetOutputExample([]*doctorModel.DrugRecordForApp{
		{
			SerialNo:    "11,12",
			DrugName:    "硝苯地平,依普纳利",
			Dosage:      "5mg,10mg",
			UseDateTime: &now,
			Memo:        "",
			Type:        1,
		},
	})

	s.setDocFun(a, function)

	return function
}

func (s *Data) ListDietRecord(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.SportDietRecordDataFilterEx{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InputError, err)
		return
	}
	argument.PatientID = s.getLoginUserId(a)

	data, be := s.patientBusiness.Data().ListDietRecord(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Data) ListDietRecordDoc(a document.Assistant) document.Function {
	now := types.Time(time.Now())
	function := a.CreateFunction("获取饮食数据列表")
	function.SetNote("获取患者饮食数据列表")
	function.SetInputExample(&doctorModel.SportDietRecordDataFilter{
		HappenStartDate: &now,
		HappenEndDate:   &now,
	})
	function.SetOutputExample([]*doctorModel.DietRecordForApp{
		{
			SerialNo:       "11,12",
			Kinds:          "主食,肉类",
			Appetite:       "80g,100g",
			HappenDateTime: &now,
			Memo:           "",
			Type:           1,
		},
	})

	s.setDocFun(a, function)

	return function
}

func (s *Data) GetDietRecord(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.SportDietRecordFilter{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InputError, err)
		return
	}

	data, be := s.patientBusiness.Data().GetDietRecord(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Data) GetDietRecordDoc(a document.Assistant) document.Function {
	now := types.Time(time.Now())
	function := a.CreateFunction("获取饮食数据列表")
	function.SetNote("获取患者饮食数据列表")
	function.SetInputExample(&doctorModel.SportDietRecordFilter{
		SerialNo: 1,
	})
	function.SetOutputExample(&doctorModel.DietRecordForApp{
		SerialNo:       "11,12",
		Kinds:          "主食,肉类",
		Appetite:       "80g,100g",
		HappenDateTime: &now,
		Memo:           "",
		Type:           1,
	})

	s.setDocFun(a, function)

	return function
}

func (s *Data) ListSportRecord(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.SportDietRecordDataFilterEx{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InputError, err)
		return
	}
	argument.PatientID = s.getLoginUserId(a)

	data, be := s.patientBusiness.Data().ListSportRecord(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Data) ListSportRecordDoc(a document.Assistant) document.Function {
	now := types.Time(time.Now())
	function := a.CreateFunction("获取运动数据列表")
	function.SetNote("获取患者运动数据列表")
	function.SetInputExample(&doctorModel.SportDietRecordDataFilter{
		HappenStartDate: &now,
		HappenEndDate:   &now,
	})
	function.SetOutputExample([]*doctorModel.SportRecordForApp{
		{
			SerialNo:       11,
			StepCount:      0,
			HappenDateTime: &now,
			Memo:           "",
			SportsType:     "慢跑",
			DurationTime:   20,
			Intensity:      "中",
		},
	})

	s.setDocFun(a, function)

	return function
}

func (s *Data) ListDiscomfortRecord(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.DiscomfortRecordDataFilterEx{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InputError, err)
		return
	}
	argument.PatientID = s.getLoginUserId(a)

	data, be := s.patientBusiness.Data().ListDiscomfortRecord(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Data) ListDiscomfortRecordDoc(a document.Assistant) document.Function {
	now := types.Time(time.Now())
	function := a.CreateFunction("获取不适记录列表")
	function.SetNote("获取患者不适记录列表")
	function.SetInputExample(&doctorModel.DiscomfortRecordDataFilter{
		HappenStartDate: &now,
		HappenEndDate:   &now,
	})
	function.SetOutputExample([]*doctorModel.DiscomfortRecordForApp{
		{
			SerialNo:       11,
			Discomfort:     "剧烈头痛,恶心呕吐",
			HappenDateTime: &now,
			Memo:           "",
		},
	})

	s.setDocFun(a, function)

	return function
}

func (s *Data) ListStepRecord(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.SportDietRecordDataFilterEx{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InputError, err)
		return
	}
	argument.PatientID = s.getLoginUserId(a)

	data, be := s.patientBusiness.Data().ListStepRecord(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Data) ListStepRecordDoc(a document.Assistant) document.Function {
	now := types.Time(time.Now())
	function := a.CreateFunction("获取步行数据列表")
	function.SetNote("获取患者步行数据列表")
	function.SetInputExample(&doctorModel.SportDietRecordDataFilter{
		HappenStartDate: &now,
		HappenEndDate:   &now,
	})
	function.SetOutputExample([]*doctorModel.StepRecordData{
		{
			SerialNo:       11,
			StepCount:      3699,
			HappenDateTime: &now,
			Memo:           "",
		},
	})

	s.setDocFun(a, function)

	return function
}

func (s *Data) OneDayStepRecord(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.StepRecordDataFilter{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InputError, err)
		return
	}
	arg := &doctorModel.SportDietRecordDataFilterEx{}
	arg.PatientID = s.getLoginUserId(a)
	arg.HappenStartDate = argument.HappenDate
	arg.HappenEndDate = argument.HappenDate

	data, be := s.patientBusiness.Data().OneDayStepRecord(arg)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Data) OneDayStepRecordDoc(a document.Assistant) document.Function {
	now := types.Time(time.Now())
	function := a.CreateFunction("获取某天的步行数据")
	function.SetNote("获取患者某天的步行数据")
	function.SetInputExample(&doctorModel.StepRecordDataFilter{
		HappenDate: &now,
	})
	function.SetOutputExample(&doctorModel.WeRunData{
		Step: 11,
	})
	s.setDocFun(a, function)

	return function
}
func (s *Data) DecryptWeRunData(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.WeRunCryptDataInput{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InputError, err)
		return
	}
	argument.PatientID = s.getLoginUserId(a)

	data, be := s.patientBusiness.Data().DecryptWeRunData(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Data) DecryptWeRunDataDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("解码微信运动步数")
	function.SetNote("解码微信运动步数")
	function.SetInputExample(&doctorModel.WeRunCryptDataInput{
		EncryptedData: "",
		Iv:            "",
		SessionKey:    "",
	})
	function.SetOutputExample(&doctorModel.WeRunData{
		TimeStamp: 0,
		Step:      0,
	})
	s.setDocFun(a, function)

	return function
}

func (s *Data) TodayRecord(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.PatientInfoBase{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InputError, err)
		return
	}
	argument.PatientID = s.getLoginUserId(a)

	data, be := s.patientBusiness.Data().TodayRecord(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Data) TodayRecordDoc(a document.Assistant) document.Function {
	heartRate := uint64(65)
	now := types.Time(time.Now())
	function := a.CreateFunction("获取患者今天记录的所有数据")
	function.SetNote("获取患者今天记录的所有数据")
	function.SetInputExample(nil)
	function.SetOutputExample(&doctorModel.UserRecordCollection{
		BloodPressureRecords: []*doctorModel.BloodPressureRecordForApp{
			{
				SerialNo:          11,
				SystolicPressure:  110,
				DiastolicPressure: 78,
				HeartRate:         &heartRate,
				MeasureDateTime:   &now,
				Memo:              "服用硝苯地平控释片2小时后",
				Type:              1,
			},
		},
		WeightRecords: []*doctorModel.WeightRecordForApp{
			{
				SerialNo:        12,
				Weight:          60.5,
				MeasureDateTime: &now,
				Memo:            "有氧运动半小时后",
				Type:            1,
			},
		},
		DrugRecords: []*doctorModel.DrugRecordForApp{
			{
				SerialNo:    "11,12",
				DrugName:    "硝苯地平,依普纳利",
				Dosage:      "5mg,10mg",
				UseDateTime: &now,
				Memo:        "",
				Type:        1,
			},
		},
		DietRecords: []*doctorModel.DietRecordForApp{
			{
				SerialNo:       "13,14",
				Kinds:          "主食,肉类",
				Appetite:       "80g,100g",
				HappenDateTime: &now,
				Memo:           "",
				Type:           1,
			},
		},
		SportRecords: []*doctorModel.SportRecordForApp{
			{
				SerialNo:       12,
				SportsType:     "跳绳",
				DurationTime:   20,
				Intensity:      "中",
				StepCount:      0,
				Memo:           "",
				HappenDateTime: &now,
			},
		},
		DiscomfortRecords: []*doctorModel.DiscomfortRecordForApp{
			{
				SerialNo:       15,
				Discomfort:     "剧烈头痛,恶心呕吐,胸痛,四肢麻木无力,语言不清",
				Memo:           "",
				HappenDateTime: &now,
			},
		},
	})
	s.setDocFun(a, function)

	return function
}

func (s *Data) MonthlyReport(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.ViewMonthlyReportFilterEx{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InputError, err)
		return
	}
	argument.PatientID = s.getLoginUserId(a)

	data, be := s.patientBusiness.Data().MonthlyReport(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Data) MonthlyReportDoc(a document.Assistant) document.Function {
	sbp := uint64(138)
	dbp := uint64(88)
	hr := uint64(60)
	times := uint64(12)
	height := uint64(168)
	weight := float64(60.5)
	bmi := float64(24.0)
	now := types.Time(time.Now())
	function := a.CreateFunction("获取患者的月报数据")
	function.SetNote("获取患者指定月份的月报数据")
	function.SetInputExample(&doctorModel.ViewMonthlyReportFilter{
		YearMonth: "2018-09",
	})
	function.SetOutputExample(&doctorModel.MonthlyReportForApp{
		ViewMonthlyReport: doctorModel.ViewMonthlyReport{
			BpMeasuredTimes:     58,
			BpPlanedTimes:       60,
			WeightMeasuredTimes: 30,
			WeightPlanedTimes:   30,
			DrugTakenTimes:      10,
			DrugPlanedTimes:     30,
			HrMeasuredTimes:     58,
			DaysInManagement:    169,
			CurrentBP:           "136/88",
			GoalBP:              "140/90",
			MaxSBP:              &sbp,
			MaxSBPTime:          &now,
			MinSBP:              &sbp,
			MinSBPTime:          &now,
			MaxDBP:              &dbp,
			MaxDBPTime:          &now,
			MinDBP:              &dbp,
			MinDBPTime:          &now,
			MaxRange:            &sbp,
			MaxRangeTime:        &now,
			MinRange:            &dbp,
			MinRangeTime:        &now,
			AvgSBP:              &sbp,
			AvgDBP:              &dbp,
			AvgRange:            &dbp,
			BpNormalTimes:       &times,
			BpHigherTimes:       &times,
			BpLowerTimes:        &times,
			MaxHR:               &hr,
			MaxHRTime:           &now,
			MinHR:               &hr,
			MinHRTime:           &now,
			AvgHR:               &hr,
			HrNormalTimes:       &times,
			HrHigherTimes:       &times,
			HrLowerTimes:        &times,
			CurrentHeight:       &height,
			CurrentWeight:       &weight,
			TargetWeight:        &weight,
			CurrentBMI:          &bmi,
		},
		SBPLine: []doctorModel.RecordDataPoint{
			{
				Value: 138,
				Date:  "2018-09-08",
			},
		},
		DBPLine: []doctorModel.RecordDataPoint{
			{
				Value: 98,
				Date:  "2018-09-08",
			},
		},
		HRLine: []doctorModel.RecordDataPoint{
			{
				Value: 60,
				Date:  "2018-09-08",
			},
		},
		BMILine: []doctorModel.RecordDataPoint{
			{
				Value: 22.6,
				Date:  "2018-09-11",
			},
		},
	})
	s.setDocFun(a, function)

	return function
}

func (s *Data) PatientLogined(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.PatientInfoBase{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InputError, err)
		return
	}
	argument.PatientID = s.getLoginUserId(a)

	data, be := s.patientBusiness.Data().PatientLogined(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Data) PatientLoginedDoc(a document.Assistant) document.Function {
	heartRate := uint64(65)
	now := types.Time(time.Now())
	function := a.CreateFunction("获取用户登录后的数据")
	function.SetNote("获取用户登录后的数据")
	function.SetInputExample(nil)
	function.SetOutputExample(&doctorModel.LoginedDataForApp{
		ManagementPlan: doctorModel.ManagementPlanForApp{
			GoalSBP:          "140",
			GoalDBP:          "90",
			GoalBMI:          "24",
			BPTasks:          []doctorModel.BPTask{{0}, {1}, {2}},
			WeightTasks:      []doctorModel.WeightTask{{0}},
			DrugTasks:        []doctorModel.DrugTask{},
			DrugSuggestion:   "",
			DietSuggestion:   "低糖少盐",
			SportsSuggestion: "每周运动一次",
			CreateDate:       &now,
		},
		LoginUserInfo: doctorModel.PatientUserInfoForApp{
			PatientID:          80,
			Name:               "李二毛",
			Sex:                "男",
			DateOfBirth:        &now,
			IdentityCardNumber: "330106199909099876",
			PhoneNumber:        "12345678901",
			Photo:              "",
			NewestHeight:       170,
			NewestWeight:       60.5,
			Nickname:           "狗蛋",
			RegistDate:         &now,
			UUID:               "",
		},
		TodayRecords: doctorModel.UserRecordCollection{
			BloodPressureRecords: []*doctorModel.BloodPressureRecordForApp{
				{
					SerialNo:          11,
					SystolicPressure:  110,
					DiastolicPressure: 78,
					HeartRate:         &heartRate,
					MeasureDateTime:   &now,
					Memo:              "服用硝苯地平控释片2小时后",
					Type:              1,
				},
			},
			WeightRecords: []*doctorModel.WeightRecordForApp{
				{
					SerialNo:        12,
					Weight:          60.5,
					MeasureDateTime: &now,
					Memo:            "有氧运动半小时后",
					Type:            1,
				},
			},
			DrugRecords: []*doctorModel.DrugRecordForApp{
				{
					SerialNo:    "11,12",
					DrugName:    "硝苯地平,依普纳利",
					Dosage:      "5mg,10mg",
					UseDateTime: &now,
					Memo:        "",
					Type:        1,
				},
			},
			DietRecords: []*doctorModel.DietRecordForApp{
				{
					SerialNo:       "13,14",
					Kinds:          "主食,肉类",
					Appetite:       "80g,100g",
					HappenDateTime: &now,
					Memo:           "",
					Type:           1,
				},
			},
			SportRecords: []*doctorModel.SportRecordForApp{
				{
					SerialNo:       12,
					SportsType:     "跳绳",
					DurationTime:   20,
					Intensity:      "中",
					StepCount:      0,
					Memo:           "",
					HappenDateTime: &now,
				},
			},
			DiscomfortRecords: []*doctorModel.DiscomfortRecordForApp{
				{
					SerialNo:       15,
					Discomfort:     "剧烈头痛,恶心呕吐,胸痛,四肢麻木无力,语言不清",
					Memo:           "",
					HappenDateTime: &now,
				},
			},
		},
	})

	s.setDocFun(a, function)

	return function
}

func (s *Data) CheckAppUpdate(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.AppVersionRecordFromUser{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}

	argument.PatientID = s.getLoginUserId(a)
	data, be := s.patientBusiness.Data().CheckAppUpdate(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Data) CheckAppUpdateDoc(a document.Assistant) document.Function {
	now := types.Time(time.Now())
	function := a.CreateFunction("检查App版本更新")
	function.SetNote("检查用户当前App版本更新情况")
	function.SetInputExample(&doctorModel.AppVersionRecordBase{
		VersionCode: 1,
	})
	function.SetOutputExample(&doctorModel.AppVersionRecord{
		AppVersionRecordBase: doctorModel.AppVersionRecordBase{
			VersionCode: 2,
		},
		VersionName:    "1.0.0.2",
		UpdateContent:  "【更新】：",
		UpdateDate:     &now,
		CompMinVersion: 1,
	})

	s.setDocFun(a, function)

	return function
}
