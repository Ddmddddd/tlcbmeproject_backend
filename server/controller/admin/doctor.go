package admin

import (
	"tlcbme_project/business/doctor"
	"tlcbme_project/database/memory"
	"tlcbme_project/server/config"
	"tlcbme_project/server/controller"
	"github.com/ktpswjz/httpserver/document"
	"github.com/ktpswjz/httpserver/types"
)

type Doctor struct {
	controller.Controller
	controller.Enum
	doc

	doctorBusiness doctor.Business
}

func NewDoctor(cfg *config.Config, log types.Log, dbToken memory.Token, doctorBusiness doctor.Business) *Doctor {
	instance := &Doctor{doctorBusiness: doctorBusiness}
	instance.SetLog(log)
	instance.Config = cfg
	instance.DbToken = dbToken
	instance.GetCatalog = instance.getEnumCatalog

	return instance
}

func (s *Doctor) setDocFun(a document.Assistant, fun document.Function) {
	catalog := s.rootCatalog(a).
		CreateChild("网站管理", "网站管理相关接口").
		CreateChild("医生端网站", "医生端网站管理相关接口")
	catalog.SetFunction(fun)
}
