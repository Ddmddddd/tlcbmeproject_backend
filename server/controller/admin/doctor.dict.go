package admin

import (
	doctorModel "tlcbme_project/data/model/doctor"
	"tlcbme_project/errors"
	"fmt"
	"github.com/ktpswjz/httpserver/document"
	"github.com/ktpswjz/httpserver/router"
	"net/http"
	"strings"
)

func (s *Doctor) setDictDocFun(a document.Assistant, fun document.Function, name string) {
	catalog := s.rootCatalog(a).
		CreateChild("网站管理", "网站管理相关接口").
		CreateChild("医生端网站", "医生端网站管理相关接口").
		CreateChild("字典信息", "医生端网站字典相关接口").
		CreateChild(name, "")
	catalog.SetFunction(fun)
}

// 机构字典
func (s *Doctor) CreateDictOrg(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.OrgDictCreate{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InputError, err)
		return
	}
	if strings.TrimSpace(argument.OrgCode) == "" {
		a.Error(errors.InputInvalid, "机构代码为空")
		return
	}
	if argument.OrgName == "" {
		a.Error(errors.InputInvalid, "机构名称为空")
		return
	}

	data, be := s.doctorBusiness.Dict().CreateOrg(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Doctor) CreateDictOrgDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("新建")
	function.SetNote("新建字典项，并返回字典项序列号")
	function.SetInputExample(&doctorModel.OrgDictCreate{
		OrgCode: "2894782947239239",
		OrgName: "第一人民医院",
	})
	function.SetOutputExample(111)

	s.setDictDocFun(a, function, "机构字典")

	return function
}

func (s *Doctor) DeleteDictOrg(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.DictFilter{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InputError, err)
		return
	}
	if argument.SerialNo == nil {
		a.Error(errors.InputInvalid, "序列号为空")
	}
	if *argument.SerialNo < 1 {
		a.Error(errors.InputInvalid, "序列号无效")
		return
	}

	data, be := s.doctorBusiness.Dict().DeleteOrg(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Doctor) UDeleteDictOrgDoc(a document.Assistant) document.Function {
	serialNo := uint64(1)

	function := a.CreateFunction("删除")
	function.SetNote("删除字典项，并返回被删除的字典项数量")
	function.SetInputExample(&doctorModel.DictFilter{
		SerialNo: &serialNo,
	})
	function.SetOutputExample(1)

	s.setDictDocFun(a, function, "机构字典")

	return function
}

func (s *Doctor) UpdateDictOrg(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.OrgDictUpdate{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InputError, err)
		return
	}
	if argument.SerialNo < 1 {
		a.Error(errors.InputInvalid, "序列号无效")
		return
	}
	if argument.OrgName == "" {
		a.Error(errors.InputInvalid, "机构名称为空")
		return
	}

	data, be := s.doctorBusiness.Dict().EditOrg(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Doctor) UpdateDictOrgDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("修改")
	function.SetNote("修改字典项，并返回字典项序列号")
	function.SetInputExample(&doctorModel.OrgDictUpdate{
		SerialNo: 1,
		OrgName:  "第一人民医院",
	})
	function.SetOutputExample(111)

	s.setDictDocFun(a, function, "机构字典")

	return function
}

func (s *Doctor) ListDictOrg(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.DictKeywordFilter{}
	a.GetArgument(r, argument)
	data, be := s.doctorBusiness.Dict().ListOrg(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Doctor) ListDictOrgDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("列表")
	function.SetNote("获取字典列表")
	function.SetInputExample(&doctorModel.DictKeywordFilter{
		Keyword: "yy",
	})
	function.SetOutputExample([]*doctorModel.OrgDict{
		{
			OrgCode: "2894782947239239",
			OrgName: "第一人民医院",
		},
	})

	s.setDictDocFun(a, function, "机构字典")

	return function
}

func (s *Doctor) CreateDictDevice(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.DeviceDictCreate{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InputError, err)
		return
	}
	if strings.TrimSpace(argument.OrgCode) == "" {
		a.Error(errors.InputInvalid, "机构代码为空")
		return
	}
	if argument.DeviceCode == "" {
		a.Error(errors.InputInvalid, "设备id为空")
		return
	}
	if argument.DeviceName == "" {
		a.Error(errors.InputInvalid, "设备名称为空")
		return
	}
	if argument.DeviceType == "" {
		a.Error(errors.InputInvalid, "设备类型为空")
		return
	}

	data, be := s.doctorBusiness.Dict().CreateDevice(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Doctor) CreateDictDeviceDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("新建")
	function.SetNote("新建字典项，并返回字典项序列号")
	function.SetInputExample(&doctorModel.DeviceDictCreate{
		OrgCode:    "2894782947239239",
		DeviceName: "悦奇血压计",
		DeviceType: "血压计",
		DeviceCode: "200101",
	})
	function.SetOutputExample(111)

	s.setDictDocFun(a, function, "设备字典")

	return function
}

func (s *Doctor) ListDictDevice(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.DictKeywordFilter{}
	a.GetArgument(r, argument)
	data, be := s.doctorBusiness.Dict().ListDevice(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Doctor) ListDictDeviceDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("列表")
	function.SetNote("获取字典列表")
	function.SetInputExample(&doctorModel.DictKeywordFilter{
		Keyword: "yy",
	})
	function.SetOutputExample([]*doctorModel.DeviceDict{
		{
			OrgCode: "2894782947239239",
		},
	})

	s.setDictDocFun(a, function, "设备字典")

	return function
}

func (s *Doctor) SearchDivisionTree(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.DivisionDictTreeFilter{}
	a.GetArgument(r, argument)
	data, be := s.doctorBusiness.Dict().SearchDivisionTree(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Doctor) SearchDivisionTreeDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("树")
	function.SetNote("获取字典列树")
	function.SetInputExample(&doctorModel.DivisionDictTreeFilter{
		Admin: true,
	})
	function.SetOutputExample([]*doctorModel.DivisionDictTreeAdmin{
		{
			DivisionDictTreeItem: doctorModel.DivisionDictTreeItem{
				ItemCode: "110000000000",
				ItemName: "北京",
				FullName: "北京市",
			},
			Children: []*doctorModel.DivisionDictTreeAdmin{
				{
					DivisionDictTreeItem: doctorModel.DivisionDictTreeItem{
						ItemCode: "110101000000",
						ItemName: "东城",
						FullName: "东城区",
					},
					Children: []*doctorModel.DivisionDictTreeAdmin{},
				},
				{
					DivisionDictTreeItem: doctorModel.DivisionDictTreeItem{
						ItemCode: "110102000000",
						ItemName: "西城",
						FullName: "西城区",
					},
					Children: []*doctorModel.DivisionDictTreeAdmin{},
				},
			},
		},
	})

	s.setDictDocFun(a, function, "行政区划字典")

	return function
}

func (s *Doctor) SearchDivisionParentCodes(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.DivisionDictDeleteFilter{}
	a.GetArgument(r, argument)
	data, be := s.doctorBusiness.Dict().SearchDivisionParentCodes(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Doctor) SearchDivisionParentCodesDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("获取父级代码")
	function.SetNote("获取父级代码")
	function.SetInputExample(&doctorModel.DivisionDictDeleteFilter{
		ItemCode: "0",
	})

	s.setDictDocFun(a, function, "行政区划字典")

	return function
}

func (s *Doctor) CreateDivision(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.DivisionDictCreate{}
	a.GetArgument(r, argument)
	be := s.doctorBusiness.Dict().CreateDivision(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(argument)
}

func (s *Doctor) CreateDivisionDoc(a document.Assistant) document.Function {
	parent := "110000000000"
	argument := &doctorModel.DivisionDictCreate{
		Parent: &parent,
		Items: []*doctorModel.DivisionBaseInfo{
			{
				ItemCode: "110101000000",
				ItemName: "东城",
				FullName: "东城区",
			},
			{
				ItemCode: "110102000000",
				ItemName: "西城",
				FullName: "西城区",
			},
			{
				ItemCode: "110105000000",
				ItemName: "朝阳",
				FullName: "朝阳区",
			},
		},
	}

	function := a.CreateFunction("新建")
	function.SetNote("新建字典项目")
	function.SetInputExample(argument)
	function.SetOutputExample(argument)

	s.setDictDocFun(a, function, "行政区划字典")

	return function
}

func (s *Doctor) DeleteDivision(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.DivisionDictFilter{}
	a.GetArgument(r, argument)
	data, be := s.doctorBusiness.Dict().DeleteDivision(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Doctor) DeleteDivisionDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("删除")
	function.SetNote("删除字典项目")
	function.SetInputExample(&doctorModel.DivisionDictDeleteFilter{
		ItemCode: "0",
	})

	s.setDictDocFun(a, function, "行政区划字典")

	return function
}

func (s *Doctor) DeleteDictDevice(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.DictFilter{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InputError, err)
		return
	}
	if argument.SerialNo == nil {
		a.Error(errors.InputInvalid, "序列号为空")
	}
	if *argument.SerialNo < 1 {
		a.Error(errors.InputInvalid, "序列号无效")
		return
	}

	data, be := s.doctorBusiness.Dict().DeleteDevice(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Doctor) UDeleteDictDeviceDoc(a document.Assistant) document.Function {
	serialNo := uint64(1)

	function := a.CreateFunction("删除")
	function.SetNote("删除字典项，并返回被删除的字典项数量")
	function.SetInputExample(&doctorModel.DictFilter{
		SerialNo: &serialNo,
	})
	function.SetOutputExample(1)

	s.setDictDocFun(a, function, "设备字典")

	return function
}

func (s *Doctor) UpdateDictDevice(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.DeviceDictUpdate{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InputError, err)
		return
	}
	if argument.SerialNo < 1 {
		a.Error(errors.InputInvalid, "序列号无效")
		return
	}
	if argument.OrgCode == "" {
		a.Error(errors.InputInvalid, "机构名称为空")
		return
	}
	if argument.DeviceType == "" {
		a.Error(errors.InputInvalid, "设备类别为空")
		return
	}
	if argument.DeviceCode == "" {
		a.Error(errors.InputInvalid, "设备代码为空")
		return
	}

	data, be := s.doctorBusiness.Dict().EditDevice(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Doctor) UpdateDictDeviceDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("修改")
	function.SetNote("修改字典项，并返回字典项序列号")
	function.SetInputExample(&doctorModel.OrgDictUpdate{
		SerialNo: 1,
		OrgName:  "第一人民医院",
	})
	function.SetOutputExample(111)

	s.setDictDocFun(a, function, "机构字典")

	return function
}

func (s *Doctor) CreateDictDrug(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.DrugDictCreate{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InputError, err)
		return
	}
	if strings.TrimSpace(argument.ItemName) == "" {
		a.Error(errors.InputInvalid, "药物名称为空")
		return
	}
	if strings.TrimSpace(argument.Units) == "" {
		a.Error(errors.InputInvalid, "药物units为空")
		return
	}
	if fmt.Sprint(argument.ItemCode) == "" {
		a.Error(errors.InputInvalid, "药物code为空")
		return
	}
	if strings.TrimSpace(argument.Effect) == "" {
		a.Error(errors.InputInvalid, "药物Effect为空")
		return
	}
	if strings.TrimSpace(argument.InputCode) == "" {
		a.Error(errors.InputInvalid, "药物InputCode为空")
		return
	}
	if strings.TrimSpace(argument.Specification) == "" {
		a.Error(errors.InputInvalid, "药物Specification为空")
		return
	}

	data, be := s.doctorBusiness.Dict().CreateDrug(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Doctor) CreateDictDrugDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("新建")
	function.SetNote("新建字典项，并返回字典项序列号")
	function.SetInputExample(&doctorModel.DrugDictCreate{
		InputCode:     "xbdpp",
		ItemCode:      "5",
		ItemName:      "硝苯地平片",
		Specification: "10mg×100片/瓶",
		Units:         "mg,片",
		Effect:        "降压",
	})
	function.SetOutputExample(111)

	s.setDictDocFun(a, function, "药物字典")

	return function
}

func (s *Doctor) ListDictDrug(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.DictKeywordFilter{}
	a.GetArgument(r, argument)
	data, be := s.doctorBusiness.Dict().ListDrugAdmin(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Doctor) ListDictDrugDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("列表")
	function.SetNote("获取字典列表")
	function.SetInputExample(&doctorModel.DictKeywordFilter{
		Keyword: "yy",
	})
	function.SetOutputExample([]*doctorModel.DrugDict{
		{
			InputCode:     "xbdpp",
			ItemCode:      5,
			ItemName:      "硝苯地平片",
			Specification: "10mg×100片/瓶",
			Units:         "mg,片",
			Effect:        "降压",
		},
	})

	s.setDictDocFun(a, function, "药物字典")

	return function
}

func (s *Doctor) UpdateDictDrug(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.DrugDictUpdate{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InputError, err)
		return
	}
	if argument.SerialNo < 1 {
		a.Error(errors.InputInvalid, "序列号无效")
		return
	}
	if argument.Units == "" {
		a.Error(errors.InputInvalid, "机构units为空")
		return
	}
	if argument.Specification == "" {
		a.Error(errors.InputInvalid, "设备Specification为空")
		return
	}
	if argument.ItemName == "" {
		a.Error(errors.InputInvalid, "设备ItemName为空")
		return
	}
	if argument.InputCode == "" {
		a.Error(errors.InputInvalid, "设备InputCode为空")
		return
	}
	if argument.Effect == "" {
		a.Error(errors.InputInvalid, "设备Effect为空")
		return
	}

	data, be := s.doctorBusiness.Dict().EditDrug(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Doctor) UpdateDictDrugDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("修改")
	function.SetNote("修改字典项，并返回字典项序列号")
	function.SetInputExample(&doctorModel.DrugDictUpdate{
		SerialNo: 1,
	})
	function.SetOutputExample(111)

	s.setDictDocFun(a, function, "药物字典")

	return function
}

func (s *Doctor) DeleteDictDrug(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.DictFilter{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InputError, err)
		return
	}
	if argument.SerialNo == nil {
		a.Error(errors.InputInvalid, "序列号为空")
	}
	if *argument.SerialNo < 1 {
		a.Error(errors.InputInvalid, "序列号无效")
		return
	}

	data, be := s.doctorBusiness.Dict().DeleteDrug(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Doctor) UDeleteDictDrugDoc(a document.Assistant) document.Function {
	serialNo := uint64(1)

	function := a.CreateFunction("删除")
	function.SetNote("删除字典项，并返回被删除的字典项数量")
	function.SetInputExample(&doctorModel.DictFilter{
		SerialNo: &serialNo,
	})
	function.SetOutputExample(1)

	s.setDictDocFun(a, function, "设备字典")

	return function
}
