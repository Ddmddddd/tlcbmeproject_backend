package admin

import (
	"bytes"
	"tlcbme_project/data/model/admin"
	"tlcbme_project/database/memory"
	"tlcbme_project/errors"
	"tlcbme_project/server/config"
	"tlcbme_project/server/controller"
	"fmt"
	"github.com/ktpswjz/httpserver/archive"
	"github.com/ktpswjz/httpserver/document"
	"github.com/ktpswjz/httpserver/router"
	"github.com/ktpswjz/httpserver/types"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"path/filepath"
	"time"
)

type Service struct {
	controller.Controller
	doc

	bootTime time.Time
}

func NewService(cfg *config.Config, log types.Log, dbToken memory.Token) *Service {
	instance := &Service{}
	instance.Config = cfg
	instance.SetLog(log)
	instance.DbToken = dbToken
	instance.bootTime = time.Now()

	return instance
}

func (s *Service) setDocFun(a document.Assistant, fun document.Function) {
	catalog := s.rootCatalog(a).CreateChild("服务管理", "后台服务相关接口")
	catalog.SetFunction(fun)
}

func (s *Service) GetInfo(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	data := &admin.ServiceInfo{BootTime: types.Time(s.bootTime)}
	if s.Config != nil {
		args := s.Config.GetArgs()
		if args != nil {
			data.Name = args.ModuleName()
			data.Version = args.ModuleVersion().ToString()
			data.Remark = args.ModuleRemark()
		}
		if a.CanRestart() {
			if s.Config.Service != "" {
				data.Name = s.Config.Service
			}
		}
	}

	a.Success(data)
}

func (s *Service) GetInfoDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("获取服务信息")
	function.SetNote("获取当前服务信息")
	function.SetOutputExample(&admin.ServiceInfo{
		Name:     "server",
		BootTime: types.Time(time.Now()),
		Version:  "1.0.1.0",
		Remark:   "XXX服务",
	})
	function.SetContentType("")

	s.setDocFun(a, function)

	return function
}

func (s *Service) GetConfig(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	a.Success(s.Config)
}

func (s *Service) GetConfigDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("获取服务配置")
	function.SetNote("获取当前服务配置")
	function.SetOutputExample(s.Config)
	function.SetContentType("")

	s.setDocFun(a, function)

	return function
}

func (s *Service) CanRestart(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	a.Success(a.CanRestart())
}

func (s *Service) CanRestartDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("是否可在线重启")
	function.SetNote("判断当前服务是否可以在线重启")
	function.SetOutputExample(true)
	function.SetContentType("")

	s.setDocFun(a, function)

	return function
}

func (s *Service) Restart(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	if !a.CanRestart() {
		a.Error(errors.NotSupport, "当前不在服务模式下运行")
		return
	}

	go func(a router.Assistant) {
		time.Sleep(2 * time.Second)
		err := a.Restart()
		if err != nil {
			s.LogError("重启服务失败:", err)
		}
		os.Exit(1)
	}(a)

	a.Success(true)
}

func (s *Service) RestartDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("重启服务")
	function.SetNote("重新启动当前服务")
	function.SetContentType("")

	s.setDocFun(a, function)

	return function
}

func (s *Service) CanUpdate(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	a.Success(a.CanUpdate())
}

func (s *Service) CanUpdateDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("是否可在线更新")
	function.SetNote("判断当前服务是否可以在线更新")
	function.SetOutputExample(true)
	function.SetContentType("")

	s.setDocFun(a, function)

	return function
}

func (s *Service) Update(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	if !s.update(w, r, a) {
		return
	}

	go func(a router.Assistant) {
		time.Sleep(2 * time.Second)
		err := a.Restart()
		if err != nil {
			s.LogError("更新服务后重启失败:", err)
		}
		os.Exit(0)
	}(a)
}

func (s *Service) update(w http.ResponseWriter, r *http.Request, a router.Assistant) bool {
	if !a.CanUpdate() {
		a.Error(errors.NotSupport, "服务不支持在线更新")
		return false
	}

	appFile, _, err := r.FormFile("file")
	if err != nil {
		a.Error(errors.InputInvalid, "invalid file: ", err)
		return false
	}
	defer appFile.Close()
	var buf bytes.Buffer
	fileSize, err := buf.ReadFrom(appFile)
	if err != nil {
		a.Error(errors.InputInvalid, "read file error: ", err)
		return false
	}
	if fileSize < 0 {
		a.Error(errors.InputInvalid, "invalid file: size is zero")
		return false
	}

	oldBinFilePath := s.Config.GetArgs().ModulePath()
	tempFolder := filepath.Join(filepath.Dir(oldBinFilePath), a.GenerateGuid())
	err = os.MkdirAll(tempFolder, 0777)
	if err != nil {
		a.Error(errors.InputInvalid, fmt.Sprintf("create temp folder '%s' error:", tempFolder), err)
		return false
	}
	defer os.RemoveAll(tempFolder)

	fileData := buf.Bytes()
	zipFile := &archive.Zip{}
	err = zipFile.DecompressMemory(fileData, tempFolder)
	if err != nil {
		a.Error(errors.InputInvalid, "decompress file error: ", err)
		return false
	}

	binFileName := s.Config.GetArgs().ModuleName()
	newBinFilePath, err := s.getBinFilePath(tempFolder, binFileName)
	if err != nil {
		a.Error(errors.InputInvalid, err)
		return false
	}
	module := &types.Module{Path: newBinFilePath}
	moduleName := module.Name()
	if moduleName != binFileName {
		a.Error(errors.InputInvalid, fmt.Sprintf("模块名称(%s)无效", moduleName))
		return false
	}
	moduleType := module.Type()
	if moduleType != s.Config.GetArgs().ModuleType() {
		a.Error(errors.InputInvalid, fmt.Sprintf("模块名称(%s)无效", moduleType))
		return false
	}

	err = os.Remove(oldBinFilePath)
	if err != nil {
		a.Error(errors.InternalError, err)
		return false
	}
	_, err = s.copyFile(newBinFilePath, oldBinFilePath)
	if err != nil {
		a.Error(errors.InternalError, err)
		return false
	}

	a.Success(nil)

	return true
}

func (s *Service) copyFile(source, dest string) (int64, error) {
	sourceFile, err := os.Open(source)
	if err != nil {
		return 0, err
	}
	defer sourceFile.Close()

	sourceFileInfo, err := sourceFile.Stat()
	if err != nil {
		return 0, err
	}

	destFile, err := os.OpenFile(dest, os.O_RDWR|os.O_CREATE|os.O_TRUNC, sourceFileInfo.Mode())
	if err != nil {
		return 0, err
	}
	defer destFile.Close()

	return io.Copy(destFile, sourceFile)
}

func (s *Service) getBinFilePath(folderPath, fileName string) (string, error) {
	paths, err := ioutil.ReadDir(folderPath)
	if err != nil {
		return "", err
	}

	for _, path := range paths {
		if path.IsDir() {
			appPath, err := s.getBinFilePath(filepath.Join(folderPath, path.Name()), fileName)
			if err != nil {
				continue
			}
			return appPath, nil
		} else {
			if path.Name() == fileName {
				return filepath.Join(folderPath, path.Name()), nil
			}
		}
	}

	return "", fmt.Errorf("服务主程序(%s)不存在", "tlcbme_project")
}
