package admin

import (
	"fmt"
	"testing"
)

type rel struct {
	// 权重
	w float64
	// nrv
	nrv float64
}
var dataMap = map[string]rel{
	"na":{w:0.0735,nrv:2000},
	"fat":{w:0.0711,nrv:60},
	"protein":{w:0.0723,nrv:60},
	"ca":{w:0.0699,nrv:800},
	"k":{w:0.0613,nrv:2000},
	"mg":{w:0.0600,nrv:300},
	"df":{w:0.0711,nrv:25},
	"fe":{w:0.0662,nrv:15},
	"va":{w:0.0650,nrv:800},
	"vb1":{w:0.0650,nrv:1.4},
	"vb2":{w:0.0650,nrv:1.4},
	"vc":{w:0.0600,nrv:100},
	"vd":{w:0.0650,nrv:5},
	"zn":{w:0.0637,nrv:15},
}
func Test_Grade(t *testing.T) {
	// SELECT ENERGY_KC,NA,FETT,PROTEIN,CA,POT,MG,DF,FE,VITA,VITB1,VITB2,VITC,VITD,ZN FROM `fooddb-local`.food
	//where Serial_No = 103
	//; food 表查出来的
	getGrade(149,164,3.14,2.02,27.6,86.7,15.54,1.26,1.5,96.7,0.02,0.05,17.3,0,0.6)
}

func getGrade(energy, na ,fat,protein,ca,k,mg,df,fe,va,vb1,vb2,vc,vd,zn float64)  {
	// 推荐
	// 蛋白质
	t1 := getTrace(protein,energy,dataMap["protein"].w,dataMap["protein"].nrv)
	// 膳食纤维
	t2 := getTrace(df,energy,dataMap["df"].w,dataMap["df"].nrv)
	// 钙
	t3 := getTrace(ca,energy,dataMap["ca"].w,dataMap["ca"].nrv)
	// 铁
	t4 := getTrace(fe,energy,dataMap["fe"].w,dataMap["fe"].nrv)
	// 锌
	t5 := getTrace(zn,energy,dataMap["zn"].w,dataMap["zn"].nrv)
	// 镁
	t6 := getTrace(mg,energy,dataMap["mg"].w,dataMap["mg"].nrv)
	// 钾
	t7 := getTrace(k,energy,dataMap["k"].w,dataMap["k"].nrv)
	// va
	t8 := getTrace(va,energy,dataMap["va"].w,dataMap["va"].nrv)
	// vb1
	t9 := getTrace(vb1,energy,dataMap["vb1"].w,dataMap["vb1"].nrv)
	// vb2
	t10 := getTrace(vb2,energy,dataMap["vb2"].w,dataMap["vb2"].nrv)
	// vc
	t11 := getTrace(vc,energy,dataMap["vc"].w,dataMap["vc"].nrv)
	// vd
	t12 := getTrace(vd,energy,dataMap["vd"].w,dataMap["vd"].nrv)

	// 限制性
	// 钠
	t13 := getTrace(na,energy,dataMap["na"].w,dataMap["na"].nrv)
	// 脂肪
	t14 := getTrace(fat,energy,dataMap["fat"].w,dataMap["fat"].nrv)

	fmt.Println("蛋白质：",t1)
	fmt.Println("膳食纤维：",t2)
	fmt.Println("钙：",t3)
	fmt.Println("铁：",t4)
	fmt.Println("锌：",t5)
	fmt.Println("镁：",t6)
	fmt.Println("钾：",t7)
	fmt.Println("va：",t8)
	fmt.Println("vb1：",t9)
	fmt.Println("vb2：",t10)
	fmt.Println("vc：",t11)
	fmt.Println("vd：",t12)
	fmt.Println("推荐营养素：",t1 + t2 + t3 + t4 + t5 + t6 + t7 + t8 + t9 + t10 + t11 + t12 )
	fmt.Println("钠：",t13)
	fmt.Println("脂肪",t14)
	fmt.Println("限制性营养素",t13 + t14)

}

func getTrace(trace,energy,w,nrv float64) float64 {
	return trace / (energy / 100) * w / nrv
}