package admin

import (
	"encoding/json"
	"fmt"
	"log"
	"reflect"
	"strconv"
	"strings"
	"testing"
	"time"
)

func TestSms_getFields(t *testing.T) {
	end := time.Now()
	start := time.Date(2021, 2, 1, 0, 0, 0, 0, time.Local)
	fmt.Println((end.Sub(start).Hours()) / 24)

}

func TestIntegralTask(t *testing.T)  {

}

func TestStringConvert(t *testing.T) {
	var i Data
	var js = `{"date1": {"DATA": "${time} 19:30"}, "thing2": {"DATA": "晚餐后2小时"}}`
	str := string(js)
	fmt.Println(str)
	err := json.Unmarshal([]byte(str), &i)
	if err != nil {
		panic(err)
	}
	day := time.Now().Format("2006-01-02")
	i.Date1.Value = strings.Replace(i.Date1.Value, "${time}", day, -1)
	fmt.Println("info: ", i)
}

func TestSet(t *testing.T) {
	m := make(map[uint64]bool)

	m[1] = false
	if _, ok := m[1]; !ok {
		fmt.Println("no")
	} else {
		fmt.Println("yes")
	}
}

type DataJson struct {
	Thing1 	thing  `json:"thing1"`
	Thing4	thing  `json:"thing4"`
}

func TestStringConvertJson(t *testing.T) {
	i := DataJson{}
	var js = `{"thing1": {"value": "饮食拍照打卡"}, "thing4": {"value": "营养师评价您的饮食了"}}`
	str := string(js)
	err := json.Unmarshal([]byte(str), &i)
	if err != nil {
		panic(err)
	}
	fmt.Println("info: ", i)
}

func TestReplace(t *testing.T) {
	newPage := strings.Replace("pages/function/input/inputfood?commentId=${commentId}", "${commentId}", strconv.FormatUint(53, 10), -1)
	fmt.Println(strconv.FormatUint(53, 10))
	fmt.Println(newPage)
}

func TestReflect(t *testing.T) {
	//fmt.Println(GetFieldName(Student{}))
	//fmt.Println(GetFieldName(&Student{}))
	fmt.Println(GetFieldName( "Name"))

	//fmt.Println(GetTagName(&Student{}))
}

type Student struct {
	Name  string `json:"name"`
	Age   int    `json:"age"`
	Grade int    `json:"grade"`
}

//获取结构体中字段的名称

func GetFieldName(val string) string {
	student := &Student{
		Name:  "张三",
		Age:   10,
		Grade: 100,
	}
	var obj interface{} = student
	v := reflect.ValueOf(obj)
	v = v.Elem()
	field := v.FieldByName(val).IsValid()
	if field {

	}
 	return "1"
	//if t.Kind() == reflect.Ptr {
	//	t = t.Elem()
	//}
	//if t.Kind() != reflect.Struct {
	//	log.Println("Check type error not Struct")
	//	return nil
	//}
	//fieldNum := t.NumField()
	//result := make([]string, 0, fieldNum)
	//for i := 0; i < fieldNum; i++ {
	//	result = append(result, t.Field(i).Name)
	//}
	//return result
}

//获取结构体中Tag的值，如果没有tag则返回字段值

func GetTagName(structName interface{}) []string {
	t := reflect.TypeOf(structName)
	if t.Kind() == reflect.Ptr {
		t = t.Elem()
	}
	if t.Kind() != reflect.Struct {
		log.Println("Check type error not Struct")
		return nil
	}
	fieldNum := t.NumField()
	result := make([]string, 0, fieldNum)
	for i := 0; i < fieldNum; i++ {
		tagName := t.Field(i).Name
		tags := strings.Split(string(t.Field(i).Tag), "\"")
		if len(tags) > 1 {
			tagName = tags[1]
		}
		result = append(result, tagName)
	}
	return result
}