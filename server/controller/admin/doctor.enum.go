package admin

import (
	"github.com/ktpswjz/httpserver/document"
)

func (s *Doctor) getEnumCatalog(a document.Assistant) document.Catalog {
	catalog := s.rootCatalog(a).
		CreateChild("网站管理", "网站管理相关接口").
		CreateChild("医生端网站", "医生端网站管理相关接口").
		CreateChild("枚举列表", "医生端网站管理枚举值相关接口")

	return catalog
}
