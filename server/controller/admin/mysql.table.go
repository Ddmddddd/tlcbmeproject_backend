package admin

import (
	"crypto/sha1"
	"encoding/hex"
	"fmt"
	"sort"
	"strings"
	"time"
)

type mysqlTable struct {
	Name    string `json:"name" note:"名称"`
	Type    string `json:"type" note:"类型，VIEW或BASE TABLE"`
	Comment string `json:"comment" note:"说明"`

	UpdateTime   *time.Time `json:"updateTime" note:"最后更新时间"`
	Timestamp    uint64     `json:"timestamp" note:"最后更新时间戳"`
	Rows         *uint64    `json:"rows" note:"总行数"`
	RowsInserted *uint64    `json:"rowsInserted" note:"已插入行数"`
	RowsUpdated  *uint64    `json:"rowsUpdated" note:"已更新行数"`
	RowsDeleted  *uint64    `json:"rowsDeleted" note:"已删除行数"`

	Columns []*mysqlColumn `json:"columns" note:"列"`
}

func (s *mysqlTable) IsTable() bool {
	if strings.ToLower(s.Type) == "base table" {
		return true
	} else {
		return false
	}
}

func (s *mysqlTable) IsView() bool {
	if strings.ToLower(s.Type) == "view" {
		return true
	} else {
		return false
	}
}

func (s *mysqlTable) GetCreateSql(dropIfExist bool) string {
	columnCount := len(s.Columns)
	if columnCount < 1 {
		return ""
	}

	sb := &strings.Builder{}
	if s.IsTable() {
		if dropIfExist {
			sb.WriteString(fmt.Sprintf("DROP TABLE IF EXISTS `%s`;\r\n", s.Name))
		}

		sb.WriteString(fmt.Sprintf("CREATE TABLE `%s` (", s.Name))
		sb.WriteString(fmt.Sprintln())

		primaryKeys := make([]string, 0)
		uniqueKeys := make([]string, 0)
		for i := 0; i < columnCount; i++ {
			column := s.Columns[i]
			sb.WriteString(fmt.Sprintf("`%s` %s ", column.Name, column.Type))
			if !column.IsNullAble() {
				sb.WriteString("NOT NULL ")
			}
			if column.IsAutoIncrement() {
				sb.WriteString("AUTO_INCREMENT ")
			}
			if column.DataDefault != nil {
				sb.WriteString(fmt.Sprintf("DEFAULT '%s' ", *column.DataDefault))
			}
			if len(column.Comment) > 0 {
				sb.WriteString(fmt.Sprintf("COMMENT '%s' ", column.Comment))
			}
			if i < columnCount-1 {
				sb.WriteString(",")
				sb.WriteString(fmt.Sprintln())
			}

			if column.IsPrimaryKey() {
				primaryKeys = append(primaryKeys, fmt.Sprintf("`%s`", column.Name))
			}
			if column.IsUniqueKey() {
				uniqueKeys = append(uniqueKeys, fmt.Sprintf("%s", column.Name))
			}
		}

		if len(primaryKeys) > 0 {
			sb.WriteString(", ")
			sb.WriteString(fmt.Sprintln())
			sb.WriteString(fmt.Sprintf("PRIMARY KEY (%s) ", strings.Join(primaryKeys, ",")))
		}

		uniqueKeyCount := len(uniqueKeys)
		for i := 0; i < uniqueKeyCount; i++ {
			sb.WriteString(",")
			sb.WriteString(fmt.Sprintln())
			sb.WriteString(fmt.Sprintf("UNIQUE KEY `%s_UNIQUE` (`%s`) ", uniqueKeys[i], uniqueKeys[i]))
		}

		sb.WriteString(fmt.Sprintln())
		sb.WriteString(") ")
		if len(s.Comment) > 0 {
			sb.WriteString(fmt.Sprintf("COMMENT='%s'", s.Comment))
		}
		sb.WriteString(fmt.Sprintln())
	}

	return sb.String()
}

func (s *mysqlTable) GetDropSql() string {
	if s.IsTable() {
		return fmt.Sprintf("DROP TABLE `%s`;", s.Name)
	} else if s.IsView() {
		return fmt.Sprintf("DROP VIEW `%s`;", s.Name)
	}

	return ""
}

func (s *mysqlTable) GetDeleteSql() string {
	if s.IsTable() {
		return fmt.Sprintf("DELETE FROM `%s`;", s.Name)
	}

	return ""
}

func (s *mysqlTable) GetHash() string {
	sb := strings.Builder{}
	sb.WriteString(s.Name)

	colCount := len(s.Columns)
	if colCount > 0 {
		sortedColumns := make([]*mysqlColumn, 0)
		for colIndex := 0; colIndex < colCount; colIndex++ {
			sortedColumns = append(sortedColumns, s.Columns[colIndex])
		}
		sort.Slice(sortedColumns, func(i, j int) bool {
			return strings.Compare(sortedColumns[i].Name, sortedColumns[j].Name) > 0
		})
		for colIndex := 0; colIndex < colCount; colIndex++ {
			col := sortedColumns[colIndex]

			sb.WriteString(col.Name)
			sb.WriteString(col.Type)
			sb.WriteString(col.Key)
			sb.WriteString(col.NullAble)
			sb.WriteString(col.Extra)
			if col.DataDefault != nil {
				sb.WriteString(*col.DataDefault)
			}
		}
	}

	sha := sha1.New()
	_, err := sha.Write([]byte(sb.String()))
	if err != nil {
		return ""
	}

	hashed := sha.Sum(nil)
	return hex.EncodeToString(hashed)
}
