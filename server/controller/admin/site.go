package admin

import (
	"bytes"
	"tlcbme_project/data/model/admin"
	"tlcbme_project/database/memory"
	"tlcbme_project/errors"
	"tlcbme_project/server/config"
	"tlcbme_project/server/controller"
	"fmt"
	"github.com/ktpswjz/httpserver/archive"
	"github.com/ktpswjz/httpserver/document"
	"github.com/ktpswjz/httpserver/router"
	"github.com/ktpswjz/httpserver/types"
	"net/http"
	"os"
	"path/filepath"
	"time"
)

type Site struct {
	controller.Controller
	doc

	adminSite string
	docSite   string
}

func NewSite(cfg *config.Config, log types.Log, dbToken memory.Token, adminSite, docSite string) *Site {
	instance := &Site{}
	instance.SetLog(log)
	instance.Config = cfg
	instance.DbToken = dbToken
	instance.adminSite = adminSite
	instance.docSite = docSite

	return instance
}

func (s *Site) setDocFun(a document.Assistant, fun document.Function) {
	catalog := s.rootCatalog(a).CreateChild("网站管理", "网站管理相关接口")
	catalog.SetFunction(fun)
}

func (s *Site) GetDefaultInfo(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	s.getFixInfo(s.Config.Site.Doctor.Root, "", w, r, p, a)
}

func (s *Site) GetDefaultInfoDoc(a document.Assistant) document.Function {
	return s.getFixInfoDoc(a, "医生端")
}

func (s *Site) UploadDefault(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	s.uploadFix(s.Config.Site.Doctor.Root, w, r, p, a)
}

func (s *Site) GetAdminInfo(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	s.getFixInfo(s.Config.Site.Admin.Root, s.adminSite, w, r, p, a)
}

func (s *Site) GetAdminInfoDoc(a document.Assistant) document.Function {
	return s.getFixInfoDoc(a, "管理")
}

func (s *Site) UploadAdmin(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	s.uploadFix(s.Config.Site.Admin.Root, w, r, p, a)
}

func (s *Site) GetDocInfo(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	s.getFixInfo(s.Config.Site.Doc.Root, s.docSite, w, r, p, a)
}

func (s *Site) GetDocInfoDoc(a document.Assistant) document.Function {
	return s.getFixInfoDoc(a, "文档")
}

func (s *Site) UploadDoc(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	s.uploadFix(s.Config.Site.Doc.Root, w, r, p, a)
}

func (s *Site) getFixInfo(root, path string, w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	fi, err := os.Stat(root)
	if os.IsNotExist(err) {
		a.Error(errors.NotExist, err)
		return
	}
	if !fi.IsDir() {
		a.Error(errors.Exception, "config site root is not folder")
		return
	}

	data := &admin.SiteInfo{DeployTime: types.Time(fi.ModTime())}
	data.Version, _ = s.GetSiteVersion(root)
	data.Url = fmt.Sprintf("%s://%s%s/", a.Schema(), r.Host, path)

	a.Success(data)
}

func (s *Site) getFixInfoDoc(a document.Assistant, name string) document.Function {
	function := a.CreateFunction(fmt.Sprintf("获取%s网站信息", name))
	function.SetNote(fmt.Sprintf("获取%s网站的访问地址、版本号等信息", name))
	function.SetOutputExample(&admin.SiteInfo{
		DeployTime: types.Time(time.Now()),
		Version:    "1.0.1.0",
		Url:        "https://www.example.com",
	})
	function.SetContentType("")

	s.setDocFun(a, function)

	return function
}

func (s *Site) uploadFix(root string, w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	appFile, _, err := r.FormFile("file")
	if err != nil {
		a.Error(errors.InputInvalid, "invalid file: ", err)
		return
	}
	defer appFile.Close()
	var buf bytes.Buffer
	fileSize, err := buf.ReadFrom(appFile)
	if err != nil {
		a.Error(errors.InputInvalid, "read file error: ", err)
		return
	}
	if fileSize < 0 {
		a.Error(errors.InputInvalid, "invalid file: size is zero")
		return
	}

	tempFolder := filepath.Join(filepath.Dir(root), a.GenerateGuid())
	err = os.MkdirAll(tempFolder, 0777)
	if err != nil {
		a.Error(errors.InputInvalid, fmt.Sprintf("create temp folder '%s' error:", tempFolder), err)
		return
	}
	defer os.RemoveAll(tempFolder)

	fileData := buf.Bytes()
	zipFile := &archive.Zip{}
	err = zipFile.DecompressMemory(fileData, tempFolder)
	if err != nil {
		a.Error(errors.InputInvalid, "decompress file error: ", err)
		return
	}

	appFolder := root
	err = os.RemoveAll(appFolder)
	if err != nil {
		a.Error(errors.InputInvalid, "remove original site error:", err)
		return
	}
	os.MkdirAll(filepath.Dir(appFolder), 0777)
	err = os.Rename(tempFolder, appFolder)
	if err != nil {
		a.Error(errors.InputInvalid, fmt.Sprintf("rename folder '%s' error:", appFolder), err)
		return
	}

	a.Success(nil)
}

func (s *Site) UploadApp(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	appPath := r.FormValue("path")
	if appPath == "" {
		a.Error(errors.InputInvalid, "path name is empty")
		return
	}

	appFile, _, err := r.FormFile("file")
	if err != nil {
		a.Error(errors.InputInvalid, "invalid file: ", err)
		return
	}
	defer appFile.Close()
	var buf bytes.Buffer
	fileSize, err := buf.ReadFrom(appFile)
	if err != nil {
		a.Error(errors.InputInvalid, "read file error: ", err)
		return
	}
	if fileSize < 0 {
		a.Error(errors.InputInvalid, "invalid file: size is zero")
		return
	}

	tempFolder := filepath.Join(s.Config.Site.App.Root, a.GenerateGuid())
	err = os.MkdirAll(tempFolder, 0777)
	if err != nil {
		a.Error(errors.InputInvalid, fmt.Sprintf("create temp folder '%s' error:", tempFolder), err)
		return
	}
	defer os.RemoveAll(tempFolder)

	fileData := buf.Bytes()
	zipFile := &archive.Zip{}
	err = zipFile.DecompressMemory(fileData, tempFolder)
	if err != nil {
		a.Error(errors.InputInvalid, "decompress file error: ", err)
		return
	}

	appFolder := filepath.Join(s.Config.Site.App.Root, appPath)
	err = os.RemoveAll(appFolder)
	if err != nil {
		a.Error(errors.InputInvalid, fmt.Sprintf("remove original app '%s' error:", appPath), err)
		return
	}
	os.MkdirAll(filepath.Dir(appFolder), 0777)
	err = os.Rename(tempFolder, appFolder)
	if err != nil {
		a.Error(errors.InputInvalid, fmt.Sprintf("rename app folder '%s' error:", appFolder), err)
		return
	}
	appInfo := &admin.SiteApp{
		UploadTime: types.Time(time.Now()),
	}
	appInfo.Remark = r.FormValue("remark")
	appInfo.Version = r.FormValue("version")
	if appInfo.Version == "" {
		appInfo.Version = "1.0.1.0"
	}
	token, err := s.DbToken.Get(a.Token())
	if err == nil && token != nil {
		appInfo.UploadUser = token.UserAccount
	}
	appInfoName := filepath.Join(appFolder, "app.info")
	appInfo.SaveToFile(appInfoName)

	a.Success(appPath)
}

func (s *Site) TreeApp(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	baseUrl := fmt.Sprintf("%s://%s/app", a.Schema(), r.Host)
	appTree := &admin.SiteAppTree{}
	appTree.ParseChildren(s.Config.Site.App.Root, baseUrl)

	a.Success(appTree.Children)
}

func (s *Site) TreeAppDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("获取应用程序列表")
	function.SetNote("获取服务系统当前所有应用程序")
	function.SetOutputExample([]admin.SiteAppTree{
		{
			Path:       "test",
			UploadTime: types.Time(time.Now()),
			UploadUser: "admin",
			Version:    "1.0.1.0",
			Url:        "https://www.example.com/app/test",
		},
	})
	function.SetContentType("")

	s.setDocFun(a, function)

	return function
}

func (s *Site) DeleteApp(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	filter := &admin.SiteAppFilter{}
	err := a.GetArgument(r, filter)
	if err != nil {
		a.Error(errors.InputError, err)
		return
	}
	if filter.Path == "" {
		a.Error(errors.InputError, "应用程序路径为空")
		return
	}

	appPath := filepath.Join(s.Config.Site.App.Root, filter.Path)
	info, err := os.Stat(appPath)
	if os.IsNotExist(err) {
		a.Error(errors.InputError, fmt.Sprintf("应用程序'%s'不存在", filter.Path))
		return
	}
	if !info.IsDir() {
		a.Error(errors.InputError, fmt.Sprintf("应用程序'%s'无效", filter.Path))
		return
	}
	err = os.RemoveAll(appPath)
	if err != nil {
		a.Error(errors.Exception, err)
		return
	}

	a.Success(filter.Path)
}

func (s *Site) DeleteAppDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("删除应用程序")
	function.SetNote("删除存在的应用程序")
	function.SetInputExample(&admin.SiteAppFilter{
		Path: "test",
	})
	function.SetOutputExample("test")

	s.setDocFun(a, function)

	return function
}
