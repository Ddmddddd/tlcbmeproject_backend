package admin

import "strings"

type mysqlColumn struct {
	Name        string  `json:"name" note:"名称"`
	Type        string  `json:"type" note:"类型"`
	Comment     string  `json:"comment" note:"说明"`
	Key         string  `json:"key" note:"键值，例如：PRI标识主键"`
	NullAble    string  `json:"null_able" note:"可空，NO或YES"`
	DataType    string  `json:"data_type" note:"数据类型"`
	DataDefault *string `json:"data_default" note:"数据默认值"`
	Extra       string  `json:"extra" note:"扩展说明，例如：auto_increment"`
}

func (s *mysqlColumn) IsNullAble() bool {
	if strings.ToLower(s.NullAble) == "yes" {
		return true
	} else {
		return false
	}
}

func (s *mysqlColumn) IsAutoIncrement() bool {
	if strings.ToLower(s.Extra) == "auto_increment" {
		return true
	} else {
		return false
	}
}

func (s *mysqlColumn) IsPrimaryKey() bool {
	if strings.ToLower(s.Key) == "pri" {
		return true
	} else {
		return false
	}
}

func (s *mysqlColumn) IsUniqueKey() bool {
	if strings.ToLower(s.Key) == "uni" {
		return true
	} else {
		return false
	}
}
