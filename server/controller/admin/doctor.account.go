package admin

import (
	doctorModel "tlcbme_project/data/model/doctor"
	"tlcbme_project/errors"
	"fmt"
	"github.com/ktpswjz/httpserver/document"
	"github.com/ktpswjz/httpserver/router"
	"github.com/ktpswjz/httpserver/types"
	"net/http"
	"strings"
	"time"
)

func (s *Doctor) setAccountDocFun(a document.Assistant, fun document.Function) {
	catalog := s.rootCatalog(a).
		CreateChild("网站管理", "网站管理相关接口").
		CreateChild("医生端网站", "医生端网站管理相关接口").
		CreateChild("账号信息", "医生端网站管理账号相关接口")
	catalog.SetFunction(fun)
}

func (s *Doctor) CreateAccount(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.DoctorUserAuthsCreate{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InputError, err)
		return
	}
	if strings.TrimSpace(argument.UserName) == "" {
		a.Error(errors.InputInvalid, "用户名为空")
		return
	}
	if argument.Password == "" {
		a.Error(errors.InputInvalid, "密码为空")
		return
	}

	data, be := s.doctorBusiness.Authentication().CreateAccount(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Doctor) CreateAccountDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("新建医生账号")
	function.SetNote("新建医生账号并绑定手机号、电子邮箱等信息。成功时返回账号ID")
	function.SetInputExample(&doctorModel.DoctorUserAuthsCreate{
		UserName:    "admin",
		MobilePhone: "18866668888",
		Email:       "me@example.com",
		Password:    "***",
	})
	function.SetOutputExample(111)

	s.setAccountDocFun(a, function)

	return function
}
func (s *Doctor) CreateAccountOneTime(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.DoctorUserAuthsCreateOneTime{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InputError, err)
		return
	}
	if strings.TrimSpace(argument.UserName) == "" {
		a.Error(errors.InputInvalid, "用户名为空")
		return
	}
	if strings.TrimSpace(argument.Name) == "" {
		a.Error(errors.InputInvalid, "姓名为空")
		return
	}
	if argument.Password == "" {
		a.Error(errors.InputInvalid, "密码为空")
		return
	}
	if argument.OrgCode == "" {
		a.Error(errors.InputInvalid, "执行机构为空")
		return
	}

	data, be := s.doctorBusiness.Authentication().CreateAccountOneTime(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}
func (s *Doctor) CreateAccountOneTimeDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("一次性新建认证医生账号")
	function.SetNote("新建医生账号并绑定手机号、执行机构等信息。成功时返回账号ID")
	verifiedStatus := uint64(1)
	userType := uint64(0)
	function.SetInputExample(&doctorModel.DoctorUserAuthsCreateOneTime{
		UserName:       "admin",
		MobilePhone:    "18866668888",
		Password:       "***",
		Name:           "zhangsan",
		OrgCode:        "1234564754",
		Status:         0,
		VerifiedStatus: &verifiedStatus,
		UserType:       &userType,
	})
	function.SetOutputExample(111)

	s.setAccountDocFun(a, function)

	return function
}
func (s *Doctor) ListAccount(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	filter := &doctorModel.DoctorUserAuthsInfoFilter{}
	a.GetArgument(r, filter)

	data, be := s.doctorBusiness.Authentication().ListAccount(filter)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Doctor) ListAccountDoc(a document.Assistant) document.Function {
	now := types.Time(time.Now())
	userID := uint64(12)
	userName := "admin"
	mobilePhone := "13818765432"
	email := ""
	status := uint64(0)

	function := a.CreateFunction("获取医生账号列表")
	function.SetNote("获取所有医生账号信息")
	function.SetInputExample(&doctorModel.DoctorUserAuthsInfoFilter{
		UserID:      &userID,
		UserName:    &userName,
		MobilePhone: &mobilePhone,
		Email:       &email,
		Status:      &status,
	})
	function.SetOutputExample([]*doctorModel.DoctorUserAuthsInfo{
		{
			UserID:            11,
			UserName:          "admin",
			MobilePhone:       "18866668888",
			Email:             "me@example.com",
			Status:            0,
			RegistDateTime:    &now,
			LoginCount:        10,
			LastLoginDateTime: &now,
			UseSeconds:        24342,
		},
	})

	s.setAccountDocFun(a, function)

	return function
}

func (s *Doctor) ListAccountLike(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	filter := &doctorModel.DoctorUserAuthsInfoLikeFilter{}
	a.GetArgument(r, filter)

	data, be := s.doctorBusiness.Authentication().ListAccountLike(filter)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Doctor) ListAccountLikeDoc(a document.Assistant) document.Function {
	now := types.Time(time.Now())

	function := a.CreateFunction("获取医生账号列表(模糊匹配)")
	function.SetNote("获取所有医生账号信息")
	function.SetInputExample(&doctorModel.DoctorUserAuthsInfoLikeFilter{
		Account: "admin",
		Statuses: []uint64{
			0,
		},
	})
	function.SetOutputExample([]*doctorModel.DoctorUserAuthsInfo{
		{
			UserID:            11,
			UserName:          "admin",
			MobilePhone:       "18866668888",
			Email:             "me@example.com",
			Status:            0,
			RegistDateTime:    &now,
			LoginCount:        10,
			LastLoginDateTime: &now,
			UseSeconds:        24342,
		},
	})

	s.setAccountDocFun(a, function)

	return function
}

func (s *Doctor) EditAccount(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.DoctorUserAuthsEdit{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InputError, err)
		return
	}
	if strings.TrimSpace(argument.UserName) == "" {
		a.Error(errors.InputInvalid, "用户名为空")
		return
	}
	if argument.UserID < 1 {
		a.Error(errors.InputInvalid, "用户ID无效")
		return
	}

	data, be := s.doctorBusiness.Authentication().EditAccount(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Doctor) EditAccountDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("编辑医生账号")
	function.SetNote("编辑医生账号信息，如手机号、电子邮箱、状态等信息。成功时返回账号ID")
	function.SetInputExample(&doctorModel.DoctorUserAuthsEdit{
		UserID:      0,
		UserName:    "admin",
		MobilePhone: "18866668888",
		Email:       "me@example.com",
		Status:      1,
	})
	function.SetOutputExample(111)

	s.setAccountDocFun(a, function)

	return function
}

func (s *Doctor) ResetAccountPassword(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.DoctorUserAuthsPassword{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InputError, err)
		return
	}
	if argument.UserID < 1 {
		a.Error(errors.InputInvalid, "用户ID无效")
		return
	}
	if argument.Password == "" {
		a.Error(errors.InputInvalid, "密码为空")
		return
	}

	data, be := s.doctorBusiness.Authentication().SetAccountPassword(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Doctor) ResetAccountPasswordDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("重置医生账号密码")
	function.SetNote("重新设置医生账号密码。成功时返回账号ID")
	function.SetInputExample(&doctorModel.DoctorUserAuthsPassword{
		UserID:   0,
		Password: "***",
	})
	function.SetOutputExample(111)

	s.setAccountDocFun(a, function)

	return function
}

func (s *Doctor) SaveBasicInfo(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.DoctorUserBaseInfoEdit{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InputError, err)
		return
	}
	if argument.UserID < 1 {
		a.Error(errors.InputInvalid, fmt.Errorf("用户ID'%d'无效", argument.UserID))
		return
	}

	data, be := s.doctorBusiness.UserBasicInfo().Save(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Doctor) SaveBasicInfoDoc(a document.Assistant) document.Function {
	dateOfBirth := types.Time(time.Now())
	sex := uint64(0)
	function := a.CreateFunction("编辑医生账号基本信息")
	function.SetNote("保存医生账号基本信息，包括姓名、性别、头像等")
	function.SetInputExample(&doctorModel.DoctorUserBaseInfoEdit{
		UserID:             1,
		Name:               "张三",
		Sex:                &sex,
		DateOfBirth:        &dateOfBirth,
		IdentityCardNumber: "330106200012129876",
		Country:            "中国",
		Nation:             "壮族",
		NativePlace:        "浙江杭州",
		Photo:              "",
		Nickname:           "蓝精灵",
		PersonalSign:       "在那山的那边海的那边有一群蓝精灵",
		Phone:              "13812344321",
		EducationLevel:     "大学",
		Job:                "Job",
		Title:              "中国",
	})
	function.SetOutputExample(11)

	s.setAccountDocFun(a, function)

	return function
}

func (s *Doctor) GetBasicInfo(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.DoctorUserBaseInfoFilter{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InputError, err)
		return
	}
	if argument.UserID < 1 {
		a.Error(errors.InputInvalid, fmt.Errorf("用户ID'%d'无效", argument.UserID))
		return
	}

	data, be := s.doctorBusiness.UserBasicInfo().Detail(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Doctor) GetBasicInfoDoc(a document.Assistant) document.Function {
	dateOfBirth := types.Time(time.Now())
	sex := uint64(0)
	function := a.CreateFunction("获取医生账号基本信息")
	function.SetNote("获取医生账号基本信息，包括姓名、性别、头像等")
	function.SetInputExample(&doctorModel.DoctorUserBaseInfoFilter{
		UserID: 1,
	})
	function.SetOutputExample(&doctorModel.DoctorUserBaseInfoEx{
		DoctorUserBaseInfo: doctorModel.DoctorUserBaseInfo{
			SerialNo:           10,
			UserID:             1,
			Name:               "张三",
			Sex:                &sex,
			DateOfBirth:        &dateOfBirth,
			IdentityCardNumber: "330106200012129876",
			Country:            "中国",
			Nation:             "壮族",
			NativePlace:        "浙江杭州",
			Photo:              "",
			Nickname:           "蓝精灵",
			PersonalSign:       "在那山的那边海的那边有一群蓝精灵",
			Phone:              "13812344321",
			EducationLevel:     "大学",
			Job:                "Job",
			Title:              "中国",
		},
		SexText: "男",
	})

	s.setAccountDocFun(a, function)

	return function
}

func (s *Doctor) SaveVerifiedInfo(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.VerifiedDoctorUserEdit{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InputError, err)
		return
	}
	if argument.UserID < 1 {
		a.Error(errors.InputInvalid, fmt.Errorf("用户ID'%d'无效", argument.UserID))
		return
	}

	data, be := s.doctorBusiness.Verified().Save(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Doctor) SaveVerifiedInfoDoc(a document.Assistant) document.Function {
	verifiedStatus := uint64(0)
	userType := uint64(0)
	function := a.CreateFunction("编辑医生账号认证信息")
	function.SetNote("保存医生账号认证信息，包括机构、状态、职业证件等")
	function.SetInputExample(&doctorModel.VerifiedDoctorUserEdit{
		UserID:         1,
		OrgCode:        "232442",
		UserType:       &userType,
		VerifiedStatus: &verifiedStatus,
	})
	function.SetOutputExample(11)

	s.setAccountDocFun(a, function)

	return function
}

func (s *Doctor) GetVerifiedInfo(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.VerifiedDoctorUserFilter{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InputError, err)
		return
	}
	if argument.UserID < 1 {
		a.Error(errors.InputInvalid, fmt.Errorf("用户ID'%d'无效", argument.UserID))
		return
	}

	data, be := s.doctorBusiness.Verified().Detail(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Doctor) GetVerifiedInfoDoc(a document.Assistant) document.Function {
	verifiedStatus := uint64(0)
	userType := uint64(0)
	function := a.CreateFunction("获取医生账号认证信息")
	function.SetNote("获取医生账号认证信息，包括机构、状态、职业证件等")
	function.SetInputExample(&doctorModel.VerifiedDoctorUserFilter{
		UserID: 1,
	})
	function.SetOutputExample(&doctorModel.VerifiedDoctorUserEx{
		VerifiedDoctorUser: doctorModel.VerifiedDoctorUser{
			SerialNo:       10,
			UserID:         1,
			OrgCode:        "232442",
			UserType:       &userType,
			VerifiedStatus: &verifiedStatus,
		},
		UserTypeText:       "医生",
		VerifiedStatusText: "未认证",
	})

	s.setAccountDocFun(a, function)

	return function
}
func (s *Doctor) GetRightIDList(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.DoctorAddedRightFilter{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InputError, err)
		return
	}
	if argument.UserID < 1 {
		a.Error(errors.InputInvalid, fmt.Errorf("用户ID'%d'无效", argument.UserID))
		return
	}

	data, be := s.doctorBusiness.Verified().RightIDList(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}
	a.Success(data)
}

func (s *Doctor) GetRightIDListDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("获取医生账号权限信息")
	function.SetNote("获取医生账号权限信息")
	function.SetInputExample(&doctorModel.DoctorAddedRightFilter{
		UserID: 1,
	})
	function.SetOutputExample(&doctorModel.DoctorAddedRight{
		RightID: 1,
	})

	s.setAccountDocFun(a, function)

	return function
}
func (s *Doctor) SaveRightIDList(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.DoctorAddedRight{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InputError, err)
		return
	}
	if argument.UserID < 1 {
		a.Error(errors.InputInvalid, fmt.Errorf("用户ID'%d'无效", argument.UserID))
		return
	}

	data, be := s.doctorBusiness.Verified().SaveRightIDList(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}
	a.Success(data)
}

func (s *Doctor) SaveRightIDListDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("添加医生账号权限信息")
	function.SetNote("添加医生账号权限信息")
	function.SetInputExample(&doctorModel.DoctorAddedRight{
		UserID:  1,
		RightID: 1,
	})
	function.SetOutputExample([]doctorModel.DoctorAddedRight{
		{
			RightID: 1,
		},
	})

	s.setAccountDocFun(a, function)

	return function
}
