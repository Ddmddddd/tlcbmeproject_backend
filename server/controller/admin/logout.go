package admin

import (
	"tlcbme_project/database/memory"
	"tlcbme_project/errors"
	"tlcbme_project/server/config"
	"tlcbme_project/server/controller"
	"github.com/ktpswjz/httpserver/document"
	"github.com/ktpswjz/httpserver/router"
	"github.com/ktpswjz/httpserver/types"
	"net/http"
)

type Logout struct {
	controller.Controller
	doc
}

func NewLogout(cfg *config.Config, log types.Log, dbToken memory.Token) *Logout {
	instance := &Logout{}
	instance.SetLog(log)
	instance.Config = cfg
	instance.DbToken = dbToken

	return instance
}

func (s *Logout) setDocFun(a document.Assistant, fun document.Function) {
	catalog := s.rootCatalog(a).CreateChild("权限管理", "系统授权相关接口")
	catalog.SetFunction(fun)
}

func (s *Logout) Logout(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	token, err := s.DbToken.Get(a.Token())
	if err != nil {
		a.Error(errors.Exception, err)
		return
	}
	if token == nil {
		a.Error(errors.NotExist, "凭证'", a.Token(), "'不存在")
		return
	}
	err = s.DbToken.Del(token)
	if err != nil {
		a.Error(errors.Exception, err)
		return
	}

	a.Success(true)
}

func (s *Logout) LogoutDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("退出登录")
	function.SetNote("退出登录, 使当前凭证失效")
	function.SetOutputExample(true)
	function.SetContentType("")

	s.setDocFun(a, function)

	return function
}
