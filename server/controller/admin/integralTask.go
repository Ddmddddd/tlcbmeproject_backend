package admin

import (
	"time"
	"tlcbme_project/data/entity/sqldb"
	"tlcbme_project/data/model/doctor"
)

type IntegralTask struct {
	Task
}

func (s *Task) Start() {
	go func() {
		s.Cancel = make(chan bool, 1)
		now := time.Now()
		next1 := time.Date(now.Year(), now.Month(), now.Day(), 0, 5, 0, 0, now.Location())
		next2 := next1.Add(24 * time.Hour)
		if next1.After(now) {
			s.Interval = next1.Sub(now)
		} else {
			s.Interval = next2.Sub(now)
		}
		for {
			t := time.NewTimer(s.Interval)
			go func() {
				select {
				case <-s.Cancel:
					break
				}
			}()
			<-t.C
			s.Interval = 24 * time.Hour
			go s.startTask()
		}
	}()
}

func (s *Task) Stop() {
	s.Cancel <- true
}

func (s *Task) startTask() {
	patients := make([]*doctor.PatientUserAuths, 0)
	t := time.Now()
	lastT := t.AddDate(0, 0, -28)
	lastDay := time.Date(lastT.Year(), lastT.Month(), lastT.Day(), 0, 0, 0, 0, time.Local)
	today := time.Date(t.Year(), t.Month(), t.Day(), 0, 0, 0, 0, time.Local)
	dbBaseEntity := &sqldb.PatientUserAuths{}
	dbBaseFilter := &sqldb.PatientUserAuthsRegistDateFilter{
		StartDateTime: &lastDay,
		EndDateTime:   &today,
	}
	sqlBaseFilter := s.sqlDatabase.NewFilter(dbBaseFilter, false, false)
	err := s.sqlDatabase.SelectList(dbBaseEntity, func() {
		result := &doctor.PatientUserAuths{}
		dbBaseEntity.CopyTo(result)
		patients = append(patients, result)
	}, nil, sqlBaseFilter)
	if err != nil {
		return
	}
	for _, patient := range patients {
		registDateTime := time.Time(*patient.RegistDateTime)
		t2 := time.Date(registDateTime.Year(), registDateTime.Month(), registDateTime.Day(), 0, 0, 0, 0, time.Local)
		timeGap := uint64(today.Sub(t2).Hours()/24) - 1
		if timeGap < 0 || timeGap > 28 || timeGap%7 != 0 {
			continue
		}
		//获取这周7天内的最新体重数据
		lastWeekT := t.AddDate(0, 0, -7)
		lastWeekDay := time.Date(lastWeekT.Year(), lastWeekT.Month(), lastWeekT.Day(), 0, 0, 0, 0, time.Local)
		dbWeightEntity := &sqldb.WeightRecord{}
		dbWeightFilter := &sqldb.WeightRecordPatientAndDateFilter{
			PatientID:        patient.UserID,
			MeasureStartDate: &lastWeekDay,
		}
		sqlWeightFilter := s.sqlDatabase.NewFilter(dbWeightFilter, false, false)
		recentWeight := 0.0
		err = s.sqlDatabase.SelectList(dbWeightEntity, func() {
			recentWeight = dbWeightEntity.Weight
		}, nil, sqlWeightFilter)
		if err != nil || recentWeight == 0.0 {
			continue
		}
		//获取上周7天内的最新体重数据
		preWeekT := t.AddDate(0, 0, -14)
		preWeekDay := time.Date(preWeekT.Year(), preWeekT.Month(), preWeekT.Day(), 0, 0, 0, 0, time.Local)
		dbPreWeightEntity := &sqldb.WeightRecord{}
		dbPreWeightFilter := &sqldb.WeightRecordDataFilter{
			PatientID:        patient.UserID,
			MeasureStartDate: &preWeekDay,
			MeasureEndDate:   &lastWeekDay,
		}
		sqlPreWeightFilter := s.sqlDatabase.NewFilter(dbPreWeightFilter, false, false)
		lastWeight := 0.0
		err = s.sqlDatabase.SelectList(dbPreWeightEntity, func() {
			lastWeight = dbPreWeightEntity.Weight
		}, nil, sqlPreWeightFilter)
		if err != nil || lastWeight == 0.0 {
			continue
		}
		//减重不超过0.5kg
		if lastWeight-recentWeight < 0.5 {
			continue
		}
		dbExEntity := &sqldb.ExpPatientTaskRecord{}
		dbExFilter := &sqldb.ExpPatientTaskRecordLastWeekFilter{
			PatientID:      patient.UserID,
			TaskName:       "运动",
			CreateDateTime: &lastWeekDay,
		}
		sqlExFilter := s.sqlDatabase.NewFilter(dbExFilter, false, false)
		cnt := make(map[int]bool)
		err = s.sqlDatabase.SelectList(dbExEntity, func() {
			dayGap := int(dbExEntity.CreateDateTime.Sub(lastWeekDay) / 24)
			cnt[dayGap] = true
		}, nil, sqlExFilter)
		if len(cnt) < 3 {
			continue
		}
		//计算需要提供多少积分
		week := int(today.Sub(t2).Hours()/24/7) - 1
		integral := 88 + week*100
		err = s.patient.AddIntegralForPatient(patient.UserID, uint64(integral), "每周目标")
		if err != nil {
			continue
		}
		////同步在总积分表里更新积分，需先判断是否存在该用户
		//dbTotalEntity := &sqldb.Exptotalpoint{}
		//dbTotalFilter := &sqldb.ExptotalPointFilter{
		//	PatientID: patient.UserID,
		//}
		//sqlTotalFilter := s.sqlDatabase.NewFilter(dbTotalFilter,false,false)
		//err = s.sqlDatabase.SelectOne(dbTotalEntity,sqlTotalFilter)
		//if err == nil {
		//	newTotalPoint := dbTotalEntity.TotalPoint + uint64(integral)
		//	dbTotalUpdate := &sqldb.ExptotalPointUpdate{
		//		TotalPoint: newTotalPoint,
		//	}
		//	s.sqlDatabase.Update(dbTotalUpdate,sqlTotalFilter)
		//} else {
		//	dbInfoEntity := &sqldb.PatientUserBaseInfo{}
		//	dbInfoFilter := &sqldb.PatientUserBaseInfoUserIdFilter{
		//		UserID: patient.UserID,
		//	}
		//	sqlInfoFilter := s.sqlDatabase.NewFilter(dbInfoFilter,false,false)
		//	err = s.sqlDatabase.SelectOne(dbInfoEntity,sqlInfoFilter)
		//	if err == nil {
		//		dbNewEntity := &sqldb.Exptotalpoint{
		//			PatientID: patient.UserID,
		//			PatientName: dbInfoEntity.Name,
		//			TotalPoint: uint64(integral),
		//		}
		//		_, err = s.sqlDatabase.Insert(dbNewEntity)
		//	}
		//}
	}
}
