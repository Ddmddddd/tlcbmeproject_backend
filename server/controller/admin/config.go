package admin

import (
	"fmt"
	"github.com/ktpswjz/httpserver/document"
	"github.com/ktpswjz/httpserver/router"
	"github.com/ktpswjz/httpserver/security/hash"
	"github.com/ktpswjz/httpserver/security/jwt"
	"github.com/ktpswjz/httpserver/types"
	uuid "github.com/satori/go.uuid"
	"net/http"
	"time"
	"tlcbme_project/business"
	"tlcbme_project/business/doctor/api"
	patientApi "tlcbme_project/business/patient/api"
	"tlcbme_project/data/model"
	"tlcbme_project/data/model/doctor"
	"tlcbme_project/data/model/manage"
	"tlcbme_project/data/model/platform"
	"tlcbme_project/database"
	"tlcbme_project/database/memory"
	"tlcbme_project/errors"
	"tlcbme_project/server/config"
	"tlcbme_project/server/controller"
)

type Config struct {
	controller.Controller
	doc

	sqlDatabase   database.SqlDatabase
	client        business.Client
	SmsTask       SmsTask
	StaticalTask  StatisticalTask
	IntegralTask  IntegralTask
	HistoryConfig HistoryConfig
}

func NewConfig(cfg *config.Config, log types.Log, dbToken memory.Token, sqlDatabase database.SqlDatabase, client business.Client, SendSms api.Sms, PatientData patientApi.Data) *Config {
	instance := &Config{}
	instance.Config = cfg
	instance.SetLog(log)
	instance.DbToken = dbToken
	instance.sqlDatabase = sqlDatabase
	instance.client = client
	//instance.Task.StartWeChatRemindTask()
	instance.HistoryConfig = HistoryConfig{
		StatisticalTask: StatisticalTask{
			Task: Task{
				Config:      instance.Config,
				sqlDatabase: sqlDatabase,
			},
		},
	}
	instance.SmsTask = SmsTask{
		sms: SendSms,
		Task: Task{
			Config:      instance.Config,
			sqlDatabase: sqlDatabase,
		},
	}
	//instance.SmsTask.Start()
	instance.StaticalTask = StatisticalTask{
		Task: Task{
			Config:      instance.Config,
			sqlDatabase: sqlDatabase,
		},
	}
	//instance.StaticalTask.Start()
	instance.IntegralTask = IntegralTask{
		Task: Task{
			Config:      instance.Config,
			sqlDatabase: sqlDatabase,
			patient:     PatientData,
		},
	}
	//instance.IntegralTask.Start()
	return instance
}

func (s *Config) setDocFun(a document.Assistant, fun document.Function) {
	catalog := s.rootCatalog(a).
		CreateChild("服务管理", "后台服务相关接口").
		CreateChild("配置管理", "后台服务配置相关接口")
	catalog.SetFunction(fun)
}

func (s *Config) GetMySql(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	a.Success(s.Config.Database.Mysql)
}

func (s *Config) GetMySqlDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("获取数据库配置信息")
	function.SetNote("获取当前数据库连接配置信息")
	function.SetOutputExample(&config.DatabaseMysql{
		Server:   "127.0.0.1",
		Port:     3306,
		Schema:   "schema_name",
		Charset:  "utf8",
		User:     "root",
		Password: "",
		TimeOut:  10,
	})
	function.SetContentType("")

	s.setDocFun(a, function)

	return function
}

func (s *Config) SetMySql(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &config.DatabaseMysql{
		Server:  "127.0.0.1",
		Port:    3306,
		Schema:  "tlcbme_project",
		Charset: "utf8",
		TimeOut: 10,
	}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InputError, err)
		return
	}

	cfg := config.NewConfig()
	cfg.LoadFromFile(s.Config.GetPath())
	cfg.Database.Mysql.CopyFrom(argument)
	err = cfg.SaveToFile(s.Config.GetPath())
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}

	s.Config.Database.Mysql.CopyFrom(argument)
	a.Success(s.Config.Database.Mysql)
}

func (s *Config) SetMySqlDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("修改数据库配置信息")
	function.SetNote("修改当前数据库连接配置并保存")
	function.SetInputExample(&config.DatabaseMysql{
		Server:   "127.0.0.1",
		Port:     3306,
		Schema:   "schema_name",
		Charset:  "utf8",
		User:     "root",
		Password: "",
		TimeOut:  10,
	})
	function.SetOutputExample(&config.DatabaseMysql{
		Server:   "127.0.0.1",
		Port:     3306,
		Schema:   "schema_name",
		Charset:  "utf8",
		User:     "root",
		Password: "",
		TimeOut:  10,
	})

	s.setDocFun(a, function)

	return function
}

func (s *Config) TestMySql(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	sqlDbVer, err := s.sqlDatabase.Test()
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}

	a.Success(sqlDbVer)
}

func (s *Config) TestMySqlDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("测试数据库配置信息")
	function.SetNote("测试当前数据库连接配置是否有效，成功时返回数据库版本信息")
	function.SetOutputExample("5.7.22-0ubuntu18.04.1")
	function.SetContentType("")

	s.setDocFun(a, function)

	return function
}

func (s *Config) DownloadMysqlSchema(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	sqlAccess, err := s.sqlDatabase.NewAccess(false)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}
	defer sqlAccess.Close()

	mysql := &mysql{}
	schemaName := s.Config.Database.Mysql.Schema
	tables, err := mysql.getTables(sqlAccess, schemaName, true)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}
	views, err := mysql.getViews(sqlAccess, schemaName)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}

	w.Header().Set("Content-Disposition", fmt.Sprintf("attachment; filename=%s.sql", schemaName))
	w.Header().Set("Content-Type", "application/octet-stream charset=utf-8")

	for _, table := range tables {
		table.Columns, _ = mysql.getColumns(sqlAccess, schemaName, table.Name)
		fmt.Fprint(w, table.GetCreateSql(true))
		fmt.Fprint(w, ";\r\n\r\n")
	}
	for _, view := range views {
		fmt.Fprint(w, view.GetCreateSql())
		fmt.Fprint(w, ";\r\n")
	}
}

func (s *Config) DownloadMysqlSchemaDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("下载数据库脚本")
	function.SetNote("下载数据库结构脚本")

	s.setDocFun(a, function)

	return function
}

func (s *Config) GetManageProvider(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &config.ManagementFilter{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InputError, err)
		return
	}
	if argument.ProviderType == config.ManagementProviderTypeHbp {
		a.Success(s.Config.Management.Providers.Hbp)
	} else if argument.ProviderType == config.ManagementProviderTypeDm {
		a.Success(s.Config.Management.Providers.Dm)
	} else {
		a.Error(errors.InputInvalid, fmt.Sprintf("提供者类型(providerType=%d)无效", argument.ProviderType))
	}
}

func (s *Config) GetManageProviderDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("获取管理服务提供者配置信息")
	function.SetNote("获取管理服务提供者标识集授权信息配置信息")
	function.SetInputExample(&config.ManagementFilter{
		ProviderType: config.ManagementProviderTypeHbp,
	})
	function.SetOutputExample(&config.ManagementProviderHbp{
		ManagementProvider: config.ManagementProvider{
			ID: "ManagerServiceID",
			Auth: config.ManagementProviderAuth{
				ID:         "OrgID",
				Type:       "JWT",
				Algorithm:  "HS256",
				Secret:     "pwd",
				Expiration: 300,
			},
		},

		Api: config.ManagementProviderHbpApi{
			ManagementProviderApi: config.ManagementProviderApi{
				BaseUrl: "http://192.168.1.1:8080/svc",
			},
			Uri: config.ManagementProviderHbpApiUri{
				ManagementProviderApiUri: config.ManagementProviderApiUri{
					RegPatient:          "/1.0/patient/regpatient",
					RegDisease:          "/1.0/patient/regdisease",
					UpdateSetting:       "/1.0/hbp/patient/update/setting",
					UploadBloodPressure: "/1.0/hbp/patient/measure",
					UploadDiscomfort:    "/1.0/hbp/patient/discomfort",
					DeleteRecord:        "/1.0/hbp/patient/delete/record",
					UploadFollowup:      "/1.0/hbp/patient/followup",
					UploadAssess:        "/1.0/hbp/patient/riskassess",
				},
			},
		},
	})

	s.setDocFun(a, function)

	return function
}
func (s *Config) SetManageProvider(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &config.ManagementArgument{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InputError, err)
		return
	}
	if argument.ProviderType == config.ManagementProviderTypeHbp {
		data := &config.ManagementProviderHbp{}
		s.Config.Management.Providers.Hbp.CopyTo(data)
		err = argument.GetData(data)
		if err != nil {
			a.Error(errors.InputError, err)
			return
		}

		cfg := config.NewConfig()
		cfg.LoadFromFile(s.Config.GetPath())
		cfg.Management.Providers.Hbp.CopyFrom(data)
		err = cfg.SaveToFile(s.Config.GetPath())
		if err != nil {
			a.Error(errors.InternalError, err)
			return
		}

		s.Config.Management.Providers.Hbp.CopyFrom(data)
		a.Success(s.Config.Management.Providers.Hbp)

	} else if argument.ProviderType == config.ManagementProviderTypeDm {
		data := &config.ManagementProviderDm{}
		s.Config.Management.Providers.Dm.CopyTo(data)
		err = argument.GetData(data)
		if err != nil {
			a.Error(errors.InputError, err)
			return
		}

		cfg := config.NewConfig()
		cfg.LoadFromFile(s.Config.GetPath())
		cfg.Management.Providers.Dm.CopyFrom(data)
		err = cfg.SaveToFile(s.Config.GetPath())
		if err != nil {
			a.Error(errors.InternalError, err)
			return
		}

		s.Config.Management.Providers.Dm.CopyFrom(data)
		a.Success(s.Config.Management.Providers.Dm)
	} else {
		a.Error(errors.InputInvalid, fmt.Sprintf("提供者类型(providerType=%d)无效", argument.ProviderType))
	}
}

func (s *Config) SetManageProviderDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("修改管理服务提供者配置信息")
	function.SetNote("修改管理服务提供者标识集授权信息配置信息")
	function.SetInputExample(&config.ManagementArgument{
		ProviderType: config.ManagementProviderTypeHbp,
		Data: &config.ManagementProviderHbp{
			ManagementProvider: config.ManagementProvider{
				ID: "ManagerServiceID",
				Auth: config.ManagementProviderAuth{
					ID:         "OrgID",
					Type:       "JWT",
					Algorithm:  "HS256",
					Secret:     "pwd",
					Expiration: 300,
				},
			},
			Api: config.ManagementProviderHbpApi{},
		},
	})
	function.SetOutputExample(&config.ManagementProviderHbp{
		ManagementProvider: config.ManagementProvider{
			ID: "ManagerServiceID",
			Auth: config.ManagementProviderAuth{
				ID:         "OrgID",
				Type:       "JWT",
				Algorithm:  "HS256",
				Secret:     "pwd",
				Expiration: 300,
			},
		},

		Api: config.ManagementProviderHbpApi{
			ManagementProviderApi: config.ManagementProviderApi{
				BaseUrl: "http://192.168.1.1:8080/svc",
			},
			Uri: config.ManagementProviderHbpApiUri{
				ManagementProviderApiUri: config.ManagementProviderApiUri{
					RegPatient:          "/1.0/patient/regpatient",
					RegDisease:          "/1.0/patient/regdisease",
					UpdateSetting:       "/1.0/hbp/patient/update/setting",
					UploadBloodPressure: "/1.0/hbp/patient/measure",
					UploadDiscomfort:    "/1.0/hbp/patient/discomfort",
					DeleteRecord:        "/1.0/hbp/patient/delete/record",
					UploadFollowup:      "/1.0/hbp/patient/followup",
					UploadAssess:        "/1.0/hbp/patient/riskassess",
				},
			},
		},
	})

	s.setDocFun(a, function)

	return function
}

func (s *Config) CreateJwt(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	secret := ""
	algorithm := ""
	expiration := time.Second
	payload := &model.JwtPayload{
		ID: a.GenerateGuid(),
	}

	argument := &config.ManagementFilter{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InputError, err)
		return
	}
	if argument.ProviderType == config.ManagementProviderTypeHbp {
		secret = s.Config.Management.Providers.Hbp.Auth.Secret
		algorithm = s.Config.Management.Providers.Hbp.Auth.Algorithm
		expiration = time.Second * time.Duration(s.Config.Management.Providers.Hbp.Auth.Expiration)
		payload.Issuer = s.Config.Management.Providers.Hbp.ID
		payload.Subject = s.Config.Management.Providers.Hbp.Auth.ID
		payload.Audience = s.Config.Management.Providers.Hbp.ID
	} else if argument.ProviderType == config.ManagementProviderTypeDm {
		secret = s.Config.Management.Providers.Dm.Auth.Secret
		algorithm = s.Config.Management.Providers.Dm.Auth.Algorithm
		expiration = time.Second * time.Duration(s.Config.Management.Providers.Dm.Auth.Expiration)
		payload.Issuer = s.Config.Management.Providers.Dm.ID
		payload.Subject = s.Config.Management.Providers.Dm.Auth.ID
		payload.Audience = s.Config.Management.Providers.Dm.ID
	} else {
		a.Error(errors.InputInvalid, fmt.Sprintf("提供者类型(providerType=%d)无效", argument.ProviderType))
		return
	}

	now := time.Now()
	payload.ExpirationTime = now.Add(expiration).Unix()
	payload.NotBefore = now.Add(-expiration).Unix()
	payload.IssuedAt = now.Unix()

	jwtToken, err := jwt.Encode(secret, algorithm, payload)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}

	result := &model.JWT{
		JWT:     jwtToken,
		Payload: payload,
	}

	var header interface{}
	vs, err := jwt.Decode(jwtToken, nil, &header)
	if err == nil {
		result.Header = header
		result.Signature = vs[2]
	}

	a.Success(result)
}

func (s *Config) CreateJwtDoc(a document.Assistant) document.Function {
	now := time.Now()
	expiration := time.Second * time.Duration(s.Config.Management.Providers.Hbp.Auth.Expiration)

	function := a.CreateFunction("生成JWT凭证")
	function.SetNote("使用系统当前配置生成JWT凭证")
	function.SetInputExample(&config.ManagementFilter{
		ProviderType: config.ManagementProviderTypeHbp,
	})
	function.SetOutputExample(&model.JWT{
		JWT: "",
		Header: jwt.Header{
			Algorithm: "HS256",
			Type:      "JWT",
		},
		Payload: &model.JwtPayload{
			Issuer:         "Issuer",
			Subject:        "Subject",
			Audience:       "Audience",
			ExpirationTime: now.Add(expiration).Unix(),
			NotBefore:      now.Unix(),
			IssuedAt:       now.Unix(),
			ID:             "GUID",
		},
	})

	s.setDocFun(a, function)

	return function
}

func (s *Config) GetManageProviderApi(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &config.ManagementFilter{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InputError, err)
		return
	}
	if argument.ProviderType == config.ManagementProviderTypeHbp {
		a.Success(s.Config.Management.Providers.Hbp.Api)
	} else if argument.ProviderType == config.ManagementProviderTypeDm {
		a.Success(s.Config.Management.Providers.Dm.Api)
	} else {
		a.Error(errors.InputInvalid, fmt.Sprintf("提供者类型(providerType=%d)无效", argument.ProviderType))
	}
}

func (s *Config) GetManageProviderApiDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("获取管理服务接口配置信息")
	function.SetNote("获取管理服务接配置信息")
	function.SetInputExample(&config.ManagementFilter{
		ProviderType: config.ManagementProviderTypeHbp,
	})
	function.SetOutputExample(&config.ManagementProviderHbpApi{
		ManagementProviderApi: config.ManagementProviderApi{
			BaseUrl: "",
		},
		Uri: config.ManagementProviderHbpApiUri{
			ManagementProviderApiUri: config.ManagementProviderApiUri{
				RegPatient:          "/1.0/patient/regpatient",
				RegDisease:          "/1.0/patient/regdisease",
				UpdateSetting:       "/1.0/hbp/patient/update/setting",
				UploadBloodPressure: "/1.0/hbp/patient/measure",
				UploadDiscomfort:    "/1.0/hbp/patient/discomfort",
				DeleteRecord:        "/1.0/hbp/patient/delete/record",
				UploadFollowup:      "/1.0/hbp/patient/followup",
				UploadAssess:        "/1.0/hbp/patient/riskassess",
			},
		},
	})

	s.setDocFun(a, function)

	return function
}

func (s *Config) SetManageProviderApi(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &config.ManagementArgument{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InputError, err)
		return
	}
	if argument.ProviderType == config.ManagementProviderTypeHbp {
		data := &config.ManagementProviderHbpApi{}
		s.Config.Management.Providers.Hbp.Api.CopyTo(data)
		err = argument.GetData(data)
		if err != nil {
			a.Error(errors.InputError, err)
			return
		}

		cfg := config.NewConfig()
		cfg.LoadFromFile(s.Config.GetPath())
		cfg.Management.Providers.Hbp.Api.CopyFrom(data)
		err = cfg.SaveToFile(s.Config.GetPath())
		if err != nil {
			a.Error(errors.InternalError, err)
			return
		}

		s.Config.Management.Providers.Hbp.Api.CopyFrom(data)
		a.Success(s.Config.Management.Providers.Hbp.Api)
	} else if argument.ProviderType == config.ManagementProviderTypeDm {
		data := &config.ManagementProviderDmApi{}
		s.Config.Management.Providers.Dm.Api.CopyTo(data)
		err = argument.GetData(data)
		if err != nil {
			a.Error(errors.InputError, err)
			return
		}

		cfg := config.NewConfig()
		cfg.LoadFromFile(s.Config.GetPath())
		cfg.Management.Providers.Dm.Api.CopyFrom(data)
		err = cfg.SaveToFile(s.Config.GetPath())
		if err != nil {
			a.Error(errors.InternalError, err)
			return
		}

		s.Config.Management.Providers.Dm.Api.CopyFrom(data)
		a.Success(s.Config.Management.Providers.Dm.Api)
	} else {
		a.Error(errors.InputInvalid, fmt.Sprintf("提供者类型(providerType=%d)无效", argument.ProviderType))
	}
}

func (s *Config) SetManageProviderApiDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("修改管理服务接口配置信息")
	function.SetNote("修改管理服务接口配置信息")
	function.SetInputExample(&config.ManagementProviderHbpApi{
		ManagementProviderApi: config.ManagementProviderApi{
			BaseUrl: "http://192.168.1.1:8080/svc",
		},
		Uri: config.ManagementProviderHbpApiUri{
			ManagementProviderApiUri: config.ManagementProviderApiUri{
				RegPatient:          "/1.0/patient/regpatient",
				RegDisease:          "/1.0/patient/regdisease",
				UpdateSetting:       "/1.0/hbp/patient/update/setting",
				UploadBloodPressure: "/1.0/hbp/patient/measure",
				UploadDiscomfort:    "/1.0/hbp/patient/discomfort",
				DeleteRecord:        "/1.0/hbp/patient/delete/record",
				UploadFollowup:      "/1.0/hbp/patient/followup",
				UploadAssess:        "/1.0/hbp/patient/riskassess",
			},
		},
	})
	function.SetOutputExample(&config.ManagementProviderHbpApi{
		ManagementProviderApi: config.ManagementProviderApi{
			BaseUrl: "http://192.168.1.1:8080/svc",
		},
		Uri: config.ManagementProviderHbpApiUri{
			ManagementProviderApiUri: config.ManagementProviderApiUri{
				RegPatient:          "/1.0/patient/regpatient",
				RegDisease:          "/1.0/patient/regdisease",
				UpdateSetting:       "/1.0/hbp/patient/update/setting",
				UploadBloodPressure: "/1.0/hbp/patient/measure",
				UploadDiscomfort:    "/1.0/hbp/patient/discomfort",
				DeleteRecord:        "/1.0/hbp/patient/delete/record",
				UploadFollowup:      "/1.0/hbp/patient/followup",
				UploadAssess:        "/1.0/hbp/patient/riskassess",
			},
		},
	})

	s.setDocFun(a, function)

	return function
}

func (s *Config) TestManageProviderApi(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &manage.Test{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InputError, err)
		return
	}
	providerType := config.ManagementProviderType(argument.ProviderType)
	if providerType == config.ManagementProviderTypeHbp {
	} else if providerType == config.ManagementProviderTypeDm {
	} else {
		a.Error(errors.InputInvalid, fmt.Sprintf("提供者类型(providerType=%d)无效", argument.ProviderType))
		return
	}

	result, err := s.client.ManagePost(providerType, argument.Uri, argument.Argument)
	if err != nil {
		a.Error(errors.Unknown, err)
		return
	}

	a.Success(result)
}

func (s *Config) TestManageProviderApiDoc(a document.Assistant) document.Function {
	now := types.Time(time.Now())
	heartRate := uint64(64)
	function := a.CreateFunction("测试管理服务接口")
	function.SetNote("测试管理服务接口")
	function.SetInputExample(&manage.Test{
		ProviderType: 1,
		Uri:          "/1.0/hbp/patient/measure",
		Argument: &manage.BloodPressureInput{
			PatientID: "232442",
			Record: manage.BloodPressureRecord{
				SystolicPressure:  110,
				DiastolicPressure: 78,
				HeartRate:         &heartRate,
				MeasureDateTime:   &now,
			},
		},
	})
	function.SetOutputExample(&manage.Result{
		Code: 0,
	})

	s.setDocFun(a, function)

	return function
}

func (s *Config) IsShowDoctorAppDownloadCode(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	a.Success(s.Config.Site.Doctor.ShowAppDownloadCodeInLoginPage)
}

func (s *Config) IsShowDoctorAppDownloadCodeDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("是否在医生端登录页面显示app下载二维码")
	function.SetNote("是否在医生端登录页面显示app下载二维码")
	function.SetOutputExample(bool(true))

	s.setDocFun(a, function)

	return function
}

func (s *Config) ShowDoctorAppDownloadCode(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	var argument struct {
		Show bool `json:"show" note:"true显示；false-不显示"`
	}
	err := a.GetArgument(r, &argument)
	if err != nil {
		a.Error(errors.InputError, err)
		return
	}

	cfg := config.NewConfig()
	cfg.LoadFromFile(s.Config.GetPath())
	if cfg.Site.Doctor.ShowAppDownloadCodeInLoginPage == argument.Show {
		a.Success(argument.Show)
		return
	}
	cfg.Site.Doctor.ShowAppDownloadCodeInLoginPage = argument.Show
	err = cfg.SaveToFile(s.Config.GetPath())
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}

	s.Config.Site.Doctor.ShowAppDownloadCodeInLoginPage = argument.Show
	a.Success(argument.Show)
}

func (s *Config) ShowDoctorAppDownloadCodeDoc(a document.Assistant) document.Function {
	var input struct {
		Show bool `json:"show" note:"true显示；false-不显示"`
	}

	function := a.CreateFunction("设置是否在医生端登录页面显示app下载二维码")
	function.SetNote("设置是否在医生端登录页面显示app下载二维码")
	function.SetInputExample(input)
	function.SetOutputExample(bool(true))

	s.setDocFun(a, function)

	return function
}
func (s *Config) GetPlatformProvider(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	a.Success(s.Config.Platform.Providers)
}
func (s *Config) GetPlatformProviderDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("获取信息平台服务提供者配置信息")
	function.SetNote("获取信息平台服务提供者标识集授权信息配置信息")
	function.SetInputExample(nil)
	function.SetOutputExample(&config.PlatformProvider{
		PlatformProviderInfo: config.PlatformProviderInfo{
			Token: "37ce1223-46af-4fcc-b938-b7d740479a44",
		},
		Api: config.PlatformProviderApi{
			PlatformProviderBaseApi: config.PlatformProviderBaseApi{
				BaseUrl: "https://www.medevice.pro",
			},
			Uri: config.PlatformProviderApiUri{
				UploadData: "/v1/application/chronic/",
			},
		},
	})

	s.setDocFun(a, function)

	return function
}
func (s *Config) SetPlatformProvider(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &config.PlatformArgument{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InputError, err)
		return
	}
	data := &config.PlatformProvider{}
	s.Config.Platform.Providers.CopyTo(data)
	err = argument.GetData(data)
	if err != nil {
		a.Error(errors.InputError, err)
		return
	}

	cfg := config.NewConfig()
	cfg.LoadFromFile(s.Config.GetPath())
	cfg.Platform.Providers.CopyFrom(data)
	err = cfg.SaveToFile(s.Config.GetPath())
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}

	s.Config.Platform.Providers.CopyFrom(data)
	a.Success(s.Config.Platform.Providers)
}
func (s *Config) SetPlatformProviderDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("修改信息平台服务提供者配置信息")
	function.SetNote("修改信息平台服务提供者配置信息")
	function.SetInputExample(&config.PlatformArgument{
		Data: &config.PlatformProvider{
			PlatformProviderInfo: config.PlatformProviderInfo{
				Token: "37ce1223-46af-4fcc-b938-b7d740479a44",
			},
			Api: config.PlatformProviderApi{},
		},
	})
	function.SetOutputExample(&config.PlatformProvider{
		PlatformProviderInfo: config.PlatformProviderInfo{
			Token: "37ce1223-46af-4fcc-b938-b7d740479a44",
		},

		Api: config.PlatformProviderApi{
			PlatformProviderBaseApi: config.PlatformProviderBaseApi{
				BaseUrl: "https://www.medevice.pro",
			},
			Uri: config.PlatformProviderApiUri{
				UploadData: "/v1/application/chronic/",
			},
		},
	})

	s.setDocFun(a, function)

	return function
}
func (s *Config) GetPlatformProviderApi(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	a.Success(s.Config.Platform.Providers.Api)
}

func (s *Config) GetPlatformProviderApiDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("获取管理服务接口配置信息")
	function.SetNote("获取管理服务接配置信息")
	function.SetInputExample(&config.ManagementFilter{
		ProviderType: config.ManagementProviderTypeHbp,
	})
	function.SetOutputExample(&config.ManagementProviderHbpApi{
		ManagementProviderApi: config.ManagementProviderApi{
			BaseUrl: "",
		},
		Uri: config.ManagementProviderHbpApiUri{
			ManagementProviderApiUri: config.ManagementProviderApiUri{
				RegPatient:          "/1.0/patient/regpatient",
				RegDisease:          "/1.0/patient/regdisease",
				UpdateSetting:       "/1.0/hbp/patient/update/setting",
				UploadBloodPressure: "/1.0/hbp/patient/measure",
				UploadDiscomfort:    "/1.0/hbp/patient/discomfort",
				DeleteRecord:        "/1.0/hbp/patient/delete/record",
				UploadFollowup:      "/1.0/hbp/patient/followup",
				UploadAssess:        "/1.0/hbp/patient/riskassess",
			},
		},
	})

	s.setDocFun(a, function)

	return function
}
func (s *Config) SetPlatformProviderApi(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &config.PlatformArgument{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InputError, err)
		return
	}
	data := &config.PlatformProviderApi{}
	s.Config.Platform.Providers.Api.CopyTo(data)
	err = argument.GetData(data)
	if err != nil {
		a.Error(errors.InputError, err)
		return
	}

	cfg := config.NewConfig()
	cfg.LoadFromFile(s.Config.GetPath())
	cfg.Platform.Providers.Api.CopyFrom(data)
	err = cfg.SaveToFile(s.Config.GetPath())
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}

	s.Config.Platform.Providers.Api.CopyFrom(data)
	a.Success(s.Config.Platform.Providers.Api)
}

func (s *Config) SetPlatformProviderApiDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("修改信息平台服务接口配置信息")
	function.SetNote("修改信息平台服务接口配置信息")
	function.SetInputExample(&config.PlatformProviderApi{
		PlatformProviderBaseApi: config.PlatformProviderBaseApi{
			BaseUrl: "https://www.medevice.pro",
		},
		Uri: config.PlatformProviderApiUri{
			UploadData: "/v1/application/chronic/",
		},
	})
	function.SetOutputExample(&config.PlatformProviderApi{
		PlatformProviderBaseApi: config.PlatformProviderBaseApi{
			BaseUrl: "https://www.medevice.pro",
		},
		Uri: config.PlatformProviderApiUri{
			UploadData: "/v1/application/chronic/",
		},
	})

	s.setDocFun(a, function)

	return function
}
func (s *Config) CreateControl(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	control := &model.Control{}
	nonce, _ := uuid.NewV4()
	now := time.Now().Unix()
	control.Nonce = fmt.Sprintf("%s", nonce)
	sign := fmt.Sprintf("timestamp=%d&nonce=%s", now, control.Nonce)
	sign2, _ := hash.Hash(sign, uint64(21))
	control.Timestamp = fmt.Sprintf("%d", now)
	control.Sign = sign2

	a.Success(control)
}

func (s *Config) CreateControlDoc(a document.Assistant) document.Function {
	now := time.Now()

	function := a.CreateFunction("生成安全控制参数")
	function.SetNote("使用系统当前配置生成安全控制参数")
	function.SetInputExample(nil)
	function.SetOutputExample(&model.Control{
		Timestamp: string(now.Unix()),
		Nonce:     "0e703cb8-6081-4e82-89ba-3604245011d1",
		Sign:      " b9aac8a91ce8c1cc8819bdf14aff963d53c7063e",
	})

	s.setDocFun(a, function)

	return function
}
func (s *Config) TestPlatformProviderApi(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &platform.Test{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InputError, err)
		return
	}

	result, err := s.client.PlatformPost(argument.Uri, argument.Argument)
	if err != nil {
		a.Error(errors.Unknown, err)
		return
	}

	a.Success(result)
}

func (s *Config) TestPlatformProviderApiDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("测试信息平台服务接口")
	function.SetNote("测试信息平台服务接口")
	docName := "李四"
	function.SetInputExample(&platform.Test{
		Uri: "/1.0/hbp/patient/measure",
		Argument: &doctor.UploadData{
			Hospital:    "宁夏医科大学总医院",
			DoctorName:  &docName,
			PatientName: "张三",
			DataType:    "血压",
			Time:        "2018-12-12 12:12:12",
		},
	})
	function.SetOutputExample(&platform.Result{
		Code: 200,
	})

	s.setDocFun(a, function)

	return function
}

func (s *Config) GetSms(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	a.Success(s.Config.IntegrationAliyun.Sms)
}

func (s *Config) GetSmsDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("获取短信配置信息")
	function.SetNote("获取短信配置信息")
	function.SetOutputExample(&config.IntegrationAliyunSms{
		Url:          "http://dysmsapi.aliyuncs.com/",
		AccessKeyId:  "LTAI4Fd8eNtX67jEg7Md7WL8",
		AccessSecret: "Uxib52NooDCEi5cZITs0d1nFfjJvag",
		Templates: config.IntegrationAliyunSmsTemplates{
			CCP: config.IntegrationAliyunSmsTemplate{
				Sign: "慢病微管家",
			},
		},
	})
	function.SetContentType("")

	s.setDocFun(a, function)

	return function
}

func (s *Config) SetSms(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &config.IntegrationAliyunSms{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InputError, err)
		return
	}

	cfg := config.NewConfig()
	cfg.LoadFromFile(s.Config.GetPath())
	cfg.IntegrationAliyun.Sms.CopyFrom(argument)
	err = cfg.SaveToFile(s.Config.GetPath())
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}

	s.Config.IntegrationAliyun.Sms.CopyFrom(argument)
	a.Success(s.Config.IntegrationAliyun.Sms)
}

func (s *Config) SetSmsDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("修改短信配置信息")
	function.SetNote("修改短信配置并保存")
	function.SetInputExample(&config.IntegrationAliyunSms{})
	function.SetOutputExample(&config.IntegrationAliyunSms{})

	s.setDocFun(a, function)

	return function
}

func (s *Config) GetTask(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	a.Success(s.Config.SmsTask)
}

func (s *Config) GetTaskDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("获取定时任务配置信息")
	function.SetNote("获取定时任务配置信息")
	function.SetOutputExample(&config.Task{
		FixedTime: config.FixedTime{
			Hour:   8,
			Minute: 10,
			Second: 0,
		},
		IntervalTime: config.IntervalTime{
			Hour:   48,
			Minute: 0,
			Second: 0,
		},
	})
	function.SetContentType("")

	s.setDocFun(a, function)

	return function
}

func (s *Config) SetTask(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &config.Task{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InputError, err)
		return
	}

	cfg := config.NewConfig()
	cfg.LoadFromFile(s.Config.GetPath())
	cfg.SmsTask.CopyFrom(argument)
	err = cfg.SaveToFile(s.Config.GetPath())
	s.SmsTask.Stop()
	s.SmsTask.Start()
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}

	s.Config.SmsTask.CopyFrom(argument)
	a.Success(s.Config.SmsTask)
}

func (s *Config) SetTaskDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("修改定时任务配置信息")
	function.SetNote("修改定时任务配置信息并保存")
	function.SetInputExample(&config.Task{})
	function.SetOutputExample(&config.Task{})

	s.setDocFun(a, function)

	return function
}

func (s *Config) GetStaticalTask(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	a.Success(s.Config.StatisticalTask)
}

func (s *Config) GetStaticalTaskDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("获取定时任务配置信息")
	function.SetNote("获取定时任务配置信息")
	function.SetOutputExample(&config.Task{
		FixedTime: config.FixedTime{
			Hour:   8,
			Minute: 10,
			Second: 0,
		},
		IntervalTime: config.IntervalTime{
			Hour:   48,
			Minute: 0,
			Second: 0,
		},
	})
	function.SetContentType("")

	s.setDocFun(a, function)

	return function
}

func (s *Config) SetStaticalTask(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &config.Task{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InputError, err)
		return
	}

	cfg := config.NewConfig()
	cfg.LoadFromFile(s.Config.GetPath())
	cfg.StatisticalTask.CopyFrom(argument)
	err = cfg.SaveToFile(s.Config.GetPath())
	s.StaticalTask.Stop()
	s.StaticalTask.Start()
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}

	s.Config.StatisticalTask.CopyFrom(argument)
	a.Success(s.Config.StatisticalTask)
}

func (s *Config) SetStaticalTaskDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("修改定时任务配置信息")
	function.SetNote("修改定时任务配置信息并保存")
	function.SetInputExample(&config.Task{})
	function.SetOutputExample(&config.Task{})

	s.setDocFun(a, function)

	return function
}

func (s *Config) GetHistoryConfig(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	a.Success(s.Config.HistoryConfig)
}

func (s *Config) GetHistoryConfigDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("获取数据统计历史数据配置信息")
	function.SetNote("获取数据统计历史数据配置信息")
	function.SetOutputExample(&config.HistoryConfig{
		Year:  2021,
		Month: 1,
	})
	function.SetContentType("")

	s.setDocFun(a, function)

	return function
}

func (s *Config) SetHistoryConfig(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &config.HistoryConfig{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InputError, err)
		return
	}

	cfg := config.NewConfig()
	cfg.LoadFromFile(s.Config.GetPath())
	cfg.HistoryConfig.CopyFrom(argument)
	s.Config.HistoryConfig.CopyFrom(argument)
	err = cfg.SaveToFile(s.Config.GetPath())
	s.HistoryConfig.Stop()
	s.HistoryConfig.Start()
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}
	a.Success(s.Config.HistoryConfig)
}

func (s *Config) SetHistoryConfigDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("修改数据统计历史数据配置信息")
	function.SetNote("修改数据统计历史数据配置信息")
	function.SetInputExample(&config.HistoryConfig{})
	function.SetOutputExample(&config.HistoryConfig{})

	s.setDocFun(a, function)

	return function
}
