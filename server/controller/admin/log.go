package admin

import (
	"tlcbme_project/business"
	"tlcbme_project/data/entity/sqldb"
	"tlcbme_project/data/model"
	"tlcbme_project/data/model/doctor"
	"tlcbme_project/database"
	"tlcbme_project/database/memory"
	"tlcbme_project/errors"
	"tlcbme_project/server/config"
	"tlcbme_project/server/controller"
	"fmt"
	"github.com/ktpswjz/httpserver/document"
	"github.com/ktpswjz/httpserver/router"
	"github.com/ktpswjz/httpserver/types"
	"net/http"
	"time"
)

type Log struct {
	controller.Controller
	doc

	sqlDatabase database.SqlDatabase
	client      business.Client
}

func NewLog(cfg *config.Config, log types.Log, dbToken memory.Token, sqlDatabase database.SqlDatabase, client business.Client) *Log {
	instance := &Log{}
	instance.Config = cfg
	instance.SetLog(log)
	instance.DbToken = dbToken
	instance.sqlDatabase = sqlDatabase
	instance.client = client

	return instance
}

func (s *Log) setDocFun(a document.Assistant, fun document.Function) {
	catalog := s.rootCatalog(a).CreateChild("日志管理", "后台服务相关接口")
	catalog.SetFunction(fun)
}

func (s *Log) ListApiStat(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctor.ViewApiLogStatFilter{}
	a.GetArgument(r, argument)
	dbFiler := &sqldb.ViewApiLogStatFilter{}
	dbFiler.CopyFrom(argument)
	sqlFilter := s.sqlDatabase.NewFilter(dbFiler, false, false)

	results := make([]*doctor.ViewApiLogStatEx, 0)
	dbEntity := &sqldb.ViewApiLogStat{}
	err := s.sqlDatabase.SelectList(dbEntity, func() {
		result := &doctor.ViewApiLogStatEx{}
		dbEntity.CopyToEx(result)
		results = append(results, result)
	}, nil, sqlFilter)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}

	a.Success(results)
}

func (s *Log) ListApiStatDoc(a document.Assistant) document.Function {
	avgTime := float64(1205)
	maxTime := uint64(1105)
	minTime := uint64(8205)

	function := a.CreateFunction("获取接口调用统计信息列表")
	function.SetNote("获取接口调用日志统计信息")
	function.SetInputExample(&doctor.ViewApiLogStatFilter{
		Uri: "/",
	})
	function.SetOutputExample([]*doctor.ViewApiLogStatEx{
		{
			ViewApiLogStat: doctor.ViewApiLogStat{
				Uri:     "/api",
				Count:   10,
				AvgTime: &avgTime,
				MaxTime: &maxTime,
				MinTime: &minTime,
			},
			AvgTimeText: "199.305µs",
			MaxTimeText: "399.305µs",
			MinTimeText: "102.305µs",
		},
	})

	s.setDocFun(a, function)

	return function
}

func (s *Log) ListApi(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctor.ApiLogFilter{}
	a.GetArgument(r, argument)
	dbFiler := &sqldb.ApiLogFilter{}
	dbFiler.CopyFrom(argument)
	sqlFilter := s.sqlDatabase.NewFilter(dbFiler, false, false)

	dbOrder := &sqldb.ApiLogOrder{}

	results := make([]*doctor.ApiLogInfo, 0)
	dbEntity := &sqldb.ApiLogInfo{}
	err := s.sqlDatabase.SelectList(dbEntity, func() {
		result := &doctor.ApiLogInfo{}
		dbEntity.CopyTo(result)
		results = append(results, result)
	}, dbOrder, sqlFilter)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}

	a.Success(results)
}

func (s *Log) ListApiDoc(a document.Assistant) document.Function {
	now := types.Time(time.Now())

	function := a.CreateFunction("获取接口调用信息列表")
	function.SetNote("获取接口调用日志信息，返回除输入输出参数之外的详细信息")
	function.SetInputExample(&doctor.ApiLogFilter{
		Schema: "http",
		Uri:    "/",
		RIP:    "192.168.1.102",
	})
	function.SetOutputExample([]*doctor.ApiLogInfo{
		{
			SerialNo:       1808080848450270,
			Schema:         "http",
			Uri:            "/api",
			StartTime:      &now,
			EndTime:        &now,
			ElapseTime:     199305,
			ElapseTimeText: "199.305µs",
			RIP:            "192.168.1.2",
			Result:         0,
		},
	})

	s.setDocFun(a, function)

	return function
}

func (s *Log) ListApiPage(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	filter := &doctor.ApiLogFilter{}
	argument := &model.PageFilter{
		Page: model.Page{
			Index: 1,
			Size:  15,
		},
		Filter: filter,
	}
	a.GetArgument(r, argument)
	dbFiler := &sqldb.ApiLogFilter{}
	dbFiler.CopyFrom(filter)
	sqlFilter := s.sqlDatabase.NewFilter(dbFiler, false, false)

	dbOrder := &sqldb.ApiLogOrder{}

	result := &model.PageResult{}
	datas := make([]*doctor.ApiLogInfo, 0)
	dbEntity := &sqldb.ApiLogInfo{}
	err := s.sqlDatabase.SelectPage(dbEntity, func(total, page, size, index uint64) {
		result.Total = total
		result.Count = page
		result.Size = size
		result.Index = index
	}, func() {
		data := &doctor.ApiLogInfo{}
		dbEntity.CopyTo(data)
		datas = append(datas, data)
	}, argument.Size, argument.Index, dbOrder, sqlFilter)
	result.Data = datas

	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}

	a.Success(result)
}

func (s *Log) ListApiPageDoc(a document.Assistant) document.Function {
	now := types.Time(time.Now())

	function := a.CreateFunction("获取接口调用信息列表（分页）")
	function.SetNote("获取接口调用日志信息，返回除输入输出参数之外的详细信息")
	function.SetInputExample(&model.PageFilter{
		Page: model.Page{
			Index: 1,
			Size:  15,
		},
		Filter: &doctor.ApiLogFilter{
			Schema: "http",
			Uri:    "/",
			RIP:    "192.168.1.102",
		},
	})
	function.SetOutputExample(&model.PageResult{
		Page: model.Page{
			Index: 1,
			Size:  15,
		},
		Total: 100,
		Count: 7,
		Data: []*doctor.ApiLogInfo{
			{
				SerialNo:       1808080848450270,
				Schema:         "http",
				Uri:            "/api",
				StartTime:      &now,
				EndTime:        &now,
				ElapseTime:     199305,
				ElapseTimeText: "199.305µs",
				RIP:            "192.168.1.2",
				Result:         0,
			},
		},
	})

	s.setDocFun(a, function)

	return function
}

func (s *Log) GetApiArgument(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctor.ApiLogFilterBase{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InputError, err)
		return
	}
	if argument.SerialNo == nil {
		a.Error(errors.InputError, "序列号(serialNo)为空")
		return
	}

	dbFiler := &sqldb.ApiLogFilterBase{}
	dbFiler.CopyFrom(argument)
	sqlFilter := s.sqlDatabase.NewFilter(dbFiler, false, false)
	dbEntity := &sqldb.ApiLogArgument{}
	err = s.sqlDatabase.SelectOne(dbEntity, sqlFilter)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}

	result := &doctor.ApiLogArgument{}
	dbEntity.CopyTo(result)
	result.SerialNo = *argument.SerialNo

	a.Success(result)
}

func (s *Log) GetApiArgumentDoc(a document.Assistant) document.Function {
	serialNo := uint64(1808081646300206)

	function := a.CreateFunction("获取接口调用参数信息")
	function.SetNote("获取接口调用日志的输入输出参数信息")
	function.SetInputExample(&doctor.ApiLogFilterBase{
		SerialNo: &serialNo,
	})
	function.SetOutputExample(&doctor.ApiLogArgument{})

	s.setDocFun(a, function)

	return function
}

func (s *Log) ListClientPage(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	filter := &doctor.ClientLogFilter{}
	argument := &model.PageFilter{
		Page: model.Page{
			Index: 1,
			Size:  15,
		},
		Filter: filter,
	}
	a.GetArgument(r, argument)
	dbFiler := &sqldb.ClientLogFilter{}
	dbFiler.CopyFrom(filter)
	sqlFilter := s.sqlDatabase.NewFilter(dbFiler, false, false)

	dbOrder := &sqldb.ClientLogOrder{}

	result := &model.PageResult{}
	datas := make([]*doctor.ClientLogInfo, 0)
	dbEntity := &sqldb.ClientLogInfo{}
	err := s.sqlDatabase.SelectPage(dbEntity, func(total, page, size, index uint64) {
		result.Total = total
		result.Count = page
		result.Size = size
		result.Index = index
	}, func() {
		data := &doctor.ClientLogInfo{}
		dbEntity.CopyTo(data)
		datas = append(datas, data)
	}, argument.Size, argument.Index, dbOrder, sqlFilter)
	result.Data = datas

	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}

	a.Success(result)
}

func (s *Log) ListClientPageDoc(a document.Assistant) document.Function {
	now := types.Time(time.Now())

	function := a.CreateFunction("获取客户端调用信息列表（分页）")
	function.SetNote("获取客户端接口调用日志信息，返回除输入输出参数之外的详细信息")
	function.SetInputExample(&model.PageFilter{
		Page: model.Page{
			Index: 1,
			Size:  15,
		},
		Filter: &doctor.ClientLogFilter{
			Schema: "http",
			Uri:    "/",
			Host:   "192.168.1.102",
		},
	})
	function.SetOutputExample(&model.PageResult{
		Page: model.Page{
			Index: 1,
			Size:  15,
		},
		Total: 100,
		Count: 7,
		Data: []*doctor.ClientLogInfo{
			{
				SerialNo:       1808080848450270,
				Schema:         "http",
				Uri:            "/api",
				StartTime:      &now,
				EndTime:        &now,
				ElapseTime:     199305,
				ElapseTimeText: "199.305µs",
				Host:           "192.168.1.2",
				Result:         200,
			},
		},
	})

	s.setDocFun(a, function)

	return function
}

func (s *Log) GetClientArgument(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctor.ClientLogFilterBase{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InputError, err)
		return
	}
	if argument.SerialNo == nil {
		a.Error(errors.InputError, "序列号(serialNo)为空")
		return
	}

	dbFiler := &sqldb.ClientLogFilterBase{}
	dbFiler.CopyFrom(argument)
	sqlFilter := s.sqlDatabase.NewFilter(dbFiler, false, false)
	dbEntity := &sqldb.ClientLogArgument{}
	err = s.sqlDatabase.SelectOne(dbEntity, sqlFilter)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}

	result := &doctor.ClientLogArgument{}
	dbEntity.CopyTo(result)
	result.SerialNo = *argument.SerialNo

	a.Success(result)
}

func (s *Log) GetClientArgumentDoc(a document.Assistant) document.Function {
	serialNo := uint64(1808081646300206)

	function := a.CreateFunction("获取客户端调用参数信息")
	function.SetNote("获取客户端调用接口日志的输入输出参数信息")
	function.SetInputExample(&doctor.ClientLogFilterBase{
		SerialNo: &serialNo,
	})
	function.SetOutputExample(&doctor.ClientLogArgument{})

	s.setDocFun(a, function)

	return function
}

func (s *Log) SearchManageFailRecordPage(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	filter := &doctor.ManageApiFailRecordFilter{}
	argument := &model.PageFilter{
		Page: model.Page{
			Index: 1,
			Size:  15,
		},
		Filter: filter,
	}
	a.GetArgument(r, argument)
	dbFiler := &sqldb.ManageApiFailRecordFilter{}
	dbFiler.CopyFrom(filter)
	sqlFilter := s.sqlDatabase.NewFilter(dbFiler, false, false)

	dbOrder := &sqldb.ManageApiFailRecordOrder{}

	result := &model.PageResult{}
	datas := make([]*doctor.ManageApiFailRecordEx, 0)
	dbEntity := &sqldb.ManageApiFailRecord{}
	err := s.sqlDatabase.SelectPage(dbEntity, func(total, page, size, index uint64) {
		result.Total = total
		result.Count = page
		result.Size = size
		result.Index = index
	}, func() {
		data := &doctor.ManageApiFailRecordEx{}
		dbEntity.CopyToEx(data)
		datas = append(datas, data)
	}, argument.Size, argument.Index, dbOrder, sqlFilter)
	result.Data = datas

	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}

	a.Success(result)
}

func (s *Log) SearchManageFailRecordPageDoc(a document.Assistant) document.Function {
	now := types.Time(time.Now())

	function := a.CreateFunction("获取管理服务调用错误信息列表（分页）")
	function.SetNote("获取管理服务调用错误信息列表")
	function.SetInputExample(&model.PageFilter{
		Page: model.Page{
			Index: 1,
			Size:  15,
		},
		Filter: &doctor.ManageApiFailRecordFilter{
			Uri: "/",
		},
	})
	function.SetOutputExample(&model.PageResult{
		Page: model.Page{
			Index: 1,
			Size:  15,
		},
		Total: 100,
		Count: 7,
		Data: []*doctor.ManageApiFailRecordEx{
			{
				SerialNo:     1808080848450270,
				PatientID:    232442,
				ProviderType: 1,
				BusinessName: "注冊病人",
				Uri:          "/api",
				DateTime:     &now,
			},
		},
	})

	s.setDocFun(a, function)

	return function
}

func (s *Log) RetryManageFailRecord(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctor.ManageApiFailRecordFilter{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InputError, err)
		return
	}

	sqlAccess, err := s.sqlDatabase.NewAccess(false)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}
	defer sqlAccess.Close()

	dbFiler := &sqldb.ManageApiFailRecordFilter{}
	dbFiler.CopyFrom(argument)
	sqlFilter := s.sqlDatabase.NewFilter(dbFiler, false, false)
	dbOrder := &sqldb.ManageApiFailRecordOrder2{}
	dbEntity := &sqldb.ManageApiFailRecord{}
	count := uint64(0)
	toDeleteSerialNos := make([]uint64, 0)
	dbUpdate := &sqldb.ManageApiFailRecordUpdate{}
	err = s.sqlDatabase.SelectList(dbEntity, func() {
		providerType := config.ManagementProviderType(dbEntity.ProviderType)
		r, e := s.client.ManagePost(providerType, dbEntity.Uri, dbEntity.Input)
		if e != nil || r.Code != 0 {
			now := time.Now()
			dbUpdate.DateTime = &now
			dbUpdate.SerialNo = dbEntity.SerialNo
			dbUpdate.RepeatCount = dbEntity.RepeatCount + 1
			if e != nil {
				dbUpdate.ErrMsg = e.Error()
			} else {
				dbUpdate.ErrMsg = fmt.Sprintf("%d-%s", r.Code, r.Message)
			}
			sqlAccess.UpdateByPrimaryKey(dbUpdate)
		} else {
			toDeleteSerialNos = append(toDeleteSerialNos, dbEntity.SerialNo)
		}

		count++
	}, dbOrder, sqlFilter)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}

	if len(toDeleteSerialNos) > 0 {
		dbFilerDelete := &sqldb.ManageApiFailRecordFilter{
			SerialNos: toDeleteSerialNos,
		}
		sqlFilter = s.sqlDatabase.NewFilter(dbFilerDelete, false, false)
		_, err = sqlAccess.Delete(dbEntity, sqlFilter)
		if err != nil {
			a.Error(errors.InternalError, err)
			return
		}
	}

	a.Success(count)
}

func (s *Log) RetryManageFailRecordDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("重试管理服务调用")
	function.SetNote("重试管理服务调用")
	function.SetInputExample(&doctor.ManageApiFailRecordFilter{
		SerialNos: []uint64{
			0,
			1,
		},
	})
	function.SetOutputExample(uint64(2))

	s.setDocFun(a, function)

	return function
}

func (s *Log) DeleteManageFailRecord(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctor.ManageApiFailRecordFilter{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InputError, err)
		return
	}

	sqlAccess, err := s.sqlDatabase.NewAccess(false)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}
	defer sqlAccess.Close()

	dbFiler := &sqldb.ManageApiFailRecordFilter{}
	dbFiler.CopyFrom(argument)
	sqlFilter := s.sqlDatabase.NewFilter(dbFiler, false, false)
	dbEntity := &sqldb.ManageApiFailRecord{}
	count, err := sqlAccess.Delete(dbEntity, sqlFilter)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}

	a.Success(count)
}

func (s *Log) DeleteManageFailRecordDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("重试删除管理服务调用失败记录")
	function.SetNote("重试删除管理服务调用失败记录")
	function.SetInputExample(&doctor.ManageApiFailRecordFilter{
		SerialNos: []uint64{
			0,
			1,
		},
	})
	function.SetOutputExample(uint64(2))

	s.setDocFun(a, function)

	return function
}
func (s *Log) SearchPlatformFailRecordPage(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	filter := &doctor.PlatformApiFailRecordFilter{}
	argument := &model.PageFilter{
		Page: model.Page{
			Index: 1,
			Size:  15,
		},
		Filter: filter,
	}
	a.GetArgument(r, argument)
	dbFiler := &sqldb.PlatformApiFailRecordFilter{}
	dbFiler.CopyFrom(filter)
	sqlFilter := s.sqlDatabase.NewFilter(dbFiler, false, false)

	dbOrder := &sqldb.PlatformApiFailRecordOrder{}

	result := &model.PageResult{}
	datas := make([]*doctor.PlatformApiFailRecordEx, 0)
	dbEntity := &sqldb.PlatformApiFailRecord{}
	err := s.sqlDatabase.SelectPage(dbEntity, func(total, page, size, index uint64) {
		result.Total = total
		result.Count = page
		result.Size = size
		result.Index = index
	}, func() {
		data := &doctor.PlatformApiFailRecordEx{}
		dbEntity.CopyToEx(data)
		datas = append(datas, data)
	}, argument.Size, argument.Index, dbOrder, sqlFilter)
	result.Data = datas

	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}

	a.Success(result)
}

func (s *Log) SearchPlatformFailRecordPageDoc(a document.Assistant) document.Function {
	now := types.Time(time.Now())

	function := a.CreateFunction("获取信息平台服务调用错误信息列表（分页）")
	function.SetNote("获取信息平台服务调用错误信息列表")
	function.SetInputExample(&model.PageFilter{
		Page: model.Page{
			Index: 1,
			Size:  15,
		},
		Filter: &doctor.PlatformApiFailRecordFilter{
			Uri: "/",
		},
	})
	function.SetOutputExample(&model.PageResult{
		Page: model.Page{
			Index: 1,
			Size:  15,
		},
		Total: 100,
		Count: 7,
		Data: []*doctor.PlatformApiFailRecordEx{
			{
				SerialNo:     1808080848450270,
				PatientID:    232442,
				BusinessName: "慢病系统数据监测资源",
				Uri:          "/api",
				DateTime:     &now,
			},
		},
	})

	s.setDocFun(a, function)

	return function
}

func (s *Log) RetryPlatformFailRecord(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctor.PlatformApiFailRecordFilter{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InputError, err)
		return
	}

	sqlAccess, err := s.sqlDatabase.NewAccess(false)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}
	defer sqlAccess.Close()

	dbFiler := &sqldb.PlatformApiFailRecordFilter{}
	dbFiler.CopyFrom(argument)
	sqlFilter := s.sqlDatabase.NewFilter(dbFiler, false, false)
	dbOrder := &sqldb.PlatformApiFailRecordOrder2{}
	dbEntity := &sqldb.PlatformApiFailRecord{}
	count := uint64(0)
	toDeleteSerialNos := make([]uint64, 0)
	dbUpdate := &sqldb.PlatformApiFailRecordUpdate{}
	err = s.sqlDatabase.SelectList(dbEntity, func() {
		r, e := s.client.PlatformPost(dbEntity.Uri, dbEntity.Input)
		if e != nil || (r.Code != 200 && r.Msg != "") {
			now := time.Now()
			dbUpdate.DateTime = &now
			dbUpdate.SerialNo = dbEntity.SerialNo
			dbUpdate.RepeatCount = dbEntity.RepeatCount + 1
			if e != nil {
				dbUpdate.ErrMsg = e.Error()
			} else {
				dbUpdate.ErrMsg = fmt.Sprintf("%d-%s", r.Code, r.Msg)
			}
			sqlAccess.UpdateByPrimaryKey(dbUpdate)
		} else {
			toDeleteSerialNos = append(toDeleteSerialNos, dbEntity.SerialNo)
		}

		count++
	}, dbOrder, sqlFilter)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}

	if len(toDeleteSerialNos) > 0 {
		dbFilerDelete := &sqldb.PlatformApiFailRecordFilter{
			SerialNos: toDeleteSerialNos,
		}
		sqlFilter = s.sqlDatabase.NewFilter(dbFilerDelete, false, false)
		_, err = sqlAccess.Delete(dbEntity, sqlFilter)
		if err != nil {
			a.Error(errors.InternalError, err)
			return
		}
	}

	a.Success(count)
}

func (s *Log) RetryPlatformFailRecordDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("重试信息平台服务调用")
	function.SetNote("重试信息平台服务调用")
	function.SetInputExample(&doctor.PlatformApiFailRecordFilter{
		SerialNos: []uint64{
			0,
			1,
		},
	})
	function.SetOutputExample(uint64(2))

	s.setDocFun(a, function)

	return function
}

func (s *Log) DeletePlatformFailRecord(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctor.PlatformApiFailRecordFilter{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InputError, err)
		return
	}

	sqlAccess, err := s.sqlDatabase.NewAccess(false)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}
	defer sqlAccess.Close()

	dbFiler := &sqldb.PlatformApiFailRecordFilter{}
	dbFiler.CopyFrom(argument)
	sqlFilter := s.sqlDatabase.NewFilter(dbFiler, false, false)
	dbEntity := &sqldb.PlatformApiFailRecord{}
	count, err := sqlAccess.Delete(dbEntity, sqlFilter)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}

	a.Success(count)
}

func (s *Log) DeletePlatformFailRecordDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("重试删除信息平台服务调用失败记录")
	function.SetNote("重试删除信息平台服务调用失败记录")
	function.SetInputExample(&doctor.PlatformApiFailRecordFilter{
		SerialNos: []uint64{
			0,
			1,
		},
	})
	function.SetOutputExample(uint64(2))

	s.setDocFun(a, function)

	return function
}
