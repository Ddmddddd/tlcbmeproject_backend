package admin

import (
	"bytes"
	"tlcbme_project/data/entity/sqldb"
	"tlcbme_project/data/model/doctor"
	"tlcbme_project/database"
	"tlcbme_project/database/memory"
	"tlcbme_project/errors"
	"tlcbme_project/server/config"
	"tlcbme_project/server/controller"
	"encoding/base64"
	"fmt"
	"github.com/boombuler/barcode/qr"
	"github.com/ktpswjz/httpserver/document"
	"github.com/ktpswjz/httpserver/router"
	"github.com/ktpswjz/httpserver/types"
	"image/png"
	"io"
	"net/http"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"time"
)

type Install struct {
	controller.Controller
	doc

	sqlDatabase database.SqlDatabase
	sitePrefix  string
}

func NewInstall(cfg *config.Config, log types.Log, dbToken memory.Token, sqlDatabase database.SqlDatabase, sitePrefix string) *Install {
	instance := &Install{}
	instance.SetLog(log)
	instance.Config = cfg
	instance.DbToken = dbToken
	instance.sqlDatabase = sqlDatabase
	instance.sitePrefix = sitePrefix

	return instance
}

func (s *Install) setDocFun(a document.Assistant, fun document.Function, catalog string) {
	ctl := s.rootCatalog(a).CreateChild("安装包管理", "安装包管理相关接口")
	if len(catalog) > 0 {
		ctl = ctl.CreateChild(catalog, "")
	}
	ctl.SetFunction(fun)
}

func (s *Install) UploadAndroid(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	versionCode := r.FormValue("versionCode")
	if versionCode == "" {
		a.Error(errors.InputError, "版本代码为空")
		return
	}
	versionCodeVal, err := strconv.ParseUint(versionCode, 10, 64)
	if err != nil {
		a.Error(errors.InputInvalid, fmt.Sprintf("版本代码(%s)无效", versionCode))
		return
	}

	compMinVersion := r.FormValue("compMinVersion")
	if compMinVersion == "" {
		a.Error(errors.InputError, "最小兼容版本代码为空")
		return
	}
	compMinVersionVal, err := strconv.ParseUint(compMinVersion, 10, 64)
	if err != nil {
		a.Error(errors.InputInvalid, fmt.Sprintf("最小兼容版本代码(%s)无效", compMinVersion))
		return
	}

	sqlAccess, err := s.sqlDatabase.NewAccess(true)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}
	defer sqlAccess.Close()

	dbFilter := &sqldb.AppVersionHistoryCodeFilter{
		VersionCode: versionCodeVal,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	dbEntity := &sqldb.AppVersionHistory{}
	err = sqlAccess.SelectOne(dbEntity, sqlFilter)
	if err != nil {
		if !s.sqlDatabase.IsNoRows(err) {
			a.Error(errors.InternalError, err)
			return
		}
	} else {
		a.Error(errors.InputInvalid, fmt.Sprintf("版本代码(%s)已存在", versionCode))
		return
	}

	now := types.Time(time.Now())
	info := &doctor.AppVersionHistory{
		VersionCode:    versionCodeVal,
		CompMinVersion: compMinVersionVal,
		UpdateDate:     &now,
		IsForced:       0,
	}
	info.VersionName = r.FormValue("versionName")
	info.UpdateContent = r.FormValue("updateContent")
	isForced := r.FormValue("isForced")
	if strings.ToLower(isForced) == "true" {
		info.IsForced = 1
	}
	dbEntity.CopyFrom(info)
	_, err = sqlAccess.InsertSelective(dbEntity)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}

	file, _, err := r.FormFile("file")
	if err != nil {
		a.Error(errors.InputInvalid, "invalid file: ", err)
		return
	}
	defer file.Close()

	folderPath := filepath.Join(s.Config.Install.Root, s.Config.Install.Android.FolderName)
	err = os.MkdirAll(folderPath, 0777)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}
	filePath := filepath.Join(folderPath, s.Config.Install.Android.FileName)
	fileWriter, err := os.Create(filePath)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}
	defer fileWriter.Close()
	_, err = io.Copy(fileWriter, file)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}
	infoPath := filepath.Join(folderPath, "info.json")
	info.SaveToFile(infoPath)

	historyFolder := filepath.Join(folderPath, "history", versionCode)
	err = os.MkdirAll(historyFolder, 0777)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}
	historyPath := filepath.Join(historyFolder, s.Config.Install.Android.FileName)
	historyWriter, err := os.Create(historyPath)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}
	defer historyWriter.Close()

	file.Seek(0, 0)
	_, err = io.Copy(historyWriter, file)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}
	infoPath = filepath.Join(historyFolder, "info.json")
	info.SaveToFile(infoPath)

	err = sqlAccess.Commit()
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}

	a.Success(filePath)
}

func (s *Install) GetAndroidInfo(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	filePath := filepath.Join(s.Config.Install.Root, s.Config.Install.Android.FolderName, "info.json")
	info := &doctor.AppVersionHistoryEx{}
	err := info.LoadFromFile(filePath)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}
	info.Url = fmt.Sprintf("%s://%s%s/%s/%s",
		a.Schema(), r.Host, s.sitePrefix, s.Config.Install.Android.FolderName, s.Config.Install.Android.FileName)

	code, err := qr.Encode(info.Url, qr.H, qr.Auto)
	if err == nil {
		var buf bytes.Buffer
		err = png.Encode(&buf, code)
		if err == nil {
			qrCode := base64.StdEncoding.EncodeToString(buf.Bytes())

			info.UrlCode = fmt.Sprintf("data:image/png;base64,%s", qrCode)
		}
	}

	a.Success(info)
}

func (s *Install) GetAndroidInfoDoc(a document.Assistant) document.Function {
	now := types.Time(time.Now())

	function := a.CreateFunction("获取安卓安装包信息")
	function.SetNote("获取安卓安装包信息，成功时返回当前最新版本的信息")
	function.SetOutputExample(&doctor.AppVersionHistoryEx{
		AppVersionHistory: doctor.AppVersionHistory{
			SerialNo:       42,
			VersionCode:    1,
			VersionName:    ":1.0.1.1",
			UpdateDate:     &now,
			UpdateContent:  "修改Bug",
			IsForced:       0,
			CompMinVersion: 1,
		},
		Url: "http://tlcbme_project.vico-lab.com/download/android/hypertension.apk",
	})
	function.SetContentType("")

	s.setDocFun(a, function, "安卓")

	return function
}

func (s *Install) UploadWeChat(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	file, _, err := r.FormFile("file")
	if err != nil {
		a.Error(errors.InputInvalid, "invalid file: ", err)
		return
	}
	defer file.Close()
	folderPath := filepath.Join(s.Config.Install.Root, s.Config.Install.Wechat.FolderName)
	err = os.MkdirAll(folderPath, 0777)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}
	filePath := filepath.Join(folderPath, s.Config.Install.Wechat.FileName)
	fileWriter, err := os.Create(filePath)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}
	defer fileWriter.Close()
	_, err = io.Copy(fileWriter, file)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}

	a.Success(filePath)
}

func (s *Install) GetWechatInfo(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	url := fmt.Sprintf("%s://%s%s/%s/%s",
		a.Schema(), r.Host, s.sitePrefix, s.Config.Install.Wechat.FolderName, s.Config.Install.Wechat.FileName)
	a.Success(url)
}

func (s *Install) GetWechatInfoDoc(a document.Assistant) document.Function {
	url := fmt.Sprintf("%s/%s/%s",
		s.sitePrefix, s.Config.Install.Wechat.FolderName, s.Config.Install.Wechat.FileName)
	function := a.CreateFunction("获取微信二维码信息")
	function.SetNote("获取微信二维码信息，成功时返回当前最新二维码图片的url")
	function.SetOutputExample(url)
	function.SetContentType("")

	s.setDocFun(a, function, "微信")

	return function
}

func (s *Install) UploadBrowserSetupForChrome(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	file, _, err := r.FormFile("file")
	if err != nil {
		a.Error(errors.InputInvalid, "invalid file: ", err)
		return
	}
	defer file.Close()

	folderPath := filepath.Join(s.Config.Install.Root, s.Config.Install.Browser.FolderName, s.Config.Install.Browser.Chrome.FolderName)
	err = os.MkdirAll(folderPath, 0777)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}
	filePath := filepath.Join(folderPath, s.Config.Install.Browser.Chrome.FileName)
	fileWriter, err := os.Create(filePath)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}
	defer fileWriter.Close()

	_, err = io.Copy(fileWriter, file)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}

	a.Success(filePath)
}

func (s *Install) GetBrowserSetupInfoForChrome(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	url := fmt.Sprintf("%s://%s%s/%s/%s/%s",
		a.Schema(), r.Host, s.sitePrefix,
		s.Config.Install.Browser.FolderName, s.Config.Install.Browser.Chrome.FolderName, s.Config.Install.Browser.Chrome.FileName)

	folderPath := filepath.Join(s.Config.Install.Root, s.Config.Install.Browser.FolderName, s.Config.Install.Browser.Chrome.FolderName)
	filePath := filepath.Join(folderPath, s.Config.Install.Browser.Chrome.FileName)
	_, err := os.Stat(filePath)
	if err != nil {
		url = ""
	}

	a.Success(url)
}

func (s *Install) GetBrowserSetupInfoForChromeDoc(a document.Assistant) document.Function {
	url := fmt.Sprintf("%s/%s/%s",
		s.sitePrefix, s.Config.Install.Wechat.FolderName, s.Config.Install.Wechat.FileName)
	function := a.CreateFunction("获取谷歌浏览器安装包信息")
	function.SetNote("获取谷歌浏览器安装包信息，成功时安装包下载路径")
	function.SetOutputExample(url)
	function.SetContentType("")

	s.setDocFun(a, function, "浏览器")

	return function
}
