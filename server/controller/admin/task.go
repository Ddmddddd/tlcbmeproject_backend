package admin

import (
	"tlcbme_project/business/doctor/api"
	PatientApi "tlcbme_project/business/patient/api"
	"tlcbme_project/data/entity/sqldb"
	"tlcbme_project/data/model/doctor"
	"tlcbme_project/database"
	"tlcbme_project/server/config"
	"crypto/rand"
	"encoding/json"
	"fmt"
	"github.com/ktpswjz/httpserver/types"
	"io"
	"strings"
	"time"
)

var OnTime = make(chan *doctor.Timingtask, 1)

type Task struct {
	Config      *config.Config `json:"config"`
	Interval    time.Duration  `json:"interval"`
	Cancel      chan bool      `json:"cancel"`
	sqlDatabase database.SqlDatabase
	patient		PatientApi.Data
}
type SmsTask struct {
	Task
	sms api.Sms
}

type TaskManage interface {
	//Start()
	//Stop()
	//StartWeChatRemindTask()
}

func (s *SmsTask) Start() {
	go func() {
		s.Cancel = make(chan bool, 1)
		now := time.Now()
		next1 := time.Date(now.Year(), now.Month(), now.Day(), s.Config.SmsTask.FixedTime.Hour, s.Config.SmsTask.FixedTime.Minute, s.Config.SmsTask.FixedTime.Second, 0, now.Location())
		next2 := next1.Add(24 * time.Hour)
		if next1.After(now) {
			s.Interval = next1.Sub(now)
		} else {
			s.Interval = next2.Sub(now)
		}
		for {
			t := time.NewTimer(s.Interval)
			go func() {
				select {
				case <-s.Cancel:
					break
				}
			}()
			<-t.C
			s.Interval = 24 * time.Hour
			go s.startTask()
			go s.startWeChatRemindTask()
		}
	}()
}

func (s *SmsTask) Stop() {
	s.Cancel <- true
}

func (s *SmsTask) startTask() {
	results := make([]*doctor.Timingtask, 0)
	dbEntity := &sqldb.Timingtask{}
	sqlFilter := s.sqlDatabase.NewFilter(nil, false, false)
	_ = s.sqlDatabase.SelectList(dbEntity, func() {
		result := &doctor.Timingtask{}
		dbEntity.CopyTo(result)
		results = append(results, result)
	}, nil, sqlFilter)
	h, _ := time.ParseDuration(fmt.Sprint(s.Config.SmsTask.IntervalTime.Hour) + "h")
	m, _ := time.ParseDuration(fmt.Sprint(s.Config.SmsTask.IntervalTime.Minute) + "m")
	second, _ := time.ParseDuration(fmt.Sprint(s.Config.SmsTask.IntervalTime.Second) + "s")
	go func() {
		for _, task := range results {
			mm1 := time.Time(*task.Timingstartdatetime).Add(h).Add(m).Add(second)
			if time.Now().After(mm1) {
				OnTime <- task
			}
		}
	}()

	for {
		select {
		case task := <-OnTime:
			dbUserEntity := &sqldb.PatientUserBaseInfo{}
			dbFiler := &sqldb.PatientUserBaseInfoBmiFilter{
				UserID: task.Patientid,
			}
			sqlFilter := s.sqlDatabase.NewFilter(dbFiler, false, false)
			_ = s.sqlDatabase.SelectOne(dbUserEntity, sqlFilter)
			dbOrgEntity := &sqldb.ManagedPatientIndex{}
			dbOrgFiler := &sqldb.ManagedPatientIndexPatientIDFilter{
				PatientID: task.Patientid,
			}
			sqlFilter = s.sqlDatabase.NewFilter(dbOrgFiler, false, false)
			_ = s.sqlDatabase.SelectOne(dbOrgEntity, sqlFilter)
			now := types.Time(time.Now())
			argument := &doctor.SmsParam{}
			argument.SignatureNonce = GenerateGuid()
			argument.Sender = task.Operatorid
			argument.SendDateTime = &now
			if dbUserEntity.Phone != nil {
				argument.PhoneNumber = *dbUserEntity.Phone
			}
			if dbOrgEntity.OrgCode != nil {
				argument.OrgCode = *dbOrgEntity.OrgCode
			}
			argument.TemplateType = 1
			argument.PatientID = task.Patientid
			argument.TemplateParam = fmt.Sprintf(`{"patientName":"%s","doctorName":"%s"}`, dbUserEntity.Name, task.DoctorName)
			if task.Tasktype == 1 {
				htnResults := s.getLastHtnRecord(task)
				if len(htnResults) == 0 || time.Time(*htnResults[0].MeasureDateTime).Before(time.Time(*task.Timingstartdatetime)) {
					s.setSmsArgument(argument, "血压未测量提醒", dbUserEntity.Name, task.DoctorName)
					s.sms.SendSms(argument)
				}
			} else if task.Tasktype == 2 {
				dmResults := s.getLastDmRecord(task)
				if len(dmResults) == 0 || time.Time(*dmResults[0].MeasureDateTime).Before(time.Time(*task.Timingstartdatetime)) {
					s.setSmsArgument(argument, "血糖未测量提醒", dbUserEntity.Name, task.DoctorName)
					s.sms.SendSms(argument)
				}
			}
			dbTaskEntity := &sqldb.Timingtask{}
			dbTaskFiler := &sqldb.TimingtaskFilter{
				Serialno: task.Serialno,
			}
			sqlTaskFilter := s.sqlDatabase.NewFilter(dbTaskFiler, false, false)
			_, _ = s.sqlDatabase.Delete(dbTaskEntity, sqlTaskFilter)
		}
	}
}

func (s *SmsTask) getLastHtnRecord(task *doctor.Timingtask) []*doctor.BloodPressureRecord {
	dbHtnRecord := &sqldb.BloodPressureRecord{}
	dbFiler := &sqldb.BloodPressureRecordPatientFilter{
		PatientID: task.Patientid,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFiler, false, false)
	dbOrder := &sqldb.BloodGlucoseRecordDownOrder{}
	htnResults := make([]*doctor.BloodPressureRecord, 0)
	_ = s.sqlDatabase.SelectList(dbHtnRecord, func() {
		result := &doctor.BloodPressureRecord{}
		dbHtnRecord.CopyTo(result)
		htnResults = append(htnResults, result)
	}, dbOrder, sqlFilter)

	return htnResults
}

func (s *SmsTask) getLastDmRecord(task *doctor.Timingtask) []*doctor.BloodGlucoseRecord {
	dbDmRecord := &sqldb.BloodGlucoseRecord{}
	dbFiler := &sqldb.BloodPressureRecordPatientFilter{
		PatientID: task.Patientid,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFiler, false, false)
	dmResults := make([]*doctor.BloodGlucoseRecord, 0)
	dbOrder := &sqldb.BloodPressureRecordLastOrder{}
	_ = s.sqlDatabase.SelectList(dbDmRecord, func() {
		result := &doctor.BloodGlucoseRecord{}
		dbDmRecord.CopyTo(result)
		dmResults = append(dmResults, result)
	}, dbOrder, sqlFilter)

	return dmResults
}

func GenerateGuid() string {
	uuid := make([]byte, 16)
	n, err := io.ReadFull(rand.Reader, uuid)
	if n != len(uuid) || err != nil {
		return ""
	}

	uuid[8] = uuid[8]&^0xc0 | 0x80
	uuid[6] = uuid[6]&^0xf0 | 0x40

	return fmt.Sprintf("%x%x%x%x%x", uuid[0:4], uuid[4:6], uuid[6:8], uuid[8:10], uuid[10:])
}

func (s *SmsTask) setSmsArgument(argument *doctor.SmsParam, tempType, patientName, doctorName string) {
	dbSmsTempEntity := &sqldb.SmsTemplate{}
	dbFiler := &sqldb.SmsTemplateUseFilter{
		TemplateName: tempType,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFiler, false, false)
	_ = s.sqlDatabase.SelectOne(dbSmsTempEntity, sqlFilter)
	argument.TemplateCode = dbSmsTempEntity.TemplateCode
	argument.SmsContent = strings.Replace(strings.Replace(dbSmsTempEntity.TemplateContent, "${patientName}", patientName, -1), "${doctorName}", doctorName, -1)
}

func (s *Task) startWeChatRemindTask() () {
	accessToken, err := s.patient.GetWeChatAccessToken()
	if err != nil {
		return
	}
	dmTasks := s.getBloodGlucoseRemindTasks()

	type d map[string]*doctor.WeChatRemindDict
	dict := make(d)

	for _, task := range dmTasks {
		necessity := s.getDmTaskNecessity(task)
		if necessity == true {
			send := s.getMessageBodyByTemplateID(task.TemplateID, dict)
			argument := &doctor.WeChatRemindSend{
				PatientID: task.PatientID,
				ReceiverOpenID: task.OpenID,
				TemplateID: task.TemplateID,
				Page: send.Page,
				Data: s.setDataDate(send.Data),
				Lang: send.Lang,
				MiniprogramState: send.MiniprogramState,
			}
			s.patient.SubscribeMessageSend(accessToken, argument)
			s.createMessageRemindTask(task.PatientID)
		}
		s.setRemindTaskStatusComplete(task.SerialNo)
	}
}

func (s *Task) getBloodGlucoseRemindTasks() []*doctor.WeChatRemindTask {
	bloodGlucoseTasks := make([]*doctor.WeChatRemindTask, 0)

	now := time.Now()
	startDateTime := now.AddDate(0, 0, -4)
	endDateTime := now.AddDate(0, 0, -3)

	dbEntity := &sqldb.WeChatRemindTask{}
	dbFilter := &sqldb.WeChatRemindTaskFilter{
		StartDateTime: &startDateTime,
		EndDateTime: &endDateTime,
		Status: 0,
		Type: 2,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	err := s.sqlDatabase.SelectList(dbEntity, func() {
		result := &doctor.WeChatRemindTask{}
		dbEntity.CopyTo(result)
		bloodGlucoseTasks = append(bloodGlucoseTasks, result)
	}, nil, sqlFilter)
	if err != nil {
		return  nil
	}

	return bloodGlucoseTasks
}

func (s *Task) getDmTaskNecessity(task *doctor.WeChatRemindTask) bool {
	dbDmRecord := &sqldb.BloodGlucoseRecord{}
	sqlEntity := s.sqlDatabase.NewEntity()
	sqlEntity.Parse(dbDmRecord)

	sqlBuilder := s.sqlDatabase.NewBuilder()
	sqlBuilder.Select(sqlEntity.ScanFields(), false).From(sqlEntity.Name())
	sqlBuilder.Append("where PatientID = ? order by MeasureDateTime desc LIMIT 1")
	query := sqlBuilder.Query()

	sqlAccess, err := s.sqlDatabase.NewAccess(true)
	if err != nil {
		return false
	}
	defer sqlAccess.Close()

	row := sqlAccess.QueryRow(query, task.PatientID)
	err = row.Scan(sqlEntity.ScanArgs()...)
	if s.sqlDatabase.IsNoRows(err) {
		return true
	} else if err != nil {
		return false
	}

	result := &doctor.BloodGlucoseRecord{}
	dbDmRecord.CopyTo(result)

	now := time.Now()
	judgeTime := now.AddDate(0,0,-3)

	if time.Time(*result.MeasureDateTime).Before(judgeTime) {
		return true
	}

	return false
}

func (s *Task) getMessageBodyByTemplateID(templateID string, dict map[string]*doctor.WeChatRemindDict) *doctor.WeChatRemindDict {
	if res, ok := dict[templateID]; ok {
		return res
	}

	dbEntity := &sqldb.WeChatRemindDict{}
	dbFilter := &sqldb.WeChatRemindDictFilter{
		TemplateID: templateID,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)

	s.sqlDatabase.SelectOne(dbEntity, sqlFilter)

	result := &doctor.WeChatRemindDict{}
	dbEntity.CopyTo(result)

	dict[templateID] = result
	return result
}

func (s *Task) setRemindTaskStatusComplete(serialNo uint64) error {
	dbEntity := &sqldb.WeChatRemindTask{}
	dbFilter := &sqldb.WeChatRemindTaskPrimaryFilter{
		SerialNo: serialNo,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	err := s.sqlDatabase.SelectOne(dbEntity, sqlFilter)
	if err != nil {
		return err
	}

	dbEntity.Status = 1
	_, err = s.sqlDatabase.UpdateSelectiveByPrimaryKey(dbEntity)
	if err != nil {
		return err
	}

	return nil
}

type thing struct {
	Value string	`json:"value"`
}

type Data struct {
	Date1 	thing  `json:"date1"`
	Thing2	thing  `json:"thing2"`
}

func (s *Task) setDataDate(data interface{}) interface{} {
	var i Data
	js, err := json.Marshal(data)
	str := string(js)
	if err != nil {
		panic(err)
	}
	err = json.Unmarshal([]byte(str), &i)
	if err != nil {
		panic(err)
	}
	day := time.Now().Format("2006-01-02")
	i.Date1.Value = strings.Replace(i.Date1.Value, "${time}", day, -1)
	return i
}

func (s *Task) createMessageRemindTask(patientID uint64) {
	dbEntity := &sqldb.ManagedPatientIndex{}
	dbFilter := &sqldb.ManagedPatientIndexPatientIDFilter{
		PatientID: patientID,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	err := s.sqlDatabase.SelectOne(dbEntity, sqlFilter)

	h, _ := time.ParseDuration("1h")
	now := time.Now().Add(h)
	// 插入定时任务表
	dbEntityTiming := &sqldb.Timingtask{
		Patientid: patientID,
		Tasktype: 2,
		Operatorid: *dbEntity.HealthManagerID,
		DoctorName: *dbEntity.HealthManagerName,
		Timingstartdatetime:&now,
	}
	_, err = s.sqlDatabase.InsertSelective(dbEntityTiming)
	if err != nil {
		return
	}
}


























