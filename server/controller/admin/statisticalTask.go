package admin

import (
	"tlcbme_project/data/entity/sqldb"
	"tlcbme_project/data/model/doctor"
	"math"
	"time"
)

type StatisticalTask struct {
	Task
}

const (
	Data_Patient_All    = 1
	Data_Patient_Active = 2

	Data_BloodPressure_level_New = 3
	Data_BloodPressure_level_One = 4
	Data_BloodPressure_level_Two = 5
	Data_BloodPressure_level_End = 6

	Data_BloodGlcouse_level_New   = 7
	Data_BloodGlcouse_level_One   = 8
	Data_BloodGlcouse_level_Two   = 9
	Data_BloodGlcouse_level_Three = 10
	Data_BloodGlcouse_level_End   = 11

	Data_BloodPressure_All_Count      = 12
	Data_BloodPressure_Abnormal_Count = 13

	Data_BloodPressure_Rate = 14

	Data_BloodGlcouse_All_Count      = 15
	Data_BloodGlcouse_Abnormal_Count = 16

	Data_BloodGlcouse_Rate = 17

	Data_Sport  = 18
	Data_Diet   = 19
	Data_Drug   = 20
	Data_Weight = 21

	Data_Followup_Normal = 22
	Data_Followup_Alret  = 23

	Data_Alert_Rate  = 24
	Data_Alert_Count = 25

	Data_BloodPressure_High_Risk   = 26
	Data_BloodPressure_Medium_Risk = 27
	Data_BloodPressure_Low_Risk    = 28

	Data_BloodGlcouse_High_Risk   = 29
	Data_BloodGlcouse_Medium_Risk = 30
	Data_BloodGlcouse_Low_Risk    = 31
)

func (s *StatisticalTask) Start() {
	go func() {
		s.Cancel = make(chan bool, 1)
		now := time.Now()
		next1 := time.Date(now.Year(), now.Month(), now.Day(), s.Config.StatisticalTask.FixedTime.Hour, s.Config.StatisticalTask.FixedTime.Minute,
			s.Config.StatisticalTask.FixedTime.Second, 0, now.Location())
		next2 := next1.Add(24 * time.Hour)
		if next1.After(now) {
			s.Interval = next1.Sub(now)
		} else {
			s.Interval = next2.Sub(now)
		}
		for {
			t := time.NewTimer(s.Interval)
			go func() {
				select {
				case <-s.Cancel:
					break
				}
			}()
			<-t.C
			s.Interval = 24 * time.Hour
			go s.startTask()
		}
	}()
}

func (s *StatisticalTask) Stop() {
	s.Cancel <- true
}

func (s *StatisticalTask) startTask() {
	orgs := s.getOrgCodes()
	for _, orgCode := range orgs {
		// 1.活跃人数，管理人数
		s.savePatientInManageData(orgCode.OrgCode)
		// 2.高血压管理分级人数，按高血压管理分级来统计人数，柱状图，每根柱子代表不同管理等级的人数
		s.saveBloodPressureGradeData(orgCode.OrgCode)
		// 3.糖尿病管理分级人数，按糖尿病管理分级来统计人数，柱状图，每根柱子代表不同管理等级的人数
		s.saveBloodGlucoseGradeData(orgCode.OrgCode)
		// 4.血压数据=正常血压数据+异常血压数据
		s.saveBloodPressureData(orgCode.OrgCode)
		// 5.血压达标率=最近一次血压达标的人数/提交血压的总人数  最近一次血压数据未触发预警，即为达标，达标人数/总人数=达标率。
		s.saveBloodPressureCompliancerateData(orgCode.OrgCode)
		// 6.血糖数据=正常血糖数据+异常血糖数据
		s.saveBloodGlucoseData(orgCode.OrgCode)
		// 7.血糖达标率=最近一次血糖达标的人数/提交血糖的总人数  最近一次血糖数据未触发预警，即为达标，达标人数/总人数=达标率。
		s.saveBloodGlucoseCompliancerateData(orgCode.OrgCode)
		// 8.同血压数据为柱状图，只需按类型统计数量，例如饮食、运动、服药、体重等
		s.saveOtherData(orgCode.OrgCode)
		// 9.常规随访和预警随访放在一张图中，同正常血压数据和异常血压数据
		s.saveFollowupData(orgCode.OrgCode)
		// 10.预警情况 包括预警次数与预警当日解决率，预警当日解决率=预警当日解决/预警当日次数，月数据取当月平均值 。柱状图配合折线图，柱状图代表预警情况，折线图代表预警当日解决率
		s.saveAlertData(orgCode.OrgCode)
		// 11.高血压危险分层评估 高危xx人 中危xx人 低危xx人 按月计数
		s.saveBloodPressureRiskData(orgCode.OrgCode)
		// 12.糖尿病危险分层评估 高危xx人 中危xx人 低危xx人 按月计数
		s.saveBloodGlucoseRiskData(orgCode.OrgCode)
	}
}

func (s *StatisticalTask) getOrgCodes() []*doctor.OrgDict {
	sqlAccess, err := s.sqlDatabase.NewAccess(false)
	results := make([]*doctor.OrgDict, 0)
	if err != nil {
		return results
	}
	defer sqlAccess.Close()

	dbEntity := &sqldb.OrgDict{}
	sqlFilter := s.sqlDatabase.NewFilter(nil, false, false)
	err = sqlAccess.SelectList(dbEntity, func() {
		result := &doctor.OrgDict{}
		dbEntity.CopyTo(result)
		results = append(results, result)
	}, nil, sqlFilter)
	if err != nil {
		return results
	}
	return results
}

func (s *StatisticalTask) savePatientInManageData(s2 string, date ...time.Time) {
	year, month := s.getYearAndMonth()
	if len(date) > 0 {
		month = date[0].Month()
		year = uint64(date[0].Year())
	}
	end := time.Date(int(year), month, 1, 0, 0, 0, 0, time.Local).AddDate(0, 1, 0)
	sqlAccess, err := s.sqlDatabase.NewAccess(false)
	if err != nil {
		return
	}
	defer sqlAccess.Close()
	dbFilter := &sqldb.ManagedPatientIndexOrgCodeFilter{
		OrgCode:             &s2,
		ManageStartDateTime: &end,
	}
	dbFilter2 := &sqldb.ManagedPatientIndexActiveFilter{
		OrgCode:             &s2,
		PatientActiveDegree: 1,
		ManageStartDateTime: &end,
	}
	dbEntity := &sqldb.ManagedPatientIndex{}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	sqlFilter2 := s.sqlDatabase.NewFilter(dbFilter2, false, false)
	all, _ := sqlAccess.SelectCount(dbEntity, sqlFilter)
	active, _ := sqlAccess.SelectCount(dbEntity, sqlFilter2)

	dbFilter4 := &sqldb.StatisticrecordFilter{
		Year:    year,
		Month:   uint64(month),
		Orgcode: s2,
		Type:    Data_Patient_All,
	}
	dbFilter5 := &sqldb.StatisticrecordFilter{
		Year:    year,
		Month:   uint64(month),
		Orgcode: s2,
		Type:    Data_Patient_Active,
	}
	sqlFilter4 := s.sqlDatabase.NewFilter(dbFilter4, false, false)
	sqlFilter5 := s.sqlDatabase.NewFilter(dbFilter5, false, false)
	dbStaticRecordEntity := &sqldb.Statisticrecord{}
	err = s.sqlDatabase.SelectOne(dbStaticRecordEntity, sqlFilter4)
	dbStaticEntity := &sqldb.Statisticrecord{
		Year:    year,
		Month:   uint64(month),
		Count:   &all,
		Orgcode: s2,
		Type:    Data_Patient_All,
	}
	dbStaticActiveEntity := &sqldb.Statisticrecord{
		Year:    year,
		Month:   uint64(month),
		Count:   &active,
		Orgcode: s2,
		Type:    Data_Patient_Active,
	}
	exist := true
	if err != nil {
		if s.sqlDatabase.IsNoRows(err) {
			exist = false
		} else {
			return
		}
	}
	if exist {
		_, _ = sqlAccess.UpdateSelective(dbStaticEntity, sqlFilter4)
		_, _ = sqlAccess.UpdateSelective(dbStaticActiveEntity, sqlFilter5)
	} else {
		_, _ = sqlAccess.InsertSelective(dbStaticEntity)
		_, _ = sqlAccess.InsertSelective(dbStaticActiveEntity)
	}
}

func (s *StatisticalTask) saveBloodPressureGradeData(s2 string, date ...time.Time) {
	year, month := s.getYearAndMonth()
	if len(date) > 0 {
		month = date[0].Month()
		year = uint64(date[0].Year())
	}
	endStart := time.Date(int(year), month, 1, 0, 0, 0, 0, time.Local).AddDate(0, 1, 0)
	sqlAccess, err := s.sqlDatabase.NewAccess(false)
	if err != nil {
		return
	}
	defer sqlAccess.Close()
	newData := uint64(0)
	one := uint64(0)
	two := uint64(0)
	end := uint64(0)
	htn := uint64(1)
	dbFilter := &sqldb.ViewManagedIndexHtnFilter{
		OrgCode:             &s2,
		ManageLevel:         []byte("0"),
		ManageClassCode:     &htn,
		ManageStartDateTime: &endStart,
	}
	dbFilter2 := &sqldb.ViewManagedIndexHtnFilter{
		OrgCode:             &s2,
		ManageLevel:         []byte("1"),
		ManageClassCode:     &htn,
		ManageStartDateTime: &endStart,
	}
	dbFilter3 := &sqldb.ViewManagedIndexHtnFilter{
		OrgCode:             &s2,
		ManageLevel:         []byte("2"),
		ManageClassCode:     &htn,
		ManageStartDateTime: &endStart,
	}
	dbFilter4 := &sqldb.ViewManagedIndexHtnFilter{
		OrgCode:             &s2,
		ManageLevel:         []byte("-1"),
		ManageClassCode:     &htn,
		ManageStartDateTime: &endStart,
	}
	dbEntity := &sqldb.ViewManagedIndex{}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	sqlFilter2 := s.sqlDatabase.NewFilter(dbFilter2, false, false)
	sqlFilter3 := s.sqlDatabase.NewFilter(dbFilter3, false, false)
	sqlFilter4 := s.sqlDatabase.NewFilter(dbFilter4, false, false)
	newData, _ = s.sqlDatabase.SelectCount(dbEntity, sqlFilter)
	one, _ = s.sqlDatabase.SelectCount(dbEntity, sqlFilter2)
	two, _ = s.sqlDatabase.SelectCount(dbEntity, sqlFilter3)
	end, _ = s.sqlDatabase.SelectCount(dbEntity, sqlFilter4)
	dbStaticEntity := &sqldb.Statisticrecord{
		Year:    year,
		Month:   uint64(month),
		Count:   &newData,
		Orgcode: s2,
		Type:    Data_BloodPressure_level_New,
	}
	dbStaticEntity2 := &sqldb.Statisticrecord{
		Year:    year,
		Month:   uint64(month),
		Count:   &one,
		Orgcode: s2,
		Type:    Data_BloodPressure_level_One,
	}
	dbStaticEntity3 := &sqldb.Statisticrecord{
		Year:    year,
		Month:   uint64(month),
		Count:   &two,
		Orgcode: s2,
		Type:    Data_BloodPressure_level_Two,
	}
	dbStaticEntity4 := &sqldb.Statisticrecord{
		Year:    year,
		Month:   uint64(month),
		Count:   &end,
		Orgcode: s2,
		Type:    Data_BloodPressure_level_End,
	}

	dbUpdateFilter := &sqldb.StatisticrecordFilter{
		Year:    year,
		Month:   uint64(month),
		Orgcode: s2,
		Type:    Data_BloodPressure_level_New,
	}
	dbUpdateFilter2 := &sqldb.StatisticrecordFilter{
		Year:    year,
		Month:   uint64(month),
		Orgcode: s2,
		Type:    Data_BloodPressure_level_One,
	}
	dbUpdateFilter3 := &sqldb.StatisticrecordFilter{
		Year:    year,
		Month:   uint64(month),
		Orgcode: s2,
		Type:    Data_BloodPressure_level_Two,
	}
	dbUpdateFilter4 := &sqldb.StatisticrecordFilter{
		Year:    year,
		Month:   uint64(month),
		Orgcode: s2,
		Type:    Data_BloodPressure_level_End,
	}
	sqlUpdateFilter := s.sqlDatabase.NewFilter(dbUpdateFilter, false, false)
	sqlUpdateFilter2 := s.sqlDatabase.NewFilter(dbUpdateFilter2, false, false)
	sqlUpdateFilter3 := s.sqlDatabase.NewFilter(dbUpdateFilter3, false, false)
	sqlUpdateFilter4 := s.sqlDatabase.NewFilter(dbUpdateFilter4, false, false)
	dbStaticRecordEntity := &sqldb.Statisticrecord{}
	err = s.sqlDatabase.SelectOne(dbStaticRecordEntity, sqlUpdateFilter)
	exist := true
	if err != nil {
		if s.sqlDatabase.IsNoRows(err) {
			exist = false
		} else {
			return
		}
	}
	if exist {
		_, _ = sqlAccess.UpdateSelective(dbStaticEntity, sqlUpdateFilter)
		_, _ = sqlAccess.UpdateSelective(dbStaticEntity2, sqlUpdateFilter2)
		_, _ = sqlAccess.UpdateSelective(dbStaticEntity3, sqlUpdateFilter3)
		_, _ = sqlAccess.UpdateSelective(dbStaticEntity4, sqlUpdateFilter4)
	} else {
		_, _ = sqlAccess.InsertSelective(dbStaticEntity)
		_, _ = sqlAccess.InsertSelective(dbStaticEntity2)
		_, _ = sqlAccess.InsertSelective(dbStaticEntity3)
		_, _ = sqlAccess.InsertSelective(dbStaticEntity4)
	}
}

func (s *StatisticalTask) saveBloodGlucoseGradeData(s2 string, date ...time.Time) {
	year, month := s.getYearAndMonth()
	if len(date) > 0 {
		month = date[0].Month()
		year = uint64(date[0].Year())
	}
	endStart := time.Date(int(year), month, 1, 0, 0, 0, 0, time.Local).AddDate(0, 1, 0)
	sqlAccess, err := s.sqlDatabase.NewAccess(false)
	if err != nil {
		return
	}
	newData := uint64(0)
	one := uint64(0)
	two := uint64(0)
	three := uint64(0)
	end := uint64(0)

	defer sqlAccess.Close()
	dm := uint64(2)
	dbFilter := &sqldb.ViewManagedIndexExtFilter{
		OrgCode:             &s2,
		ManageLevel:         []byte("0"),
		ManageClassCode:     &dm,
		ManageStartDateTime: &endStart,
	}
	dbFilter2 := &sqldb.ViewManagedIndexExtFilter{
		OrgCode:             &s2,
		ManageLevel:         []byte("10"),
		ManageClassCode:     &dm,
		ManageStartDateTime: &endStart,
	}
	dbFilter3 := &sqldb.ViewManagedIndexExtFilter{
		OrgCode:             &s2,
		ManageLevel:         []byte("20"),
		ManageClassCode:     &dm,
		ManageStartDateTime: &endStart,
	}
	dbFilter4 := &sqldb.ViewManagedIndexExtFilter{
		OrgCode:             &s2,
		ManageLevel:         []byte("30"),
		ManageClassCode:     &dm,
		ManageStartDateTime: &endStart,
	}
	dbFilter5 := &sqldb.ViewManagedIndexExtFilter{
		OrgCode:             &s2,
		ManageLevel:         []byte("-1"),
		ManageClassCode:     &dm,
		ManageStartDateTime: &endStart,
	}
	dbEntity := &sqldb.ViewManagedIndexCount{}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	sqlFilter2 := s.sqlDatabase.NewFilter(dbFilter2, false, false)
	sqlFilter3 := s.sqlDatabase.NewFilter(dbFilter3, false, false)
	sqlFilter4 := s.sqlDatabase.NewFilter(dbFilter4, false, false)
	sqlFilter5 := s.sqlDatabase.NewFilter(dbFilter5, false, false)
	newData, _ = s.sqlDatabase.SelectCount(dbEntity, sqlFilter)
	one, _ = s.sqlDatabase.SelectCount(dbEntity, sqlFilter2)
	two, _ = s.sqlDatabase.SelectCount(dbEntity, sqlFilter3)
	three, _ = s.sqlDatabase.SelectCount(dbEntity, sqlFilter4)
	end, _ = s.sqlDatabase.SelectCount(dbEntity, sqlFilter5)
	dbStaticEntity := &sqldb.Statisticrecord{
		Year:    year,
		Month:   uint64(month),
		Count:   &newData,
		Orgcode: s2,
		Type:    Data_BloodGlcouse_level_New,
	}
	dbStaticEntity2 := &sqldb.Statisticrecord{
		Year:    year,
		Month:   uint64(month),
		Count:   &one,
		Orgcode: s2,
		Type:    Data_BloodGlcouse_level_One,
	}
	dbStaticEntity3 := &sqldb.Statisticrecord{
		Year:    year,
		Month:   uint64(month),
		Count:   &two,
		Orgcode: s2,
		Type:    Data_BloodGlcouse_level_Two,
	}
	dbStaticEntity4 := &sqldb.Statisticrecord{
		Year:    year,
		Month:   uint64(month),
		Count:   &three,
		Orgcode: s2,
		Type:    Data_BloodGlcouse_level_Three,
	}
	dbStaticEntity5 := &sqldb.Statisticrecord{
		Year:    year,
		Month:   uint64(month),
		Count:   &end,
		Orgcode: s2,
		Type:    Data_BloodGlcouse_level_End,
	}

	dbUpdateFilter := &sqldb.StatisticrecordFilter{
		Year:    year,
		Month:   uint64(month),
		Orgcode: s2,
		Type:    Data_BloodGlcouse_level_New,
	}
	dbUpdateFilter2 := &sqldb.StatisticrecordFilter{
		Year:    year,
		Month:   uint64(month),
		Orgcode: s2,
		Type:    Data_BloodGlcouse_level_One,
	}
	dbUpdateFilter3 := &sqldb.StatisticrecordFilter{
		Year:    year,
		Month:   uint64(month),
		Orgcode: s2,
		Type:    Data_BloodGlcouse_level_Two,
	}
	dbUpdateFilter4 := &sqldb.StatisticrecordFilter{
		Year:    year,
		Month:   uint64(month),
		Orgcode: s2,
		Type:    Data_BloodGlcouse_level_Three,
	}
	dbUpdateFilter5 := &sqldb.StatisticrecordFilter{
		Year:    year,
		Month:   uint64(month),
		Orgcode: s2,
		Type:    Data_BloodGlcouse_level_End,
	}
	sqlUpdateFilter := s.sqlDatabase.NewFilter(dbUpdateFilter, false, false)
	sqlUpdateFilter2 := s.sqlDatabase.NewFilter(dbUpdateFilter2, false, false)
	sqlUpdateFilter3 := s.sqlDatabase.NewFilter(dbUpdateFilter3, false, false)
	sqlUpdateFilter4 := s.sqlDatabase.NewFilter(dbUpdateFilter4, false, false)
	sqlUpdateFilter5 := s.sqlDatabase.NewFilter(dbUpdateFilter5, false, false)
	dbStaticRecordEntity := &sqldb.Statisticrecord{}
	err = s.sqlDatabase.SelectOne(dbStaticRecordEntity, sqlUpdateFilter)
	exist := true
	if err != nil {
		if s.sqlDatabase.IsNoRows(err) {
			exist = false
		} else {
			return
		}
	}
	if exist {
		_, _ = sqlAccess.UpdateSelective(dbStaticEntity, sqlUpdateFilter)
		_, _ = sqlAccess.UpdateSelective(dbStaticEntity2, sqlUpdateFilter2)
		_, _ = sqlAccess.UpdateSelective(dbStaticEntity3, sqlUpdateFilter3)
		_, _ = sqlAccess.UpdateSelective(dbStaticEntity4, sqlUpdateFilter4)
		_, _ = sqlAccess.UpdateSelective(dbStaticEntity5, sqlUpdateFilter5)
	} else {
		_, _ = sqlAccess.InsertSelective(dbStaticEntity)
		_, _ = sqlAccess.InsertSelective(dbStaticEntity2)
		_, _ = sqlAccess.InsertSelective(dbStaticEntity3)
		_, _ = sqlAccess.InsertSelective(dbStaticEntity4)
		_, _ = sqlAccess.InsertSelective(dbStaticEntity5)
	}
}

func (s *StatisticalTask) saveBloodPressureData(s2 string, date ...time.Time) {
	year, month := s.getYearAndMonth()
	if len(date) > 0 {
		month = date[0].Month()
		year = uint64(date[0].Year())
	}
	endStart := time.Date(int(year), month, 1, 0, 0, 0, 0, time.Local).AddDate(0, 1, 0)
	sqlAccess, err := s.sqlDatabase.NewAccess(false)
	if err != nil {
		return
	}

	defer sqlAccess.Close()
	dbEntity := &sqldb.Viewbloodpressurerecord{}
	dbFilter := &sqldb.ViewBloodPressureFilter{
		Orgcode:         &s2,
		Measuredatetime: &endStart,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	all, _ := sqlAccess.SelectCount(dbEntity, sqlFilter)
	dbFilter2 := &sqldb.ViewBloodPressureQualifiedFilter{
		Orgcode:               &s2,
		SystolicpressureLow:   90,
		SystolicpressureHigh:  180,
		DiastolicpressureLow:  60,
		DiastolicpressureHigh: 110,
		Measuredatetime:       &endStart,
	}
	sqlFilter2 := s.sqlDatabase.NewFilter(dbFilter2, false, false)
	Qualified, _ := sqlAccess.SelectCount(dbEntity, sqlFilter2)
	dbStaticEntity := &sqldb.Statisticrecord{
		Year:    year,
		Month:   uint64(month),
		Orgcode: s2,
		Count:   &all,
		Type:    Data_BloodPressure_All_Count,
	}
	abNormal := all - Qualified
	dbStaticEntity2 := &sqldb.Statisticrecord{
		Year:    year,
		Month:   uint64(month),
		Orgcode: s2,
		Count:   &abNormal,
		Type:    Data_BloodPressure_Abnormal_Count,
	}
	dbUpdateFilter := &sqldb.StatisticrecordFilter{
		Year:    year,
		Month:   uint64(month),
		Orgcode: s2,
		Type:    Data_BloodPressure_All_Count,
	}
	dbUpdateFilter2 := &sqldb.StatisticrecordFilter{
		Year:    year,
		Month:   uint64(month),
		Orgcode: s2,
		Type:    Data_BloodPressure_Abnormal_Count,
	}
	sqlUpdateFilter := s.sqlDatabase.NewFilter(dbUpdateFilter, false, false)
	sqlUpdateFilter2 := s.sqlDatabase.NewFilter(dbUpdateFilter2, false, false)
	dbStaticRecordEntity := &sqldb.Statisticrecord{}
	err = s.sqlDatabase.SelectOne(dbStaticRecordEntity, sqlUpdateFilter)
	exist := true
	if err != nil {
		if s.sqlDatabase.IsNoRows(err) {
			exist = false
		} else {
			return
		}
	}
	if exist {
		_, _ = sqlAccess.UpdateSelective(dbStaticEntity, sqlUpdateFilter)
		_, _ = sqlAccess.UpdateSelective(dbStaticEntity2, sqlUpdateFilter2)
	} else {
		_, _ = sqlAccess.InsertSelective(dbStaticEntity)
		_, _ = sqlAccess.InsertSelective(dbStaticEntity2)
	}
}

func (s *StatisticalTask) saveBloodPressureCompliancerateData(s2 string, date ...time.Time) {
	year, month := s.getYearAndMonth()
	if len(date) > 0 {
		month = date[0].Month()
		year = uint64(date[0].Year())
	}
	endStart := time.Date(int(year), month, 1, 0, 0, 0, 0, time.Local).AddDate(0, 1, 0)
	sqlAccess, err := s.sqlDatabase.NewAccess(false)
	if err != nil {
		return
	}
	all := uint64(0)
	qualified := uint64(0)

	defer sqlAccess.Close()
	dbEntity := &sqldb.Viewbloodpressurerecord{}
	dbOrder := &sqldb.ViewBloodPressureOrder{}
	dbFilter := &sqldb.ViewBloodPressureFilter{
		Orgcode:         &s2,
		Measuredatetime: &endStart,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	_ = sqlAccess.SelectDistinct(dbEntity, func() {
		if dbEntity.Systolicpressure >= 90 && dbEntity.Systolicpressure <= 180 && dbEntity.Diastolicpressure >= 60 && dbEntity.Diastolicpressure <= 110 {
			qualified++
		}
		all++
	}, dbOrder, sqlFilter)
	rate := float64(float64(qualified) / float64(all))
	dbStaticEntity := &sqldb.Statisticrecord{
		Year:    year,
		Month:   uint64(month),
		Orgcode: s2,
		Rate:    &rate,
		Type:    Data_BloodPressure_Rate,
	}
	dbUpdateFilter := &sqldb.StatisticrecordFilter{
		Year:    year,
		Month:   uint64(month),
		Orgcode: s2,
		Type:    Data_BloodPressure_Rate,
	}
	sqlUpdateFilter := s.sqlDatabase.NewFilter(dbUpdateFilter, false, false)
	dbStaticRecordEntity := &sqldb.Statisticrecord{}
	err = s.sqlDatabase.SelectOne(dbStaticRecordEntity, sqlUpdateFilter)
	exist := true
	if err != nil {
		if s.sqlDatabase.IsNoRows(err) {
			exist = false
		} else {
			return
		}
	}
	if exist {
		_, _ = sqlAccess.UpdateSelective(dbStaticEntity, sqlUpdateFilter)
	} else {
		_, _ = sqlAccess.InsertSelective(dbStaticEntity)
	}
}

func (s *StatisticalTask) saveBloodGlucoseData(s2 string, date ...time.Time) {
	year, month := s.getYearAndMonth()
	if len(date) > 0 {
		month = date[0].Month()
		year = uint64(date[0].Year())
	}
	endStart := time.Date(int(year), month, 1, 0, 0, 0, 0, time.Local).AddDate(0, 1, 0)
	sqlAccess, err := s.sqlDatabase.NewAccess(false)
	if err != nil {
		return
	}

	defer sqlAccess.Close()
	dbEntity := &sqldb.Viewbloodglucoserecord{}
	dbFilter := &sqldb.ViewBloodGlucoseFilter{
		Orgcode:         &s2,
		Measuredatetime: &endStart,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	all, _ := sqlAccess.SelectCount(dbEntity, sqlFilter)
	dbFilter2 := &sqldb.ViewBloodGlucoseQualifiedFilter{
		Orgcode:          &s2,
		BloodglucoseLow:  3.9,
		BloodglucoseHigh: 16.7,
		Measuredatetime:  &endStart,
	}
	sqlFilter2 := s.sqlDatabase.NewFilter(dbFilter2, false, false)
	Qualified, _ := sqlAccess.SelectCount(dbEntity, sqlFilter2)
	dbStaticEntity := &sqldb.Statisticrecord{
		Year:    year,
		Month:   uint64(month),
		Orgcode: s2,
		Count:   &all,
		Type:    Data_BloodGlcouse_All_Count,
	}
	abNormal := all - Qualified
	dbStaticEntity2 := &sqldb.Statisticrecord{
		Year:    year,
		Month:   uint64(month),
		Orgcode: s2,
		Count:   &abNormal,
		Type:    Data_BloodGlcouse_Abnormal_Count,
	}
	dbUpdateFilter := &sqldb.StatisticrecordFilter{
		Year:    year,
		Month:   uint64(month),
		Orgcode: s2,
		Type:    Data_BloodGlcouse_All_Count,
	}
	dbUpdateFilter2 := &sqldb.StatisticrecordFilter{
		Year:    year,
		Month:   uint64(month),
		Orgcode: s2,
		Type:    Data_BloodGlcouse_Abnormal_Count,
	}
	sqlUpdateFilter := s.sqlDatabase.NewFilter(dbUpdateFilter, false, false)
	sqlUpdateFilter2 := s.sqlDatabase.NewFilter(dbUpdateFilter2, false, false)
	dbStaticRecordEntity := &sqldb.Statisticrecord{}
	err = s.sqlDatabase.SelectOne(dbStaticRecordEntity, sqlUpdateFilter)
	exist := true
	if err != nil {
		if s.sqlDatabase.IsNoRows(err) {
			exist = false
		} else {
			return
		}
	}
	if exist {
		_, _ = sqlAccess.UpdateSelective(dbStaticEntity, sqlUpdateFilter)
		_, _ = sqlAccess.UpdateSelective(dbStaticEntity2, sqlUpdateFilter2)
	} else {
		_, _ = sqlAccess.InsertSelective(dbStaticEntity)
		_, _ = sqlAccess.InsertSelective(dbStaticEntity2)
	}
}

func (s *StatisticalTask) saveBloodGlucoseCompliancerateData(s2 string, date ...time.Time) {
	year, month := s.getYearAndMonth()
	if len(date) > 0 {
		month = date[0].Month()
		year = uint64(date[0].Year())
	}
	endStart := time.Date(int(year), month, 1, 0, 0, 0, 0, time.Local).AddDate(0, 1, 0)
	sqlAccess, err := s.sqlDatabase.NewAccess(false)
	if err != nil {
		return
	}
	all := uint64(0)
	qualified := uint64(0)

	defer sqlAccess.Close()
	dbEntity := &sqldb.Viewbloodglucoserecord{}
	dbOrder := &sqldb.ViewBloodGlucoseOrder{}
	dbFilter := &sqldb.ViewBloodGlucoseFilter{
		Orgcode:         &s2,
		Measuredatetime: &endStart,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	_ = sqlAccess.SelectDistinct(dbEntity, func() {
		if dbEntity.Bloodglucose > 3.9 && dbEntity.Bloodglucose <= 16.7 {
			qualified++
		}
		all++
	}, dbOrder, sqlFilter)
	rate := float64(float64(qualified) / float64(all))
	dbStaticEntity := &sqldb.Statisticrecord{
		Year:    year,
		Month:   uint64(month),
		Orgcode: s2,
		Rate:    &rate,
		Type:    Data_BloodGlcouse_Rate,
	}
	dbUpdateFilter := &sqldb.StatisticrecordFilter{
		Year:    year,
		Month:   uint64(month),
		Orgcode: s2,
		Type:    Data_BloodGlcouse_Rate,
	}
	sqlUpdateFilter := s.sqlDatabase.NewFilter(dbUpdateFilter, false, false)
	dbStaticRecordEntity := &sqldb.Statisticrecord{}
	err = s.sqlDatabase.SelectOne(dbStaticRecordEntity, sqlUpdateFilter)
	exist := true
	if err != nil {
		if s.sqlDatabase.IsNoRows(err) {
			exist = false
		} else {
			return
		}
	}
	if exist {
		_, _ = sqlAccess.UpdateSelective(dbStaticEntity, sqlUpdateFilter)
	} else {
		_, _ = sqlAccess.InsertSelective(dbStaticEntity)
	}
}

func (s *StatisticalTask) saveOtherData(s2 string, date ...time.Time) {
	year, month := s.getYearAndMonth()
	if len(date) > 0 {
		month = date[0].Month()
		year = uint64(date[0].Year())
	}
	endStart := time.Date(int(year), month, 1, 0, 0, 0, 0, time.Local).AddDate(0, 1, 0)
	sqlAccess, err := s.sqlDatabase.NewAccess(false)
	if err != nil {
		return
	}

	defer sqlAccess.Close()
	dbEntity := &sqldb.Viewsportdietorgrecord{}
	dbFilter := &sqldb.ViewsportdietorgrecordFilter{
		Orgcode:        &s2,
		RecordType:     "运动",
		Happendatetime: &endStart,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	sport, _ := sqlAccess.SelectCount(dbEntity, sqlFilter)
	dbFilter2 := &sqldb.ViewsportdietorgrecordFilter{
		Orgcode:        &s2,
		RecordType:     "饮食",
		Happendatetime: &endStart,
	}
	sqlFilter2 := s.sqlDatabase.NewFilter(dbFilter2, false, false)
	diet, _ := sqlAccess.SelectCount(dbEntity, sqlFilter2)

	dbEntity3 := &sqldb.Viewdruguserecord{}
	dbFilter3 := &sqldb.ViewdruguserecordFilter{
		Orgcode:     &s2,
		Usedatetime: &endStart,
	}
	sqlFilter3 := s.sqlDatabase.NewFilter(dbFilter3, false, false)
	drug, _ := sqlAccess.SelectCount(dbEntity3, sqlFilter3)

	dbEntity4 := &sqldb.Viewweightrecord{}
	dbFilter4 := &sqldb.ViewweightrecordFilter{
		Orgcode:         &s2,
		Measuredatetime: &endStart,
	}
	sqlFilter4 := s.sqlDatabase.NewFilter(dbFilter4, false, false)
	weight, _ := sqlAccess.SelectCount(dbEntity4, sqlFilter4)
	dbStaticEntity := &sqldb.Statisticrecord{
		Year:    year,
		Month:   uint64(month),
		Orgcode: s2,
		Count:   &sport,
		Type:    Data_Sport,
	}
	dbStaticEntity2 := &sqldb.Statisticrecord{
		Year:    year,
		Month:   uint64(month),
		Orgcode: s2,
		Count:   &diet,
		Type:    Data_Diet,
	}
	dbStaticEntity3 := &sqldb.Statisticrecord{
		Year:    year,
		Month:   uint64(month),
		Orgcode: s2,
		Count:   &drug,
		Type:    Data_Drug,
	}
	dbStaticEntity4 := &sqldb.Statisticrecord{
		Year:    year,
		Month:   uint64(month),
		Orgcode: s2,
		Count:   &weight,
		Type:    Data_Weight,
	}
	dbUpdateFilter := &sqldb.StatisticrecordFilter{
		Year:    year,
		Month:   uint64(month),
		Orgcode: s2,
		Type:    Data_Sport,
	}
	dbUpdateFilter2 := &sqldb.StatisticrecordFilter{
		Year:    year,
		Month:   uint64(month),
		Orgcode: s2,
		Type:    Data_Diet,
	}
	dbUpdateFilter3 := &sqldb.StatisticrecordFilter{
		Year:    year,
		Month:   uint64(month),
		Orgcode: s2,
		Type:    Data_Drug,
	}
	dbUpdateFilter4 := &sqldb.StatisticrecordFilter{
		Year:    year,
		Month:   uint64(month),
		Orgcode: s2,
		Type:    Data_Weight,
	}
	sqlUpdateFilter := s.sqlDatabase.NewFilter(dbUpdateFilter, false, false)
	sqlUpdateFilter2 := s.sqlDatabase.NewFilter(dbUpdateFilter2, false, false)
	sqlUpdateFilter3 := s.sqlDatabase.NewFilter(dbUpdateFilter3, false, false)
	sqlUpdateFilter4 := s.sqlDatabase.NewFilter(dbUpdateFilter4, false, false)
	dbStaticRecordEntity := &sqldb.Statisticrecord{}
	err = s.sqlDatabase.SelectOne(dbStaticRecordEntity, sqlUpdateFilter)
	exist := true
	if err != nil {
		if s.sqlDatabase.IsNoRows(err) {
			exist = false
		} else {
			return
		}
	}
	if exist {
		_, _ = sqlAccess.UpdateSelective(dbStaticEntity, sqlUpdateFilter)
		_, _ = sqlAccess.UpdateSelective(dbStaticEntity2, sqlUpdateFilter2)
		_, _ = sqlAccess.UpdateSelective(dbStaticEntity3, sqlUpdateFilter3)
		_, _ = sqlAccess.UpdateSelective(dbStaticEntity4, sqlUpdateFilter4)
	} else {
		_, _ = sqlAccess.InsertSelective(dbStaticEntity)
		_, _ = sqlAccess.InsertSelective(dbStaticEntity2)
		_, _ = sqlAccess.InsertSelective(dbStaticEntity3)
		_, _ = sqlAccess.InsertSelective(dbStaticEntity4)
	}
}

func (s *StatisticalTask) saveFollowupData(s2 string, date ...time.Time) {
	year, month := s.getYearAndMonth()
	sqlAccess, err := s.sqlDatabase.NewAccess(false)
	if err != nil {
		return
	}
	if len(date) > 0 {
		month = date[0].Month()
		year = uint64(date[0].Year())
	}
	endStart := time.Date(int(year), month, 1, 0, 0, 0, 0, time.Local).AddDate(0, 1, 0)
	defer sqlAccess.Close()
	dbEntity := &sqldb.Viewfollowuprecord{}
	dbFilter := &sqldb.ViewfollowuprecordFilter{
		Orgcode:          &s2,
		FollowupDateTime: &endStart,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	all, _ := sqlAccess.SelectCount(dbEntity, sqlFilter)
	dbFilter2 := &sqldb.ViewfollowuprecordFilter{
		Orgcode:          &s2,
		Followuptype:     "预警干预",
		FollowupDateTime: &endStart,
	}
	sqlFilter2 := s.sqlDatabase.NewFilter(dbFilter2, false, false)
	alert, _ := sqlAccess.SelectCount(dbEntity, sqlFilter2)
	normal := all - alert
	dbStaticEntity := &sqldb.Statisticrecord{
		Year:    year,
		Month:   uint64(month),
		Orgcode: s2,
		Count:   &normal,
		Type:    Data_Followup_Normal,
	}
	dbStaticEntity2 := &sqldb.Statisticrecord{
		Year:    year,
		Month:   uint64(month),
		Orgcode: s2,
		Count:   &alert,
		Type:    Data_Followup_Alret,
	}
	dbUpdateFilter := &sqldb.StatisticrecordFilter{
		Year:    year,
		Month:   uint64(month),
		Orgcode: s2,
		Type:    Data_Followup_Normal,
	}
	dbUpdateFilter2 := &sqldb.StatisticrecordFilter{
		Year:    year,
		Month:   uint64(month),
		Orgcode: s2,
		Type:    Data_Followup_Alret,
	}
	sqlUpdateFilter := s.sqlDatabase.NewFilter(dbUpdateFilter, false, false)
	sqlUpdateFilter2 := s.sqlDatabase.NewFilter(dbUpdateFilter2, false, false)
	dbStaticRecordEntity := &sqldb.Statisticrecord{}
	err = s.sqlDatabase.SelectOne(dbStaticRecordEntity, sqlUpdateFilter)
	exist := true
	if err != nil {
		if s.sqlDatabase.IsNoRows(err) {
			exist = false
		} else {
			return
		}
	}
	if exist {
		_, _ = sqlAccess.UpdateSelective(dbStaticEntity, sqlUpdateFilter)
		_, _ = sqlAccess.UpdateSelective(dbStaticEntity2, sqlUpdateFilter2)
	} else {
		_, _ = sqlAccess.InsertSelective(dbStaticEntity)
		_, _ = sqlAccess.InsertSelective(dbStaticEntity2)
	}
}

func (s *StatisticalTask) saveAlertData(s2 string, date ...time.Time) {
	year, month := s.getYearAndMonth()
	sqlAccess, err := s.sqlDatabase.NewAccess(false)
	if len(date) > 0 {
		month = date[0].Month()
		year = uint64(date[0].Year())
	}
	endStart := time.Date(int(year), month, 1, 0, 0, 0, 0, time.Local).AddDate(0, 1, 0)
	if err != nil {
		return
	}
	defer sqlAccess.Close()
	end := time.Now()
	if end.After(endStart) {
		end = endStart
	}
	one := time.Date(int(year), month, 1, 0, 0, 0, 0, time.Local)
	days := int(math.Floor((end.Sub(one).Hours()) / 24))
	useDays := days
	rate := float64(0)
	allCount := uint64(0)
	for i := 1; i <= days; i++ {
		startDate := one.AddDate(0, 0, i-1)
		endDate := one.AddDate(0, 0, i)
		dbAllFilter := &sqldb.ViewAlertRecordStatFilter{
			OrgCode:        &s2,
			AlertStartDate: &startDate,
			AlertEndDate:   &endDate,
		}
		dbEntity := &sqldb.ViewAlertRecord{}
		dbHandleFilter := &sqldb.ViewAlertRecordStatFilter{
			OrgCode:        &s2,
			AlertStartDate: &startDate,
			AlertEndDate:   &endDate,
			Statuses:       []uint64{1},
		}
		sqlAllFilter := s.sqlDatabase.NewFilter(dbAllFilter, false, false)
		sqlHandleFilter := s.sqlDatabase.NewFilter(dbHandleFilter, false, false)
		all, _ := sqlAccess.SelectCount(dbEntity, sqlAllFilter)
		handle, _ := sqlAccess.SelectCount(dbEntity, sqlHandleFilter)
		if all == 0 {
			useDays--
		} else {
			rate += float64(float64(handle) / float64(all))
		}
		allCount += all
	}
	averageRate := float64(0)
	if useDays != 0 {
		averageRate = float64(float64(rate) / float64(useDays))
	}

	dbUpdateFilter := &sqldb.StatisticrecordFilter{
		Year:    year,
		Month:   uint64(month),
		Orgcode: s2,
		Type:    Data_Alert_Rate,
	}
	dbUpdateFilter2 := &sqldb.StatisticrecordFilter{
		Year:    year,
		Month:   uint64(month),
		Orgcode: s2,
		Type:    Data_Alert_Count,
	}
	dbStaticEntity := &sqldb.Statisticrecord{
		Year:    year,
		Month:   uint64(month),
		Orgcode: s2,
		Rate:    &averageRate,
		Type:    Data_Alert_Rate,
	}
	dbStaticEntity2 := &sqldb.Statisticrecord{
		Year:    year,
		Month:   uint64(month),
		Orgcode: s2,
		Count:   &allCount,
		Type:    Data_Alert_Count,
	}
	sqlUpdateFilter := s.sqlDatabase.NewFilter(dbUpdateFilter, false, false)
	sqlUpdateFilter2 := s.sqlDatabase.NewFilter(dbUpdateFilter2, false, false)
	dbStaticRecordEntity := &sqldb.Statisticrecord{}
	err = s.sqlDatabase.SelectOne(dbStaticRecordEntity, sqlUpdateFilter)
	exist := true
	if err != nil {
		if s.sqlDatabase.IsNoRows(err) {
			exist = false
		} else {
			return
		}
	}
	if exist {
		_, _ = sqlAccess.UpdateSelective(dbStaticEntity, sqlUpdateFilter)
		_, _ = sqlAccess.UpdateSelective(dbStaticEntity2, sqlUpdateFilter2)
	} else {
		_, _ = sqlAccess.InsertSelective(dbStaticEntity)
		_, _ = sqlAccess.InsertSelective(dbStaticEntity2)
	}
}

func (s *StatisticalTask) saveBloodPressureRiskData(s2 string, date ...time.Time) {
	year, month := s.getYearAndMonth()
	sqlAccess, err := s.sqlDatabase.NewAccess(false)
	if len(date) > 0 {
		month = date[0].Month()
		year = uint64(date[0].Year())
	}
	endStart := time.Date(int(year), month, 1, 0, 0, 0, 0, time.Local).AddDate(0, 1, 0)
	if err != nil {
		return
	}
	high := uint64(0)
	medium := uint64(0)
	low := uint64(0)

	defer sqlAccess.Close()
	dbEntity := &sqldb.Viewassessmentrecord{}
	dbOrder := &sqldb.ViewassessmentrecordOrder{}
	dbFilter := &sqldb.ViewassessmentrecordFilter{
		Orgcode:        &s2,
		Assessmentname: "%高血压%",
		Assessdatetime: &endStart,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	_ = sqlAccess.SelectDistinct(dbEntity, func() {
		if dbEntity.Summary == "高危" {
			high++
		} else if dbEntity.Summary == "中危" {
			medium++
		} else if dbEntity.Summary == "低危" {
			low++
		}

	}, dbOrder, sqlFilter)
	dbStaticEntity := &sqldb.Statisticrecord{
		Year:    year,
		Month:   uint64(month),
		Orgcode: s2,
		Count:   &high,
		Type:    Data_BloodPressure_High_Risk,
	}
	dbStaticEntity2 := &sqldb.Statisticrecord{
		Year:    year,
		Month:   uint64(month),
		Orgcode: s2,
		Count:   &medium,
		Type:    Data_BloodPressure_Medium_Risk,
	}
	dbStaticEntity3 := &sqldb.Statisticrecord{
		Year:    year,
		Month:   uint64(month),
		Orgcode: s2,
		Count:   &low,
		Type:    Data_BloodPressure_Low_Risk,
	}
	dbUpdateFilter := &sqldb.StatisticrecordFilter{
		Year:    year,
		Month:   uint64(month),
		Orgcode: s2,
		Type:    Data_BloodPressure_High_Risk,
	}
	dbUpdateFilter2 := &sqldb.StatisticrecordFilter{
		Year:    year,
		Month:   uint64(month),
		Orgcode: s2,
		Type:    Data_BloodPressure_Medium_Risk,
	}
	dbUpdateFilter3 := &sqldb.StatisticrecordFilter{
		Year:    year,
		Month:   uint64(month),
		Orgcode: s2,
		Type:    Data_BloodPressure_Low_Risk,
	}
	sqlUpdateFilter := s.sqlDatabase.NewFilter(dbUpdateFilter, false, false)
	sqlUpdateFilter2 := s.sqlDatabase.NewFilter(dbUpdateFilter2, false, false)
	sqlUpdateFilter3 := s.sqlDatabase.NewFilter(dbUpdateFilter3, false, false)
	dbStaticRecordEntity := &sqldb.Statisticrecord{}
	err = s.sqlDatabase.SelectOne(dbStaticRecordEntity, sqlUpdateFilter)
	exist := true
	if err != nil {
		if s.sqlDatabase.IsNoRows(err) {
			exist = false
		} else {
			return
		}
	}
	if exist {
		_, _ = sqlAccess.UpdateSelective(dbStaticEntity, sqlUpdateFilter)
		_, _ = sqlAccess.UpdateSelective(dbStaticEntity2, sqlUpdateFilter2)
		_, _ = sqlAccess.UpdateSelective(dbStaticEntity3, sqlUpdateFilter3)
	} else {
		_, _ = sqlAccess.InsertSelective(dbStaticEntity)
		_, _ = sqlAccess.InsertSelective(dbStaticEntity2)
		_, _ = sqlAccess.InsertSelective(dbStaticEntity3)
	}
}

func (s *StatisticalTask) saveBloodGlucoseRiskData(s2 string, date ...time.Time) {
	year, month := s.getYearAndMonth()
	sqlAccess, err := s.sqlDatabase.NewAccess(false)
	if len(date) > 0 {
		month = date[0].Month()
		year = uint64(date[0].Year())
	}
	endStart := time.Date(int(year), month, 1, 0, 0, 0, 0, time.Local).AddDate(0, 1, 0)
	if err != nil {
		return
	}
	high := uint64(0)
	medium := uint64(0)
	low := uint64(0)

	defer sqlAccess.Close()
	dbEntity := &sqldb.Viewassessmentrecord{}
	dbOrder := &sqldb.ViewassessmentrecordOrder{}
	dbFilter := &sqldb.ViewassessmentrecordFilter{
		Orgcode:        &s2,
		Assessmentname: "%糖尿病%",
		Assessdatetime: &endStart,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	_ = sqlAccess.SelectDistinct(dbEntity, func() {
		if dbEntity.Summary == "极高危" || dbEntity.Summary == "严格" {
			high++
		} else if dbEntity.Summary == "高危" || dbEntity.Summary == "一般" {
			medium++
		} else if dbEntity.Summary == "中危" || dbEntity.Summary == "宽松" {
			low++
		}

	}, dbOrder, sqlFilter)
	dbStaticEntity := &sqldb.Statisticrecord{
		Year:    year,
		Month:   uint64(month),
		Orgcode: s2,
		Count:   &high,
		Type:    Data_BloodGlcouse_High_Risk,
	}
	dbStaticEntity2 := &sqldb.Statisticrecord{
		Year:    year,
		Month:   uint64(month),
		Orgcode: s2,
		Count:   &medium,
		Type:    Data_BloodGlcouse_Medium_Risk,
	}
	dbStaticEntity3 := &sqldb.Statisticrecord{
		Year:    year,
		Month:   uint64(month),
		Orgcode: s2,
		Count:   &low,
		Type:    Data_BloodGlcouse_Low_Risk,
	}
	dbUpdateFilter := &sqldb.StatisticrecordFilter{
		Year:    year,
		Month:   uint64(month),
		Orgcode: s2,
		Type:    Data_BloodGlcouse_High_Risk,
	}
	dbUpdateFilter2 := &sqldb.StatisticrecordFilter{
		Year:    year,
		Month:   uint64(month),
		Orgcode: s2,
		Type:    Data_BloodGlcouse_Medium_Risk,
	}
	dbUpdateFilter3 := &sqldb.StatisticrecordFilter{
		Year:    year,
		Month:   uint64(month),
		Orgcode: s2,
		Type:    Data_BloodGlcouse_Low_Risk,
	}
	sqlUpdateFilter := s.sqlDatabase.NewFilter(dbUpdateFilter, false, false)
	sqlUpdateFilter2 := s.sqlDatabase.NewFilter(dbUpdateFilter2, false, false)
	sqlUpdateFilter3 := s.sqlDatabase.NewFilter(dbUpdateFilter3, false, false)
	dbStaticRecordEntity := &sqldb.Statisticrecord{}
	err = s.sqlDatabase.SelectOne(dbStaticRecordEntity, sqlUpdateFilter)
	exist := true
	if err != nil {
		if s.sqlDatabase.IsNoRows(err) {
			exist = false
		} else {
			return
		}
	}
	if exist {
		_, _ = sqlAccess.UpdateSelective(dbStaticEntity, sqlUpdateFilter)
		_, _ = sqlAccess.UpdateSelective(dbStaticEntity2, sqlUpdateFilter2)
		_, _ = sqlAccess.UpdateSelective(dbStaticEntity3, sqlUpdateFilter3)
	} else {
		_, _ = sqlAccess.InsertSelective(dbStaticEntity)
		_, _ = sqlAccess.InsertSelective(dbStaticEntity2)
		_, _ = sqlAccess.InsertSelective(dbStaticEntity3)
	}
}

func (s *StatisticalTask) getYearAndMonth() (uint64, time.Month) {
	year, month, day := time.Now().Date()
	thisMonth := time.Date(year, month, 1, 0, 0, 0, 0, time.Local)
	if day == 1 {
		start := thisMonth.AddDate(0, -1, 0)
		return uint64(start.Year()), start.Month()
	} else {
		return uint64(year), month
	}
}
