package admin

import (
	"tlcbme_project/database"
	"fmt"
	"strings"
)

type mysql struct {
}

func (s *mysql) getTables(sqlAccess database.SqlAccess, schema string, tableOnly bool) ([]*mysqlTable, error) {
	sb := &strings.Builder{}
	sb.WriteString("SELECT ")
	sb.WriteString("`information_schema`.`tables`.`table_name` as `table_name`, ")
	sb.WriteString("`information_schema`.`tables`.`table_type` as `table_type`, ")
	sb.WriteString("`information_schema`.`tables`.`table_comment` as `table_comment`, ")
	sb.WriteString("`information_schema`.`tables`.`update_time` as `update_time`, ")
	sb.WriteString("`information_schema`.`tables`.`table_rows` as `table_rows`, ")
	sb.WriteString("`performance_schema`.`table_io_waits_summary_by_table`.`COUNT_INSERT` as `rows_inserted`, ")
	sb.WriteString("`performance_schema`.`table_io_waits_summary_by_table`.`COUNT_UPDATE` as `rows_updated`, ")
	sb.WriteString("`performance_schema`.`table_io_waits_summary_by_table`.`COUNT_DELETE` as `rows_deleted` ")

	sb.WriteString("from `information_schema`.`tables` ")
	sb.WriteString("left join `performance_schema`.`table_io_waits_summary_by_table` on ")
	sb.WriteString("( ")
	sb.WriteString("`performance_schema`.`table_io_waits_summary_by_table`.`OBJECT_SCHEMA` = `information_schema`.`tables`.`table_schema` ")
	sb.WriteString("and ")
	sb.WriteString("`performance_schema`.`table_io_waits_summary_by_table`.`OBJECT_NAME` = `information_schema`.`tables`.`table_name` ")
	sb.WriteString(") ")

	sb.WriteString("where `information_schema`.`tables`.`table_schema`=? ")
	if tableOnly {
		sb.WriteString("and `table_type`='BASE TABLE' ")
	}

	rows, err := sqlAccess.Query(sb.String(), schema)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	results := make([]*mysqlTable, 0)
	for rows.Next() {
		result := &mysqlTable{}
		err = rows.Scan(&result.Name,
			&result.Type,
			&result.Comment,
			&result.UpdateTime,
			&result.Rows,
			&result.RowsInserted,
			&result.RowsUpdated,
			&result.RowsDeleted)
		if err != nil {
			return results, err
		}

		if result.UpdateTime != nil {
			result.Timestamp = uint64(result.UpdateTime.Unix())
		}

		results = append(results, result)
	}

	return results, nil
}

func (s *mysql) getViews(sqlAccess database.SqlAccess, schema string) ([]*mysqlView, error) {
	sb := &strings.Builder{}
	sb.WriteString("SELECT ")
	sb.WriteString("`table_schema`, ")
	sb.WriteString("`table_name`, ")
	sb.WriteString("`view_definition` ")

	sb.WriteString("from `information_schema`.`views` ")
	sb.WriteString("where `table_schema`=? ")

	rows, err := sqlAccess.Query(sb.String(), schema)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	tableSchema := ""
	results := make([]*mysqlView, 0)
	for rows.Next() {
		result := &mysqlView{}
		err = rows.Scan(&tableSchema,
			&result.Name,
			&result.Definition)
		if err != nil {
			return results, err
		}
		result.Definition = strings.Replace(result.Definition, fmt.Sprintf("`%s`.", tableSchema), "", -1)

		results = append(results, result)
	}

	return results, nil
}

func (s *mysql) getColumns(sqlAccess database.SqlAccess, schema, table string) ([]*mysqlColumn, error) {
	sb := &strings.Builder{}
	sb.WriteString("SELECT ")
	sb.WriteString("`column_name`, ")
	sb.WriteString("`column_type`, ")
	sb.WriteString("`column_comment`, ")
	sb.WriteString("`column_key`, ")
	sb.WriteString("`is_nullable`, ")
	sb.WriteString("`data_type`, ")
	sb.WriteString("`column_default`, ")
	sb.WriteString("`extra` ")

	sb.WriteString("from `information_schema`.`columns` ")
	sb.WriteString("where `table_schema`=? and `table_name`=? ")

	rows, err := sqlAccess.Query(sb.String(), schema, table)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	results := make([]*mysqlColumn, 0)
	for rows.Next() {
		result := &mysqlColumn{}
		err = rows.Scan(&result.Name,
			&result.Type,
			&result.Comment,
			&result.Key,
			&result.NullAble,
			&result.DataType,
			&result.DataDefault,
			&result.Extra)
		if err != nil {
			return results, err
		}

		results = append(results, result)
	}

	return results, nil
}
