package admin

import (
	"fmt"
)

type mysqlView struct {
	Name       string `json:"name" note:"名称"`
	Definition string `json:"definition" note:"定义"`
}

func (s *mysqlView) GetCreateSql() string {
	return fmt.Sprintf("CREATE OR REPLACE VIEW `%s` As %s", s.Name, s.Definition)
}
