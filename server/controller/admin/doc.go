package admin

import "github.com/ktpswjz/httpserver/document"

type doc struct {
}

func (s *doc) rootCatalog(a document.Assistant) document.Catalog {
	return a.CreateCatalog("管理平台接口", "管理平台相关接口")
}
