package admin

import (
	"sync"
	"time"
)

type HistoryConfig struct {
	StatisticalTask
}

func (s *HistoryConfig) Start() {
	s.startTask()
}

func (s *HistoryConfig) Stop() {
	//s.Cancel <- true
}

func (s *HistoryConfig) startTask() {
	month := time.Month(s.Config.HistoryConfig.Month)
	start := time.Date(int(s.Config.HistoryConfig.Year), month, 1, 0, 0, 0, 0, time.Local)
	for {
		if start.After(time.Now()) {
			break
		}
		orgs := s.getOrgCodes()
		wg := sync.WaitGroup{}
		for _, orgCode := range orgs {
			if *orgCode.IsValid == 1 {
				wg.Add(1)
				go func(orgCode string, date time.Time) {
					// 1.活跃人数，管理人数
					wg2 := sync.WaitGroup{}
					wg2.Add(1)
					go func() {
						s.savePatientInManageData(orgCode, date)
						wg2.Done()
					}()
					wg2.Add(1)
					// 2.高血压管理分级人数，按高血压管理分级来统计人数，柱状图，每根柱子代表不同管理等级的人数
					go func() {
						s.saveBloodPressureGradeData(orgCode, date)
						wg2.Done()
					}()
					wg2.Add(1)
					// 3.糖尿病管理分级人数，按糖尿病管理分级来统计人数，柱状图，每根柱子代表不同管理等级的人数
					go func() {
						s.saveBloodGlucoseGradeData(orgCode, date)
						wg2.Done()
					}()
					wg2.Add(1)
					// 4.血压数据=正常血压数据+异常血压数据
					go func() {
						s.saveBloodPressureData(orgCode, date)
						wg2.Done()
					}()
					wg2.Add(1)
					// 5.血压达标率=最近一次血压达标的人数/提交血压的总人数  最近一次血压数据未触发预警，即为达标，达标人数/总人数=达标率。
					go func() {
						s.saveBloodPressureCompliancerateData(orgCode, date)
						wg2.Done()
					}()
					wg2.Add(1)
					// 6.血糖数据=正常血糖数据+异常血糖数据
					go func() {
						s.saveBloodGlucoseData(orgCode, date)
						wg2.Done()
					}()
					wg2.Add(1)
					// 7.血糖达标率=最近一次血糖达标的人数/提交血糖的总人数  最近一次血糖数据未触发预警，即为达标，达标人数/总人数=达标率。
					go func() {
						s.saveBloodGlucoseCompliancerateData(orgCode, date)
						wg2.Done()
					}()
					wg2.Add(1)
					// 8.同血压数据为柱状图，只需按类型统计数量，例如饮食、运动、服药、体重等
					go func() {
						s.saveOtherData(orgCode, date)
						wg2.Done()
					}()
					wg2.Add(1)
					// 9.常规随访和预警随访放在一张图中，同正常血压数据和异常血压数据
					go func() {
						s.saveFollowupData(orgCode, date)
						wg2.Done()
					}()
					wg2.Add(1)
					// 10.预警情况 包括预警次数与预警当日解决率，预警当日解决率=预警当日解决/预警当日次数，月数据取当月平均值 。柱状图配合折线图，柱状图代表预警情况，折线图代表预警当日解决率
					go func() {
						s.saveAlertData(orgCode, date)
						wg2.Done()
					}()
					wg2.Add(1)
					// 11.高血压危险分层评估 高危xx人 中危xx人 低危xx人 按月计数
					go func() {
						s.saveBloodPressureRiskData(orgCode, date)
						wg2.Done()
					}()
					wg2.Add(1)
					// 12.糖尿病危险分层评估 高危xx人 中危xx人 低危xx人 按月计数
					go func() {
						s.saveBloodGlucoseRiskData(orgCode, date)
						wg2.Done()
					}()
					wg2.Wait()
					wg.Done()
				}(orgCode.OrgCode, start)
			}
		}
		wg.Wait()
		start = start.AddDate(0, 1, 0)

	}

}
