package doc

import (
	"tlcbme_project/data/model"
	"tlcbme_project/data/model/doc"
	"tlcbme_project/database/memory"
	"tlcbme_project/errors"
	"tlcbme_project/server/config"
	"tlcbme_project/server/controller"
	"fmt"
	"github.com/ktpswjz/httpserver/document"
	"github.com/ktpswjz/httpserver/router"
	"github.com/ktpswjz/httpserver/security/jwt"
	"github.com/ktpswjz/httpserver/types"
	"net/http"
	"strings"
	"time"
)

type Doc struct {
	controller.Controller
	apiType ApiType

	Document     document.Document
	AdminToken   memory.Token
	PatientToken memory.Token

	AdminAuthenticate   func(account, password string) (uint64, error)
	DefaultAuthenticate func(account, password string) (uint64, error)
	PateintAuthenticate func(account, password string) (uint64, error)
	AdminLogined        func(userID uint64, token *model.Token)
	DefaultLogined      func(userID uint64, token *model.Token)
	PatientLogined      func(userID uint64, token *model.Token)
}

func NewDoc(cfg *config.Config, log types.Log, dbToken memory.Token, doc document.Document, apiType ApiType) *Doc {
	instance := &Doc{Document: doc, apiType: apiType}
	instance.SetLog(log)
	instance.Config = cfg
	instance.DbToken = dbToken

	return instance
}

func (s *Doc) GetCatalogTree(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	filter := &doc.CatalogFilter{
		Keywords: "",
	}
	a.GetArgument(r, filter)

	a.Success(s.Document.GetCatalogTree(filter.Keywords))
}

func (s *Doc) GetFunction(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	id := p.ByName("id")
	fun := s.Document.GetFunction(id)
	if fun == nil {
		a.Error(errors.NotExist)
		return
	}
	fun.FullPath = fmt.Sprintf("%s://%s%s", a.Schema(), r.Host, fun.Path)

	a.Success(fun)
}

func (s *Doc) CreateToken(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	filter := &model.TokenFilter{
		Account:  "",
		Password: "",
		FunId:    "",
	}
	err := a.GetArgument(r, filter)
	if err != nil {
		a.Error(errors.InputError, err)
		return
	}
	account := strings.ToLower(strings.TrimSpace(filter.Account))
	if account == "" {
		a.Error(errors.InputInvalid, "账号为空")
		return
	}
	password := strings.TrimSpace(filter.Password)
	if password == "" {
		a.Error(errors.InputInvalid, "密码为空")
		return
	}
	funId := filter.FunId
	if funId == "" {
		a.Error(errors.InputInvalid, "接口标识为空")
		return
	}
	fun := s.Document.GetFunction(funId)
	if fun == nil {
		a.Error(errors.InputInvalid, "接口'", funId, "'不存在")
		return
	}

	authenticate := s.AdminAuthenticate
	logined := s.AdminLogined
	dbToken := s.AdminToken
	if s.apiType.IsAdminApi(fun.Path) {

	} else if s.apiType.IsDefaultApi(fun.Path) {
		authenticate = s.DefaultAuthenticate
		logined = s.DefaultLogined
		dbToken = s.DbToken
	} else if s.apiType.IsPatientApi(fun.Path) {
		authenticate = s.PateintAuthenticate
		logined = s.PatientLogined
		dbToken = s.PatientToken
	} else {
		a.Error(errors.InputInvalid, "未知的接口类型'", fun.Path, "'")
		return
	}

	if authenticate == nil {
		a.Error(errors.Exception, "not auth provider")
		return
	}
	id, err := authenticate(account, password)
	if err != nil {
		a.Error(errors.LoginAccountOrPasswordInvalid, err)
		return
	}

	now := time.Now()
	token := &model.Token{
		ID:          a.GenerateGuid(),
		UserAccount: filter.Account,
		UserID:      id,
		LoginIP:     a.RIP(),
		LoginTime:   now,
		ActiveTime:  now,
	}
	if logined != nil {
		logined(id, token)
	}
	err = dbToken.Set(token)
	if err != nil {
		a.Error(errors.Exception, err)
		return
	}

	login := &model.Login{
		Token:   token.ID,
		Account: token.UserAccount,
	}

	a.Success(login)
}

func (s *Doc) CreateJwt(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	now := time.Now()
	expiration := time.Second * time.Duration(s.Config.Management.Providers.Hbp.Auth.Expiration)
	payload := &model.JwtPayload{
		Issuer:         s.Config.Management.Providers.Hbp.ID,
		Subject:        s.Config.Management.Providers.Hbp.ID,
		Audience:       s.Config.Management.Providers.Hbp.Auth.ID,
		ExpirationTime: now.Add(expiration).Unix(),
		NotBefore:      now.Add(-expiration).Unix(),
		IssuedAt:       now.Unix(),
		ID:             a.GenerateGuid(),
	}

	jwtToken, err := jwt.Encode(s.Config.Management.Providers.Hbp.Auth.Secret, s.Config.Management.Providers.Hbp.Auth.Algorithm, payload)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}

	a.Success(jwtToken)
}
