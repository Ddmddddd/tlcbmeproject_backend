package doc

type ApiType interface {
	IsDefaultApi(path string) bool
	IsAdminApi(path string) bool
	IsPatientApi(path string) bool
}
