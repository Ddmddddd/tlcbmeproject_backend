package auth

import (
	"tlcbme_project/business/doctor"
	"tlcbme_project/data/model"
	"tlcbme_project/database/memory"
	"tlcbme_project/server/config"
	"github.com/ktpswjz/httpserver/document"
	"github.com/ktpswjz/httpserver/router"
	"github.com/ktpswjz/httpserver/types"
	"net/http"
)

type Doctor struct {
	Auth

	doctorBusiness doctor.Business
}

func NewDoctor(cfg *config.Config, log types.Log, dbToken memory.Token, doctorBusiness doctor.Business) *Doctor {
	instance := &Doctor{doctorBusiness: doctorBusiness}
	instance.SetLog(log)
	instance.Config = cfg
	instance.DbToken = dbToken
	instance.errorCount = make(map[string]int, 0)
	instance.passwordRequired = true

	return instance
}

func (s *Doctor) GetInfo(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	data := &model.Info{
		Name:        s.Config.Name,
		BackVersion: s.Config.GetArgs().ModuleVersion().ToString(),
	}
	data.FrontVersion, _ = s.GetSiteVersion(s.Config.Site.Doctor.Root)

	a.Success(data)
}

func (s *Doctor) GetInfoDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("获取平台信息")
	function.SetNote("获取系统名称、版本号等信息")
	function.SetOutputExample(&model.Info{
		Name:         "服务器",
		BackVersion:  "1.0.1.0",
		FrontVersion: "1.0.1.8",
	})
	function.IgnoreToken(true)
	function.SetContentType("")

	catalog := a.CreateCatalog("医生平台接口", "医生端台相关接口")
	catalog = catalog.CreateChild("系统信息", "系统信息相关接口")
	catalog.SetFunction(function)

	return function
}

func (s *Doctor) GetRsaPublicKeyDoc(a document.Assistant) document.Function {
	return s.getRsaPublicKeyDoc(a, "医生平台接口")
}

func (s *Doctor) GetCaptchaDoc(a document.Assistant) document.Function {
	return s.getCaptchaDoc(a, "医生平台接口")
}

func (s *Doctor) LoginDoc(a document.Assistant) document.Function {
	return s.loginDoc(a, "医生平台接口")
}

func (s *Doctor) IsShowAppDownloadCode(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	a.Success(s.Config.Site.Doctor.ShowAppDownloadCodeInLoginPage)
}

func (s *Doctor) IsShowAppDownloadCodeDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("是否在登录页面显示app下载二维码")
	function.SetNote("是否在登录页面显示app下载二维码")
	function.SetOutputExample(bool(true))
	function.SetContentType("")
	function.IgnoreToken(true)

	catalog := a.CreateCatalog("医生平台接口", "管理平台相关接口")
	catalog = catalog.CreateChild("权限管理", "系统授权相关接口")
	catalog.SetFunction(function)

	return function
}
