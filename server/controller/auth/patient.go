package auth

import (
	"tlcbme_project/data/model"
	"tlcbme_project/database/memory"
	"tlcbme_project/server/config"
	"github.com/ktpswjz/httpserver/document"
	"github.com/ktpswjz/httpserver/router"
	"github.com/ktpswjz/httpserver/types"
	"net/http"
)

type Patient struct {
	Auth
}

func NewPatient(cfg *config.Config, log types.Log, dbToken memory.Token) *Patient {
	instance := &Patient{}
	instance.SetLog(log)
	instance.Config = cfg
	instance.DbToken = dbToken
	instance.errorCount = make(map[string]int, 0)
	instance.passwordRequired = false

	return instance
}

func (s *Patient) GetInfo(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	data := &model.Info{
		Name:        s.Config.Name,
		BackVersion: s.Config.GetArgs().ModuleVersion().ToString(),
	}
	data.FrontVersion, _ = s.GetSiteVersion(s.Config.Site.Patient.Root)

	a.Success(data)
}

func (s *Patient) GetInfoDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("获取平台信息")
	function.SetNote("获取系统名称、版本号等信息")
	function.SetOutputExample(&model.Info{
		Name:         "服务器",
		BackVersion:  "1.0.1.0",
		FrontVersion: "1.0.1.8",
	})
	function.IgnoreToken(true)
	function.SetContentType("")

	catalog := a.CreateCatalog("患者平台接口", "患者移动平台相关接口")
	catalog = catalog.CreateChild("系统信息", "系统信息相关接口")
	catalog.SetFunction(function)

	return function
}

func (s *Patient) GetRsaPublicKeyDoc(a document.Assistant) document.Function {
	return s.getRsaPublicKeyDoc(a, "患者平台接口")
}

func (s *Patient) GetCaptchaDoc(a document.Assistant) document.Function {
	return s.getCaptchaDoc(a, "患者平台接口")
}

func (s *Patient) LoginDoc(a document.Assistant) document.Function {
	return s.loginDoc(a, "患者平台接口")
}

func (s *Patient) ValidateUserNameDoc(a document.Assistant) document.Function {
	return s.validateUserNameDoc(a, "患者平台接口")
}

func (s *Patient) RegisterUserDoc(a document.Assistant) document.Function {
	return s.registerUserDoc(a, "患者平台接口")
}

func (s *Patient) GetWeChatOpenIDByCodeDoc(a document.Assistant) document.Function {
	return s.getWeChatOpenIDByCodeDoc(a, "患者平台接口")
}

func (s *Patient) LoginByWeChatDoc(a document.Assistant) document.Function {
	return s.loginByWeChatDoc(a, "患者平台接口")
}