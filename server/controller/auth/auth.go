package auth

import (
	"github.com/ktpswjz/httpserver/document"
	"github.com/ktpswjz/httpserver/router"
	"github.com/ktpswjz/httpserver/types"
	"github.com/mojocn/base64Captcha"
	"net/http"
	"strings"
	"time"
	"tlcbme_project/business"
	"tlcbme_project/data/entity/sqldb"
	"tlcbme_project/data/model"
	"tlcbme_project/data/model/doctor"
	"tlcbme_project/errors"
	"tlcbme_project/server/controller"
)

type Auth struct {
	controller.Controller

	errorCount       map[string]int
	passwordRequired bool

	Authenticate               func(account, password string) (uint64, error)
	Logined                    func(userID uint64, token *model.Token)
	ValidateUser               func(userName string) (uint64, error)
	CreateUser                 func(userInfoCreate *doctor.PatientUserInfoCreateForApp) (uint64, error)
	RegisterPatient            func(patientID uint64) error
	GetWeChatOpenID            func(filter *model.WeChatCodeFilter) (*doctor.WeChatCode2SessionResult, error)
	GetPatientByOpenID         func(filter *model.WeChatLoginFilter) (*sqldb.PatientUserAuths, error)
	AuditPatient               func(argument *doctor.PatientUserAuditEx) business.Error
	CreateDietPlan             func(patientID uint64) business.Error
	SendCodeWithPhone          func(phone *model.SendCodePhone) (string, error)
	NewChangePasswordWithPhone func(argument *model.NewChangePasswordWithPhone) (string, error)
}

func (s *Auth) GetRsaPublicKey(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	randKey := a.RandKey()
	if randKey == nil {
		a.Error(errors.InternalError)
		return
	}
	publicKey, err := randKey.PublicKey()
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}
	keyVal, err := publicKey.SaveToMemory()
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}

	a.Success(string(keyVal))
}

func (s *Auth) getRsaPublicKeyDoc(a document.Assistant, root string) document.Function {
	function := a.CreateFunction("获取RSA公钥")
	function.SetNote("获取服务临时RSA公钥，用于对密码等敏感信息进行加密，公钥在服务重启后失效")
	function.SetOutputExample("-----BEGIN PUBLIC KEY-----...-----END PUBLIC KEY-----")
	function.IgnoreToken(true)

	catalog := a.CreateCatalog(root, "管理平台相关接口")
	catalog = catalog.CreateChild("权限管理", "系统授权相关接口")
	catalog.SetFunction(function)

	return function
}

func (s *Auth) GetCaptcha(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	filter := &model.CaptchaFilter{
		Mode:   base64Captcha.CaptchaModeNumberAlphabet,
		Length: 4,
		Width:  100,
		Height: 30,
	}
	err := a.GetArgument(r, filter)
	if err != nil {
		a.Error(errors.InputError, err)
		return
	}

	captchaConfig := base64Captcha.ConfigCharacter{
		Mode:               filter.Mode,
		Height:             filter.Height,
		Width:              filter.Width,
		CaptchaLen:         filter.Length,
		ComplexOfNoiseText: base64Captcha.CaptchaComplexLower,
		ComplexOfNoiseDot:  base64Captcha.CaptchaComplexLower,
		IsShowHollowLine:   false,
		IsShowNoiseDot:     false,
		IsShowNoiseText:    false,
		IsShowSlimeLine:    false,
		IsShowSineLine:     false,
		IsUseSimpleFont:    true,
	}
	captchaId, captchaValue := base64Captcha.GenerateCaptcha("", captchaConfig)

	data := &model.Captcha{
		ID:       captchaId,
		Value:    base64Captcha.CaptchaWriteToBase64Encoding(captchaValue),
		Required: s.captchaRequired(a.RIP()),
	}
	randKey := a.RandKey()
	if randKey != nil {
		publicKey, err := randKey.PublicKey()
		if err == nil {
			keyVal, err := publicKey.SaveToMemory()
			if err == nil {
				data.RsaKey = string(keyVal)
			}
		}
	}

	a.Success(data)
}

func (s *Auth) getCaptchaDoc(a document.Assistant, root string) document.Function {
	function := a.CreateFunction("获取验证码")
	function.SetNote("获取用户登陆需要的验证码信息")
	function.SetInputExample(&model.CaptchaFilter{
		Mode:   base64Captcha.CaptchaModeNumberAlphabet,
		Length: 4,
		Width:  100,
		Height: 30,
	})
	function.SetOutputExample(&model.Captcha{
		ID:       "GKSVhVMRAHsyVuXSrMYs",
		Value:    "data:image/png;base64,iVBOR...",
		RsaKey:   "-----BEGIN PUBLIC KEY-----...-----END PUBLIC KEY-----",
		Required: false,
	})
	function.IgnoreToken(true)

	catalog := a.CreateCatalog(root, "管理平台相关接口")
	catalog = catalog.CreateChild("权限管理", "系统授权相关接口")
	catalog.SetFunction(function)

	return function
}

func (s *Auth) Login(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	filter := &model.LoginFilter{}
	err := a.GetArgument(r, filter)
	if err != nil {
		a.Error(errors.InputError, err)
		return
	}

	requireCaptcha := s.captchaRequired(a.RIP())
	err = filter.Check(requireCaptcha, s.passwordRequired)
	if err != nil {
		a.Error(errors.InputInvalid, err)
		return
	}

	if requireCaptcha {
		if !base64Captcha.VerifyCaptcha(filter.CaptchaId, filter.CaptchaValue) {
			a.Error(errors.LoginCaptchaInvalid)
			return
		}
	}

	if s.Authenticate == nil {
		a.Error(errors.Exception, "not auth provider")
		return
	}

	pwd := filter.Password
	if strings.ToLower(filter.Encryption) == "rsa" {
		decryptedPwd, err := a.RandKey().DecryptData(filter.Password)
		if err != nil {
			a.Error(errors.LoginPasswordInvalid, err)
			s.increaseErrorCount(a.RIP())
			return
		}
		pwd = string(decryptedPwd)
	}

	id, err := s.Authenticate(filter.Account, pwd)
	if err != nil {
		a.Error(errors.LoginAccountOrPasswordInvalid, err)
		s.increaseErrorCount(a.RIP())
		return
	}

	now := time.Now()
	token := &model.Token{
		ID:          a.GenerateGuid(),
		UserAccount: filter.Account,
		UserID:      id,
		LoginIP:     a.RIP(),
		LoginTime:   now,
		ActiveTime:  now,
	}
	if s.Logined != nil {
		s.Logined(id, token)
	}
	err = s.DbToken.Set(token)
	if err != nil {
		a.Error(errors.Exception, err)
		return
	}

	login := &model.Login{
		Token:       token.ID,
		Account:     token.UserAccount,
		RightIDList: token.RightIDList,
		Host:        r.Host,
	}

	a.Success(login)
	s.clearErrorCount(a.RIP())
}

func (s *Auth) loginDoc(a document.Assistant, root string) document.Function {
	function := a.CreateFunction("用户登录")
	function.SetNote("通过用户账号及密码进行登录获取凭证")
	function.SetInputExample(&model.LoginFilter{
		Account:      "admin",
		Password:     "1",
		CaptchaId:    "r4kcmz2E12e0qJQOvqRB",
		CaptchaValue: "1e35",
		Encryption:   "",
	})
	function.SetOutputExample(&model.Login{
		Token: "71b9b7e2ac6d4166b18f414942ff3481",
	})
	function.IgnoreToken(true)

	catalog := a.CreateCatalog(root, "管理平台相关接口")
	catalog = catalog.CreateChild("权限管理", "系统授权相关接口")
	catalog.SetFunction(function)

	return function
}

func (s *Auth) captchaRequired(ip string) bool {
	if s.errorCount == nil {
		return false
	}

	count, ok := s.errorCount[ip]
	if ok {
		if count < 3 {
			return false
		} else {
			return true
		}
	}

	return false
}

func (s *Auth) increaseErrorCount(ip string) {
	if s.errorCount == nil {
		return
	}

	count := 1
	v, ok := s.errorCount[ip]
	if ok {
		count += v
	}

	s.errorCount[ip] = count
}

func (s *Auth) clearErrorCount(ip string) {
	if s.errorCount == nil {
		return
	}

	_, ok := s.errorCount[ip]
	if ok {
		delete(s.errorCount, ip)
	}
}

func (s *Auth) ValidateUserName(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &model.UserNameValidate{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InputError, err)
		return
	}

	if s.ValidateUser == nil {
		a.Error(errors.Exception, "not auth provider")
		return
	}

	data, err := s.ValidateUser(argument.UserName)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}

	a.Success(data)
}

func (s *Auth) validateUserNameDoc(a document.Assistant, root string) document.Function {
	function := a.CreateFunction("用户名验证")
	function.SetNote("验证该用户是否存在")
	function.SetInputExample(&model.UserNameValidate{
		UserName: "sb001",
	})
	function.SetOutputExample(0)
	function.IgnoreToken(true)

	catalog := a.CreateCatalog(root, "管理平台相关接口")
	catalog = catalog.CreateChild("权限管理", "系统授权相关接口")
	catalog.SetFunction(function)

	return function
}

func (s *Auth) RegisterUser(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctor.PatientUserInfoCreateForApp{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InputError, err)
		return
	}

	data, err := s.CreateUser(argument)
	if err != nil {
		a.Error(errors.RegisterFailed, err)
		return
	}
	//绕过审核，直接写死，只用于特殊情况（体重项目）
	newArgument := &doctor.PatientUserAuditEx{}
	newArgument.ReviewerOrgCode = argument.HospitalCode
	newArgument.ReviewerID = argument.HealthManagerID
	newArgument.Status = 1
	newArgument.ReviewerName = "TLC注册营养师团队"
	newArgument.ReviewDateTime = types.Time(time.Now())
	newArgument.DoctorName = "TLC注册营养师团队"
	doctorID := uint64(64)
	newArgument.DoctorID = &doctorID
	newArgument.HealthManagerID = &argument.HealthManagerID
	newArgument.HealthManagerName = argument.HealthManagerName
	manageClass := &doctor.ManageClass{
		ItemCode: 1,
		ItemName: "高血压",
	}
	manageClasses := make([]*doctor.ManageClass, 0)
	manageClasses = append(manageClasses, manageClass)
	newArgument.ManageClasses = manageClasses
	newArgument.PatientID = data
	sex := uint64(0)
	switch argument.Sex {
	case "男":
		sex = 1
	case "女":
		sex = 2
	}
	//newArgument.IdentityCardNumber = argument.IdentityCardNumber
	newArgument.Name = argument.Name
	newArgument.Sex = &sex
	newArgument.DateOfBirth = argument.DateOfBirth
	newArgument.Phone = argument.PhoneNumber
	newArgument.EducationLevel = argument.EducationLevel
	newArgument.JobType = argument.JobType
	newArgument.Height = &argument.Height
	newArgument.Weight = &argument.Weight
	newArgument.Diagnosis = "高血压"
	go func(newArgument *doctor.PatientUserAuditEx) {
		s.AuditPatient(newArgument)
	}(newArgument)
	//go func(userId uint64) {
	//	s.RegisterPatient(userId)
	//}(data)
	//go func(patientID uint64) {
	//	s.CreateDietPlan(patientID)
	//}(data)

	a.Success(data)
}

func (s *Auth) registerUserDoc(a document.Assistant, root string) document.Function {
	now := types.Time(time.Now())
	function := a.CreateFunction("注册用户")
	function.SetNote("手机端注册新用户")
	function.SetInputExample(&doctor.PatientUserInfoCreateForApp{
		UserName:    "T0001283p",
		Name:        "测试员",
		Sex:         "男",
		DateOfBirth: &now,
		//IdentityCardNumber: "330106199909099876",
		PhoneNumber:     "12345678901",
		EducationLevel:  "本科",
		JobType:         "科技",
		Height:          170,
		Weight:          60.5,
		Nickname:        "小测",
		HospitalCode:    "",
		HealthManagerID: 12,
		Password:        "124",
		SourceFrom:      "APP",
	})
	function.SetOutputExample(117)
	function.IgnoreToken(true)

	catalog := a.CreateCatalog(root, "管理平台相关接口")
	catalog = catalog.CreateChild("权限管理", "系统授权相关接口")
	catalog.SetFunction(function)

	return function
}

func (s *Auth) GetWeChatOpenIDByCode(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	filter := &model.WeChatCodeFilter{}
	err := a.GetArgument(r, filter)
	if err != nil {
		a.Error(errors.InputError, err)
		return
	}

	openID, err := s.GetWeChatOpenID(filter)
	if err != nil {
		a.Error(errors.InputError, err)
		return
	}

	a.Success(openID)
}

func (s *Auth) getWeChatOpenIDByCodeDoc(a document.Assistant, root string) document.Function {
	function := a.CreateFunction("用户获取微信open-id")
	function.SetNote("通过用户启动小程序时得到的code来获取用户的open-id")
	function.SetInputExample(&model.WeChatCodeFilter{
		Code: "微信code",
	})
	function.SetOutputExample(0)
	function.IgnoreToken(true)

	catalog := a.CreateCatalog(root, "管理平台相关接口")
	catalog = catalog.CreateChild("权限管理", "系统授权相关接口")
	catalog.SetFunction(function)

	return function
}

func (s *Auth) LoginByWeChat(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &model.WeChatLoginFilter{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InputError, err)
		return
	}

	patientUser, err := s.GetPatientByOpenID(argument)
	if err != nil {
		a.Error(errors.InputInvalid, err)
		return
	}

	now := time.Now()
	token := &model.Token{
		ID:          a.GenerateGuid(),
		UserAccount: patientUser.UserName,
		UserID:      patientUser.UserID,
		LoginIP:     a.RIP(),
		LoginTime:   now,
		ActiveTime:  now,
	}
	if s.Logined != nil {
		s.Logined(patientUser.UserID, token)
	}
	err = s.DbToken.Set(token)
	if err != nil {
		a.Error(errors.Exception, err)
		return
	}

	login := &model.Login{
		Token:       token.ID,
		Account:     token.UserAccount,
		RightIDList: token.RightIDList,
		Host:        r.Host,
	}
	a.Success(login)
}

func (s *Auth) loginByWeChatDoc(a document.Assistant, root string) document.Function {
	function := a.CreateFunction("用户微信登录")
	function.SetNote("通过用户的微信账号进行登录获取凭证")
	function.SetInputExample(&model.WeChatLoginFilter{
		Identitier: "微信open-id",
		Status:     "用户状态,0-正常,1-停用",
	})
	function.SetOutputExample(&model.Login{
		Token: "71b9b7e2ac6d4166b18f414942ff3481",
	})
	function.IgnoreToken(true)

	catalog := a.CreateCatalog(root, "管理平台相关接口")
	catalog = catalog.CreateChild("权限管理", "系统授权相关接口")
	catalog.SetFunction(function)

	return function
}

func (s *Auth) SendCode(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &model.SendCodePhone{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InputError, err)
		return
	}

	data, err := s.SendCodeWithPhone(argument)
	if err != nil {
		a.Error(errors.InputError, err)
		return
	}

	a.Success(data)
}

func (s *Auth) NewChangePassword(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &model.NewChangePasswordWithPhone{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InputError, err)
		return
	}

	data2, err := s.NewChangePasswordWithPhone(argument)
	if err != nil {
		a.Error(errors.InputError, err)
		return
	}

	a.Success(data2)
}
