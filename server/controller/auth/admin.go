package auth

import (
	"tlcbme_project/data/model"
	"tlcbme_project/database/memory"
	"tlcbme_project/server/config"
	"github.com/ktpswjz/httpserver/document"
	"github.com/ktpswjz/httpserver/router"
	"github.com/ktpswjz/httpserver/types"
	"net/http"
)

type Admin struct {
	Auth
}

func NewAdmin(cfg *config.Config, log types.Log, dbToken memory.Token) *Admin {
	instance := &Admin{}
	instance.SetLog(log)
	instance.Config = cfg
	instance.DbToken = dbToken
	instance.errorCount = make(map[string]int, 0)
	instance.passwordRequired = true

	return instance
}

func (s *Admin) GetInfo(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	data := &model.Info{
		Name:        s.Config.Name,
		BackVersion: s.Config.GetArgs().ModuleVersion().ToString(),
	}
	data.FrontVersion, _ = s.GetSiteVersion(s.Config.Site.Admin.Root)

	a.Success(data)
}

func (s *Admin) GetInfoDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("获取平台信息")
	function.SetNote("获取系统名称、版本号等信息")
	function.SetOutputExample(&model.Info{
		Name:         "服务器",
		BackVersion:  "1.0.1.0",
		FrontVersion: "1.0.1.8",
	})
	function.IgnoreToken(true)
	function.SetContentType("")

	catalog := a.CreateCatalog("管理平台接口", "管理平台相关接口")
	catalog = catalog.CreateChild("系统信息", "系统信息相关接口")
	catalog.SetFunction(function)

	return function
}

func (s *Admin) GetRsaPublicKeyDoc(a document.Assistant) document.Function {
	return s.getRsaPublicKeyDoc(a, "管理平台接口")
}

func (s *Admin) GetCaptchaDoc(a document.Assistant) document.Function {
	return s.getCaptchaDoc(a, "管理平台接口")
}

func (s *Admin) LoginDoc(a document.Assistant) document.Function {
	return s.loginDoc(a, "管理平台接口")
}
