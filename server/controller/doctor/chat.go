package doctor

import (
	"tlcbme_project/business/doctor"
	doctorModel "tlcbme_project/data/model/doctor"
	"tlcbme_project/database/memory"
	"tlcbme_project/errors"
	"tlcbme_project/server/config"
	"github.com/ktpswjz/httpserver/document"
	"github.com/ktpswjz/httpserver/router"
	"github.com/ktpswjz/httpserver/types"
	"net/http"
	"time"
)

type Chat struct {
	doc
}

func NewChat(cfg *config.Config, log types.Log, dbToken memory.Token, doctorBusiness doctor.Business) *Chat {
	instance := &Chat{}
	instance.SetLog(log)
	instance.Config = cfg
	instance.DbToken = dbToken
	instance.doctorBusiness = doctorBusiness

	return instance
}

func (s *Chat) setDocFun(a document.Assistant, fun document.Function) {
	catalog := s.rootCatalog(a).CreateChild("医患交流信息", "医患交流信息相关接口")
	catalog.SetFunction(fun)
}

func (s *Chat) SendChatMessageFromDoctor(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.DoctorPatientChatMsg{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}
	token, err := s.DbToken.Get(a.Token())
	if err != nil {
		a.Error(errors.InternalError, err)
	}
	argument.SenderID = token.UserID
	now := types.Time(time.Now())
	if argument.MsgDateTime == nil {
		argument.MsgDateTime = &now
	}
	data, be := s.doctorBusiness.Chat().SendChatMessageFromDoctor(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}
func (s *Chat) SendChatMessageFromDoctorDoc(a document.Assistant) document.Function {
	now := types.Time(time.Now())

	function := a.CreateFunction("发送医患沟通聊天信息")
	function.SetNote("发送医患沟通的聊天信息,成功时返回信息序列号")
	function.SetInputExample(&doctorModel.DoctorPatientChatMsgData{
		ReceiverID:  1342,
		MsgContent:  "你好",
		MsgDateTime: &now,
	})
	function.SetOutputExample(11)

	s.setDocFun(a, function)

	return function
}

func (s *Chat) SendChatMessageReadFromDoctor(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.DoctorPatientChatMsgRead{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}
	token, err := s.DbToken.Get(a.Token())
	if err != nil {
		a.Error(errors.InternalError, err)
	}
	argument.SenderID = token.UserID

	be := s.doctorBusiness.Chat().SendChatMessageReadFromDoctor(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(nil)
}
func (s *Chat) SendChatMessageReadFromDoctorDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("发送医患沟通信息已阅读")
	function.SetNote("发送医患沟通的聊天信息已被阅读的消息")
	function.SetInputExample(&doctorModel.DoctorPatientChatMsgReadData{
		ReceiverID: 1342,
		MsgSerialNos: []uint64{
			11,
			12,
		},
	})
	function.SetOutputExample(11)

	s.setDocFun(a, function)

	return function
}

func (s *Chat) GetChatMessageList(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.ChatMsgUserInfo{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}
	token, err := s.DbToken.Get(a.Token())
	if err != nil {
		a.Error(errors.InternalError, err)
	}
	argument.DoctorID = token.UserID

	data, be := s.doctorBusiness.Chat().GetChatMessageList(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Chat) GetChatMessageListDoc(a document.Assistant) document.Function {
	now := types.Time(time.Now())

	function := a.CreateFunction("获取医患沟通的信息")
	function.SetNote("获取患者和选择医生的聊天记录")
	function.SetInputExample(&doctorModel.ChatMsgDoctorID{
		DoctorID: 13,
	})
	function.SetOutputExample([]*doctorModel.DoctorPatientChatMsg{
		{
			SerialNo:    1,
			SenderID:    11,
			ReceiverID:  12,
			MsgContent:  "你好",
			MsgDateTime: &now,
			MsgFlag:     1,
		},
		{
			SerialNo:    1,
			SenderID:    12,
			ReceiverID:  11,
			MsgContent:  "你好，too",
			MsgDateTime: &now,
			MsgFlag:     0,
		},
	})

	s.setDocFun(a, function)

	return function
}

func (s *Chat) GetChatPatientIDListDoc(a document.Assistant) document.Function {

	function := a.CreateFunction("获取医患沟通的患者id列表")
	function.SetNote("获取医患沟通的患者id列表")
	function.SetInputExample(nil)
	function.SetOutputExample([]*doctorModel.DoctorPatientChatID{
		{
			ReceiverID: 21,
			SenderID:   11215156,
		},
		{
			ReceiverID: 11215156,
			SenderID:   21,
		},
		{
			ReceiverID: 11215157,
			SenderID:   21,
		},
	})

	s.setDocFun(a, function)

	return function
}

func (s *Chat) GetChatPatientLastMsgList(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.DoctorInfoBase{}
	token, err := s.DbToken.Get(a.Token())
	if err != nil {
		a.Error(errors.InternalError, err)
	}
	argument.DoctorID = token.UserID

	data, be := s.doctorBusiness.Chat().GetChatPatientLatsMsgList(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Chat) GetChatPatientLastMsgListDoc(a document.Assistant) document.Function {
	now := types.Time(time.Now())
	sex := uint64(1)
	function := a.CreateFunction("获取医患沟通的最新消息列表")
	function.SetNote("获取医患沟通的最新消息列表")
	function.SetInputExample(nil)
	function.SetOutputExample([]*doctorModel.DoctorPatienLastChatMsg{
		{
			SerialNo:    21,
			PatientName: "张三",
			PatientID:   110254,
			MsgDateTime: &now,
			MsgContent:  "你好",
			Sex:         &sex,
			Count:       0,
		},
		{
			SerialNo:    21,
			PatientName: "张三",
			PatientID:   110254,
			MsgDateTime: &now,
			MsgContent:  "你好",
			Sex:         &sex,
			Count:       0,
		},
	})

	s.setDocFun(a, function)

	return function
}
