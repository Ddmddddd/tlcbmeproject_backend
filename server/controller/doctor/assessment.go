package doctor

import (
	"tlcbme_project/business/doctor"
	"tlcbme_project/business/patient"
	"tlcbme_project/data/model"
	doctorModel "tlcbme_project/data/model/doctor"
	"tlcbme_project/database/memory"
	"tlcbme_project/errors"
	"tlcbme_project/server/config"
	"github.com/ktpswjz/httpserver/document"
	"github.com/ktpswjz/httpserver/router"
	"github.com/ktpswjz/httpserver/types"
	"net/http"
	"time"
)

type Assessment struct {
	doc
}

func NewAssessment(cfg *config.Config, log types.Log, dbToken memory.Token, doctorBusiness doctor.Business, patientBusiness patient.Business) *Assessment {
	instance := &Assessment{}
	instance.SetLog(log)
	instance.Config = cfg
	instance.DbToken = dbToken
	instance.doctorBusiness = doctorBusiness
	instance.patientBusiness = patientBusiness

	return instance
}

func (s *Assessment) setDocFun(a document.Assistant, fun document.Function) {
	catalog := s.rootCatalog(a).CreateChild("危险评估", "危险记录评估相关接口")
	catalog.SetFunction(fun)
}

func (s *Assessment) CreateAssessmentRecord(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.AssessmentRecordCreateEx{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InputError, err)
		return
	}
	token, err := s.DbToken.Get(a.Token())
	if err != nil {
		a.Error(errors.InternalError, err)
	}
	argument.OperatorID = token.UserID
	now := types.Time(time.Now())
	data, weightSno, be := s.doctorBusiness.Assessment().CreateAssessmentRecord(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	result := &doctorModel.AssessmentRecordCreateResult{
		SerialNo:       data,
		PatientID:      argument.PatientID,
		AssessDateTime: &now,
		Summary:        argument.Summary,
		Others:         argument.Others,
	}
	go func() {
		s.patientBusiness.Data().UploadDataToManage(weightSno)
	}()

	a.Success(result)
}

func (s *Assessment) CreateAssessmentRecordDoc(a document.Assistant) document.Function {
	now := types.Time(time.Now())

	function := a.CreateFunction("新增患者评估记录")
	function.SetNote("新增患者评估记录，成功时返回记录序列号")
	function.SetInputExample(&doctorModel.AssessmentRecordCreate{
		PatientID:              100,
		AssessmentName:         "高血压危险分层评估",
		AssessmentSheetCode:    1,
		AssessmentSheetVersion: 1,
		Content: &doctorModel.AssessmentSheet{
			SheetCode:    1,
			SheetVersion: 1,
		},
	})
	function.SetOutputExample(&doctorModel.AssessmentRecordCreateResult{
		SerialNo:       42,
		PatientID:      100,
		AssessDateTime: &now,
		Summary:        "高危",
	})

	s.setDocFun(a, function)

	return function
}

func (s *Assessment) SearchAssessmentRecordPage(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	filter := &doctorModel.AssessmentRecordFilter{}
	argument := &model.PageFilter{
		Page: model.Page{
			Index: 1,
			Size:  15,
		},
		Filter: filter,
	}
	a.GetArgument(r, argument)

	result, be := s.doctorBusiness.Assessment().SearchAssessmentRecordPage(argument.Index, argument.Size, filter)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(result)
}

func (s *Assessment) SearchAssessmentRecordPageDoc(a document.Assistant) document.Function {
	now := types.Time(time.Now())
	patientID := uint64(1)

	function := a.CreateFunction("获取患者评估记录信息（分页）")
	function.SetNote("获取患者评估记录信息")
	function.SetInputExample(&model.PageFilter{
		Page: model.Page{
			Index: 1,
			Size:  15,
		},
		Filter: &doctorModel.AssessmentRecordFilter{
			PatientID: &patientID,
		},
	})
	function.SetOutputExample(&model.PageResult{
		Page: model.Page{
			Index: 1,
			Size:  15,
		},
		Total: 100,
		Count: 7,
		Data: []*doctorModel.AssessmentRecordListItem{
			{
				PatientID:              patientID,
				AssessDateTime:         &now,
				AssessmentName:         "高血压危险分层评估",
				AssessmentSheetCode:    1,
				AssessmentSheetVersion: 1,
				Summary:                "中危",
			},
		},
	})

	s.setDocFun(a, function)

	return function
}

func (s *Assessment) SearchAssessmentRecordOne(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.AssessmentRecordFilter{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InputError, err)
		return
	}

	result, be := s.doctorBusiness.Assessment().SearchAssessmentRecordOne(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(result)
}

func (s *Assessment) SearchAssessmentRecordOneDoc(a document.Assistant) document.Function {
	now := types.Time(time.Now())
	patientID := uint64(1)

	function := a.CreateFunction("获取评估记录详细信息")
	function.SetNote("获取评估记录详细信息")
	function.SetInputExample(&doctorModel.AssessmentRecordFilter{
		PatientID: &patientID,
	})
	function.SetOutputExample(&doctorModel.AssessmentRecord{
		PatientID:              patientID,
		AssessDateTime:         &now,
		AssessmentName:         "高血压危险分层评估",
		AssessmentSheetCode:    1,
		AssessmentSheetVersion: 1,
		Summary:                "中危",
		Content: &doctorModel.AssessmentSheet{
			SheetCode:    1,
			SheetVersion: 1,
		},
	})
	s.setDocFun(a, function)

	return function
}
