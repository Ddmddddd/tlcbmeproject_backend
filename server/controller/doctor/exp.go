package doctor

import (
	"github.com/ktpswjz/httpserver/document"
	"github.com/ktpswjz/httpserver/router"
	"github.com/ktpswjz/httpserver/types"
	"net/http"
	"time"
	"tlcbme_project/business"
	"tlcbme_project/business/doctor"
	"tlcbme_project/business/patient"
	"tlcbme_project/data/enum"
	"tlcbme_project/data/model"
	doctorModel "tlcbme_project/data/model/doctor"
	"tlcbme_project/database/memory"
	"tlcbme_project/errors"
	"tlcbme_project/server/config"
)

type Exp struct {
	doc
}

func NewExp(cfg *config.Config, log types.Log, dbToken memory.Token, doctorBusiness doctor.Business, patientBusiness patient.Business) *Exp {
	instance := &Exp{}
	instance.SetLog(log)
	instance.Config = cfg
	instance.DbToken = dbToken
	instance.doctorBusiness = doctorBusiness
	instance.patientBusiness = patientBusiness

	return instance
}

func (s *Exp) setDocFun(a document.Assistant, fun document.Function) {
	catalog := s.rootCatalog(a).CreateChild("饮食实验", "饮食实验相关接口")
	catalog.SetFunction(fun)
}

func (s *Exp) GetPatientExpInfo(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.ExpPatientInfoGet{}
	a.GetArgument(r, argument)

	data, be := s.patientBusiness.Data().GetExpPatientBaseInfo(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Exp) GetPatientExpInfoDoc(a document.Assistant) document.Function {
	patientID := uint64(100)
	function := a.CreateFunction("获取患者实验信息")
	function.SetNote("获取患者实验信息")
	function.SetInputExample(&doctorModel.ExpPatientInfoGet{
		PatientID: patientID,
	})
	function.SetOutputExample(&doctorModel.ExpPatientBaseInfo{
		PatientID: 100,
		Status:    0,
		//UndoneScales:   []uint64{1,2},
	})
	s.setDocFun(a, function)

	return function
}

func (s *Exp) IncludePatientInExp(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.ExpPatientCreate{}
	a.GetArgument(r, argument)
	now := types.Time(time.Now())
	argument.CreateDateTime = &now

	be := s.doctorBusiness.Exp().IncludePatientInExp(argument, false)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(nil)
}

func (s *Exp) IncludePatientInExpDoc(a document.Assistant) document.Function {
	patientID := uint64(100)
	now := types.Time(time.Now())
	function := a.CreateFunction("实验纳入患者")
	function.SetNote("将指定患者纳入到饮食实验中")
	function.SetInputExample(&doctorModel.ExpPatientCreate{
		PatientID:      patientID,
		Type:           0,
		CreateDateTime: &now,
	})
	function.SetOutputExample(nil)
	s.setDocFun(a, function)

	return function
}

func (s *Exp) RemovePatientFromExp(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.ExpPatientStatusChange{}
	a.GetArgument(r, argument)
	now := types.Time(time.Now())
	argument.UpdateDateTime = &now

	be := s.doctorBusiness.Exp().ChangePatientStatus(argument, false)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(nil)
}

func (s *Exp) RemovePatientFromExpDoc(a document.Assistant) document.Function {
	patientID := uint64(100)
	now := types.Time(time.Now())
	function := a.CreateFunction("实验移除患者")
	function.SetNote("将指定患者移除出饮食实验")
	function.SetInputExample(&doctorModel.ExpPatientStatusChange{
		PatientID:      patientID,
		Status:         0,
		UpdateDateTime: &now,
	})
	function.SetOutputExample(nil)
	s.setDocFun(a, function)

	return function
}

func (s *Exp) GetAllExpCommonScale(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	data, be := s.doctorBusiness.Exp().GetAllDoctorExpCommonScale()
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Exp) GetAllExpCommonScaleDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("获取所有实验量表")
	function.SetNote("从数据库中获取所有实验量表")
	function.SetInputExample(nil)
	function.SetOutputExample([]*doctorModel.ExpCommonScale{
		{
			SerialNo:    1,
			Title:       "test",
			Content:     "",
			Description: "无",
			ScaleType:   0,
		},
	})
	s.setDocFun(a, function)

	return function
}

func (s *Exp) GetExpCommonScale(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.ExpCommonScaleSerialNoFilter{}
	a.GetArgument(r, argument)

	data, be := s.doctorBusiness.Exp().GetExpCommonScale(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Exp) GetExpCommonScaleDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("获取实验量表")
	function.SetNote("从数据库中获取实验量表")
	function.SetInputExample(&doctorModel.ExpCommonScaleSerialNoFilter{
		SerialNo: 1,
	})
	function.SetOutputExample(doctorModel.ExpCommonScale{
		SerialNo:    1,
		Title:       "test",
		Content:     "",
		Description: "无",
		ScaleType:   0,
	})
	s.setDocFun(a, function)

	return function
}

func (s *Exp) CommitPatientExpScaleRecord(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.ExpScaleRecordCreate{}
	a.GetArgument(r, argument)

	if argument.CreateDateTime == nil {
		now := types.Time(time.Now())
		argument.CreateDateTime = &now
	}
	token, err := s.DbToken.Get(a.Token())
	if err != nil {
		a.Error(errors.InternalError, err)
	}
	argument.OperatorID = token.UserID
	//argument.UndoneScales, err = s.patientBusiness.Data().GetUndoneScales(argument.PatientID)
	//if err != nil {
	//	a.Error(errors.InternalError, err)
	//}

	data, be := s.doctorBusiness.Exp().CommitPatientExpScaleRecord(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Exp) CommitPatientExpScaleRecordDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("提交患者实验量表")
	function.SetNote("提交患者实验量表")
	function.SetInputExample(&doctorModel.ExpScaleRecord{
		PatientID: 100,
		ScaleType: 0,
		ScaleID:   1,
		Result: &doctorModel.Question{
			Type:     "singleCheck",
			Title:    "您平时吃饭要多久",
			Options:  nil,
			Degree:   nil,
			Barriers: nil,
			Checked:  nil,
		},
	})
	function.SetOutputExample(1)
	s.setDocFun(a, function)

	return function
}

func (s *Exp) GetPatientActionPlan(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.ExpPatientActionPlan{}
	a.GetArgument(r, argument)

	data, be := s.patientBusiness.Data().GetPatientExpActionPlanList(argument, false)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Exp) GetPatientActionPlanDoc(a document.Assistant) document.Function {
	patientID := uint64(100)
	now := types.Time(time.Now())
	function := a.CreateFunction("获取行动计划")
	function.SetNote("获取相应患者的行动计划")
	function.SetInputExample(&doctorModel.ExpPatientActionPlan{
		PatientID: patientID,
		Status:    0,
	})
	function.SetOutputExample([]*doctorModel.ExpActionPlan{
		{
			SerialNo:       11,
			PatientID:      patientID,
			ActionPlan:     "细嚼慢咽",
			Status:         0,
			CreateDateTime: &now,
			UpdateDateTime: nil,
			EditorType:     0,
		},
	})
	s.setDocFun(a, function)

	return function
}

func (s *Exp) CreatePatientActionPlan(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.ExpPatientActionPlanCreate{}
	a.GetArgument(r, argument)

	be := s.doctorBusiness.Exp().CreatePatientExpActionPlan(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(nil)
}

func (s *Exp) CreatePatientActionPlanDoc(a document.Assistant) document.Function {
	now := types.Time(time.Now())
	function := a.CreateFunction("调整行动计划")
	function.SetNote("调整相应患者的行动计划，包括计划内容")
	function.SetInputExample(&doctorModel.ExpPatientActionPlanCreate{
		ActionPlan:     "测试修改",
		CreateDateTime: &now,
		EditorType:     1,
		Level:          3,
	})
	function.SetOutputExample([]*doctorModel.ExpActionPlan{
		{
			SerialNo:       11,
			PatientID:      100,
			ActionPlan:     "细嚼慢咽",
			Status:         0,
			CreateDateTime: &now,
			UpdateDateTime: &now,
			EditorType:     0,
		},
	})
	s.setDocFun(a, function)

	return function
}

func (s *Exp) EditPatientActionPlan(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.ExpPatientActionPlanEdit{}
	a.GetArgument(r, argument)

	now := types.Time(time.Now())
	argument.UpdateDateTime = &now

	be := s.doctorBusiness.Exp().EditPatientExpActionPlan(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(nil)
}

func (s *Exp) EditPatientActionPlanDoc(a document.Assistant) document.Function {
	now := types.Time(time.Now())
	function := a.CreateFunction("调整行动计划")
	function.SetNote("调整相应患者的行动计划，包括计划内容")
	function.SetInputExample(&doctorModel.ExpPatientActionPlanEdit{
		SerialNo:       11,
		Status:         0,
		ActionPlan:     "测试修改",
		UpdateDateTime: &now,
		EditorType:     1,
		Level:          3,
	})
	function.SetOutputExample([]*doctorModel.ExpActionPlan{
		{
			SerialNo:       11,
			PatientID:      100,
			ActionPlan:     "细嚼慢咽",
			Status:         0,
			CreateDateTime: &now,
			UpdateDateTime: &now,
			EditorType:     0,
		},
	})
	s.setDocFun(a, function)

	return function
}

func (s *Exp) CommitPatientActionPlan(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.ExpActionPlanRecordForApp{}
	a.GetArgument(r, argument)

	sno, be := s.patientBusiness.Data().CommitExpActionPlanRecord(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(sno)
}

func (s *Exp) CommitPatientActionPlanDoc(a document.Assistant) document.Function {
	now := types.Time(time.Now())
	function := a.CreateFunction("提交行动计划")
	function.SetNote("提交相应患者的行动计划完成情况")
	function.SetInputExample(&doctorModel.ExpActionPlanRecordForApp{
		PatientID:      100,
		ActionPlan:     "测试提交",
		Completion:     2,
		CreateDateTime: &now,
	})
	function.SetOutputExample(11)
	s.setDocFun(a, function)

	return function
}

func (s *Exp) GetPatientActionPlanRecord(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.ExpActionPlanRecordTimeFilter{}
	a.GetArgument(r, argument)

	data, be := s.doctorBusiness.Exp().GetPatientActionPlanRecordInfo(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Exp) GetPatientActionPlanRecordDoc(a document.Assistant) document.Function {
	now := types.Time(time.Now())
	function := a.CreateFunction("获取行动计划完成情况")
	function.SetNote("获取相应患者的行动计划完成情况")
	function.SetInputExample(&doctorModel.ExpActionPlanRecordTimeFilter{
		PatientID:     100,
		StartDateTime: &now,
	})
	function.SetOutputExample(11)
	s.setDocFun(a, function)

	return function
}

func (s *Exp) GetPatientTaskRecordList(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.ExpPatientTaskRecordGetListInput{}
	a.GetArgument(r, argument)

	data, be := s.doctorBusiness.Exp().GetPatientTaskRecordList(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Exp) GetPatientTaskRecordListDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("获取行动计划完成情况")
	function.SetNote("获取相应患者的行动计划完成情况")
	function.SetInputExample(&doctorModel.ExpPatientTaskRecordGetListInput{
		PatientID: 100,
	})
	function.SetOutputExample([]*doctorModel.ExpPatientTaskRecords{
		{
			ParentTaskName: "粗粮",
			RecordTaskSnos: []uint64{1},
			RecordTask:     []string{"阅读"},
		},
	})
	s.setDocFun(a, function)

	return function
}

func (s *Exp) GetPatientTaskRecord(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.ExpPatientTaskRecordGetInput{}
	a.GetArgument(r, argument)

	data, be := s.doctorBusiness.Exp().GetPatientTaskRecord(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Exp) GetPatientTaskRecordDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("获取行动计划完成情况")
	function.SetNote("获取相应患者的行动计划完成情况")
	function.SetInputExample(&doctorModel.ExpPatientTaskRecordGetInput{
		SerialNo: 1,
	})
	function.SetOutputExample(&doctorModel.ExpPatientTaskRecord{})
	s.setDocFun(a, function)

	return function
}

func (s *Exp) GetPatientDietRecord(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.SportDietRecordFilter{}
	a.GetArgument(r, argument)

	data, be := s.doctorBusiness.Exp().GetPatientDietRecord(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Exp) GetPatientDietRecordDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("获取行动计划完成情况")
	function.SetNote("获取相应患者的行动计划完成情况")
	function.SetInputExample(&doctorModel.SportDietRecordFilter{
		SerialNo: 1,
	})
	function.SetOutputExample(&doctorModel.SportDietRecord{})
	s.setDocFun(a, function)

	return function
}

func (s *Exp) GetPatientDietRecordComment(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.ExpDoctorCommentToPatientSportDietRecordFilter{}
	a.GetArgument(r, argument)

	data, be := s.doctorBusiness.Exp().GetPatientDietRecordComment(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Exp) GetPatientDietRecordCommentDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("获取患者饮食的评论")
	function.SetNote("获取患者饮食的评论")
	function.SetInputExample(&doctorModel.ExpDoctorCommentToPatientSportDietRecordFilter{
		SportDietRecord: 1,
	})
	function.SetOutputExample(&doctorModel.ExpDoctorCommentToPatient{})
	s.setDocFun(a, function)

	return function
}

func (s *Exp) CommitPatientDietRecordComment(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.ExpDoctorCommentToPatientCreate{}
	a.GetArgument(r, argument)

	sno, be := s.doctorBusiness.Exp().CommitPatientDietRecordComment(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}
	if argument.SerialNo == 0 {
		newArgument := &doctorModel.WeChatRemindTaskSend{
			PatientID:  argument.PatientID,
			TemplateID: argument.TemplateID,
		}
		be = s.patientBusiness.Data().SendPhotoFeedbackRemind(newArgument, sno)
		if be != nil {
			a.Error(be.Error(), be.Detail())
			return
		}
	}

	a.Success(true)
}

func (s *Exp) CommitPatientDietRecordCommentDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("提交或修改患者饮食的评论")
	function.SetNote("提交或修改患者饮食的评论")
	function.SetInputExample(&doctorModel.ExpDoctorCommentToPatient{
		SportDietRecord: 1,
	})
	function.SetOutputExample(nil)
	s.setDocFun(a, function)

	return function
}

func (s *Exp) GetCommentTemplate(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.ExpDoctorCommentTemplateType{}
	a.GetArgument(r, argument)
	argument.EditorID = s.getLoginUserId(a)

	data, be := s.doctorBusiness.Exp().GetCommentTemplate(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Exp) GetCommentTemplateDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("获取评论模板")
	function.SetNote("获取评论模板，包括个人的与所有的")
	function.SetInputExample(&doctorModel.ExpDoctorCommentTemplateType{
		TemplateType: 1,
	})
	function.SetOutputExample([]*doctorModel.ExpDoctorCommentTemplate{
		{
			SerialNo:        1,
			TemplateType:    1,
			TemplateContent: "多吃饭，少干活",
		},
	})
	s.setDocFun(a, function)

	return function
}

func (s *Exp) AddCommentTemplate(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.ExpDoctorCommentTemplateCreate{}
	a.GetArgument(r, argument)
	argument.EditorID = s.getLoginUserId(a)

	be := s.doctorBusiness.Exp().AddCommentTemplate(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(true)
}

func (s *Exp) AddCommentTemplateDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("增加评论模板")
	function.SetNote("增加评论模板")
	function.SetInputExample(&doctorModel.ExpDoctorCommentTemplateCreate{})
	function.SetOutputExample(nil)
	s.setDocFun(a, function)

	return function
}

func (s *Exp) DeleteCommentTemplate(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.ExpDoctorCommentTemplateDelete{}
	a.GetArgument(r, argument)
	argument.EditorID = s.getLoginUserId(a)

	be := s.doctorBusiness.Exp().DeleteCommentTemplate(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(true)
}

func (s *Exp) DeleteCommentTemplateDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("增加评论模板")
	function.SetNote("增加评论模板")
	function.SetInputExample(&doctorModel.ExpDoctorCommentTemplateCreate{})
	function.SetOutputExample(nil)
	s.setDocFun(a, function)

	return function
}

func (s *Exp) TopCommentTemplate(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.ExpDoctorCommentTemplateDelete{}
	a.GetArgument(r, argument)
	argument.EditorID = s.getLoginUserId(a)

	be := s.doctorBusiness.Exp().TopCommentTemplate(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(true)
}

func (s *Exp) GetUnCommentTaskCount(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.ExpPatientTaskRecordGetByName{}
	a.GetArgument(r, argument)
	argument.HealthManagerID = s.getLoginUserId(a)

	data, be := s.doctorBusiness.Exp().GetUnCommentTaskCount(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Exp) GetUnCommentTaskCountDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("获取未评论的数量")
	function.SetNote("获取未评论的数量")
	function.SetInputExample(&doctorModel.ExpDoctorCommentTemplateType{
		TemplateType: 1,
	})
	function.SetOutputExample([]*doctorModel.ExpDoctorCommentTemplate{
		{
			SerialNo:        1,
			TemplateType:    1,
			TemplateContent: "多吃饭，少干活",
		},
	})
	s.setDocFun(a, function)

	return function
}

func (s *Exp) GetUnCommentTaskPage(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	filter := &doctorModel.ExpPatientTaskRecordGetByName{}
	argument := &model.PageFilter{
		Page: model.Page{
			Index: 1,
			Size:  15,
		},
		Filter: filter,
	}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}
	filter.HealthManagerID = s.getLoginUserId(a)

	data, be := s.doctorBusiness.Exp().GetUnCommentTaskPage(argument.Index, argument.Size, filter)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Exp) GetUnCommentTaskPageDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("根据任务名获取未评论的任务")
	function.SetNote("根据任务名获取未评论的任务，通常为拍照任务，需要评论")
	function.SetInputExample(&doctorModel.ExpPatientTaskRecordGetByName{
		TaskName: "拍照",
	})
	function.SetOutputExample([]*doctorModel.ExpPatientTaskRecordForApp{
		{
			SerialNo:       1,
			PatientID:      100,
			TaskID:         1,
			CreateDateTime: nil,
			Record:         nil,
		},
	})
	s.setDocFun(a, function)

	return function
}

func (s *Exp) GetCurrentDayWithoutClockingInPage(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	filter := &doctorModel.ExpPatientTaskRecordGetByName{}
	argument := &model.PageFilter{
		Page: model.Page{
			Index: 1,
			Size:  15,
		},
		Filter: filter,
	}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}
	token, err := s.DbToken.Get(a.Token())
	if err != nil {
		a.Error(errors.InternalError, err)
	}
	//filter.HealthManagerID = s.getLoginUserId(a)
	// 0-医生，1-健康管理师
	if token.UserType == enum.DoctorUserTypes.Doctor().Key {
		filter.HealthManagerID = token.UserID
	} else if token.UserType == enum.DoctorUserTypes.HealthManager().Key {
		filter.HealthManagerID = token.UserID
	} else if token.UserType == enum.DoctorUserTypes.DoctorAndHealthManager().Key {
		filter.DoctorID = token.UserID
		filter.HealthManagerID = token.UserID
	}
	data, be := s.doctorBusiness.Exp().GetCurrentDayWithoutClockingInPage(argument.Index, argument.Size, filter)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Exp) GetCurrentDayWithoutClockingInPageDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("根据任务名获取当日未打卡")
	function.SetNote("根据任务名获取当日未打卡")
	function.SetInputExample(&doctorModel.ExpPatientTaskRecordGetByName{
		TaskName: "拍照",
	})
	function.SetOutputExample([]*doctorModel.ExpPatientTaskRecordForApp{
		{
			SerialNo:       1,
			PatientID:      100,
			TaskID:         1,
			CreateDateTime: nil,
			Record:         nil,
		},
	})
	s.setDocFun(a, function)

	return function
}

func (s *Exp) GetCommentRecordPage(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	filter := &doctorModel.ExpPatientTaskRecordGetByNameAll{}
	argument := &model.PageFilter{
		Page: model.Page{
			Index: 1,
			Size:  15,
		},
		Filter: filter,
	}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}
	//filter.HealthManagerID = s.getLoginUserId(a)

	data, be := s.doctorBusiness.Exp().GetCommentRecordPage(argument.Index, argument.Size, filter)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Exp) DeleteCommentRecord(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.ExpPatientTaskRecordDelete{}
	a.GetArgument(r, argument)
	be := s.doctorBusiness.Exp().DeleteCommentRecord(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(true)
}

func (s *Exp) GetPatientCheckData(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.ExpPatientTaskRecordGetCheckDataInput{}
	a.GetArgument(r, argument)

	data, be := s.doctorBusiness.Exp().GetPatientCheckData(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Exp) GetPatientBodyExamImageList(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.ExpPatientBodyExamImageGet{}
	a.GetArgument(r, argument)

	data, be := s.doctorBusiness.Exp().GetPatientBodyExamImage(argument.PatientID)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Exp) UploadPatientFile(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.ExpPatientUploadFile{}
	a.GetArgument(r, argument)

	be := s.doctorBusiness.Exp().UploadPatientFile(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(true)
}

func (s *Exp) GetPatientFile(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.ExpPatientUploadFileGet{}
	a.GetArgument(r, argument)

	data, be := s.doctorBusiness.Exp().GetPatientFile(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Exp) GetPatientDietPlan(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.ExpPatientDietPlanGet{}
	a.GetArgument(r, argument)

	data, be := s.doctorBusiness.Exp().GetPatientDietPlan(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Exp) DeletePatientDietPlan(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.ExpPatientDietPlanDelete{}
	a.GetArgument(r, argument)
	be := s.patientBusiness.Data().DeletePatientDietPlan(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(true)
}

func (s *Exp) SavePatientDietPlan(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.ExpPatientDietPlanSave{}
	a.GetArgument(r, argument)

	be := s.doctorBusiness.Exp().SavePatientDietPlan(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(true)
}

func (s *Exp) SaveAndDelayPatientDietPlan(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.ExpPatientDietPlanSave{}
	a.GetArgument(r, argument)

	be := s.doctorBusiness.Exp().SaveAndDelayPatientDietPlan(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(true)
}

func (s *Exp) StartPatientDietPlan(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.ExpPatientDietPlanGet{}
	a.GetArgument(r, argument)

	be := s.doctorBusiness.Exp().StartDietPlan(argument.PatientID)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(true)
}

func (s *Exp) GetTaskTree(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	data, be := s.doctorBusiness.Exp().GetTaskTree()
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Exp) AddTask(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.ExpTaskRepositoryAddInput{}
	a.GetArgument(r, argument)
	token, err := s.DbToken.Get(a.Token())
	if err != nil {
		a.Error(errors.InternalError, err)
	}
	argument.EditorID = token.UserID
	now := types.Time(time.Now())
	argument.CreateDateTime = &now

	data, be := s.doctorBusiness.Exp().AddTask(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Exp) EditTask(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.ExpTaskRepositoryEditInput{}
	a.GetArgument(r, argument)
	token, err := s.DbToken.Get(a.Token())
	if err != nil {
		a.Error(errors.InternalError, err)
	}
	argument.EditorID = token.UserID
	now := types.Time(time.Now())
	argument.UpdateTime = &now

	data, be := s.doctorBusiness.Exp().EditTask(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Exp) DeleteTask(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.ExpTaskRepositoryDeleteInput{}
	a.GetArgument(r, argument)
	token, err := s.DbToken.Get(a.Token())
	if err != nil {
		a.Error(errors.InternalError, err)
	}
	argument.EditorID = token.UserID
	now := types.Time(time.Now())
	argument.UpdateTime = &now
	argument.Status = 1 //将状态置1

	data, be := s.doctorBusiness.Exp().DeleteTask(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Exp) GetKnowledgeByType(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.ExpKnowledgeTypeInput{}
	a.GetArgument(r, argument)
	argument.Status = 0 //0-正常，1-弃用

	data, be := s.doctorBusiness.Exp().GetKnowledgeByType(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Exp) EditKnowledge(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.ExpKnowledgeEditInput{}
	a.GetArgument(r, argument)
	token, err := s.DbToken.Get(a.Token())
	if err != nil {
		a.Error(errors.InternalError, err)
	}
	argument.EditorID = token.UserID
	now := types.Time(time.Now())
	argument.UpdateTime = &now

	be := s.doctorBusiness.Exp().EditKnowledge(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(nil)
}

func (s *Exp) AddKnowledge(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.ExpKnowledgeAddInput{}
	a.GetArgument(r, argument)
	token, err := s.DbToken.Get(a.Token())
	if err != nil {
		a.Error(errors.InternalError, err)
	}
	argument.EditorID = token.UserID
	now := types.Time(time.Now())
	argument.CreateDateTime = &now

	sno, be := s.doctorBusiness.Exp().AddKnowledge(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(sno)
}

func (s *Exp) DeleteKnowledge(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.ExpKnowledgeDeleteInput{}
	a.GetArgument(r, argument)
	token, err := s.DbToken.Get(a.Token())
	if err != nil {
		a.Error(errors.InternalError, err)
	}
	argument.EditorID = token.UserID
	now := types.Time(time.Now())
	argument.UpdateTime = &now
	argument.Status = 1 //弃用

	be := s.doctorBusiness.Exp().DeleteKnowledge(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(true)
}

func (s *Exp) GetQuestionByKnowledgeID(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.ExpKnowledgeIDInput{}
	a.GetArgument(r, argument)
	argument.Status = 0 //状态为0表示正常，1表示弃用

	data, be := s.doctorBusiness.Exp().GetQuestionByKnowledgeID(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Exp) AddQuestion(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.ExpQuestionToKnowledgeAddInput{}
	a.GetArgument(r, argument)
	token, err := s.DbToken.Get(a.Token())
	if err != nil {
		a.Error(errors.InternalError, err)
	}
	argument.EditorID = token.UserID
	now := types.Time(time.Now())
	argument.CreateDateTime = &now

	sno, be := s.doctorBusiness.Exp().AddQuestion(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(sno)
}

func (s *Exp) DeleteQuestion(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.ExpQuestionToKnowledgeDeleteInput{}
	a.GetArgument(r, argument)
	token, err := s.DbToken.Get(a.Token())
	if err != nil {
		a.Error(errors.InternalError, err)
	}
	argument.EditorID = token.UserID
	now := types.Time(time.Now())
	argument.UpdateTime = &now
	argument.Status = 1 //弃用

	be := s.doctorBusiness.Exp().DeleteQuestion(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(true)
}

func (s *Exp) EditQuestion(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.ExpQuestionToKnowledgeEditInput{}
	a.GetArgument(r, argument)
	token, err := s.DbToken.Get(a.Token())
	if err != nil {
		a.Error(errors.InternalError, err)
	}
	argument.EditorID = token.UserID
	now := types.Time(time.Now())
	argument.UpdateTime = &now

	be := s.doctorBusiness.Exp().EditQuestion(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(true)
}

func (s *Exp) GetScale(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.ExpSmallScaleTaskIDInput{}
	a.GetArgument(r, argument)

	data, be := s.patientBusiness.Data().GetSmallScale(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Exp) EditScale(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.ExpSmallScaleContentEdit{}
	a.GetArgument(r, argument)

	be := s.patientBusiness.Data().EditSmallScale(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(true)
}

func (s *Exp) NutrientAnalysis(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.NutrientAnalysisFilter{}
	a.GetArgument(r, argument)

	data, _ := s.patientBusiness.Data().NutrientAnalysis(argument)
	//if be != nil {
	//	a.Error(be.Error(), be.Detail())
	//	return
	//}

	a.Success(data)
}

func (s *Exp) NutrientAnalysisDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("营养分析")
	function.SetNote("营养分析")
	now := types.Time(time.Now())
	function.SetInputExample(&doctorModel.NutrientAnalysisFilter{
		PatientID:        10000,
		AnalysisDateTime: &now,
	})
	function.SetOutputExample(&doctorModel.NutrientAnalysisResult{})
	s.setDocFun(a, function)

	return function
}

func (s *Exp) SaveNutrientAnalysis(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.NutrientAnalysisFilter{}
	a.GetArgument(r, argument)
	data, be := s.patientBusiness.Data().NutrientAnalysis(argument)

	s.patientBusiness.Data().CreateNutrientReportRecord(data, argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Exp) SaveNutrientAnalysisDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("手工触发营养分析任务")
	function.SetNote("手工触发营养分析任务")
	now := types.Time(time.Now())
	function.SetInputExample(&doctorModel.NutrientAnalysisFilter{
		PatientID:        10000,
		AnalysisDateTime: &now,
	})
	function.SetOutputExample(&doctorModel.NutrientAnalysisResult{})
	s.setDocFun(a, function)

	return function
}

func (s *Exp) GetHistoryGrade(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.NutrientAnalysisFilterEx{}
	a.GetArgument(r, argument)
	data, be := s.patientBusiness.Data().GetHistoryGrade(argument)

	//s.patientBusiness.Data().CreateNutrientReportRecord(data, argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Exp) GetHistoryGradeDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("营养分析历史分数")
	function.SetNote("营养分析历史分数")
	now := types.Time(time.Now())
	function.SetInputExample(&doctorModel.NutrientAnalysisFilter{
		PatientID:        10000,
		AnalysisDateTime: &now,
	})
	function.SetOutputExample(&doctorModel.NutrientAnalysisResult{})
	s.setDocFun(a, function)

	return function
}

func (s *Exp) EditTaskFeature(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.ExpPatientFeatureToTask{}
	a.GetArgument(r, argument)

	var be business.Error
	if argument.SerialNo == 0 {
		be = s.doctorBusiness.Exp().CreateFeatureToTask(argument)
	} else {
		be = s.doctorBusiness.Exp().EditFeatureToTask(argument)
	}

	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(true)
}

func (s *Exp) GetHistoryEnergy(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.NutrientAnalysisFilterEx{}
	a.GetArgument(r, argument)
	data, be := s.patientBusiness.Data().GetHistoryEnergy(argument)

	//s.patientBusiness.Data().CreateNutrientReportRecord(data, argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Exp) GetHistoryEnergyDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("营养分析历史能量")
	function.SetNote("营养分析历史能量")
	now := types.Time(time.Now())
	function.SetInputExample(&doctorModel.NutrientAnalysisFilter{
		PatientID:        10000,
		AnalysisDateTime: &now,
	})
	function.SetOutputExample(nil)
	s.setDocFun(a, function)

	return function
}

func (s *Exp) GetIntegral(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.ChatMsgPatientID{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}
	data, be := s.patientBusiness.Data().GetIntegral(argument)

	//s.patientBusiness.Data().CreateNutrientReportRecord(data, argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Exp) GetIntegralDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("获取用户积分")
	function.SetNote("获取用户积分")
	function.SetInputExample(&doctorModel.ChatMsgPatientID{
		PatientID: 100214,
	})
	function.SetOutputExample(nil)
	s.setDocFun(a, function)

	return function
}

func (s *Exp) GetGradeRule(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	token, err := s.DbToken.Get(a.Token())
	if err != nil {
		a.Error(errors.InternalError, err)
	}

	data, be := s.patientBusiness.Data().GetGradeRule(token.OrgCode)

	//s.patientBusiness.Data().CreateNutrientReportRecord(data, argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Exp) GetGradeRuleDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("获取积分规则")
	function.SetNote("获取积分规则")
	function.SetInputExample(nil)
	function.SetOutputExample(doctorModel.ExpIntegralRule{})
	s.setDocFun(a, function)

	return function
}

func (s *Exp) UpdateGradeRule(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.ExpIntegralRule{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}

	token, err := s.DbToken.Get(a.Token())
	if err != nil {
		a.Error(errors.InternalError, err)
	}
	argument.OrgCode = token.OrgCode

	data, be := s.patientBusiness.Data().UpdateGradeRule(argument)

	//s.patientBusiness.Data().CreateNutrientReportRecord(data, argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Exp) UpdateGradeRuleDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("获取积分规则")
	function.SetNote("获取积分规则")
	function.SetInputExample(nil)
	function.SetOutputExample(doctorModel.ExpIntegralRule{})
	s.setDocFun(a, function)

	return function
}

func (s *Exp) GetDietProgramData(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.ChatMsgPatientID{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}

	data, be := s.patientBusiness.Data().GetNutritionIntervention(argument.PatientID)

	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Exp) GetDietProgramDataDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("获取饮食方案界面所有数据")
	function.SetNote("获取饮食方案界面所有数据")
	function.SetInputExample(doctorModel.ChatMsgPatientID{})
	function.SetOutputExample(doctorModel.ExpPatientNutritionInterventionTotal{})
	s.setDocFun(a, function)

	return function
}

func (s *Exp) UpdateNutritionalIndicator(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.ExpPatientNutritionalIndicator{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}

	be := s.patientBusiness.Data().UpdateNutritionIntervention(argument)

	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(true)
}

func (s *Exp) UpdateNutritionalIndicatorDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("修改用户营养指标目标数值")
	function.SetNote("修改用户营养指标目标数值")
	function.SetInputExample(doctorModel.ExpPatientNutritionalIndicator{})
	function.SetOutputExample(true)
	s.setDocFun(a, function)

	return function
}

func (s *Exp) UpdateDietProgram(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {

	//dietPrograms := make([]*doctorModel.ExpPatientDietProgram, 0)
	//dietProgram := &doctorModel.ExpPatientDietProgram{}
	//argument := &model.PageFilter{
	//	Page: model.Page{
	//		Index: 1,
	//		Size:  15,
	//	},
	//	Filter: filter,
	//}
	//
	//
	//
	//argument := &doctorModel.ExpPatientDietProgramUpdate{}
	//err := a.GetArgument(r, argument)
	//if err != nil {
	//	a.Error(errors.InternalError, err)
	//	return
	//}
	//
	//be := s.patientBusiness.Data().UpdateDietProgram(argument)
	//
	//if be != nil {
	//	a.Error(be.Error(), be.Detail())
	//	return
	//}
	//
	//a.Success(true)
	//
	//
	//
	argument := &doctorModel.ExpPatientDietProgramWholeUpdate{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}
	be := s.patientBusiness.Data().UpdateWholeDietProgram(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}
	a.Success(true)
}

func (s *Exp) UpdateDietProgramDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("更新用户的饮食方案")
	function.SetNote("更新用户的饮食方案")
	function.SetInputExample(doctorModel.ExpPatientDietProgram{})
	function.SetOutputExample(true)
	s.setDocFun(a, function)

	return function
}

func (s *Exp) GetDietPlanTemplate(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	data, be := s.patientBusiness.Data().GetDietPlanTemplate()
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Exp) GetNutritionWeeklyReport(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.ExpNutritionInterventionReportGet{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}
	data, be := s.patientBusiness.Data().GetWeeklyReport(argument.PatientID)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}
	a.Success(data)
}

func (s *Exp) GetAllGroupTasks(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.TlcTeamGroupTaskGroupKindsInput{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}
	data, be := s.patientBusiness.Data().GetAllGroupTasks(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}
	a.Success(data)
}

func (s *Exp) CreateGroupTask(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.TlcTeamGroupTaskCreateInfo{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}
	data, be := s.patientBusiness.Data().CreateGroupTask(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}
	a.Success(data)
}

func (s *Exp) StopGroupTask(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.TlcTeamGroupTaskStopInfo{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}
	data, be := s.patientBusiness.Data().StopGroupTask(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}
	a.Success(data)
}

func (s *Exp) ListGroupDict(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	data, be := s.patientBusiness.Data().ListGroupDict()
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}
	a.Success(data)
}

func (s *Exp) ListTeamDict(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	data, be := s.patientBusiness.Data().ListTeamDict()
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}
	a.Success(data)
}

func (s *Exp) AddCamp(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.CampListAddInput{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}

	sno, be := s.doctorBusiness.Exp().AddCamp(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}
	a.Success(sno)
}

func (s *Exp) ListCampPage(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &model.PageFilter{
		Page: model.Page{
			Index: 1,
			Size: 15,
		},
	}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}

	data, be := s.doctorBusiness.Exp().ListCampPage(argument.Index, argument.Size)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}
	a.Success(data)
}

func (s *Exp) ListCampDict(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	data, be := s.doctorBusiness.Exp().ListCampDict()
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}
	a.Success(data)
}

func (s *Exp) GetCamp(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.CampListSerialNo{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}

	data, be := s.doctorBusiness.Exp().GetCamp(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}
	a.Success(data)
}

func (s *Exp) UpdateCamp(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.CampListUpdateInput{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}

	data, be := s.doctorBusiness.Exp().UpdateCamp(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Exp) DeleteCamp(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.CampListSerialNo{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}

	be := s.doctorBusiness.Exp().DeleteCamp(argument)
	if be != nil {
		a.Error(errors.InternalError, err)
		return
	}

	a.Success(true)
}