package doctor

import (
	"tlcbme_project/business/doctor"
	"tlcbme_project/business/patient"
	doctorModel "tlcbme_project/data/model/doctor"
	"tlcbme_project/database/memory"
	"tlcbme_project/errors"
	"tlcbme_project/server/config"
	"github.com/ktpswjz/httpserver/document"
	"github.com/ktpswjz/httpserver/router"
	"github.com/ktpswjz/httpserver/types"
	"net/http"
	"time"
)

type Drug struct {
	doc
}

func NewDrug(cfg *config.Config, log types.Log, dbToken memory.Token, doctorBusiness doctor.Business, patientBusiness patient.Business) *Drug {
	instance := &Drug{}
	instance.SetLog(log)
	instance.Config = cfg
	instance.DbToken = dbToken
	instance.doctorBusiness = doctorBusiness
	instance.patientBusiness = patientBusiness

	return instance
}

func (s *Drug) setDocFun(a document.Assistant, fun document.Function) {
	catalog := s.rootCatalog(a).CreateChild("用药管理", "用药信息相关接口")
	catalog.SetFunction(fun)
}

func (s *Drug) SearchDrugUseRecordIndexList(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.DrugUseRecordIndexFilter{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InputError, err)
		return
	}

	data, be := s.doctorBusiness.Drug().SearchDrugUseRecordIndexList(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Drug) SearchDrugUseRecordIndexListDoc(a document.Assistant) document.Function {
	patientID := uint64(100)
	status := uint64(0)
	now := time.Now()
	firstUseDateTime := types.Time(now.Add(3 * 24 * time.Hour))

	function := a.CreateFunction("获取患者用药索引列表")
	function.SetNote("获取患者用药索引列表")
	function.SetInputExample(&doctorModel.DrugUseRecordIndexFilter{
		PatientID: &patientID,
		Status:    &status,
	})
	function.SetOutputExample([]*doctorModel.DrugUseRecordIndexEx{
		{
			DrugUseRecordIndex: doctorModel.DrugUseRecordIndex{
				PatientID:        patientID,
				DrugName:         "氨氯地平片",
				Dosage:           "5mg",
				Freq:             "1次/日",
				FirstUseDateTime: &firstUseDateTime,
				Status:           status,
			},
			UsedDays: 3,
		},
	})
	s.setDocFun(a, function)

	return function
}

func (s *Drug) SearchDrugUseRecordMonthGroupList(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.DrugUseRecordFilter{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InputError, err)
		return
	}

	data, be := s.doctorBusiness.Drug().SearchDrugUseRecordMonthGroupList(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Drug) SearchDrugUseRecordMonthGroupListDoc(a document.Assistant) document.Function {
	patientID := uint64(100)
	now := time.Now()
	useDateEnd := types.Time(now)

	function := a.CreateFunction("获取患者用药记录列表（按月分组）")
	function.SetNote("获取患者用药记录列表")
	function.SetInputExample(&doctorModel.DrugUseRecordFilter{
		PatientID:  &patientID,
		DrugName:   "氨氯地平片",
		UseDateEnd: &useDateEnd,
	})
	function.SetOutputExample([]*doctorModel.DrugUseRecordMonthGroup{
		{
			Year:  2018,
			Month: 10,
			Records: []*doctorModel.DrugUseRecord{
				{
					PatientID:   patientID,
					DrugName:    "氨氯地平片",
					UseDateTime: &useDateEnd,
				},
			},
		},
	})
	s.setDocFun(a, function)

	return function
}

func (s *Drug) SearchDrugUseRecordIndexModifiedLogList(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.DrugUseRecordIndexModifiedLogFilter{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InputError, err)
		return
	}

	data, be := s.doctorBusiness.Drug().SearchDrugUseRecordIndexModifiedLogList(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Drug) SearchDrugUseRecordIndexModifiedLogListDoc(a document.Assistant) document.Function {
	patientID := uint64(100)
	DURISerialNo := uint64(232442)
	now := time.Now()
	modifyDateTime := types.Time(now)

	function := a.CreateFunction("获取患者用药记录用法列表")
	function.SetNote("获取患者用药记录用法列表")
	function.SetInputExample(&doctorModel.DrugUseRecordIndexModifiedLogFilter{
		PatientID: &patientID,
		DrugName:  "氨氯地平片",
	})
	function.SetOutputExample([]*doctorModel.DrugUseRecordIndexModifiedLog{
		{
			SerialNo:       324,
			DURISerialNo:   &DURISerialNo,
			PatientID:      patientID,
			DrugName:       "氨氯地平片",
			Dosage:         "5mg",
			Freq:           "1次/日",
			ModifyType:     "开始使用",
			ModifyDateTime: &modifyDateTime,
		},
	})
	s.setDocFun(a, function)

	return function
}

func (s *Drug) CreateDrugUseRecordIndexModifiedLog(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.DrugUseRecordIndexModifiedLogCreate{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InputError, err)
		return
	}

	data, be := s.doctorBusiness.Drug().CreateDrugUseRecordIndexModifiedLog(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Drug) CreateDrugUseRecordIndexModifiedLogDoc(a document.Assistant) document.Function {
	patientID := uint64(100)
	DURISerialNo := uint64(232442)

	function := a.CreateFunction("新增用药记录或用法")
	function.SetNote("修改用药状态或用法")
	function.SetInputExample(&doctorModel.DrugUseRecordIndexModifiedLogCreate{
		PatientID:    patientID,
		DrugName:     "氨氯地平片",
		DURISerialNo: &DURISerialNo,
		Dosage:       "5mg",
		Freq:         "1次/日",
		ModifyType:   "调整用法",
	})
	function.SetOutputExample(uint64(324))
	s.setDocFun(a, function)

	return function
}

func (s *Drug) CreateDrugUseRecordIndex(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.DrugRecordForDoctorList{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InputError, err)
		return
	}
	token, err := s.DbToken.Get(a.Token())
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}
	if token == nil {
		a.Error(errors.AuthNoToken)
		return
	}
	data, be := s.doctorBusiness.Drug().CreateDrugUseRecordIndex(argument, token.UserName)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Drug) CreateDrugUseRecordIndexDoc(a document.Assistant) document.Function {
	patientID := uint64(100)

	function := a.CreateFunction("新增用药记录")
	function.SetNote("新增用药记录")
	function.SetInputExample([]*doctorModel.DrugUseRecordIndex{
		{
			PatientID: patientID,
			DrugName:  "氨氯地平片",
			Dosage:    "5mg",
			Freq:      "1次/日",
		},
	})
	function.SetOutputExample(uint64(324))
	s.setDocFun(a, function)

	return function
}
