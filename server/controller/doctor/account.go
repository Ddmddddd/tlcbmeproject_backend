package doctor

import (
	"bytes"
	"tlcbme_project/business/doctor"
	doctorModel "tlcbme_project/data/model/doctor"
	"tlcbme_project/database/memory"
	"tlcbme_project/errors"
	"tlcbme_project/server/config"
	"encoding/base64"
	"fmt"
	"github.com/boombuler/barcode/qr"
	"github.com/ktpswjz/httpserver/document"
	"github.com/ktpswjz/httpserver/router"
	"github.com/ktpswjz/httpserver/types"
	"image/png"
	"net/http"
	"path/filepath"
	"strings"
	"time"
)

type Account struct {
	doc

	downloadSitePrefix string
}

func NewAccount(cfg *config.Config, log types.Log, dbToken memory.Token, doctorBusiness doctor.Business, downloadSitePrefix string) *Account {
	instance := &Account{}
	instance.SetLog(log)
	instance.Config = cfg
	instance.DbToken = dbToken
	instance.doctorBusiness = doctorBusiness
	instance.downloadSitePrefix = downloadSitePrefix

	return instance
}

func (s *Account) setDocFun(a document.Assistant, fun document.Function) {
	catalog := s.rootCatalog(a).CreateChild("用户管理", "医生信息相关接口")
	catalog.SetFunction(fun)
}

func (s *Account) Logout(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	token, err := s.DbToken.Get(a.Token())
	if err != nil {
		a.Error(errors.Exception, err)
		return
	}
	if token == nil {
		a.Error(errors.NotExist, "凭证'", a.Token(), "'不存在")
		return
	}
	err = s.DbToken.Del(token)
	if err != nil {
		a.Error(errors.Exception, err)
		return
	}

	a.Success(true)
}

func (s *Account) LogoutDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("退出登录")
	function.SetNote("退出登录, 使当前凭证失效")
	function.SetOutputExample(true)
	function.SetContentType("")

	catalog := s.rootCatalog(a).CreateChild("权限管理", "系统授权相关接口")
	catalog.SetFunction(function)

	return function
}

func (s *Account) GetBasicInfo(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.ViewDoctorUserFilter{
		UserID: s.getLoginUserId(a),
	}
	a.GetArgument(r, argument)
	if argument.UserID < 1 {
		a.Error(errors.InputInvalid, fmt.Errorf("用户ID'%d'无效", argument.UserID))
		return
	}

	data, be := s.doctorBusiness.Authentication().Detail(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Account) GetBasicInfoDoc(a document.Assistant) document.Function {
	userType := uint64(0)
	function := a.CreateFunction("获取账号基本信息")
	function.SetNote("获取医生账号基本信息，包括姓名、性别、头像等。如果未指定用户ID，则返回当前登录用户的基本信息")
	function.SetInputExample(&doctorModel.ViewDoctorUserFilter{
		UserID: 1,
	})
	function.SetOutputExample(&doctorModel.ViewDoctorUserEx{
		ViewDoctorUser: doctorModel.ViewDoctorUser{
			UserID:         1,
			UserName:       "",
			MobilePhone:    "",
			Email:          "",
			Status:         0,
			LoginCount:     0,
			Name:           "张三",
			Phone:          "13812344321",
			Photo:          "",
			OrgCode:        "224214",
			UserType:       &userType,
			VerifiedStatus: &userType,
			OrgName:        "第一人民医院",
		},
		StatusText:         "正常",
		UserTypeText:       "医生",
		VerifiedStatusText: "未认证",
	})

	s.setDocFun(a, function)

	return function
}

func (s *Account) ChangePassword(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.DoctorUserAuthsChangePassword{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InputError, err)
		return
	}
	if argument.OldPassword == "" {
		a.Error(errors.InputInvalid, "旧密码为空")
		return
	}
	if argument.NewPassword == "" {
		a.Error(errors.InputInvalid, "新密码为空")
		return
	}

	oldPassword := argument.OldPassword
	newPassword := argument.NewPassword
	if strings.ToLower(argument.Encryption) == "rsa" {
		decryptedOldPassword, err := a.RandKey().DecryptData(argument.OldPassword)
		if err != nil {
			a.Error(errors.LoginPasswordInvalid, err)
			return
		}
		oldPassword = string(decryptedOldPassword)

		decryptedOldNewPassword, err := a.RandKey().DecryptData(argument.NewPassword)
		if err != nil {
			a.Error(errors.LoginPasswordInvalid, err)
			return
		}
		newPassword = string(decryptedOldNewPassword)
	}

	be := s.doctorBusiness.Authentication().ChangePassword(s.getLoginUserId(a), oldPassword, newPassword)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(true)
}

func (s *Account) ChangePasswordDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("修改当前登录用户密码")
	function.SetNote("修改当前登录用户的账号密码")
	function.SetInputExample(&doctorModel.DoctorUserAuthsChangePassword{
		OldPassword: "old",
		NewPassword: "new",
		Encryption:  "rsa",
	})
	function.SetOutputExample(true)

	s.setDocFun(a, function)

	return function
}

func (s *Account) GetAndroidInfo(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	filePath := filepath.Join(s.Config.Install.Root, s.Config.Install.Android.FolderName, "info.json")
	info := &doctorModel.AppVersionHistoryEx{}
	err := info.LoadFromFile(filePath)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}
	info.Url = fmt.Sprintf("%s://%s%s/%s/%s",
		a.Schema(), r.Host, s.downloadSitePrefix, s.Config.Install.Android.FolderName, s.Config.Install.Android.FileName)

	code, err := qr.Encode(info.Url, qr.H, qr.Auto)
	if err == nil {
		var buf bytes.Buffer
		err = png.Encode(&buf, code)
		if err == nil {
			qrCode := base64.StdEncoding.EncodeToString(buf.Bytes())

			info.UrlCode = fmt.Sprintf("data:image/png;base64,%s", qrCode)
		}
	}

	a.Success(info)
}

func (s *Account) GetAndroidInfoDoc(a document.Assistant) document.Function {
	now := types.Time(time.Now())

	function := a.CreateFunction("获取安卓安装包信息")
	function.SetNote("获取安卓安装包信息，成功时返回当前最新版本的信息")
	function.SetOutputExample(&doctorModel.AppVersionHistoryEx{
		AppVersionHistory: doctorModel.AppVersionHistory{
			SerialNo:       42,
			VersionCode:    1,
			VersionName:    ":1.0.1.1",
			UpdateDate:     &now,
			UpdateContent:  "修改Bug",
			IsForced:       0,
			CompMinVersion: 1,
		},
		Url: "http://tlcbme_project.vico-lab.com/download/android/hypertension.apk",
	})
	function.SetContentType("")

	s.setDocFun(a, function)

	return function
}
