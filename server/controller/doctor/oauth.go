package doctor

import (
	"tlcbme_project/business/doctor"
	"tlcbme_project/data/model"
	"tlcbme_project/data/model/oauth"
	"tlcbme_project/database/memory"
	"tlcbme_project/errors"
	"tlcbme_project/server/config"
	"github.com/ktpswjz/httpserver/document"
	"github.com/ktpswjz/httpserver/router"
	"github.com/ktpswjz/httpserver/types"
	"net/http"
	"time"
)

type Oauth struct {
	doc
	Logined func(userID uint64, token *model.Token)
}

func NewOauth(cfg *config.Config, log types.Log, dbToken memory.Token, doctorBusiness doctor.Business, Logined func(userID uint64, token *model.Token)) *Oauth {
	instance := &Oauth{}
	instance.SetLog(log)
	instance.Config = cfg
	instance.DbToken = dbToken
	instance.doctorBusiness = doctorBusiness
	instance.Logined = Logined
	return instance
}

func (s *Oauth) setDocFun(a document.Assistant, fun document.Function) {
	catalog := s.rootCatalog(a).CreateChild("信息平台对接", "信息平台对接相关借口")
	catalog.SetFunction(fun)
}

func (s *Oauth) GetOauthCode(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &oauth.Input{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InputError, err)
		return
	}
	account, be := s.doctorBusiness.Oauth().GetOauthUserInfo(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}
	if account != nil {
		now := time.Now()
		token := &model.Token{
			ID:          a.GenerateGuid(),
			UserAccount: account.UserName,
			UserID:      account.UserId,
			LoginIP:     a.RIP(),
			LoginTime:   now,
			ActiveTime:  now,
		}
		if s.Logined != nil {
			s.Logined(account.UserId, token)
		}
		err = s.DbToken.Set(token)
		if err != nil {
			a.Error(errors.Exception, err)
			return
		}

		login := &model.Login{
			Token:       token.ID,
			Account:     token.UserAccount,
			RightIDList: token.RightIDList,
			Host:        r.Host,
		}
		a.Success(login)
	} else {
		a.Success(nil)
	}
}

func (s *Oauth) GetOauthCodeDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("获取平台code")
	function.SetNote("获取平台code")
	//function.SetInputExample(&doctorModel.SmsParam{
	//	TemplateCode:  "SMS_1330248924729",
	//	TemplateParam: "{patientName:'张三'}",
	//	PhoneNumber:   "123456789",
	//})
	//function.SetOutputExample("ok")

	s.setDocFun(a, function)

	return function
}
