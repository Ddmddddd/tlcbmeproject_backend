package doctor

import (
	"tlcbme_project/business/doctor"
	"tlcbme_project/business/patient"
	"tlcbme_project/data/model"
	"tlcbme_project/database/memory"
	"tlcbme_project/errors"
	"tlcbme_project/server/config"
	"tlcbme_project/server/notify"
	"fmt"
	"github.com/gorilla/websocket"
	"github.com/ktpswjz/httpserver/document"
	"github.com/ktpswjz/httpserver/router"
	"github.com/ktpswjz/httpserver/types"
	"net/http"
	"time"
)

type Socket struct {
	doc

	upgrader       websocket.Upgrader
	notifyChannels notify.ChannelCollection
}

type NotifyData interface {
	GetData() interface{}
}

func NewSocket(cfg *config.Config, log types.Log, dbToken memory.Token, doctorBusiness doctor.Business, patientBusiness patient.Business, notifyChannels notify.ChannelCollection) *Socket {
	instance := &Socket{}
	instance.SetLog(log)
	instance.Config = cfg
	instance.DbToken = dbToken
	instance.doctorBusiness = doctorBusiness
	instance.patientBusiness = patientBusiness

	instance.upgrader = websocket.Upgrader{CheckOrigin: checkOrigin}
	instance.notifyChannels = notifyChannels

	return instance
}

func (s *Socket) setDocFun(a document.Assistant, fun document.Function) {
	catalog := s.rootCatalog(a).CreateChild("WebSocket", "WebSocket相关接口")
	catalog.SetFunction(fun)
}

func (s *Socket) NotifySubscribe(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	token, err := s.DbToken.Get(a.Token())
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}

	conn, err := s.upgrader.Upgrade(w, r, nil)
	if err != nil {
		s.LogError("notify subscribe socket connect fail:", err)
		a.Error(errors.InternalError, err)
		return
	}
	defer conn.Close()

	notifyChannel := s.notifyChannels.NewChannel()
	defer s.notifyChannels.Remove(notifyChannel)

	exited := make(chan bool, 5)
	go func(channel notify.Channel, conn *websocket.Conn, token *model.Token) {
		alertCount := newSocketAlertCount(token, s.doctorBusiness.Patient())
		auditCount := newSocketAuditCount(token, s.doctorBusiness.Patient())
		followupCount := newSocketFollowupCount(token, s.doctorBusiness.Followup())
		chatUnReadCount := newSocketChatUnReadCount(token, s.doctorBusiness.Chat())
		for {
			select {
			case <-exited:
				return
			case msg, ok := <-channel.ReadEx():
				if !ok {
					return
				}
				if msg.BusinessID != notify.BusinessDoctor {
					continue
				}

				switch msg.NotifyID {
				case notify.DoctorNewPatientAlert:
					msg.Data = alertCount.GetData()
				case notify.DoctorAuditPatient:
					msg.Data = auditCount.GetData()
				case notify.DoctorUpdateFollowup:
					msg.Data = followupCount.GetData()
				case notify.DoctorCountUnReadMessage:
					msg.Data = chatUnReadCount.GetData()
				}

				conn.WriteJSON(msg)
			}
		}
	}(notifyChannel, conn, token)

	// init
	notifyChannel.Write(&notify.Message{
		BusinessID: notify.BusinessDoctor,
		NotifyID:   notify.DoctorNewPatientAlert,
		Time:       types.Time(time.Now()),
	})
	notifyChannel.Write(&notify.Message{
		BusinessID: notify.BusinessDoctor,
		NotifyID:   notify.DoctorAuditPatient,
		Time:       types.Time(time.Now()),
	})
	notifyChannel.Write(&notify.Message{
		BusinessID: notify.BusinessDoctor,
		NotifyID:   notify.DoctorUpdateFollowup,
		Time:       types.Time(time.Now()),
	})
	notifyChannel.Write(&notify.Message{
		BusinessID: notify.BusinessDoctor,
		NotifyID:   notify.DoctorCountUnReadMessage,
		Time:       types.Time(time.Now()),
	})

	// reset token time
	expirationMinutes := s.Config.Site.Doctor.Api.Token.Expiration
	if expirationMinutes > 0 {
		checkInterval := time.Minute*time.Duration(expirationMinutes) - 30*time.Second
		checkTimer := time.NewTimer(checkInterval)

		go func() {
			// 异常处理
			defer func() {
				if err := recover(); err != nil {
					s.LogError("NotifySubscribe reset token time", err)
				}
			}()

			for {
				select {
				case <-exited:
					return
				case <-checkTimer.C:
					checkTimer.Reset(checkInterval)
					token.ActiveTime = time.Now()
				}
			}
		}()
	}

	for {
		msgType, msgContent, err := conn.ReadMessage()
		if err != nil {
			s.LogError("notify subscribe read message fail:", err)
			break
		}
		if msgType == websocket.CloseMessage {
			break
		}

		if msgType == websocket.TextMessage {
			fmt.Println(msgContent)

			conn.WriteMessage(msgType, []byte("srv: "+string(msgContent)))
		}
	}

	exited <- true
	exited <- true
	a.Success(true)
}

func (s *Socket) NotifySubscribeDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("通知订阅")
	function.SetNote("订阅并接收系统推送的通知，该接口保持阻塞至连接关闭")
	function.SetContentType("")

	s.setDocFun(a, function)

	return function
}

func checkOrigin(r *http.Request) bool {
	if r != nil {
	}
	return true
}
