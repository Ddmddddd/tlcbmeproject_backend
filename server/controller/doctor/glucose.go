package doctor

import (
	"tlcbme_project/business/doctor"
	"tlcbme_project/business/patient"
	"tlcbme_project/data/model"
	doctorModel "tlcbme_project/data/model/doctor"
	"tlcbme_project/database/memory"
	"tlcbme_project/errors"
	"tlcbme_project/server/config"
	"github.com/ktpswjz/httpserver/document"
	"github.com/ktpswjz/httpserver/router"
	"github.com/ktpswjz/httpserver/types"
	"net/http"
	"time"
)

type Glucose struct {
	doc
}

func NewGlucose(cfg *config.Config, log types.Log, dbToken memory.Token, doctorBusiness doctor.Business, patientBusiness patient.Business) *Glucose {
	instance := &Glucose{}
	instance.SetLog(log)
	instance.Config = cfg
	instance.DbToken = dbToken
	instance.doctorBusiness = doctorBusiness
	instance.patientBusiness = patientBusiness

	return instance
}

func (s *Glucose) setDocFun(a document.Assistant, fun document.Function) {
	catalog := s.rootCatalog(a).CreateChild("血糖信息", "血糖信息相关接口")
	catalog.SetFunction(fun)
}

func (s *Glucose) CreateRecord(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.BloodGlucoseRecordCreateEx{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InputError, err)
		return
	}

	data, be := s.patientBusiness.Data().CreateBloodGlucoseRecord(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	//go func(sno uint64) {
	//	s.patientBusiness.Data().UploadBloodGlucoseData(sno)
	//}(data)

	a.Success(data)
}

func (s *Glucose) CreateRecordDoc(a document.Assistant) document.Function {
	patientID := uint64(100)
	now := time.Now()
	measureDateTime := types.Time(time.Date(now.Year(), now.Month(), now.Day(), now.Hour(), now.Minute(), 0, 0, now.Location()))

	function := a.CreateFunction("新建患者血糖记录")
	function.SetNote("新建患者血糖记录，成功时返回记录序列号")
	function.SetInputExample(&doctorModel.BloodGlucoseRecordCreateEx{
		PatientID: patientID,
		BloodGlucoseRecordCreate: doctorModel.BloodGlucoseRecordCreate{
			MeasureDateTime: &measureDateTime,
			BloodGlucose:    6.3,
			BloodType:       "毛细血管血",
			TimePoint:       "早餐后2h",
		},
	})
	function.SetOutputExample(uint64(123))
	s.setDocFun(a, function)

	return function
}

func (s *Glucose) SearchRecordTrendList(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.BloodGlucoseRecordFilter{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InputError, err)
		return
	}

	data, be := s.doctorBusiness.BloodGlucose().SearchRecordTrendList(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Glucose) SearchRecordTrendListDoc(a document.Assistant) document.Function {
	patientID := uint64(100)
	now := time.Now()
	measureDateTime := types.Time(time.Date(now.Year(), now.Month(), now.Day(), now.Hour(), now.Minute(), 0, 0, now.Location()))

	function := a.CreateFunction("获取患者血糖趋势图数据列表")
	function.SetNote("获取患者血糖趋势图数据列表")
	function.SetInputExample(&doctorModel.BloodGlucoseRecordFilter{
		PatientID:      &patientID,
		MeasureDateEnd: &measureDateTime,
	})
	function.SetOutputExample([]*doctorModel.BloodGlucoseRecordTrend{
		{
			BloodType:       "毛细血管血",
			TimePoint:       "早餐后2h",
			BloodGlucose:    7.8,
			MeasureDateTime: &measureDateTime,
		},
	})
	s.setDocFun(a, function)

	return function
}

func (s *Glucose) SearchRecordTableList(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	filter := &doctorModel.BloodGlucoseRecordFilter{}
	argument := &model.PageFilter{
		Page: model.Page{
			Index: 1,
			Size:  15,
		},
		Filter: filter,
	}
	a.GetArgument(r, argument)

	result, be := s.doctorBusiness.BloodGlucose().SearchRecordTableList(argument.Index, argument.Size, filter)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(result)
}

func (s *Glucose) SearchRecordTableListDoc(a document.Assistant) document.Function {
	now := types.Time(time.Now())
	patientID := uint64(1)
	heart := uint64(95)

	function := a.CreateFunction("获取患者血压表格数据")
	function.SetNote("获取患者血压表格数据")
	function.SetInputExample(&model.PageFilter{
		Page: model.Page{
			Index: 1,
			Size:  15,
		},
		Filter: &doctorModel.BloodGlucoseRecordDataFilterEx{
			PatientID: patientID,
		},
	})
	function.SetOutputExample(&model.PageResult{
		Page: model.Page{
			Index: 1,
			Size:  5,
		},
		Total: 100,
		Count: 7,
		Data: []*doctorModel.BloodPressureRecordData{
			{
				SystolicPressure:  120,
				DiastolicPressure: 90,
				HeartRate:         &heart,
				MeasureDateTime:   &now,
			},
		},
	})

	s.setDocFun(a, function)

	return function
}
