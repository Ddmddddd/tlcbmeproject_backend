package doctor

import (
	"tlcbme_project/business/doctor/api"
	"tlcbme_project/data/model"
	"tlcbme_project/data/model/doctor"
)

type socketAuditCount struct {
	filter  *doctor.ManagementApplicationReviewFilter
	patient api.Patient
}

func newSocketAuditCount(token *model.Token, patient api.Patient) NotifyData {
	instance := &socketAuditCount{
		filter:  &doctor.ManagementApplicationReviewFilter{},
		patient: patient,
	}

	instance.filter.Statuses = make([]uint64, 1)
	instance.filter.Statuses[0] = 0
	instance.filter.VisitOrgCode = token.OrgCode
	instance.filter.HealthManagerID = &token.UserID

	return instance
}

func (s *socketAuditCount) GetData() interface{} {
	data, err := s.patient.SearchAuditCount(s.filter)
	if err != nil {
		return 0
	}

	return data
}
