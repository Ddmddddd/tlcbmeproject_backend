package doctor

import (
	"tlcbme_project/server/controller"
	"github.com/ktpswjz/httpserver/document"
)

type Enum struct {
	doc
	controller.Enum
}

func NewEnum() *Enum {
	instance := &Enum{}
	instance.GetCatalog = instance.getCatalog

	return instance
}

func (s *Enum) getCatalog(a document.Assistant) document.Catalog {
	catalog := s.rootCatalog(a).CreateChild("枚举信息", "枚举值相关接口")
	return catalog
}
