package doctor

import (
	"tlcbme_project/business/doctor/api"
	"tlcbme_project/data/enum"
	"tlcbme_project/data/model"
	"tlcbme_project/data/model/doctor"
)

type socketAlertCount struct {
	filters []*doctor.AlertRecordFilterEx
	patient api.Patient
}

func newSocketAlertCount(token *model.Token, patient api.Patient) NotifyData {
	instance := &socketAlertCount{
		filters: make([]*doctor.AlertRecordFilterEx, 0),
		patient: patient,
	}

	if token.UserType == enum.DoctorUserTypes.Doctor().Key {
		filter := &doctor.AlertRecordFilterEx{}
		filter.DoctorID = &token.UserID
		filter.Statuses = make([]uint64, 1)
		filter.Statuses[0] = 0
		instance.filters = append(instance.filters, filter)
	} else if token.UserType == enum.DoctorUserTypes.HealthManager().Key {
		filter := &doctor.AlertRecordFilterEx{}
		filter.HealthManagerID = &token.UserID
		filter.Statuses = make([]uint64, 1)
		filter.Statuses[0] = 0
		instance.filters = append(instance.filters, filter)
	} else if token.UserType == enum.DoctorUserTypes.DoctorAndHealthManager().Key {
		filter := &doctor.AlertRecordFilterEx{}
		filter.DoctorID = &token.UserID
		filter.Statuses = make([]uint64, 1)
		filter.Statuses[0] = 0
		instance.filters = append(instance.filters, filter)

		filter2 := &doctor.AlertRecordFilterEx{}
		filter2.HealthManagerID = &token.UserID
		filter2.Statuses = make([]uint64, 1)
		filter2.Statuses[0] = 0
		instance.filters = append(instance.filters, filter2)
	}

	return instance
}

func (s *socketAlertCount) GetData() interface{} {
	data, err := s.patient.CountAlert(s.filters)
	if err != nil {
		return 0
	}

	return data
}
