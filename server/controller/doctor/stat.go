package doctor

import (
	"tlcbme_project/business/doctor"
	"tlcbme_project/data/model/stat"
	"tlcbme_project/database/memory"
	"tlcbme_project/errors"
	"tlcbme_project/server/config"
	"github.com/ktpswjz/httpserver/document"
	"github.com/ktpswjz/httpserver/router"
	"github.com/ktpswjz/httpserver/types"
	"net/http"
)

type Stat struct {
	doc
}

func NewStat(cfg *config.Config, log types.Log, dbToken memory.Token, doctorBusiness doctor.Business) *Stat {
	instance := &Stat{}
	instance.SetLog(log)
	instance.Config = cfg
	instance.DbToken = dbToken
	instance.doctorBusiness = doctorBusiness

	return instance
}

func (s *Stat) setDocFun(a document.Assistant, fun document.Function) {
	catalog := s.rootCatalog(a).CreateChild("数据统计", "数据统计相关接口")
	catalog.SetFunction(fun)
}

func (s *Stat) GetPatientManageCount(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &stat.StatInput{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InputError, err)
		return
	}
	token, err := s.DbToken.Get(a.Token())
	if err != nil {
		a.Error(errors.InternalError, err)
	}
	argument.OrgCode = token.OrgCode
	data, be := s.doctorBusiness.Stat().GetPatientManageCount(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}
	a.Success(data)
}

func (s *Stat) GetPatientManageCountDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("获取病人管理数量统计信息")
	function.SetNote("获取病人管理数量统计信息")
	function.SetInputExample(&stat.StatInput{
		Mode: 1,
	})
	function.SetOutputExample("ok")

	s.setDocFun(a, function)

	return function
}

func (s *Stat) GetBloodPressureLevelCount(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &stat.StatInput{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InputError, err)
		return
	}
	token, err := s.DbToken.Get(a.Token())
	if err != nil {
		a.Error(errors.InternalError, err)
	}
	argument.OrgCode = token.OrgCode
	data, be := s.doctorBusiness.Stat().GetBloodPressureLevelCount(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}
	a.Success(data)
}

func (s *Stat) GetBloodPressureLevelCountDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("获取高血压病人管理等级统计信息")
	function.SetNote("获取高血压病人管理等级统计信息")
	function.SetInputExample(&stat.StatInput{
		Mode: 1,
	})
	function.SetOutputExample("ok")

	s.setDocFun(a, function)

	return function
}

func (s *Stat) GetBloodGlucoseLevelCount(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &stat.StatInput{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InputError, err)
		return
	}
	token, err := s.DbToken.Get(a.Token())
	if err != nil {
		a.Error(errors.InternalError, err)
	}
	argument.OrgCode = token.OrgCode
	data, be := s.doctorBusiness.Stat().GetBloodGlucoseLevelCount(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}
	a.Success(data)
}

func (s *Stat) GetBloodGlucoseLevelCountDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("获取糖尿病病人管理等级统计信息")
	function.SetNote("获取糖尿病病人管理等级统计信息")
	function.SetInputExample(&stat.StatInput{
		Mode: 1,
	})
	function.SetOutputExample("ok")

	s.setDocFun(a, function)

	return function
}

func (s *Stat) GetBloodPressureCount(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &stat.StatInput{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InputError, err)
		return
	}
	token, err := s.DbToken.Get(a.Token())
	if err != nil {
		a.Error(errors.InternalError, err)
	}
	argument.OrgCode = token.OrgCode
	data, be := s.doctorBusiness.Stat().GetBloodPressureCount(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}
	a.Success(data)
}

func (s *Stat) GetBloodPressureCountDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("获取血压提交统计信息")
	function.SetNote("获取血压提交统计信息")
	function.SetInputExample(&stat.StatInput{
		Mode: 1,
	})
	function.SetOutputExample("ok")

	s.setDocFun(a, function)

	return function
}

func (s *Stat) GetBloodPressureRate(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &stat.StatInput{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InputError, err)
		return
	}
	token, err := s.DbToken.Get(a.Token())
	if err != nil {
		a.Error(errors.InternalError, err)
	}
	argument.OrgCode = token.OrgCode
	data, be := s.doctorBusiness.Stat().GetBloodPressureRate(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}
	a.Success(data)
}

func (s *Stat) GetBloodPressureRateDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("获取血压达标率统计信息")
	function.SetNote("获取血压达标率统计信息")
	function.SetInputExample(&stat.StatInput{
		Mode: 1,
	})
	function.SetOutputExample("ok")

	s.setDocFun(a, function)

	return function
}

func (s *Stat) GetBloodGlucoseCount(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &stat.StatInput{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InputError, err)
		return
	}
	token, err := s.DbToken.Get(a.Token())
	if err != nil {
		a.Error(errors.InternalError, err)
	}
	argument.OrgCode = token.OrgCode
	data, be := s.doctorBusiness.Stat().GetBloodGlucoseCount(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}
	a.Success(data)
}

func (s *Stat) GetBloodGlucoseCountDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("获取血糖提交数据统计信息")
	function.SetNote("获取血糖提交数据统计信息")
	function.SetInputExample(&stat.StatInput{
		Mode: 1,
	})
	function.SetOutputExample("ok")

	s.setDocFun(a, function)

	return function
}

func (s *Stat) GetBloodGlucoseRate(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &stat.StatInput{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InputError, err)
		return
	}
	token, err := s.DbToken.Get(a.Token())
	if err != nil {
		a.Error(errors.InternalError, err)
	}
	argument.OrgCode = token.OrgCode
	data, be := s.doctorBusiness.Stat().GetBloodGlucoseRate(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}
	a.Success(data)
}

func (s *Stat) GetBloodGlucoseRateDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("获取血糖达标率统计信息")
	function.SetNote("获取血糖达标率统计信息")
	function.SetInputExample(&stat.StatInput{
		Mode: 1,
	})
	function.SetOutputExample("ok")

	s.setDocFun(a, function)

	return function
}

func (s *Stat) GetOtherData(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &stat.StatInput{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InputError, err)
		return
	}
	token, err := s.DbToken.Get(a.Token())
	if err != nil {
		a.Error(errors.InternalError, err)
	}
	argument.OrgCode = token.OrgCode
	data, be := s.doctorBusiness.Stat().GetOtherData(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}
	a.Success(data)
}

func (s *Stat) GetOtherDataDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("获取其他数据统计信息")
	function.SetNote("获取其他数据统计信息")
	function.SetInputExample(&stat.StatInput{
		Mode: 1,
	})
	function.SetOutputExample("ok")

	s.setDocFun(a, function)

	return function
}

func (s *Stat) GetFollowupData(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &stat.StatInput{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InputError, err)
		return
	}
	token, err := s.DbToken.Get(a.Token())
	if err != nil {
		a.Error(errors.InternalError, err)
	}
	argument.OrgCode = token.OrgCode
	data, be := s.doctorBusiness.Stat().GetFollowupData(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}
	a.Success(data)
}

func (s *Stat) GetFollowupDataDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("获取随访统计信息")
	function.SetNote("获取随访统计信息")
	function.SetInputExample(&stat.StatInput{
		Mode: 1,
	})
	function.SetOutputExample("ok")

	s.setDocFun(a, function)

	return function
}

func (s *Stat) GetAlertData(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &stat.StatInput{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InputError, err)
		return
	}
	token, err := s.DbToken.Get(a.Token())
	if err != nil {
		a.Error(errors.InternalError, err)
	}
	argument.OrgCode = token.OrgCode
	data, be := s.doctorBusiness.Stat().GetAlertData(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}
	a.Success(data)
}

func (s *Stat) GetAlertDataDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("获取预警处理信息")
	function.SetNote("获取预警处理信息")
	function.SetInputExample(&stat.StatInput{
		Mode: 1,
	})
	function.SetOutputExample("ok")

	s.setDocFun(a, function)

	return function
}

func (s *Stat) GetBloodPressureRiskData(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &stat.StatInput{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InputError, err)
		return
	}
	token, err := s.DbToken.Get(a.Token())
	if err != nil {
		a.Error(errors.InternalError, err)
	}
	argument.OrgCode = token.OrgCode
	data, be := s.doctorBusiness.Stat().GetBloodPressureRiskData(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}
	a.Success(data)
}

func (s *Stat) GetBloodPressureRiskDataDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("获取预警处理信息")
	function.SetNote("获取预警处理信息")
	function.SetInputExample(&stat.StatInput{
		Mode: 1,
	})
	function.SetOutputExample("ok")

	s.setDocFun(a, function)

	return function
}

func (s *Stat) GetBloodGlucoseRiskData(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &stat.StatInput{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InputError, err)
		return
	}
	token, err := s.DbToken.Get(a.Token())
	if err != nil {
		a.Error(errors.InternalError, err)
	}
	argument.OrgCode = token.OrgCode
	data, be := s.doctorBusiness.Stat().GetBloodGlucoseRiskData(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}
	a.Success(data)
}

func (s *Stat) GetBloodGlucoseRiskDataDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("获取预警处理信息")
	function.SetNote("获取预警处理信息")
	function.SetInputExample(&stat.StatInput{
		Mode: 1,
	})
	function.SetOutputExample("ok")

	s.setDocFun(a, function)

	return function
}
