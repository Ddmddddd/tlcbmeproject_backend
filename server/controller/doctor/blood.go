package doctor

import (
	"tlcbme_project/business/doctor"
	"tlcbme_project/business/patient"
	"tlcbme_project/data/model"
	doctorModel "tlcbme_project/data/model/doctor"
	"tlcbme_project/database/memory"
	"tlcbme_project/errors"
	"tlcbme_project/server/config"
	"github.com/ktpswjz/httpserver/document"
	"github.com/ktpswjz/httpserver/router"
	"github.com/ktpswjz/httpserver/types"
	"net/http"
	"time"
)

type Blood struct {
	doc
}

func NewBlood(cfg *config.Config, log types.Log, dbToken memory.Token, doctorBusiness doctor.Business, patientBusiness patient.Business) *Blood {
	instance := &Blood{}
	instance.SetLog(log)
	instance.Config = cfg
	instance.DbToken = dbToken
	instance.doctorBusiness = doctorBusiness
	instance.patientBusiness = patientBusiness

	return instance
}

func (s *Blood) setDocFun(a document.Assistant, fun document.Function) {
	catalog := s.rootCatalog(a).CreateChild("血压信息", "血压信息相关接口")
	catalog.SetFunction(fun)
}

func (s *Blood) SearchTrendSingleList(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.BloodPressureRecordTrendFilter{
		MaxCount: 15,
	}
	a.GetArgument(r, argument)

	data, be := s.doctorBusiness.BloodPressure().SearchTrendSingleList(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Blood) SearchTrendSingleListDoc(a document.Assistant) document.Function {
	patientID := uint64(100)
	heartRate := uint64(65)
	now := time.Now()
	dateStart := types.Time(now.Add(-30 * 24 * time.Hour))
	date := now.Add(-5 * 24 * time.Hour)
	measureDateTime := types.Time(time.Date(date.Year(), date.Month(), date.Day(), date.Hour(), date.Minute(), 0, 0, date.Location()))

	function := a.CreateFunction("获取单次血压数据列表")
	function.SetNote("获取单次血压数据列表，指定时间前后个15次")
	function.SetInputExample(&doctorModel.BloodPressureRecordTrendFilter{
		PatientID:       patientID,
		MeasureDateTime: &measureDateTime,
		MaxCount:        15,
	})
	function.SetOutputExample(&doctorModel.BloodPressureRecordTrendStat{
		Records: []*doctorModel.BloodPressureRecordTrend{
			{
				SystolicPressure:  110,
				DiastolicPressure: 78,
				HeartRate:         &heartRate,
				MeasureDateTime:   &measureDateTime,
			},
		},
		TimeStart: dateStart,
		TimeEnd:   measureDateTime,
	})
	s.setDocFun(a, function)

	return function
}

func (s *Blood) SearchTrendSingleRange(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.BloodPressureRecordDataFilterEx{}
	a.GetArgument(r, argument)

	data, be := s.doctorBusiness.BloodPressure().SearchTrendSingleRange(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Blood) SearchTrendSingleRangeDoc(a document.Assistant) document.Function {
	patientID := uint64(100)
	function := a.CreateFunction("获取血压数据")
	function.SetNote("获取血压数据列表，指定时间范围")
	function.SetInputExample(&doctorModel.BloodPressureRecordDataFilterEx{
		PatientID: patientID,
	})
	function.SetOutputExample(&doctorModel.BloodPressureRecordTrendStat{
		Records: []*doctorModel.BloodPressureRecordTrend{
			{
				SystolicPressure:  110,
				DiastolicPressure: 78,
			},
		},
	})
	s.setDocFun(a, function)

	return function
}

func (s *Blood) SearchTableSingleRange(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	filter := &doctorModel.BloodPressureRecordDataFilterEx{}
	argument := &model.PageFilter{
		Page: model.Page{
			Index: 1,
			Size:  15,
		},
		Filter: filter,
	}
	a.GetArgument(r, argument)

	result, be := s.doctorBusiness.BloodPressure().SearchTableSingleRange(argument.Index, argument.Size, filter)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(result)
}

func (s *Blood) SearchTableSingleRangeDoc(a document.Assistant) document.Function {
	now := types.Time(time.Now())
	patientID := uint64(1)
	heart := uint64(95)

	function := a.CreateFunction("获取患者血压表格数据")
	function.SetNote("获取患者血压表格数据")
	function.SetInputExample(&model.PageFilter{
		Page: model.Page{
			Index: 1,
			Size:  15,
		},
		Filter: &doctorModel.BloodGlucoseRecordDataFilterEx{
			PatientID: patientID,
		},
	})
	function.SetOutputExample(&model.PageResult{
		Page: model.Page{
			Index: 1,
			Size:  5,
		},
		Total: 100,
		Count: 7,
		Data: []*doctorModel.BloodPressureRecordData{
			{
				SystolicPressure:  120,
				DiastolicPressure: 90,
				HeartRate:         &heart,
				MeasureDateTime:   &now,
			},
		},
	})

	s.setDocFun(a, function)

	return function
}

func (s *Blood) SearchRecordLastOne(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.BloodPressureRecordPatientFilter{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InputError, err)
		return
	}

	data, be := s.doctorBusiness.BloodPressure().SearchRecordLastOne(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Blood) SearchRecordLastOneDoc(a document.Assistant) document.Function {
	patientID := uint64(100)
	heartRate := uint64(65)
	now := types.Time(time.Now())

	function := a.CreateFunction("获取患者最后测量的血压")
	function.SetNote("获取患者最后测量的血压")
	function.SetInputExample(&doctorModel.BloodPressureRecordPatientFilter{
		PatientID: patientID,
	})
	function.SetOutputExample(&doctorModel.BloodPressureRecord{
		SerialNo:          324,
		PatientID:         patientID,
		SystolicPressure:  110,
		DiastolicPressure: 78,
		HeartRate:         &heartRate,
		MeasureDateTime:   &now,
	})
	s.setDocFun(a, function)

	return function
}
