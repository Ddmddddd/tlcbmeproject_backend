package doctor

import (
	"tlcbme_project/business/doctor"
	doctorModel "tlcbme_project/data/model/doctor"
	"tlcbme_project/database/memory"
	"tlcbme_project/errors"
	"tlcbme_project/server/config"
	"fmt"
	"github.com/ktpswjz/httpserver/document"
	"github.com/ktpswjz/httpserver/router"
	"github.com/ktpswjz/httpserver/types"
	"net/http"
	"time"
)

type Sms struct {
	doc
}

func NewSms(cfg *config.Config, log types.Log, dbToken memory.Token, doctorBusiness doctor.Business) *Sms {
	instance := &Sms{}
	instance.SetLog(log)
	instance.Config = cfg
	instance.DbToken = dbToken
	instance.doctorBusiness = doctorBusiness

	return instance
}

func (s *Sms) setDocFun(a document.Assistant, fun document.Function) {
	catalog := s.rootCatalog(a).CreateChild("短信管理信息", "短信管理信息相关接口")
	catalog.SetFunction(fun)
}

func (s *Sms) SendSms(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.SmsParam{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InputError, err)
		return
	}
	token, err := s.DbToken.Get(a.Token())
	if err != nil {
		a.Error(errors.InternalError, err)
	}
	now := types.Time(time.Now())
	argument.OutId = fmt.Sprint(a.RID())
	argument.SignatureNonce = a.GenerateGuid()
	argument.Sender = token.UserID
	argument.SendDateTime = &now
	data, be := s.doctorBusiness.Sms().SendSms(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Sms) SendSmsDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("发送短信")
	function.SetNote("发送短信")
	function.SetInputExample(&doctorModel.SmsParam{
		TemplateCode:  "SMS_1330248924729",
		TemplateParam: "{patientName:'张三'}",
		PhoneNumber:   "123456789",
	})
	function.SetOutputExample("ok")

	s.setDocFun(a, function)

	return function
}

func (s *Sms) GetSmsRecordList(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.SmsSendRecordFilter{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InputError, err)
		return
	}

	data, be := s.doctorBusiness.Sms().GetSmsRecordList(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Sms) GetSmsRecordListDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("获得最近三个月的短信发送记录")
	function.SetNote("获得最近三个月的短信发送记录")
	typed := uint64(1)
	id := uint64(123456)
	function.SetInputExample(doctorModel.SmsSendRecordFilter{
		PatientID: &id,
	})
	function.SetOutputExample([]doctorModel.SmsSendRecord{
		{
			TemplateCode:     "SMS_1330248924729",
			ReceiverPhoneNum: "123456789",
			TemplateType:     &typed,
			SerialNo:         1,
			SmsContent:       "",
		},
	})

	s.setDocFun(a, function)

	return function
}
