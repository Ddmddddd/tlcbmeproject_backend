package doctor

import (
	"tlcbme_project/business/doctor"
	"tlcbme_project/business/patient"
	"tlcbme_project/data/model"
	doctorModel "tlcbme_project/data/model/doctor"
	"tlcbme_project/database/memory"
	"tlcbme_project/errors"
	"tlcbme_project/server/config"
	"github.com/ktpswjz/httpserver/document"
	"github.com/ktpswjz/httpserver/router"
	"github.com/ktpswjz/httpserver/types"
	"net/http"
	"time"
)

type Followup struct {
	doc
}

func NewFollowup(cfg *config.Config, log types.Log, dbToken memory.Token, doctorBusiness doctor.Business, patientBusiness patient.Business) *Followup {
	instance := &Followup{}
	instance.SetLog(log)
	instance.Config = cfg
	instance.DbToken = dbToken
	instance.doctorBusiness = doctorBusiness
	instance.patientBusiness = patientBusiness

	return instance
}

func (s *Followup) setDocFun(a document.Assistant, fun document.Function) {
	catalog := s.rootCatalog(a).CreateChild("随访管理", "随访信息相关接口")
	catalog.SetFunction(fun)
}

func (s *Followup) SearchFollowupPlanPage(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	token, err := s.DbToken.Get(a.Token())
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}
	if token == nil {
		a.Error(errors.AuthNoToken)
		return
	}

	filter := &doctorModel.ViewFollowupPlanFilterEx{}
	argument := &model.PageFilter{
		Page: model.Page{
			Index: 1,
			Size:  15,
		},
		Filter: filter,
	}
	a.GetArgument(r, argument)
	filter.OrgCode = token.OrgCode
	filter.DoctorID = &token.UserID
	filter.HealthManagerID = &token.UserID

	result, be := s.doctorBusiness.Followup().SearchFollowupPlanPage(argument.Index, argument.Size, filter)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(result)
}

func (s *Followup) SearchFollowupPlanPageDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("获取随访计划信息（分页）")
	function.SetNote("获取随访计划信息列表")
	function.SetInputExample(&model.PageFilter{
		Page: model.Page{
			Index: 1,
			Size:  15,
		},
		Filter: &doctorModel.ViewFollowupPlanFilter{
			ManageStatuses: []uint64{0, 1, 2},
		},
	})
	function.SetOutputExample(&model.PageResult{
		Page: model.Page{
			Index: 1,
			Size:  15,
		},
		Total: 100,
		Count: 7,
		Data: []*doctorModel.ViewFollowupPlanEx{
			{
				ViewFollowupPlan: doctorModel.ViewFollowupPlan{
					SerialNo:    1808080848450270,
					PatientID:   20181011,
					PatientName: "Tom",
				},
				FollowupDays: 60,
			},
		},
		Extend: &doctorModel.ViewFollowupPlanCount{
			Total:    100,
			Waiting:  7,
			Finished: 92,
		},
	})

	s.setDocFun(a, function)

	return function
}

func (s *Followup) IgnoreFollowupPlan(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.FollowupPlanIgnore{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InputError, err)
		return
	}

	result, be := s.doctorBusiness.Followup().IgnorePlan(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(result)
}

func (s *Followup) IgnoreFollowupPlanDoc(a document.Assistant) document.Function {
	serialNo := uint64(8)
	followupType := "高血压常规随访"

	function := a.CreateFunction("忽略随访计划")
	function.SetNote("忽略随访计划")
	function.SetInputExample(&doctorModel.FollowupPlanIgnore{
		SerialNo:     serialNo,
		PatientID:    87,
		FollowUpType: followupType,
		StatusMemo:   "重复计划",
	})
	function.SetOutputExample(serialNo)

	s.setDocFun(a, function)

	return function
}

func (s *Followup) SaveFollowupRecord(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	token, err := s.DbToken.Get(a.Token())
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}
	if token == nil {
		a.Error(errors.AuthNoToken)
		return
	}

	argument := &doctorModel.FollowupRecordSaveEx{}
	err = a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InputError, err)
		return
	}

	argument.DoctorID = token.UserID
	argument.DoctorName = token.UserName

	result, be := s.doctorBusiness.Followup().SaveFollowupRecord(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(result)
}

func (s *Followup) SaveFollowupRecordDoc(a document.Assistant) document.Function {
	serialNo := uint64(8)
	now := types.Time(time.Now())

	function := a.CreateFunction("保存随访记录信息")
	function.SetNote("保存随访记录信息，当指定序列号时更新当前记录，否则新增一条记录")
	function.SetInputExample(&doctorModel.FollowupRecordSaveEx{
		FollowupRecordSave: doctorModel.FollowupRecordSave{
			SerialNo:                &serialNo,
			PatientID:               87,
			FollowupType:            "三个月例行随访",
			FollowupMethod:          "电话",
			Status:                  1,
			FailureReason:           "电话停机",
			DeathTime:               &now,
			CauseOfDeath:            "车祸死亡",
			FollowupTemplateCode:    1,
			FollowupTemplateVersion: 1,
			Summary:                 "血压170/99mmHg，控制不满意，嘱患者近期复诊。",
			FollowupPlanSerialNo:    &serialNo,
			Content: struct {
				Code                uint64 `json:"code" note:"随访模板代码"`
				Version             uint64 `json:"version" note:"随访记录内容模板版本号"`
				BloodPressureStatus int    `json:"bloodPressureStatus" note:"血压状况: 0-正常, 1-异常"`
				MedicationStatus    int    `json:"medicationStatus" note:"服药状况: 0-按时, 1-未按时"`
				DietaryStatus       int    `json:"dietaryStatus" note:"饮食状况: 0-良好, 1-一般"`
				SportsStatus        int    `json:"sportsStatus" note:"运动状况: 0-良好, 1-一般"`
				Others              string `json:"others" note:"其他信息"`
			}{
				Code:                1,
				Version:             1,
				BloodPressureStatus: 0,
				MedicationStatus:    1,
				DietaryStatus:       0,
				SportsStatus:        0,
				Others:              "其他信息...",
			},
		},
		FollowupDateTime: &now,
	})
	function.SetOutputExample(serialNo)

	s.setDocFun(a, function)

	return function
}

func (s *Followup) SearchFollowupRecordPage(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	filter := &doctorModel.FollowupRecordFilter{}
	argument := &model.PageFilter{
		Page: model.Page{
			Index: 1,
			Size:  15,
		},
		Filter: filter,
	}
	a.GetArgument(r, argument)

	result, be := s.doctorBusiness.Followup().SearchFollowupRecordPage(argument.Index, argument.Size, filter)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(result)
}

func (s *Followup) SearchFollowupRecordPageDoc(a document.Assistant) document.Function {
	patientID := uint64(87)

	function := a.CreateFunction("获取随访记录信息（分页）")
	function.SetNote("获取随访记录信息列表")
	function.SetInputExample(&model.PageFilter{
		Page: model.Page{
			Index: 1,
			Size:  15,
		},
		Filter: &doctorModel.FollowupRecordFilter{
			PatientID: &patientID,
		},
	})
	function.SetOutputExample(&model.PageResult{
		Page: model.Page{
			Index: 1,
			Size:  15,
		},
		Total: 100,
		Count: 7,
		Data: []*doctorModel.FollowupRecordEx{
			{
				FollowupRecord: doctorModel.FollowupRecord{
					SerialNo:  1808080848450270,
					PatientID: patientID,
				},
			},
		},
	})

	s.setDocFun(a, function)

	return function
}
