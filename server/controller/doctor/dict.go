package doctor

import (
	"tlcbme_project/business/doctor"
	"tlcbme_project/database/memory"
	"tlcbme_project/server/config"
	"github.com/ktpswjz/httpserver/document"
	"github.com/ktpswjz/httpserver/router"
	"github.com/ktpswjz/httpserver/types"
	"net/http"
	"time"

	doctorModel "tlcbme_project/data/model/doctor"
)

type Dict struct {
	doc
}

func NewDict(cfg *config.Config, log types.Log, dbToken memory.Token, doctorBusiness doctor.Business) *Dict {
	instance := &Dict{}
	instance.SetLog(log)
	instance.Config = cfg
	instance.DbToken = dbToken
	instance.doctorBusiness = doctorBusiness

	return instance
}

func (s *Dict) setDocFun(a document.Assistant, fun document.Function) {
	catalog := s.rootCatalog(a).CreateChild("字典信息", "字典列表相关接口")
	catalog.SetFunction(fun)
}

func (s *Dict) ListDoctor(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.ViewDoctorUserDictFilter{}
	a.GetArgument(r, argument)
	orgCode := s.getLoginUserOrgCode(a)

	data, be := s.doctorBusiness.Dict().ListDoctor(argument, orgCode)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Dict) ListDoctorDoc(a document.Assistant) document.Function {
	userType := uint64(0)

	function := a.CreateFunction("获取医生用户列表")
	function.SetNote("获取当前医生用户所在机构所有用户信息")
	function.SetInputExample(&doctorModel.ViewDoctorUserDictFilter{
		UserType: &userType,
	})
	function.SetOutputExample([]*doctorModel.ViewDoctorUserDict{
		{
			UserID: 0,
			Name:   "张三",
		},
	})
	s.setDocFun(a, function)

	return function
}

func (s *Dict) ListManageClass(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.DictCodeItemFilter{}
	a.GetArgument(r, argument)

	data, be := s.doctorBusiness.Dict().ListManageClass(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Dict) ListManageClassDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("获取管理分类列表")
	function.SetNote("获取患者管理分类信息")
	function.SetInputExample(&doctorModel.DictCodeItemFilter{
		InputCode: "xy",
	})
	function.SetOutputExample([]*doctorModel.DictCodeItem{
		{
			Code: 1,
			Name: "高血压",
		},
	})
	s.setDocFun(a, function)

	return function
}

func (s *Dict) ListPatientFeature(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.DictCodeItemFilter{}
	a.GetArgument(r, argument)

	data, be := s.doctorBusiness.Dict().ListPatientFeature(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Dict) ListPatientFeatureDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("获取患者特点列表")
	function.SetNote("获取患者患者特点信息")
	function.SetInputExample(&doctorModel.DictCodeItemFilter{
		InputCode: "xy",
	})
	function.SetOutputExample([]*doctorModel.DictCodeItem{
		{
			Code: 1,
			Name: "老年人",
		},
	})
	s.setDocFun(a, function)

	return function
}

func (s *Dict) ListDictDrug(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	data, be := s.doctorBusiness.Dict().ListDrug()
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Dict) ListDictDrugDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("获取药品列表")
	function.SetNote("获取字典列表")
	function.SetInputExample(nil)
	function.SetOutputExample([]*doctorModel.DrugDict{
		{
			SerialNo:      1,
			ItemCode:      1,
			ItemName:      "硝苯地平片",
			Specification: "10mg×100片/瓶",
			Units:         "mg,片",
			Effect:        "降压",
			InputCode:     "xbdpp",
			IsValid:       1,
		},
	})

	s.setDocFun(a, function)

	return function
}

func (s *Dict) ListDictSmsParam(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	data, be := s.doctorBusiness.Dict().ListSmsParam()
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Dict) ListDictSmsParamDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("获取短信模板参数列表")
	function.SetNote("获取短信模板参数列表")
	function.SetInputExample(nil)
	function.SetOutputExample([]*doctorModel.SmsTemplateParam{
		{
			SerialNo:          1,
			TemplateParamName: "${patientName}",
			ParamDataType:     0,
			Editable:          0,
			InputCode:         "",
			IsValid:           1,
		},
	})

	s.setDocFun(a, function)

	return function
}

func (s *Dict) ListDictSmsContent(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	data, be := s.doctorBusiness.Dict().ListSmsContent()
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Dict) ListDictSmsContentDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("获取短信模板内容列表")
	function.SetNote("获取短信模板内容列表")
	function.SetInputExample(nil)
	function.SetOutputExample([]*doctorModel.SmsTemplate{
		{
			SerialNo:        1,
			TemplateType:    1,
			TemplateCode:    "SMS_185845590",
			TemplateName:    "血压偏高提醒",
			TemplateContent: "【慢病微管家】${patientName}，您好！我是您的慢病管理医生${doctorName}。您的血压偏高，一次血压偏高不能反应什么，为了医生能够更全面地了解您的血压控制情况，帮助您一起控制血压，建议您至少每周测量3-4次。如有不适随时测量，我会持续关注，并给您提供指导意见。",
			IsValid:         1,
		},
	})

	s.setDocFun(a, function)

	return function
}

func (s *Dict) GetServerTime(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	a.Success(types.Time(time.Now()))
}

func (s *Dict) GetServerTimeDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("获取服务器时间")
	function.SetNote("获取服务器时间")
	function.SetInputExample(nil)
	function.SetOutputExample(types.Time(time.Now()))

	s.setDocFun(a, function)

	return function
}
