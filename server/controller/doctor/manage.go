package doctor

import (
	"tlcbme_project/business/doctor"
	"tlcbme_project/business/patient"
	doctorModel "tlcbme_project/data/model/doctor"
	"tlcbme_project/database/memory"
	"tlcbme_project/server/config"
	"github.com/ktpswjz/httpserver/document"
	"github.com/ktpswjz/httpserver/example/webserver/server/errors"
	"github.com/ktpswjz/httpserver/router"
	"github.com/ktpswjz/httpserver/types"
	"net/http"
	"time"
)

type Manage struct {
	doc
}

func NewManage(cfg *config.Config, log types.Log, dbToken memory.Token, doctorBusiness doctor.Business, patientBusiness patient.Business) *Manage {
	instance := &Manage{}
	instance.SetLog(log)
	instance.Config = cfg
	instance.DbToken = dbToken
	instance.doctorBusiness = doctorBusiness
	instance.patientBusiness = patientBusiness

	return instance
}

func (s *Manage) setDocFun(a document.Assistant, fun document.Function) {
	catalog := s.rootCatalog(a).CreateChild("管理等级", "管理等级信息相关接口")
	catalog.SetFunction(fun)
}

func (s *Manage) SearchHtnDetailList(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.HtnManageDetailFilter{}
	a.GetArgument(r, argument)

	data, be := s.doctorBusiness.Manage().SearchHtnDetailList(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Manage) SearchHtnDetailListDoc(a document.Assistant) document.Function {
	patientID := uint64(100)
	now := time.Now()
	dateStart := types.Time(now.Add(-30 * 24 * time.Hour))

	function := a.CreateFunction("获取高血压管理详细列表")
	function.SetNote("获取高血压管理详细列表")
	function.SetInputExample(&doctorModel.HtnManageDetailFilter{
		PatientID:       &patientID,
		ManageDateStart: &dateStart,
	})
	function.SetOutputExample([]*doctorModel.HtnManageDetail{
		{
			PatientID:                patientID,
			ManageLevel:              2,
			ManageLevelStartDateTime: &dateStart,
		},
	})
	s.setDocFun(a, function)

	return function
}

func (s *Manage) SearchHtnLevelTrend(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.TrendLevelHtnFilter{}
	a.GetArgument(r, argument)

	data, be := s.doctorBusiness.Manage().SearchHtnLevelTrend(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Manage) SearchHtnLevelTrendDoc(a document.Assistant) document.Function {
	patientID := uint64(100)
	now := time.Now()
	dateStart := types.Time(now.Add(-30 * 24 * time.Hour))

	function := a.CreateFunction("获取高血压管理等级变化信息")
	function.SetNote("获取高血压管理等级变化信息")
	function.SetInputExample(&doctorModel.TrendLevelHtnFilter{
		PatientID: patientID,
		MonthAgo:  1,
	})
	function.SetOutputExample(&doctorModel.TrendLevelHtn{
		DateStart: dateStart,
		DateEnd:   types.Time(now),
		Levels: []*doctorModel.TrendLevelHtnLevel{
			{
				Level:         1,
				StartDateTime: dateStart,
			},
		},
		Followups: []*doctorModel.TrendLevelHtnFollowup{
			{
				Status:           1,
				FollowupDateTime: dateStart,
			},
		},
	})
	s.setDocFun(a, function)

	return function
}
func (s *Manage) SearchDmLevelTrend(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.TrendLevelDmFilter{}
	a.GetArgument(r, argument)

	data, be := s.doctorBusiness.Manage().SearchDmLevelTrend(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}
func (s *Manage) SearchDmLevelTrendDoc(a document.Assistant) document.Function {
	patientID := uint64(100)
	now := time.Now()
	dateStart := types.Time(now.Add(-30 * 24 * time.Hour))

	function := a.CreateFunction("获取糖尿病管理等级变化信息")
	function.SetNote("获取糖尿病管理等级变化信息")
	function.SetInputExample(&doctorModel.TrendLevelDmFilter{
		PatientID: patientID,
		MonthAgo:  1,
	})
	function.SetOutputExample(&doctorModel.TrendLevelDm{
		DateStart: dateStart,
		DateEnd:   types.Time(now),
		Levels: []*doctorModel.TrendLevelDmLevel{
			{
				Level:         1,
				StartDateTime: dateStart,
			},
		},
		Followups: []*doctorModel.TrendLevelDmFollowup{
			{
				Status:           1,
				FollowupDateTime: dateStart,
			},
		},
	})
	s.setDocFun(a, function)

	return function
}

func (s *Manage) SearchManageStatus(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.ManagedPatientIndexFilterBase{}
	a.GetArgument(r, argument)

	data, be := s.doctorBusiness.Manage().SearchManageStatus(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Manage) SearchManageStatusDoc(a document.Assistant) document.Function {
	patientID := uint64(100)
	now := types.Time(time.Now())

	function := a.CreateFunction("获取患者管理状态")
	function.SetNote("获取患者管理状态")
	function.SetInputExample(&doctorModel.ManagedPatientIndexFilterBase{
		PatientID: &patientID,
	})
	function.SetOutputExample(&doctorModel.ManagedPatientIndexStatus{
		ManageStatus:      2,
		ManageEndDateTime: &now,
		ManageEndReason:   "患者长久联系不到",
	})
	s.setDocFun(a, function)

	return function
}

func (s *Manage) TerminateManage(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.ManagedPatientIndexTerminate{}
	a.GetArgument(r, argument)
	token, err := s.DbToken.Get(a.Token())
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}
	data, be := s.doctorBusiness.Manage().TerminateManage(argument, token.UserName, token.UserID)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Manage) TerminateManageDoc(a document.Assistant) document.Function {
	patientID := uint64(100)

	function := a.CreateFunction("终止患者管理")
	function.SetNote("终止患者管理")
	function.SetInputExample(&doctorModel.ManagedPatientIndexTerminate{
		PatientID:       patientID,
		ManageEndReason: "患者长久联系不到",
	})
	function.SetOutputExample(patientID)
	s.setDocFun(a, function)

	return function
}

func (s *Manage) RecoverManage(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.ManagedPatientIndexFilterBase{}
	a.GetArgument(r, argument)

	data, be := s.doctorBusiness.Manage().RecoverManage(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Manage) RecoverManageDoc(a document.Assistant) document.Function {
	patientID := uint64(100)

	function := a.CreateFunction("恢复患者管理")
	function.SetNote("恢复患者管理")
	function.SetInputExample(&doctorModel.ManagedPatientIndexFilterBase{
		PatientID: &patientID,
	})
	function.SetOutputExample(patientID)
	s.setDocFun(a, function)

	return function
}
func (s *Manage) UpdateManagementPlan(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	token, err := s.DbToken.Get(a.Token())
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}
	if token == nil {
		a.Error(errors.AuthNoToken)
		return
	}

	argument := &doctorModel.ManagementPlanAdjust{}
	err = a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InputError, err)
		return
	}

	now := types.Time(time.Now())
	argument.LastModifyDateTime = &now
	argument.ModifierID = &token.UserID
	argument.ModifierName = token.UserName

	result, be := s.doctorBusiness.Manage().UpdateManagementPlan(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(result)
}

func (s *Manage) UpdateManagementPlanDoc(a document.Assistant) document.Function {
	serialNO := uint64(97)
	patientID := uint64(100)

	function := a.CreateFunction("更新患者管理计划")
	function.SetNote("更新患者管理计划")
	function.SetInputExample(&doctorModel.ManagementPlanAdjust{
		PatientID:          patientID,
		SerialNo:           serialNO,
		LastModifyDateTime: nil,
		ModifierID:         nil,
		ModifierName:       "",
	})
	function.SetOutputExample(serialNO)
	s.setDocFun(a, function)

	return function
}
