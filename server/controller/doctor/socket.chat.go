package doctor

import (
	"tlcbme_project/business/doctor/api"
	"tlcbme_project/data/model"
	"tlcbme_project/data/model/doctor"
)

type socketChatUnReadCount struct {
	filter *doctor.ChatUnReadMsgCountFilter
	chat   api.Chat
}

func newSocketChatUnReadCount(token *model.Token, chat api.Chat) NotifyData {
	instance := &socketChatUnReadCount{
		filter: &doctor.ChatUnReadMsgCountFilter{},
		chat:   chat,
	}

	instance.filter.MsgFlag = 0
	instance.filter.ReceiverID = token.UserID

	return instance
}

func (s *socketChatUnReadCount) GetData() interface{} {
	data, err := s.chat.CountUnReadMsg(s.filter)
	if err != nil {
		return 0
	}

	return data
}
