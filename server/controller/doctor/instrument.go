package doctor

import (
	"tlcbme_project/business/doctor"
	"tlcbme_project/business/patient"
	doctorModel "tlcbme_project/data/model/doctor"
	"tlcbme_project/database/memory"
	"tlcbme_project/errors"
	"tlcbme_project/server/config"
	"encoding/base64"
	"fmt"
	"github.com/bluesky335/IDCheck/IdNumber"
	"github.com/ktpswjz/httpserver/document"
	"github.com/ktpswjz/httpserver/router"
	"github.com/ktpswjz/httpserver/types"
	"net/http"
	"strings"
	"time"
)

type Instrument struct {
	doc
}

func NewInstrument(cfg *config.Config, log types.Log, dbToken memory.Token, doctorBusiness doctor.Business, patientBusiness patient.Business) *Instrument {
	instance := &Instrument{}
	instance.SetLog(log)
	instance.Config = cfg
	instance.DbToken = dbToken
	instance.doctorBusiness = doctorBusiness
	instance.patientBusiness = patientBusiness
	return instance
}

func (s *Instrument) setDocFun(a document.Assistant, fun document.Function) {
	catalog := s.rootCatalog(a).CreateChild("仪器对接信息", "仪器对接信息相关接口")
	catalog.SetFunction(fun)
}

func (s *Instrument) CreateBloodPressureRecord(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	if !checkAuth(r) {
		w.Header().Set("WWW-Authenticate", `Basic realm="MY REALM"`)
		w.WriteHeader(http.StatusUnauthorized)
		return
	}
	argument := &doctorModel.InstrumentBloodPressureRecord{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InputError, err)
		return
	}
	id := IdNumber.New(argument.IdentityCardNumber)
	if !id.IsValid() {
		a.Error(errors.InputError, fmt.Errorf("身份证错误"))
		return
	}
	setBaseInfo(argument, id)
	exist, userId, be := s.doctorBusiness.Instrument().ExistIDNumber(argument.IdentityCardNumber)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}
	if !exist {
		sourceForm, hospitalCode, err := s.doctorBusiness.Instrument().GetHospitalCode(argument.DeviceId)
		if err != nil {
			a.Error(err.Error(), err.Detail())
			return
		}
		if screenPatient(argument.SystolicPressure, argument.DiastolicPressure) {
			patientInfo := &doctorModel.PatientUserInfoCreateForApp{
				UserName:           argument.IdentityCardNumber,
				Sex:                argument.Sex,
				DateOfBirth:        argument.DateOfBirth,
				IdentityCardNumber: argument.IdentityCardNumber,
				Password:           "",
				SourceFrom:         sourceForm,
				Name:               argument.Name,
				HospitalCode:       hospitalCode,
			}
			userId, err = s.doctorBusiness.Instrument().CreateUser(patientInfo)
			if err != nil {
				a.Error(err.Error(), err.Detail())
				return
			}
		}
	}
	record := &doctorModel.BloodPressureRecordForAppEx{
		PatientID: userId,
		BloodPressureRecordForApp: doctorModel.BloodPressureRecordForApp{
			SystolicPressure:  argument.SystolicPressure,
			DiastolicPressure: argument.DiastolicPressure,
			HeartRate:         argument.HeartRate,
			MeasureDateTime:   argument.MeasureDateTime,
		},
	}
	s.patientBusiness.Data().CommitBloodPressureRecord(record, true)

	a.Success(userId)
}

func (s *Instrument) CreateBloodPressureRecordDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("仪器新增血压记录")
	function.SetNote("仪器新增血压记录")
	now := types.Time(time.Now())
	heart := uint64(90)
	function.SetInputExample(&doctorModel.InstrumentBloodPressureRecord{
		InstrumentPatientInfo: doctorModel.InstrumentPatientInfo{
			IdentityCardNumber: "330106200012129876",
			Name:               "张三",
			Sex:                "男",
			DateOfBirth:        &now,
		},
		SystolicPressure:  130,
		DiastolicPressure: 90,
		HeartRate:         &heart,
		MeasureDateTime:   &now,
		DeviceId:          "239120170101",
	})
	function.SetOutputExample("ok")

	s.setDocFun(a, function)

	return function
}

// 根据血压筛查患者
func screenPatient(systolicPressure, diastolicPressure uint64) bool {

	return true
}

func setBaseInfo(info *doctorModel.InstrumentBloodPressureRecord, id IdNumber.IdNumber) {
	if info.Sex == "" {
		sex, _ := id.GetGender()
		if sex == 0 {
			info.Sex = "女"
		} else {
			info.Sex = "男"
		}
	}
	if info.DateOfBirth == nil {
		date, _ := id.GetBirthday()
		time := types.Time(date)
		info.DateOfBirth = &time
	}

}

func checkAuth(r *http.Request) bool {
	s := strings.SplitN(r.Header.Get("Authorization"), " ", 2)
	if len(s) != 2 {
		return false
	}

	b, err := base64.StdEncoding.DecodeString(s[1])
	if err != nil {
		return false
	}

	pair := strings.SplitN(string(b), ":", 2)
	if len(pair) != 2 {
		return false
	}

	return pair[0] == "device" && pair[1] == "device"
}
