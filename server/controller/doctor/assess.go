package doctor

import (
	"tlcbme_project/business/doctor"
	"tlcbme_project/business/patient"
	"tlcbme_project/data/model"
	doctorModel "tlcbme_project/data/model/doctor"
	"tlcbme_project/database/memory"
	"tlcbme_project/server/config"
	"github.com/ktpswjz/httpserver/document"
	"github.com/ktpswjz/httpserver/router"
	"github.com/ktpswjz/httpserver/types"
	"net/http"
	"time"
)

type Assess struct {
	doc
}

func NewAssess(cfg *config.Config, log types.Log, dbToken memory.Token, doctorBusiness doctor.Business, patientBusiness patient.Business) *Assess {
	instance := &Assess{}
	instance.SetLog(log)
	instance.Config = cfg
	instance.DbToken = dbToken
	instance.doctorBusiness = doctorBusiness
	instance.patientBusiness = patientBusiness

	return instance
}

func (s *Assess) setDocFun(a document.Assistant, fun document.Function) {
	catalog := s.rootCatalog(a).CreateChild("评估管理", "评估管理相关接口")
	catalog.SetFunction(fun)
}

func (s *Assess) SearchRiskAssessRecordPage(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	filter := &doctorModel.RiskAssessRecordFilter{}
	argument := &model.PageFilter{
		Page: model.Page{
			Index: 1,
			Size:  15,
		},
		Filter: filter,
	}
	a.GetArgument(r, argument)

	result, be := s.doctorBusiness.RiskAssess().SearchRecordPage(argument.Index, argument.Size, filter)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(result)
}

func (s *Assess) SearchRiskAssessRecordPageDoc(a document.Assistant) document.Function {
	patientID := uint64(100)
	now := types.Time(time.Now())

	function := a.CreateFunction("获取危险评估信息（分页）")
	function.SetNote("获取危险评估信息列表")
	function.SetInputExample(&model.PageFilter{
		Page: model.Page{
			Index: 1,
			Size:  15,
		},
		Filter: &doctorModel.RiskAssessRecordFilter{
			PatientID: &patientID,
		},
	})
	function.SetOutputExample(&model.PageResult{
		Page: model.Page{
			Index: 1,
			Size:  15,
		},
		Total: 100,
		Count: 7,
		Data: []*doctorModel.RiskAssessRecordEx{
			{
				RiskAssessRecord: doctorModel.RiskAssessRecord{
					SerialNo:        1808080848450270,
					PatientID:       20181011,
					Level:           2,
					Name:            "高血压危险分层评估",
					ReceiveDateTime: &now,
				},
				LevelText: "中危",
			},
		},
	})

	s.setDocFun(a, function)

	return function
}
