package doctor

import (
	"fmt"
	"github.com/ktpswjz/httpserver/document"
	"github.com/ktpswjz/httpserver/router"
	"github.com/ktpswjz/httpserver/types"
	"net/http"
	"time"
	"tlcbme_project/business/doctor"
	"tlcbme_project/business/patient"
	"tlcbme_project/data/enum"
	"tlcbme_project/data/model"
	doctorModel "tlcbme_project/data/model/doctor"
	"tlcbme_project/data/model/manage"
	"tlcbme_project/database/memory"
	"tlcbme_project/errors"
	"tlcbme_project/server/config"
)

type Patient struct {
	doc
}

func NewPatient(cfg *config.Config, log types.Log, dbToken memory.Token, doctorBusiness doctor.Business, patientBusiness patient.Business) *Patient {
	instance := &Patient{}
	instance.SetLog(log)
	instance.Config = cfg
	instance.DbToken = dbToken
	instance.doctorBusiness = doctorBusiness
	instance.patientBusiness = patientBusiness

	return instance
}

func (s *Patient) setDocFun(a document.Assistant, fun document.Function) {
	catalog := s.rootCatalog(a).CreateChild("患者管理", "患者信息相关接口")
	catalog.SetFunction(fun)
}

func (s *Patient) CreatePatient(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.PatientUserCreateEx{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InputError, err)
		return
	}
	err = argument.Verify()
	if err != nil {
		a.Error(errors.InputInvalid, err)
		return
	}
	token, err := s.DbToken.Get(a.Token())
	if err != nil {
		a.Error(errors.InternalError, err)
	}
	argument.OrgCode = token.OrgCode
	if argument.OrgCode == "" {
		a.Error(errors.NotSupport, fmt.Errorf("当前登录用户信息不完善(缺少机构信息)"))
		return
	}
	argument.ActorID = token.UserID
	argument.ActorName = token.UserName

	if argument.UserName == "" {
		argument.UserName = fmt.Sprintf("tlcbme_project%d", a.RID())
	}
	if argument.Password == "" {
		//argument.Password = "123456"
	}

	data, be := s.doctorBusiness.Patient().CreateUser(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	if argument.DietManage != "" {
		now := types.Time(time.Now())
		expArgument := &doctorModel.ExpPatientCreate{
			PatientID:      data,
			Type:           1,
			CreateDateTime: &now,
		}
		be = s.doctorBusiness.Exp().IncludePatientInExp(expArgument, true)
		if be != nil {
			a.Error(be.Error(), be.Detail())
			return
		}
	}

	go func(userId uint64) {
		s.patientBusiness.Data().RegisterPatientAndDisease(userId)
	}(data)

	a.Success(&doctorModel.PatientUserCreateSuccess{
		UserID:   data,
		UserName: argument.UserName,
		Password: argument.Password,
	})
}

func (s *Patient) CreatePatientDoc(a document.Assistant) document.Function {
	sex := uint64(1)
	doctorID := uint64(11)
	healthManagerID := uint64(12)
	now := types.Time(time.Now())
	function := a.CreateFunction("新增患者")
	function.SetNote("新建患者用户，包括登录账号、基本信息、管理索引等")
	function.SetInputExample(&doctorModel.PatientUserCreate{
		IdentityCardNumber: "330106200012129876",
		Name:               "张三",
		Sex:                &sex,
		DateOfBirth:        &now,
		Phone:              "13812344321",
		EducationLevel:     "大学",
		JobType:            "",
		PatientFeatures: []string{
			"肥胖",
			"残疾人",
		},
		ManageClasses: []*doctorModel.ManageClass{
			{
				ItemCode: 1,
				ItemName: "高血压",
			},
			{
				ItemCode: 2,
				ItemName: "糖尿病",
			},
			{
				ItemCode: 3,
				ItemName: "慢阻肺",
			},
		},
		DoctorID:          &doctorID,
		DoctorName:        "李四",
		HealthManagerID:   &healthManagerID,
		HealthManagerName: "王五",
		UserName:          "tlcbme_project",
		Password:          "123456",
	})
	function.SetOutputExample(&doctorModel.PatientUserCreateSuccess{
		UserID:   11,
		UserName: "zs",
		Password: "123",
	})

	s.setDocFun(a, function)

	return function
}

func (s *Patient) ListManagedPatientIndexPage(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	token, err := s.DbToken.Get(a.Token())
	if err != nil {
		a.Error(errors.InternalError, err)
	}
	filters := make([]*doctorModel.ViewManagedPatientIndexListFilterEx, 0)
	filter := &doctorModel.ViewManagedPatientIndexListFilterEx{}
	argument := &model.PageFilter{
		Page: model.Page{
			Index: 1,
			Size:  15,
		},
		Filter: filter,
	}
	a.GetArgument(r, argument)
	filter.OrgCode = token.OrgCode
	// 0-医生，1-健康管理师
	if token.UserType == enum.DoctorUserTypes.Doctor().Key {
		filter.DoctorID = &token.UserID
		filters = append(filters, filter)
	} else if token.UserType == enum.DoctorUserTypes.HealthManager().Key {
		filter.HealthManagerID = &token.UserID
		filters = append(filters, filter)
	} else if token.UserType == enum.DoctorUserTypes.DoctorAndHealthManager().Key {
		filter2 := *filter
		filter3 := &filter2
		filter.DoctorID = &token.UserID
		filters = append(filters, filter)
		if len(filter.HealthManagerIDList) == 0 {
			filter3.HealthManagerID = &token.UserID
			filters = append(filters, filter3)
		}
	}

	result, be := s.doctorBusiness.Patient().ListViewManagedPatientIndexPage(argument.Index, argument.Size, filters)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(result)
}

func (s *Patient) ListManagedPatientIndexPageDoc(a document.Assistant) document.Function {
	startDaysAgo := uint64(6)

	function := a.CreateFunction("获取患者主索引信息（分页）")
	function.SetNote("获取获取患者主索引信息列表")
	function.SetInputExample(&model.PageFilter{
		Page: model.Page{
			Index: 1,
			Size:  15,
		},
		Filter: &doctorModel.ViewManagedPatientIndexListFilter{
			StartDaysAgo:   &startDaysAgo,
			ManageStatuses: []uint64{0, 1, 2},
		},
	})
	function.SetOutputExample(&model.PageResult{
		Page: model.Page{
			Index: 1,
			Size:  15,
		},
		Total: 100,
		Count: 7,
		Data: []*doctorModel.ViewManagedPatientIndexEx{
			{
				ViewManagedPatientIndex: doctorModel.ViewManagedPatientIndex{
					SerialNo:    1808080848450270,
					PatientID:   20181011,
					PatientName: "Tom",
				},
				Age:     60,
				SexText: "男",
			},
		},
	})

	s.setDocFun(a, function)

	return function
}

func (s *Patient) SearchViewManagedIndexMangeLevelCount(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	token, err := s.DbToken.Get(a.Token())
	if err != nil {
		a.Error(errors.InternalError, err)
	}

	argument := &doctorModel.ViewManagedIndexFilterEx{}
	a.GetArgument(r, argument)

	argument.OrgCode = token.OrgCode
	// 0-医生，1-健康管理师
	if token.UserType == enum.DoctorUserTypes.Doctor().Key {
		argument.DoctorID = &token.UserID
	} else if token.UserType == enum.DoctorUserTypes.HealthManager().Key {
		argument.HealthManagerID = &token.UserID
	} else if token.UserType == enum.DoctorUserTypes.DoctorAndHealthManager().Key {
		argument.HealthManagerID = &token.UserID
		argument.DoctorID = &token.UserID
	}

	result, be := s.doctorBusiness.Patient().SearchViewManagedIndexMangeLevelCount(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(result)
}

func (s *Patient) SearchViewManagedIndexMangeLevelCountDoc(a document.Assistant) document.Function {
	manageClassCode := uint64(1)

	function := a.CreateFunction("获取高血压管理分级数量")
	function.SetNote("获取高血压管理分级数量")
	function.SetInputExample(&doctorModel.ViewManagedIndexFilter{
		ManageClassCode: &manageClassCode,
	})
	function.SetOutputExample(&doctorModel.ViewManagedIndexCountManageLevel{
		Total:      0,
		Terminated: 0,
	})

	s.setDocFun(a, function)

	return function
}

func (s *Patient) SearchViewManagedIndexDmTypeCount(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	token, err := s.DbToken.Get(a.Token())
	if err != nil {
		a.Error(errors.InternalError, err)
	}

	argument := &doctorModel.ViewManagedIndexFilterEx{}
	a.GetArgument(r, argument)

	argument.OrgCode = token.OrgCode
	// 0-医生，1-健康管理师
	if token.UserType == enum.DoctorUserTypes.Doctor().Key {
		argument.DoctorID = &token.UserID
	} else if token.UserType == enum.DoctorUserTypes.HealthManager().Key {
		argument.HealthManagerID = &token.UserID
	} else if token.UserType == enum.DoctorUserTypes.DoctorAndHealthManager().Key {
		argument.HealthManagerID = &token.UserID
		argument.DoctorID = &token.UserID
	}

	result, be := s.doctorBusiness.Patient().SearchViewManagedIndexDmTypeCount(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(result)
}

func (s *Patient) SearchViewManagedIndexDmTypeCountDoc(a document.Assistant) document.Function {
	manageClassCode := uint64(2)

	function := a.CreateFunction("获取糖尿病类型数量")
	function.SetNote("获取糖尿病类型数量")
	function.SetInputExample(&doctorModel.ViewManagedIndexFilter{
		ManageClassCode: &manageClassCode,
	})
	function.SetOutputExample(&doctorModel.ViewManagedIndexCountDmType{
		Total:      0,
		Terminated: 0,
	})

	s.setDocFun(a, function)

	return function
}

func (s *Patient) SearchViewManagedIndexPage(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	token, err := s.DbToken.Get(a.Token())
	if err != nil {
		a.Error(errors.InternalError, err)
	}

	filter := &doctorModel.ViewManagedIndexFilterEx{}
	filters := make([]*doctorModel.ViewManagedIndexFilterEx, 0)
	argument := &model.PageFilter{
		Page: model.Page{
			Index: 1,
			Size:  15,
		},
		Filter: filter,
	}
	a.GetArgument(r, argument)

	filter.OrgCode = token.OrgCode
	// 0-医生，1-健康管理师
	if token.UserType == enum.DoctorUserTypes.Doctor().Key {
		filter.DoctorID = &token.UserID
		filters = append(filters, filter)
	} else if token.UserType == enum.DoctorUserTypes.HealthManager().Key {
		filter.HealthManagerID = &token.UserID
		filters = append(filters, filter)
	} else if token.UserType == enum.DoctorUserTypes.DoctorAndHealthManager().Key {
		filter2 := *filter
		filter3 := &filter2
		filter.HealthManagerID = &token.UserID
		filters = append(filters, filter)
		filter3.DoctorID = &token.UserID
		filters = append(filters, filter3)
	}

	result, be := s.doctorBusiness.Patient().SearchViewManagedIndexPage(argument.Index, argument.Size, filters)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(result)
}

func (s *Patient) SearchViewManagedIndexPageDoc(a document.Assistant) document.Function {
	now := types.Time(time.Now())
	manageClassCode := uint64(1)
	prop := "manageLevel"
	order := "ascending"

	function := a.CreateFunction("获取患者管理信息（分页）")
	function.SetNote("获取患者管理信息")
	function.SetInputExample(&model.PageFilter{
		Page: model.Page{
			Index: 1,
			Size:  15,
		},
		Filter: &doctorModel.ViewManagedIndexFilter{
			ManageClassCode: &manageClassCode,
			Order: &model.Order{
				Prop:  &prop,
				Order: &order,
			},
		},
	})
	function.SetOutputExample(&model.PageResult{
		Page: model.Page{
			Index: 1,
			Size:  15,
		},
		Total: 100,
		Count: 7,
		Data: []*doctorModel.ViewManagedIndexEx{
			{
				ViewManagedIndex: doctorModel.ViewManagedIndex{
					SerialNo:            1808080848450270,
					PatientID:           20181011,
					PatientName:         "Tom",
					ManageStartDateTime: &now,
					Ext: &doctorModel.ManagedPatientIndexExt{
						Htn: doctorModel.ManagedPatientIndexExtHtn{
							ManagedPatientIndexExtData: doctorModel.ManagedPatientIndexExtData{
								ManageLevel:              2,
								ManageLevelStartDateTime: now,
							},
						},
					},
				},
				Age:     60,
				SexText: "男",
			},
		},
	})

	s.setDocFun(a, function)

	return function
}

func (s *Patient) CreateBloodPressureRecord(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.BloodPressureRecord{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InputError, err)
		return
	}

	now := types.Time(time.Now())
	argument.InputDateTime = &now
	if argument.MeasureDateTime == nil {
		argument.MeasureDateTime = &now
	}

	data, be := s.patientBusiness.Data().CreateBloodPressureRecord(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Patient) CreateBloodPressureRecordDoc(a document.Assistant) document.Function {
	heartRate := uint64(65)
	measureBodyPart := uint64(0)
	MeasurePlace := uint64(0)
	now := types.Time(time.Now())

	function := a.CreateFunction("新增血压记录")
	function.SetNote("新增血压记录患者血压记录信息,成功时返回记录序列号")
	function.SetInputExample(&doctorModel.BloodPressureRecordCreateEx{
		BloodPressureRecordCreate: doctorModel.BloodPressureRecordCreate{
			SystolicPressure:  110,
			DiastolicPressure: 78,
			HeartRate:         &heartRate,
			MeasureBodyPart:   &measureBodyPart,
			Memo:              "服用硝苯地平控释片2小时后",
			MeasurePlace:      &MeasurePlace,
			MeasureDateTime:   &now,
		},
		PatientID: 232442,
	})
	function.SetOutputExample(11)

	s.setDocFun(a, function)

	return function
}

func (s *Patient) ListBloodPressureRecord(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.BloodPressureRecordDataFilterEx{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InputError, err)
		return
	}

	data, be := s.patientBusiness.Data().ListBloodPressureRecord(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Patient) ListBloodPressureRecordDoc(a document.Assistant) document.Function {
	heartRate := uint64(65)
	now := types.Time(time.Now())
	function := a.CreateFunction("获取血压数据列表")
	function.SetNote("获取患者血压及心率数据列表")
	function.SetInputExample(&doctorModel.BloodPressureRecordDataFilterEx{
		BloodPressureRecordDataFilter: doctorModel.BloodPressureRecordDataFilter{
			MeasureStartDate: &now,
			MeasureEndDate:   &now,
		},
		PatientID: 232442,
	})
	function.SetOutputExample([]*doctorModel.BloodPressureRecordData{
		{
			SystolicPressure:  110,
			DiastolicPressure: 78,
			HeartRate:         &heartRate,
			MeasureDateTime:   &now,
			Memo:              "服用硝苯地平控释片2小时后",
		},
	})

	s.setDocFun(a, function)

	return function
}

func (s *Patient) StatBloodPressureRecord(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.BloodPressureRecordDataFilterEx{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InputError, err)
		return
	}

	data, be := s.patientBusiness.Data().StatBloodPressureRecord(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Patient) StatBloodPressureRecordDoc(a document.Assistant) document.Function {
	heartRate := uint64(65)
	now := types.Time(time.Now())
	function := a.CreateFunction("获取血压数据统计信息")
	function.SetNote("获取患者血压及心率数据统计信息")
	function.SetInputExample(&doctorModel.BloodPressureRecordDataFilterEx{
		BloodPressureRecordDataFilter: doctorModel.BloodPressureRecordDataFilter{
			MeasureStartDate: &now,
			MeasureEndDate:   &now,
		},
		PatientID: 232442,
	})
	function.SetOutputExample(&doctorModel.BloodPressureRecordDataStat{
		Min: &doctorModel.BloodPressureRecordData{
			SystolicPressure:  110,
			DiastolicPressure: 78,
			HeartRate:         &heartRate,
			MeasureDateTime:   &now,
		},
		Max: &doctorModel.BloodPressureRecordData{
			SystolicPressure:  110,
			DiastolicPressure: 78,
			HeartRate:         &heartRate,
			MeasureDateTime:   &now,
		},
		Items: []*doctorModel.BloodPressureRecordData{
			{
				SystolicPressure:  110,
				DiastolicPressure: 78,
				HeartRate:         &heartRate,
				MeasureDateTime:   &now,
			},
		},
	})

	s.setDocFun(a, function)

	return function
}

func (s *Patient) ListAlertRecordPage(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	filter := &doctorModel.AlertRecordFilter{}
	argument := &model.PageFilter{
		Page: model.Page{
			Index: 1,
			Size:  15,
		},
		Filter: filter,
	}
	a.GetArgument(r, argument)

	result, be := s.doctorBusiness.Patient().ListAlertRecordPage(argument.Index, argument.Size, filter)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(result)
}

func (s *Patient) ListAlertRecordPageDoc(a document.Assistant) document.Function {
	patientID := uint64(43)
	now := types.Time(time.Now())
	status := uint64(0)

	function := a.CreateFunction("获取患者预警记录信息（分页）")
	function.SetNote("获取获取患预警记录信息列表")
	function.SetInputExample(&model.PageFilter{
		Page: model.Page{
			Index: 1,
			Size:  15,
		},
		Filter: &doctorModel.AlertRecordFilter{
			PatientID: &patientID,
			Statuses:  []uint64{0, 1},
		},
	})
	function.SetOutputExample(&model.PageResult{
		Page: model.Page{
			Index: 1,
			Size:  15,
		},
		Total: 100,
		Count: 7,
		Data: []*doctorModel.AlertRecordEx{
			{
				AlertRecord: doctorModel.AlertRecord{
					SerialNo:        1808080848450270,
					PatientID:       20181011,
					AlertCode:       "H05",
					AlertType:       "血压",
					AlertName:       "单次血压异常偏高",
					AlertReason:     "180/100mmH",
					AlertMessage:    "消息内容",
					AlertDateTime:   &now,
					ReceiveDateTime: &now,
					Status:          status,
					ProcessMode:     &status,
				},
				StatusText:      "未处理",
				ProcessModeText: "随访",
			},
		},
	})

	s.setDocFun(a, function)

	return function
}

func (s *Patient) ListAlert(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.AlertRecordFilterEx{}
	arguments := make([]*doctorModel.AlertRecordFilterEx, 0)
	a.GetArgument(r, argument)

	token, err := s.DbToken.Get(a.Token())
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}
	if token.UserType == enum.DoctorUserTypes.Doctor().Key {
		argument.DoctorID = &token.UserID
		arguments = append(arguments, argument)
	} else if token.UserType == enum.DoctorUserTypes.HealthManager().Key {
		argument.HealthManagerID = &token.UserID
		arguments = append(arguments, argument)
	} else if token.UserType == enum.DoctorUserTypes.DoctorAndHealthManager().Key {
		argument2 := *argument
		argument3 := &argument2
		argument.HealthManagerID = &token.UserID
		arguments = append(arguments, argument)
		argument3.DoctorID = &token.UserID
		arguments = append(arguments, argument3)
	}

	result, be := s.doctorBusiness.Patient().ListAlert(arguments)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(result)
}

func (s *Patient) ListAlertDoc(a document.Assistant) document.Function {
	patientID := uint64(43)
	now := types.Time(time.Now())
	status := uint64(0)
	sex := uint64(1)

	function := a.CreateFunction("获取患者预警记录信息")
	function.SetNote("获取获取患预警记录信息列表")
	function.SetInputExample(&doctorModel.AlertRecordFilter{
		Statuses: []uint64{0, 1},
	})
	function.SetOutputExample([]doctorModel.AlertPatientInfo{
		{
			PatientID:   patientID,
			PatientName: "张三",
			Sex:         &sex,
			SexText:     "男",
			DateOfBirth: &now,
			Age:         60,
			Tag:         "冠心病 高血压 糖尿病 老年人 肥胖 支架手术术后",
			Photo:       "data:image/png;base64,iVBOR...",
			Alert: doctorModel.AlertInfo{
				BloodPressures: []doctorModel.AlertRecordEx{
					{
						AlertRecord: doctorModel.AlertRecord{
							SerialNo:        1808080848450270,
							PatientID:       patientID,
							AlertCode:       "H05",
							AlertType:       "血压",
							AlertName:       "单次血压异常偏高",
							AlertReason:     "180/100mmH",
							AlertMessage:    "消息内容",
							AlertDateTime:   &now,
							ReceiveDateTime: &now,
							Status:          status,
							ProcessMode:     &status,
						},
						StatusText:      "未处理",
						ProcessModeText: "随访",
					},
				},
				BloodGlucoses: []doctorModel.AlertRecordEx{
					{
						AlertRecord: doctorModel.AlertRecord{
							SerialNo:        1808080848450270,
							PatientID:       patientID,
							AlertCode:       "H05",
							AlertType:       "血糖",
							AlertName:       "单次血糖异常偏高",
							AlertReason:     "9.1 mmol/L",
							AlertMessage:    "消息内容",
							AlertDateTime:   &now,
							ReceiveDateTime: &now,
							Status:          status,
							ProcessMode:     &status,
						},
						StatusText:      "未处理",
						ProcessModeText: "随访",
					},
				},
				Others: []doctorModel.AlertRecordEx{
					{
						AlertRecord: doctorModel.AlertRecord{
							SerialNo:        1808080848450270,
							PatientID:       patientID,
							AlertCode:       "H05",
							AlertType:       "未知",
							AlertName:       "XXX异常",
							AlertReason:     "180/100mmH",
							AlertMessage:    "消息内容",
							AlertDateTime:   &now,
							ReceiveDateTime: &now,
							Status:          status,
							ProcessMode:     &status,
						},
						StatusText:      "未处理",
						ProcessModeText: "随访",
					},
				},
			},
		},
	})

	s.setDocFun(a, function)

	return function
}

func (s *Patient) SearchAlert(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.PatientInfoBase{}
	a.GetArgument(r, argument)

	result, be := s.doctorBusiness.Patient().SearchAlert(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(result)
}

func (s *Patient) SearchAlertDoc(a document.Assistant) document.Function {
	patientID := uint64(43)

	function := a.CreateFunction("获取患者未处理预警记录信息")
	function.SetNote("获取患者未处理预警记录信息")
	function.SetInputExample(&doctorModel.PatientInfoBase{
		PatientID: patientID,
	})
	function.SetOutputExample([]doctorModel.AlertPatientRecord{
		{
			AlertCode: "G01",
		},
	})

	s.setDocFun(a, function)

	return function
}

func (s *Patient) CountAlert(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	arguments := make([]*doctorModel.AlertRecordFilterEx, 0)
	argument1 := &doctorModel.AlertRecordFilterEx{}
	a.GetArgument(r, argument1)

	token, err := s.DbToken.Get(a.Token())
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}
	if token.UserType == enum.DoctorUserTypes.Doctor().Key {
		argument1.DoctorID = &token.UserID
		arguments = append(arguments, argument1)
	} else if token.UserType == enum.DoctorUserTypes.HealthManager().Key {
		argument1.HealthManagerID = &token.UserID
		arguments = append(arguments, argument1)
	} else if token.UserType == enum.DoctorUserTypes.DoctorAndHealthManager().Key {
		argument2 := *argument1
		argument3 := &argument2

		argument1.DoctorID = &token.UserID
		arguments = append(arguments, argument1)

		argument3.HealthManagerID = &token.UserID
		arguments = append(arguments, argument3)
	}

	result, be := s.doctorBusiness.Patient().CountAlert(arguments)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(result)
}

func (s *Patient) CountAlertDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("获取预警预警患者数量")
	function.SetNote("获取预警预警患者数量")
	function.SetInputExample(&doctorModel.AlertRecordFilter{
		Statuses: []uint64{0, 1},
	})
	function.SetOutputExample(11)

	s.setDocFun(a, function)

	return function
}

func (s *Patient) IgnoreAlert(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.AlertRecordIgnoreEx{}
	a.GetArgument(r, argument)
	if len(argument.SerialNos) < 1 {
		a.Error(errors.InputInvalid, fmt.Errorf("为指定预警记录序号serialNos"))
		return
	}

	token, err := s.DbToken.Get(a.Token())
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}
	now := time.Now()
	processMode := enum.AlertProcessModes.Ignore().Key
	argument.IgnoreDoctorID = &token.UserID
	argument.IgnoreDoctorName = token.UserName
	argument.IgnoreDateTime = &now
	argument.Status = enum.AlertStatuses.Processed().Key
	argument.ProcessMode = &processMode

	result, be := s.doctorBusiness.Patient().IgnoreAlert(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(result)
}

func (s *Patient) IgnoreAlertDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("忽略患者预警预警记录")
	function.SetNote("忽略患者预警预警记录并返回被忽略的记录数")
	function.SetInputExample(&doctorModel.AlertRecordIgnore{
		SerialNos:    []uint64{324, 432},
		IgnoreReason: "重复预警",
	})
	function.SetOutputExample(2)

	s.setDocFun(a, function)

	return function
}

func (s *Patient) SearchPatientOne(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.ViewPatientFilter{}
	a.GetArgument(r, argument)

	result, be := s.doctorBusiness.Patient().SearchPatientOne(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(result)
}

func (s *Patient) SearchPatientOneDoc(a document.Assistant) document.Function {
	patientID := uint64(88)

	function := a.CreateFunction("获取患者信息")
	function.SetNote("获取患者信息")
	function.SetInputExample(&doctorModel.ViewPatientFilter{
		PatientID: &patientID,
	})
	function.SetOutputExample(&doctorModel.ViewPatientEx{
		ViewPatient: doctorModel.ViewPatient{
			PatientID: patientID,
		},
	})

	s.setDocFun(a, function)

	return function
}

func (s *Patient) SearchPatientBasicInfoOne(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.ViewPatientBaseInfoFilterEx{}
	a.GetArgument(r, argument)

	token, err := s.DbToken.Get(a.Token())
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}
	argument.OrgCode = token.OrgCode

	result, be := s.doctorBusiness.Patient().SearchPatientBasicInfoOne(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(result)
}

func (s *Patient) SearchPatientBasicInfoOneDoc(a document.Assistant) document.Function {
	patientID := uint64(88)

	function := a.CreateFunction("获取患者基本信息")
	function.SetNote("获取患者基本信息，患者须属于当前管理机构")
	function.SetInputExample(&doctorModel.ViewPatientBaseInfoFilter{
		PatientID: &patientID,
	})
	function.SetOutputExample(&doctorModel.ViewPatientBaseInfoEx{
		ViewPatientBaseInfo: doctorModel.ViewPatientBaseInfo{
			PatientID: patientID,
		},
	})

	s.setDocFun(a, function)

	return function
}

func (s *Patient) SearchManagementPlanList(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.ManagementPlanFilter{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InputError, err)
		return
	}

	data, be := s.doctorBusiness.Patient().SearchManagementPlanList(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Patient) SearchManagementPlanListDoc(a document.Assistant) document.Function {
	patientID := uint64(87)
	now := types.Time(time.Now())
	function := a.CreateFunction("获取患者管理计划列表")
	function.SetNote("获取患者管理计划列表")
	function.SetInputExample(&doctorModel.ManagementPlanFilter{
		PatientID: &patientID,
	})
	function.SetOutputExample([]*doctorModel.ManagementPlanEx{
		{
			ManagementPlan: doctorModel.ManagementPlan{
				SerialNo:  324,
				PatientID: 232442,
				Content: &manage.InputDataManagement{
					Rank: 1,
					Advise: &manage.InputDataManagementAdvise{
						Date: &now,
					},
				},
			},
		},
	})

	s.setDocFun(a, function)

	return function
}

func (s *Patient) SearchManagementApplicationReviewPage(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	token, err := s.DbToken.Get(a.Token())
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}

	filter := &doctorModel.ViewManagementApplicationReviewFilterEx{}
	argument := &model.PageFilter{
		Page: model.Page{
			Index: 1,
			Size:  15,
		},
		Filter: filter,
	}
	a.GetArgument(r, argument)
	filter.VisitOrgCode = token.OrgCode
	result, be := s.doctorBusiness.Patient().SearchManagementApplicationReviewPage(argument.Index, argument.Size, filter)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(result)
}

func (s *Patient) SearchManagementApplicationReviewPageDoc(a document.Assistant) document.Function {
	sex := uint64(1)
	now := types.Time(time.Now())
	status := uint64(0)

	function := a.CreateFunction("获取注册患者记录信息（分页）")
	function.SetNote("获取注册患者记录信息列表及分页信息")
	function.SetInputExample(&model.PageFilter{
		Page: model.Page{
			Index: 1,
			Size:  15,
		},
		Filter: &doctorModel.ViewManagementApplicationReviewFilter{
			Statuses: []uint64{0},
		},
	})
	function.SetOutputExample(&model.PageResult{
		Page: model.Page{
			Index: 1,
			Size:  15,
		},
		Total: 100,
		Count: 7,
		Data: []*doctorModel.ViewManagementApplicationReviewEx{
			{
				ViewManagementApplicationReview: doctorModel.ViewManagementApplicationReview{
					SerialNo:        1808080848450270,
					PatientID:       20181011,
					PatientName:     "张三",
					ApplicationFrom: "APP",
					RegistDateTime:  &now,
					Diagnosis:       "高血压，糖尿病",
					Status:          status,
					Sex:             &sex,
				},
				SexText: "男",
				Age:     60,
			},
		},
	})

	s.setDocFun(a, function)

	return function
}

func (s *Patient) AuditPatient(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.PatientUserAuditEx{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InputError, err)
		return
	}

	token, err := s.DbToken.Get(a.Token())
	if err != nil {
		a.Error(errors.InternalError, err)
	}
	argument.ReviewerOrgCode = token.OrgCode
	if argument.ReviewerOrgCode == "" {
		a.Error(errors.NotSupport, fmt.Errorf("当前登录用户信息不完善(缺少机构信息)"))
		return
	}
	argument.ReviewerID = token.UserID
	argument.ReviewerName = token.UserName
	argument.ReviewDateTime = types.Time(time.Now())

	data, sno, be := s.doctorBusiness.Patient().AuditPatientUser(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}
	if argument.Status == 1 {
		go func(userId uint64) {
			s.patientBusiness.Data().RegisterDisease(userId)
		}(data)
		if argument.DietManage != "" {
			now := types.Time(time.Now())
			expArgument := &doctorModel.ExpPatientCreate{
				PatientID:      data,
				Type:           1,
				CreateDateTime: &now,
			}
			be = s.doctorBusiness.Exp().IncludePatientInExp(expArgument, true)
			if be != nil {
				a.Error(be.Error(), be.Detail())
				return
			}
		}
	}
	if sno != 0 {
		s.patientBusiness.Data().UploadDataToManage(sno)
	}
	a.Success(data)
}

func (s *Patient) AuditPatientDoc(a document.Assistant) document.Function {
	doctorID := uint64(11)
	healthManagerID := uint64(12)
	function := a.CreateFunction("审核注册患者")
	function.SetNote("同意或拒绝患者注册，当同意时将创建患者管理索引")
	function.SetInputExample(&doctorModel.PatientUserAudit{
		PatientID: 88,
		Status:    1,
		PatientFeatures: []string{
			"肥胖",
			"残疾人",
		},
		ManageClasses: []*doctorModel.ManageClass{
			{
				ItemCode: 1,
				ItemName: "高血压",
			},
			{
				ItemCode: 2,
				ItemName: "糖尿病",
			},
			{
				ItemCode: 3,
				ItemName: "慢阻肺",
			},
		},
		DoctorID:          &doctorID,
		DoctorName:        "李四",
		HealthManagerID:   &healthManagerID,
		HealthManagerName: "王五",
	})
	function.SetOutputExample(uint64(88))

	s.setDocFun(a, function)

	return function
}

func (s *Patient) CreateWeightRecord(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.WeightRecordForAppEx{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InputError, err)
		return
	}
	if argument.MeasureDateTime == nil {
		now := types.Time(time.Now())
		argument.MeasureDateTime = &now
	}

	data, be := s.patientBusiness.Data().CommitWeightRecord(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Patient) CreateWeightRecordDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("新增患者体重记录")
	function.SetNote("新增患者体重记录，成功返回记录学列号")
	function.SetInputExample(&doctorModel.WeightRecordCreate{
		PatientID: 88,
		WeightRecordData: doctorModel.WeightRecordData{
			Weight: 60.5,
			Memo:   "备注",
		},
	})
	function.SetOutputExample(uint64(88))

	s.setDocFun(a, function)

	return function
}

func (s *Patient) SearchWeightRecordList(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.WeightRecordDataFilterEx{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InputError, err)
		return
	}

	data, be := s.patientBusiness.Data().ListWeightRecord(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Patient) SearchWeightRecordListDoc(a document.Assistant) document.Function {
	now := types.Time(time.Now())
	function := a.CreateFunction("获取体重数据列表")
	function.SetNote("获取患者体重数据列表，按测量时间升序排列")
	function.SetInputExample(&doctorModel.WeightRecordDataFilterEx{
		WeightRecordDataFilter: doctorModel.WeightRecordDataFilter{
			MeasureStartDate: &now,
			MeasureEndDate:   &now,
		},
		PatientID: 100,
	})
	function.SetOutputExample([]*doctorModel.WeightRecordData{
		{
			Weight:          60.5,
			MeasureDateTime: &now,
			Memo:            "有氧运动半小时后",
		},
	})

	s.setDocFun(a, function)

	return function
}

func (s *Patient) SearchPatientBodyMassIndex(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.PatientInfoBase{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InputError, err)
		return
	}

	data, be := s.patientBusiness.Data().SearchPatientBodyMassIndex(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Patient) SearchPatientBodyMassIndexDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("获取体重BMI指数")
	function.SetNote("获取体重BMI指数，当缺少体重或身高信息时返回空值")
	function.SetInputExample(&doctorModel.PatientInfoBase{
		PatientID: 100,
	})
	function.SetOutputExample(float64(23.7))

	s.setDocFun(a, function)

	return function
}

func (s *Patient) SearchViewSportDietRecordPage(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	filter := &doctorModel.ViewSportDietRecordFilter{}
	argument := &model.PageFilter{
		Page: model.Page{
			Index: 1,
			Size:  15,
		},
		Filter: filter,
	}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InputError, err)
	}

	result, be := s.doctorBusiness.Patient().SearchViewSportDietRecordPage(argument.Index, argument.Size, filter)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(result)
}

func (s *Patient) SearchViewSportDietRecordPageDoc(a document.Assistant) document.Function {
	now := types.Time(time.Now())

	function := a.CreateFunction("获取患者运动与饮食信息（分页）")
	function.SetNote("获取患者运动与饮食信息")
	function.SetInputExample(&model.PageFilter{
		Page: model.Page{
			Index: 1,
			Size:  15,
		},
		Filter: &doctorModel.ViewSportDietRecordFilter{
			PatientID:     100,
			HappenDateEnd: &now,
		},
	})
	function.SetOutputExample(&model.PageResult{
		Page: model.Page{
			Index: 1,
			Size:  15,
		},
		Total: 100,
		Count: 7,
		Data: []*doctorModel.ViewSportDietRecord{
			{
				PatientID:     100,
				HappenDate:    "2018-10-30",
				DietBreakfast: "主食50g，水果20g",
				DietLunch:     "蔬菜210g，主食260g",
				DietDinner:    "主食150g",
				DietOther:     "棒冰1根",
				Sport:         "跳绳33min 运动强度中，步行121步 运动强度低",
			},
		},
	})

	s.setDocFun(a, function)

	return function
}

func (s *Patient) SearchDiscomfortRecordPage(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	filter := &doctorModel.DiscomfortRecordDataFilterEx{}
	argument := &model.PageFilter{
		Page: model.Page{
			Index: 1,
			Size:  15,
		},
		Filter: filter,
	}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InputError, err)
	}

	result, be := s.doctorBusiness.Patient().SearchDiscomfortRecordPage(argument.Index, argument.Size, filter)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(result)
}

func (s *Patient) SearchDiscomfortRecordPageDoc(a document.Assistant) document.Function {
	now := types.Time(time.Now())

	function := a.CreateFunction("获取患者不适记录信息（分页）")
	function.SetNote("获取患者不适记录信息")
	function.SetInputExample(&model.PageFilter{
		Page: model.Page{
			Index: 1,
			Size:  15,
		},
		Filter: &doctorModel.DiscomfortRecordDataFilterEx{
			DiscomfortRecordDataFilter: doctorModel.DiscomfortRecordDataFilter{
				HappenEndDate: &now,
			},
			PatientID: 100,
		},
	})
	function.SetOutputExample(&model.PageResult{
		Page: model.Page{
			Index: 1,
			Size:  15,
		},
		Total: 100,
		Count: 7,
		Data: []*doctorModel.DiscomfortRecord{
			{
				PatientID:      100,
				Discomfort:     "剧烈头痛，恶心呕吐",
				Memo:           "",
				HappenDateTime: &now,
			},
		},
	})

	s.setDocFun(a, function)

	return function
}

func (s *Patient) SearchViewStatDiscomfortList(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.ViewStatDiscomfortFilter{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InputError, err)
		return
	}

	data, be := s.doctorBusiness.Patient().SearchViewStatDiscomfortList(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Patient) SearchViewStatDiscomfortListDoc(a document.Assistant) document.Function {
	now := types.Time(time.Now())
	function := a.CreateFunction("获取患者不适记录统计列表")
	function.SetNote("获取患者不适记录统计列表，按数量降序排列")
	function.SetInputExample(&doctorModel.ViewStatDiscomfortFilter{
		PatientID:         100,
		HappenDateTimeEnd: &now,
	})
	function.SetOutputExample([]*doctorModel.ViewStatDiscomfort{
		{
			PatientID:  100,
			Count:      3,
			Discomfort: "头痛",
		},
	})

	s.setDocFun(a, function)

	return function
}

func (s *Patient) SearchPatientHeightAndWeight(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.PatientInfoBase{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InputError, err)
		return
	}

	data, be := s.doctorBusiness.Patient().SearchPatientHeightAndWeight(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Patient) SearchPatientHeightAndWeightDoc(a document.Assistant) document.Function {
	height := uint64(175)
	weight := float64(65.3)

	function := a.CreateFunction("获取患者最新的身高体重信息")
	function.SetNote("获取患者最新的身高体重信息")
	function.SetInputExample(&doctorModel.PatientInfoBase{
		PatientID: 100,
	})
	function.SetOutputExample(&doctorModel.PatientUserHeightAndWeight{
		Height: &height,
		Weight: &weight,
	})

	s.setDocFun(a, function)

	return function
}
func (s *Patient) UpdatePatientHeight(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.PatientUserHeight{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InputError, err)
		return
	}

	data, be := s.doctorBusiness.Patient().UpdatePatientHeight(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}
func (s *Patient) UpdatePatientHeightDoc(a document.Assistant) document.Function {
	height := uint64(175)

	function := a.CreateFunction("更新患者身高")
	function.SetNote("更新患者身高")
	function.SetInputExample(&doctorModel.PatientUserHeight{
		PatientID: 100,
		Height:    &height,
	})
	function.SetOutputExample(&doctorModel.PatientUserHeight{
		PatientID: 100,
	})

	s.setDocFun(a, function)

	return function
}
func (s *Patient) UpdatePatientUserInfo(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.PatientUserBaseInfoModify{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InputError, err)
		return
	}
	err = argument.Verify()
	if err != nil {
		a.Error(errors.InputInvalid, err)
		return
	}
	token, err := s.DbToken.Get(a.Token())
	if err != nil {
		a.Error(errors.InternalError, err)
	}
	argument.VisitOrgCode = token.OrgCode
	doctorName := token.UserName
	doctorID := token.UserID
	if argument.VisitOrgCode == "" {
		a.Error(errors.NotSupport, fmt.Errorf("当前登录用户信息不完善(缺少机构信息)"))
		return
	}
	data, _, be := s.doctorBusiness.Patient().ModifyPatientUserBaseInfo(argument, doctorName, doctorID)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}
	//if sno != 0 {
	//	s.patientBusiness.Data().UploadDataToManage(sno)
	//}

	a.Success(data)
}
func (s *Patient) UpdatePatientUserInfoDoc(a document.Assistant) document.Function {
	sex := uint64(1)
	height := uint64(175)
	SerialNo := uint64(175)
	UserID := uint64(175)
	Weight := float64(50)
	now := types.Time(time.Now())
	healthManagerID := uint64(12)
	DoctorID := uint64(15)
	function := a.CreateFunction("修改患者信息")
	function.SetNote("修改患者信息")
	function.SetInputExample(&doctorModel.PatientUserBaseInfoModify{
		SerialNo:           SerialNo,
		Height:             &height,
		IdentityCardNumber: "330106200012129876",
		Name:               "张三",
		Sex:                &sex,
		DateOfBirth:        &now,
		Phone:              "13812344321",
		EducationLevel:     "大学",
		JobType:            "",
		PatientFeature: []string{
			"肥胖",
			"残疾人",
		},
		DoctorID:          &DoctorID,
		DoctorName:        "李四",
		HealthManagerID:   &healthManagerID,
		HealthManagerName: "王五",
		UserID:            UserID,
		Weight:            &Weight,
		DiagnosisMemo:     "",
		Diagnosis:         "",
	})
	function.SetOutputExample(&doctorModel.PatientInfoBase{
		PatientID: 100,
	})

	s.setDocFun(a, function)

	return function
}

func (s *Patient) UpdatePatientDiagnosismemo(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.ManagementApplicationReviewMemo{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InputError, err)
		return
	}

	data, be := s.doctorBusiness.Patient().UpdatePatientDiagnosismemo(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}
func (s *Patient) UpdatePatientDiagnosismemoDoc(a document.Assistant) document.Function {

	function := a.CreateFunction("更新患者备注")
	function.SetNote("更新患者备注")
	function.SetInputExample(&doctorModel.ManagementApplicationReviewMemo{
		PatientID:     100,
		DiagnosisMemo: "饮食备注",
	})
	function.SetOutputExample(&doctorModel.ManagementApplicationReviewMemo{
		PatientID: 100,
	})

	s.setDocFun(a, function)

	return function
}

func (s *Patient) ListPatientWeightPhoto(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.WeightRecordDataFilterEx{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InputError, err)
		return
	}

	data, be := s.doctorBusiness.Patient().ListPatientWeightPhoto(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}
func (s *Patient) ListPatientWeightPhotoDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("获取患者体重打卡照片")
	function.SetNote("获取患者体重打卡照片")
	function.SetInputExample(&doctorModel.WeightRecordDataFilterEx{
		PatientID: 100,
	})
	now := types.Time(time.Now())
	function.SetOutputExample(&doctorModel.WeightRecordForApp{
		SerialNo:        100,
		Weight:          40,
		MeasureDateTime: &now,
		Memo:            "备注",
		Type:            1,
		Photo:           "http://",
	})

	s.setDocFun(a, function)

	return function
}

func (s *Patient) GetPatientClockDay(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.ExpPatientTaskRecordGetListInput{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InputError, err)
		return
	}
	//data, be := s.doctorBusiness.Patient().GetPatientClockDay(argument)
	data, be := s.patientBusiness.Data().GetPatientClock(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)

}
func (s *Patient) GetPatientClockDayDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("获取患者打卡情况用以打卡日历")
	function.SetNote("获取患者打卡情况用以打卡日历")
	function.SetInputExample(&doctorModel.ExpPatientTaskRecordGetListInput{
		PatientID: 100,
	})
	function.SetOutputExample([][]time.Time{})

	s.setDocFun(a, function)

	return function
}
