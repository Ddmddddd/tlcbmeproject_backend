package doctor

import (
	"tlcbme_project/business/doctor/api"
	"tlcbme_project/data/model"
	"tlcbme_project/data/model/doctor"
	"github.com/ktpswjz/httpserver/types"
	"time"
)

type socketFollowupCount struct {
	filter   *doctor.ViewFollowupPlanFilterEx
	followup api.Followup
}

func newSocketFollowupCount(token *model.Token, followup api.Followup) NotifyData {
	instance := &socketFollowupCount{
		filter:   &doctor.ViewFollowupPlanFilterEx{},
		followup: followup,
	}

	instance.filter.ManageStatuses = make([]uint64, 1)
	instance.filter.ManageStatuses[0] = 0
	instance.filter.OrgCode = token.OrgCode
	instance.filter.DoctorID = &token.UserID
	instance.filter.HealthManagerID = &token.UserID
	instance.filter.Today = true

	return instance
}

func (s *socketFollowupCount) GetData() interface{} {
	now := types.Time(time.Now())
	//s.filter.FollowupDateStart = &now
	s.filter.FollowupDateEnd = &now

	data, err := s.followup.SearchFollowupPlanCount(s.filter)
	if err != nil {
		return 0
	}

	return data.Waiting
}
