package callback

import (
	"tlcbme_project/business/doctor"
	"tlcbme_project/business/patient"
	"tlcbme_project/server/controller"
	"github.com/ktpswjz/httpserver/document"
)

type doc struct {
	controller.Controller

	doctorBusiness  doctor.Business
	patientBusiness patient.Business
}

func (s *doc) rootCatalog(a document.Assistant) document.Catalog {
	return a.CreateCatalog("回调平台接口", "平台相关管理服务回调接口")
}
