package callback

import (
	"tlcbme_project/business"
	"tlcbme_project/data/model/doctor"
	"tlcbme_project/data/model/manage"
	"tlcbme_project/errors"
	"fmt"
)

func (s *Manage) pushHbp(data *manage.InputData, index *doctor.ManagedPatientIndex) business.Error {
	if data == nil || index == nil {
		return business.NewError(errors.InputError, fmt.Errorf("参数为空"))
	}

	if data.ContainFlag(manage.FlagRiskAssess) { // 危险评估
		if data.RiskAssess.Level > 0 {
			data.RiskAssess.Name = "高血压危险分层评估"
			be := s.doctorBusiness.Assessment().SaveRecord(index, data.RiskAssess)
			if be != nil {
				return be
			}
		}
	}
	if data.ContainFlag(manage.FlagManagement) { // 管理计划
		flag := uint64(1)
		if data.Management.Advise != nil {
			data.Management.Advise.Type = "高血压"
		}
		be := s.doctorBusiness.Patient().SaveManagement(index, data.Management, flag)
		if be != nil {
			return be
		}
	}
	if data.ContainFlag(manage.FlagFollowup) { // 随访排期
		if data.Followup == nil {
			return business.NewError(errors.InputInvalid, fmt.Errorf("缺少随访排期数据"))
		}
		plan := &doctor.FollowupPlan{
			PatientID:    index.PatientID,
			FollowupDate: data.Followup.ScheduledDate,
			FollowUpType: "高血压常规随访",
		}
		_, be := s.doctorBusiness.Followup().CreatePlan(plan)
		if be != nil {
			return be
		}
	}
	if data.ContainFlag(manage.FlagWarnings) { // 危险预警
		alerts := data.Warnings
		if len(alerts) > 0 {
			be := s.doctorBusiness.Patient().SaveAlerts(index, alerts)
			if be != nil {
				return be
			}
		}

	}
	if data.ContainFlag(manage.FlagCompliance) { // 依从度及活跃度
		var activeDegree uint64
		count := len(data.Compliance)
		for i := 0; i < count; i++ {
			compliance := data.Compliance[i]
			if compliance.Code == 1 { // 1 - 测量依从度
				if compliance.NoActiveDays >= 14 {
					activeDegree = 0
				} else {
					activeDegree = 1
				}
				be := s.doctorBusiness.Patient().UpdateComplianceRateAndPatientActiveDegree(index, s.convertCompliance(compliance.Value), activeDegree)
				if be != nil {
					return be
				}
			}
		}
	}

	return nil
}
