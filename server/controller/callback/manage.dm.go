package callback

import (
	"tlcbme_project/business"
	"tlcbme_project/data/model/doctor"
	"tlcbme_project/data/model/manage"
	"tlcbme_project/errors"
	"fmt"
)

func (s *Manage) pushDm(data *manage.InputData, index *doctor.ManagedPatientIndex) business.Error {
	if data == nil || index == nil {
		return business.NewError(errors.InputError, fmt.Errorf("参数为空"))
	}

	if data.ContainFlag(manage.FlagRiskAssess) { // 危险评估
		if data.RiskAssess.Level > 0 {
			data.RiskAssess.Name = "糖尿病风险评估"
			be := s.doctorBusiness.Assessment().SaveRecord(index, data.RiskAssess)
			if be != nil {
				return be
			}
		}
	}
	if data.ContainFlag(manage.FlagManagement) { // 管理计划
		flag := uint64(2)
		if data.Management.Advise != nil {
			data.Management.Advise.Type = "糖尿病"
		}
		be := s.doctorBusiness.Patient().SaveManagement(index, data.Management, flag)
		if be != nil {
			return be
		}
	}
	if data.ContainFlag(manage.FlagFollowup) { // 随访排期
		if data.Followup == nil {
			return business.NewError(errors.InputInvalid, fmt.Errorf("缺少随访排期数据"))
		}
		plan := &doctor.FollowupPlan{
			PatientID:    index.PatientID,
			FollowupDate: data.Followup.ScheduledDate,
			FollowUpType: "糖尿病常规随访",
		}
		_, be := s.doctorBusiness.Followup().CreatePlan(plan)
		if be != nil {
			return be
		}
	}
	if data.ContainFlag(manage.FlagWarnings) { // 危险预警
		alerts := data.Warnings
		if len(alerts) > 0 {
			be := s.doctorBusiness.Patient().SaveAlerts(index, alerts)
			if be != nil {
				return be
			}
		}

	}
	if data.ContainFlag(manage.FlagCompliance) { // 依从度
		var activeDegree uint64
		count := len(data.Compliance)
		for i := 0; i < count; i++ {
			compliance := data.Compliance[i]
			if compliance.Code == 1 { // 1 - 测量依从度
				if compliance.NoActiveDays >= 14 {
					activeDegree = 0
				} else {
					activeDegree = 1
				}
				be := s.doctorBusiness.Patient().UpdateComplianceRateAndPatientActiveDegree(index, s.convertCompliance(compliance.Value), activeDegree)
				if be != nil {
					return be
				}
			}
		}
	}
	if data.ContainFlag(manage.FlagBarrier) { //健康饮食障碍
		barrierResult := data.Barriers

		be := s.doctorBusiness.Exp().SaveBarriers(index, barrierResult)
		if be != nil {
			return be
		}
	}
	if data.ContainFlag(manage.FlagActionPlan) { //生活计划
		actionPlans := data.ActionPlans
		if len(actionPlans) > 0 {
			be := s.doctorBusiness.Exp().SaveActionPlans(index, actionPlans)
			if be != nil {
				return be
			}
		}
	}

	return nil
}
