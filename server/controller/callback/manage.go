package callback

import (
	"tlcbme_project/business/doctor"
	"tlcbme_project/business/patient"
	"tlcbme_project/data/model"
	modelDoctor "tlcbme_project/data/model/doctor"
	"tlcbme_project/data/model/manage"
	"tlcbme_project/errors"
	"tlcbme_project/server/config"
	"encoding/json"
	"fmt"
	"github.com/ktpswjz/httpserver/document"
	"github.com/ktpswjz/httpserver/router"
	"github.com/ktpswjz/httpserver/types"
	"net/http"
	"time"
)

type Manage struct {
	doc
}

func NewManage(cfg *config.Config, log types.Log, doctorBusiness doctor.Business, patientBusiness patient.Business) *Manage {
	instance := &Manage{}
	instance.SetLog(log)
	instance.Config = cfg
	instance.doctorBusiness = doctorBusiness
	instance.patientBusiness = patientBusiness

	return instance
}

func (s *Manage) setDocFun(a document.Assistant, fun document.Function) {
	fun.IgnoreToken(true)
	catalog := s.rootCatalog(a).CreateChild("管理服务", "管理服务消息推送相关回调接口")
	catalog.SetFunction(fun)
}

func (s *Manage) SubscribeReceive(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	v, ok := a.Get("jwt.payload")
	if !ok {
		a.Error(errors.AuthNoToken, "jwt payload not found")
		return
	}
	jwtPayload := v.(*model.JwtPayload)
	if jwtPayload == nil {
		a.Error(errors.AuthNoToken, "jwt payload invalid: nil")
		return
	}

	var input interface{}
	err := a.GetArgument(r, &input)
	if err != nil {
		a.Error(errors.InputError, err)
		return
	}

	inputData, err := json.Marshal(input)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}
	argument := &manage.Input{}
	err = json.Unmarshal(inputData, argument)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}

	patientID, err := argument.Data.GetPatientID()
	if err != nil {
		a.Error(errors.InputError, "患者编号无效：", err)
		return
	}
	filter := &modelDoctor.ManagedPatientIndexFilterBase{
		PatientID: &patientID,
	}
	patientIndex, be := s.doctorBusiness.Patient().GetManagedIndex(filter)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	providerType := s.Config.Management.Providers.GetType(jwtPayload.Subject)
	if providerType == config.ManagementProviderTypeHbp {
		be = s.pushHbp(&argument.Data, patientIndex)
		if be != nil {
			a.Error(be.Error(), be.Detail())
			return
		}
	} else if providerType == config.ManagementProviderTypeDm {
		be = s.pushDm(&argument.Data, patientIndex)
		if be != nil {
			a.Error(be.Error(), be.Detail())
			return
		}
	} else {
		a.Error(errors.InputError, fmt.Errorf("接收方(aud=%s)无效", jwtPayload.Subject))
		return
	}

	a.Success(argument)
}

func (s *Manage) SubscribeReceiveDoc(a document.Assistant) document.Function {
	title := "用药指导"
	value := "ACEI 和 ARB / β 受体阻滞剂 / 钙通道阻滞剂 / 利尿剂"
	now := types.Time(time.Now())
	function := a.CreateFunction("订阅接收")
	function.SetNote("接送订阅管理服务的推送消息")
	function.SetInputExample(&manage.Input{
		Data: manage.InputData{
			PatientID: "43",
			RiskAssess: &manage.InputDataRiskAssess{
				Level:        3,
				NextSchedule: &now,
				Memo:         "高危",
			},
			Management: &manage.InputDataManagement{
				Rank: 1,
				Memo: "管理初期",
				Advise: &manage.InputDataManagementAdvise{
					Date:  &now,
					Title: "一级管理计划",
					MeasuringFrequency: []manage.InputDataManagementAdviseMeasuringFrequency{
						{
							Name:     "血压",
							Interval: 1,
							Unit:     "天",
							TimePoints: []string{
								"2",
								"3",
							},
							Times: 2,
						},
					},
					DrugSuggestion: manage.InputDataManagementAdviseDrugSuggestion{
						Title: &title,
						Value: &value,
					},
					ControlGoal: []manage.InputDataManagementAdviseControlGoal{
						{
							Name:     "收缩压",
							Operator: "<",
							Value:    "130",
							Unit:     "mmHg",
						},
						{
							Name:     "舒张压",
							Operator: "<",
							Value:    "100",
							Unit:     "mmHg",
						},
					},
					LifeIntervention: manage.InputDataManagementAdviseLifeIntervention{
						Title: "生活处方",
						Prescription: []manage.InputDataManagementAdviseLifeInterventionPrescription{
							{
								Title: "饮食处方",
								Value: "低盐低脂饮食，食盐摄入量<6g，控制主食，晚餐 5-7 分饱，新鲜蔬菜 500g 水果 1 个（苹果、梨、桃子等）",
							},
							{
								Title: "运动处方",
								Value: "1）中等强度的有氧运动，如快走、慢跑、跳舞、健身操、骑车、登山等；\n2）每天一次，每次30 分钟，运动中心率要达到 135 次/分；\n3）运动时间选择下午或傍晚，注意循序渐进、贵在坚持。",
							},
						},
					},
					Revisit: true,
				},
			},
			Followup: &manage.InputDataFollowup{
				ScheduledDate: &now,
				Memo:          "",
			},
			Warnings: []manage.InputDataWarnings{
				{
					Code:     "B05",
					Type:     "血压",
					Name:     "单次血压异常偏高",
					Reason:   "160 / 95 mmHg",
					Message:  "",
					DateTime: &now,
				},
			},
			Compliance: []manage.InputDataCompliance{
				{
					Name:         "测量依从度",
					Code:         1,
					Value:        4.5,
					NoActiveDays: 0,
				},
				{
					Name:         "服药依从度",
					Code:         2,
					Value:        5.0,
					NoActiveDays: 0,
				},
			},
		},
	})
	function.AddInputQuery("jwt", "接口调用凭证", true)

	s.setDocFun(a, function)

	return function
}

func (s *Manage) convertCompliance(v float64) uint64 {
	if v >= 1 {
		return 5
	} else if v > 0.8 {
		return 4
	} else if v > 0.5 {
		return 3
	} else if v > 0 {
		return 2
	} else {
		return 1
	}

}
