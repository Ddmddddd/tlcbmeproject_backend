package public

import (
	"encoding/json"
	"fmt"
	"github.com/ktpswjz/httpserver/document"
	"github.com/ktpswjz/httpserver/router"
	"net/http"
	"tlcbme_project/business/doctor"
	"tlcbme_project/business/patient"
	"tlcbme_project/data/model"
	doctorModel "tlcbme_project/data/model/doctor"
	"tlcbme_project/data/model/jinscale"
	"tlcbme_project/errors"
	"tlcbme_project/server/controller"
)

type public struct {
	controller.Controller

	doctorBusiness  doctor.Business
	patientBusiness patient.Business
}

func (s *public) rootCatalog(a document.Assistant) document.Catalog {
	return a.CreateCatalog("公共平台接口", "公共平台相关接口")
}

func (s *Dict) ListDivision(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.DivisionBaseInfo{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}

	data, be := s.patientBusiness.Data().ListDivision(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Dict) ListDivisionDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("获取上线医院的政治区划列表")
	function.SetNote("获取上线医院的政治区划列表")
	function.SetInputExample(nil)
	function.SetOutputExample([]*doctorModel.DivisionBaseInfo{
		{
			ItemCode: "640000000000",
			ItemName: "宁夏",
			FullName: "宁夏回族自治区",
		},
	})
	function.IgnoreToken(true)
	s.setDocFun(a, function)

	return function
}

func (s *Dict) ListOrgAndDoctor(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.OrgAndDoctorFilter{}
	err := a.GetArgument(r, argument)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}

	data, be := s.patientBusiness.Data().ListOrgAndDoctor(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Dict) ListOrgAndDoctorDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("获取上线的医院及其认证医生的列表")
	function.SetNote("获取上线的医院及其认证医生的列表")
	function.SetInputExample(&doctorModel.OrgAndDoctorFilter{
		DivisionCode: "640000000000",
		UserType:     1,
	})
	function.SetOutputExample([]*doctorModel.OrgAndDoctorInfo{
		{
			OrgCode: "126400004540034768",
			OrgName: "宁夏医科大学总医院",
			Doctors: []doctorModel.DoctorUserAuthsBaseInfo{
				{
					UserID:   1,
					UserName: "zhangsan",
					Name:     "张三",
				},
			},
		},
	})
	function.IgnoreToken(true)
	s.setDocFun(a, function)

	return function
}

func (s *Dict) SearchDivisionTree(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &doctorModel.DivisionDictTreeFilter{}
	a.GetArgument(r, argument)
	data, be := s.doctorBusiness.Dict().SearchDivisionTree(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Dict) SearchDivisionTreeDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("获取行政区划树")
	function.SetNote("获取行政区划树")
	function.SetInputExample(&doctorModel.DivisionDictTreeFilter{
		Admin: true,
	})
	function.SetOutputExample([]*doctorModel.DivisionDictTreeAdmin{
		{
			DivisionDictTreeItem: doctorModel.DivisionDictTreeItem{
				ItemCode: "110000000000",
				ItemName: "北京",
				FullName: "北京市",
			},
			Children: []*doctorModel.DivisionDictTreeAdmin{
				{
					DivisionDictTreeItem: doctorModel.DivisionDictTreeItem{
						ItemCode: "110101000000",
						ItemName: "东城",
						FullName: "东城区",
					},
					Children: []*doctorModel.DivisionDictTreeAdmin{},
				},
				{
					DivisionDictTreeItem: doctorModel.DivisionDictTreeItem{
						ItemCode: "110102000000",
						ItemName: "西城",
						FullName: "西城区",
					},
					Children: []*doctorModel.DivisionDictTreeAdmin{},
				},
			},
		},
	})
	function.IgnoreToken(true)

	s.setDocFun(a, function)

	return function
}

func (s *Dict) ListDictDrug(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	data, be := s.doctorBusiness.Dict().ListDrug()
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Dict) ListDictDrugDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("获取药品列表")
	function.SetNote("获取字典列表")
	function.SetInputExample(nil)
	function.SetOutputExample([]*doctorModel.DrugDict{
		{
			SerialNo:      1,
			ItemCode:      1,
			ItemName:      "硝苯地平片",
			Specification: "10mg×100片/瓶",
			Units:         "mg,片",
			Effect:        "降压",
			InputCode:     "xbdpp",
			IsValid:       1,
		},
	})

	s.setDocFun(a, function)

	return function
}

func (s *Dict) ListDictFoodCategory(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	data, be := s.doctorBusiness.Dict().ListFoodCategory()
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Dict) ListDictFoodCategoryDoc(a document.Assistant) document.Function {
	function := a.CreateFunction("获取食物分类列表")
	function.SetNote("获取食物分类列表")
	function.SetInputExample(nil)
	function.SetOutputExample([]*doctorModel.ExpFoodCategory{
		{
			SerialNo: 1,
			Name:     "主食",
		},
	})

	s.setDocFun(a, function)

	return function
}

func (s *Dict) ListDictFoodPageByCategory(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	filter := &doctorModel.ExpFoodDictCategoryFilter{}
	argument := &model.PageFilter{
		Page: model.Page{
			Index: 1,
			Size:  15,
		},
		Filter: filter,
	}
	a.GetArgument(r, argument)

	data, be := s.doctorBusiness.Dict().ListFoodPageByCategory(argument.Index, argument.Size, filter)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Dict) ListDictFoodPageByCategoryDoc(a document.Assistant) document.Function {
	nutrition := 1.0
	function := a.CreateFunction("按分类获取食物页信息")
	function.SetNote("按分类获取食物页信息")
	function.SetInputExample(&model.PageFilter{
		Page: model.Page{
			Index: 1,
			Size:  15,
		},
		Filter: &doctorModel.ExpFoodDictCategoryFilter{
			Category: 1,
		},
	})
	function.SetOutputExample(&model.PageResult{
		Page: model.Page{
			Index: 1,
			Size:  15,
		},
		Total: 100,
		Count: 7,
		Data: []*doctorModel.ExpFoodDict{
			{
				SerialNo:              3,
				Category:              1,
				Name:                  "白粥,又叫:白粥（粳米），稀饭，大米粥，白米粥，米粥，大米汤汤",
				ImageUrl:              "https://exp.zjubiomedit.com/images/food3.png",
				Calorie:               46,
				Recommended:           0,
				Comment:               "一种碳水化合物和水分含量都较高的主食，粥比饭能更好的减少食物摄入，适宜减肥期间食用。",
				NutritionCarbohydrate: &nutrition,
				NutritionFat:          &nutrition,
				NutritionProtein:      &nutrition,
				NutritionFibre:        &nutrition,
				NutritionVitaminA:     &nutrition,
				NutritionVitaminC:     &nutrition,
				NutritionVitaminE:     &nutrition,
				NutritionCarotene:     &nutrition,
				NutritionThiamine:     &nutrition,
				NutritionRiboflavin:   &nutrition,
				NutritionNiacin:       &nutrition,
				NutritionCholesterol:  &nutrition,
				NutritionMagnesium:    &nutrition,
				Nutrition_calcium:     &nutrition,
				NutritionIron:         &nutrition,
				NutritionZinc:         &nutrition,
				NutritionCopper:       &nutrition,
				NutritionManganese:    &nutrition,
				NutritionPotassium:    &nutrition,
				NutritionPhosphorus:   &nutrition,
				NutritionSodium:       &nutrition,
				NutritionSelenium:     &nutrition,
			},
		},
	})

	s.setDocFun(a, function)

	return function
}

func (s *Dict) FoodSearch(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	filter := &doctorModel.FoodSearchFilter{}
	a.GetArgument(r, filter)

	data, be := s.doctorBusiness.Dict().FoodSearch(filter)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Dict) FoodSearchDoc(a document.Assistant) document.Function {
	nutrition := 1.0
	function := a.CreateFunction("搜索食物")
	function.SetNote("搜索食物")
	function.SetInputExample(nil)
	function.SetOutputExample([]*doctorModel.ExpFoodDict{
		{
			SerialNo:              3,
			Category:              1,
			Name:                  "白粥,又叫:白粥（粳米），稀饭，大米粥，白米粥，米粥，大米汤汤",
			ImageUrl:              "https://exp.zjubiomedit.com/images/food3.png",
			Calorie:               46,
			Recommended:           0,
			Comment:               "一种碳水化合物和水分含量都较高的主食，粥比饭能更好的减少食物摄入，适宜减肥期间食用。",
			NutritionCarbohydrate: &nutrition,
			NutritionFat:          &nutrition,
			NutritionProtein:      &nutrition,
			NutritionFibre:        &nutrition,
			NutritionVitaminA:     &nutrition,
			NutritionVitaminC:     &nutrition,
			NutritionVitaminE:     &nutrition,
			NutritionCarotene:     &nutrition,
			NutritionThiamine:     &nutrition,
			NutritionRiboflavin:   &nutrition,
			NutritionNiacin:       &nutrition,
			NutritionCholesterol:  &nutrition,
			NutritionMagnesium:    &nutrition,
			Nutrition_calcium:     &nutrition,
			NutritionIron:         &nutrition,
			NutritionZinc:         &nutrition,
			NutritionCopper:       &nutrition,
			NutritionManganese:    &nutrition,
			NutritionPotassium:    &nutrition,
			NutritionPhosphorus:   &nutrition,
			NutritionSodium:       &nutrition,
			NutritionSelenium:     &nutrition,
		},
	})

	s.setDocFun(a, function)

	return function
}

func (s *Dict) UploadBaseJinScale(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &jinscale.BaseScale{}
	a.GetArgument(r, argument)

	be := s.doctorBusiness.Dict().UploadBaseJinScale(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(true)
}

func (s *Dict) UploadWeightJinScale(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &jinscale.WeightScale{}
	a.GetArgument(r, argument)

	be := s.doctorBusiness.Dict().UploadWeightJinScale(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(true)
}

func (s *Dict) UploadDietJinScale(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &jinscale.DietScale{}
	a.GetArgument(r, argument)

	be := s.doctorBusiness.Dict().UploadDietJinScale(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(true)
}

func (s *Dict) UploadExerciseJinScale(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &jinscale.ExerciseScale{}
	a.GetArgument(r, argument)

	be := s.doctorBusiness.Dict().UploadExerciseJinScale(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(true)
}

func (s *Dict) UploadFitnessJinScale(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &jinscale.FitnessScale{}
	a.GetArgument(r, argument)

	be := s.doctorBusiness.Dict().UploadFitnessJinScale(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(true)
}

func (s *Dict) UploadPostureJinScale(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &jinscale.PostureScale{}
	a.GetArgument(r, argument)

	be := s.doctorBusiness.Dict().UploadPostureJinScale(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(true)
}

func (s *Dict) UploadFitnessEndJinScale(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &jinscale.FitnessEndScale{}
	a.GetArgument(r, argument)

	be := s.doctorBusiness.Dict().UploadFitnessEndJinScale(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(true)
}

func (s *Dict) UploadPostureEndJinScale(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &jinscale.PostureEndScale{}
	a.GetArgument(r, argument)

	be := s.doctorBusiness.Dict().UploadPostureEndJinScale(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(true)
}

func (s *Dict) UploadEndJinScale(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &jinscale.EndScale{}
	a.GetArgument(r, argument)

	be := s.doctorBusiness.Dict().UploadEndJinScale(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(true)
}

func (s *Dict) UploadNewEndJinScale(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &jinscale.NewEndScale{}
	a.GetArgument(r, argument)

	be := s.doctorBusiness.Dict().UploadNewEndJinScale(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(true)
}

func (s *Dict) UploadBaseHealthJinScale(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &jinscale.BaseHealthScale{}
	a.GetArgument(r, argument)

	be := s.doctorBusiness.Dict().UploadBaseHealthJinScale(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(true)
}

func (s *Dict) GetKnowledgeLinkList(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	data, be := s.doctorBusiness.Dict().GetKnowledgeLinkList()
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(data)
}

func (s *Dict) UploadSportsTeamJinScale(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &jinscale.SportsTeamScale{}
	a.GetArgument(r, argument)

	be := s.doctorBusiness.Dict().UploadSportsTeamJinScale(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(true)

}

func (s *Dict) UploadTestWangScale(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	argument := &jinscale.TestWangScale{}
	query := r.URL.Query()
	argument.Timestamp = query["timestamp"][0]
	argument.AppKey = query["app_key"][0]
	params := query["params"][0]
	fmt.Println(params)
	fmt.Println([]byte(params))
	json.Unmarshal([]byte(params), &argument.Params)
	//println(argument)
	//err := json.NewDecoder(a.).Decode(argument)
	//if err != nil {
	//	a.Error(errors.InternalError, err)
	//	return
	//}
	//	s.input, err = json.Marshal(v)
	be := s.doctorBusiness.Dict().UploadTestWangScale(argument)
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}

	a.Success(true)
}

func (s *Dict) GetCampList(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {

	data, be := s.doctorBusiness.Dict().GetCampList()
	if be != nil {
		a.Error(be.Error(), be.Detail())
		return
	}
	a.Success(data)

}
