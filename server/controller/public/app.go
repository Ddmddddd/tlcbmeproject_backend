package public

import (
	"bytes"
	"tlcbme_project/business/doctor"
	"tlcbme_project/business/patient"
	doctorModel "tlcbme_project/data/model/doctor"
	"tlcbme_project/errors"
	"tlcbme_project/server/config"
	"encoding/base64"
	"fmt"
	"github.com/boombuler/barcode/qr"
	"github.com/ktpswjz/httpserver/document"
	"github.com/ktpswjz/httpserver/router"
	"github.com/ktpswjz/httpserver/types"
	"image/png"
	"net/http"
	"path/filepath"
	"time"
)

type App struct {
	public

	sitePrefix string
}

func NewApp(cfg *config.Config, log types.Log, doctorBusiness doctor.Business, patientBusiness patient.Business, sitePrefix string) *App {
	instance := &App{}
	instance.SetLog(log)
	instance.Config = cfg
	instance.doctorBusiness = doctorBusiness
	instance.patientBusiness = patientBusiness
	instance.sitePrefix = sitePrefix

	return instance
}

func (s *App) setDocFun(a document.Assistant, fun document.Function) {
	fun.IgnoreToken(true)

	catalog := s.rootCatalog(a).
		CreateChild("移动应用", "移动应用相关接口")
	catalog.SetFunction(fun)
}

func (s *App) GetAndroidInfo(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	filePath := filepath.Join(s.Config.Install.Root, s.Config.Install.Android.FolderName, "info.json")
	info := &doctorModel.AppVersionHistoryEx{}
	err := info.LoadFromFile(filePath)
	if err != nil {
		a.Error(errors.InternalError, err)
		return
	}
	info.Url = fmt.Sprintf("%s://%s%s/%s/%s",
		a.Schema(), r.Host, s.sitePrefix, s.Config.Install.Android.FolderName, s.Config.Install.Android.FileName)

	code, err := qr.Encode(info.Url, qr.H, qr.Auto)
	if err == nil {
		var buf bytes.Buffer
		err = png.Encode(&buf, code)
		if err == nil {
			qrCode := base64.StdEncoding.EncodeToString(buf.Bytes())

			info.UrlCode = fmt.Sprintf("data:image/png;base64,%s", qrCode)
		}
	}

	a.Success(info)
}

func (s *App) GetAndroidInfoDoc(a document.Assistant) document.Function {
	now := types.Time(time.Now())

	function := a.CreateFunction("获取安卓安装包信息")
	function.SetNote("获取安卓安装包信息，成功时返回当前最新版本的信息")
	function.SetOutputExample(&doctorModel.AppVersionHistoryEx{
		AppVersionHistory: doctorModel.AppVersionHistory{
			SerialNo:       42,
			VersionCode:    1,
			VersionName:    ":1.0.1.1",
			UpdateDate:     &now,
			UpdateContent:  "修改Bug",
			IsForced:       0,
			CompMinVersion: 1,
		},
		Url: "http://tlcbme_project.vico-lab.com/download/android/hypertension.apk",
	})
	function.SetContentType("")

	s.setDocFun(a, function)

	return function
}
func (s *App) GetWechatInfo(w http.ResponseWriter, r *http.Request, p router.Params, a router.Assistant) {
	url := fmt.Sprintf("%s://%s%s/%s/%s",
		a.Schema(), r.Host, s.sitePrefix, s.Config.Install.Wechat.FolderName, s.Config.Install.Wechat.FileName)
	a.Success(url)
}
func (s *App) GetWechatInfoDoc(a document.Assistant) document.Function {
	url := fmt.Sprintf("%s/%s/%s",
		s.sitePrefix, s.Config.Install.Wechat.FolderName, s.Config.Install.Wechat.FileName)
	function := a.CreateFunction("获取微信二维码信息")
	function.SetNote("获取微信二维码信息，成功时返回当前最新二维码图片的url")
	function.SetOutputExample(url)
	function.SetContentType("")

	s.setDocFun(a, function)

	return function
}
