package public

import (
	"tlcbme_project/business/doctor"
	"tlcbme_project/business/patient"
	"tlcbme_project/server/config"
	"github.com/ktpswjz/httpserver/document"
	"github.com/ktpswjz/httpserver/types"
)

type Dict struct {
	public
}

func NewDict(cfg *config.Config, log types.Log, doctorBusiness doctor.Business, patientBusiness patient.Business) *Dict {
	instance := &Dict{}
	instance.SetLog(log)
	instance.Config = cfg
	instance.doctorBusiness = doctorBusiness
	instance.patientBusiness = patientBusiness

	return instance
}

func (s *Dict) setDocFun(a document.Assistant, fun document.Function) {
	catalog := s.rootCatalog(a).
		CreateChild("字典信息", "字典信息相关接口")
	catalog.SetFunction(fun)
}
