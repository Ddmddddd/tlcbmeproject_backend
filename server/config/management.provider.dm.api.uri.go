package config

type ManagementProviderDmApiUri struct {
	ManagementProviderApiUri
}

func (s *ManagementProviderDmApiUri) CopyTo(target *ManagementProviderDmApiUri) {
	if target == nil {
		return
	}
	s.ManagementProviderApiUri.CopyTo(&target.ManagementProviderApiUri)
}

func (s *ManagementProviderDmApiUri) CopyFrom(source *ManagementProviderDmApiUri) {
	if source == nil {
		return
	}
	s.ManagementProviderApiUri.CopyFrom(&source.ManagementProviderApiUri)
}
