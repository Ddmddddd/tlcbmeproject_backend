package config

type Site struct {
	Root     string       `json:"root" note:"物理路径"`
	Doc      SiteDoc      `json:"doc" note:"文档"`
	App      SiteApp      `json:"app" note:"应用程序站点"`
	Admin    SiteAdmin    `json:"admin" note:"管理站点"`
	Doctor   SiteDoctor   `json:"doctor" note:"医生站点，默认"`
	Patient  SitePatient  `json:"patient" note:"患者站点"`
	Callback SiteCallback `json:"callback" note:"服务管理回调站点"`
}
