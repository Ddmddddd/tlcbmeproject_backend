package config

type ManagementProviderDm struct {
	ManagementProvider

	Api ManagementProviderDmApi `json:"api" note:"接口信息"`
}

func (s *ManagementProviderDm) CopyTo(target *ManagementProviderDm) {
	if target == nil {
		return
	}
	s.ManagementProvider.CopyTo(&target.ManagementProvider)
}

func (s *ManagementProviderDm) CopyFrom(source *ManagementProviderDm) {
	if source == nil {
		return
	}
	s.ManagementProvider.CopyFrom(&source.ManagementProvider)
}
