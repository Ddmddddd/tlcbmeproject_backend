package config

type ManagementProviderAuth struct {
	ID         string `json:"id" note:"机构标识"`
	Type       string `json:"type" note:"认证方式, 默认JWT"`
	Algorithm  string `json:"algorithm" note:"签名算法, 默认HS256"`
	Secret     string `json:"secret" note:"密钥"`
	Expiration int64  `json:"expiration" note:"凭证有效期, 单位妙, 默认300(5分钟)"`
}
