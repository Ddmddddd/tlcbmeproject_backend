package config

type PlatformProvider struct {
	PlatformProviderInfo

	Api PlatformProviderApi `json:"api" note:"接口信息"`
}

func (s *PlatformProvider) CopyTo(target *PlatformProvider) {
	if target == nil {
		return
	}
	s.PlatformProviderInfo.CopyTo(&target.PlatformProviderInfo)
}

func (s *PlatformProvider) CopyFrom(source *PlatformProvider) {
	if source == nil {
		return
	}
	s.PlatformProviderInfo.CopyFrom(&source.PlatformProviderInfo)
}
