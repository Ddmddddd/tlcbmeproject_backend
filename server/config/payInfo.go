package config

type PayInfo struct {
	Cert   string  `json:"cert"`
	Key    string  `json:"key"`
	ApiKey string  `json:"apiKey"`
	AppId  string  `json:"appId"`
	MchId  string  `json:"mchId"`
}