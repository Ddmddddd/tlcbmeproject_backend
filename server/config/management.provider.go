package config

type ManagementProvider struct {
	ID   string                 `json:"id" note:"服务提供者标识ID"`
	Auth ManagementProviderAuth `json:"auth" note:"授权信息"`
}

func (s *ManagementProvider) CopyTo(target *ManagementProvider) {
	if target == nil {
		return
	}
	target.ID = s.ID

	target.Auth.ID = s.Auth.ID
	target.Auth.Type = s.Auth.Type
	target.Auth.Algorithm = s.Auth.Algorithm
	target.Auth.Secret = s.Auth.Secret
	target.Auth.Expiration = s.Auth.Expiration
}

func (s *ManagementProvider) CopyFrom(source *ManagementProvider) {
	if source == nil {
		return
	}
	s.ID = source.ID

	s.Auth.ID = source.Auth.ID
	s.Auth.Type = source.Auth.Type
	s.Auth.Algorithm = source.Auth.Algorithm
	s.Auth.Secret = source.Auth.Secret
	s.Auth.Expiration = source.Auth.Expiration
}
