package config

import "fmt"

type DatabaseMysql struct {
	Server   string `json:"server" note:"服务器名称或IP, 默认127.0.0.1"`
	Port     int    `json:"port" note:"服务器端口, 默认3306"`
	Schema   string `json:"schema" note:"数据库名称, 默认cdmwb"`
	Charset  string `json:"charset" note:"字符集, 默认utf8"`
	TimeOut  int    `json:"timeOut" note:"连接超时时间，单位秒，默认10"`
	User     string `json:"user" note:"登录名"`
	Password string `json:"password" note:"登陆密码"`
}

func (s *DatabaseMysql) DriverName() string {
	return "mysql"
}

func (s *DatabaseMysql) SourceName() string {
	return fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?charset=%s&timeout=%ds&parseTime=true&loc=Local",
		s.User,
		s.Password,
		s.Server,
		s.Port,
		s.Schema,
		s.Charset,
		s.TimeOut)
}

func (s *DatabaseMysql) CopyFrom(source *DatabaseMysql) {
	if source == nil {
		return
	}

	s.Server = source.Server
	s.Port = source.Port
	s.Schema = source.Schema
	s.Charset = source.Charset
	s.TimeOut = source.TimeOut
	s.User = source.User
	s.Password = source.Password
}
