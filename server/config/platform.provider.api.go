package config

type PlatformProviderBaseApi struct {
	BaseUrl string `json:"baseUrl" note:"基地址，如https://www.medevice.pro"`
}

func (s *PlatformProviderBaseApi) CopyTo(target *PlatformProviderBaseApi) {
	if target == nil {
		return
	}
	target.BaseUrl = s.BaseUrl
}

func (s *PlatformProviderBaseApi) CopyFrom(source *PlatformProviderBaseApi) {
	if source == nil {
		return
	}
	s.BaseUrl = source.BaseUrl
}
