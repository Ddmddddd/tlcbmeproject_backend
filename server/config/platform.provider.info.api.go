package config

type PlatformProviderApi struct {
	PlatformProviderBaseApi

	Uri PlatformProviderApiUri `json:"uri" note:"路径"`
}

func (s *PlatformProviderApi) CopyTo(target *PlatformProviderApi) {
	if target == nil {
		return
	}
	s.PlatformProviderBaseApi.CopyTo(&target.PlatformProviderBaseApi)
	s.Uri.CopyTo(&target.Uri)
}

func (s *PlatformProviderApi) CopyFrom(source *PlatformProviderApi) {
	if source == nil {
		return
	}
	s.PlatformProviderBaseApi.CopyFrom(&source.PlatformProviderBaseApi)
	s.Uri.CopyFrom(&source.Uri)
}
