package config

type ManagementProviderApiUri struct {
	RegPatient string `json:"regPatient" note:"患者注册，如/1.0/patient/regpatient"`
	RegDisease string `json:"regDisease" note:"患者疾病注册，如/1.0/patient/regdisease"`

	UpdateSetting       string `json:"updateSetting" note:"更新指定患者的管理设定，如/1.0/patient/update/setting"`
	UploadBloodPressure string `json:"uploadBloodPressure" note:"上传患者测量的血压数据，如/1.0/patient/measure"`
	UploadBloodGlucose  string `json:"uploadBloodGlucose" note:"上传患者测量的血糖数据，如/1.0/patient/measure"`
	UploadDiscomfort    string `json:"uploadDiscomfort" note:"上传患者测量的不适记录，如/1.0/patient/discomfort"`
	DeleteRecord        string `json:"deleteRecord" note:"删除患者提交的血糖数据，如/1.0/patient/delete/record"`
	UploadFollowup      string `json:"uploadFollowup" note:"提交患者随访结果数据，如/1.0/patient/followup"`
	UploadAssess        string `json:"uploadAssess" note:"上传患者危险评估数据，如/1.0/patient/riskassess"`
	StopService         string `json:"stopService" note:"终止高血压管理，如/1.0/hbp/patient/service/stop"`
	StartService        string `json:"startService"  note:"重新启动高血压管理，如/1.0/hbp/patient/service/start"`
	DeleteDiscomfort    string `json:"deleteDiscomfort" note:"删除患者不适症数据，如/1.0/dm/patient/delete/discomfort" `

	RegExp 				 		string `json:"regExp" note:"患者纳入实验，如/1.0/dm/patient/regExp"`
	ChangeExpStatus		 		string `json:"changeExpStatus" note:"患者纳入实验，如/1.0/dm/patient/exp/change/status"`
	UploadActionPlanData 		string `json:"uploadActionPlanData" note:"上传患者的生活计划数据，如/1.0/dm/patient/exp/action/plan/record/commit"`
	UploadActionPlanListData 	string `json:"uploadActionPlanListData" note:"上传患者的生活计划列表数据，如/1.0/dm/patient/exp/action/plan/record/list/commit"`
	CommitPatientBarrierInfo	string `json:"commitPatientBarrierInfo" note:"上传患者的健康饮食障碍信息，如/1.0/dm/patient/exp/scale/record/commit"`
	EditActionPlan				string `json:"editActionPlan" note:"修改患者的生活计划，如/1.0/dm/patient/exp/action/plan/edit"`
	EditActionPlanRecord		string `json:"editActionPlanRecord" note:"修改患者的生活计划记录，如/1.0/dm/patient/exp/action/plan/record/edit"`
}

func (s *ManagementProviderApiUri) CopyTo(target *ManagementProviderApiUri) {
	if target == nil {
		return
	}
	target.RegPatient = s.RegPatient
	target.RegDisease = s.RegDisease

	target.UpdateSetting = s.UpdateSetting
	target.UploadBloodPressure = s.UploadBloodPressure
	target.UploadBloodGlucose = s.UploadBloodGlucose
	target.UploadDiscomfort = s.UploadDiscomfort
	target.DeleteRecord = s.DeleteRecord
	target.UploadFollowup = s.UploadFollowup
	target.UploadAssess = s.UploadAssess
	target.StartService = s.StartService
	target.StopService = s.StopService
	target.DeleteRecord = s.DeleteRecord
	target.DeleteDiscomfort = s.DeleteDiscomfort
	target.RegExp = s.RegExp
}

func (s *ManagementProviderApiUri) CopyFrom(source *ManagementProviderApiUri) {
	if source == nil {
		return
	}
	s.RegPatient = source.RegPatient
	s.RegDisease = source.RegDisease

	s.UpdateSetting = source.UpdateSetting
	s.UploadBloodPressure = source.UploadBloodPressure
	s.UploadBloodGlucose = source.UploadBloodGlucose
	s.UploadDiscomfort = source.UploadDiscomfort
	s.DeleteRecord = source.DeleteRecord
	s.UploadFollowup = source.UploadFollowup
	s.UploadAssess = source.UploadAssess
	s.StopService = source.StopService
	s.StartService = source.StartService
	s.DeleteDiscomfort = source.DeleteDiscomfort
	s.RegExp = source.RegExp
}
