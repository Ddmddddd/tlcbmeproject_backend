package config

type InstallBrowser struct {
	FolderName string `json:"folderName" note:"版本文件夹名称，默认：browser"`

	Chrome InstallBrowserSetup `json:"chrome" note:"谷歌浏览器"`
}
