package config

import "encoding/json"

type PlatformFilter struct {
	ProviderType ManagementProviderType `json:"providerType" required:"true" note:"提供者类型，1-高血压；2-糖尿病"`
}

type PlatformArgument struct {
	Data interface{} `json:"data" note:"数据"`
}

func (s *PlatformArgument) GetData(v interface{}) error {
	data, err := json.Marshal(s.Data)
	if err != nil {
		return err
	}

	return json.Unmarshal(data, v)
}
