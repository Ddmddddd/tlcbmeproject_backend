package config

type ManagementProviderApi struct {
	BaseUrl string `json:"baseUrl" note:"基地址，如http://192.168.1.1:8080/svc"`
}

func (s *ManagementProviderApi) CopyTo(target *ManagementProviderApi) {
	if target == nil {
		return
	}
	target.BaseUrl = s.BaseUrl
}

func (s *ManagementProviderApi) CopyFrom(source *ManagementProviderApi) {
	if source == nil {
		return
	}
	s.BaseUrl = source.BaseUrl
}
