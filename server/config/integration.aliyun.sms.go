package config

type IntegrationAliyunSms struct {
	Url          string `json:"url" note:"接口地址"`
	AccessKeyId  string `json:"accessKeyId" note:"ID"`
	AccessSecret string `json:"accessSecret" note:"签名密钥"`

	Templates IntegrationAliyunSmsTemplates `json:"templates" note:"模板"`
}

func (s *IntegrationAliyunSms) CopyFrom(source *IntegrationAliyunSms) {
	if source == nil {
		return
	}

	s.Url = source.Url
	s.AccessKeyId = source.AccessKeyId
	s.AccessSecret = source.AccessSecret
	s.Templates.CCP.Sign = source.Templates.CCP.Sign
}

func (s *IntegrationAliyunSms) CopyTo(target *IntegrationAliyunSms) bool {
	if target == nil {
		return false
	}

	isChange := false
	if target.Url != s.Url {
		target.Url = s.Url
		isChange = true
	}
	if target.AccessKeyId != s.AccessKeyId {
		target.AccessKeyId = s.AccessKeyId
		isChange = true
	}
	if target.AccessSecret != s.AccessSecret {
		target.AccessSecret = s.AccessSecret
		isChange = true
	}
	if target.Templates.CCP.Sign != s.Templates.CCP.Sign {
		target.Templates.CCP.Sign = s.Templates.CCP.Sign
		isChange = true
	}

	return isChange
}
