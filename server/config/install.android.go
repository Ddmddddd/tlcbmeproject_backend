package config

type InstallAndroid struct {
	FileName   string `json:"fileName" note:"文件名，默认：hypertension.apk"`
	FolderName string `json:"folderName" note:"版本文件夹名称，默认：android"`
}
