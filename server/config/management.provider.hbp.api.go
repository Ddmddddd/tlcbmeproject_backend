package config

type ManagementProviderHbpApi struct {
	ManagementProviderApi

	Uri ManagementProviderHbpApiUri `json:"uri" note:"路径"`
}

func (s *ManagementProviderHbpApi) CopyTo(target *ManagementProviderHbpApi) {
	if target == nil {
		return
	}
	s.ManagementProviderApi.CopyTo(&target.ManagementProviderApi)
	s.Uri.CopyTo(&target.Uri)
}

func (s *ManagementProviderHbpApi) CopyFrom(source *ManagementProviderHbpApi) {
	if source == nil {
		return
	}
	s.ManagementProviderApi.CopyFrom(&source.ManagementProviderApi)
	s.Uri.CopyFrom(&source.Uri)
}
