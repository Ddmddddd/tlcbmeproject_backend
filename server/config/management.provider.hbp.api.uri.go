package config

type ManagementProviderHbpApiUri struct {
	ManagementProviderApiUri
}

func (s *ManagementProviderHbpApiUri) CopyTo(target *ManagementProviderHbpApiUri) {
	if target == nil {
		return
	}
	s.ManagementProviderApiUri.CopyTo(&target.ManagementProviderApiUri)
}

func (s *ManagementProviderHbpApiUri) CopyFrom(source *ManagementProviderHbpApiUri) {
	if source == nil {
		return
	}
	s.ManagementProviderApiUri.CopyFrom(&source.ManagementProviderApiUri)
}
