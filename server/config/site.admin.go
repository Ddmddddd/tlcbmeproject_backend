package config

type SiteAdmin struct {
	Enable bool            `json:"enable" note:"是否启用"`
	Root   string          `json:"root" note:"物理路径"`
	Api    SiteAdminApi    `json:"api" note:"接口"`
	Users  []SiteAdminUser `json:"users" note:"用户"`
	Ldap   SiteAdminLdap   `json:"ldap" note:"LDAP验证"`
}
