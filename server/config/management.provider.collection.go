package config

type ManagementProviderType int

const (
	ManagementProviderTypeUnknown ManagementProviderType = 0 // 未知
	ManagementProviderTypeHbp     ManagementProviderType = 1 // 高血压
	ManagementProviderTypeDm      ManagementProviderType = 2 // 糖尿病
)

type ManagementProviderCollection struct {
	Hbp ManagementProviderHbp `json:"hbp" note:"高血压服务提供者"`
	Dm  ManagementProviderDm  `json:"dm" note:"糖尿病服务提供者"`
}

func (s *ManagementProviderCollection) GetType(subject string) ManagementProviderType {
	if subject == s.Hbp.ID {
		return ManagementProviderTypeHbp
	} else if subject == s.Dm.ID {
		return ManagementProviderTypeDm
	}

	return ManagementProviderTypeUnknown
}
