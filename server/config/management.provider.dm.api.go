package config

type ManagementProviderDmApi struct {
	ManagementProviderApi

	Uri ManagementProviderDmApiUri `json:"uri" note:"路径"`
}

func (s *ManagementProviderDmApi) CopyTo(target *ManagementProviderDmApi) {
	if target == nil {
		return
	}
	s.ManagementProviderApi.CopyTo(&target.ManagementProviderApi)
	s.Uri.CopyTo(&target.Uri)
}

func (s *ManagementProviderDmApi) CopyFrom(source *ManagementProviderDmApi) {
	if source == nil {
		return
	}
	s.ManagementProviderApi.CopyFrom(&source.ManagementProviderApi)
	s.Uri.CopyFrom(&source.Uri)
}
