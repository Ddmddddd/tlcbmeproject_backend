package config

type ManagementProviderHbp struct {
	ManagementProvider

	Api ManagementProviderHbpApi `json:"api" note:"接口信息"`
}

func (s *ManagementProviderHbp) CopyTo(target *ManagementProviderHbp) {
	if target == nil {
		return
	}
	s.ManagementProvider.CopyTo(&target.ManagementProvider)
}

func (s *ManagementProviderHbp) CopyFrom(source *ManagementProviderHbp) {
	if source == nil {
		return
	}
	s.ManagementProvider.CopyFrom(&source.ManagementProvider)
}
