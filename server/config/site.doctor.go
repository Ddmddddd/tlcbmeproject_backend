package config

type SiteDoctor struct {
	Root string         `json:"root" note:"物理路径"`
	Api  SiteDoctorApi  `json:"api" note:"接口"`
	User SiteDoctorUser `json:"user" note:"用户"`

	ShowAppDownloadCodeInLoginPage bool `json:"showAppDownloadCodeInLoginPage" note:"是否在登录页面显示App下载二维码"`
}
