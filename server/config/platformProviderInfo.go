package config

type PlatformProviderInfo struct {
	Token     string `json:"token" note:"平台身份认证token"`
	AppId     string `json:"appId"`
	AppSecret string `json:"appSecret"`
}

func (s *PlatformProviderInfo) CopyTo(target *PlatformProviderInfo) {
	if target == nil {
		return
	}
	target.Token = s.Token
	target.AppId = s.AppId
	target.AppSecret = s.AppSecret
}

func (s *PlatformProviderInfo) CopyFrom(source *PlatformProviderInfo) {
	if source == nil {
		return
	}
	s.Token = source.Token
	s.AppId = source.AppId
	s.AppSecret = source.AppSecret

}
