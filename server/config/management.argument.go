package config

import "encoding/json"

type ManagementFilter struct {
	ProviderType ManagementProviderType `json:"providerType" required:"true" note:"提供者类型，1-高血压；2-糖尿病"`
}

type ManagementArgument struct {
	ProviderType ManagementProviderType `json:"providerType" required:"true" note:"提供者类型，1-高血压；2-糖尿病"`
	Data         interface{}            `json:"data" note:"数据"`
}

func (s *ManagementArgument) GetData(v interface{}) error {
	data, err := json.Marshal(s.Data)
	if err != nil {
		return err
	}

	return json.Unmarshal(data, v)
}
