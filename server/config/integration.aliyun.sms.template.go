package config

type IntegrationAliyunSmsTemplate struct {
	Code string `json:"code" note:"模板代码"`
	Sign string `json:"sign" note:"模板签名"`
}

func (s *IntegrationAliyunSmsTemplate) CopyFrom(source *IntegrationAliyunSmsTemplate) {
	if source == nil {
		return
	}

	s.Code = source.Code
	s.Sign = source.Sign
}

func (s *IntegrationAliyunSmsTemplate) CopyTo(target *IntegrationAliyunSmsTemplate) bool {
	if target == nil {
		return false
	}

	isChange := false
	if target.Code != s.Code {
		target.Code = s.Code
		isChange = true
	}
	if target.Sign != s.Sign {
		target.Sign = s.Sign
		isChange = true
	}

	return isChange
}
