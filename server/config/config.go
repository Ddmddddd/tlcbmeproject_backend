package config

import (
	"encoding/json"
	"fmt"
	"github.com/ktpswjz/httpserver/http/server/configure"
	"github.com/ktpswjz/httpserver/types"
	"io/ioutil"
	"os"
	"path/filepath"
	"sync"
)

type Config struct {
	path  string // file path to be loaded
	mutex sync.RWMutex
	args  *types.Args

	Service           string           `json:"service" note:"服务名称，默认cdmwb"`
	Name              string           `json:"name" note:"服务平台名称"`
	Log               Log              `json:"log" note:"日志"`
	Server            configure.Server `json:"server" note:"服务器"`
	Database          Database         `json:"database" note:"数据库"`
	NutritionDatabase Database         `json:"nutritionDatabase" note:"营养数据库"`
	Site              Site             `json:"site" note:"网站"`
	Install           Install          `json:"install" note:"安装包"`
	SmsTask           Task             `json:"task" note:"短信任务时间配置"`

	StatisticalTask Task `json:"task" note:"短信任务时间配置"`

	HistoryConfig HistoryConfig `json:"historyConfig" note:"数据统计历史数据处理"`

	Management        Management        `json:"management" note:"管理服务"`
	Platform          Platform          `json:"upload" note:"信息平台服务"`
	IntegrationAliyun IntegrationAliyun `json:"integrationAliyun" note:"阿里云配置"`
	WeChatAppInfo     WeChatAppInfo     `json:"weChatAppInfo" note:"微信小程序信息"`
	PayInfo           PayInfo           `json:"payInfo" note:"支付配置"`
}

func NewConfig() *Config {
	return &Config{
		Service: "tlcbme_project",
		Name:    "慢病管理平台",
		Log: Log{
			Folder: "",
			Level:  "error|warning|info",
		},
		Server: configure.Server{
			Http: configure.Http{
				Enabled: true,
				Address: "",
				Port:    "8088",
			},
			Https: configure.Https{
				Enabled: false,
				Address: "",
				Port:    "8443",
				Cert: configure.Certificate{
					File:     "",
					Password: "",
				},
			},
		},
		Database: Database{
			Mysql: DatabaseMysql{
				Server:   "127.0.0.1",
				Port:     3306,
				Schema:   "tlcbme_project",
				Charset:  "utf8",
				User:     "root",
				Password: "zjubio2019",
				TimeOut:  10,
			},
		},
		NutritionDatabase: Database{
			Mysql: DatabaseMysql{
				Server:   "127.0.0.1",
				Port:     3306,
				Schema:   "fooddb-local",
				Charset:  "utf8",
				User:     "root",
				Password: "root",
				TimeOut:  10,
			},
		},
		Site: Site{
			Root: "",
			Doc: SiteDoc{
				Enable: true,
				Root:   "",
			},
			App: SiteApp{
				Root: "",
			},
			Admin: SiteAdmin{
				Enable: true,
				Root:   "",
				Api: SiteAdminApi{
					Token: Token{
						Expiration: 30,
					},
				},
				Users: []SiteAdminUser{
					{
						Account:  "admin",
						Password: "1",
					},
				},
				Ldap: SiteAdminLdap{
					Enable: true,
					Host:   "vico-lab.com",
					Port:   389,
					Base:   "dc=vico-lab,dc=com",
				},
			},
			Doctor: SiteDoctor{
				Root: "",
				Api: SiteDoctorApi{
					Token: Token{
						Expiration: 30,
					},
				},
				User: SiteDoctorUser{
					PasswordFormat: 11,
				},
			},
			Patient: SitePatient{
				Root: "",
				Api: SitePatientApi{
					Token: Token{
						Expiration: 30,
					},
				},
				User: SitePatientUser{
					PasswordFormat: 11,
				},
			},
			Callback: SiteCallback{
				Root: "",
			},
		},
		Install: Install{
			Root: "",
			Android: InstallAndroid{
				FileName:   "hypertension.apk",
				FolderName: "android",
			},
			Wechat: InstallWechat{
				FileName:   "qrcode.png",
				FolderName: "wechat",
			},
			Browser: InstallBrowser{
				FolderName: "browser",
				Chrome: InstallBrowserSetup{
					FileName:   "ChromeStandaloneSetup.exe",
					FolderName: "chrome",
				},
			},
		},
		Management: Management{
			Providers: ManagementProviderCollection{
				Hbp: ManagementProviderHbp{
					ManagementProvider: ManagementProvider{
						ID: "",
						Auth: ManagementProviderAuth{
							ID:         "",
							Type:       "JWT",
							Algorithm:  "HS256",
							Secret:     "",
							Expiration: 300,
						},
					},

					Api: ManagementProviderHbpApi{
						ManagementProviderApi: ManagementProviderApi{
							BaseUrl: "",
						},
						Uri: ManagementProviderHbpApiUri{
							ManagementProviderApiUri: ManagementProviderApiUri{
								RegPatient:          "/patient/regpatient",
								RegDisease:          "/patient/regdisease",
								UpdateSetting:       "/hbp/patient/update/setting",
								UploadBloodPressure: "/hbp/patient/measure",
								UploadDiscomfort:    "/hbp/patient/discomfort",
								DeleteRecord:        "/hbp/patient/delete/record",
								UploadFollowup:      "/hbp/patient/daily/followup",
								UploadAssess:        "/hbp/patient/riskassess",
								StartService:        "/hbp/patient/service/start",
								StopService:         "/hbp/patient/service/stop",
								DeleteDiscomfort:    "/hbp/patient/delete/discomfort",
							},
						},
					},
				},

				Dm: ManagementProviderDm{
					ManagementProvider: ManagementProvider{
						ID: "",
						Auth: ManagementProviderAuth{
							ID:         "",
							Type:       "JWT",
							Algorithm:  "HS256",
							Secret:     "",
							Expiration: 300,
						},
					},

					Api: ManagementProviderDmApi{
						ManagementProviderApi: ManagementProviderApi{
							BaseUrl: "",
						},
						Uri: ManagementProviderDmApiUri{
							ManagementProviderApiUri: ManagementProviderApiUri{
								RegPatient:         "/patient/regpatient",
								RegDisease:         "/patient/regdisease",
								UpdateSetting:      "/dm/patient/update/setting",
								UploadBloodGlucose: "/dm/patient/measure",
								DeleteRecord:       "/dm/patient/delete/record",
								UploadFollowup:     "/dm/patient/daily/followup",
								UploadAssess:       "/dm/patient/riskAssess",
								StartService:       "/dm/patient/service/start",
								StopService:        "/dm/patient/service/stop",
								UploadDiscomfort:   "/dm/patient/discomfort",
								DeleteDiscomfort:   "/dm/patient/delete/discomfort",
							},
						},
					},
				},
			},
		},
		Platform: Platform{
			Providers: PlatformProvider{
				PlatformProviderInfo: PlatformProviderInfo{
					Token:     "37ce1223-46af-4fcc-b938-b7d740479a44",
					AppId:     "chronicsystem",
					AppSecret: "9a6747fc6259aa374ab4e1bb03074b6ec672cf99",
				},
				Api: PlatformProviderApi{
					Uri: PlatformProviderApiUri{
						UploadData: "/v1/application/chronic/",
						UserData:   "/userinfo",
						TokenData:  "/oauth2/token",
					},
					PlatformProviderBaseApi: PlatformProviderBaseApi{
						BaseUrl: "https://www.medevice.pro",
					},
				},
			},
		},
		IntegrationAliyun: IntegrationAliyun{
			Sms: IntegrationAliyunSms{
				Url:          "http://dysmsapi.aliyuncs.com/",
				AccessKeyId:  "LTAI4Fd8eNtX67jEg7Md7WL8",
				AccessSecret: "Uxib52NooDCEi5cZITs0d1nFfjJvag",
				Templates: IntegrationAliyunSmsTemplates{
					CCP: IntegrationAliyunSmsTemplate{
						Sign: "慢病微管家",
					},
				},
			},
		},
		SmsTask: Task{
			FixedTime: FixedTime{
				Hour:   8,
				Minute: 0,
				Second: 0,
			},
			IntervalTime: IntervalTime{
				Hour:   48,
				Minute: 0,
				Second: 0,
			},
		},
		WeChatAppInfo: WeChatAppInfo{
			AppId:     "wxdd20aa2fb7c5967c",
			AppSecret: "408df9d0604556f44e57a4ff157239b2",
		},
		PayInfo: PayInfo{
			Cert:   "/Users/ddm/cert/apiclient_cert.pem",
			Key:    "/Users/ddm/cert/apiclient_key.pem",
			ApiKey: "Tlc729946Tlc729946Tlc729946Tlc72",
			AppId:  "wx02ad655a859f9031",
			MchId:  "1578222071",
		},
		StatisticalTask: Task{
			FixedTime: FixedTime{
				Hour:   0,
				Minute: 0,
				Second: 0,
			},
			IntervalTime: IntervalTime{
				Hour:   24,
				Minute: 0,
				Second: 0,
			},
		},
		HistoryConfig: HistoryConfig{
			Year:  2021,
			Month: 1,
		},
	}
}

func (s *Config) SetPath(path string) {
	s.path = path
}

func (s *Config) GetPath() string {
	return s.path
}

func (s *Config) SetArgs(args *types.Args) {
	s.args = args
}

func (s *Config) GetArgs() *types.Args {
	return s.args
}

func (s *Config) GetServer() *configure.Server {
	return &s.Server
}

func (s *Config) LoadFromFile(filePath string) error {
	s.mutex.Lock()
	defer s.mutex.Unlock()

	bytes, err := ioutil.ReadFile(filePath)
	if err != nil {
		return err
	}

	return json.Unmarshal(bytes, s)
}

func (s *Config) SaveToFile(filePath string) error {
	s.mutex.Lock()
	defer s.mutex.Unlock()

	bytes, err := json.MarshalIndent(s, "", "    ")
	if err != nil {
		return err
	}

	fileFolder := filepath.Dir(filePath)
	_, err = os.Stat(fileFolder)
	if os.IsNotExist(err) {
		os.MkdirAll(fileFolder, 0777)
	}

	file, err := os.Create(filePath)
	if err != nil {
		return err
	}
	defer file.Close()

	_, err = fmt.Fprint(file, string(bytes[:]))

	return err
}

func (s *Config) String() string {
	bytes, err := json.Marshal(s)
	if err != nil {
		return ""
	}

	return string(bytes[:])
}

func (s *Config) FormatString() string {
	bytes, err := json.MarshalIndent(s, "", "    ")
	if err != nil {
		return ""
	}

	return string(bytes[:])
}
