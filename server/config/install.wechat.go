package config

type InstallWechat struct {
	FileName   string `json:"fileName" note:"文件名，默认：qrcode.png"`
	FolderName string `json:"folderName" note:"版本文件夹名称，默认：wechat"`
}
