package config

type PlatformProviderApiUri struct {
	UploadData string `json:"uploadData" note:"如/v1/application/chronic"`
	UserData   string `json:"userData" note:"/userinfo"`
	TokenData  string `json:"tokenData" note:"/oauth2/token"`
}

func (s *PlatformProviderApiUri) CopyTo(target *PlatformProviderApiUri) {
	if target == nil {
		return
	}
	target.UploadData = s.UploadData
	target.UserData = s.UserData
	target.TokenData = s.TokenData
}

func (s *PlatformProviderApiUri) CopyFrom(source *PlatformProviderApiUri) {
	if source == nil {
		return
	}
	s.UploadData = source.UploadData
	s.UserData = source.UserData
	s.TokenData = source.TokenData
}
