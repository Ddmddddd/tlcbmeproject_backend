package config

type SiteDoctorUser struct {
	PasswordFormat uint64 `json:"passwordFormat" note:"登录密码格式 0-明文，11-MD5(默认)，21-SHA1，22-SHA256，23-SHA384，24-SHA512"`
}
