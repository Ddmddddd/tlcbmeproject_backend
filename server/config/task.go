package config

type Task struct {
	FixedTime    FixedTime    `json:"fixedTime" note:"每日所固定的时间及，时间点"`
	IntervalTime IntervalTime `json:"intervalTime" note:"时间间隔"`
}

type HistoryConfig struct {
	Year  int `json:"year" note:"年份"`
	Month int `json:"month" note:"月份"`
}

type FixedTime struct {
	Hour   int `json:"hour" note:"几点"`
	Minute int `json:"minute" note:"几分钟"`
	Second int `json:"second" note:"几秒"`
}

type IntervalTime struct {
	Hour   int `json:"hour" note:"间隔几个小时"`
	Minute int `json:"minute" note:"间隔几分钟"`
	Second int `json:"second" note:"间隔几秒"`
}

func (s *Task) CopyFrom(source *Task) {
	if source == nil {
		return
	}

	s.FixedTime.Hour = source.FixedTime.Hour
	s.FixedTime.Minute = source.FixedTime.Minute
	s.FixedTime.Second = source.FixedTime.Second
	s.IntervalTime.Hour = source.IntervalTime.Hour
	s.IntervalTime.Minute = source.IntervalTime.Minute
	s.IntervalTime.Second = source.IntervalTime.Second
}

func (s *Task) CopyTo(target *Task) bool {
	if target == nil {
		return false
	}

	isChange := false
	if target.FixedTime.Hour != s.FixedTime.Hour {
		target.FixedTime.Hour = s.FixedTime.Hour
		isChange = true
	}
	if target.FixedTime.Minute != s.FixedTime.Minute {
		target.FixedTime.Minute = s.FixedTime.Minute
		isChange = true
	}
	if target.FixedTime.Second != s.FixedTime.Second {
		target.FixedTime.Second = s.FixedTime.Second
		isChange = true
	}
	if target.IntervalTime.Hour != s.IntervalTime.Hour {
		target.IntervalTime.Hour = s.IntervalTime.Hour
		isChange = true
	}
	if target.IntervalTime.Minute != s.IntervalTime.Minute {
		target.IntervalTime.Minute = s.IntervalTime.Minute
		isChange = true
	}
	if target.IntervalTime.Second != s.IntervalTime.Second {
		target.IntervalTime.Second = s.IntervalTime.Second
		isChange = true
	}

	return isChange
}

func (s *HistoryConfig) CopyFrom(source *HistoryConfig) {
	if source == nil {
		return
	}

	s.Year = source.Year
	s.Month = source.Month
}

func (s *HistoryConfig) CopyTo(target *HistoryConfig) bool {
	if target == nil {
		return false
	}

	isChange := false
	if target.Year != s.Year {
		target.Year = s.Year
		isChange = true
	}
	if target.Month != s.Month {
		target.Month = s.Month
		isChange = true
	}

	return isChange
}
