package config

type Database struct {
	Mysql DatabaseMysql `json:"mysql" note:"MYSQL数据库"`
}

func (s *Database) DriverName() string {
	return s.Mysql.DriverName()
}

func (s *Database) SourceName() string {
	return s.Mysql.SourceName()
}
