package config

type WeChatAppInfo struct {
	AppId  		string   `json:"appid" note:"小程序appid"`
	AppSecret	string	 `json:"appsecret" note:"小程序appsecret"`
}
