package config

type SitePatient struct {
	Root string          `json:"root" note:"物理路径"`
	Api  SitePatientApi  `json:"api" note:"接口"`
	User SitePatientUser `json:"user" note:"用户"`
}
