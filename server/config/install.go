package config

type Install struct {
	Root    string         `json:"root" note:"物理路径"`
	Android InstallAndroid `json:"android" note:"安卓"`
	Wechat  InstallWechat  `json:"wechat" note:"微信"`
	Browser InstallBrowser `json:"browser" note:"浏览器"`
}
