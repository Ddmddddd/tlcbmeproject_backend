package mysql

import (
	"tlcbme_project/database"
	"database/sql"
)

type nomal struct {
	access

	db *sql.DB
}

func (s *nomal) Close() error {
	return s.db.Close()
}

func (s *nomal) Commit() error {
	return nil
}

func (s *nomal) Exec(query string, args ...interface{}) (sql.Result, error) {
	return s.db.Exec(query, args...)
}

func (s *nomal) Prepare(query string) (*sql.Stmt, error) {
	return s.db.Prepare(query)
}

func (s *nomal) Query(query string, args ...interface{}) (*sql.Rows, error) {
	return s.db.Query(query, args...)
}

func (s *nomal) QueryRow(query string, args ...interface{}) *sql.Row {
	return s.db.QueryRow(query, args...)
}

func (s *nomal) IsNoRows(err error) bool {
	return s.isNoRows(err)
}

func (s *nomal) Insert(entity interface{}) (uint64, error) {
	return s.insert(s, false, entity)
}

func (s *nomal) InsertSelective(entity interface{}) (uint64, error) {
	return s.insert(s, true, entity)
}

func (s *nomal) Delete(entity interface{}, filters ...database.SqlFilter) (uint64, error) {
	return s.delete(s, entity, filters...)
}

func (s *nomal) Update(entity interface{}, filters ...database.SqlFilter) (uint64, error) {
	return s.update(s, false, entity, filters...)
}

func (s *nomal) UpdateSelective(entity interface{}, filters ...database.SqlFilter) (uint64, error) {
	return s.update(s, true, entity, filters...)
}

func (s *nomal) UpdateByPrimaryKey(entity interface{}) (uint64, error) {
	return s.updateByPrimaryKey(s, false, entity)
}

func (s *nomal) UpdateSelectiveByPrimaryKey(entity interface{}) (uint64, error) {
	return s.updateByPrimaryKey(s, true, entity)
}

func (s *nomal) SelectOne(entity interface{}, filters ...database.SqlFilter) error {
	return s.selectOne(s, entity, filters...)
}

func (s *nomal) SelectDistinct(entity interface{}, row func(), order interface{}, filters ...database.SqlFilter) error {
	return s.selectList(s, true, entity, row, order, filters...)
}

func (s *nomal) SelectList(entity interface{}, row func(), order interface{}, filters ...database.SqlFilter) error {
	return s.selectList(s, false, entity, row, order, filters...)
}

func (s *nomal) SelectPage(entity interface{}, page func(total, page, size, index uint64), row func(), size, index uint64, order interface{}, filters ...database.SqlFilter) error {
	return s.selectPage(s, entity, page, row, size, index, order, filters...)
}

func (s *nomal) SelectCount(dbEntity interface{}, filters ...database.SqlFilter) (uint64, error) {
	sqlEntity := &entity{}
	err := sqlEntity.Parse(dbEntity)
	if err != nil {
		return 0, err
	}

	return s.selectCount(s, sqlEntity.Name(), filters...)
}
