package mysql

import (
	"database/sql"
	"time"
	"tlcbme_project/database"

	_ "github.com/go-sql-driver/mysql"
)

type mysql struct {
	connection database.SqlConnection
}

func NewDatabase(conn database.SqlConnection) database.SqlDatabase {
	return &mysql{connection: conn}
}

func (s *mysql) Open() (*sql.DB, error) {
	db, err := sql.Open(s.connection.DriverName(), s.connection.SourceName())
	db.SetMaxIdleConns(0)
	db.SetConnMaxLifetime(time.Second)
	//db.SetConnMaxLifetime(time.Minute * 4)
	if err != nil {
		return nil, err
	}

	return db, nil
}

func (s *mysql) Test() (string, error) {
	db, err := sql.Open(s.connection.DriverName(), s.connection.SourceName())
	if err != nil {
		return "", err
	}
	defer db.Close()

	err = db.Ping()
	if err != nil {
		return "", err
	}

	dbVer := ""
	row := db.QueryRow("SELECT VERSION()")
	if row != nil {
		err = row.Scan(&dbVer)
		if err == nil {
			return dbVer, nil
		}
	}

	return "", nil
}

func (s *mysql) NewAccess(transactional bool) (database.SqlAccess, error) {
	db, err := sql.Open(s.connection.DriverName(), s.connection.SourceName())
	db.SetMaxIdleConns(0)
	db.SetConnMaxLifetime(time.Second * 1000)
	//db.SetConnMaxLifetime(time.Minute * 4)
	if err != nil {
		return nil, err
	}

	if transactional {
		tx, err := db.Begin()
		if err != nil {
			db.Close()
			return nil, err
		}

		return &transaction{db: db, tx: tx}, nil
	}

	return &nomal{db: db}, nil
}

func (s *mysql) NewEntity() database.SqlEntity {
	return &entity{}
}

func (s *mysql) NewBuilder() database.SqlBuilder {
	instance := &builder{}
	instance.Reset()

	return instance
}

func (s *mysql) NewFilter(entity interface{}, fieldOr, groupOr bool) database.SqlFilter {
	return newFilter(entity, fieldOr, groupOr)
}

func (s *mysql) IsNoRows(err error) bool {
	if err == nil {
		return false
	}

	if err == sql.ErrNoRows {
		return true
	}

	return false
}

func (s *mysql) Insert(entity interface{}) (uint64, error) {
	sqlAccess, err := s.NewAccess(false)
	if err != nil {
		return 0, err
	}
	defer sqlAccess.Close()

	return sqlAccess.Insert(entity)
}

func (s *mysql) InsertSelective(entity interface{}) (uint64, error) {
	sqlAccess, err := s.NewAccess(false)
	if err != nil {
		return 0, err
	}
	defer sqlAccess.Close()

	return sqlAccess.InsertSelective(entity)
}

func (s *mysql) Delete(entity interface{}, filters ...database.SqlFilter) (uint64, error) {
	sqlAccess, err := s.NewAccess(false)
	if err != nil {
		return 0, err
	}
	defer sqlAccess.Close()

	return sqlAccess.Delete(entity, filters...)
}

func (s *mysql) Update(entity interface{}, filters ...database.SqlFilter) (uint64, error) {
	sqlAccess, err := s.NewAccess(false)
	if err != nil {
		return 0, err
	}
	defer sqlAccess.Close()

	return sqlAccess.Update(entity, filters...)
}

func (s *mysql) UpdateSelective(entity interface{}, filters ...database.SqlFilter) (uint64, error) {
	sqlAccess, err := s.NewAccess(false)
	if err != nil {
		return 0, err
	}
	defer sqlAccess.Close()

	return sqlAccess.UpdateSelective(entity, filters...)
}

func (s *mysql) UpdateByPrimaryKey(entity interface{}) (uint64, error) {
	sqlAccess, err := s.NewAccess(false)
	if err != nil {
		return 0, err
	}
	defer sqlAccess.Close()

	return sqlAccess.UpdateByPrimaryKey(entity)
}

func (s *mysql) UpdateSelectiveByPrimaryKey(entity interface{}) (uint64, error) {
	sqlAccess, err := s.NewAccess(false)
	if err != nil {
		return 0, err
	}
	defer sqlAccess.Close()

	return sqlAccess.UpdateSelectiveByPrimaryKey(entity)
}

func (s *mysql) SelectOne(entity interface{}, filters ...database.SqlFilter) error {
	sqlAccess, err := s.NewAccess(false)
	if err != nil {
		return err
	}
	defer sqlAccess.Close()

	return sqlAccess.SelectOne(entity, filters...)
}

func (s *mysql) SelectDistinct(entity interface{}, row func(), order interface{}, filters ...database.SqlFilter) error {
	sqlAccess, err := s.NewAccess(false)
	if err != nil {
		return err
	}
	defer sqlAccess.Close()

	return sqlAccess.SelectDistinct(entity, row, order, filters...)
}

func (s *mysql) SelectList(entity interface{}, row func(), order interface{}, filters ...database.SqlFilter) error {
	sqlAccess, err := s.NewAccess(false)
	if err != nil {
		return err
	}
	defer sqlAccess.Close()

	return sqlAccess.SelectList(entity, row, order, filters...)
}

func (s *mysql) SelectPage(entity interface{}, page func(total, page, size, index uint64), row func(), size, index uint64, order interface{}, filters ...database.SqlFilter) error {
	sqlAccess, err := s.NewAccess(false)
	if err != nil {
		return err
	}
	defer sqlAccess.Close()

	return sqlAccess.SelectPage(entity, page, row, size, index, order, filters...)
}

func (s *mysql) SelectCount(entity interface{}, filters ...database.SqlFilter) (uint64, error) {
	sqlAccess, err := s.NewAccess(false)
	if err != nil {
		return 0, err
	}
	defer sqlAccess.Close()

	return sqlAccess.SelectCount(entity, filters...)
}
