package memory

import (
	"container/list"
	"fmt"
	"github.com/ktpswjz/httpserver/types"
	"sync"
	"time"
)

type Jwt interface {
	Add(jti string, exp int64) error
}

func NewJwt(expirationSeconds int64, log types.Log) Jwt {
	instance := &innerJwt{}
	instance.SetLog(log)
	instance.ids = make(map[string]int64)
	instance.idTimes = list.New()

	if expirationSeconds > 0 {
		expiration := time.Duration(expirationSeconds) * time.Second
		go func(interval time.Duration) {
			instance.checkExpiration(interval)
		}(expiration)
	}

	return instance
}

type idTime struct {
	id  string
	exp time.Time
}

type innerJwt struct {
	types.Base
	sync.RWMutex

	// [ID(jti)]ExpirationTime(exp)
	ids     map[string]int64
	idTimes *list.List
}

func (s *innerJwt) Add(jti string, exp int64) error {
	s.Lock()
	defer s.Unlock()

	_, ok := s.ids[jti]
	if ok {
		return fmt.Errorf("jti(%s)重复使用", jti)
	}
	s.ids[jti] = exp

	s.idTimes.PushBack(idTime{
		id:  jti,
		exp: time.Unix(exp, 0),
	})

	return nil
}

func (s *innerJwt) checkExpiration(interval time.Duration) {
	for {
		time.Sleep(interval)
		s.LogDebug("begin checking jwt expiration...")
		now := time.Now()
		s.deleteExpiration()
		s.LogDebug("end checking jwt expiration, time elapse: ", time.Now().Sub(now))
	}
}

func (s *innerJwt) deleteExpiration() {
	now := time.Now()
	s.Lock()
	defer s.Unlock()

	for e := s.idTimes.Front(); e != nil; {
		ev, ok := e.Value.(idTime)
		if !ok {
			return
		}
		if ev.exp.Before(now) {
			delete(s.ids, ev.id)
			next := e.Next()
			s.idTimes.Remove(e)
			e = next
		} else {
			return
		}
	}
}
