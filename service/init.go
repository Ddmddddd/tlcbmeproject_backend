package main

import (
	"tlcbme_project/server/config"
	"fmt"
	"github.com/kardianos/service"
	"github.com/ktpswjz/httpserver/logger"
	"github.com/ktpswjz/httpserver/types"
	"os"
	"path/filepath"
	"time"
)

const (
	moduleType    = "server"
	moduleName    = "tlcbme_project"
	moduleRemark  = "慢病管理平台"
	moduleVersion = "1.1.7.3"
)

var (
	cfg                 = config.NewConfig()
	log                 = &logger.Writer{Level: logger.LevelAll}
	pro                 = &Program{}
	svc service.Service = nil
)

func init() {
	moduleArgs := &types.Args{}
	serverArgs := &Args{}
	moduleArgs.Parse(os.Args, moduleType, moduleName, moduleVersion, moduleRemark, serverArgs)

	// service
	svcCfg := &service.Config{
		Name:        moduleName,
		DisplayName: moduleName,
		Description: moduleRemark,
	}
	configPath := serverArgs.config
	if configPath != "" {
		svcCfg.Arguments = []string{fmt.Sprintf("-config=%s", configPath)}
	}

	rootFolder := filepath.Dir(moduleArgs.ModuleFolder())
	// config
	if configPath == "" {
		configPath = filepath.Join(rootFolder, "cfg", "tlcbme_project.json")
	}
	_, err := os.Stat(configPath)
	if os.IsNotExist(err) {
		if serverArgs.name != "" {
			cfg.Service = serverArgs.name
		}
		err = cfg.SaveToFile(configPath)
		if err != nil {
			fmt.Println("generate configure file fail: ", err)
		}
	} else {
		err = cfg.LoadFromFile(configPath)
		if err != nil {
			fmt.Println("load configure file fail: ", err)
		}
	}
	cfg.SetPath(configPath)
	cfg.SetArgs(moduleArgs)
	if cfg.Service != "" {
		svcCfg.Name = cfg.Service
	}

	svcVal, err := service.New(pro, svcCfg)
	if err != nil {
		fmt.Print("init service fail: ", err)
		os.Exit(4)
	}
	svc = svcVal
	if serverArgs.help {
		serverArgs.ShowHelp()
		os.Exit(0)
	} else if serverArgs.isInstall {
		err = svc.Install()
		if err != nil {
			fmt.Println("install service ", svcCfg.Name, " fail: ", err)
		} else {
			fmt.Println("install service ", svcCfg.Name, " success")
		}
		os.Exit(0)
	} else if serverArgs.isUninstall {
		err = svc.Uninstall()
		if err != nil {
			fmt.Println("uninstall service ", svcCfg.Name, " fail: ", err)
		} else {
			fmt.Println("uninstall service ", svcCfg.Name, " success")
		}
		os.Exit(0)
	} else if serverArgs.isStatus {
		status, err := svc.Status()
		if err != nil {
			fmt.Println("show status of service ", svcCfg.Name, " fail: ", err)
		} else {
			if status == service.StatusRunning {
				fmt.Print(svcCfg.Name, ": ")
				fmt.Println("running")
			} else if status == service.StatusStopped {
				fmt.Print(svcCfg.Name, ": ")
				fmt.Println("stopped")
			} else {
				fmt.Print(svcCfg.Name, ": ")
				fmt.Println("not installed")
			}
		}
		os.Exit(0)
	} else if serverArgs.isStart {
		err = svc.Start()
		if err != nil {
			fmt.Println("start service ", svcCfg.Name, " fail: ", err)
		} else {
			fmt.Println("start service ", svcCfg.Name, " success")
		}
		os.Exit(0)
	} else if serverArgs.isStop {
		err = svc.Stop()
		if err != nil {
			fmt.Println("stop service ", svcCfg.Name, " fail: ", err)
		} else {
			fmt.Println("stop service ", svcCfg.Name, " success")
		}
		os.Exit(0)
	} else if serverArgs.isRestart {
		err = svc.Restart()
		if err != nil {
			fmt.Println("restart service ", svcCfg.Name, " fail: ", err)
		} else {
			fmt.Println("restart service ", svcCfg.Name, " success")
		}
		os.Exit(0)
	}

	if cfg.Site.Root == "" {
		cfg.Site.Root = filepath.Join(rootFolder, "site", "root")
	}
	if cfg.Site.Doctor.Root == "" {
		cfg.Site.Doctor.Root = filepath.Join(rootFolder, "site", "doctor")
	}
	if cfg.Site.Admin.Root == "" {
		cfg.Site.Admin.Root = filepath.Join(rootFolder, "site", "admin")
	}
	if cfg.Site.Doc.Root == "" {
		cfg.Site.Doc.Root = filepath.Join(rootFolder, "site", "doc")
	}
	if cfg.Site.Patient.Root == "" {
		cfg.Site.Patient.Root = filepath.Join(rootFolder, "site", "patient")
	}
	if cfg.Site.Callback.Root == "" {
		cfg.Site.Callback.Root = filepath.Join(rootFolder, "site", "callback")
	}
	if cfg.Site.App.Root == "" {
		cfg.Site.App.Root = filepath.Join(rootFolder, "site", "apps")
	}

	if cfg.Install.Root == "" {
		cfg.Install.Root = filepath.Join(rootFolder, "download")
	}

	if cfg.Server.Https.Enabled {
		certFilePath := cfg.Server.Https.Cert.File
		if certFilePath == "" {
			certFilePath = filepath.Join(rootFolder, "crt", "server.pfx")
			cfg.Server.Https.Cert.File = certFilePath
		}
	}

	log.Init(cfg.Log.Level, moduleName, cfg.Log.Folder)
	log.Std = true

	now := time.Now()
	zoneName, zoneOffset := now.Zone()
	LogInfo("start at: ", moduleArgs.ModulePath())
	LogInfo("version: ", moduleVersion)
	LogInfo("zone: ", zoneName, "-", zoneOffset/int(time.Hour.Seconds()))
	LogInfo("log path: ", cfg.Log.Folder)
	LogInfo("configure path: ", configPath)
	LogInfo("configure info: ", cfg)
}
