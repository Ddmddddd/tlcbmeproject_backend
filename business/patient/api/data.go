package api

import (
	"github.com/ktpswjz/httpserver/types"
	"time"
	"tlcbme_project/business"
	"tlcbme_project/data/model/doctor"
	"tlcbme_project/data/model/nutrient"
	"tlcbme_project/database"
)

type Data interface {
	CreateBloodPressureRecord(argument *doctor.BloodPressureRecord) (uint64, business.Error)
	StatBloodPressureRecord(filter *doctor.BloodPressureRecordDataFilterEx) (*doctor.BloodPressureRecordDataStat, business.Error)
	RegisterPatientAndDisease(patientID uint64) error
	RegisterDisease(patientID uint64) error
	SearchPatientBodyMassIndex(filter *doctor.PatientInfoBase) (*float64, business.Error)

	CommitBloodPressureRecord(argument *doctor.BloodPressureRecordForAppEx, isDevice bool) (uint64, business.Error)
	CommitBloodGlucoseRecord(argument *doctor.BloodGlucoseRecordForAppEx) (uint64, business.Error)
	CommitWeightRecord(argument *doctor.WeightRecordForAppEx) (uint64, business.Error)
	CommitDiscomfortRecord(argument *doctor.DiscomfortRecordForAppEx) (uint64, business.Error)
	CommitSportRecord(argument *doctor.SportRecordForAppEx) (uint64, business.Error)
	CommitDietRecord(argument *doctor.DietRecordForAppEx) (string, business.Error)
	CommitDrugRecord(argument *doctor.DrugRecordForAppEx) (string, business.Error)

	DeleteBloodPressureRecord(argument *doctor.BloodPressureRecordForAppEx) business.Error
	DeleteBloodGlucoseRecord(argument *doctor.BloodGlucoseRecordForAppEx) business.Error
	DeleteWeightRecord(argument *doctor.WeightRecordForAppEx) business.Error
	DeleteDiscomfortRecord(argument *doctor.DiscomfortRecordForAppEx) business.Error
	DeleteSportRecord(argument *doctor.SportRecordForAppEx) business.Error
	DeleteDietRecord(argument *doctor.DietRecordForAppEx) business.Error
	DeleteDrugRecord(argument *doctor.DrugRecordForAppEx) business.Error

	ListBloodPressureRecord(filter *doctor.BloodPressureRecordDataFilterEx) ([]*doctor.BloodPressureRecordForApp, business.Error)
	ListBloodGlucoseRecord(filter *doctor.BloodGlucoseRecordDataFilterEx) ([]*doctor.BloodGlucoseRecordForApp, business.Error)
	ListWeightRecord(filter *doctor.WeightRecordDataFilterEx) ([]*doctor.WeightRecordForApp, business.Error)
	ListWeightBloodRecord(filter *doctor.ViewWeightPageDataFilterEx) ([]*doctor.ViewWeightPage, business.Error)
	ListDrugRecord(filter *doctor.DrugRecordDataFilterEx) ([]*doctor.DrugRecordForApp, business.Error)
	ListDietRecord(filter *doctor.SportDietRecordDataFilterEx) ([]*doctor.DietRecordForApp, business.Error)
	GetDietRecord(filter *doctor.SportDietRecordFilter) (*doctor.DietRecordForApp, business.Error)
	ListSportRecord(filter *doctor.SportDietRecordDataFilterEx) ([]*doctor.SportRecordForApp, business.Error)
	ListDiscomfortRecord(filter *doctor.DiscomfortRecordDataFilterEx) ([]*doctor.DiscomfortRecordForApp, business.Error)
	ListStepRecord(filter *doctor.SportDietRecordDataFilterEx) ([]*doctor.StepRecordData, business.Error)
	OneDayStepRecord(filter *doctor.SportDietRecordDataFilterEx) (*doctor.SportRecordForApp, business.Error)
	TodayRecord(argument *doctor.PatientInfoBase) (*doctor.UserRecordCollection, business.Error)
	MonthlyReport(filter *doctor.ViewMonthlyReportFilterEx) (*doctor.MonthlyReportForApp, business.Error)

	PatientLogined(argument *doctor.PatientInfoBase) (*doctor.LoginedDataForApp, business.Error)
	CheckAppUpdate(argument *doctor.AppVersionRecordFromUser) (*doctor.AppVersionRecord, business.Error)

	ListDivision(argument *doctor.DivisionBaseInfo) ([]*doctor.DivisionBaseInfo, business.Error)
	ListOrgAndDoctor(argument *doctor.OrgAndDoctorFilter) ([]*doctor.OrgAndDoctorInfo, business.Error)

	CreateBloodGlucoseRecord(argument *doctor.BloodGlucoseRecordCreateEx) (uint64, business.Error)
	UploadBloodGlucoseData(serialNo uint64) (int, error)

	CreateMonthlyReport(argument *doctor.MonthlyReportYearMonth) business.Error

	CommitUsabilityStayTimeInfo(argument *doctor.UsabilityStayTime) (uint64, business.Error)
	CommitUsabilityEventInfo(argument *doctor.UsabilityEvent) (uint64, business.Error)
	CommitUsabilityDeviceInfo(argument *doctor.UsabilityDevice) (uint64, business.Error)
	CommitUsabilityFeedbackInfo(argument *doctor.UsabilityFeedBack) (uint64, business.Error)

	SendChatMessage(argument *doctor.DoctorPatientChatMsg) (uint64, business.Error)
	SendChatMessageRead(argument *doctor.DoctorPatientChatMsgRead) business.Error
	GetChatMessageList(argument *doctor.ChatMsgUserInfo) ([]*doctor.DoctorPatientChatMsg, business.Error)
	GetChatDoctorList(argument *doctor.PatientInfoBase) ([]*doctor.ChatDoctorWithMsgInfoForApp, business.Error)

	PatientCheckWeChatStatus(argument *doctor.PatientUserAuthsEx) (*doctor.PatientWeChatStatus, business.Error)

	//exp相关
	GetExpPatientBaseInfo(argument *doctor.ExpPatientInfoGet) (*doctor.ExpPatientBaseInfo, business.Error)
	CommitDefaultScaleRecord(argument *doctor.ExpDefaultScaleRecordCreate) business.Error
	CreateDefaultTask(patientID uint64) business.Error
	//GetUndoneScales(patientID uint64) ([]uint64, error)
	GetScaleBySnoList(argument *doctor.ExpCommonScaleSerialNoListFilter) ([]*doctor.ExpCommonScale, business.Error)
	GetPatientExpActionPlanList(argument *doctor.ExpPatientActionPlan, record bool) ([]*doctor.ExpActionPlan, business.Error)
	CommitExpActionPlanRecord(argument *doctor.ExpActionPlanRecordForApp) (uint64, business.Error)
	EditExpActionPlanRecord(argument *doctor.ExpActionPlanRecordEditForApp) business.Error
	UpdateActionPlanRecordTimes(sqlAccess database.SqlAccess, sno uint64) error
	GetExpActionPlanRecord(argument *doctor.ExpActionPlanRecordFilter, record bool) ([]*doctor.ExpActionPlanRecord, business.Error)
	GetOneGame() ([]*doctor.ExpFoodGI, business.Error)
	CommitGameResultRecord(argument *doctor.ExpGameResultRecord) (uint64, business.Error)
	ListGameResultRecord(filter *doctor.ExpGameResultRecordFilter) ([]*doctor.ExpGameResultRecord, business.Error)
	ListGameResultRecordOrder(filter *doctor.ExpGameResultRecordOrderFilter) ([]*doctor.ExpGameResultRecord, business.Error)
	GetPatientTodayTask(patientID uint64) (*doctor.ExpTaskRepositoryOutput, business.Error)
	GetPatientTodayKnowledge(patientID uint64, filter *doctor.ExpKnowledgeTypeInput) (*doctor.ExpKnowledge, business.Error)
	CommitKnowledge(patientID uint64, argument *doctor.ExpKnowledgeToPatientStatusEdit) business.Error
	GetHistoryKnowledge(patientID uint64) ([]*doctor.ExpKnowledge, business.Error)
	GetKnowledgeByID(patientID uint64, filter *doctor.ExpKnowledgeSerialNoInput) (*doctor.ExpKnowledge, business.Error)
	CreateRandKnowledgeFromPatient(patientID uint64, filter *doctor.ExpKnowledgeTypeInput) (*doctor.ExpKnowledge, business.Error)
	GetQuestionByDayIndex(argument *doctor.ExpKnowledgeToPatientDayIndex) ([]*doctor.ExpQuestionToKnowledge, business.Error)
	CommitTaskRecord(argument *doctor.ExpPatientTaskRecordInput) (uint64, business.Error)
	GetUndoneSmallScale(patientID uint64) ([]*doctor.ExpSmallScale, business.Error)
	GetSmallScale(argument *doctor.ExpSmallScaleTaskIDInput) (*doctor.ExpSmallScale, business.Error)
	EditSmallScale(argument *doctor.ExpSmallScaleContentEdit) business.Error
	CommitSmallScaleRecord(argument *doctor.ExpSmallScaleRecordInput) business.Error
	GetTaskRecord(argument *doctor.ExpPatientTaskRecordGetByTaskIDsAndTime) ([]*doctor.ExpPatientTaskRecord, business.Error)
	GetTaskCreditHistory(patientID uint64) ([]*doctor.ExpPatientTaskCreditHistory, business.Error)
	GetUnreadDoctorCommentList(argument *doctor.ExpDoctorCommentToPatientUnreadGet) ([]*doctor.ExpDoctorCommentToPatient, business.Error)
	ReadComment(argument *doctor.ExpDoctorCommentToPatientRead) business.Error
	GetCommentListBySnos(argument *doctor.ExpDoctorCommentToPatientSportDietSnosFilter) ([]*doctor.ExpDoctorCommentToPatient, business.Error)
	GetDietAndCommentInfo(argument *doctor.ExpDoctorCommentToPatientFilter) (*doctor.ExpDoctorCommentToPatientWithDietInfo, business.Error)
	GetTodayUnreadCommentCount(argument *doctor.ExpDoctorCommentToPatientTodayUnreadGet) (uint64, business.Error)
	UploadPatientBodyExamImage(argument *doctor.ExpPatientBodyExamImageCreate) business.Error

	GetJinScale(argument *doctor.ExpJinScaleCampFilter) ([]*doctor.ExpJinScale, business.Error)
	GetFinishedJinScale(patientID uint64) ([]string, business.Error)
	GetExercisePlan(patientID uint64) ([]*doctor.ExpExerciseBase, business.Error)
	CreateExercisePlan(argument *doctor.ExpExercisePlanCreate) business.Error
	GetPatientIntegralHistory(patientID uint64) ([]*doctor.ExpIntegral, business.Error)
	AddIntegralForPatient(patientID uint64, integral uint64, reason string) error
	GetPatientUnreadIntegral(argument *doctor.ExpIntegralUnreadGet) ([]*doctor.ExpIntegral, business.Error)
	ReadPatientIntegral(argument *doctor.ExpIntegralRead) business.Error

	GetSettingDietitian(patientID uint64) (*doctor.ManagedPatientIndex, business.Error)
	GetPatientIntegralAll() ([]*doctor.ViewTotalPoint, business.Error)

	GetDietPlan(argument *doctor.ExpPatientDietPlanGet) ([]*doctor.ExpPatientDietPlan, business.Error)
	GetDietPlanByLevel(argument *doctor.ExpPatientDietPlanGetByLevel) ([]*doctor.ExpPatientDietPlan, business.Error)
	ReadDietPlan(argument *doctor.ExpPatientDietPlanRead) business.Error

	CommitPatientReply(argument *doctor.ExpDoctorCommentToPatientReply) business.Error
	GetPatientClock(argument *doctor.ExpPatientTaskRecordGetListInput) ([][]time.Time, business.Error)

	CommitJudge(argument *doctor.PatientJudgeDoctorApp) business.Error
	CommitBug(argument *doctor.UserBugFeedbackApp) business.Error

	GetPatientInfo(argument *doctor.PatientInfoBase) (*doctor.PatientUserBaseInfo, business.Error)
	UpdatePatientInfo(argument *doctor.PatientUserInfoUpdate) business.Error

	CommitTotalFood(argument *doctor.DietRecordTotalForApp) (uint64, business.Error)
	DeleteTotalFood(argument *doctor.DietRecordTotalDeleteForApp) (uint64, business.Error)
	//提醒业务
	WxRemindCreate(argument *doctor.WeChatRemindTaskCreate) (uint64, business.Error)
	SmsRemindCreate(argument *doctor.TimingTaskCreate) (uint64, business.Error)
	PhotoFeedbackRemindCreate(argument *doctor.WeChatRemindTaskCreate) (uint64, business.Error)
	SendPhotoFeedbackRemind(argument *doctor.WeChatRemindTaskSend, sno uint64) business.Error
	GetWeChatAccessToken() (string, error)
	SubscribeMessageSend(token string, argument *doctor.WeChatRemindSend) business.Error
	SubscribeMessageSendJudge() business.Error

	UploadDataToManage(sno uint64) error
	DecryptWeRunData(argument *doctor.WeRunCryptDataInput) (*doctor.WeRunData, business.Error)

	//红包
	Transfer(argument *doctor.ExpPayToPatient) (*doctor.WithdrawResult, business.Error)

	CommitThreeMeals(ex *doctor.DietRecordEx) (uint64, business.Error)
	NutrientAnalysis(filter *doctor.NutrientAnalysisFilter) (*doctor.NutrientAnalysisResult, business.Error)

	// 生成每日营养报告
	CreateNutrientReport(sTime *types.Time)
	CreateNutrientReportRecord(*doctor.NutrientAnalysisResult, *doctor.NutrientAnalysisFilter)
	GetMealContent(argument *doctor.DietContentFilter) ([]*doctor.DietContent, business.Error)
	DeleteMealContent(argument *doctor.DietRecordFilter) (uint64, business.Error)
	GetHistoryGrade(argument *doctor.NutrientAnalysisFilterEx) ([]*doctor.Grade, business.Error)
	GetHistoryEnergy(argument *doctor.NutrientAnalysisFilterEx) ([]*doctor.Grade, business.Error)
	UpdateUseTime(argument *doctor.PatientUserAuthsUpdateTime) business.Error

	// 食物搜索添加相关
	FoodSearch(filter *doctor.FoodSearchFilter) ([]*nutrient.FoodSearchResult, business.Error)
	GetFoodMeasureUnit(filter *doctor.FoodMeasureUnitFilter) ([]*nutrient.Food_measure_unit, business.Error)
	FoodMultiSearch(filter *doctor.FoodMultiSearchFilter) ([]*nutrient.FoodSearchResult, business.Error)
	FoodBasicSearch(filter *doctor.FoodSearchFilter) ([]*nutrient.FoodSearchResult, business.Error)
	AddPatientRecipe(argument *nutrient.NewRecipeFromApp) (*nutrient.FoodSearchResult, business.Error)

	// 用户积分
	GetIntegral(filter *doctor.ChatMsgPatientID) (*doctor.Integral, business.Error)
	GetGradeRule(orgCode string) (*doctor.ExpIntegralRule, business.Error)
	UpdateGradeRule(rule *doctor.ExpIntegralRule) (uint64, business.Error)

	//营养干预
	GetNutritionIntervention(patientID uint64) (*doctor.ExpPatientNutritionInterventionTotal, business.Error)
	UpdateNutritionIntervention(argument *doctor.ExpPatientNutritionalIndicator) business.Error
	GetNutritionInterventionQuestion() ([]*doctor.ExpNutritionQuestionRepository, business.Error)
	ResultNutritionInterventionQuestion(argument *doctor.ExpPatientNutritionalQuestionInput) business.Error
	IfNutritionInterventionQuestion(argument *doctor.ExpPatientNutritionalQuestionFilter) (bool, business.Error)
	CreateNutritionInterventionReport(sTime *time.Time)
	GetWeeklyReport(patientID uint64) (*doctor.ExpNutritionInterventionRecord, business.Error)
	UpdateWeeklyHabitReport(argument *doctor.ExpNutritionInterventionHabitReportHabitUpdate) business.Error
	UpdateDietProgram(argument *doctor.ExpPatientDietProgram) business.Error
	UpdateWholeDietProgram(argument *doctor.ExpPatientDietProgramWholeUpdate) business.Error
	GetDietPlanTemplate() ([]*doctor.ExpDietPlanRepository, business.Error)
	DeletePatientDietPlan(argument *doctor.ExpPatientDietPlanDelete) business.Error

	//健康教育相关
	GetTagInfo() ([]*doctor.Tag, business.Error)
	GetPatientTag(argument *doctor.PatientTagInfo) ([]*doctor.PatientTag, business.Error)
	DeletePatientTag(argument *doctor.PatientToTagDelete) (uint64, business.Error)
	AddPatientTag(argument *doctor.PatientToTagAdd) (uint64, business.Error)
	GetKnowledgeList(argument *doctor.SelectedTags) ([]*doctor.KnowledgeInfo, business.Error)
	AddKnowledgeLoveNum(argument *doctor.KnowledgeInfoId) (uint64, business.Error)
	GetKnowledgeIdList(argument *doctor.PatientCollectedKnowledgePatientId) ([]*doctor.PatientCollectedKnowledgeKnowledgeId, business.Error)
	GetTlcKnowledgeById(argument *doctor.KnowledgeInfoId) (*doctor.KnowledgeInfo, business.Error)
	AddCollectedKnowledge(argument *doctor.PatientCollectedKnowledgeAdd) (uint64, business.Error)
	DeleteCollectedKnowledge(argument *doctor.PatientCollectedKnowledgeDelete) (uint64, business.Error)
	GetCollectedKnowledgeList(argument *doctor.PatientCollectedKnowledgeIdList) ([]*doctor.KnowledgeInfo, business.Error)
	GiveDateToGetRcKnw(argument *doctor.KnowledgeInfoDateString) (uint64, business.Error)
	TlcGetTodayKnowledge(argument *doctor.KnowledgeInfoDateString) ([]*doctor.KnowledgeInfoIncludingComments, business.Error)
	TlcGetKnowledgeStar(argument *doctor.TlcPatientToRecKnowledgeGetStar) (uint64, business.Error)
	TlcUpdateKnowledgeStar(argument *doctor.TlcPatientToRecKnowledgeUpdateStar) (uint64, business.Error)
	TlcChangeKnowledgeStatus(argument *doctor.TlcPatientToRecKnowledgeChangeStatus) (uint64, business.Error)
	TlcGetHistoryKnowledge(argument *doctor.TlcPatientToRecKnowledgePatientID) ([]*doctor.KnowledgeInfoIncludingComments, business.Error)
	//评论功能
	TlcSendKnwComment(argument *doctor.TlcPatientSendKnwComment) (uint64, business.Error)
	TlcGetKnwCommentsInfo(argument *doctor.TlcGetKnwComments) ([]*doctor.TlcGetKnwCommentsInfo, business.Error)
	//健康教育整合积分
	TlcHasKnwCommitedAsTask(argument *doctor.ExpPatientTaskRecordCheckKnwRecord) (bool, business.Error)
	//用户问题卡片
	TlcPreRequestOfQuestion(argument *doctor.TlcHeEduPatientQuestionGetBindWithKnw) (bool, business.Error)
	TlcGetIntegratedQuestionInfo(argument *doctor.TlcHeEduPatientQuestionGetBindWithKnw) ([]*doctor.TlcHeEduPatientIntegratedQuestionInfo, business.Error)
	TlcRecordPatientAnswerQuestion(argument *doctor.TlcHeEduPatientAnswerQuestionRecord) (uint64, business.Error)
	//获得用户的基本信息
	TlcGetPatientName(argument *doctor.TlcPatientNameQuery) (*doctor.TlcPatientNameQueryResult, business.Error)

	//社群群组
	CreateTeam(argument *doctor.TlcTeamCreateInfo) (bool, business.Error)
	CreateTeamPre(patientID uint64) (*doctor.TlcTeamCreatePreInfoToApp, business.Error)
	TeamQueryByGroup(argument *doctor.TlcTeamQueryByGroup) ([]*doctor.TlcTeamInfoIncMemNum, business.Error)
	TeamQuerySearch(argument *doctor.TlcTeamQueryBySearch) ([]*doctor.TlcTeamInfoIncMemNum, business.Error)
	JoinTeam(argument *doctor.TlcUserJoinTeamInfo) (bool, business.Error)
	IsJoinedTeam(patientID uint64) (uint64, business.Error)
	GetTeamMemInfo(argument *doctor.TlcTeamUserFuncFromApp) (*doctor.TlcTeamUserFuncMemsInfo, business.Error)
	DeleteTeamMem(argument *doctor.TlcTeamUserFuncFromApp) (bool, business.Error)
	ListGroupDict() ([]*doctor.TlcTeamInfoGroupDict, business.Error)
	ListTeamDict() ([]*doctor.TlcTeamInfoTeamDict, business.Error)
	//社群功能
	GetWholeTeamDietsData(argument *doctor.TlcTeamUserFuncFromAppDiets) (*doctor.TlcTeamUserFuncDietsData, business.Error)
	GetTeamWeightsData(argument *doctor.TlcTeamUserFuncFromApp) (*doctor.TlcTeamUserFuncWeightsData, business.Error)
	GetTeamSportStepsData(argument *doctor.TlcTeamUserFuncFromApp) (*doctor.TlcTeamUserFuncSportStepsData, business.Error)
	GetWholeTeamTaskInfo(argument *doctor.TlcTeamUserFuncFromApp) (*doctor.TlcTeamUserFuncTaskInfo, business.Error)
	GetTeamInfo(argument *doctor.TlcTeamInfoFromAppFuncArea) (*doctor.TlcTeamInfoToAppFuncArea, business.Error)
	GetTeamRankWholeInfo(argument *doctor.TlcTeamInfoFromAppFuncArea) (*doctor.TlcTeamRankWholeInfo, business.Error)
	InsertTeamIntegral(argument *doctor.TlcTeamInfoFromAppFuncArea) (bool, business.Error)
	GetAllGroupTasks(argument *doctor.TlcTeamGroupTaskGroupKindsInput) ([]*doctor.TlcTeamGroupTaskIntegratedInfo, business.Error)
	CreateGroupTask(argument *doctor.TlcTeamGroupTaskCreateInfo) (bool, business.Error)
	StopGroupTask(argument *doctor.TlcTeamGroupTaskStopInfo) (bool, business.Error)

	//运动打卡
	GetSportItem(argument *doctor.SportItemType) ([]*doctor.SportItem, business.Error)
}
