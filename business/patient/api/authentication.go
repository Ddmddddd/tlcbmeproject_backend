package api

import (
	"tlcbme_project/business"
	"tlcbme_project/data/entity/sqldb"
	"tlcbme_project/data/model"
	"tlcbme_project/data/model/doctor"
)

type Authentication interface {
	Authenticate() func(account, password string) (uint64, error)
	Logined() func(userID uint64, token *model.Token)
	ValidateUser() func(userName string) (uint64, error)
	CreateUser() func(userInfoCreate *doctor.PatientUserInfoCreateForApp) (uint64, error)
	RegisterPatient() func(patientID uint64) error
	ChangePassword(userID uint64, oldPassword, newPassword string) business.Error
	ChangePhoneNumber(userID uint64, phoneNumber string) business.Error
	PatientBindWeChat(argument *doctor.PatientUserAuthsEx) (uint64, business.Error)
	PatientReleaseWeChat(userID uint64, IdentityType string, Identitier string) business.Error
	GetWeChatOpenID() func(filter *model.WeChatCodeFilter) (*doctor.WeChatCode2SessionResult, error)
	GetPatientByOpenID() func(filter *model.WeChatLoginFilter) (*sqldb.PatientUserAuths, error)
	ChangeNickname(userID uint64, nickname string) business.Error
	AuditPatient() func(argument *doctor.PatientUserAuditEx) business.Error
	CreateDietPlan() func(patientID uint64) business.Error
	SendCodeWithPhone() func(phone *model.SendCodePhone) (string, error)
	NewChangePasswordWithPhone() func(argument *model.NewChangePasswordWithPhone) (string, error)
}
