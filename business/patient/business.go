package patient

import (
	"github.com/ktpswjz/httpserver/types"
	"time"
	"tlcbme_project/business"
	"tlcbme_project/business/patient/api"
	"tlcbme_project/business/patient/impl"
	"tlcbme_project/database"
	"tlcbme_project/database/memory"
	"tlcbme_project/server/config"
	"tlcbme_project/server/notify"
)

type Business interface {
	Authentication() api.Authentication
	Data() api.Data
}

func NewBusiness(cfg *config.Config,
	log types.Log,
	sqlDatabase database.SqlDatabase,
	nSqlDatabase database.SqlDatabase,
	memoryToken memory.Token,
	client business.Client,
	notifyChannels notify.ChannelCollection) Business {

	instance := &innerBusiness{cfg: cfg, sqlDatabase: sqlDatabase, nSqlDatabase: nSqlDatabase, memoryToken: memoryToken, client: client, notifyChannels: notifyChannels}
	instance.SetLog(log)
	instance.init()

	//f, err := excelize.OpenFile("D:/2.xlsx")
	//if err != nil {
	//	fmt.Println(err)
	//}
	//// 获取 Sheet1 上所有单元格
	//rows, err := f.GetRows("Sheet")
	//if err != nil {
	//	fmt.Println(err)
	//}
	//for index, row := range rows {
	//	if index > 0{
	//		dbEntity := &nutrientdb.FoodAlias{}
	//		dbEntity.Frequency_Flag = 100
	//		for colIndex, colCell := range row {
	//			switch colIndex {
	//			case 5:
	//				number, _ := strconv.ParseUint(string(colCell), 10, 64)
	//				dbEntity.Serial_No = number
	//			case 2:
	//				dbEntity.Alias = colCell
	//			}
	//
	//		}
	//		_,err := instance.nSqlDatabase.UpdateSelectiveByPrimaryKey(dbEntity)
	//		if err != nil{
	//			fmt.Println(err)
	//		}
	//	}
	//}

	//f, err := excelize.OpenFile("D:/2.xlsx")
	//if err != nil {
	//	fmt.Println(err)
	//}
	////获取 Sheet1 上所有单元格
	//rows, err := f.GetRows("Sheet")
	//if err != nil {
	//	fmt.Println(err)
	//}
	//for index, row := range rows {
	//	if index > 0{
	//		dbEntity := &nutrientdb.Food_measure_unit{}
	//		dbEntity.Number_Part = float64(1)
	//		dbEntity.Flag = 0
	//		for colIndex, colCell := range row {
	//			switch colIndex {
	//			case 3:
	//				dbEntity.Unit_Part = colCell
	//				dbEntity.Unit_Name_Part = colCell
	//			case 4:
	//				number, _ := strconv.ParseFloat(colCell, 64)
	//				dbEntity.Edible_Part = number
	//			case 5:
	//				number, _ := strconv.ParseUint(colCell, 10, 64)
	//				dbEntity.Food_Serial_No = number
	//			case 7:
	//				if colCell != ""{
	//					dbEntity.Level_Part = &colCell
	//				}
	//			}
	//		}
	//
	//		dbEntity2 := &nutrientdb.Food{}
	//		dbFilter2 := &nutrientdb.FoodFilter{
	//			Serial_No:dbEntity.Food_Serial_No,
	//		}
	//		sqlFilte2 := instance.nSqlDatabase.NewFilter(dbFilter2,false,false)
	//		err := instance.nSqlDatabase.SelectOne(dbEntity2,sqlFilte2)
	//		if err != nil{
	//			instance.LogInfo(err)
	//		}
	//
	//
	//		dbEntity3 := &nutrientdb.Food_measure_unit{}
	//		dbEntity3.Flag = 1
	//		dbEntity3.Food_Serial_No = dbEntity2.Serial_No
	//		dbEntity3.Number_Part = 100
	//		dbEntity3.Unit_Part = "g"
	//		dbEntity3.Unit_Name_Part = "克"
	//		if dbEntity2.FOOD_DEP != nil{
	//			number, _ := strconv.ParseFloat(*dbEntity2.FOOD_DEP, 64)
	//			dbEntity3.Edible_Part = number
	//		}
	//
	//		dbFilter3 := &nutrientdb.Food_measure_unit_Filter{
	//			Food_Serial_No:dbEntity3.Food_Serial_No,
	//			Flag:1,
	//		}
	//		sqlFilter3 := instance.nSqlDatabase.NewFilter(dbFilter3,false,false)
	//		dbEntity4 := &nutrientdb.Food_measure_unit{}
	//		err = instance.nSqlDatabase.SelectOne(dbEntity4,sqlFilter3)
	//		if err != nil {
	//			if instance.nSqlDatabase.IsNoRows(err) {
	//				_,err = instance.nSqlDatabase.InsertSelective(dbEntity3)
	//				if err != nil{
	//					instance.LogInfo(err)
	//				}
	//			}
	//		}
	//		_,err = instance.nSqlDatabase.InsertSelective(dbEntity)
	//		if err != nil{
	//			instance.LogInfo(err)
	//		}
	//	}
	//
	//}

	// 生成月报的定时任务
	go func() {
		timerTicker := time.NewTimer(time.Second)
		for {
			<-timerTicker.C
			timerTicker.Reset(time.Hour)

			now := time.Now()
			//if now.Day() == 1 && now.Hour() == 2 {
			//	// 每月1号凌晨2:00生成上个月的月报
			//	year := now.Year()
			//	var sMonth string
			//	month := int(now.Month())
			//	if month == 1 {
			//		year = year - 1
			//		sMonth = "12"
			//	} else if month > 10 {
			//		sMonth = fmt.Sprintf("%d", month-1)
			//	} else {
			//		sMonth = fmt.Sprintf("0%d", month-1)
			//	}
			//	yearMonth := fmt.Sprintf("%d-%s", year, sMonth)
			//	argument := &doctor.MonthlyReportYearMonth{YearMonth: yearMonth}
			//	instance.data.CreateMonthlyReport(argument)
			//}

			if now.Hour() == 2 {
				//每天凌晨2:00生成昨天的营养摄入结果
				sTime := now.AddDate(0, 0, -1)
				sTime2 := types.Time(sTime)
				instance.data.CreateNutrientReport(&sTime2)
				//log.Info("business.go")
				//instance.data.SubscribeMessageSendJudge()
			}

			if now.Weekday() == 1 && now.Hour() == 3 {
				// 每周一凌晨2:00生成上周的营养干预分析
				sTime := now.AddDate(0, 0, -1)
				instance.data.CreateNutritionInterventionReport(&sTime)
			}

			//if err = f.clo(); err != nil {
			//	fmt.Println(err)
			//}
		}
	}()

	return instance
}

type innerBusiness struct {
	types.Base

	cfg            *config.Config
	sqlDatabase    database.SqlDatabase
	nSqlDatabase   database.SqlDatabase
	memoryToken    memory.Token
	client         business.Client
	notifyChannels notify.ChannelCollection

	authentication api.Authentication
	data           api.Data
}

func (s *innerBusiness) init() {
	s.authentication = impl.NewAuthentication(s.cfg, s.GetLog(), s.sqlDatabase, s.memoryToken, s.cfg.Site.Patient.User.PasswordFormat, s.notifyChannels, s.client)
	s.data = impl.NewData(s.cfg, s.GetLog(), s.sqlDatabase, s.nSqlDatabase, s.memoryToken, s.client, s.notifyChannels)
}

func (s *innerBusiness) Authentication() api.Authentication {
	return s.authentication
}

func (s *innerBusiness) Data() api.Data {
	return s.data
}
