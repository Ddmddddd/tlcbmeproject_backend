package impl

import (
	"tlcbme_project/business"
	"tlcbme_project/data/entity/sqldb"
	"tlcbme_project/data/model/doctor"
	"tlcbme_project/errors"
	"tlcbme_project/server/notify"
	"github.com/ktpswjz/httpserver/types"
	"time"
)

func (s *Data) SendChatMessage(argument *doctor.DoctorPatientChatMsg) (uint64, business.Error) {
	dbEntity := &sqldb.DoctorPatientChatMsg{}
	dbEntity.CopyFrom(argument)
	sno, err := s.sqlDatabase.InsertSelective(dbEntity)
	if err != nil {
		return 0, business.NewError(errors.InternalError, err)
	}
	argument.SerialNo = sno

	s.WriteNotify(&notify.Message{
		BusinessID: notify.BusinessDoctor,
		NotifyID:   notify.DoctorNewChatMessage,
		Time:       types.Time(time.Now()),
		Data:       argument,
	})
	s.WriteNotify(&notify.Message{
		BusinessID: notify.BusinessDoctor,
		NotifyID:   notify.DoctorCountUnReadMessage,
		Time:       types.Time(time.Now()),
	})
	return sno, nil
}

func (s *Data) SendChatMessageRead(argument *doctor.DoctorPatientChatMsgRead) business.Error {
	sqlAccess, err := s.sqlDatabase.NewAccess(true)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}
	defer sqlAccess.Close()

	for _, serialNo := range argument.MsgSerialNos {
		dbEntity := &sqldb.DoctorPatientChatMsgRead{}
		dbEntity.SerialNo = serialNo
		dbEntity.MsgFlag = 1
		_, err := sqlAccess.UpdateSelectiveByPrimaryKey(dbEntity)
		if err != nil {
			return business.NewError(errors.InternalError, err)
		}
	}
	err = sqlAccess.Commit()
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}

	s.WriteNotify(&notify.Message{
		BusinessID: notify.BusinessDoctor,
		NotifyID:   notify.DoctorChatMessageRead,
		Time:       types.Time(time.Now()),
		Data:       argument,
	})

	return nil
}

func (s *Data) GetChatMessageList(argument *doctor.ChatMsgUserInfo) ([]*doctor.DoctorPatientChatMsg, business.Error) {
	results := make([]*doctor.DoctorPatientChatMsg, 0)

	dbEntity := &sqldb.DoctorPatientChatMsg{}
	dbFilter1 := &sqldb.DoctorPatientChatMsgFilter{
		SenderID:   argument.PatientID,
		ReceiverID: argument.DoctorID,
	}
	dbFilter2 := &sqldb.DoctorPatientChatMsgFilter{
		SenderID:   argument.DoctorID,
		ReceiverID: argument.PatientID,
	}

	sqlFilter1 := s.sqlDatabase.NewFilter(dbFilter1, false, false)
	sqlFilter2 := s.sqlDatabase.NewFilter(dbFilter2, false, true)
	dbOrder := &sqldb.DoctorPatientChatMsgDateTimeOrder{}

	err := s.sqlDatabase.SelectList(dbEntity, func() {
		result := &doctor.DoctorPatientChatMsg{}
		dbEntity.CopyTo(result)
		results = append(results, result)
	}, dbOrder, sqlFilter1, sqlFilter2)
	if err != nil {
		return results, business.NewError(errors.InternalError, err)
	}

	return results, nil
}

func (s *Data) GetChatDoctorList(argument *doctor.PatientInfoBase) ([]*doctor.ChatDoctorWithMsgInfoForApp, business.Error) {
	results := make([]*doctor.ChatDoctorWithMsgInfoForApp, 0)

	dbFilter := &sqldb.ManagedPatientIndexPatientIDFilter{
		PatientID: argument.PatientID,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	dbEntity := &sqldb.ManagedPatientIndex{}
	err := s.sqlDatabase.SelectOne(dbEntity, sqlFilter)
	if err != nil {
		if s.sqlDatabase.IsNoRows(err) {
			return results, nil
		} else {
			return results, business.NewError(errors.InternalError, err)
		}
	}

	if dbEntity.DoctorID != nil && dbEntity.DoctorName != nil {
		chatDoctor, err := s.getChatDoctorMsgInfo(argument.PatientID, uint64(*dbEntity.DoctorID), string(*dbEntity.DoctorName))
		if err != nil {
			return results, err
		}
		results = append(results, chatDoctor)
	}

	if dbEntity.HealthManagerID != nil && dbEntity.HealthManagerName != nil && (dbEntity.DoctorID != nil && uint64(*dbEntity.DoctorID) != uint64(*dbEntity.HealthManagerID)) {
		chatDoctor, err := s.getChatDoctorMsgInfo(argument.PatientID, uint64(*dbEntity.HealthManagerID), string(*dbEntity.HealthManagerName))
		if err != nil {
			return results, err
		}
		results = append(results, chatDoctor)
	}

	return results, nil
}

func (s *Data) getChatDoctorMsgInfo(patientID uint64, doctorID uint64, doctorName string) (*doctor.ChatDoctorWithMsgInfoForApp, business.Error) {
	unreadMsgLit := make([]uint64, 0)

	dbFilter := &sqldb.DoctorPatientChatMsgWithFlagFilter{
		SenderID:   doctorID,
		ReceiverID: patientID,
		MsgFlag:    0,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	dbEntity := &sqldb.DoctorPatientChatMsg{}
	err := s.sqlDatabase.SelectList(dbEntity, func() {
		msgNo := dbEntity.SerialNo
		unreadMsgLit = append(unreadMsgLit, msgNo)
	}, nil, sqlFilter)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}

	chatDoctor := &doctor.ChatDoctorWithMsgInfoForApp{
		UserID:        doctorID,
		Name:          doctorName,
		UnReadMsgList: unreadMsgLit,
	}

	dbNewestChatMsgEntity := &sqldb.DoctorPatientChatMsgBaseInfo{}
	sqlNewestChatMsgEntity := s.sqlDatabase.NewEntity()
	sqlNewestChatMsgEntity.Parse(dbNewestChatMsgEntity)
	sqlBuilder := s.sqlDatabase.NewBuilder()
	sqlBuilder.Select(sqlNewestChatMsgEntity.ScanFields(), false).From(sqlNewestChatMsgEntity.Name())
	sqlBuilder.Append("WHERE (SenderID = ? AND ReceiverID = ?) OR (ReceiverID = ? AND SenderID = ?) order by MsgDateTime desc LIMIT 1")
	query := sqlBuilder.Query()

	sqlAccess, err := s.sqlDatabase.NewAccess(false)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}
	defer sqlAccess.Close()

	row := sqlAccess.QueryRow(query, patientID, doctorID, patientID, doctorID)
	err = row.Scan(sqlNewestChatMsgEntity.ScanArgs()...)
	if s.sqlDatabase.IsNoRows(err) {
		// do nothing
	} else if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	} else {
		chatDoctor.LastMsgContent = dbNewestChatMsgEntity.MsgContent
		msgDateTime := types.Time(*dbNewestChatMsgEntity.MsgDateTime)
		chatDoctor.LastMsgDateTime = &msgDateTime
	}

	return chatDoctor, nil
}
