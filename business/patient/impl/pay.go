package impl

import (
	"bytes"
	"crypto/md5"
	"crypto/tls"
	"encoding/hex"
	"encoding/xml"
	"io/ioutil"
	"math/rand"
	"net/http"
	"net/url"
	"reflect"
	"strconv"
	"strings"
	"time"
	"tlcbme_project/business"
	"tlcbme_project/data/model/doctor"
	"tlcbme_project/errors"
	"unsafe"
)

func (s *Data) Transfer(argument *doctor.ExpPayToPatient) (*doctor.WithdrawResult, business.Error) {
	partnerTradeNo := GetRandomNumber(32)
	amount := "1"
	desc := "测试"
	result, flag, err := s.WithdrawMoney(argument.OpenId, amount, partnerTradeNo, desc)
	if !flag {
		if err != nil {
			return nil, business.NewError(errors.InternalError, err)
		} else {
			return result, nil
		}
	}

	return result, nil
}

var transfers = "https://api.mch.weixin.qq.com/mmpaymkttransfers/promotion/transfers"

//付款，成功返回自定义订单号，微信订单号，true，失败返回错误信息，false
func (s *Data) WithdrawMoney(openid, amount, partnerTradeNo, desc string) (*doctor.WithdrawResult, bool, error) {
	order := &doctor.WithdrawOrder{}
	order.MchAppid = s.cfg.PayInfo.AppId
	order.Mchid = s.cfg.PayInfo.MchId
	order.Openid = openid
	order.Amount, _ = strconv.Atoi(amount)
	order.Desc = desc
	order.PartnerTradeNo = partnerTradeNo
	order.DeviceInfo = "WEB"
	order.CheckName = "NO_CHECK" //NO_CHECK：不校验真实姓名 FORCE_CHECK：强校验真实姓名
	//order.SpbillCreateIp = clientIp
	order.NonceStr = s.RandStringRunes(32)
	order.Sign = s.md5WithdrawOrder(order)
	xmlBody, _ := xml.MarshalIndent(order, " ", " ")
	resp, err := s.SecurePost(transfers, xmlBody)
	if err != nil {
		return nil, false, err
	}
	defer resp.Body.Close()
	bodyByte, _ := ioutil.ReadAll(resp.Body)
	var res *doctor.WithdrawResult
	xmlerr := xml.Unmarshal(bodyByte, &res)
	if xmlerr != nil {
		return nil, false, xmlerr
	}
	if res.ReturnCode == "SUCCESS" && res.ResultCode == "SUCCESS" {
		return res, true, nil
	}
	return res, false, nil
}
//md5签名
func (s *Data) md5WithdrawOrder(order *doctor.WithdrawOrder) string {
	o := url.Values{}
	o.Add("mch_appid", order.MchAppid)
	o.Add("mchid", order.Mchid)
	o.Add("device_info", order.DeviceInfo)
	o.Add("partner_trade_no", order.PartnerTradeNo)
	o.Add("check_name", order.CheckName)
	o.Add("amount", strconv.Itoa(order.Amount))
	//o.Add("spbill_create_ip", order.SpbillCreateIp)
	o.Add("desc", order.Desc)
	o.Add("nonce_str", order.NonceStr)
	o.Add("openid", order.Openid)
	r, _ := url.QueryUnescape(o.Encode())
	return s.md5([]byte(r + "&key=" + s.cfg.PayInfo.ApiKey))
}

var _tlsConfig *tls.Config
//采用单例模式初始化ca
func (s *Data) getTLSConfig() (*tls.Config, error) {
	if _tlsConfig != nil {
		return _tlsConfig, nil
	}
	// load cert
	cert, err := tls.LoadX509KeyPair(s.cfg.PayInfo.Cert, s.cfg.PayInfo.Key)
	if err != nil {
		return nil, err
	}
	_tlsConfig = &tls.Config{
		Certificates: []tls.Certificate{cert},
	}
	return _tlsConfig, nil
}
//携带ca证书的安全请求
func (s *Data) SecurePost(url string, xmlContent []byte) (*http.Response, error) {
	tlsConfig, err := s.getTLSConfig()
	if err != nil {
		return nil, err
	}
	tr := &http.Transport{TLSClientConfig: tlsConfig}
	client := &http.Client{Transport: tr}
	return client.Post(
		url,
		"application/xml",
		bytes.NewBuffer(xmlContent))
}

var letterRunes = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")
func (s *Data) RandStringRunes(n int) string {
	b := make([]rune, n)
	for i := range b {
		b[i] = letterRunes[rand.Intn(len(letterRunes))]
	}
	return string(b)
}
func (s *Data) md5(str []byte) string {
	m := md5.New()
	m.Write(str)
	sign := hex.EncodeToString(m.Sum(nil))
	result := strings.ToUpper(sign)
	return result
}

//随机生成数字字符串
func GetRandomNumber(l int) string {
	str := "0123456789"
	bytes := []byte(str)
	var result []byte
	r := rand.New(rand.NewSource(time.Now().UnixNano()))
	for i := 0; i < l; i++ {
		result = append(result, bytes[r.Intn(len(bytes))])
	}
	return BytesToString(result)
}

// BytesToString 0 拷贝转换 slice byte 为 string
func BytesToString(b []byte) (s string) {
	_bptr := (*reflect.SliceHeader)(unsafe.Pointer(&b))
	_sptr := (*reflect.StringHeader)(unsafe.Pointer(&s))
	_sptr.Data = _bptr.Data
	_sptr.Len = _bptr.Len
	return s
}
