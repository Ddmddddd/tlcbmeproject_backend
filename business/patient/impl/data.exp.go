package impl

import (
	"encoding/json"
	"fmt"
	"github.com/ktpswjz/httpserver/types"
	"math"
	"sort"
	"strconv"
	"time"
	"tlcbme_project/business"
	"tlcbme_project/data/entity/sqldb"
	"tlcbme_project/data/enum"
	"tlcbme_project/data/model/doctor"
	"tlcbme_project/data/model/manage"
	"tlcbme_project/database"
	"tlcbme_project/errors"
	"tlcbme_project/server/config"
)

func (s *Data) GetExpPatientBaseInfo(argument *doctor.ExpPatientInfoGet) (*doctor.ExpPatientBaseInfo, business.Error) {
	dbEntity := &sqldb.ExpPatient{}
	dbFilter := &sqldb.ExpPatientIDFilter{
		PatientID: argument.PatientID,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)

	dataEntity := &doctor.ExpPatientBaseInfo{}
	err := s.sqlDatabase.SelectOne(dbEntity, sqlFilter)
	if err != nil {
		if s.sqlDatabase.IsNoRows(err) {
			dataEntity.Status = 0
			return dataEntity, nil
		} else {
			return nil, business.NewError(errors.InternalError, err)
		}
	}

	dbEntity.CopyToBaseInfo(dataEntity)

	// 该状态处于管理中但有未完成的问卷
	//if dataEntity.Status == enum.ExpStatuses.Scale().Key {
	//	dataEntity.UndoneScales, err = s.GetUndoneScales(dataEntity.PatientID)
	//	if err != nil {
	//		return nil, business.NewError(errors.InternalError, err)
	//	}
	//}

	// 该状态未完成行动计划选择
	if dataEntity.Status == enum.ExpStatuses.Task().Key {
		return dataEntity, nil
	}

	return dataEntity, nil
}

func (s *Data) CommitDefaultScaleRecord(argument *doctor.ExpDefaultScaleRecordCreate) business.Error {
	dbEntity := &sqldb.ExpScaleRecord{}
	dbEntity.CopyFromDefaultCreate(argument)
	_, err := s.sqlDatabase.Insert(dbEntity)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}

	taskOrders := make([]uint64, 0)

	resByre, err := json.Marshal(argument.Result)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}
	var questions []*doctor.DefaultScaleQuestion
	err = json.Unmarshal(resByre, &questions)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}
	for i := 0; i < len(questions); i++ {
		if questions[i].Checked == 0 {
			taskOrders = append(taskOrders, uint64(i+1))
		}
	}

	// 默认情况，选择了所有选项
	if len(taskOrders) == len(questions) {
		err := s.CreateDefaultTask(argument.PatientID)
		if err != nil {
			return err
		}

		return nil
	}

	// 有自己选择的情况
	err = s.CreateTaskByChoose(argument.PatientID, taskOrders)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}

	return nil
}

func (s *Data) CreateDefaultTask(patientID uint64) business.Error {
	taskList := make([][][]uint64, 0)
	dbEntity := &sqldb.ExpTaskRepository{}
	dbFilter := &sqldb.ExpTaskRepositoryStatusFilter{
		Status: 0,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	parentSnoToOrder := make(map[int]int)
	err := s.sqlDatabase.SelectList(dbEntity, func() {
		if dbEntity.ParentTaskID != 0 {
			weekIndex := parentSnoToOrder[int(dbEntity.ParentTaskID)]
			for i := len(taskList); i < weekIndex; i++ {
				weekList := make([][]uint64, 0)
				taskList = append(taskList, weekList)
			}
			for i := len(taskList[weekIndex-1]); i < int(dbEntity.TaskOrder); i++ {
				dayList := make([]uint64, 0)
				taskList[weekIndex-1] = append(taskList[weekIndex-1], dayList)
			}
			taskList[weekIndex-1][dbEntity.TaskOrder-1] = append(taskList[weekIndex-1][dbEntity.TaskOrder-1], dbEntity.SerialNo)
		} else {
			parentSnoToOrder[int(dbEntity.SerialNo)] = int(dbEntity.TaskOrder)
		}
	}, nil, sqlFilter)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}

	str, err := json.Marshal(taskList)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}
	now := time.Now()
	taskEntity := &sqldb.ExpPatientTaskList{
		PatientID:      patientID,
		TaskList:       string(str),
		CreateDateTime: &now,
	}
	_, err = s.sqlDatabase.Insert(taskEntity)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}

	return nil
}

func (s *Data) CreateTaskByChoose(patientID uint64, taskOrders []uint64) error {
	taskList := make([][][]uint64, 0)
	snos := make([]uint64, 0)
	dbEntity := &sqldb.ExpTaskRepository{}
	dbFilter := &sqldb.ExpTaskRepositoryTaskOrdersFilter{
		TaskOrders:   taskOrders,
		Status:       0,
		ParentTaskID: 0,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	err := s.sqlDatabase.SelectList(dbEntity, func() {
		weekList := make([][]uint64, 0)
		taskList = append(taskList, weekList)
		snos = append(snos, dbEntity.SerialNo)
	}, nil, sqlFilter)
	if err != nil {
		return err
	}

	for i := 0; i < len(snos); i++ {
		dbFilter := &sqldb.ExpTaskRepositoryParentTaskIDFilter{
			Status:       0,
			ParentTaskID: snos[i],
		}
		sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
		err := s.sqlDatabase.SelectList(dbEntity, func() {
			for j := len(taskList[i]); j < int(dbEntity.TaskOrder); j++ {
				dayList := make([]uint64, 0)
				taskList[i] = append(taskList[i], dayList)
			}
			taskList[i][dbEntity.TaskOrder-1] = append(taskList[i][dbEntity.TaskOrder-1], dbEntity.SerialNo)
		}, nil, sqlFilter)
		if err != nil {
			return err
		}
	}

	str, err := json.Marshal(taskList)
	if err != nil {
		return err
	}
	now := time.Now()
	taskEntity := &sqldb.ExpPatientTaskList{
		PatientID:      patientID,
		TaskList:       string(str),
		CreateDateTime: &now,
	}
	_, err = s.sqlDatabase.Insert(taskEntity)
	if err != nil {
		return err
	}

	return nil
}

// 获取患者未完成的问卷
//func (s *Data) GetUndoneScales(patientID uint64) ([]uint64, error) {
//	scaleSnos := make(map[uint64]bool)
//	res := make([]uint64, 0)
//
//	dbRecordEntity := &sqldb.ExpScaleRecord{}
//	dbRecordFilter := &sqldb.ExpScaleRecordFilterByID{
//		PatientID: patientID,
//	}
//	sqlRecordFilter := s.sqlDatabase.NewFilter(dbRecordFilter, false, false)
//	err := s.sqlDatabase.SelectList(dbRecordEntity, func() {
//		scaleSnos[dbRecordEntity.ScaleID] = true
//	}, nil, sqlRecordFilter)
//
//	dbEntity := &sqldb.ExpCommonScale{}
//	sqlFilter := s.sqlDatabase.NewFilter(nil, false, false)
//	err = s.sqlDatabase.SelectList(dbEntity, func() {
//		if _, ok := scaleSnos[dbEntity.SerialNo]; !ok {
//			res = append(res, dbEntity.SerialNo)
//		}
//	}, nil, sqlFilter)
//	if err != nil {
//		return nil, err
//	}
//
//	return res, nil
//}

func (s *Data) GetScaleBySnoList(argument *doctor.ExpCommonScaleSerialNoListFilter) ([]*doctor.ExpCommonScale, business.Error) {
	results := make([]*doctor.ExpCommonScale, 0)

	dbEntity := &sqldb.ExpCommonScale{}
	dbFilter := &sqldb.ExpCommonScaleSerialNoListFilter{
		SerialNos: argument.SerialNos,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	err := s.sqlDatabase.SelectList(dbEntity, func() {
		result := &doctor.ExpCommonScale{}
		dbEntity.CopyTo(result)
		results = append(results, result)
	}, nil, sqlFilter)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}

	return results, nil
}

func (s *Data) GetPatientExpActionPlanList(argument *doctor.ExpPatientActionPlan, record bool) ([]*doctor.ExpActionPlan, business.Error) {
	results := make([]*doctor.ExpActionPlan, 0)

	dbEntity := &sqldb.ExpActionPlan{}
	dbFilter := &sqldb.ExpActionPlanPatientIDAndStatusFilter{
		PatientID: argument.PatientID,
		Status:    argument.Status,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	dbOrder := &sqldb.ExpActionPlanOrder{}
	now := time.Now()
	snos := make([]uint64, 0)

	err := s.sqlDatabase.SelectList(dbEntity, func() {
		result := &doctor.ExpActionPlan{}
		dbEntity.CopyTo(result)
		results = append(results, result)
		if record {
			dbRecordEntity := &sqldb.ExpActionPlanRecord{}
			dbRecordEntity.CopyFromActionPlan(result)
			dbRecordEntity.Completion = 1 //无感记录Completion为1
			dbRecordEntity.CreateDateTime = &now
			sno, err := s.sqlDatabase.Insert(dbRecordEntity)
			if err != nil {
				return
			}
			if dbRecordEntity.SeqActionPlan != 0 {
				snos = append(snos, sno)
			}
		}
	}, dbOrder, sqlFilter)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}

	if record && len(snos) != 0 {
		go func() {
			s.chActionPlanRecordList <- snos
		}()
	}

	return results, nil
}

func (s *Data) CommitExpActionPlanRecord(argument *doctor.ExpActionPlanRecordForApp) (uint64, business.Error) {
	sqlAccess, err := s.sqlDatabase.NewAccess(true)
	if err != nil {
		return 0, business.NewError(errors.InternalError, err)
	}
	defer sqlAccess.Close()

	dbEntity := &sqldb.ExpActionPlanRecord{}
	dbEntity.CopyFromApp(argument)

	sno, err := sqlAccess.InsertSelective(dbEntity)
	if err != nil {
		return 0, business.NewError(errors.InternalError, err)
	}
	err = sqlAccess.Commit()
	if err != nil {
		return 0, business.NewError(errors.InternalError, err)
	}

	if dbEntity.SeqActionPlan != 0 {
		go func(patientId uint64, dataType string, time *types.Time) {
			s.uploadDataToPlatform(patientId, dataType, true, false, time)
			s.chActionPlanRecord <- sno
		}(dbEntity.PatientID, "生活计划提交", argument.CreateDateTime)
	}

	if argument.Completion == 2 {
		err = s.UpdateActionPlanRecordTimes(sqlAccess, argument.ActionPlanID)
		if err != nil {
			return 0, business.NewError(errors.InternalError, err)
		}
	}

	return sno, nil
}

func (s *Data) EditExpActionPlanRecord(argument *doctor.ExpActionPlanRecordEditForApp) business.Error {
	sqlAccess, err := s.sqlDatabase.NewAccess(true)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}
	defer sqlAccess.Close()

	now := time.Now()
	zeroHour := time.Date(now.Year(), now.Month(), now.Day(), 0, 0, 0, 0, time.UTC)
	dbFilter := &sqldb.ExpActionPlanRecordEditFilter{
		ActionPlanID:   argument.SerialNo,
		CreateDateTime: &zeroHour,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)

	dbEntity := &sqldb.ExpActionPlanRecordEditForApp{}
	dbEntity.CopyFromEdit(argument)

	_, err = sqlAccess.UpdateSelective(dbEntity, sqlFilter)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}

	if argument.Completion == 2 {
		err = s.UpdateActionPlanRecordTimes(sqlAccess, argument.ActionPlanID)
		if err != nil {
			return business.NewError(errors.InternalError, err)
		}
	}

	err = sqlAccess.Commit()
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}

	go func() {
		s.EditActionPlanRecordEng(argument.SerialNo, zeroHour)
	}()

	return nil
}

func (s *Data) UpdateActionPlanRecordTimes(sqlAccess database.SqlAccess, sno uint64) error {
	dbEntity := &sqldb.ExpActionPlan{}
	sqlEntity := s.sqlDatabase.NewEntity()
	sqlEntity.Parse(dbEntity)
	sqlBuilder := s.sqlDatabase.NewBuilder()
	sqlBuilder.Update(sqlEntity.Name())
	sqlBuilder.Append("set RecordTimes = RecordTimes + 1 where SerialNo = ?")
	query := sqlBuilder.Query()

	_, err := sqlAccess.Query(query, sno)
	if err != nil {
		return err
	}

	return nil
}

func (s *Data) GetExpActionPlanRecord(argument *doctor.ExpActionPlanRecordFilter, record bool) ([]*doctor.ExpActionPlanRecord, business.Error) {
	results := make([]*doctor.ExpActionPlanRecord, 0)
	sqlAccess, err := s.sqlDatabase.NewAccess(true)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}
	defer sqlAccess.Close()

	dbEntity := &sqldb.ExpActionPlanRecord{}
	dbFilter := &sqldb.ExpActionPlanRecordGetFilter{}
	dbFilter.CopyFromFilter(argument)
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	dbOrder := &sqldb.ExpActionPlanRecordOrder{}

	err = s.sqlDatabase.SelectList(dbEntity, func() {
		result := &doctor.ExpActionPlanRecord{}
		dbEntity.CopyTo(result)
		results = append(results, result)
	}, dbOrder, sqlFilter)

	//无感记录
	if record && len(results) == 0 {
		newArgument := &doctor.ExpPatientActionPlan{}
		newArgument.PatientID = argument.PatientID
		newArgument.Status = 0
		s.GetPatientExpActionPlanList(newArgument, true)
	}

	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}
	err = sqlAccess.Commit()
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}

	return results, nil
}

func (s *Data) GetOneGame() ([]*doctor.ExpFoodGI, business.Error) {
	results := make([]*doctor.ExpFoodGI, 0)
	sqlAccess, err := s.sqlDatabase.NewAccess(true)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}
	defer sqlAccess.Close()

	dbEntity := &sqldb.ExpFoodGI{}
	sqlEntity := s.sqlDatabase.NewEntity()
	sqlEntity.Parse(dbEntity)
	sqlBuilder := s.sqlDatabase.NewBuilder()
	sqlBuilder.Select(sqlEntity.ScanFields(), false).From(sqlEntity.Name())
	sqlBuilder.Append("as t1 where t1.SerialNo >= (Rand()*(select max(SerialNo) from ExpFoodGI)) limit 10") // 不要用order by rand()来返回，效率很低
	query := sqlBuilder.Query()

	rows, err := sqlAccess.Query(query)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	} else {
		for rows.Next() {
			err = rows.Scan(sqlEntity.ScanArgs()...)
			if err != nil {
				return nil, business.NewError(errors.InternalError, err)
			}

			result := &doctor.ExpFoodGI{}
			dbEntity.CopyTo(result)
			results = append(results, result)
		}
	}
	err = sqlAccess.Commit()
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}

	return results, nil
}

func (s *Data) CommitGameResultRecord(argument *doctor.ExpGameResultRecord) (uint64, business.Error) {
	sqlAccess, err := s.sqlDatabase.NewAccess(true)
	if err != nil {
		return 0, business.NewError(errors.InternalError, err)
	}
	defer sqlAccess.Close()

	dbEntity := &sqldb.ExpGameResultRecord{}
	dbEntity.CopyFrom(argument)

	sno, err := sqlAccess.InsertSelective(dbEntity)
	if err != nil {
		return 0, business.NewError(errors.InternalError, err)
	}
	err = sqlAccess.Commit()
	if err != nil {
		return 0, business.NewError(errors.InternalError, err)
	}

	return sno, nil
}

func (s *Data) ListGameResultRecord(filter *doctor.ExpGameResultRecordFilter) ([]*doctor.ExpGameResultRecord, business.Error) {
	results := make([]*doctor.ExpGameResultRecord, 0)

	dbEntity := &sqldb.ExpGameResultRecord{}
	dbFilter := &sqldb.ExpGameResultRecordFilter{}
	dbFilter.CopyFrom(filter)
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	dbOrder := &sqldb.ExpGameResultRecordOrder{}

	err := s.sqlDatabase.SelectList(dbEntity, func() {
		result := &doctor.ExpGameResultRecord{}
		dbEntity.CopyTo(result)
		results = append(results, result)
	}, dbOrder, sqlFilter)
	if err != nil {
		return results, business.NewError(errors.InternalError, err)
	}

	return results, nil
}

func (s *Data) ListGameResultRecordOrder(filter *doctor.ExpGameResultRecordOrderFilter) ([]*doctor.ExpGameResultRecord, business.Error) {
	sqlAccess, err := s.sqlDatabase.NewAccess(true)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}
	defer sqlAccess.Close()

	results := make([]*doctor.ExpGameResultRecord, 0)

	dbEntity := &sqldb.ExpGameResultRecord{}

	sqlString := "SELECT SerialNo, PatientID, PatientName, max(Point) as Point, CreateDateTime from ExpGameResultRecord " +
		"where CreateDateTime >= ? group by PatientID order by Point desc"

	startDateTime := time.Time(*filter.StartDateTime)

	rows, err := sqlAccess.Query(sqlString, startDateTime)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	} else {
		for rows.Next() {
			err = rows.Scan(&dbEntity.SerialNo, &dbEntity.PatientID, &dbEntity.PatientName, &dbEntity.Point, &dbEntity.CreateDateTime)
			if err != nil {
				return nil, business.NewError(errors.InternalError, err)
			}

			result := &doctor.ExpGameResultRecord{}
			dbEntity.CopyTo(result)
			results = append(results, result)
		}
	}

	return results, nil
}

//提交患者量表结果至引擎
func (s *Data) EditActionPlanRecordEng(sno uint64, zeroHour time.Time) error {
	dbFilter := &sqldb.ExpActionPlanRecordEditFilter{
		ActionPlanID:   sno,
		CreateDateTime: &zeroHour,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	dbEntity := &sqldb.ExpActionPlanRecord{}
	err := s.sqlDatabase.SelectOne(dbEntity, sqlFilter)
	if err != nil {
		return err
	}

	dataEntity := &doctor.ExpActionPlanRecord{}
	dbEntity.CopyTo(dataEntity)

	if dataEntity.SeqActionPlan == 0 {
		return nil
	}

	argument := &manage.ExpActionPlanRecordEditInput{}
	argument.PatientID = fmt.Sprint(dataEntity.PatientID)
	argument.ActionPlanRecordEditInfo.SerialNo = fmt.Sprint(dataEntity.SerialNo)
	argument.ActionPlanRecordEditInfo.Completion = dataEntity.Completion
	argument.ActionPlanRecordEditInfo.ActionTime = dataEntity.CreateDateTime
	providerType := config.ManagementProviderTypeDm
	uri := s.cfg.Management.Providers.Dm.Api.Uri.EditActionPlanRecord

	result, err := s.client.ManageBusinessPost("修改患者生活计划记录", dataEntity.PatientID, providerType, uri, argument)
	if err != nil {
		return err
	}
	if result.Code != 0 {
		return fmt.Errorf("%d-%s", result.Code, result.Message)
	}
	return nil
}

func (s *Data) GetPatientTodayTask(patientID uint64) (*doctor.ExpTaskRepositoryOutput, business.Error) {
	dbEntity := &sqldb.ExpPatientTaskList{}
	dbFilter := &sqldb.ExpPatientTaskListStatusFilter{
		Status:    0,
		PatientID: patientID,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	err := s.sqlDatabase.SelectOne(dbEntity, sqlFilter)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}

	taskList := make([][][]uint64, 0)
	err = json.Unmarshal([]byte(dbEntity.TaskList), &taskList)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}

	now := time.Now()
	dayGap := 0
	if dbEntity.UpdateTime != nil {
		dayGap = timeSub(now, *dbEntity.UpdateTime)
	} else {
		dayGap = timeSub(now, *dbEntity.CreateDateTime)
	}
	if dayGap > 0 {
		dbEntity.DayIndex++
	}
	dayIndex := int(dbEntity.DayIndex)
	weekIndex := 0
	weekDayIndex := 0
	for i := 0; i < len(taskList); i++ {
		if dayIndex >= len(taskList[i]) {
			weekIndex++
			dayIndex -= len(taskList[i])
		} else {
			weekDayIndex = dayIndex
			break
		}
	}
	if weekIndex >= len(taskList) {
		// 弃用该计划，并重新生成新计划
		sqlAccess, err := s.sqlDatabase.NewAccess(true)
		if err != nil {
			return nil, business.NewError(errors.InternalError, err)
		}
		defer sqlAccess.Close()

		dbEntity.Status = 1 //弃用
		dbEntity.UpdateTime = &now
		_, err = sqlAccess.UpdateSelectiveByPrimaryKey(dbEntity)
		if err != nil {
			return nil, business.NewError(errors.InternalError, err)
		}

		dbNewEntity := &sqldb.ExpPatientTaskList{
			PatientID:      patientID,
			TaskList:       dbEntity.TaskList,
			CreateDateTime: &now,
		}
		sno, err := sqlAccess.Insert(dbNewEntity)
		if err != nil {
			return nil, business.NewError(errors.InternalError, err)
		}

		err = sqlAccess.Commit()
		if err != nil {
			return nil, business.NewError(errors.InternalError, err)
		}

		weekIndex = 0
		weekDayIndex = 0 //用于返回当天计划
		dbEntity = dbNewEntity
		dbEntity.SerialNo = sno
	}
	// 返回当天计划
	result := &doctor.ExpTaskRepositoryOutput{}
	result.Snos = make([]uint64, 0)
	result.TaskName = make([]string, 0)
	result.Target = make([]string, 0)

	snos := taskList[weekIndex][weekDayIndex]
	dbTaskEntity := &sqldb.ExpTaskRepository{}
	if len(snos) > 0 {
		dbTaskFilter := &sqldb.ExpTaskRepositorySerialNosFilter{
			SerialNos: snos,
		}
		sqlTaskFilter := s.sqlDatabase.NewFilter(dbTaskFilter, false, false)
		err = s.sqlDatabase.SelectList(dbTaskEntity, func() {
			result.Snos = append(result.Snos, dbTaskEntity.SerialNo)
			result.TaskName = append(result.TaskName, dbTaskEntity.TaskName)
			if dbTaskEntity.Target != nil {
				result.Target = append(result.Target, *dbTaskEntity.Target)
			} else {
				result.Target = append(result.Target, "")
			}
		}, nil, sqlTaskFilter)
		if err != nil {
			return nil, business.NewError(errors.InternalError, err)
		}
	}

	result.TaskOrder = uint64(weekDayIndex + 1)
	result.DayIndex = dbEntity.DayIndex

	// 获取父任务信息
	dbParentTaskFilter := &sqldb.ExpTaskRepositoryTaskOrderFilter{
		Status:       0,
		ParentTaskID: 0,
		TaskOrder:    uint64(weekIndex + 1),
	}
	sqlParentTaskFilter := s.sqlDatabase.NewFilter(dbParentTaskFilter, false, false)
	err = s.sqlDatabase.SelectOne(dbTaskEntity, sqlParentTaskFilter)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}
	result.ParentTaskName = dbTaskEntity.TaskName
	if dbTaskEntity.Target != nil {
		result.ParentTaskTarget = *dbTaskEntity.Target
	}
	filter := &doctor.ExpKnowledgeTypeInput{
		Type:     result.ParentTaskName,
		Status:   0,
		DayIndex: result.DayIndex,
	}
	if dayGap > 0 || dbEntity.UpdateTime == nil {
		str := dbEntity.TaskList
		for i, task := range result.TaskName {
			//生成阅读任务
			if task == enum.TaskNames.Read().Value {
				flag, busErr := s.CreateKnowledgeFromPatient(patientID, filter)
				if busErr != nil {
					return nil, busErr
				}
				if flag {
					newTaskList, busErr := s.DeleteNextAnswerTask(patientID, int(result.DayIndex), taskList)
					if busErr != nil {
						return nil, busErr
					}
					strs, err := json.Marshal(newTaskList)
					if err != nil {
						return nil, business.NewError(errors.InternalError, err)
					}
					str = string(strs)
					newSnos := make([]uint64, 0)
					newTaskName := make([]string, 0)
					newTarget := make([]string, 0)
					for j := 0; j < len(newTaskList[weekIndex][weekDayIndex]); j++ {
						for k := 0; k < len(result.Snos); k++ {
							if result.Snos[k] == newTaskList[weekIndex][weekDayIndex][j] {
								newSnos = append(newSnos, result.Snos[k])
								newTaskName = append(newTaskName, result.TaskName[k])
								newTarget = append(newTarget, result.Target[k])
							}
						}
					}
					result.Snos = newSnos
					result.TaskName = newTaskName
					result.Target = newTarget
				}
			}
			if task == enum.TaskNames.Scale().Value {
				busErr := s.CreateSmallScaleFromPatient(patientID, snos[i])
				if busErr != nil {
					return nil, busErr
				}
			}
		}
		updateEntity := &sqldb.ExpPatientTaskListDayIndexUpdate{
			SerialNo:   dbEntity.SerialNo,
			TaskList:   str,
			DayIndex:   dbEntity.DayIndex,
			UpdateTime: &now,
		}
		_, err = s.sqlDatabase.UpdateSelectiveByPrimaryKey(updateEntity)
		if err != nil {
			return nil, business.NewError(errors.InternalError, err)
		}
	}

	return result, nil
}

func timeSub(t1, t2 time.Time) int {
	t1 = time.Date(t1.Year(), t1.Month(), t1.Day(), 0, 0, 0, 0, time.Local)
	t2 = time.Date(t2.Year(), t2.Month(), t2.Day(), 0, 0, 0, 0, time.Local)

	return int(t1.Sub(t2).Hours() / 24)
}

//获取患者今天的知识，如果没有，则自动创建后推送
func (s *Data) GetPatientTodayKnowledge(patientID uint64, filter *doctor.ExpKnowledgeTypeInput) (*doctor.ExpKnowledge, business.Error) {
	now := time.Now()
	now = time.Date(now.Year(), now.Month(), now.Day(), 0, 0, 0, 0, time.Local)
	dbEntity := &sqldb.ExpKnowledgeToPatient{}
	dbFilter := &sqldb.ExpKnowledgeToPatientFilter{
		PatientID:      patientID,
		DayIndex:       filter.DayIndex,
		CreateDateTime: &now,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	err := s.sqlDatabase.SelectOne(dbEntity, sqlFilter)
	if err != nil {
		if s.sqlDatabase.IsNoRows(err) {
			result, businessErr := s.CreateRandKnowledgeFromPatient(patientID, filter)
			if businessErr != nil {
				return nil, businessErr
			} else {
				return result, nil
			}
		} else {
			return nil, business.NewError(errors.InternalError, err)
		}
	}
	dbKnoEntity := &sqldb.ExpKnowledge{}
	dbKnoFilter := &sqldb.ExpKnowledgeSerialNoFilter{
		SerialNo: dbEntity.KnowledgeID,
	}
	sqlKnoFilter := s.sqlDatabase.NewFilter(dbKnoFilter, false, false)
	err = s.sqlDatabase.SelectOne(dbKnoEntity, sqlKnoFilter)
	result := &doctor.ExpKnowledge{}
	dbKnoEntity.CopyTo(result)
	result.Status = dbEntity.Status //修改状态为患者的学习状态

	return result, nil
}

func (s *Data) CommitKnowledge(patientID uint64, argument *doctor.ExpKnowledgeToPatientStatusEdit) business.Error {
	sqlAccess, err := s.sqlDatabase.NewAccess(true)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}
	defer sqlAccess.Close()

	dbEntity := &sqldb.ExpKnowledgeToPatient{}
	sqlEntity := s.sqlDatabase.NewEntity()
	sqlEntity.Parse(dbEntity)
	sqlBuilder := s.sqlDatabase.NewBuilder()
	sqlBuilder.Select(sqlEntity.ScanFields(), false).From(sqlEntity.Name())
	sqlBuilder.Append("where PatientID = ? and KnowledgeID = ? order by SerialNo desc limit 1")
	query := sqlBuilder.Query()

	rows, err := sqlAccess.Query(query, patientID, argument.KnowledgeID)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}
	for rows.Next() {
		err = rows.Scan(sqlEntity.ScanArgs()...)
		if err != nil {
			return business.NewError(errors.InternalError, err)
		}
	}

	updateEntity := &sqldb.ExpKnowledgeToPatientStatusEdit{
		SerialNo: dbEntity.SerialNo,
		Status:   argument.Status,
	}
	_, err = sqlAccess.UpdateSelectiveByPrimaryKey(updateEntity)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}

	now := types.Time(time.Now())
	knoID := &doctor.KnowledgeID{
		KnowledgeID: argument.KnowledgeID,
	}
	recordArgument := &doctor.ExpPatientTaskRecordInput{
		PatientID:      patientID,
		TaskID:         argument.TaskID,
		ParentTaskName: argument.ParentTaskName,
		TaskName:       enum.TaskNames.Read().Value,
		CreateDateTime: &now,
		Credit:         argument.Credit,
		Record:         knoID,
	}
	_, businessErr := s.CommitTaskRecord(recordArgument)
	if businessErr != nil {
		return businessErr
	}

	err = sqlAccess.Commit()
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}

	return nil
}

func (s *Data) GetHistoryKnowledge(patientID uint64) ([]*doctor.ExpKnowledge, business.Error) {
	idToStatus := make(map[uint64]uint64)
	ids := make([]uint64, 0)
	dbEntity := &sqldb.ExpKnowledgeToPatient{}
	dbFilter := &sqldb.ExpKnowledgeToPatientIDFilter{
		PatientID: patientID,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	err := s.sqlDatabase.SelectDistinct(dbEntity, func() {
		ids = append(ids, dbEntity.KnowledgeID)
		idToStatus[dbEntity.KnowledgeID] = dbEntity.Status
	}, nil, sqlFilter)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}

	results := make([]*doctor.ExpKnowledge, 0)
	knoEntity := &sqldb.ExpKnowledge{}
	knoFilter := &sqldb.ExpKnowledgeSerialNosFilter{
		SerialNos: ids,
	}
	sqlKnoFilter := s.sqlDatabase.NewFilter(knoFilter, false, false)
	err = s.sqlDatabase.SelectList(knoEntity, func() {
		result := &doctor.ExpKnowledge{}
		knoEntity.CopyTo(result)
		result.Status = idToStatus[knoEntity.SerialNo]
		results = append(results, result)
	}, nil, sqlKnoFilter)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}

	return results, nil
}

func (s *Data) GetKnowledgeByID(patientID uint64, filter *doctor.ExpKnowledgeSerialNoInput) (*doctor.ExpKnowledge, business.Error) {
	dbEntity := &sqldb.ExpKnowledge{}
	dbFilter := &sqldb.ExpKnowledgeSerialNoFilter{
		SerialNo: filter.SerialNo,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	err := s.sqlDatabase.SelectOne(dbEntity, sqlFilter)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}

	result := &doctor.ExpKnowledge{}
	dbEntity.CopyTo(result)

	//获取该患者这条知识的完成状态
	dbToEntity := &sqldb.ExpKnowledgeToPatient{}
	dbToFilter := &sqldb.ExpKnowledgeToPatientIDsFilter{
		PatientID:   patientID,
		KnowledgeID: filter.SerialNo,
	}
	sqlToFilter := s.sqlDatabase.NewFilter(dbToFilter, false, false)
	err = s.sqlDatabase.SelectOne(dbToEntity, sqlToFilter)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}

	result.Status = dbToEntity.Status

	return result, nil
}

//为患者生成一个随机的知识（可能是以前阅读过的）
func (s *Data) CreateRandKnowledgeFromPatient(patientID uint64, filter *doctor.ExpKnowledgeTypeInput) (*doctor.ExpKnowledge, business.Error) {
	sqlAccess, err := s.sqlDatabase.NewAccess(true)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}
	defer sqlAccess.Close()

	dbEntity := &sqldb.ExpKnowledge{}
	sqlEntity := s.sqlDatabase.NewEntity()
	sqlEntity.Parse(dbEntity)
	sqlBuilder := s.sqlDatabase.NewBuilder()
	sqlBuilder.Select(sqlEntity.ScanFields(), false).From(sqlEntity.Name())
	sqlBuilder.Append("where SerialNo >= (Rand()*(select max(SerialNo) from ExpKnowledge)) and Type = ? and Status = ? order by SerialNo limit 1")
	query := sqlBuilder.Query()

	rows, err := sqlAccess.Query(query, filter.Type, filter.Status)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}
	for rows.Next() {
		err = rows.Scan(sqlEntity.ScanArgs()...)
		if err != nil {
			return nil, business.NewError(errors.InternalError, err)
		}
	}

	result := &doctor.ExpKnowledge{}
	dbEntity.CopyTo(result)

	now := time.Now()
	dbNewEntity := &sqldb.ExpKnowledgeToPatient{
		PatientID:      patientID,
		KnowledgeID:    dbEntity.SerialNo,
		DayIndex:       filter.DayIndex,
		CreateDateTime: &now,
	}
	_, err = sqlAccess.InsertSelective(dbNewEntity)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}

	err = sqlAccess.Commit()
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}
	return result, nil
}

// 为患者按顺序生成一个知识，不会重复，如果return true，代表这个知识没有相关问题，需要将下一次答题任务取消
func (s *Data) CreateKnowledgeFromPatient(patientID uint64, filter *doctor.ExpKnowledgeTypeInput) (bool, business.Error) {
	sqlAccess, err := s.sqlDatabase.NewAccess(true)
	if err != nil {
		return false, business.NewError(errors.InternalError, err)
	}
	defer sqlAccess.Close()

	dbEntity := &sqldb.ExpKnowledgeToPatient{}
	sqlEntity := s.sqlDatabase.NewEntity()
	sqlEntity.Parse(dbEntity)
	sqlBuilder := s.sqlDatabase.NewBuilder()
	sqlBuilder.Select(sqlEntity.ScanFields(), false).From(sqlEntity.Name())
	sqlBuilder.Append("where PatientID = ? and Type = ? order by CreateDateTime desc limit 1")
	query := sqlBuilder.Query()

	row := sqlAccess.QueryRow(query, patientID, filter.Type)
	err = row.Scan(sqlEntity.ScanArgs()...)
	if err != nil {
		if !s.sqlDatabase.IsNoRows(err) {
			return false, business.NewError(errors.InternalError, err)
		}
	}

	dbKnoEntity := &sqldb.ExpKnowledge{}
	sqlKnoEntity := s.sqlDatabase.NewEntity()
	sqlKnoEntity.Parse(dbKnoEntity)
	sqlKnoBuilder := s.sqlDatabase.NewBuilder()
	sqlKnoBuilder.Select(sqlKnoEntity.ScanFields(), false).From(sqlKnoEntity.Name())
	sqlKnoBuilder.Append("where SerialNo > ? and Type = ? and Status = 0 order by SerialNo limit 1")
	knoQuery := sqlKnoBuilder.Query()
	knoRow := sqlAccess.QueryRow(knoQuery, dbEntity.KnowledgeID, filter.Type)
	err = knoRow.Scan(sqlKnoEntity.ScanArgs()...)
	if err != nil {
		if s.sqlDatabase.IsNoRows(err) {
			//一轮知识学习完毕后，下一步解决方案，待考虑
			return false, nil
		}
		return false, business.NewError(errors.InternalError, err)
	}

	now := time.Now()
	dbNewToEntity := &sqldb.ExpKnowledgeToPatient{
		PatientID:      patientID,
		Type:           dbKnoEntity.Type,
		KnowledgeID:    dbKnoEntity.SerialNo,
		CreateDateTime: &now,
		DayIndex:       filter.DayIndex,
	}
	_, err = sqlAccess.InsertSelective(dbNewToEntity)
	if err != nil {
		return false, business.NewError(errors.InternalError, err)
	}

	err = sqlAccess.Commit()
	if err != nil {
		return false, business.NewError(errors.InternalError, err)
	}

	dbQuestionEntity := &sqldb.ExpQuestionToKnowledge{}
	dbQuestionFilter := &sqldb.ExpQuestionKnowledgeIDFilter{
		KnowledgeID: dbKnoEntity.SerialNo,
		Status:      0,
	}
	sqlQuestionFilter := s.sqlDatabase.NewFilter(dbQuestionFilter, false, false)
	err = s.sqlDatabase.SelectOne(dbQuestionEntity, sqlQuestionFilter)
	if err != nil {
		if s.sqlDatabase.IsNoRows(err) {
			return true, nil
		} else {
			return false, business.NewError(errors.InternalError, err)
		}
	}

	return false, nil
}

func (s *Data) DeleteNextAnswerTask(patientID uint64, dayIndex int, taskList [][][]uint64) ([][][]uint64, business.Error) {
	weekIndex := 0
	weekDayIndex := 0
	for i := 0; i < len(taskList); i++ {
		if dayIndex >= len(taskList[i]) {
			weekIndex++
			dayIndex -= len(taskList[i])
		} else {
			weekDayIndex = dayIndex
			break
		}
	}

	flag := false
	for j := weekDayIndex + 1; j < len(taskList[weekIndex]); j++ {
		if flag {
			break
		}
		if len(taskList[weekIndex][j]) == 0 {
			continue
		}
		dbEntity := &sqldb.ExpTaskRepository{}
		dbFilter := &sqldb.ExpTaskRepositorySerialNosFilter{
			SerialNos: taskList[weekIndex][j],
		}
		sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
		err := s.sqlDatabase.SelectList(dbEntity, func() {
			if dbEntity.TaskName == "答题" {
				newTaskList := make([]uint64, 0)
				for k := 0; k < len(taskList[weekIndex][j]); k++ {
					if dbEntity.SerialNo != taskList[weekIndex][j][k] {
						newTaskList = append(newTaskList, taskList[weekIndex][j][k])
					}
				}
				taskList[weekIndex][j] = newTaskList
				flag = true
			}
		}, nil, sqlFilter)
		if err != nil {
			return nil, business.NewError(errors.InternalError, err)
		}
	}

	return taskList, nil
}

//为患者生成问卷
func (s *Data) CreateSmallScaleFromPatient(patientID uint64, sno uint64) business.Error {
	dbEntity := &sqldb.ExpSmallScale{}
	dbFilter := &sqldb.ExpSmallScaleTaskIDFilter{
		TaskID: sno,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	err := s.sqlDatabase.SelectOne(dbEntity, sqlFilter)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}

	//检查该患者是否有未完成的该问卷，如果有，此次不需要单独生成，否则会存在重复的未完成问卷
	dbToEntity := &sqldb.ExpScaleToPatient{}
	dbToFilter := &sqldb.ExpScaleToPatientSmallScaleFilter{
		SmallScaleID: &dbEntity.SerialNo,
		Status:       0,
	}
	sqlToFilter := s.sqlDatabase.NewFilter(dbToFilter, false, false)
	err = s.sqlDatabase.SelectOne(dbToEntity, sqlToFilter)
	if err != nil {
		if !s.sqlDatabase.IsNoRows(err) {
			return business.NewError(errors.InternalError, err)
		}
	} else {
		return nil
	}

	now := time.Now()
	dbNewToEntity := &sqldb.ExpScaleToPatient{
		PatientID:      patientID,
		TaskID:         &sno,
		SmallScaleID:   &dbEntity.SerialNo,
		CreateDateTime: &now,
	}
	_, err = s.sqlDatabase.InsertSelective(dbNewToEntity)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}

	return nil
}

func (s *Data) GetQuestionByDayIndex(argument *doctor.ExpKnowledgeToPatientDayIndex) ([]*doctor.ExpQuestionToKnowledge, business.Error) {
	sqlAccess, err := s.sqlDatabase.NewAccess(true)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}
	defer sqlAccess.Close()

	dbEntity := &sqldb.ExpKnowledgeToPatient{}
	sqlEntity := s.sqlDatabase.NewEntity()
	sqlEntity.Parse(dbEntity)
	sqlBuilder := s.sqlDatabase.NewBuilder()
	sqlBuilder.Select(sqlEntity.ScanFields(), false).From(sqlEntity.Name())
	sqlBuilder.Append("where patientID = ? and dayIndex < ? order by dayIndex desc, serialNo desc limit 1")
	query := sqlBuilder.Query()

	rows, err := sqlAccess.Query(query, argument.PatientID, argument.DayIndex)
	if err != nil {
		if s.sqlDatabase.IsNoRows(err) {
			return nil, nil
		} else {
			return nil, business.NewError(errors.InternalError, err)
		}
	}
	for rows.Next() {
		err = rows.Scan(sqlEntity.ScanArgs()...)
		if err != nil {
			return nil, business.NewError(errors.InternalError, err)
		}
	}

	results := make([]*doctor.ExpQuestionToKnowledge, 0)
	dbQuestionEntity := &sqldb.ExpQuestionToKnowledge{}
	dbQuestionFilter := &sqldb.ExpQuestionKnowledgeIDFilter{
		KnowledgeID: dbEntity.KnowledgeID,
		Status:      0,
	}
	sqlQuestionFilter := s.sqlDatabase.NewFilter(dbQuestionFilter, false, false)
	err = sqlAccess.SelectList(dbQuestionEntity, func() {
		result := &doctor.ExpQuestionToKnowledge{}
		dbQuestionEntity.CopyTo(result)
		results = append(results, result)
	}, nil, sqlQuestionFilter)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}

	err = sqlAccess.Commit()
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}

	return results, nil
}

func (s *Data) CommitTaskRecord(argument *doctor.ExpPatientTaskRecordInput) (uint64, business.Error) {
	sqlAccess, err := s.sqlDatabase.NewAccess(true)
	if err != nil {
		return 0, business.NewError(errors.InternalError, err)
	}
	defer sqlAccess.Close()

	// 查找当前积分规则
	pointEntity := &sqldb.ExpIntegralRule{}
	pointFilter := &sqldb.ExpIntegralRuleFilter{OrgCode: "20210422"}
	sqlPointFilter := s.sqlDatabase.NewFilter(pointFilter, false, false)
	err = sqlAccess.SelectOne(pointEntity, sqlPointFilter)
	if err != nil {
		return 0, business.NewError(errors.InternalError, err)
	}

	dbEntity := &sqldb.ExpPatientTaskRecordInput{}
	dbEntity.CopyFrom(argument)
	_, err = sqlAccess.InsertSelective(dbEntity)
	if err != nil {
		return 0, business.NewError(errors.InternalError, err)
	}
	t := time.Now()
	today := time.Date(t.Year(), t.Month(), t.Day(), 0, 0, 0, 0, time.Local)
	dbIntEntity := &sqldb.ExpIntegral{}
	dbIntFilter := &sqldb.ExpIntegralFilter{
		PatientID:      argument.PatientID,
		Reason:         "三餐打卡",
		CreateDateTime: &today,
	}
	sqlIntFilter := s.sqlDatabase.NewFilter(dbIntFilter, false, false)
	foodPoint := 0
	sportPoint := 0
	weightPoint := 0
	heEduPoint := 0 //健康教育的积分
	//existFoodPoint := 0
	if *argument.ParentTaskName == "早餐" || *argument.ParentTaskName == "中餐" || *argument.ParentTaskName == "晚餐" {
		//判断今天是否已经给过积分
		count, err := sqlAccess.SelectCount(dbIntEntity, sqlIntFilter)
		//updateFoodFlag := false
		if err != nil {
			if !sqlAccess.IsNoRows(err) {
				return 0, business.NewError(errors.InternalError, err)
			}
		}
		if count == 0 {
			//return 0, nil
			foodPoint = int(*pointEntity.FirstThreeMeals)
		} else {
			dbTaskEntity := &sqldb.ExpPatientTaskRecord{}
			dbTaskFilter := &sqldb.ExpPatientTaskRecordTimeFilter{
				PatientID:      argument.PatientID,
				CreateDateTime: &today,
			}
			sqlTaskFilter := s.sqlDatabase.NewFilter(dbTaskFilter, false, false)
			m := make(map[string]int)
			m[*argument.ParentTaskName] = 1
			err = sqlAccess.SelectList(dbTaskEntity, func() {
				if *dbTaskEntity.ParentTaskName == "早餐" || *dbTaskEntity.ParentTaskName == "中餐" || *dbTaskEntity.ParentTaskName == "晚餐" {
					m[*dbTaskEntity.ParentTaskName] = 1
				}
			}, nil, sqlTaskFilter)
			if err != nil {
				return 0, business.NewError(errors.InternalError, err)
			}
			if count == 1 && len(m) == 2 {
				foodPoint = int(*pointEntity.SecondThreeMeals) - int(*pointEntity.FirstThreeMeals)
			} else if count == 2 && len(m) == 3 {
				foodPoint = int(*pointEntity.ThirdThreeMeals) - int(*pointEntity.SecondThreeMeals)
			} else {
				foodPoint = 0
			}
		}
		if foodPoint != 0 {
			err = s.AddIntegralForPatient(argument.PatientID, uint64(foodPoint), "三餐打卡")
			if err != nil {
				return 0, business.NewError(errors.InternalError, err)
			}
		}
		//每日运动积分移到另一个地方，因为还要判断运动时长等其他条件
		//} else if *argument.ParentTaskName == "每日运动" {
		//	sportPoint = 3
		//	if (*argument.)
		//	//判断今天是否已经给过积分
		//	dbIntFilter.Reason = "每日运动"
		//	err = sqlAccess.SelectOne(dbIntEntity, sqlIntFilter)
		//	sportFlag := false
		//	if err == nil {
		//		sportFlag = true
		//		sportPoint = 0
		//		//return 0, nil
		//	} else if err != nil {
		//		if !sqlAccess.IsNoRows(err) {
		//			return 0, business.NewError(errors.InternalError, err)
		//		}
		//	}
		//	if !sportFlag {
		//		err = s.AddIntegralForPatient(argument.PatientID, uint64(sportPoint), "每日运动")
		//		if err != nil {
		//			return 0, business.NewError(errors.InternalError, err)
		//		}
		//	}
	} else if *argument.ParentTaskName == "晨起称重" {
		weightPoint = int(*pointEntity.Weigh)
		//判断今天是否已经给过积分
		dbIntFilter.Reason = "晨起称重"
		err = sqlAccess.SelectOne(dbIntEntity, sqlIntFilter)
		weightFlag := false
		if err == nil {
			weightFlag = true
			weightPoint = 0
			//return 0, nil
		} else if err != nil {
			if !sqlAccess.IsNoRows(err) {
				return 0, business.NewError(errors.InternalError, err)
			}
		}
		if !weightFlag {
			err = s.AddIntegralForPatient(argument.PatientID, uint64(weightPoint), "晨起称重")
			if err != nil {
				return 0, business.NewError(errors.InternalError, err)
			}
		}
	} else if *argument.ParentTaskName == "健康教育" {
		/**
		健康教育-知识阅读的积分奖励
		2022/08/22
		*/
		heEduPoint = int(*pointEntity.LearnKnw)
		//判断今日是否给过健康教育的积分
		dbIntFilter.Reason = "健康教育"
		err = sqlAccess.SelectOne(dbIntEntity, sqlIntFilter)
		heEduFlag := false
		if err == nil {
			//已经给过健康教育的积分
			heEduFlag = true
			heEduPoint = 0
		} else if err != nil {
			if !sqlAccess.IsNoRows(err) {
				return 0, business.NewError(errors.InternalError, err)
			}
		}
		//今日没给过健康教育的积分
		if !heEduFlag {
			err = s.AddIntegralForPatient(argument.PatientID, uint64(heEduPoint), "健康教育")
			if err != nil {
				return 0, business.NewError(errors.InternalError, err)
			}
		}
	}

	//完成了每日三餐打卡，需要给予一定积分
	//	dbBaseEntity := &sqldb.PatientUserAuths{}
	//	dbBaseFilter := &sqldb.PatientUserAuthsUserIdFilter{
	//		UserID: argument.PatientID,
	//	}
	//	sqlBaseFilter := s.sqlDatabase.NewFilter(dbBaseFilter, false, false)
	//	err = s.sqlDatabase.SelectOne(dbBaseEntity, sqlBaseFilter)
	//	if err != nil {
	//		return 0, business.NewError(errors.InternalError, err)
	//	}
	//t2 := time.Date(dbBaseEntity.RegistDateTime.Year(), dbBaseEntity.RegistDateTime.Month(), dbBaseEntity.RegistDateTime.Day(), 0, 0, 0, 0, time.Local)
	//timeGap := uint64(today.Sub(t2).Hours()/24)-1
	//if timeGap < 0 || timeGap > 28 { //从注册第二天开始
	//	return 0, nil
	//}
	//integral := 10 + timeGap*10 //计算今天需要提供多少分数

	////同步在总积分表里更新积分，需先判断是否存在该用户
	total := uint64(foodPoint + sportPoint + weightPoint + heEduPoint)
	//dbTotalEntity := &sqldb.Exptotalpoint{}
	//dbTotalFilter := &sqldb.ExptotalPointFilter{
	//	PatientID: argument.PatientID,
	//}
	//sqlTotalFilter := s.sqlDatabase.NewFilter(dbTotalFilter, false, false)
	//err = sqlAccess.SelectOne(dbTotalEntity, sqlTotalFilter)
	//if err == nil {
	//	newTotalPoint := dbTotalEntity.TotalPoint + total
	//	dbTotalUpdate := &sqldb.ExptotalPointUpdate{
	//		TotalPoint: newTotalPoint,
	//	}
	//	sqlAccess.Update(dbTotalUpdate, sqlTotalFilter)
	//} else {
	//	dbInfoEntity := &sqldb.PatientUserBaseInfo{}
	//	dbInfoFilter := &sqldb.PatientUserBaseInfoUserIdFilter{
	//		UserID: argument.PatientID,
	//	}
	//	sqlInfoFilter := s.sqlDatabase.NewFilter(dbInfoFilter, false, false)
	//	err = sqlAccess.SelectOne(dbInfoEntity, sqlInfoFilter)
	//	if err == nil {
	//		dbNewEntity := &sqldb.Exptotalpoint{
	//			PatientID:   argument.PatientID,
	//			PatientName: dbInfoEntity.Name,
	//			TotalPoint:  total,
	//		}
	//		_, err = sqlAccess.Insert(dbNewEntity)
	//	}
	//}
	err = sqlAccess.Commit()
	if err != nil {
		return 0, business.NewError(errors.InternalError, err)
	}
	return total, nil

}

func (s *Data) AddIntegralForPatient(patientID uint64, integral uint64, reason string) error {
	t := time.Now()
	dbEntity := &sqldb.ExpIntegral{
		PatientID:      patientID,
		Integral:       integral,
		Reason:         reason,
		CreateDateTime: &t,
	}
	_, err := s.sqlDatabase.InsertSelective(dbEntity)
	if err != nil {
		return err
	}

	return nil
}

//func (s *Data) UpdateIntegralForPatient(patientID uint64, integral uint64, reason string) error {
//	t := time.Now()
//	dbEntity := &sqldb.ExpIntegralUpdate{
//		Integral: integral,
//		CreateDateTime:  &t,
//		ReadFlag: 0,
//	}
//	today := time.Date(t.Year(), t.Month(), t.Day(), 0, 0, 0, 0, time.Local)
//	dbFilter := &sqldb.ExpIntegralFilter{
//		PatientID: patientID,
//		Reason: reason,
//		CreateDateTime: &today,
//	}
//	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
//	_, err := s.sqlDatabase.Update(dbEntity,sqlFilter)
//	if err != nil {
//		return err
//	}
//
//	return nil
//}

func (s *Data) GetPatientUnreadIntegral(argument *doctor.ExpIntegralUnreadGet) ([]*doctor.ExpIntegral, business.Error) {
	results := make([]*doctor.ExpIntegral, 0)
	dbEntity := &sqldb.ExpIntegral{}
	dbFilter := &sqldb.ExpIntegralUnreadGet{
		PatientID: argument.PatientID,
		ReadFlag:  argument.ReadFlag,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	err := s.sqlDatabase.SelectList(dbEntity, func() {
		result := &doctor.ExpIntegral{}
		dbEntity.CopyTo(result)
		results = append(results, result)
	}, nil, sqlFilter)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}

	return results, nil
}

func (s *Data) ReadPatientIntegral(argument *doctor.ExpIntegralRead) business.Error {
	dbEntity := &sqldb.ExpIntegralReadUpdate{}
	for _, serialNo := range argument.Snos {
		dbEntity.SerialNo = serialNo
		dbEntity.ReadFlag = 1
		_, err := s.sqlDatabase.UpdateSelectiveByPrimaryKey(dbEntity)
		if err != nil {
			return business.NewError(errors.InternalError, err)
		}
	}

	return nil
}

func (s *Data) GetDietPlan(argument *doctor.ExpPatientDietPlanGet) ([]*doctor.ExpPatientDietPlan, business.Error) {
	results := make([]*doctor.ExpPatientDietPlan, 0)
	dbEntity := &sqldb.ExpPatientDietPlan{}
	dbFilter := &sqldb.ExpPatientDietPlanGetFilter{
		PatientID: argument.PatientID,
		Level:     "弃用",
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	err := s.sqlDatabase.SelectList(dbEntity, func() {
		result := &doctor.ExpPatientDietPlan{}
		dbEntity.CopyTo(result)
		results = append(results, result)
	}, nil, sqlFilter)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}
	return results, nil
}

func (s *Data) GetDietPlanByLevel(argument *doctor.ExpPatientDietPlanGetByLevel) ([]*doctor.ExpPatientDietPlan, business.Error) {
	results := make([]*doctor.ExpPatientDietPlan, 0)
	dbEntity := &sqldb.ExpPatientDietPlan{}
	dbFilter := &sqldb.ExpPatientDietPlanLevelFilter{
		PatientID: argument.PatientID,
		Level:     argument.Level,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	err := s.sqlDatabase.SelectList(dbEntity, func() {
		result := &doctor.ExpPatientDietPlan{}
		dbEntity.CopyTo(result)
		results = append(results, result)
	}, nil, sqlFilter)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}
	return results, nil
}

func (s *Data) ReadDietPlan(argument *doctor.ExpPatientDietPlanRead) business.Error {
	dbEntity := &sqldb.ExpPatientDietPlanRead{
		ChangeStatus: 0,
	}
	for _, sno := range argument.SerialNos {
		dbEntity.SerialNo = sno
		_, err := s.sqlDatabase.UpdateSelectiveByPrimaryKey(dbEntity)
		if err != nil {
			return business.NewError(errors.InternalError, err)
		}
	}

	return nil
}

func (s *Data) GetUndoneSmallScale(patientID uint64) ([]*doctor.ExpSmallScale, business.Error) {
	results := make([]*doctor.ExpSmallScale, 0)

	snos := make([]uint64, 0)
	dbToEntity := &sqldb.ExpScaleToPatient{}
	dbToFilter := &sqldb.ExpScaleToPatientUndoneFilter{
		PatientID: patientID,
		Status:    0, //未完成
	}
	sqlToFilter := s.sqlDatabase.NewFilter(dbToFilter, false, false)
	err := s.sqlDatabase.SelectList(dbToEntity, func() {
		snos = append(snos, *dbToEntity.SmallScaleID)
	}, nil, sqlToFilter)
	if err != nil {
		if s.sqlDatabase.IsNoRows(err) {
			return results, nil
		}
		return nil, business.NewError(errors.InternalError, err)
	}

	dbEntity := &sqldb.ExpSmallScale{}
	dbFilter := &sqldb.ExpSmallScaleIDsFilter{
		SerialNos: snos,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	err = s.sqlDatabase.SelectList(dbEntity, func() {
		result := &doctor.ExpSmallScale{}
		dbEntity.CopyTo(result)
		results = append(results, result)
	}, nil, sqlFilter)

	return results, nil
}

func (s *Data) GetSmallScale(argument *doctor.ExpSmallScaleTaskIDInput) (*doctor.ExpSmallScale, business.Error) {
	dbEntity := &sqldb.ExpSmallScale{}
	dbFilter := &sqldb.ExpSmallScaleTaskIDFilter{
		TaskID: argument.TaskID,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	err := s.sqlDatabase.SelectOne(dbEntity, sqlFilter)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}

	result := &doctor.ExpSmallScale{}
	dbEntity.CopyTo(result)

	return result, nil
}

func (s *Data) EditSmallScale(argument *doctor.ExpSmallScaleContentEdit) business.Error {
	dbEntity := &sqldb.ExpSmallScaleEdit{}
	dbEntity.CopyFrom(argument)
	_, err := s.sqlDatabase.UpdateSelectiveByPrimaryKey(dbEntity)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}

	return nil
}

func (s *Data) CommitSmallScaleRecord(argument *doctor.ExpSmallScaleRecordInput) business.Error {
	dbEntity := &sqldb.ExpSmallScaleRecord{}
	dbEntity.CopyFromInput(argument)
	sno, err := s.sqlDatabase.Insert(dbEntity)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}

	now := time.Now()
	dbToEntity := &sqldb.ExpScaleToPatientUpdate{
		Status:     1, // 已完成
		UpdateTime: &now,
		RecordID:   &sno,
	}
	dbToFilter := &sqldb.ExpScaleToPatientSmallScaleFilter{
		SmallScaleID: &argument.SmallScaleID,
		Status:       0,
	}
	sqlToFilter := s.sqlDatabase.NewFilter(dbToFilter, false, false)
	_, err = s.sqlDatabase.UpdateSelective(dbToEntity, sqlToFilter)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}

	recordArgument := &doctor.ExpPatientTaskRecordInput{
		PatientID:      argument.PatientID,
		TaskID:         argument.TaskID,
		TaskName:       enum.TaskNames.Scale().Value,
		CreateDateTime: argument.CreateDateTime,
		Credit:         argument.Credit,
		Record:         argument.Result,
	}
	_, businessErr := s.CommitTaskRecord(recordArgument)
	if businessErr != nil {
		return businessErr
	}

	return nil
}

func (s *Data) GetTaskRecord(argument *doctor.ExpPatientTaskRecordGetByTaskIDsAndTime) ([]*doctor.ExpPatientTaskRecord, business.Error) {
	results := make([]*doctor.ExpPatientTaskRecord, 0)
	dbEntity := &sqldb.ExpPatientTaskRecord{}
	dbFilter := &sqldb.ExpPatientTaskRecordGetByTaskIDsAndTime{}
	dbFilter.CopyFrom(argument)
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)

	err := s.sqlDatabase.SelectList(dbEntity, func() {
		result := &doctor.ExpPatientTaskRecord{}
		dbEntity.CopyTo(result)
		results = append(results, result)
	}, nil, sqlFilter)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}

	return results, nil
}

func (s *Data) GetTaskCreditHistory(patientID uint64) ([]*doctor.ExpPatientTaskCreditHistory, business.Error) {
	results := make([]*doctor.ExpPatientTaskCreditHistory, 0)
	dbEntity := &sqldb.ExpPatientTaskRecord{}
	dbFilter := &sqldb.ExpPatientTaskRecordGetByPatientID{
		PatientID: patientID,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	dbOrder := &sqldb.ExpPatientTaskRecordSerialNoOrder{}
	err := s.sqlDatabase.SelectList(dbEntity, func() {
		result := &doctor.ExpPatientTaskCreditHistory{}
		dbEntity.CopyToCreditHistory(result)
		results = append(results, result)
	}, dbOrder, sqlFilter)
	if err != nil {
		if s.sqlDatabase.IsNoRows(err) {
			return results, nil
		} else {
			return nil, business.NewError(errors.InternalError, err)
		}
	}

	return results, nil
}

func (s *Data) GetUnreadDoctorCommentList(argument *doctor.ExpDoctorCommentToPatientUnreadGet) ([]*doctor.ExpDoctorCommentToPatient, business.Error) {
	results := make([]*doctor.ExpDoctorCommentToPatient, 0)
	dbEntity := &sqldb.ExpDoctorCommentToPatient{}
	dbFilter := &sqldb.ExpDoctorCommentToPatientUnreadGet{}
	dbFilter.CopyFrom(argument)
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	err := s.sqlDatabase.SelectList(dbEntity, func() {
		result := &doctor.ExpDoctorCommentToPatient{}
		dbEntity.CopyTo(result)
		results = append(results, result)
	}, nil, sqlFilter)
	if err != nil {
		if s.sqlDatabase.IsNoRows(err) {
			return nil, nil
		} else {
			return nil, business.NewError(errors.InternalError, err)
		}
	}

	return results, nil
}

func (s *Data) ReadComment(argument *doctor.ExpDoctorCommentToPatientRead) business.Error {
	dbEntity := &sqldb.ExpDoctorCommentToPatientRead{
		SerialNo: argument.SerialNo,
		ReadFlag: argument.ReadFlag,
	}
	_, err := s.sqlDatabase.UpdateSelectiveByPrimaryKey(dbEntity)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}

	return nil
}

func (s *Data) GetCommentListBySnos(argument *doctor.ExpDoctorCommentToPatientSportDietSnosFilter) ([]*doctor.ExpDoctorCommentToPatient, business.Error) {
	results := make([]*doctor.ExpDoctorCommentToPatient, 0)
	dbEntity := &sqldb.ExpDoctorCommentToPatient{}
	dbFilter := &sqldb.ExpDoctorCommentToPatientSportDietRecordSnosFilter{
		SportDietRecordList: argument.SportDietRecordList,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)

	err := s.sqlDatabase.SelectList(dbEntity, func() {
		result := &doctor.ExpDoctorCommentToPatient{}
		dbEntity.CopyTo(result)
		results = append(results, result)
	}, nil, sqlFilter)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}

	return results, nil
}

func (s *Data) GetDietAndCommentInfo(argument *doctor.ExpDoctorCommentToPatientFilter) (*doctor.ExpDoctorCommentToPatientWithDietInfo, business.Error) {
	dbEntity := &sqldb.ExpDoctorCommentToPatient{}
	dbFilter := &sqldb.ExpDoctorCommentToPatientFilter{
		SerialNo: argument.SerialNo,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	err := s.sqlDatabase.SelectOne(dbEntity, sqlFilter)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}

	dbDietEntity := &sqldb.SportDietRecord{}
	dbDietFilter := &sqldb.SportDietRecordFilter{
		SerialNo: &dbEntity.SportDietRecord,
	}
	sqlDietFilter := s.sqlDatabase.NewFilter(dbDietFilter, false, false)
	err = s.sqlDatabase.SelectOne(dbDietEntity, sqlDietFilter)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}

	dietResult := &doctor.SportDietRecord{}
	dbDietEntity.CopyTo(dietResult)
	result := &doctor.ExpDoctorCommentToPatientWithDietInfo{}
	dbEntity.CopyToWithDietInfo(result)
	result.DietInfo = dietResult

	return result, nil
}

func (s *Data) GetTodayUnreadCommentCount(argument *doctor.ExpDoctorCommentToPatientTodayUnreadGet) (uint64, business.Error) {
	result := 0

	dbEntity := &sqldb.ExpDoctorCommentToPatient{}
	dbFilter := &sqldb.ExpDoctorCommentToPatientTodayUnreadGet{}
	dbFilter.CopyFrom(argument)
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	err := s.sqlDatabase.SelectList(dbEntity, func() {
		result++
	}, nil, sqlFilter)
	if err != nil {
		return 0, business.NewError(errors.InternalError, err)
	}

	return uint64(result), nil
}

func (s *Data) UploadPatientBodyExamImage(argument *doctor.ExpPatientBodyExamImageCreate) business.Error {
	sqlAccess, err := s.sqlDatabase.NewAccess(true)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}
	defer sqlAccess.Close()

	dbEntity := &sqldb.ExpPatientBodyExamImage{}
	dbFilter := &sqldb.ExpPatientBodyExamImageFilter{
		PatientID: argument.PatientID,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)

	sqlAccess.Delete(dbEntity, sqlFilter)

	dbEntity.CopyFromCreate(argument)
	for _, imgUrl := range argument.ImgUrlList {
		dbEntity.ImgUrl = imgUrl
		_, err = sqlAccess.Insert(dbEntity)
		if err != nil {
			return business.NewError(errors.InternalError, err)
		}
	}

	err = sqlAccess.Commit()
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}

	return nil
}

func (s *Data) GetJinScale(argument *doctor.ExpJinScaleCampFilter) ([]*doctor.ExpJinScale, business.Error) {
	results := make([]*doctor.ExpJinScale, 0)
	dbEntity := &sqldb.ExpJinScale{}
	dbFilter := &sqldb.ExpJinScaleCampFilter{
		Description: &argument.Description,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	err := s.sqlDatabase.SelectList(dbEntity, func() {
		result := &doctor.ExpJinScale{}
		dbEntity.CopyTo(result)
		results = append(results, result)
	}, nil, sqlFilter)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}

	return results, nil
}

func (s *Data) GetFinishedJinScale(patientID uint64) ([]string, business.Error) {
	results := make([]string, 0)
	dbEntity := &sqldb.ExpJinScaleRecord{}
	dbFilter := &sqldb.ExpJinScaleRecordPatientFilter{
		PatientID: patientID,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	err := s.sqlDatabase.SelectList(dbEntity, func() {
		result := dbEntity.ScaleName
		results = append(results, result)
	}, nil, sqlFilter)
	if err != nil {
		if s.sqlDatabase.IsNoRows(err) {
			return results, nil
		}
		return nil, business.NewError(errors.InternalError, err)
	}

	return results, nil
}

func (s *Data) GetExercisePlan(patientID uint64) ([]*doctor.ExpExerciseBase, business.Error) {
	plans := make([]*doctor.ExpExercisePlan, 0)
	dbEntity := &sqldb.ExpExercisePlan{}
	dbFilter := &sqldb.ExpExercisePlanPatientFilter{
		PatientID: patientID,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	err := s.sqlDatabase.SelectList(dbEntity, func() {
		result := &doctor.ExpExercisePlan{}
		dbEntity.CopyTo(result)
		plans = append(plans, result)
	}, nil, sqlFilter)
	if err != nil {
		if s.sqlDatabase.IsNoRows(err) {
			return nil, nil
		}
		return nil, business.NewError(errors.InternalError, err)
	}
	minCount := math.MaxInt8
	var result *doctor.ExpExercisePlan
	for i := 0; i < len(plans); i++ {
		if int(plans[i].Counts) < minCount {
			result = plans[i]
			minCount = int(plans[i].Counts)
		}
	}

	var snos []uint64
	err = json.Unmarshal([]byte(result.ExercisePlan), &snos)

	dbBaseEntity := &sqldb.ExpExerciseBase{}
	dbBaseFilter := &sqldb.ExpExerciseBaseSnosFilter{
		Snos: snos,
	}
	sqlBaseFilter := s.sqlDatabase.NewFilter(dbBaseFilter, false, false)
	results := make([]*doctor.ExpExerciseBase, 0)
	err = s.sqlDatabase.SelectList(dbBaseEntity, func() {
		base := &doctor.ExpExerciseBase{}
		dbBaseEntity.CopyTo(base)
		results = append(results, base)
	}, nil, sqlBaseFilter)

	return results, nil
}

func (s *Data) CreateExercisePlan(argument *doctor.ExpExercisePlanCreate) business.Error {
	day := make([][]uint64, 4)
	for i := 0; i < len(argument.ChoosePlan); i++ {
		plan := argument.ChoosePlan[i]
		if plan.Target == "肩颈" || plan.Target == "胸部" || plan.Target == "腹部" {
			day[0] = append(day[0], plan.SerialNo)
		} else if plan.Target == "手臂" || plan.Target == "背部" || plan.Target == "腹部" {
			day[1] = append(day[1], plan.SerialNo)
		} else if plan.Target == "心肺" {
			day[2] = append(day[2], plan.SerialNo)
		} else if plan.Target == "大腿" {
			day[3] = append(day[3], plan.SerialNo)
		}
	}
	for i := 0; i < len(day); i++ {
		if len(day[i]) == 0 {
			continue
		}
		b, err := json.Marshal(day[i])
		if err != nil {
			return business.NewError(errors.InternalError, err)
		}
		dbEntity := &sqldb.ExpExercisePlan{
			PatientID:    argument.PatientID,
			ExercisePlan: string(b),
		}
		_, err = s.sqlDatabase.Insert(dbEntity)
		if err != nil {
			return business.NewError(errors.InternalError, err)
		}
	}

	return nil
}

func (s *Data) GetPatientIntegralHistory(patientID uint64) ([]*doctor.ExpIntegral, business.Error) {
	results := make([]*doctor.ExpIntegral, 0)
	dbEntity := &sqldb.ExpIntegral{}
	dbFilter := &sqldb.ExpIntegralPatientFilter{
		PatientID: patientID,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	err := s.sqlDatabase.SelectList(dbEntity, func() {
		result := &doctor.ExpIntegral{}
		dbEntity.CopyTo(result)
		results = append(results, result)
	}, nil, sqlFilter)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}

	return results, nil
}

func (s *Data) GetPatientIntegralAll() ([]*doctor.ViewTotalPoint, business.Error) {
	results := make([]*doctor.ViewTotalPoint, 0)
	dbEntity := &sqldb.ViewTotalPoint{}
	dbOrder := &sqldb.ViewTotalPointOrder{}

	err := s.sqlDatabase.SelectList(dbEntity, func() {
		result := &doctor.ViewTotalPoint{}
		dbEntity.CopyTo(result)
		results = append(results, result)
	}, dbOrder)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}

	return results, nil
}

func (s *Data) GetSettingDietitian(patientID uint64) (*doctor.ManagedPatientIndex, business.Error) {
	result := &doctor.ManagedPatientIndex{}
	dbFilter := &sqldb.ManagedPatientIndexPatientIDFilter{
		PatientID: patientID,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	dbEntity := &sqldb.ManagedPatientIndex{}
	err := s.sqlDatabase.SelectOne(dbEntity, sqlFilter)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}
	dbEntity.CopyTo(result)
	return result, nil
}

func (s *Data) GetChangedDietPlan(patientID uint64) ([]*doctor.ExpPatientDietPlan, business.Error) {
	results := make([]*doctor.ExpPatientDietPlan, 0)
	dbDietPlanEntity := &sqldb.ExpPatientDietPlan{}
	dbDietPlanFilter := &sqldb.ExpPatientDietPlanChangeFilter{
		PatientID:    patientID,
		ChangeStatus: 1,
	}
	sqlDietPlanFilter := s.sqlDatabase.NewFilter(dbDietPlanFilter, false, false)
	err := s.sqlDatabase.SelectList(dbDietPlanEntity, func() {
		result := &doctor.ExpPatientDietPlan{}
		dbDietPlanEntity.CopyTo(result)
		results = append(results, result)
	}, nil, sqlDietPlanFilter)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}

	return results, nil
}

func (s *Data) CommitPatientReply(argument *doctor.ExpDoctorCommentToPatientReply) business.Error {
	dbEntity := &sqldb.ExpDoctorCommentToPatientReply{}
	dbEntity.CopyFromReply(argument)
	_, err := s.sqlDatabase.UpdateSelectiveByPrimaryKey(dbEntity)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}

	//将用户回复记录同步到doctorPatientChatMsg
	sqlAccess, err := s.sqlDatabase.NewAccess(true)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}
	defer sqlAccess.Close()

	replyEntity := &sqldb.ExpDoctorCommentToPatient{}
	replyFilter := &sqldb.ExpDoctorCommentToPatientFilter{
		SerialNo: argument.SerialNo,
	}
	sqlReplyFilter := s.sqlDatabase.NewFilter(replyFilter, false, false)
	err = sqlAccess.SelectOne(replyEntity, sqlReplyFilter)
	if err != nil {
		if s.sqlDatabase.IsNoRows(err) {
			return business.NewError(errors.NotExist, err)
		} else {
			return business.NewError(errors.InternalError, err)
		}
	}

	recordEntity := &sqldb.SportDietRecord{}
	recordFilter := &sqldb.SportDietRecordFilter{
		SerialNo: &replyEntity.SportDietRecord,
	}
	sqlRecordFilter := s.sqlDatabase.NewFilter(recordFilter, false, false)
	err = sqlAccess.SelectOne(recordEntity, sqlRecordFilter)
	if err != nil {
		if s.sqlDatabase.IsNoRows(err) {
			return business.NewError(errors.NotExist, err)
		} else {
			return business.NewError(errors.InternalError, err)
		}
	}

	indexEntity := &sqldb.ManagedPatientIndex{}
	indexFilter := &sqldb.ManagedPatientIndexPatientIDFilter{
		PatientID: replyEntity.PatientID,
	}
	sqlIndexFilter := s.sqlDatabase.NewFilter(indexFilter, false, false)
	err = sqlAccess.SelectOne(indexEntity, sqlIndexFilter)
	if err != nil {
		if s.sqlDatabase.IsNoRows(err) {
			return business.NewError(errors.NotExist, err)
		} else {
			return business.NewError(errors.InternalError, err)
		}
	}

	SenderID := replyEntity.PatientID
	ReceiverID := indexEntity.HealthManagerID
	timePoint := *recordEntity.TimePoint
	patientReply := *replyEntity.PatientReply
	MsgContent := "在" + recordEntity.HappenDateTime.Format("2006-01-02") + "的" + timePoint + "餐打卡中您的点评为：\n" + replyEntity.Comment + "\n对此用户回复为：\n" + patientReply + "\n请您尽快回复用户的疑问。"
	now := types.Time(time.Now())
	chatEntity := &doctor.DoctorPatientChatMsg{
		SenderID:    SenderID,
		ReceiverID:  *ReceiverID,
		MsgContent:  MsgContent,
		MsgDateTime: &now,
	}
	s.SendChatMessage(chatEntity)

	err = sqlAccess.Commit()
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}
	return nil
}

func (s *Data) GetPatientClock(argument *doctor.ExpPatientTaskRecordGetListInput) ([][]time.Time, business.Error) {
	results := make([]*doctor.ExpPatientTaskRecord, 0)
	dbEntity := &sqldb.ExpPatientTaskRecord{}
	dbFilter := &sqldb.ExpPatientTaskRecordGetByPatientID{
		PatientID: argument.PatientID,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	err := s.sqlDatabase.SelectList(dbEntity, func() {
		result := &doctor.ExpPatientTaskRecord{}
		dbEntity.CopyTo(result)
		results = append(results, result)
	}, nil, sqlFilter)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}

	//找出哪几天是完成了三餐打卡和体重打卡的
	Days := make([][]time.Time, 0)
	if len(results) == 0 {
		return Days, nil
	}
	oneDay := time.Time(*results[0].CreateDateTime)
	oneDayStart := time.Date(oneDay.Year(), oneDay.Month(), oneDay.Day(), 0, 0, 0, 01, time.Local)
	oneDayEnd := time.Date(oneDay.Year(), oneDay.Month(), oneDay.Day(), 23, 59, 59, 59, time.Local)
	clockDays := make([]time.Time, 0)
	halfDays := make([]time.Time, 0)
	breakfirst := false
	lunch := false
	dinner := false
	weight := false

	for _, result := range results {
		createDateTime := time.Time(*result.CreateDateTime)
		if (createDateTime.After(oneDayStart) && createDateTime.Before(oneDayEnd)) || createDateTime.Equal(oneDayStart) || createDateTime.Equal(oneDayEnd) {
			taskName := *result.ParentTaskName
			if taskName == "早餐" {
				breakfirst = true
			} else if taskName == "中餐" {
				lunch = true
			} else if taskName == "晚餐" {
				dinner = true
			} else if taskName == "晨起称重" {
				weight = true
			}
		} else {
			if breakfirst && lunch && dinner && weight {
				clockDays = append(clockDays, oneDayStart)
			} else if breakfirst || lunch || dinner || weight {
				halfDays = append(halfDays, oneDayStart)
			}
			breakfirst = false
			lunch = false
			dinner = false
			weight = false
			oneDay = createDateTime
			oneDayStart = time.Date(oneDay.Year(), oneDay.Month(), oneDay.Day(), 0, 0, 0, 01, time.Local)
			oneDayEnd = time.Date(oneDay.Year(), oneDay.Month(), oneDay.Day(), 23, 59, 59, 59, time.Local)
			taskName := *result.ParentTaskName
			if taskName == "早餐" {
				breakfirst = true
			} else if taskName == "中餐" {
				lunch = true
			} else if taskName == "晚餐" {
				dinner = true
			} else if taskName == "晨起称重" {
				weight = true
			}
		}
	}
	if breakfirst && lunch && dinner && weight {
		clockDays = append(clockDays, oneDayStart)
	} else if breakfirst || lunch || dinner || weight {
		halfDays = append(halfDays, oneDayStart)
	}
	Days = append(Days, clockDays)
	Days = append(Days, halfDays)

	return Days, nil

}

func (s *Data) CommitJudge(argument *doctor.PatientJudgeDoctorApp) business.Error {
	dbEntity := &sqldb.PatientJudgeDoctor{}
	dbEntity.CopyFromApp(argument)
	_, err := s.sqlDatabase.Insert(dbEntity)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}
	return nil
}

func (s *Data) CommitBug(argument *doctor.UserBugFeedbackApp) business.Error {
	dbEntity := &sqldb.UserBugFeedback{}
	dbEntity.CopyFromApp(argument)
	_, err := s.sqlDatabase.Insert(dbEntity)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}
	return nil
}

func (s *Data) GetPatientInfo(argument *doctor.PatientInfoBase) (*doctor.PatientUserBaseInfo, business.Error) {
	dbEntity := &sqldb.PatientUserBaseInfo{}
	dbFilter := &sqldb.PatientUserBaseInfoUserIdFilter{
		UserID: argument.PatientID,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)

	err := s.sqlDatabase.SelectOne(dbEntity, sqlFilter)

	if err != nil {
		if s.sqlDatabase.IsNoRows(err) {
			return nil, business.NewError(errors.NotExist, err)
		} else {
			return nil, business.NewError(errors.InternalError, err)
		}
	}

	result := &doctor.PatientUserBaseInfo{}
	dbEntity.CopyTo(result)

	return result, nil

}

func (s *Data) UpdatePatientInfo(argument *doctor.PatientUserInfoUpdate) business.Error {
	dbEntity := &sqldb.PatientUserBaseInfoUpdate{}
	dbEntity.CopyFromUpdate(argument)
	dbFilter := &sqldb.PatientUserBaseInfoUserIdFilter{
		UserID: argument.PatientID,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	_, err := s.sqlDatabase.Update(dbEntity, sqlFilter)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}
	return nil
}

func (s *Data) UpdateUseTime(argument *doctor.PatientUserAuthsUpdateTime) business.Error {
	dbEntity1 := &sqldb.PatientUserAuthsUpdateTime{}
	dbEntity2 := &sqldb.PatientUserAuthsUpdateTime{}
	dbEntity1.CopyFromUpdate(argument)
	dbFilter := &sqldb.PatientUserBaseInfoUserIdFilter{
		UserID: argument.UserID,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	err := s.sqlDatabase.SelectOne(dbEntity2, sqlFilter)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}
	dbEntity1.UseSeconds = dbEntity1.UseSeconds + dbEntity2.UseSeconds
	_, err = s.sqlDatabase.Update(dbEntity1, sqlFilter)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}
	return nil
}

//健康教育相关的，去实现Data接口
func (s *Data) GetTagInfo() ([]*doctor.Tag, business.Error) {
	results := make([]*doctor.Tag, 0)
	dbEntity := &sqldb.TagInfo{} //等一下去sqldb下面去定义TagInfo结构
	err := s.sqlDatabase.SelectList(dbEntity, func() {
		result := &doctor.Tag{}
		dbEntity.CopyTo(result)
		results = append(results, result)
	}, nil)
	if err != nil {
		return results, business.NewError(errors.InternalError, err)
	}
	return results, nil
}

func (s *Data) GetPatientTag(argument *doctor.PatientTagInfo) ([]*doctor.PatientTag, business.Error) {
	results := make([]*doctor.PatientTag, 0)
	dbEntity := &sqldb.PatientToTag{}
	dbFilter := &sqldb.PatientToTagUserIdFilter{
		UserID: argument.PatientID,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	err := s.sqlDatabase.SelectList(dbEntity, func() {
		result := &doctor.PatientTag{}
		dbEntity.CopyToPatientTag(result)
		results = append(results, result)
	}, nil, sqlFilter)
	if err != nil {
		return results, business.NewError(errors.InternalError, err)
	}
	return results, nil
}

func (s *Data) DeletePatientTag(argument *doctor.PatientToTagDelete) (uint64, business.Error) {
	dbEntity := &sqldb.PatientToTag{}
	dbFilter := &sqldb.PatientToTagDeleteFilter{}
	dbFilter.CopyFrom(argument)
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	result, err := s.sqlDatabase.Delete(dbEntity, sqlFilter)
	if err != nil {
		return 0, business.NewError(errors.InternalError, err)
	}
	return result, nil
}

func (s *Data) AddPatientTag(argument *doctor.PatientToTagAdd) (uint64, business.Error) {
	dbEntity := &sqldb.PatientToTag{}
	dbEntity.CopyFromApp(argument)
	data, err := s.sqlDatabase.Insert(dbEntity)
	if err != nil {
		return 0, business.NewError(errors.InternalError, err)
	}
	return data, nil
}

func (s *Data) GetKnowledgeList(argument *doctor.SelectedTags) ([]*doctor.KnowledgeInfo, business.Error) {
	//先依据传进来的参数，查找到没有重复的knowledge_id
	dbKnowledgeToTagEntity := &sqldb.KnowledgeToTag{} //去entity中写这个结构
	knowledgeIdList := make([]uint64, 0)
	for i := 0; i < len(argument.SelectedTagList); i++ {
		dbKnowledgeToTagTagIdFilter := &sqldb.KnowledgeToTagTagIdFilter{
			Tag_id: argument.SelectedTagList[i],
		}
		sqlKnowledgeToTagTagIdFilter := s.sqlDatabase.NewFilter(dbKnowledgeToTagTagIdFilter, false, false)
		err := s.sqlDatabase.SelectList(dbKnowledgeToTagEntity, func() {
			knowledgeId := dbKnowledgeToTagEntity.Knowledge_id
			knowledgeIdList = append(knowledgeIdList, knowledgeId)
		}, nil, sqlKnowledgeToTagTagIdFilter)
		if err != nil {
			return nil, business.NewError(errors.InternalError, err)
		}
	}
	//调用去重函数对knowledgeIdList进行去重->不知道这个去重函数写在哪
	knowledgeIdList = removeRepByMap(knowledgeIdList)
	//依据knowledgeIdList来找知识
	results := make([]*doctor.KnowledgeInfo, 0)
	dbKnowledgeInfoEntity := &sqldb.KnowledgeInfo{}
	for i := 0; i < len(knowledgeIdList); i++ {
		dbKnowledgeInfoFilter := &sqldb.KnowledgeInfoFilterById{
			Id: knowledgeIdList[i],
		}
		sqlKnowledgeInfoFilter := s.sqlDatabase.NewFilter(dbKnowledgeInfoFilter, false, false)
		err := s.sqlDatabase.SelectOne(dbKnowledgeInfoEntity, sqlKnowledgeInfoFilter)
		if err != nil {
			return nil, business.NewError(errors.InternalError, err)
		}
		result := &doctor.KnowledgeInfo{}
		dbKnowledgeInfoEntity.CopyTo(result)
		results = append(results, result)
	}
	return results, nil
	/*
		dbKnowledgeToTagFilterList := make([]*sqldb.KnowledgeToTagTagIdFilter,0)
		for i := 0; i < len(argument.SelectedTagList); i++ {
			dbKnowledgeToTagFilter := &sqldb.KnowledgeToTagTagIdFilter{}
			dbKnowledgeToTagFilter.Tag_id = argument.SelectedTagList[i]
			dbKnowledgeToTagFilterList = append(dbKnowledgeToTagFilterList,dbKnowledgeToTagFilter)
		}
		sqlKnowledgeToTagFilterList := make([]database.SqlFilter,0)
		for i := 0; i < len(dbKnowledgeToTagFilterList); i++ {
			sqlKnowledgeToTagFilter := s.sqlDatabase.NewFilter(dbKnowledgeToTagFilterList[i],false,false)
			sqlKnowledgeToTagFilterList = append(sqlKnowledgeToTagFilterList,sqlKnowledgeToTagFilter)
		}
		//成功拿到了有关于tag->knowledge_id的过滤器列表
		err := s.sqlDatabase.SelectDistinct(dbKnowledgeToTagEntity, func() {

		},nil,sqlKnowledgeToTagFilterList)

		测试了一下，原来go语言与java不一样，不能在函数参数...的地方直接传入一个切片
		那么只能改写一下写法了~

	*/

}

//用于切片元素去重
func removeRepByMap(slc []uint64) []uint64 {
	result := []uint64{}
	tempMap := map[uint64]int{}
	for _, element := range slc {
		l := len(tempMap)
		tempMap[element] = 0
		if len(tempMap) != l {
			result = append(result, element)
		}
	}
	return result
}

func (s *Data) AddKnowledgeLoveNum(argument *doctor.KnowledgeInfoId) (uint64, business.Error) {
	//先依据knowledge_id查到原先有
	dbQueryEntity := &sqldb.KnowledgeInfo{}
	dbQueryFilter := &sqldb.KnowledgeInfoFilterById{
		Id: argument.Id,
	}
	sqlQueryFilter := s.sqlDatabase.NewFilter(dbQueryFilter, false, false)
	err := s.sqlDatabase.SelectOne(dbQueryEntity, sqlQueryFilter)
	if err != nil {
		return 0, business.NewError(errors.InternalError, err)
	}
	originalLoveNum := dbQueryEntity.LoveNum
	dbUpdateEntity := &sqldb.KnowledgeInfoAddLoveNum{
		LoveNum: originalLoveNum + 1,
	}
	//更新用的过滤器和查询用的过滤器是一致的
	data, err := s.sqlDatabase.Update(dbUpdateEntity, sqlQueryFilter)
	if err != nil {
		return 0, business.NewError(errors.InternalError, err)
	}
	return data, nil
}

func (s *Data) GetKnowledgeIdList(argument *doctor.PatientCollectedKnowledgePatientId) ([]*doctor.PatientCollectedKnowledgeKnowledgeId, business.Error) {
	results := make([]*doctor.PatientCollectedKnowledgeKnowledgeId, 0)
	dbEntity := &sqldb.PatientCollectedKnowledge{}
	dbFilter := &sqldb.PatientCollectedKnowledgeFilterByUserID{
		UserID: argument.PatientID,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	err := s.sqlDatabase.SelectList(dbEntity, func() {
		result := &doctor.PatientCollectedKnowledgeKnowledgeId{
			Collected_knowledge_id: dbEntity.Collected_knowledge_id,
		}
		results = append(results, result)
	}, nil, sqlFilter)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}
	return results, nil
}

func (s *Data) GetTlcKnowledgeById(argument *doctor.KnowledgeInfoId) (*doctor.KnowledgeInfo, business.Error) {
	dbEntity := &sqldb.KnowledgeInfo{}
	dbFilter := &sqldb.KnowledgeInfoFilterById{
		Id: argument.Id,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	err := s.sqlDatabase.SelectOne(dbEntity, sqlFilter)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}
	result := &doctor.KnowledgeInfo{}
	dbEntity.CopyTo(result)
	return result, nil
}

func (s *Data) AddCollectedKnowledge(argument *doctor.PatientCollectedKnowledgeAdd) (uint64, business.Error) {
	dbEntity := &sqldb.PatientCollectedKnowledge{}
	dbEntity.CopyFromApp(argument)
	data, err := s.sqlDatabase.Insert(dbEntity)
	if err != nil {
		return 0, business.NewError(errors.InternalError, err)
	}
	return data, nil
}

func (s *Data) DeleteCollectedKnowledge(argument *doctor.PatientCollectedKnowledgeDelete) (uint64, business.Error) {
	dbEntity := &sqldb.PatientCollectedKnowledge{}
	dbFilter := &sqldb.PatientCollectedKnowledgeDeleteFilter{}
	dbFilter.CopyFrom(argument)
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	data, err := s.sqlDatabase.Delete(dbEntity, sqlFilter)
	if err != nil {
		return 0, business.NewError(errors.InternalError, err)
	}
	return data, nil
}

func (s *Data) GetCollectedKnowledgeList(argument *doctor.PatientCollectedKnowledgeIdList) ([]*doctor.KnowledgeInfo, business.Error) {
	results := make([]*doctor.KnowledgeInfo, 0)
	dbEntity := &sqldb.KnowledgeInfo{}
	for i := 0; i < len(argument.CollectedKnowledgeIdList); i++ {
		dbFilter := &sqldb.KnowledgeInfoFilterById{
			Id: argument.CollectedKnowledgeIdList[i],
		}
		sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
		err := s.sqlDatabase.SelectOne(dbEntity, sqlFilter)
		if err != nil {
			return nil, business.NewError(errors.InternalError, err)
		}
		result := &doctor.KnowledgeInfo{}
		dbEntity.CopyTo(result)
		results = append(results, result)
	}
	return results, nil
}

func (s *Data) GiveDateToGetRcKnw(argument *doctor.KnowledgeInfoDateString) (uint64, business.Error) {
	//将前端传入的日期先转换成dayindex
	timeModelStr := "2006-01-02" //用于转换的时候的模板

	//开始时间改为数据库设置控制2022/10/23
	dbScheduleEntity := &sqldb.TlcKnwSchedule{}
	dbScheduleFilter := &sqldb.TlcKnwScheduleFilterBySno{SerialNo: 1}
	sqlScheduleFilter := s.sqlDatabase.NewFilter(dbScheduleFilter, false, false)
	s.sqlDatabase.SelectOne(dbScheduleEntity, sqlScheduleFilter)

	startTimeSrt := (*dbScheduleEntity.StartTime).Format(timeModelStr) //startTime 是训练营开始的时间
	startTime, _ := time.ParseInLocation(timeModelStr, startTimeSrt, time.Local)
	nowTime, _ := time.ParseInLocation(timeModelStr, argument.DateString, time.Local)
	dayIndex := (uint64(nowTime.Sub(startTime).Hours()) / 24) % 21 //得到了用于查询的dayIndex
	//2022/04/13修复bug....
	//判断是否已经在数据库里面了...
	for day := uint64(0); day <= dayIndex; day++ {
		/**
		判断每天是否有推送过，这样的推送方式对知识库改动之后的老用户不友好
		老用户收不到更新的知识库版本的知识
		*/
		//dbTlcPaToKnEntity := &sqldb.TlcPatientToRecKnowledge{}
		//dbTlcPaToKnDayIndexFilter := &sqldb.TlcPatientToRecKnowledgeFilterByDayIndex{
		//	Dayindex: day,
		//	UserID:   argument.PatientID,
		//}
		//sqlTlcPaToKnDayIndexFilter := s.sqlDatabase.NewFilter(dbTlcPaToKnDayIndexFilter, false, false)
		//err1 := s.sqlDatabase.SelectOne(dbTlcPaToKnEntity, sqlTlcPaToKnDayIndexFilter)
		//if err1 != nil {
		//	if s.sqlDatabase.IsNoRows(err1) {
		//		//在tlcheedurecknw这张表里去写
		//		//依据dayindex先从知识库里把东西查过来
		//		//knowledgesSelectedByDayIndex := make([]*sqldb.KnowledgeInfo,0)
		//		dbKnowledgeInfoEntity := &sqldb.KnowledgeInfo{}
		//		dbKnowledgeInfoFilter := &sqldb.KnowledgeInfoFilterByDayIndex{
		//			Dayindex: day,
		//		}
		//		sqlKnowledgeInfoFilter := s.sqlDatabase.NewFilter(dbKnowledgeInfoFilter, false, false)
		//		err2 := s.sqlDatabase.SelectList(dbKnowledgeInfoEntity, func() {
		//			dbTlcPaToKnInsertEntity := &sqldb.TlcPatientToRecKnowledge{
		//				UserID:       argument.PatientID,
		//				Knowledge_id: dbKnowledgeInfoEntity.Id,
		//				Dayindex:     dbKnowledgeInfoEntity.Dayindex,
		//			}
		//			s.sqlDatabase.Insert(dbTlcPaToKnInsertEntity)
		//		}, nil, sqlKnowledgeInfoFilter)
		//		if err2 != nil {
		//			return 0, business.NewError(errors.InternalError, err2)
		//		}
		//	} else {
		//		return 0, business.NewError(errors.InternalError, err1)
		//	}
		//} else {
		//	continue
		//}

		/**
		更新推送机制的版本
		1.先依据天数拿到知识
		2.采用逐条过滤排查机制进行插入，用于解决老用户收不到更新后知识库的问题
		更新时间:2022/08/15
		*/
		dbKnowledgeEntity := &sqldb.KnowledgeInfo{}
		dbKnowledgeFilterByDayIndex := &sqldb.KnowledgeInfoFilterByDayIndex{Dayindex: day}
		sqlKnowledgeFilterByDayIndex := s.sqlDatabase.NewFilter(dbKnowledgeFilterByDayIndex, false, false)
		err1 := s.sqlDatabase.SelectList(dbKnowledgeEntity, func() {
			//逐条过滤机制
			dbTlcPaToKnwInsertEntity := &sqldb.TlcPatientToRecKnowledge{
				UserID:       argument.PatientID,
				Knowledge_id: dbKnowledgeEntity.Id,
				Dayindex:     dbKnowledgeEntity.Dayindex,
			}
			dbTlcPaToKnwEntity := &sqldb.TlcPatientToRecKnowledge{}
			dbTlcPaToKnwFilter := &sqldb.TlcPatientToRecKnowledgeFilterByUserAndKnwID{
				UserID:       argument.PatientID,
				Knowledge_id: dbKnowledgeEntity.Id,
			}
			sqlTlcPaToKnwFilter := s.sqlDatabase.NewFilter(dbTlcPaToKnwFilter, false, false)
			err2 := s.sqlDatabase.SelectOne(dbTlcPaToKnwEntity, sqlTlcPaToKnwFilter)
			if err2 != nil {
				if s.sqlDatabase.IsNoRows(err2) {
					s.sqlDatabase.Insert(dbTlcPaToKnwInsertEntity)
				}
			} else {
				//查到已经存在->什么都不执行
			}
		}, nil, sqlKnowledgeFilterByDayIndex)
		if err1 != nil {
			return 0, business.NewError(errors.InternalError, err1)
		}
	}
	return 1, nil
}

//Tlc获取今日知识列表
func (s *Data) TlcGetTodayKnowledge(argument *doctor.KnowledgeInfoDateString) ([]*doctor.KnowledgeInfoIncludingComments, business.Error) {
	timeModelStr := "2006-01-02" //用于转换的时候的模板
	//开始时间改为数据库设置控制2022/10/23
	dbScheduleEntity := &sqldb.TlcKnwSchedule{}
	dbScheduleFilter := &sqldb.TlcKnwScheduleFilterBySno{SerialNo: 1}
	sqlScheduleFilter := s.sqlDatabase.NewFilter(dbScheduleFilter, false, false)
	s.sqlDatabase.SelectOne(dbScheduleEntity, sqlScheduleFilter)

	startTimeSrt := (*dbScheduleEntity.StartTime).Format(timeModelStr) //startTime 是训练营开始的时间
	startTime, _ := time.ParseInLocation(timeModelStr, startTimeSrt, time.Local)
	nowTime, _ := time.ParseInLocation(timeModelStr, argument.DateString, time.Local)
	dayIndex := (uint64(nowTime.Sub(startTime).Hours()) / 24) % 21 //得到了用于查询的dayIndex
	//先依据Dayindex、UserID、Status在tlcrecknw表里查出KnowledgeIdList
	knowledgeIdList := make([]uint64, 0)
	dbTlcPaToKnwEntity := &sqldb.TlcPatientToRecKnowledge{}
	dbTlcPaToKnwFilter := &sqldb.TlcPatientToRecKnowledgeTodayFilter{
		UserID:   argument.PatientID,
		Dayindex: dayIndex,
		Status:   0, //显示出来的是没读过的每日知识
	}
	sqlTlcPaToKnwFilter := s.sqlDatabase.NewFilter(dbTlcPaToKnwFilter, false, false)
	err1 := s.sqlDatabase.SelectList(dbTlcPaToKnwEntity, func() {
		knowledgeIdList = append(knowledgeIdList, dbTlcPaToKnwEntity.Knowledge_id)
	}, nil, sqlTlcPaToKnwFilter)
	if err1 != nil {
		return make([]*doctor.KnowledgeInfoIncludingComments, 0), business.NewError(errors.InternalError, err1)
	}
	results := make([]*doctor.KnowledgeInfo, 0)
	dbKnowledgeInfoEntity := &sqldb.KnowledgeInfo{}
	for i := 0; i < len(knowledgeIdList); i++ {
		//依据knowledgeId查找知识填入返回的results列表
		dbKnowledgeInfoFilter := &sqldb.KnowledgeInfoFilterById{Id: knowledgeIdList[i]}
		sqlKnowledgeInfoFilter := s.sqlDatabase.NewFilter(dbKnowledgeInfoFilter, false, false)
		err2 := s.sqlDatabase.SelectOne(dbKnowledgeInfoEntity, sqlKnowledgeInfoFilter)
		if err2 != nil {
			return nil, business.NewError(errors.InternalError, err2)
		}
		result := &doctor.KnowledgeInfo{}
		dbKnowledgeInfoEntity.CopyTo(result)
		results = append(results, result)
	}
	//Get Complete KnowledgeInfo including comments information
	finalResults := make([]*doctor.KnowledgeInfoIncludingComments, 0)
	for i := 0; i < len(results); i++ {
		finalResult := &doctor.KnowledgeInfoIncludingComments{}
		finalResult.GetAttributeFromKnwInfo(results[i])
		toGetFinalResultComments := &doctor.TlcGetKnwComments{Knowledge_id: results[i].Id}
		finalResult.CommentsList, _ = s.TlcGetKnwCommentsInfo(toGetFinalResultComments)
		finalResults = append(finalResults, finalResult)
	}
	return finalResults, nil
}

//Tlc获取用户对知识的评分
func (s *Data) TlcGetKnowledgeStar(argument *doctor.TlcPatientToRecKnowledgeGetStar) (uint64, business.Error) {
	dbEntity := &sqldb.TlcPatientToRecKnowledge{}
	dbFilter := &sqldb.TlcPatientToRecKnowledgeGetStarFilter{
		UserID:       argument.PatientID,
		Knowledge_id: argument.Knowledge_id,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	err := s.sqlDatabase.SelectOne(dbEntity, sqlFilter)
	if err != nil {
		return 0, business.NewError(errors.InternalError, err)
	}
	return dbEntity.Stars, nil
}

//Tlc更新用户对知识的评分
func (s *Data) TlcUpdateKnowledgeStar(argument *doctor.TlcPatientToRecKnowledgeUpdateStar) (uint64, business.Error) {
	dbEntity := &sqldb.TlcPatientToRecKnowledgeUpdateStar{
		Stars: argument.Stars,
	}
	dbFilter := &sqldb.TlcPatientToRecKnowledgeUpdateStarFilter{
		UserID:       argument.PatientID,
		Knowledge_id: argument.Knowledge_id,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	data, err := s.sqlDatabase.Update(dbEntity, sqlFilter)
	if err != nil {
		return 0, business.NewError(errors.InternalError, err)
	}
	return data, nil
}

func (s *Data) TlcChangeKnowledgeStatus(argument *doctor.TlcPatientToRecKnowledgeChangeStatus) (uint64, business.Error) {
	dbEntity := &sqldb.TlcPatientToRecKnowledgeChangeStatus{
		Status: 1,
	}
	dbFilter := &sqldb.TlcPatientToRecKnowledgeChangeStatusFilter{
		UserID:       argument.PatientID,
		Knowledge_id: argument.Knowledge_id,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	data, err := s.sqlDatabase.Update(dbEntity, sqlFilter)
	if err != nil {
		return 0, business.NewError(errors.InternalError, err)
	}
	//统计知识的已读次数
	countReadNum := uint64(0)
	dbQueryKnwStatusEntity := &sqldb.TlcPatientToRecKnowledge{}
	dbQueryKnwStatusFilter := &sqldb.TlcPatientToRecKnowledgeKnoStatusStatisticFilter{
		Knowledge_id: argument.Knowledge_id,
		Status:       1,
	}
	sqlQueryKnwStatusFilter := s.sqlDatabase.NewFilter(dbQueryKnwStatusFilter, false, false)
	err1 := s.sqlDatabase.SelectList(dbQueryKnwStatusEntity, func() {
		countReadNum++
	}, nil, sqlQueryKnwStatusFilter)
	if err1 != nil {
		return 0, business.NewError(errors.InternalError, err)
	}
	//完成统计，前去修改知识信息表
	dbWriteKnwEntity := &sqldb.KnowledgeInfoUpdateReadAlreadyNum{
		ReadAlreadyNum: countReadNum,
	}
	dbWriteKnwFilter := &sqldb.KnowledgeInfoFilterById{Id: argument.Knowledge_id}
	sqlWriteKnwFilter := s.sqlDatabase.NewFilter(dbWriteKnwFilter, false, false)
	_, err2 := s.sqlDatabase.Update(dbWriteKnwEntity, sqlWriteKnwFilter)
	if err2 != nil {
		return 0, business.NewError(errors.InternalError, err2)
	}
	//返回的是修改的rec表的行数
	return data, nil
}

func (s *Data) TlcGetHistoryKnowledge(argument *doctor.TlcPatientToRecKnowledgePatientID) ([]*doctor.KnowledgeInfoIncludingComments, business.Error) {
	dbQueryEntity := &sqldb.TlcPatientToRecKnowledge{}
	dbQueryFilter := &sqldb.TlcPatientToRecKnowledgeFilterByPatientID{
		UserID: argument.PatientID,
	}
	sqlQueryFilter := s.sqlDatabase.NewFilter(dbQueryFilter, false, false)
	dbQueryOrder := &sqldb.TlcPatientToRecKnowledgeOrderByDayIndex{}
	results := make([]*doctor.KnowledgeInfo, 0)
	err1 := s.sqlDatabase.SelectList(dbQueryEntity, func() {
		dbEntity := &sqldb.KnowledgeInfo{}
		dbFilter := &sqldb.KnowledgeInfoFilterById{
			Id: dbQueryEntity.Knowledge_id,
		}
		sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
		s.sqlDatabase.SelectOne(dbEntity, sqlFilter)
		result := &doctor.KnowledgeInfo{}
		dbEntity.CopyTo(result)
		results = append(results, result)
	}, dbQueryOrder, sqlQueryFilter)
	if err1 != nil {
		return nil, business.NewError(errors.InternalError, err1)
	}
	//Get Complete KnowledgeInfo including comments information
	finalResults := make([]*doctor.KnowledgeInfoIncludingComments, 0)
	for i := 0; i < len(results); i++ {
		finalResult := &doctor.KnowledgeInfoIncludingComments{}
		finalResult.GetAttributeFromKnwInfo(results[i])
		toGetFinalResultComments := &doctor.TlcGetKnwComments{Knowledge_id: results[i].Id}
		finalResult.CommentsList, _ = s.TlcGetKnwCommentsInfo(toGetFinalResultComments)
		finalResults = append(finalResults, finalResult)
	}
	return finalResults, nil
}

//评论功能
func (s *Data) TlcSendKnwComment(argument *doctor.TlcPatientSendKnwComment) (uint64, business.Error) {
	dbEntity := &sqldb.PatientCommentKnw{}
	dbEntity.CopyFrom(argument)
	data, err := s.sqlDatabase.Insert(dbEntity)
	if err != nil {
		return 0, business.NewError(errors.InternalError, err)
	}
	return data, nil
}

func (s *Data) TlcGetKnwCommentsInfo(argument *doctor.TlcGetKnwComments) ([]*doctor.TlcGetKnwCommentsInfo, business.Error) {
	dbEntity := &sqldb.TlcHeEduKnwComment{}
	dbFilter := &sqldb.TlcHeEduKnwCommentFilterByKnwID{
		Knowledge_id: argument.Knowledge_id,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	results := make([]*doctor.TlcGetKnwCommentsInfo, 0)
	err := s.sqlDatabase.SelectList(dbEntity, func() {
		result := &doctor.TlcGetKnwCommentsInfo{
			Id:           dbEntity.Id,
			Knowledge_id: dbEntity.Knowledge_id,
			PatientID:    dbEntity.UserID,
			Content:      dbEntity.Content,
		}
		//query userName
		dbQueryNameEntity := &sqldb.PatientUserBaseInfo{}
		dbQueryNameFilter := &sqldb.PatientUserBaseInfoUserIdFilter{
			UserID: dbEntity.UserID,
		}
		sqlQueryNameFilter := s.sqlDatabase.NewFilter(dbQueryNameFilter, false, false)
		s.sqlDatabase.SelectOne(dbQueryNameEntity, sqlQueryNameFilter)
		result.UserName = dbQueryNameEntity.Name
		//append result to results
		results = append(results, result)
	}, nil, sqlFilter)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}
	return results, nil
}

func (s *Data) TlcGetPatientName(argument *doctor.TlcPatientNameQuery) (*doctor.TlcPatientNameQueryResult, business.Error) {
	dbQueryNameEntity := &sqldb.PatientUserBaseInfo{}
	dbQueryNameFilter := &sqldb.PatientUserBaseInfoUserIdFilter{
		UserID: argument.PatientID,
	}
	sqlQueryNameFilter := s.sqlDatabase.NewFilter(dbQueryNameFilter, false, false)
	result := &doctor.TlcPatientNameQueryResult{}
	s.sqlDatabase.SelectOne(dbQueryNameEntity, sqlQueryNameFilter)
	result.Name = dbQueryNameEntity.Name
	return result, nil
}

func (s *Data) TlcPreRequestOfQuestion(argument *doctor.TlcHeEduPatientQuestionGetBindWithKnw) (bool, business.Error) {
	/**
	预请求的完成-现阶段的设计：
	查到已经有用户的问题卡片的记录，那么说明预请求完成
	======
	预请求第二阶段的设计:2022/08/10
	问题卡片与知识相互关联,通过用户获得的今天知识 查询得到用户收到的问题
	======
	以后的预请求设想：
	营养师以管理的方式或者标签群组的方式直接设置
	*/
	//查询所有的问题，得到问题列表
	dbQuestionEntity := &sqldb.TlcHeEduQuestion{}
	questionList := make([]*sqldb.TlcHeEduQuestion, 0)
	//知识id的过滤器 for in KnwList
	for i := 0; i < len(argument.KnwIdList); i++ {
		dbQuestionFilter := &sqldb.TlcHeEduQuestionFilterByKnwId{
			Knowledge_id: argument.KnwIdList[i],
		}
		sqlQuestionFilter := s.sqlDatabase.NewFilter(dbQuestionFilter, false, false)
		err1 := s.sqlDatabase.SelectList(dbQuestionEntity, func() {
			/**出现问题，问题在于dbQuestionEntity = 0x0011 -> Obj1
			questionList = [0x0011:Obj1]
			----
			Loop next index
			----
			dbQuestionEntity = 0x0011 -> Obj2
			questionList = [0x0011:Obj2, ox0011:Obj2]
			*/
			questionListItem := &sqldb.TlcHeEduQuestion{
				Id:           dbQuestionEntity.Id,
				Type:         dbQuestionEntity.Type,
				Question:     dbQuestionEntity.Question,
				Reply:        dbQuestionEntity.Reply,
				Explanation:  dbQuestionEntity.Explanation,
				Knowledge_id: dbQuestionEntity.Knowledge_id,
			}
			questionList = append(questionList, questionListItem)
		}, nil, sqlQuestionFilter) //之前写GetTagInfo的时候是直接不传值，这次传了nil进去，如果报错就解决这个问题
		if err1 != nil {
			return false, business.NewError(errors.InternalError, err1)
		}
	}

	//对问题列表进行循环,对每一个问题进行预请求的检查
	for i := 0; i < len(questionList); i++ {
		dbPreQueryEntity := &sqldb.TlcHeEduPatientQuestion{}
		dbPreQueryFilter := &sqldb.TlcHeEduPatientQuestionPreRequestFilter{
			Question_id: questionList[i].Id,
			UserID:      argument.PatientID,
		}
		sqlPreQueryFilter := s.sqlDatabase.NewFilter(dbPreQueryFilter, false, false)
		err2 := s.sqlDatabase.SelectOne(dbPreQueryEntity, sqlPreQueryFilter)
		if err2 != nil {
			if s.sqlDatabase.IsNoRows(err2) {
				//没查到用户获得了该问题信息
				dbPatientGetCertainQuestionEntity := &sqldb.TlcHeEduPatientGetCertainQuestion{
					Question_id:  questionList[i].Id,
					UserID:       argument.PatientID,
					Knowledge_id: questionList[i].Knowledge_id,
				}
				_, err3 := s.sqlDatabase.Insert(dbPatientGetCertainQuestionEntity)
				if err3 != nil {
					return false, business.NewError(errors.InternalError, err3)
				}
			} else {
				return false, business.NewError(errors.InternalError, err2)
			}
		} else {
			//已经查到用户获得该问题的信息
			continue
		}
	}
	//dbPreQueryEntity := &sqldb.TlcHeEduPatientQuestion{}
	//dbPreQueryFilter := &sqldb.TlcHeEduPatientQuestionFilterByPatientID{
	//	UserID: argument.PatientID,
	//}
	//sqlPreQueryFilter := s.sqlDatabase.NewFilter(dbPreQueryFilter, false, false)
	//err1 := s.sqlDatabase.SelectOne(dbPreQueryEntity,sqlPreQueryFilter)
	//if err1 != nil {
	//	if s.sqlDatabase.IsNoRows(err1) {
	//		//没有进行过预加载
	//
	//	}else {
	//		return false,business.NewError(errors.InternalError,err1)
	//	}
	//}else{
	//	return true,nil
	//}
	return true, nil
}

func (s *Data) TlcGetIntegratedQuestionInfo(argument *doctor.TlcHeEduPatientQuestionGetBindWithKnw) ([]*doctor.TlcHeEduPatientIntegratedQuestionInfo, business.Error) {
	/**
	用户获取整合好的问题列表的思路
	1.依据用户的id -> 查询用户收到的问题列表（包括问题的id和status信息）
	===>新的版本<==
	1.依据用户的id，收到的今日知识的id查询用户收到的问题列表（包括问题的id和status信息）
	2.对查询得到的问题列表进行循环
	3.循环的内部：
		a）得到循环元素的id -> 查询问题表得到相关的type、question、reply、explanation信息
		b)依据得到的循环元素问题的id进入回答表进行查询得到针对于该问题的选项列表
		c)依据问题的id，查询用户回答问题的情况并整合入返回的信息中
	4. 返回与问题列表等长的整合好的问题列表
	*/
	receivedQuestions := make([]*sqldb.TlcHeEduPatientReceivedQuestion, 0)
	dbUserReceivedQuestionEntity := &sqldb.TlcHeEduPatientQuestion{}
	for i := 0; i < len(argument.KnwIdList); i++ {
		dbUserReceivedQuestionFilter := &sqldb.TlcHeEduPatientQuestionFilterByPatientAndKnwId{
			UserID:       argument.PatientID,
			Knowledge_id: argument.KnwIdList[i],
		}
		sqlUserReceivedQuestionFilter := s.sqlDatabase.NewFilter(dbUserReceivedQuestionFilter, false, false)
		err1 := s.sqlDatabase.SelectList(dbUserReceivedQuestionEntity, func() {
			receivedQuestion := &sqldb.TlcHeEduPatientReceivedQuestion{
				Question_id: dbUserReceivedQuestionEntity.Question_id,
				Status:      dbUserReceivedQuestionEntity.Status,
			}
			receivedQuestions = append(receivedQuestions, receivedQuestion)
		}, nil, sqlUserReceivedQuestionFilter)
		if err1 != nil {
			return make([]*doctor.TlcHeEduPatientIntegratedQuestionInfo, 0), business.NewError(errors.InternalError, err1)
		}
	}
	// step2
	integratedQuestions := make([]*doctor.TlcHeEduPatientIntegratedQuestionInfo, 0)
	for i := 0; i < len(receivedQuestions); i++ {
		//通过查询整合信息
		integratedQuestion := &doctor.TlcHeEduPatientIntegratedQuestionInfo{
			Id:             0,
			Type:           0,
			Question:       "",
			Options:        nil,
			Status:         0,
			Reply:          0,
			Explanation:    "",
			TotalAnswerNum: 0,
			RightAnswerNum: 0,
		}
		integratedQuestion.Id = receivedQuestions[i].Question_id
		integratedQuestion.Status = receivedQuestions[i].Status
		dbQuestionSelfEntity := &sqldb.TlcHeEduQuestion{}
		dbQuestionSelfFilter := &sqldb.TlcHeEduQuestionFilterByQuestionId{
			Id: integratedQuestion.Id,
		}
		sqlQuestionSelfFilter := s.sqlDatabase.NewFilter(dbQuestionSelfFilter, false, false)
		err2 := s.sqlDatabase.SelectOne(dbQuestionSelfEntity, sqlQuestionSelfFilter)
		if err2 != nil {
			return make([]*doctor.TlcHeEduPatientIntegratedQuestionInfo, 0), business.NewError(errors.InternalError, err2)
		}
		integratedQuestion.Type = dbQuestionSelfEntity.Type
		integratedQuestion.Question = dbQuestionSelfEntity.Question
		integratedQuestion.Reply = dbQuestionSelfEntity.Reply
		integratedQuestion.Explanation = dbQuestionSelfEntity.Explanation
		/**
		依据问题的id在答案编制表中查询得到options
		a)问题的类型如果是判断题options先赋值为nil,倘若报错，赋值为空的列表
		b)问题的类型如果是选择题则在tlcheeduanswer表中进行查询
		*/
		if integratedQuestion.Type == 2 {
			integratedQuestion.Options = nil
		} else if integratedQuestion.Type == 1 {
			dbQuestionBindAnswerEntity := &sqldb.TlcHeEduAnswer{}
			dbQuestionBindAnswerFilter := &sqldb.TlcHeEduAnswerFilterByQuestionId{
				Question_id: integratedQuestion.Id,
			}
			sqlQuestionBindAnswerFilter := s.sqlDatabase.NewFilter(dbQuestionBindAnswerFilter, false, false)
			//SelectList
			err3 := s.sqlDatabase.SelectList(dbQuestionBindAnswerEntity, func() {
				Option := &doctor.TlcHeEduQuestionOptionInfo{
					Id:      dbQuestionBindAnswerEntity.Id,
					Content: dbQuestionBindAnswerEntity.Content,
					Isop:    dbQuestionBindAnswerEntity.Isop,
				}
				integratedQuestion.Options = append(integratedQuestion.Options, Option)
			}, nil, sqlQuestionBindAnswerFilter)
			if err3 != nil {
				integratedQuestion.Options = nil
				continue
			}
		}
		//获取用户回答该问题的统计信息
		totalAnswerCount := 0
		rightAnswerCount := 0
		dbQuestionAnswerStatusStatisticEntity := &sqldb.TlcHeEduPatientQuestion{}
		dbQuestionAnswerStatusStatisticFilter := &sqldb.TlcHeEduPatientQuestionFilterByQuestionId{Question_id: receivedQuestions[i].Question_id}
		sqlQuestionAnswerStatusStatisticFilter := s.sqlDatabase.NewFilter(dbQuestionAnswerStatusStatisticFilter, false, false)
		err4 := s.sqlDatabase.SelectList(dbQuestionAnswerStatusStatisticEntity, func() {
			if dbQuestionAnswerStatusStatisticEntity.Status != 0 {
				totalAnswerCount++
			}
			if dbQuestionAnswerStatusStatisticEntity.Status == 1 {
				rightAnswerCount++
			}
		}, nil, sqlQuestionAnswerStatusStatisticFilter)
		if err4 != nil {
			return make([]*doctor.TlcHeEduPatientIntegratedQuestionInfo, 0), business.NewError(errors.InternalError, err4)
		}
		integratedQuestion.TotalAnswerNum = uint64(totalAnswerCount)
		integratedQuestion.RightAnswerNum = uint64(rightAnswerCount)
		integratedQuestions = append(integratedQuestions, integratedQuestion)
	}
	return integratedQuestions, nil
}

func (s *Data) TlcRecordPatientAnswerQuestion(argument *doctor.TlcHeEduPatientAnswerQuestionRecord) (uint64, business.Error) {
	dbEntity := &sqldb.TlcHeEduPatientQuestionUpdate{}
	dbEntity.Status = argument.Status
	now := time.Now()
	dbEntity.Answer_time = &now
	dbFilter := &sqldb.TlcHeEduPatientQuestionRecordUpdateFilter{
		UserID:      argument.PatientID,
		Question_id: argument.Id,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	data, err := s.sqlDatabase.Update(dbEntity, sqlFilter)
	if err != nil {
		return 0, business.NewError(errors.InternalError, err)
	}
	return data, nil
}

func (s *Data) TlcHasKnwCommitedAsTask(argument *doctor.ExpPatientTaskRecordCheckKnwRecord) (bool, business.Error) {
	dbEntity := &sqldb.ExpPatientTaskRecord{}
	heEduParentTaskName := "健康教育"
	heEduTaskName := "今日知识"
	t := time.Now()
	today := time.Date(t.Year(), t.Month(), t.Day(), 0, 0, 0, 0, time.Local)
	dbFilter := &sqldb.ExpPatientTaskRecordCheckKnwFilter{
		PatientID:      argument.PatientID,
		ParentTaskName: &heEduParentTaskName,
		TaskName:       heEduTaskName,
		CreateDateTime: &today,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	err := s.sqlDatabase.SelectOne(dbEntity, sqlFilter)
	if err != nil {
		if s.sqlDatabase.IsNoRows(err) {
			return false, nil
		} else {
			return false, business.NewError(errors.InternalError, err)
		}
	} else {
		return true, nil
	}
}

func (s *Data) CreateTeam(argument *doctor.TlcTeamCreateInfo) (bool, business.Error) {
	//参数校验
	if argument.TeamNickName == "" || argument.BelongToGroup == "" ||
		argument.TeamGenerateID == "" || argument.CreatorID == 0 ||
		argument.MemNumUp > 10 || argument.MemNumUp < 1 {
		return false, nil
	}
	//创建队伍
	dbTeamEntity := &sqldb.TlcTeamInfo{}
	dbTeamFilterByGenerateID := &sqldb.TlcTeamInfoFilterByGenerateID{TeamGenerateID: argument.TeamGenerateID}
	dbTeamFilterByCreatorID := &sqldb.TlcTeamInfoFilterByCreatorID{CreatorID: argument.CreatorID}

	//自动生成的id是否被注册
	sqlTeamFilterByGenerateID := s.sqlDatabase.NewFilter(dbTeamFilterByGenerateID, false, false)
	err1 := s.sqlDatabase.SelectOne(dbTeamEntity, sqlTeamFilterByGenerateID)
	//创建者是否已经创建过小队
	sqlTeamFilterByCreatorID := s.sqlDatabase.NewFilter(dbTeamFilterByCreatorID, false, false)
	err2 := s.sqlDatabase.SelectOne(dbTeamEntity, sqlTeamFilterByCreatorID)
	if err1 == nil || err2 == nil {
		return false, nil
	}
	if s.sqlDatabase.IsNoRows(err1) && s.sqlDatabase.IsNoRows(err2) {
		now := time.Now()
		dbCreateTeam := &sqldb.TlcTeamInfoCreate{
			TeamNickName:   argument.TeamNickName,
			BelongToGroup:  argument.BelongToGroup,
			MemNumUp:       argument.MemNumUp,
			TeamGenerateID: argument.TeamGenerateID,
			CreatorID:      argument.CreatorID,
			CreateTime:     &now,
		}
		_, err3 := s.sqlDatabase.Insert(dbCreateTeam)
		if err3 != nil {
			return false, business.NewError(errors.InternalError, err3)
		}
		/**创建队伍的同时，创建者自动加入队伍
		1.查找创建队伍的SerialNo
		2.将队伍创建者加入TeamUser表
		*/
		dbCreatedTeamInfo := &sqldb.TlcTeamInfo{}
		dbCreatedTeamInfoFilter := &sqldb.TlcTeamInfoFilterByGenerateID{TeamGenerateID: argument.TeamGenerateID}
		sqlCreatedTeamInfoFilter := s.sqlDatabase.NewFilter(dbCreatedTeamInfoFilter, false, false)
		s.sqlDatabase.SelectOne(dbCreatedTeamInfo, sqlCreatedTeamInfoFilter)
		creatorJoinInfo := &doctor.TlcUserJoinTeamInfo{
			UserID:       argument.CreatorID,
			TeamSerialNo: dbCreatedTeamInfo.SerialNo,
		}
		s.JoinTeam(creatorJoinInfo)

		/**创建队伍的同时，队伍收到设置好的任务
		1.依据创建好的的队伍的大队信息查找到任务列表
		2。将查找到的任务列表插入队伍任务表
		*/
		taskIDList := make([]uint64, 0)
		dbGroupTaskEntity := &sqldb.TlcTeamGroupTask{}
		dbGroupTaskFilter := &sqldb.TlcTeamGroupTaskFilter{
			BelongToGroup: dbCreatedTeamInfo.BelongToGroup,
			Valid:         1,
		}
		sqlGroupTaskFilter := s.sqlDatabase.NewFilter(dbGroupTaskFilter, false, false)
		s.sqlDatabase.SelectList(dbGroupTaskEntity, func() {
			taskIDList = append(taskIDList, dbGroupTaskEntity.TaskSno)
		}, nil, sqlGroupTaskFilter)
		dbTeamTaskInsertEntity := &sqldb.TlcTeamTaskInsert{}
		dbTeamTaskInsertEntity.TeamSno = dbCreatedTeamInfo.SerialNo
		dbTeamTaskInsertEntity.CreateTime = &now
		for _, taskID := range taskIDList {
			dbTeamTaskInsertEntity.TeamTaskSno = taskID
			_, err4 := s.sqlDatabase.Insert(dbTeamTaskInsertEntity)
			if err4 != nil {
				return false, business.NewError(errors.InternalError, err4)
			}
		}

		return true, nil
	} else {
		return false, nil
	}
}

func (s *Data) CreateTeamPre(patientID uint64) (*doctor.TlcTeamCreatePreInfoToApp, business.Error) {

	PreInfo := &doctor.TlcTeamCreatePreInfoToApp{}
	//得到用户名
	PatientNameQueryRes, _ := s.TlcGetPatientName(&doctor.TlcPatientNameQuery{PatientID: patientID})
	PreInfo.PatientName = PatientNameQueryRes.Name
	//得到预准备的队伍ID
	nowTimeStr := time.Now().Format("20060102") // 得到时间前缀
	dbTeamInfoEntity := &sqldb.TlcTeamInfo{}
	dbTeamInfoOrder := &sqldb.TlcTeamInfoSerialNoOrder{}
	teamsSerialNoList := make([]uint64, 0)
	err := s.sqlDatabase.SelectList(dbTeamInfoEntity, func() {
		teamsSerialNoList = append(teamsSerialNoList, dbTeamInfoEntity.SerialNo)
	}, dbTeamInfoOrder)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}
	TeamGenerateID := nowTimeStr + strconv.FormatUint(teamsSerialNoList[0]+1, 10)
	PreInfo.TeamGenerateID = TeamGenerateID
	return PreInfo, nil
}

func (s *Data) TeamQueryByGroup(argument *doctor.TlcTeamQueryByGroup) ([]*doctor.TlcTeamInfoIncMemNum, business.Error) {
	//基础信息
	dbTeamInfoEntity := &sqldb.TlcTeamInfo{}
	dbTeamInfoFilter := &sqldb.TlcTeamInfoFilterByGroup{BelongToGroup: argument.BelongToGroup}
	sqlTeamInfoFilter := s.sqlDatabase.NewFilter(dbTeamInfoFilter, false, false)
	TeamList := make([]*doctor.TlcTeamInfoIncMemNum, 0)
	err := s.sqlDatabase.SelectList(dbTeamInfoEntity, func() {
		Team := &doctor.TlcTeamInfoIncMemNum{
			SerialNo:       dbTeamInfoEntity.SerialNo,
			TeamNickName:   dbTeamInfoEntity.TeamNickName,
			BelongToGroup:  dbTeamInfoEntity.BelongToGroup,
			MemNum:         s.CountTeamMemNum(dbTeamInfoEntity.SerialNo),
			MemNumUp:       dbTeamInfoEntity.MemNumUp,
			TeamGenerateID: dbTeamInfoEntity.TeamGenerateID,
			CreatorID:      dbTeamInfoEntity.CreatorID,
			CreateTime:     (*types.Time)(dbTeamInfoEntity.CreateTime),
		}
		TeamList = append(TeamList, Team)
	}, nil, sqlTeamInfoFilter)
	if err != nil {
		return make([]*doctor.TlcTeamInfoIncMemNum, 0), business.NewError(errors.InternalError, err)
	}
	return TeamList, nil
}

func (s *Data) TeamQuerySearch(argument *doctor.TlcTeamQueryBySearch) ([]*doctor.TlcTeamInfoIncMemNum, business.Error) {
	dbTeamInfoEntity := &sqldb.TlcTeamInfo{}
	dbTeamInfoFilter := &sqldb.TlcTeamInfoFilterBySearchInput{
		TeamNickName:   "%" + argument.SearchInput + "%",
		TeamGenerateID: argument.SearchInput,
	}
	sqlTeamInfoFilter := s.sqlDatabase.NewFilter(dbTeamInfoFilter, true, false)
	TeamList := make([]*doctor.TlcTeamInfoIncMemNum, 0)
	err := s.sqlDatabase.SelectList(dbTeamInfoEntity, func() {
		Team := &doctor.TlcTeamInfoIncMemNum{
			SerialNo:       dbTeamInfoEntity.SerialNo,
			TeamNickName:   dbTeamInfoEntity.TeamNickName,
			BelongToGroup:  dbTeamInfoEntity.BelongToGroup,
			MemNum:         s.CountTeamMemNum(dbTeamInfoEntity.SerialNo),
			MemNumUp:       dbTeamInfoEntity.MemNumUp,
			TeamGenerateID: dbTeamInfoEntity.TeamGenerateID,
			CreatorID:      dbTeamInfoEntity.CreatorID,
			CreateTime:     (*types.Time)(dbTeamInfoEntity.CreateTime),
		}
		TeamList = append(TeamList, Team)
	}, nil, sqlTeamInfoFilter)
	if err != nil {
		return make([]*doctor.TlcTeamInfoIncMemNum, 0), business.NewError(errors.InternalError, err)
	}
	return TeamList, nil
}

func (s *Data) CountTeamMemNum(TeamSerialNo uint64) uint64 {
	dbEntity := &sqldb.TlcTeamUser{}
	dbFilter := &sqldb.TlcTeamUserFilterByTeamSerialNo{TeamSerialNo: TeamSerialNo}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	TeamMemNum, err := s.sqlDatabase.SelectCount(dbEntity, sqlFilter)
	if err != nil {
		//包含了查找报错和 IsNoRows == true 的情况
		return 0
	}
	return TeamMemNum
}

func (s *Data) JoinTeam(argument *doctor.TlcUserJoinTeamInfo) (bool, business.Error) {

	//校验要加入的队伍是否满员
	dbTeamInfoEntity := &sqldb.TlcTeamInfo{}
	dbTeamInfoFilter := &sqldb.TlcTeamInfoFilterByTeamSerialNo{SerialNo: argument.TeamSerialNo}
	sqlTeamInfoFilter := s.sqlDatabase.NewFilter(dbTeamInfoFilter, false, false)
	err := s.sqlDatabase.SelectOne(dbTeamInfoEntity, sqlTeamInfoFilter)
	if err != nil {
		return false, business.NewError(errors.InternalError, err)
	}
	currentMemNum := s.CountTeamMemNum(argument.TeamSerialNo)
	if currentMemNum >= dbTeamInfoEntity.MemNumUp {
		return false, nil
	}
	//检测用户是否已经存在队伍
	dbCheckEntity := &sqldb.TlcTeamUser{}
	dbCheckFilter := &sqldb.TlcTeamUserFilterByUserID{UserID: argument.UserID}
	sqlCheckFilter := s.sqlDatabase.NewFilter(dbCheckFilter, false, false)
	err = s.sqlDatabase.SelectOne(dbCheckEntity, sqlCheckFilter)
	if err == nil {
		return false, nil
	} else {
		if s.sqlDatabase.IsNoRows(err) {
			//用户加入队伍
			dbEntity := &sqldb.TlcTeamUser{}
			dbEntity.UserID = argument.UserID
			dbEntity.TeamSerialNo = argument.TeamSerialNo
			now := time.Now()
			dbEntity.JoinTime = &now
			_, err = s.sqlDatabase.Insert(dbEntity)
			if err != nil {
				return false, business.NewError(errors.InternalError, err)
			}
			return true, nil
		} else {
			return false, business.NewError(errors.InternalError, err)
		}
	}
}

func (s *Data) IsJoinedTeam(patientID uint64) (uint64, business.Error) {
	dbEntity := &sqldb.TlcTeamUser{}
	dbFilter := &sqldb.TlcTeamUserFilterByUserID{UserID: patientID}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	err := s.sqlDatabase.SelectOne(dbEntity, sqlFilter)
	if err == nil {
		return dbEntity.TeamSerialNo, nil
	} else {
		if s.sqlDatabase.IsNoRows(err) {
			return 0, nil
		} else {
			return 0, business.NewError(errors.InternalError, err)
		}
	}
}

func (s *Data) GetWholeTeamDietsData(argument *doctor.TlcTeamUserFuncFromAppDiets) (*doctor.TlcTeamUserFuncDietsData, business.Error) {
	result := &doctor.TlcTeamUserFuncDietsData{}
	TeamDietsData := make([]*doctor.DietRecordForTeamUserFunc, 0)
	GroupDietsData := make([]*doctor.DietRecordForTeamUserFunc, 0)
	// 用户个人的饮食记录
	SelfDietsData, err1 := s.GetCertainPatientDiet(argument.UserID, argument.TimePoint, false)
	if err1 != nil {
		return nil, err1
	}
	result.SelfDietsData = SelfDietsData
	/**
	用户所属小队的饮食记录
	1.Query(TeamSerialNo) -> TeamUserIDList
	2.Loop(TeamUserIDList) : Collection(TeamMemRes)
	*/
	TeamUserIDList := s.GetTeamMembersIDList(argument.TeamSerialNo)
	for i := 0; i < len(TeamUserIDList); i++ {
		TeamMemDietsData, err2 := s.GetCertainPatientDiet(TeamUserIDList[i], argument.TimePoint, false)
		if err2 != nil {
			return nil, err2
		}
		TeamDietsData = append(TeamDietsData, TeamMemDietsData...)
	}
	result.TeamDietsData = TeamDietsData
	/**
	用户所属大队的饮食记录
	1.Query(TeamSerialNo) -> Group
	2.Query(Group) -> GroupUserIDList
	3.Loop(GroupUserIDList) : Collection(GroupMemRes)
	*/
	dbTeamInfoEntity := &sqldb.TlcTeamInfo{}
	dbTeamInfoFilter := &sqldb.TlcTeamInfoFilterByTeamSerialNo{SerialNo: argument.TeamSerialNo}
	sqlTeamInfoFilter := s.sqlDatabase.NewFilter(dbTeamInfoFilter, false, false)
	s.sqlDatabase.SelectOne(dbTeamInfoEntity, sqlTeamInfoFilter)
	GroupKind := dbTeamInfoEntity.BelongToGroup
	GroupUserIDList := s.GetGroupMembersIDList(GroupKind)
	for i := 0; i < len(GroupUserIDList); i++ {
		GroupMemDietsData, err3 := s.GetCertainPatientDiet(GroupUserIDList[i], argument.TimePoint, true)
		if err3 != nil {
			return nil, err3
		}
		GroupDietsData = append(GroupDietsData, GroupMemDietsData...)
	}

	// 将大队模块得到的饮食数据按照时间重新排列组合
	transGroupDietsData := doctor.DietRecordsForTeamUserFunc(GroupDietsData)
	sort.Sort(transGroupDietsData)

	result.GroupDietsData = GroupDietsData
	return result, nil
}

func (s *Data) GetCertainPatientDiet(UserID uint64, TimePoint string, isGroup bool) ([]*doctor.DietRecordForTeamUserFunc, business.Error) {
	dbEntity := &sqldb.SportDietRecord{}
	if isGroup {
		campStartTime, _ := time.ParseInLocation("2006-01-02", enum.PracticeCampInfos.CampStartDate().Value, time.Local)
		dbFilter := &sqldb.DietRecordDataForTeamFuncFilter{
			RecordType:      "饮食",
			PatientID:       UserID,
			TimePoint:       &TimePoint,
			HappenStartDate: &campStartTime,
		}
		sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
		//results := make([]*doctor.SportDietRecord, 0)
		wholeInfoResults := make([]*doctor.DietRecordForTeamUserFunc, 0)
		dbDietCommentEntity := &sqldb.ExpDoctorCommentToPatient{}
		dbDietCommentFilter := &sqldb.ExpDoctorCommentToPatientSportDietRecordFilter{}
		sqlDietCommentFilter := s.sqlDatabase.NewFilter(dbDietCommentFilter, false, false)
		err := s.sqlDatabase.SelectList(dbEntity, func() {
			result := &doctor.SportDietRecord{}
			dbEntity.CopyTo(result)
			wholeInfoResult := &doctor.DietRecordForTeamUserFunc{}
			wholeInfoResult.CopyFrom(result)
			patientNameQueryRes, _ := s.TlcGetPatientName(&doctor.TlcPatientNameQuery{PatientID: result.PatientID})
			wholeInfoResult.PatientName = patientNameQueryRes.Name
			dbDietCommentFilter.SportDietRecord = result.SerialNo
			errIn := s.sqlDatabase.SelectOne(dbDietCommentEntity, sqlDietCommentFilter)
			if errIn != nil {
				// 查找不到评论或者内部错误
			} else if dbDietCommentEntity.Comment != "" {
				dietCommentQueryRes := &doctor.ExpDoctorCommentToPatient{}
				dbDietCommentEntity.CopyTo(dietCommentQueryRes)
				wholeInfoResult.DietComment = dietCommentQueryRes
				wholeInfoResults = append(wholeInfoResults, wholeInfoResult)
			}
			//results = append(results, result)
		}, nil, sqlFilter)
		if err != nil {
			return make([]*doctor.DietRecordForTeamUserFunc, 0), business.NewError(errors.InternalError, err)
		}
		/**
		继续添加信息 - wholeInfoResults
		*/
		//wholeInfoResults := make([]*doctor.DietRecordForTeamUserFunc, 0)
		//for i := 0; i < len(results); i++ {
		//	wholeInfoResult := &doctor.DietRecordForTeamUserFunc{}
		//	wholeInfoResult.CopyFrom(results[i])
		//	patientNameQueryRes, _ := s.TlcGetPatientName(&doctor.TlcPatientNameQuery{PatientID: results[i].PatientID})
		//	wholeInfoResult.PatientName = patientNameQueryRes.Name
		//	dbDietCommentEntity := &sqldb.ExpDoctorCommentToPatient{}
		//	dbDietCommentFilter := &sqldb.ExpDoctorCommentToPatientSportDietRecordFilter{SportDietRecord: results[i].SerialNo}
		//	sqlDietCommentFilter := s.sqlDatabase.NewFilter(dbDietCommentFilter, false, false)
		//	s.sqlDatabase.SelectOne(dbDietCommentEntity, sqlDietCommentFilter)
		//	if dbDietCommentEntity.Comment != "" {
		//		dietCommentQueryRes := &doctor.ExpDoctorCommentToPatient{}
		//		dbDietCommentEntity.CopyTo(dietCommentQueryRes)
		//		wholeInfoResult.DietComment = dietCommentQueryRes
		//		wholeInfoResults = append(wholeInfoResults, wholeInfoResult)
		//	}
		//dietCommentQueryRes := &doctor.ExpDoctorCommentToPatient{}
		//dbDietCommentEntity.CopyTo(dietCommentQueryRes)
		//wholeInfoResult.DietComment = dietCommentQueryRes
		//wholeInfoResults = append(wholeInfoResults, wholeInfoResult)
		//}

		return wholeInfoResults, nil
	} else {
		t := time.Now()
		today := time.Date(t.Year(), t.Month(), t.Day(), 0, 0, 0, 0, time.Local)
		dbFilter := &sqldb.DietRecordDataForTeamFuncFilter{
			RecordType:      "饮食",
			PatientID:       UserID,
			TimePoint:       &TimePoint,
			HappenStartDate: &today,
		}
		sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
		//results := make([]*doctor.SportDietRecord, 0)
		//err := s.sqlDatabase.SelectList(dbEntity, func() {
		//	result := &doctor.SportDietRecord{}
		//	dbEntity.CopyTo(result)
		//	//results = append(results, result)
		//}, nil, sqlFilter)
		wholeInfoResults := make([]*doctor.DietRecordForTeamUserFunc, 0)
		dbDietCommentEntity := &sqldb.ExpDoctorCommentToPatient{}
		dbDietCommentFilter := &sqldb.ExpDoctorCommentToPatientSportDietRecordFilter{}
		sqlDietCommentFilter := s.sqlDatabase.NewFilter(dbDietCommentFilter, false, false)
		err := s.sqlDatabase.SelectList(dbEntity, func() {
			result := &doctor.SportDietRecord{}
			dbEntity.CopyTo(result)
			wholeInfoResult := &doctor.DietRecordForTeamUserFunc{}
			wholeInfoResult.CopyFrom(result)
			patientNameQueryRes, _ := s.TlcGetPatientName(&doctor.TlcPatientNameQuery{PatientID: result.PatientID})
			wholeInfoResult.PatientName = patientNameQueryRes.Name
			dbDietCommentFilter.SportDietRecord = result.SerialNo
			errIn := s.sqlDatabase.SelectOne(dbDietCommentEntity, sqlDietCommentFilter)
			dietCommentQueryRes := &doctor.ExpDoctorCommentToPatient{}
			if errIn != nil {
				wholeInfoResult.DietComment = dietCommentQueryRes
			} else {
				dbDietCommentEntity.CopyTo(dietCommentQueryRes)
				wholeInfoResult.DietComment = dietCommentQueryRes
			}
			wholeInfoResults = append(wholeInfoResults, wholeInfoResult)
			//results = append(results, result)
		}, nil, sqlFilter)
		if err != nil {
			return make([]*doctor.DietRecordForTeamUserFunc, 0), business.NewError(errors.InternalError, err)
		}
		/**
		继续添加信息 - wholeInfoResults
		*/
		//wholeInfoResults := make([]*doctor.DietRecordForTeamUserFunc, 0)
		//for i := 0; i < len(results); i++ {
		//	wholeInfoResult := &doctor.DietRecordForTeamUserFunc{}
		//	wholeInfoResult.CopyFrom(results[i])
		//	patientNameQueryRes, _ := s.TlcGetPatientName(&doctor.TlcPatientNameQuery{PatientID: results[i].PatientID})
		//	wholeInfoResult.PatientName = patientNameQueryRes.Name
		//	dbDietCommentEntity := &sqldb.ExpDoctorCommentToPatient{}
		//	dbDietCommentFilter := &sqldb.ExpDoctorCommentToPatientSportDietRecordFilter{SportDietRecord: results[i].SerialNo}
		//	sqlDietCommentFilter := s.sqlDatabase.NewFilter(dbDietCommentFilter, false, false)
		//	s.sqlDatabase.SelectOne(dbDietCommentEntity, sqlDietCommentFilter)
		//	dietCommentQueryRes := &doctor.ExpDoctorCommentToPatient{}
		//	dbDietCommentEntity.CopyTo(dietCommentQueryRes)
		//	wholeInfoResult.DietComment = dietCommentQueryRes
		//	wholeInfoResults = append(wholeInfoResults, wholeInfoResult)
		//}
		return wholeInfoResults, nil
	}
}

func (s *Data) GetWholeTeamTaskInfo(argument *doctor.TlcTeamUserFuncFromApp) (*doctor.TlcTeamUserFuncTaskInfo, business.Error) {
	/**
	1. Get Tasks from task repository -> commonTaskInfo
	2. Copy commonTaskInfo to SelfTaskData,TeamTaskData
	3. Add Specific Information to SelfTaskData,TeamTaskData
		3.1 As for SelfTaskData -> Check Status
		3.2 As for TeamTaskData -> Get Complete Member Number
	4. Add SelfTaskData and TeamTaskData to the Corresponding Slice
	*/
	result := &doctor.TlcTeamUserFuncTaskInfo{}
	SelfTaskDataList := make([]*doctor.TlcTeamTaskForSelf, 0)
	TeamTaskDataList := make([]*doctor.TlcTeamTaskData, 0)

	//dbCommonTaskInfoEntity := &sqldb.TlcTeamTaskRepository{}
	//dbCommonTaskInfoFilter := &sqldb.TlcTeamTaskRepositoryFilterBySerialNo{}

	commonTaskInfoList := s.GetTeamBasicTaskInfoList(argument.TeamSerialNo)
	teamMemberIDList := s.GetTeamMembersIDList(argument.TeamSerialNo)
	for _, commonTaskInfo := range commonTaskInfoList {
		SelfTaskDataItem := &doctor.TlcTeamTaskForSelf{}
		TeamTaskDataItem := &doctor.TlcTeamTaskData{}

		// Add Information to SelfTaskDataItem
		commonTaskInfo.CopyToTaskForSelf(SelfTaskDataItem) // Get Basic Information
		// Check Task Record
		if s.CheckCertainPatientTaskRecord(argument.UserID, commonTaskInfo.TaskName) {
			SelfTaskDataItem.Status = 1
		} else {
			SelfTaskDataItem.Status = 0
		}

		// Add Information to TeamTaskDataItem
		commonTaskInfo.CopyToTaskForTeam(TeamTaskDataItem) // Get Basic Information
		// Check Complete Task Member Number
		completeMemNum := 0
		for i := 0; i < len(teamMemberIDList); i++ {
			if s.CheckCertainPatientTaskRecord(teamMemberIDList[i], commonTaskInfo.TaskName) {
				completeMemNum++
			}
		}
		TeamTaskDataItem.CompMemNum = uint64(completeMemNum)

		SelfTaskDataList = append(SelfTaskDataList, SelfTaskDataItem)
		TeamTaskDataList = append(TeamTaskDataList, TeamTaskDataItem)
	}

	result.SelfTaskDataList = SelfTaskDataList
	result.TeamTaskDataList = TeamTaskDataList
	return result, nil
}

func (s *Data) GetTeamInfo(argument *doctor.TlcTeamInfoFromAppFuncArea) (*doctor.TlcTeamInfoToAppFuncArea, business.Error) {
	result := &doctor.TlcTeamInfoToAppFuncArea{}
	/**
	Part1: Get Basic Information
	Part2: Get Team Task Rules Data Structure
	Part3: Get Team Got Integral Point
	*/

	// -----Part 1-------
	dbEntityPart1 := &sqldb.TlcTeamInfo{}
	dbFilterPart1 := &sqldb.TlcTeamInfoFilterByTeamSerialNo{SerialNo: argument.SerialNo}
	sqlFilterPart1 := s.sqlDatabase.NewFilter(dbFilterPart1, false, false)
	err := s.sqlDatabase.SelectOne(dbEntityPart1, sqlFilterPart1)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}
	// Fill Information of Part1
	result.CreatorID = dbEntityPart1.CreatorID
	result.TeamNickName = dbEntityPart1.TeamNickName
	result.BelongToGroup = dbEntityPart1.BelongToGroup
	result.MemNum = s.CountTeamMemNum(argument.SerialNo)
	// Get Team Total Point
	dbEntityPart1ForTotalPoint := &sqldb.ViewTlcTeamTotalPoint{}
	dbFilterPart1ForTotalPoint := &sqldb.ViewTlcTeamTotalPointFilterByTeamSno{TeamSno: argument.SerialNo}
	sqlFilterPart1ForTotalPoint := s.sqlDatabase.NewFilter(dbFilterPart1ForTotalPoint, false, false)
	err = s.sqlDatabase.SelectOne(dbEntityPart1ForTotalPoint, sqlFilterPart1ForTotalPoint)
	if err != nil {
		if s.sqlDatabase.IsNoRows(err) {
			result.Point = 0
		} else {
			return nil, business.NewError(errors.InternalError, err)
		}
	}
	result.Point = dbEntityPart1ForTotalPoint.TeamTotalIntegral
	// ----Part2: Get Team Task Rules Data Structure
	teamTasks := s.GetTeamBasicTaskInfoList(argument.SerialNo)
	taskRules := make([]*doctor.TlcTeamTaskMapWithIntegralTem, 0)
	for _, teamTask := range teamTasks {
		taskRules = append(taskRules, teamTask.GetTaskRespondedIntegralTemplate())
	}
	result.TaskRules = taskRules
	// ----Part3: Get Team Integral Record
	dbEntityPart3 := &sqldb.TlcTeamIntegral{}
	dbFilterPart3 := &sqldb.TlcTeamIntegralFilterByTeamSno{TeamSno: argument.SerialNo}
	dbOrderPart3 := &sqldb.TlcTeamIntegralOrderByCreateTime{}
	sqlFilterPart3 := s.sqlDatabase.NewFilter(dbFilterPart3, false, false)
	pointList := make([]*doctor.TlcTeamIntegralRecord, 0)
	dbEntityPart3ForTaskRsp := &sqldb.TlcTeamTaskRepository{}
	dbFilterPart3ForTaskRsp := &sqldb.TlcTeamTaskRepositoryFilterBySno{}
	sqlFilterPart3ForTaskRsp := s.sqlDatabase.NewFilter(dbFilterPart3ForTaskRsp, false, false)
	err = s.sqlDatabase.SelectList(dbEntityPart3, func() {
		pointRecord := &doctor.TlcTeamIntegralRecord{}
		pointRecord.Integral = dbEntityPart3.Integral
		pointRecord.CreateTime = (*types.Time)(dbEntityPart3.CreateTime)
		// Using TaskSno to Get Task Content in TaskRepository
		dbFilterPart3ForTaskRsp.SerialNo = dbEntityPart3.TaskSno
		s.sqlDatabase.SelectOne(dbEntityPart3ForTaskRsp, sqlFilterPart3ForTaskRsp)
		pointRecord.Content = dbEntityPart3ForTaskRsp.Content
		pointList = append(pointList, pointRecord)
	}, dbOrderPart3, sqlFilterPart3)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}
	result.PointList = pointList
	return result, nil
}

func (s *Data) GetTeamBasicTaskInfoList(TeamSerialNo uint64) []*sqldb.TlcTeamTaskRepository {
	dbTeamTaskEntity := &sqldb.TlcTeamTask{}
	dbTeamTaskFilter := &sqldb.TlcTeamTaskFilterByTeamSno{TeamSno: TeamSerialNo}
	sqlTeamTaskFilter := s.sqlDatabase.NewFilter(dbTeamTaskFilter, false, false)
	results := make([]*sqldb.TlcTeamTaskRepository, 0)
	err := s.sqlDatabase.SelectList(dbTeamTaskEntity, func() {
		dbTeamTaskRepositoryEntity := &sqldb.TlcTeamTaskRepository{}
		dbTeamTaskRepositoryFilter := &sqldb.TlcTeamTaskRepositoryFilterBySno{SerialNo: dbTeamTaskEntity.TeamTaskSno}
		sqlTeamTaskRepositoryFilter := s.sqlDatabase.NewFilter(dbTeamTaskRepositoryFilter, false, false)
		s.sqlDatabase.SelectOne(dbTeamTaskRepositoryEntity, sqlTeamTaskRepositoryFilter)
		results = append(results, dbTeamTaskRepositoryEntity)
	}, nil, sqlTeamTaskFilter)
	if err != nil {
		return make([]*sqldb.TlcTeamTaskRepository, 0)
	}
	return results
}

func (s *Data) CheckCertainPatientTaskRecord(PatientID uint64, TaskName string) bool {
	dbEntity := &sqldb.ExpPatientTaskRecord{}
	dbFilter := &sqldb.ExpPatientTaskRecordForTeamFuncCheck{}
	// Except Task Kind Other Filter Condition is the same
	dbFilter.PatientID = PatientID
	t := time.Now()
	today := time.Date(t.Year(), t.Month(), t.Day(), 0, 0, 0, 0, time.Local)
	dbFilter.CreateDateTime = &today
	switch TaskName {
	case "饮食":
		dbFilter.ParentTaskName = []string{"早餐", "中餐", "晚餐"}
	case "运动":
		dbFilter.ParentTaskName = []string{"每日运动"}
	case "教育":
		dbFilter.ParentTaskName = []string{"健康教育"}
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	err := s.sqlDatabase.SelectOne(dbEntity, sqlFilter)
	if err != nil {
		return false
	}
	return true
}

func (s *Data) GetTeamRankWholeInfo(argument *doctor.TlcTeamInfoFromAppFuncArea) (*doctor.TlcTeamRankWholeInfo, business.Error) {
	result := &doctor.TlcTeamRankWholeInfo{}
	/**
	1. Get Self Team Integral Condition
	2. Team Group -> Get Ordered Rank Basic Information Slice
	3. Get the Index of Item in Ordered Slice (Encapsulation it as a Function)
	4. Filled All Information Needed
	*/

	//----Step1----
	dbEntityStep1 := &sqldb.ViewTlcTeamTotalPoint{}
	dbFilterStep1 := &sqldb.ViewTlcTeamTotalPointFilterByTeamSno{TeamSno: argument.SerialNo}
	sqlFilterStep1 := s.sqlDatabase.NewFilter(dbFilterStep1, false, false)
	err := s.sqlDatabase.SelectOne(dbEntityStep1, sqlFilterStep1)
	if err != nil {
		if !s.sqlDatabase.IsNoRows(err) {
			return nil, business.NewError(errors.InternalError, err)
		}
	}

	dbTeamInfoEntity := &sqldb.TlcTeamInfo{}
	dbTeamInfoFilter := &sqldb.TlcTeamInfoFilterByTeamSerialNo{SerialNo: argument.SerialNo}
	sqlTeamInfoFilter := s.sqlDatabase.NewFilter(dbTeamInfoFilter, false, false)
	s.sqlDatabase.SelectOne(dbTeamInfoEntity, sqlTeamInfoFilter)

	//----Step2----
	GroupIntegralInfoList := make([]*sqldb.ViewTlcTeamTotalPoint, 0)
	dbEntityStep2 := &sqldb.ViewTlcTeamTotalPoint{}
	dbFilterStep2 := &sqldb.ViewTlcTeamTotalPointFilterByGroup{BelongToGroup: dbTeamInfoEntity.BelongToGroup}
	sqlFilterStep2 := s.sqlDatabase.NewFilter(dbFilterStep2, false, false)
	dbOrderStep2 := &sqldb.ViewTlcTeamTotalPointOrderByIntegral{}
	err = s.sqlDatabase.SelectList(dbEntityStep2, func() {
		GroupIntegralInfoItem := &sqldb.ViewTlcTeamTotalPoint{
			TeamSno:           dbEntityStep2.TeamSno,
			TeamTotalIntegral: dbEntityStep2.TeamTotalIntegral,
			TeamNickName:      dbEntityStep2.TeamNickName,
			BelongToGroup:     dbEntityStep2.BelongToGroup,
		}
		GroupIntegralInfoList = append(GroupIntegralInfoList, GroupIntegralInfoItem)
	}, dbOrderStep2, sqlFilterStep2)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}

	//-----Step3:GetRankIndexOfCertainItemInOrderedGroupIntegralList As Encapsulation-----

	// ---Step4:Fill All of Information Needed
	selfTeamIndex := s.GetRankIndexOfCertainItemInOrderedGroupIntegralList(dbEntityStep1, GroupIntegralInfoList)
	if selfTeamIndex != -1 {
		result.TeamRank = uint64(selfTeamIndex + 1)
		result.Percent = math.Trunc((float64(len(GroupIntegralInfoList)-selfTeamIndex) / float64(len(GroupIntegralInfoList))) * 100)
	} else {
		result.TeamRank = uint64(len(GroupIntegralInfoList) + 1)
		result.Percent = 0
	}
	teamRankDataList := make([]*doctor.TeamRankData, 0)
	for i := 0; i < len(GroupIntegralInfoList); i++ {
		teamRankData := &doctor.TeamRankData{
			TeamNickName:      GroupIntegralInfoList[i].TeamNickName,
			TeamTotalIntegral: GroupIntegralInfoList[i].TeamTotalIntegral,
			TeamRank:          uint64(i + 1),
			Flag:              0,
		}
		if dbEntityStep1.TeamSno == GroupIntegralInfoList[i].TeamSno {
			teamRankData.Flag = 1
		}
		teamRankDataList = append(teamRankDataList, teamRankData)
	}
	result.GroupRankData = teamRankDataList
	return result, nil
}

func (s *Data) GetRankIndexOfCertainItemInOrderedGroupIntegralList(ItemInCheck *sqldb.ViewTlcTeamTotalPoint, OrderedGroupIntegralInfoList []*sqldb.ViewTlcTeamTotalPoint) int {

	/**
	Something Goes Wrong -> -1

	Find Rank Index -> Corresponding Index
	*/

	if ItemInCheck == nil {
		return -1
	}

	for i := 0; i < len(OrderedGroupIntegralInfoList); i++ {
		if ItemInCheck.TeamTotalIntegral == OrderedGroupIntegralInfoList[i].TeamTotalIntegral {
			return i
		}
	}
	return -1
}

func (s *Data) InsertTeamIntegral(argument *doctor.TlcTeamInfoFromAppFuncArea) (bool, business.Error) {
	/**
	1.Get TeamTaskList,TeamMemberIDList
	2.There is a loop for TeamTaskList
	>>> Start
	For each task
	i) Calculate The Complete Rate -> Inner Loop of TeamMemberList
	ii) Turn into Corresponding  Integral Adding Routine
	>> End
	*/

	// --- Preparation Information -----
	teamTaskList := s.GetTeamBasicTaskInfoList(argument.SerialNo)
	teamMemberIDList := s.GetTeamMembersIDList(argument.SerialNo)
	if len(teamMemberIDList) == 1 {
		return false, nil
	}
	teamMemNum := len(teamMemberIDList)
	// --- Handle Each Task Integral Adding Event ---
	for _, task := range teamTaskList {
		var taskCompleteRate float64
		var completeNum int = 0
		for _, memberID := range teamMemberIDList {
			if s.CheckCertainPatientTaskRecord(memberID, task.TaskName) {
				completeNum++
			}
		}
		taskCompleteRate = float64(completeNum) / float64(teamMemNum)

		grade := GetGradeOfTaskCompleteRate(taskCompleteRate)

		switch grade {
		case 1:
			Integral := enum.TlcTeamTaskIntegralRuleTems.FirstLevParticipation().Key * task.Level
			op, err := s.AddTlcTeamIntegralForCertainTask(argument.SerialNo, task.SerialNo, Integral)
			if op {
				return op, nil
			} else {
				if err != nil {
					return false, err
				}
			}
		case 2:
			Integral := enum.TlcTeamTaskIntegralRuleTems.SecondLevParticipation().Key * task.Level
			op, err := s.AddTlcTeamIntegralForCertainTask(argument.SerialNo, task.SerialNo, Integral)
			if op {
				return op, nil
			} else {
				if err != nil {
					return false, err
				}
			}
		case 3:
			Integral := enum.TlcTeamTaskIntegralRuleTems.ThirdLevParticipation().Key * task.Level
			op, err := s.AddTlcTeamIntegralForCertainTask(argument.SerialNo, task.SerialNo, Integral)
			if op {
				return op, nil
			} else {
				if err != nil {
					return false, err
				}
			}
		}
	}
	return false, nil
}

func (s *Data) AddTlcTeamIntegralForCertainTask(TeamSno uint64, TaskSno uint64, Integral uint64) (bool, business.Error) {
	dbCheckExistenceEntity := &sqldb.TlcTeamIntegral{}
	t := time.Now()
	today := time.Date(t.Year(), t.Month(), t.Day(), 0, 0, 0, 0, time.Local)
	dbCheckExistenceFilter := &sqldb.TlcTeamIntegralCheckExistenceFilter{
		TeamSno:    TeamSno,
		TaskSno:    TaskSno,
		Integral:   Integral,
		CreateTime: &today,
	}
	sqlCheckExistenceFilter := s.sqlDatabase.NewFilter(dbCheckExistenceFilter, false, false)
	err := s.sqlDatabase.SelectOne(dbCheckExistenceEntity, sqlCheckExistenceFilter)
	if err == nil {
		return false, nil
	}
	if s.sqlDatabase.IsNoRows(err) {
		dbInsertEntity := &sqldb.TlcTeamIntegralAdd{
			TeamSno:    TeamSno,
			TaskSno:    TaskSno,
			Integral:   Integral,
			CreateTime: &t,
		}
		_, insertErr := s.sqlDatabase.Insert(dbInsertEntity)
		if insertErr != nil {
			return false, business.NewError(errors.InternalError, insertErr)
		}
		return true, nil
	} else {
		return false, business.NewError(errors.InternalError, err)
	}
}

func GetGradeOfTaskCompleteRate(TaskCompleteRate float64) int {

	/**
	Calculate Grade of Task Complete Rate ( This Part is about Rule of Adding Integral )
	----
	Grade 0 -> TaskCompleteRate == 0 : no integral
	Grade 1 -> TaskCompleteRate > 0 && TaskCompleteRate < 0.5 : firstLevParticipation
	Grade 2 -> TaskCompleteRate >= 0.5 && TaskCompleteRate < 1 : secondLevParticipation
	Grade 3 -> TaskCompleteRate == 1 : thirdLevParticipation
	----
	*/
	if TaskCompleteRate == 0 {
		return 0
	} else if TaskCompleteRate > 0 && TaskCompleteRate < 0.5 {
		return 1
	} else if TaskCompleteRate >= 0.5 && TaskCompleteRate < 1 {
		return 2
	} else if TaskCompleteRate == 1 {
		return 3
	}
	return -1
}

func (s *Data) GetTeamMembersIDList(TeamSerialNo uint64) []uint64 {
	results := make([]uint64, 0)
	dbEntity := &sqldb.TlcTeamUser{}
	dbFilter := &sqldb.TlcTeamUserFilterByTeamSerialNo{
		TeamSerialNo: TeamSerialNo,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	err := s.sqlDatabase.SelectList(dbEntity, func() {
		result := dbEntity.UserID
		results = append(results, result)
	}, nil, sqlFilter)
	if err != nil {
		return make([]uint64, 0)
	}
	return results
}

func (s *Data) GetGroupMembersIDList(GroupKind string) []uint64 {
	results := make([]uint64, 0)
	dbEntity := &sqldb.TlcTeamInfo{}
	dbFilter := &sqldb.TlcTeamInfoFilterByGroup{BelongToGroup: GroupKind}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	TeamSerialNoList := make([]uint64, 0)
	err := s.sqlDatabase.SelectList(dbEntity, func() {
		TeamSerialNoList = append(TeamSerialNoList, dbEntity.SerialNo)
	}, nil, sqlFilter)
	if err != nil {
		return make([]uint64, 0)
	}
	for i := 0; i < len(TeamSerialNoList); i++ {
		UnitTeamUserIDList := s.GetTeamMembersIDList(TeamSerialNoList[i])
		results = append(results, UnitTeamUserIDList...)
	}
	return results
}

func (s *Data) GetAllGroupTasks(argument *doctor.TlcTeamGroupTaskGroupKindsInput) ([]*doctor.TlcTeamGroupTaskIntegratedInfo, business.Error) {
	/**
	Set a Loop For Input Group Kinds and Replenish Each Group Tasks Information
	*/
	results := make([]*doctor.TlcTeamGroupTaskIntegratedInfo, 0)

	for _, groupKind := range argument.GroupKinds {
		result := &doctor.TlcTeamGroupTaskIntegratedInfo{}
		result.BelongToGroup = groupKind
		// Get the Information of Group Tasks
		dbGroupTaskEntity := &sqldb.TlcTeamGroupTask{}
		dbGroupTaskFilter := &sqldb.TlcTeamGroupTaskFilter{
			BelongToGroup: groupKind,
			Valid:         1,
		}
		sqlGroupTaskFilter := s.sqlDatabase.NewFilter(dbGroupTaskFilter, false, false)
		err := s.sqlDatabase.SelectOne(dbGroupTaskEntity, sqlGroupTaskFilter)
		if err != nil {
			if s.sqlDatabase.IsNoRows(err) {
				result.GroupTaskList = []string{"无"}
				result.Flag = 0
			} else {
				return nil, business.NewError(errors.InternalError, err)
			}
		} else {
			result.Flag = 1
			s.sqlDatabase.SelectList(dbGroupTaskEntity, func() {
				dbTaskRepositoryEntity := &sqldb.TlcTeamTaskRepository{}
				dbTaskRepositoryFilter := &sqldb.TlcTeamTaskRepositoryFilterBySno{SerialNo: dbGroupTaskEntity.TaskSno}
				sqlTaskRepositoryFilter := s.sqlDatabase.NewFilter(dbTaskRepositoryFilter, false, false)
				s.sqlDatabase.SelectOne(dbTaskRepositoryEntity, sqlTaskRepositoryFilter)
				result.GroupTaskList = append(result.GroupTaskList, dbTaskRepositoryEntity.TaskName)
			}, nil, sqlGroupTaskFilter)
		}
		results = append(results, result)
	}
	return results, nil
}

func (s *Data) GetSportItem(argument *doctor.SportItemType) ([]*doctor.SportItem, business.Error) {
	results := make([]*doctor.SportItem, 0)
	dbEntity := &sqldb.SportItem{}
	dbFilter := &sqldb.SportItemTypeFilter{
		Type: &argument.Type,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)

	err := s.sqlDatabase.SelectList(dbEntity, func() {
		result := &doctor.SportItem{}
		dbEntity.CopyTo(result)
		results = append(results, result)
	}, nil, sqlFilter)

	if err != nil {
		return results, business.NewError(errors.InternalError, err)

	}
	return results, nil
}

func (s *Data) CreateGroupTask(argument *doctor.TlcTeamGroupTaskCreateInfo) (bool, business.Error) {
	/**
	Data Preparation: TaskNameList -> TaskSnoIDList
	Part1: Insert Tasks into GroupTask Table
	Part2: Group Teams Get Tasks
	*/
	taskSnoIDList := make([]uint64, 0)
	dbTaskRepositoryEntity := &sqldb.TlcTeamTaskRepository{}
	dbTaskRepositoryFilter := &sqldb.TlcTeamTaskRepositoryFilterByTaskName{}
	sqlTaskRepositoryFilter := s.sqlDatabase.NewFilter(dbTaskRepositoryFilter, false, false)
	for _, taskName := range argument.GroupTaskList {
		dbTaskRepositoryFilter.TaskName = taskName
		err := s.sqlDatabase.SelectOne(dbTaskRepositoryEntity, sqlTaskRepositoryFilter)
		if err != nil {
			return false, business.NewError(errors.InternalError, err)
		}
		taskSnoIDList = append(taskSnoIDList, dbTaskRepositoryEntity.SerialNo)
	}
	// ---Part1----
	dbTeamGroupTaskInsertEntity := &sqldb.TlcTeamGroupTaskInsert{}
	dbTeamGroupTaskInsertEntity.BelongToGroup = argument.BelongToGroup
	dbTeamGroupTaskInsertEntity.Valid = 1
	now := time.Now()
	dbTeamGroupTaskInsertEntity.CreateTime = &now
	dbTeamGroupTaskInsertEntity.UpdateTime = &now
	for i := 0; i < len(taskSnoIDList); i++ {
		dbTeamGroupTaskInsertEntity.TaskSno = taskSnoIDList[i]
		_, err := s.sqlDatabase.Insert(dbTeamGroupTaskInsertEntity)
		if err != nil {
			return false, business.NewError(errors.InternalError, err)
		}
	}

	// ---Part2----
	queryTeamArg := &doctor.TlcTeamQueryByGroup{BelongToGroup: argument.BelongToGroup}
	teamList, errAbove := s.TeamQueryByGroup(queryTeamArg)
	if errAbove != nil {
		return false, errAbove
	}
	dbTeamTaskInsertEntity := &sqldb.TlcTeamTaskInsert{}
	dbTeamTaskInsertEntity.CreateTime = &now
	for _, team := range teamList {
		dbTeamTaskInsertEntity.TeamSno = team.SerialNo
		for i := 0; i < len(taskSnoIDList); i++ {
			dbTeamTaskInsertEntity.TeamTaskSno = taskSnoIDList[i]
			_, err := s.sqlDatabase.Insert(dbTeamTaskInsertEntity)
			if err != nil {
				return false, business.NewError(errors.InternalError, err)
			}
		}
	}

	return true, nil
}

func (s *Data) StopGroupTask(argument *doctor.TlcTeamGroupTaskStopInfo) (bool, business.Error) {
	/**
	Data Preparation: TaskNameList -> TaskSnoIDList
	Part1: Set Task Valid in GroupTask Table
	Part2: Group Teams Delete Tasks
	*/
	taskSnoIDList := make([]uint64, 0)
	dbTaskRepositoryEntity := &sqldb.TlcTeamTaskRepository{}
	dbTaskRepositoryFilter := &sqldb.TlcTeamTaskRepositoryFilterByTaskName{}
	sqlTaskRepositoryFilter := s.sqlDatabase.NewFilter(dbTaskRepositoryFilter, false, false)
	for _, taskName := range argument.GroupTaskList {
		dbTaskRepositoryFilter.TaskName = taskName
		err := s.sqlDatabase.SelectOne(dbTaskRepositoryEntity, sqlTaskRepositoryFilter)
		if err != nil {
			return false, business.NewError(errors.InternalError, err)
		}
		taskSnoIDList = append(taskSnoIDList, dbTaskRepositoryEntity.SerialNo)
	}

	// --- Part1: Update Group Task Valid (Batch Update) ---
	dbGroupTaskUpdateEntity := &sqldb.TlcTeamGroupTaskStopUpdate{}
	dbGroupTaskUpdateEntity.Valid = 0
	now := time.Now()
	dbGroupTaskUpdateEntity.UpdateTime = &now
	dbGroupTaskUpdateFilter := &sqldb.TlcTeamGroupTaskFilter{
		BelongToGroup: argument.BelongToGroup,
		Valid:         1,
	}
	sqlGroupTaskUpdateFilter := s.sqlDatabase.NewFilter(dbGroupTaskUpdateFilter, false, false)
	_, err := s.sqlDatabase.Update(dbGroupTaskUpdateEntity, sqlGroupTaskUpdateFilter)
	if err != nil {
		return false, business.NewError(errors.InternalError, err)
	}

	// --- Part2: Delete Group Teams Task (OneByOne Delete)
	queryTeamArg := &doctor.TlcTeamQueryByGroup{BelongToGroup: argument.BelongToGroup}
	teamList, errAbove := s.TeamQueryByGroup(queryTeamArg)
	if errAbove != nil {
		return false, errAbove
	}

	dbTeamTaskEntity := &sqldb.TlcTeamTask{}
	dbTeamTaskFilter := &sqldb.TlcTeamTaskFilter{}
	sqlTeamTaskFilter := s.sqlDatabase.NewFilter(dbTeamTaskFilter, false, false)
	for _, team := range teamList {
		dbTeamTaskFilter.TeamSno = team.SerialNo
		for i := 0; i < len(taskSnoIDList); i++ {
			dbTeamTaskFilter.TeamTaskSno = taskSnoIDList[i]
			_, err := s.sqlDatabase.Delete(dbTeamTaskEntity, sqlTeamTaskFilter)
			if err != nil {
				return false, business.NewError(errors.InternalError, err)
			}
		}
	}

	return true, nil
}

func (s *Data) GetTeamMemInfo(argument *doctor.TlcTeamUserFuncFromApp) (*doctor.TlcTeamUserFuncMemsInfo, business.Error) {

	wholeInfo := &doctor.TlcTeamUserFuncMemsInfo{}

	// Get Request UserID
	wholeInfo.ReqUserID = argument.UserID

	// Get MemsInfoList
	results := make([]*doctor.PatientUserBaseInfo, 0)

	teamMemIDList := s.GetTeamMembersIDList(argument.TeamSerialNo)

	for _, teamMemID := range teamMemIDList {
		result, err := s.GetPatientInfo(&doctor.PatientInfoBase{PatientID: teamMemID})
		if err != nil {
			return nil, err
		}
		results = append(results, result)
	}

	wholeInfo.MemsInfoList = results

	return wholeInfo, nil
}

func (s *Data) DeleteTeamMem(argument *doctor.TlcTeamUserFuncFromApp) (bool, business.Error) {

	dbEntity := &sqldb.TlcTeamUser{}
	dbFilter := &sqldb.TlcTeamUserFilter{
		TeamSerialNo: argument.TeamSerialNo,
		UserID:       argument.UserID,
	}

	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)

	_, err := s.sqlDatabase.Delete(dbEntity, sqlFilter)
	if err != nil {
		return false, business.NewError(errors.InternalError, err)
	}
	return true, nil
}

func (s *Data) GetTeamWeightsData(argument *doctor.TlcTeamUserFuncFromApp) (*doctor.TlcTeamUserFuncWeightsData, business.Error) {
	result := &doctor.TlcTeamUserFuncWeightsData{}

	/**
	1. Get Time Series []*time.Time :
	---
	Start From Self First Record Weight Data Date
	To Group End Measuring Date
	Step is Group Update Measuring Date
	>>
	2. At Each Processed Time Point :
	---
	As for self ->
	Query (<= MeasureTimePoint) and (PatientID = ?) order by MeasureDateTime desc limit 1
	As for team ->
	Each Mem in Team : Above Query
	Get []float64 CertainDateTimeTeamMemWeightList
	Process Average Team Weight
	As for group ->
	Each Mem in Group : Above Query
	Get []float64 CertainDateTimeGroupMemWeightList
	Process Average Group Weight
	*/

	//-----Part1 : Get Time Series-----

	dbTeamInfoEntity := &sqldb.TlcTeamInfo{}
	dbTeamInfoFilter := &sqldb.TlcTeamInfoFilterByTeamSerialNo{SerialNo: argument.TeamSerialNo}
	sqlTeamInfoFilter := s.sqlDatabase.NewFilter(dbTeamInfoFilter, false, false)
	err := s.sqlDatabase.SelectOne(dbTeamInfoEntity, sqlTeamInfoFilter)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}

	originalGetTimeSeries := make([]*time.Time, 0)
	dbWeightRecordTimeEntity := &sqldb.WeightRecordMeasureDateTime{}
	dbWeightRecordTimeFilter := &sqldb.WeightRecordFilterByPatientIDList{}
	sqlWeightRecordTimeFilter := s.sqlDatabase.NewFilter(dbWeightRecordTimeFilter, false, false)
	dbWeightRecordTimeOrder := &sqldb.WeightRecordDataOrder{}
	groupMemIDList := s.GetGroupMembersIDList(dbTeamInfoEntity.BelongToGroup)
	dbWeightRecordTimeFilter.PatientIDList = groupMemIDList
	err = s.sqlDatabase.SelectList(dbWeightRecordTimeEntity, func() {
		originalGetTimeSeries = append(originalGetTimeSeries, dbWeightRecordTimeEntity.MeasureDateTime)
	}, dbWeightRecordTimeOrder, sqlWeightRecordTimeFilter)

	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}

	formattedTimeSeries := make([]*time.Time, 0)

	for _, originalTime := range originalGetTimeSeries {
		t := *originalTime
		getRidTime := time.Date(t.Year(), t.Month(), t.Day(), 0, 0, 0, 0, time.Local)
		nextDay := getRidTime.AddDate(0, 0, 1)
		formattedTime := time.Date(nextDay.Year(), nextDay.Month(), nextDay.Day(), 0, 0, 0, 0, time.Local)
		formattedTimeSeries = append(formattedTimeSeries, &formattedTime)
	}

	getCleanedTimeSeries := removeRepByMapForTimeType(formattedTimeSeries)

	selfTimeSeries := make([]*time.Time, 0)
	dbWeightRecordTimeFilter.PatientIDList = []uint64{argument.UserID}
	err = s.sqlDatabase.SelectList(dbWeightRecordTimeEntity, func() {
		selfTimeSeries = append(selfTimeSeries, dbWeightRecordTimeEntity.MeasureDateTime)
	}, dbWeightRecordTimeOrder, sqlWeightRecordTimeFilter)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}
	startTimeOriginal := selfTimeSeries[0]
	st := *startTimeOriginal
	stGetRidTime := time.Date(st.Year(), st.Month(), st.Day(), 0, 0, 0, 0, time.Local)
	stNextDay := stGetRidTime.AddDate(0, 0, 1)
	startTimeFormatted := time.Date(stNextDay.Year(), stNextDay.Month(), stNextDay.Day(), 0, 0, 0, 0, time.Local)
	startTime := &startTimeFormatted

	indexInCleanedTimeSeries := 0
	for i := 0; i < len(getCleanedTimeSeries); i++ {
		if *startTime == *getCleanedTimeSeries[i] {
			indexInCleanedTimeSeries = i
			break
		}
	}

	processedTimeSeries := make([]*time.Time, 0)

	for i := indexInCleanedTimeSeries; i < len(getCleanedTimeSeries); i++ {
		processedTimeSeries = append(processedTimeSeries, getCleanedTimeSeries[i])
	}

	// Insert Option of Practice Start Time in Time Series
	practiceCampStartTime, _ := time.ParseInLocation("2006-01-02", enum.PracticeCampInfos.CampStartDate().Value, time.Local)
	practiceTime := practiceCampStartTime.AddDate(0, 0, 1)

	proProcessedTimeSeries := make([]*time.Time, 0)

	if practiceTime.After(*processedTimeSeries[0]) && practiceTime.Before(*processedTimeSeries[len(processedTimeSeries)-1]) {
		indexInProcessTimeSeries := 0
		for i := 0; i < len(processedTimeSeries); i++ {
			if practiceTime.Before(*processedTimeSeries[i]) {
				indexInProcessTimeSeries = i
				break
			}
		}
		for i := indexInProcessTimeSeries; i < len(processedTimeSeries); i++ {
			proProcessedTimeSeries = append(proProcessedTimeSeries, processedTimeSeries[i])
		}
	} else {
		proProcessedTimeSeries = processedTimeSeries
	}

	// -----Part2: Get Weight Record in Every MeasureDataTime-----
	SelfWeightList := make([]float64, 0)
	TeamAvaWeightList := make([]float64, 0)
	GroupAvaWeightList := make([]float64, 0)
	teamMemIDList := s.GetTeamMembersIDList(argument.TeamSerialNo)
	for _, certainMeasureDataTime := range proProcessedTimeSeries {
		// For Self Weight
		selfWeight, errAbove := s.GetMostRecentWeight(argument.UserID, certainMeasureDataTime)
		if errAbove != nil {
			return nil, errAbove
		}
		// The Mechanism Decides selfWeight is always existed
		SelfWeightList = append(SelfWeightList, selfWeight)

		// For Team Average Weight in Certain MeasureDateTime
		teamWeightListInCertainTime := make([]float64, 0)
		for _, teamMemID := range teamMemIDList {
			certainTeamMemWeight, errAbove := s.GetMostRecentWeight(teamMemID, certainMeasureDataTime)
			if errAbove != nil {
				return nil, errAbove
			}
			if certainTeamMemWeight != 0 {
				teamWeightListInCertainTime = append(teamWeightListInCertainTime, certainTeamMemWeight)
			}
		}
		teamTotalWeight := float64(0)
		for _, weight := range teamWeightListInCertainTime {
			teamTotalWeight += weight
		}
		teamAvaWeight := teamTotalWeight / float64(len(teamWeightListInCertainTime)) // The Mechanism Decides Denominator will never be 0
		TeamAvaWeightList = append(TeamAvaWeightList, teamAvaWeight)

		// For Group Average Weight
		groupWeightListInCertainTime := make([]float64, 0)
		for _, groupMemID := range groupMemIDList {
			certainGroupMemWeight, errAbove := s.GetMostRecentWeight(groupMemID, certainMeasureDataTime)
			if errAbove != nil {
				return nil, errAbove
			}
			if certainGroupMemWeight != 0 {
				groupWeightListInCertainTime = append(groupWeightListInCertainTime, certainGroupMemWeight)
			}
		}
		totalGroupWeight := float64(0)
		for _, weight := range groupWeightListInCertainTime {
			totalGroupWeight += weight
		}
		groupAvaWeight := totalGroupWeight / float64(len(groupWeightListInCertainTime))
		GroupAvaWeightList = append(GroupAvaWeightList, groupAvaWeight)
	}

	// -----Convert Data Type To Result

	finalTimeSeries := make([]*types.Time, 0)
	for _, time := range proProcessedTimeSeries {
		if time == nil {
			finalTimeSeries = append(finalTimeSeries, nil)
		} else {
			finalTime := types.Time(*time)
			finalTimeSeries = append(finalTimeSeries, &finalTime)
		}
	}

	result.TimeSeries = finalTimeSeries
	result.SelfWeightData = SelfWeightList
	result.TeamWeightsData = TeamAvaWeightList
	result.GroupWeightsData = GroupAvaWeightList

	return result, nil
}

func (s *Data) GetTeamSportStepsData(argument *doctor.TlcTeamUserFuncFromApp) (*doctor.TlcTeamUserFuncSportStepsData, business.Error) {
	result := &doctor.TlcTeamUserFuncSportStepsData{}

	/**
	1. Get Time Series []*time.Time :
	---
	Start From Self First Record Sport Step Data Date
	To Group End Happen Date
	Step is Group Update Happen Date
	>>
	2. At Each Processed Time Point :
	---
	As for self ->
	Query (<= HappenTimePoint) and (PatientID = ?) order by HappenDateTime desc limit 1
	As for team ->
	Each Mem in Team : Above Query
	Get []uint64 CertainDateTimeTeamMemWeightList
	Process Average Team Weight
	As for group ->
	Each Mem in Group : Above Query
	Get []uint64 CertainDateTimeGroupMemWeightList
	Process Average Group Weight
	*/

	//-----Part1 : Get Time Series-----

	dbTeamInfoEntity := &sqldb.TlcTeamInfo{}
	dbTeamInfoFilter := &sqldb.TlcTeamInfoFilterByTeamSerialNo{SerialNo: argument.TeamSerialNo}
	sqlTeamInfoFilter := s.sqlDatabase.NewFilter(dbTeamInfoFilter, false, false)
	err := s.sqlDatabase.SelectOne(dbTeamInfoEntity, sqlTeamInfoFilter)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}

	originalGetTimeSeries := make([]*time.Time, 0)
	dbStepRecordTimeEntity := &sqldb.SportDietRecordHappenDateTime{}
	dbStepRecordTimeFilter := &sqldb.SportDietRecordStepFilterByPatientIDList{}
	sqlStepRecordTimeFilter := s.sqlDatabase.NewFilter(dbStepRecordTimeFilter, false, false)
	dbStepRecordTimeOrder := &sqldb.SportDietRecordDataOrder{}
	groupMemIDList := s.GetGroupMembersIDList(dbTeamInfoEntity.BelongToGroup)
	dbStepRecordTimeFilter.PatientIDList = groupMemIDList
	dbStepRecordTimeFilter.RecordType = "运动"
	dbStepRecordTimeFilter.ItemName = "微信运动"
	err = s.sqlDatabase.SelectList(dbStepRecordTimeEntity, func() {
		originalGetTimeSeries = append(originalGetTimeSeries, dbStepRecordTimeEntity.HappenDateTime)
	}, dbStepRecordTimeOrder, sqlStepRecordTimeFilter)

	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}

	formattedTimeSeries := make([]*time.Time, 0)

	for _, originalTime := range originalGetTimeSeries {
		t := *originalTime
		getRidTime := time.Date(t.Year(), t.Month(), t.Day(), 0, 0, 0, 0, time.Local)
		nextDay := getRidTime.AddDate(0, 0, 1)
		formattedTime := time.Date(nextDay.Year(), nextDay.Month(), nextDay.Day(), 0, 0, 0, 0, time.Local)
		formattedTimeSeries = append(formattedTimeSeries, &formattedTime)
	}

	getCleanedTimeSeries := removeRepByMapForTimeType(formattedTimeSeries)

	selfTimeSeries := make([]*time.Time, 0)
	dbStepRecordTimeFilter.PatientIDList = []uint64{argument.UserID}
	err = s.sqlDatabase.SelectList(dbStepRecordTimeEntity, func() {
		selfTimeSeries = append(selfTimeSeries, dbStepRecordTimeEntity.HappenDateTime)
	}, dbStepRecordTimeOrder, sqlStepRecordTimeFilter)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}
	startTimeOriginal := selfTimeSeries[0]
	st := *startTimeOriginal
	stGetRidTime := time.Date(st.Year(), st.Month(), st.Day(), 0, 0, 0, 0, time.Local)
	stNextDay := stGetRidTime.AddDate(0, 0, 1)
	startTimeFormatted := time.Date(stNextDay.Year(), stNextDay.Month(), stNextDay.Day(), 0, 0, 0, 0, time.Local)
	startTime := &startTimeFormatted

	indexInCleanedTimeSeries := 0
	for i := 0; i < len(getCleanedTimeSeries); i++ {
		if *startTime == *getCleanedTimeSeries[i] {
			indexInCleanedTimeSeries = i
			break
		}
	}

	processedTimeSeries := make([]*time.Time, 0)

	for i := indexInCleanedTimeSeries; i < len(getCleanedTimeSeries); i++ {
		processedTimeSeries = append(processedTimeSeries, getCleanedTimeSeries[i])
	}

	// Insert Option of Practice Start Time in Time Series
	practiceCampStartTime, _ := time.ParseInLocation("2006-01-02", enum.PracticeCampInfos.CampStartDate().Value, time.Local)
	practiceTime := practiceCampStartTime.AddDate(0, 0, 1)

	proProcessedTimeSeries := make([]*time.Time, 0)

	if practiceTime.After(*processedTimeSeries[0]) && practiceTime.Before(*processedTimeSeries[len(processedTimeSeries)-1]) {
		indexInProcessTimeSeries := 0
		for i := 0; i < len(processedTimeSeries); i++ {
			if practiceTime.Before(*processedTimeSeries[i]) {
				indexInProcessTimeSeries = i
				break
			}
		}
		for i := indexInProcessTimeSeries; i < len(processedTimeSeries); i++ {
			proProcessedTimeSeries = append(proProcessedTimeSeries, processedTimeSeries[i])
		}
	} else {
		proProcessedTimeSeries = processedTimeSeries
	}

	// -----Part2: Get Step Record in Every HappenDataTime-----
	SelfStepList := make([]uint64, 0)
	TeamAvaStepList := make([]float64, 0)
	GroupAvaStepList := make([]float64, 0)
	teamMemIDList := s.GetTeamMembersIDList(argument.TeamSerialNo)
	for _, certainHappenDateTime := range proProcessedTimeSeries {
		// For Self Step
		selfStep, errAbove := s.GetMostRecentStep(argument.UserID, certainHappenDateTime)
		if errAbove != nil {
			return nil, errAbove
		}
		// The Mechanism Decides selfStep is always existed
		SelfStepList = append(SelfStepList, selfStep)

		// For Team Average Step in Certain HappenDateTime
		teamStepListInCertainTime := make([]uint64, 0)
		for _, teamMemID := range teamMemIDList {
			certainTeamMemStep, errAbove := s.GetMostRecentStep(teamMemID, certainHappenDateTime)
			if errAbove != nil {
				return nil, errAbove
			}
			if certainTeamMemStep != 0 {
				teamStepListInCertainTime = append(teamStepListInCertainTime, certainTeamMemStep)
			}
		}
		teamTotalStep := uint64(0)
		for _, step := range teamStepListInCertainTime {
			teamTotalStep += step
		}
		teamAvaStep := float64(teamTotalStep) / float64(len(teamStepListInCertainTime)) // The Mechanism Decides Denominator will never be 0
		TeamAvaStepList = append(TeamAvaStepList, teamAvaStep)

		// For Group Average Step
		groupStepListInCertainTime := make([]uint64, 0)
		for _, groupMemID := range groupMemIDList {
			certainGroupMemStep, errAbove := s.GetMostRecentStep(groupMemID, certainHappenDateTime)
			if errAbove != nil {
				return nil, errAbove
			}
			if certainGroupMemStep != 0 {
				groupStepListInCertainTime = append(groupStepListInCertainTime, certainGroupMemStep)
			}
		}
		totalGroupStep := uint64(0)
		for _, step := range groupStepListInCertainTime {
			totalGroupStep += step
		}
		groupAvaStep := float64(totalGroupStep) / float64(len(groupStepListInCertainTime))
		GroupAvaStepList = append(GroupAvaStepList, groupAvaStep)
	}

	// -----Convert Data Type To Result

	finalTimeSeries := make([]*types.Time, 0)
	for _, time := range proProcessedTimeSeries {
		if time == nil {
			finalTimeSeries = append(finalTimeSeries, nil)
		} else {
			finalTime := types.Time(*time)
			finalTimeSeries = append(finalTimeSeries, &finalTime)
		}
	}

	result.TimeSeries = finalTimeSeries
	result.SelfStepsData = SelfStepList
	result.TeamStepsData = TeamAvaStepList
	result.GroupStepsData = GroupAvaStepList

	return result, nil
}

func removeRepByMapForTimeType(slc []*time.Time) []*time.Time {
	result := []*time.Time{}
	tempMap := map[time.Time]int{}
	for _, element := range slc {
		l := len(tempMap)
		tempMap[*element] = 0
		if len(tempMap) != l {
			result = append(result, element)
		}
	}
	return result
}

func (s *Data) GetMostRecentWeight(PatientID uint64, MeasureDateTime *time.Time) (float64, business.Error) {

	sqlAccess, err := s.sqlDatabase.NewAccess(true)
	if err != nil {
		return 0, business.NewError(errors.InternalError, err)
	}
	defer sqlAccess.Close()

	dbEntity := &sqldb.WeightRecordWeightValue{}
	sqlEntity := s.sqlDatabase.NewEntity()
	sqlEntity.Parse(dbEntity)
	sqlBuilder := s.sqlDatabase.NewBuilder()
	sqlBuilder.Select(sqlEntity.ScanFields(), false).From(sqlEntity.Name())
	sqlBuilder.Append("where PatientID = ? and MeasureDateTime <= ? order by MeasureDateTime desc limit 1")
	query := sqlBuilder.Query()

	rows, err := sqlAccess.Query(query, PatientID, MeasureDateTime)
	if err != nil {
		if s.sqlDatabase.IsNoRows(err) {
			return 0, nil
		} else {
			return 0, business.NewError(errors.InternalError, err)
		}
	}
	for rows.Next() {
		err = rows.Scan(sqlEntity.ScanArgs()...)
		if err != nil {
			return 0, business.NewError(errors.InternalError, err)
		}
	}

	err = sqlAccess.Commit()
	if err != nil {
		return 0, business.NewError(errors.InternalError, err)
	}

	return dbEntity.Weight, nil
}

func (s *Data) GetMostRecentStep(PatientID uint64, HappenDateTime *time.Time) (uint64, business.Error) {
	sqlAccess, err := s.sqlDatabase.NewAccess(true)
	if err != nil {
		return 0, business.NewError(errors.InternalError, err)
	}
	defer sqlAccess.Close()

	dbEntity := &sqldb.SportDietRecordStepItemValue{}
	sqlEntity := s.sqlDatabase.NewEntity()
	sqlEntity.Parse(dbEntity)
	sqlBuilder := s.sqlDatabase.NewBuilder()
	sqlBuilder.Select(sqlEntity.ScanFields(), false).From(sqlEntity.Name())
	sqlBuilder.Append("where RecordType = ? and ItemName = ? and PatientID = ? and HappenDateTime <= ? order by HappenDateTime desc limit 1")
	query := sqlBuilder.Query()
	rows, err := sqlAccess.Query(query, "运动", "微信运动", PatientID, HappenDateTime)
	if err != nil {
		if s.sqlDatabase.IsNoRows(err) {
			return 0, nil
		} else {
			return 0, business.NewError(errors.InternalError, err)
		}
	}
	for rows.Next() {
		err = rows.Scan(sqlEntity.ScanArgs()...)
		if err != nil {
			return 0, business.NewError(errors.InternalError, err)
		}
	}

	err = sqlAccess.Commit()
	if err != nil {
		return 0, business.NewError(errors.InternalError, err)
	}
	// Convert ItemValue:*string -> uint64
	if dbEntity.ItemValue == nil {
		return 0, nil
	}
	resStr := *dbEntity.ItemValue
	resInt, _ := strconv.Atoi(resStr)
	result := uint64(resInt)
	return result, nil
}

func (s *Data) ListGroupDict() ([]*doctor.TlcTeamInfoGroupDict, business.Error) {

	dbEntity := &sqldb.TlcTeamInfoGroup{}
	campStartTime, _ := time.ParseInLocation("2006-01-02", enum.PracticeCampInfos.CampStartDate().Value, time.Local)
	dbFilter := &sqldb.TlcTeamInfoFilterByTime{CreateTime: &campStartTime}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	results := make([]*doctor.TlcTeamInfoGroupDict, 0)
	err := s.sqlDatabase.SelectDistinct(dbEntity, func() {
		result := &doctor.TlcTeamInfoGroupDict{}
		result.Value = dbEntity.BelongToGroup
		result.Text = dbEntity.BelongToGroup
		results = append(results, result)
	}, nil, sqlFilter)
	if err != nil {
		if s.sqlDatabase.IsNoRows(err) {
			return make([]*doctor.TlcTeamInfoGroupDict, 0), nil
		}
		return make([]*doctor.TlcTeamInfoGroupDict, 0), business.NewError(errors.InternalError, err)
	}
	return results, nil
}

func (s *Data) ListTeamDict() ([]*doctor.TlcTeamInfoTeamDict, business.Error) {
	dbEntity := &sqldb.TlcTeamInfoTeamNickName{}
	campStartTime, _ := time.ParseInLocation("2006-01-02", enum.PracticeCampInfos.CampStartDate().Value, time.Local)
	dbFilter := &sqldb.TlcTeamInfoFilterByTime{CreateTime: &campStartTime}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	results := make([]*doctor.TlcTeamInfoTeamDict, 0)
	err := s.sqlDatabase.SelectDistinct(dbEntity, func() {
		result := &doctor.TlcTeamInfoTeamDict{}
		result.Text = dbEntity.TeamNickName
		result.Value = dbEntity.TeamNickName
		results = append(results, result)
	}, nil, sqlFilter)

	if err != nil {
		if s.sqlDatabase.IsNoRows(err) {
			return make([]*doctor.TlcTeamInfoTeamDict, 0), nil
		}
		return make([]*doctor.TlcTeamInfoTeamDict, 0), business.NewError(errors.InternalError, err)
	}

	return results, nil
}
