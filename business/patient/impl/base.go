package impl

import (
	"fmt"
	"github.com/ktpswjz/httpserver/types"
	"tlcbme_project/business"
	"tlcbme_project/data/entity/sqldb"
	"tlcbme_project/data/model/doctor"
	"tlcbme_project/database"
	"tlcbme_project/database/memory"
	"tlcbme_project/errors"
	"tlcbme_project/server/config"
	"tlcbme_project/server/notify"
)

type base struct {
	types.Base

	cfg            *config.Config
	sqlDatabase    database.SqlDatabase
	nSqlDatabase   database.SqlDatabase
	memoryToken    memory.Token
	client         business.Client
	notifyChannels notify.ChannelCollection
}

func (s *base) getUserAuthByID(sqlAccess database.SqlAccess, id uint64) (*sqldb.PatientUserAuths, error) {
	dbFilter := &sqldb.PatientUserAuthsFilter{
		UserID: &id,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	dbEntity := &sqldb.PatientUserAuths{}

	err := sqlAccess.SelectOne(dbEntity, sqlFilter)
	if err != nil {
		return nil, err
	}

	return dbEntity, nil
}

func (s *base) getManagedIndex(sqlAccess database.SqlAccess, patientID uint64) (*sqldb.ManagedPatientIndex, error) {
	dbFilter := &sqldb.ManagedPatientIndexFilterBase{
		PatientID: &patientID,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	dbEntity := &sqldb.ManagedPatientIndex{}

	err := s.sqlDatabase.SelectOne(dbEntity, sqlFilter)
	if err != nil {
		return nil, err
	}

	return dbEntity, nil
}

func (s base) WriteNotify(message *notify.Message) {
	if s.notifyChannels == nil {
		return
	}

	go func(msg *notify.Message) {
		s.notifyChannels.Write(message)
	}(message)
}
func (s *base) uploadDataToPlatform(patientId uint64, dataType string, needPatientName, needDoctorName bool, time *types.Time) (uint64, business.Error) {
	name := "未知"
	dbEntity := &sqldb.ViewPatientOrg{}
	dbFilter := &sqldb.ViewPatientOrgFilterBase{PatientID: &patientId}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	err := s.sqlDatabase.SelectOne(dbEntity, sqlFilter)
	if err != nil {
		return 0, business.NewError(errors.InternalError, err)
	}
	uploadData := &doctor.UploadData{}
	dbEntity.CopyToExt(uploadData)
	if !needDoctorName {
		uploadData.DoctorName = &name
	}
	if !needPatientName {
		uploadData.PatientName = name
	}
	uploadData.DataType = dataType
	uploadData.Time = string(time.String())
	uri := s.cfg.Platform.Providers.Api.Uri.UploadData
	result, err := s.client.PlatformBusinessPost("慢病系统数据监测资源", patientId, uri, uploadData)
	if err != nil {
		return 0, business.NewError(errors.InternalError, err)
	}
	if result.Code != 200 || result.Msg != "" {
		return 0, business.NewError(errors.InternalError, fmt.Errorf("%d-%s", result.Code, result.Msg))
	}
	return patientId, nil
}
