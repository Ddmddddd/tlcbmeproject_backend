package impl

import (
	"fmt"
	"github.com/ktpswjz/httpserver/types"
	"strconv"
	"strings"
	"tlcbme_project/business"
	"tlcbme_project/data/entity/nutrient"
	"tlcbme_project/data/entity/sqldb"
	"tlcbme_project/data/model/doctor"
	"tlcbme_project/data/model/nutrient"
	"tlcbme_project/errors"
)

func (s *Data) CreateNutrientReport(dateTime *types.Time) {
	//改成查有记录的用户列表而不是全部的用户列表
	patientList := make([]*sqldb.PatientUserBaseInfoMonthly, 0)
	dbEntity := &sqldb.DietRecordUser{}
	startTime := dateTime.ToDate(0)
	endTime := dateTime.ToDate(1)
	filter := &sqldb.DietRecordDateFilter{
		EatStartDateTime: startTime,
		EatEndDateTime:   endTime,
	}
	sqlFilter := s.sqlDatabase.NewFilter(filter, false, false)
	height := uint64(0)
	_ = s.sqlDatabase.SelectDistinct(dbEntity, func() {
		temp := &sqldb.PatientUserBaseInfoMonthly{
			UserID: dbEntity.PatientID,
			Height: &height,
		}
		patientList = append(patientList, temp)
	}, nil, sqlFilter)
	s.LogInfo(patientList)
	ch := make(chan int, 100)
	for _, v := range patientList {
		ch <- 1
		go s.worker(ch, dateTime, v)
	}

	//patientList := make([]*sqldb.PatientUserBaseInfoMonthly, 0)
	//dbEntity := &sqldb.PatientUserBaseInfoMonthly{}
	//_ = s.sqlDatabase.SelectList(dbEntity, func() {
	//	temp := &sqldb.PatientUserBaseInfoMonthly{
	//		UserID: dbEntity.UserID,
	//		Height: dbEntity.Height,
	//	}
	//	patientList = append(patientList, temp)
	//}, nil)
	//ch := make(chan int, 100)
	//for _, v := range patientList {
	//	ch <- 1
	//	go s.worker(ch, dateTime, v)
	//}
}
func (s *Data) worker(ch chan int, dateTime *types.Time, v *sqldb.PatientUserBaseInfoMonthly) {
	//time.Sleep(time.Second)
	filter := &doctor.NutrientAnalysisFilter{
		PatientID:        v.UserID,
		AnalysisDateTime: dateTime,
	}
	s.LogInfo(filter.PatientID)
	s.LogInfo("worker")
	result, err := s.NutrientAnalysis(filter)
	if err != nil {
		//fmt.Println(err)
	} else {
		s.LogInfo(result.DietPlan)
		s.LogInfo(result.AllIntake.Carbohydrate.Actual)
		s.CreateNutrientReportRecord(result, filter)
	}
	<-ch
}

func (s *Data) CreateNutrientReportRecord(result *doctor.NutrientAnalysisResult, filter *doctor.NutrientAnalysisFilter) {
	start := filter.AnalysisDateTime.ToDate(0)
	end := filter.AnalysisDateTime.ToDate(1)
	dbFilter := &sqldb.NutritionalAnalysisRecordFilterEx{
		PatientID:     filter.PatientID,
		StartDateTime: start,
		EndDateTime:   end,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)

	dbEntity := &sqldb.NutritionalAnalysisRecord{}
	err := s.sqlDatabase.SelectOne(dbEntity, sqlFilter)
	s.LogInfo("selectOne的结果")
	s.LogInfo(dbEntity.SerialNo)
	if err != nil {
		if s.sqlDatabase.IsNoRows(err) {
			dbEntity.CopyFromEx(result, filter)
			s.LogInfo("新增记录dbentity")
			s.LogInfo(*dbEntity.DietPlan)
			s.LogInfo(*dbEntity.Carbohydrate)
			_, _ = s.sqlDatabase.InsertSelective(dbEntity)
		}
	} else {
		if dbEntity.Carbohydrate == nil {
			upFilter := &sqldb.NutritionalAnalysisRecordNoFilter{
				SerialNo: dbEntity.SerialNo,
			}
			upSqlFilter := s.sqlDatabase.NewFilter(upFilter, false, false)
			dbEntity.CopyFromEx(result, filter)
			s.LogInfo("更新记录dbentity")
			s.LogInfo(*dbEntity.DietPlan)
			s.LogInfo(*dbEntity.Carbohydrate)
			_, _ = s.sqlDatabase.UpdateSelective(dbEntity, upSqlFilter)
		}
	}

}

func (s *Data) GetHistoryGrade(filter *doctor.NutrientAnalysisFilterEx) ([]*doctor.Grade, business.Error) {
	gradeList := make([]*doctor.Grade, 0)
	dbEntity := &sqldb.NutritionalAnalysisRecord{}
	dbFilter := &sqldb.NutritionalAnalysisRecordFilterEx{}
	if filter.DateType != nil {
		start := filter.AnalysisDateTime.ToDate(1).AddDate(0, 0, int(-(*filter.DateType)))
		dbFilter = &sqldb.NutritionalAnalysisRecordFilterEx{
			PatientID:     filter.PatientID,
			StartDateTime: &start,
			EndDateTime:   filter.AnalysisDateTime.ToDate(1),
		}
	} else {
		dbFilter.PatientID = filter.PatientID
	}

	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	err := s.sqlDatabase.SelectList(dbEntity, func() {
		dbModel1 := &doctor.Grade{}
		dbEntity.CopyToGrade(dbModel1)
		gradeList = append(gradeList, dbModel1)
	}, nil, sqlFilter)
	if err != nil {
		return nil, business.NewError(errors.InternalError, fmt.Errorf("获取历史分数失败：%s", err))
	}

	return gradeList, nil
}

func (s *Data) GetHistoryEnergy(filter *doctor.NutrientAnalysisFilterEx) ([]*doctor.Grade, business.Error) {
	gradeList := make([]*doctor.Grade, 0)
	dbEntity := &sqldb.NutritionalAnalysisRecord{}
	dbFilter := &sqldb.NutritionalAnalysisRecordFilterEx{}
	if filter.DateType != nil {
		start := filter.AnalysisDateTime.ToDate(1).AddDate(0, 0, int(-(*filter.DateType)))
		dbFilter = &sqldb.NutritionalAnalysisRecordFilterEx{
			PatientID:     filter.PatientID,
			StartDateTime: &start,
			EndDateTime:   filter.AnalysisDateTime.ToDate(1),
		}
	} else {
		dbFilter.PatientID = filter.PatientID
	}

	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	err := s.sqlDatabase.SelectList(dbEntity, func() {
		dbModel1 := &doctor.Grade{}
		dbEntity.CopyToEnergy(dbModel1)
		gradeList = append(gradeList, dbModel1)
	}, nil, sqlFilter)
	if err != nil {
		return nil, business.NewError(errors.InternalError, fmt.Errorf("获取历史能量失败：%s", err))
	}

	return gradeList, nil
}

func (s *Data) FoodSearch(argument *doctor.FoodSearchFilter) ([]*nutrient.FoodSearchResult, business.Error) {
	results := make([]*nutrient.FoodSearchResult, 0)
	if argument == nil {
		return results, business.NewError(errors.InternalError, fmt.Errorf("argument nil"))
	}

	dbFilter := &nutrientdb.FoodNameFilter{}
	dbFilter.CopyFrom(argument)
	dbFilter2 := &nutrientdb.VersionFilter{
		Version1: 2,
		Version2: 0,
	}
	sqlFilter := s.nSqlDatabase.NewFilter(dbFilter, true, false)
	sqlFilter2 := s.nSqlDatabase.NewFilter(dbFilter2, true, false)
	dbEntity := &nutrientdb.Viewfood{}
	dbOrder := &nutrientdb.FoodOrder{}
	err := s.nSqlDatabase.SelectList(dbEntity, func() {
		result := &nutrient.FoodSearchResult{}
		dbEntity.CopyToEx(result)
		results = append(results, result)
	}, dbOrder, sqlFilter, sqlFilter2)
	if err != nil {
		return results, business.NewError(errors.InternalError, err)
	}
	return results, nil
}

func (s *Data) GetFoodMeasureUnit(argument *doctor.FoodMeasureUnitFilter) ([]*nutrient.Food_measure_unit, business.Error) {
	results := make([]*nutrient.Food_measure_unit, 0)
	if argument == nil {
		return results, business.NewError(errors.InternalError, fmt.Errorf("argument nil"))
	}
	if argument.Source == "food" {
		dbFilter := &nutrientdb.Food_measure_unit_serialNo_filter{
			Food_Serial_No: argument.Serial_No,
		}
		sqlFilter := s.nSqlDatabase.NewFilter(dbFilter, false, false)
		dbEntity := &nutrientdb.Food_measure_unit{}
		err := s.nSqlDatabase.SelectList(dbEntity, func() {
			result := &nutrient.Food_measure_unit{}
			dbEntity.CopyTo(result)
			results = append(results, result)
		}, nil, sqlFilter)
		if err != nil {
			return results, business.NewError(errors.InternalError, err)
		}
	} else if argument.Source == "packagedfood" {
		dbFilter := &nutrientdb.PackagedfoodFilter{
			Serial_No: argument.Serial_No,
		}
		dbEntity := &nutrientdb.Packagedfood{}
		sqlFilter := s.nSqlDatabase.NewFilter(dbFilter, false, false)
		err := s.nSqlDatabase.SelectOne(dbEntity, sqlFilter)
		if err != nil {
			return results, business.NewError(errors.InternalError, err)
		}
		unit_Part := ""
		unit_Name_Part := ""
		edible_Part := float64(0)
		if dbEntity.PackingSizeUnit != nil {
			unit_Name_Part = *dbEntity.PackingSizeUnit
		}
		if dbEntity.Portion != nil {
			unit_Part = strings.ReplaceAll(*dbEntity.Portion, "100", "")
		}
		if dbEntity.PackingSizeNum != nil {
			edible_Part = *dbEntity.PackingSizeNum
		}
		result := &nutrient.Food_measure_unit{
			Serial_No:      dbEntity.Serial_No,
			Food_Serial_No: dbEntity.Serial_No,
			Number_Part:    1,
			Unit_Part:      unit_Part,
			Unit_Name_Part: unit_Name_Part,
			Level_Part:     "",
			Edible_Part:    edible_Part,
			Flag:           0,
		}
		result1 := &nutrient.Food_measure_unit{
			Serial_No:      dbEntity.Serial_No,
			Food_Serial_No: dbEntity.Serial_No,
			Number_Part:    100,
			Unit_Part:      "g",
			Unit_Name_Part: "克",
			Level_Part:     "",
			Edible_Part:    100,
			Flag:           1,
		}
		results = append(results, result, result1)
	}
	return results, nil
}

func (s *Data) FoodMultiSearch(argument *doctor.FoodMultiSearchFilter) ([]*nutrient.FoodSearchResult, business.Error) {
	names := strings.Split(argument.Name, "，")
	finalResults := make([]*nutrient.FoodSearchResult, 0)
	/** 这个方法太笨了，有没有更好的方法啊，union怎么在go里面用  */
	if argument.Type == 0 {
		for _, name := range names {
			if len(name) == 0 {
				continue
			}
			item := &doctor.FoodSearchFilter{Name: name}
			results, err := s.FoodSearch(item)
			if err != nil {
				return results, err
			}
			if len(results) > 0 {
				finalResults = append(finalResults, results[0])
			}
		}
	} else {
		for _, name := range names {
			if len(name) == 0 {
				continue
			}
			item := &doctor.FoodSearchFilter{Name: name}
			results, err := s.FoodBasicSearch(item)
			if err != nil {
				return nil, err
			}
			if len(results) > 0 {
				finalResults = append(finalResults, results[0])
			}
		}
	}

	//dbFilter := &nutrientdb.FoodNameFilter{
	//	NAME: name,
	//	Alias: name,
	//}
	//dbFilter2 := &nutrientdb.VersionFilter{
	//	Version1: 2,
	//	Version2: 0,
	//}
	//sqlFilter := s.nSqlDatabase.NewFilter(dbFilter, true, false)
	//sqlFilter2 := s.nSqlDatabase.NewFilter(dbFilter2, true, false)
	//dbEntity := &nutrientdb.Viewfood{}
	//dbOrder := &nutrientdb.FoodOrder{}
	//err := s.nSqlDatabase.SelectList(dbEntity, func() {
	//	result := &nutrient.FoodSearchResult{}
	//	dbEntity.CopyToEx(result)
	//	results = append(results, result)
	//}, dbOrder, sqlFilter, sqlFilter2)

	return finalResults, nil
}

func (s *Data) FoodBasicSearch(argument *doctor.FoodSearchFilter) ([]*nutrient.FoodSearchResult, business.Error) {
	results := make([]*nutrient.FoodSearchResult, 0)
	if argument == nil {
		return results, business.NewError(errors.InternalError, fmt.Errorf("argument nil"))
	}

	dbFilter := &nutrientdb.FoodNameFilter{}
	dbFilter.CopyFrom(argument)
	dbFilter2 := &nutrientdb.VersionFilter{
		Version1: 2,
		Version2: 0,
	}
	sqlFilter := s.nSqlDatabase.NewFilter(dbFilter, true, false)
	sqlFilter2 := s.nSqlDatabase.NewFilter(dbFilter2, true, false)
	dbEntity := &nutrientdb.Food{}
	dbOrder := &nutrientdb.FoodOrder{}
	err := s.nSqlDatabase.SelectList(dbEntity, func() {
		result := &nutrient.FoodSearchResult{}
		dbEntity.CopyToEx(result)
		results = append(results, result)
	}, dbOrder, sqlFilter, sqlFilter2)
	if err != nil {
		return results, business.NewError(errors.InternalError, err)
	}
	return results, nil
}

func (s *Data) AddPatientRecipe(argument *nutrient.NewRecipeFromApp) (*nutrient.FoodSearchResult, business.Error) {
	noEntity := &nutrientdb.Recipe_lib{}
	noSqlEntity := s.nSqlDatabase.NewEntity()
	noSqlEntity.Parse(noEntity)
	sqlBuilder := s.nSqlDatabase.NewBuilder()
	sqlBuilder.Select(noSqlEntity.ScanFields(), false).From(noSqlEntity.Name())
	sqlBuilder.Append("WHERE `Serial_No` = (SELECT MAX(`Serial_No`) FROM `recipe_lib`)")
	query := sqlBuilder.Query()

	sqlAccess, err := s.nSqlDatabase.NewAccess(true)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}
	defer sqlAccess.Close()

	row := sqlAccess.QueryRow(query)
	err = row.Scan(noSqlEntity.ScanArgs()...)
	if err != nil {
		if !sqlAccess.IsNoRows(err) {
			return nil, business.NewError(errors.InternalError, err)
		}
	}
	maxNo := strconv.FormatInt(int64(noEntity.Serial_No+1), 10)
	prefix := "0208"
	zeroLength := 13 - len(maxNo) - len(prefix)
	zero := ""
	for i := 0; i < zeroLength; i++ {
		zero += "0"
	}
	id := prefix + zero + maxNo
	dbEntity := &nutrientdb.Recipe_lib{}
	dbEntity.CopyFromEx(argument)
	dbEntity.ID = id
	_, err = sqlAccess.InsertSelective(dbEntity)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}
	total := float64(0)
	for _, value := range argument.List {
		compEntity := &nutrientdb.Recipe_compose{
			RECIPE_ID: id,
			FOOD_NAME: &value.FoodName,
			FOOD_ID:   &value.FoodID,
			WEIGHT:    value.WEIGHT,
		}
		_, err = sqlAccess.InsertSelective(compEntity)
		if err != nil {
			return nil, business.NewError(errors.InternalError, err)
		}
		total += value.WEIGHT
	}

	result := &nutrient.FoodSearchResult{
		Serial_No:    noEntity.Serial_No + 1,
		NAME:         argument.Name,
		FOOD_TYPE_ID: "0",
		//FOOD_TYPE_ID: strconv.FormatFloat(total, 'E', -1, 64),
		Source: "recipe",
		ID:     id,
	}
	err = sqlAccess.Commit()
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}
	return result, nil
}
