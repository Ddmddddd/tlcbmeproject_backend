package impl

import (
	"fmt"
	"github.com/ktpswjz/httpserver/types"
	"strconv"
	"strings"
	"time"
	"tlcbme_project/business"
	"tlcbme_project/data/entity/sqldb"
	"tlcbme_project/data/model/doctor"
	"tlcbme_project/data/model/manage"
	"tlcbme_project/database"
	"tlcbme_project/errors"
	"tlcbme_project/server/config"
	"tlcbme_project/server/notify"
)

func (s *Data) CommitBloodPressureRecord(argument *doctor.BloodPressureRecordForAppEx, isDevice bool) (uint64, business.Error) {
	sqlAccess, err := s.sqlDatabase.NewAccess(true)
	if err != nil {
		return 0, business.NewError(errors.InternalError, err)
	}
	defer sqlAccess.Close()

	dbEntity := &sqldb.BloodPressureRecord{}
	dbEntity.CopyFromAppEx(argument)

	sno := argument.SerialNo
	//update := false
	if argument.SerialNo > 0 {
		_, err = sqlAccess.UpdateSelectiveByPrimaryKey(dbEntity)
		//update = true
	} else {
		sno, err = sqlAccess.InsertSelective(dbEntity)
	}
	if err != nil {
		return 0, business.NewError(errors.InternalError, err)
	}
	//measureDateTime := types.Time(*dbEntity.MeasureDateTime)
	//if !isDevice {
	//	go func(patientId uint64, dataType string, time *types.Time) {
	//		s.uploadDataToPlatform(patientId, dataType, true, false, time)
	//	}(dbEntity.PatientID, "血压测量", &measureDateTime)
	//}
	err = sqlAccess.Commit()
	if err != nil {
		return 0, business.NewError(errors.InternalError, err)
	}

	//go func(sn uint64, patientId uint64) {
	//	if update {
	//		providerType := config.ManagementProviderTypeHbp
	//		uri := s.cfg.Management.Providers.Hbp.Api.Uri.DeleteRecord
	//		assess := &manage.DeleteBloodPressureRecordInput{
	//			PatientID: fmt.Sprint(patientId),
	//			SerialNo:  fmt.Sprint(sno),
	//		}
	//		s.client.ManageBusinessPost("删除患者血压数据", patientId, providerType, uri, assess)
	//	}
	//	s.chBloodPressureRecord <- sn
	//}(sno, argument.PatientID)
	//
	//s.WriteNotify(&notify.Message{
	//	BusinessID: notify.BusinessDoctor,
	//	NotifyID:   notify.DoctorUpdateBloodPressure,
	//	Time:       types.Time(time.Now()),
	//	Data:       argument.PatientID,
	//})

	return sno, nil
}

func (s *Data) CommitBloodGlucoseRecord(argument *doctor.BloodGlucoseRecordForAppEx) (uint64, business.Error) {
	sqlAccess, err := s.sqlDatabase.NewAccess(true)
	if err != nil {
		return 0, business.NewError(errors.InternalError, err)
	}
	defer sqlAccess.Close()
	dbEntity := &sqldb.BloodGlucoseRecord{}
	dbEntity.CopyFromAppEx(argument)
	//measureDateTime := types.Time(*dbEntity.MeasureDateTime)
	//go func(patientId uint64, dataType string, time *types.Time) {
	//	s.uploadDataToPlatform(patientId, dataType, true, false, time)
	//}(dbEntity.PatientID, "血糖监测", &measureDateTime)
	//return s.saveBloodGlucoseRecord(dbEntity)
	sno := argument.SerialNo
	if argument.SerialNo > 0 {
		_, err = sqlAccess.UpdateSelectiveByPrimaryKey(dbEntity)
	} else {
		sno, err = sqlAccess.InsertSelective(dbEntity)
	}
	if err != nil {
		return 0, business.NewError(errors.InternalError, err)
	}
	err = sqlAccess.Commit()
	if err != nil {
		return 0, business.NewError(errors.InternalError, err)
	}

	return sno, nil
}

func (s *Data) CommitWeightRecord(argument *doctor.WeightRecordForAppEx) (uint64, business.Error) {
	sqlAccess, err := s.sqlDatabase.NewAccess(true)
	if err != nil {
		return 0, business.NewError(errors.InternalError, err)
	}
	defer sqlAccess.Close()

	//更新血压、血糖表
	bpNo := uint64(0)
	gluNo := uint64(0)

	//1. 新增记录，血压值不为空，序号为0
	//2. 删除记录，血压值为空，序号不为0
	//3. 修改记录，血压值不为空，序号不为0
	//4. 不改动，血压值为空，序号为0
	if argument.SystolicPressure != nil || *argument.BloodPressureNo != 0 {
		//有改动
		if argument.SystolicPressure == nil {
			//删除
			bpArgument := &doctor.BloodPressureRecordForAppEx{
				PatientID: argument.PatientID,
				BloodPressureRecordForApp: doctor.BloodPressureRecordForApp{
					SerialNo: *argument.BloodPressureNo,
				},
			}
			s.DeleteBloodPressureRecord(bpArgument)
			bpNo = 0
		} else {
			bpArgument := &doctor.BloodPressureRecordForAppEx{
				PatientID: argument.PatientID,
				BloodPressureRecordForApp: doctor.BloodPressureRecordForApp{
					SerialNo:          *argument.BloodPressureNo,
					SystolicPressure:  *argument.SystolicPressure,
					DiastolicPressure: *argument.DiastolicPressure,
					MeasureDateTime:   argument.MeasureDateTime,
				},
			}
			bpNo, _ = s.CommitBloodPressureRecord(bpArgument, false)
		}
	}
	if argument.BloodGlucose != nil || *argument.BloodGlucoseNo != 0 {
		if argument.BloodGlucose == nil {
			//删除
			gluArgument := &doctor.BloodGlucoseRecordForAppEx{
				PatientID: argument.PatientID,
				BloodGlucoseRecordForApp: doctor.BloodGlucoseRecordForApp{
					SerialNo: *argument.BloodGlucoseNo,
				},
			}
			s.DeleteBloodGlucoseRecord(gluArgument)
			gluNo = 0
		} else {
			gluArgument := &doctor.BloodGlucoseRecordForAppEx{
				PatientID: argument.PatientID,
				BloodGlucoseRecordForApp: doctor.BloodGlucoseRecordForApp{
					SerialNo:        *argument.BloodGlucoseNo,
					TimePoint:       *argument.TimePoint,
					BloodGlucose:    *argument.BloodGlucose,
					MeasureDateTime: argument.MeasureDateTime,
				},
			}
			gluNo, _ = s.CommitBloodGlucoseRecord(gluArgument)
		}
	}
	count := uint64(0)
	dbLatestEntity := &sqldb.WeightRecord{}
	dbOrder := &sqldb.WeightRecordDataFilter{}
	dbFilter := &sqldb.WeightRecordPatientFilter{
		PatientID: argument.PatientID,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	sqlAccess.SelectDistinct(dbLatestEntity, func() {
		count++
	}, dbOrder, sqlFilter)

	dbEntity := &sqldb.WeightRecord{}
	dbEntity.CopyFromAppEx(argument)
	dbEntity.BloodPressureNo = &bpNo
	dbEntity.BloodGlucoseNo = &gluNo

	sno := argument.SerialNo
	if argument.SerialNo > 0 {
		_, err = sqlAccess.UpdateSelectiveByPrimaryKey(dbEntity)
	} else {
		sno, err = sqlAccess.InsertSelective(dbEntity)
	}
	if err != nil {
		return 0, business.NewError(errors.InternalError, err)
	}
	err = sqlAccess.Commit()
	if err != nil {
		return 0, business.NewError(errors.InternalError, err)
	}
	if dbLatestEntity.MeasureDateTime == nil || dbEntity.MeasureDateTime.After(*dbLatestEntity.MeasureDateTime) {
		dbEntityUpdate := &sqldb.PatientUserBaseInfoUpdateWeight{
			UserID:    argument.PatientID,
			Weight:    &argument.Weight,
			Waistline: argument.Waistline,
			Height:    argument.Height,
		}
		_, err = s.sqlDatabase.UpdateSelectiveByPrimaryKey(dbEntityUpdate)
		if err != nil {
			return 0, business.NewError(errors.InternalError, err)
		}
		//err = s.UploadDataToManage(sno)
		//if err != nil {
		//	return 0, business.NewError(errors.InternalError, err)
		//}
		//go func(sno uint64) {
		//
		//}(sno)
	}

	measureDateTime := types.Time(*dbEntity.MeasureDateTime)
	go func(patientId uint64, dataType string, time *types.Time) {
		s.uploadDataToPlatform(patientId, dataType, true, false, time)
	}(dbEntity.PatientID, "体重测量", &measureDateTime)

	s.WriteNotify(&notify.Message{
		BusinessID: notify.BusinessDoctor,
		NotifyID:   notify.DoctorUpdateWeight,
		Time:       types.Time(time.Now()),
		Data:       argument.PatientID,
	})

	return sno, nil
}

func (s *Data) CommitDrugRecord(argument *doctor.DrugRecordForAppEx) (string, business.Error) {
	res := argument.SerialNo

	sqlAccess, err := s.sqlDatabase.NewAccess(true)
	if err != nil {
		return res, business.NewError(errors.InternalError, err)
	}
	defer sqlAccess.Close()

	var sno uint64
	dbEntity := &sqldb.DrugUseRecord{}
	if len(argument.SerialNo) > 0 {
		arrSNO := strings.Split(argument.SerialNo, ",")
		for _, v := range arrSNO {
			sno, _ = strconv.ParseUint(v, 10, 64)
			dbEntity.SerialNo = sno
			dbFilter := &sqldb.DrugRecordFilter{SerialNo: &sno}
			sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
			_, err = sqlAccess.Delete(dbEntity, sqlFilter)
			if err != nil {
				return res, business.NewError(errors.InternalError, err)
			}
		}
	}

	arrNewSNO := make([]string, 0)
	indexUpdatedCount := uint64(0)
	arrDrugRecord := argument.ConvertToDrugRecordList()
	for _, v := range arrDrugRecord {
		dbEntity.CopyFrom(v)
		sno, err = sqlAccess.InsertSelective(dbEntity)
		if err != nil {
			return res, business.NewError(errors.InternalError, err)
		}
		arrNewSNO = append(arrNewSNO, strconv.FormatUint(sno, 10))

		dbEntity.SerialNo = sno
		count, err := s.saveDrugIndex(sqlAccess, dbEntity)
		if err != nil {
			return res, business.NewError(errors.InternalError, err)
		}
		indexUpdatedCount += count
	}
	res = strings.Join(arrNewSNO, ",")
	useDateTime := types.Time(*dbEntity.UseDateTime)
	go func(patientId uint64, dataType string, time *types.Time) {
		s.uploadDataToPlatform(patientId, dataType, true, false, time)
	}(dbEntity.PatientID, "服药记录", &useDateTime)
	err = sqlAccess.Commit()
	if err != nil {
		return res, business.NewError(errors.InternalError, err)
	}

	if indexUpdatedCount > 0 {
		s.WriteNotify(&notify.Message{
			BusinessID: notify.BusinessDoctor,
			NotifyID:   notify.DoctorUpdateDrug,
			Time:       types.Time(time.Now()),
			Data:       argument.PatientID,
		})
	}

	return res, nil
}

func (s *Data) saveDrugIndex(sqlAccess database.SqlAccess, record *sqldb.DrugUseRecord) (uint64, error) {
	dbFilter := &sqldb.DrugUseRecordIndexFilter{
		PatientID: &record.PatientID,
		DrugName:  record.DrugName,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	dbEntity := &sqldb.DrugUseRecordIndex{}
	err := sqlAccess.SelectOne(dbEntity, sqlFilter)
	if err != nil {
		if !s.sqlDatabase.IsNoRows(err) {
			return 0, err
		}

		dbEntity.PatientID = record.PatientID
		dbEntity.DrugName = record.DrugName
		dbEntity.Dosage = record.Dosage
		dbEntity.Status = 0
		dbEntity.FirstUseDateTime = record.UseDateTime
		dbEntity.LastUseDateTime = record.UseDateTime
		dbEntity.IsModified = 0

		sno, err := sqlAccess.InsertSelective(dbEntity)
		if err != nil {
			return 0, err
		}
		dbEntity.SerialNo = sno

		err = s.saveDrugLog(sqlAccess, dbEntity, "开始使用")
		if err != nil {
			return 0, err
		}
		return 1, nil
	} else {
		if dbEntity.Status != 0 {
			dbEntity.Status = 0
			dbEntity.Dosage = record.Dosage
			dbEntity.Freq = record.Freq
			dbEntity.LastUseDateTime = record.UseDateTime
			dbEntity.IsModified = 1
			dbEntity.LastModifyDateTime = record.UseDateTime
			_, err = sqlAccess.UpdateByPrimaryKey(dbEntity)
			if err != nil {
				return 0, err
			}
			err = s.saveDrugLog(sqlAccess, dbEntity, "开始使用")
			if err != nil {
				return 0, err
			}
			return 1, nil
		} else {
			if !s.isStringEqual(dbEntity.Dosage, record.Dosage) {
				dbEntity.Dosage = record.Dosage
				dbEntity.Freq = record.Freq
				dbEntity.IsModified = 1
				dbEntity.LastModifyDateTime = record.UseDateTime
				dbEntity.LastUseDateTime = record.UseDateTime
				_, err = sqlAccess.UpdateByPrimaryKey(dbEntity)
				if err != nil {
					return 0, err
				}
				err = s.saveDrugLog(sqlAccess, dbEntity, "调整用法")
				if err != nil {
					return 0, err
				}
				return 1, nil
			}
		}
	}

	return 0, nil
}

func (s *Data) isStringEqual(src, dst *string) bool {
	if src == nil {
		if dst == nil || *dst == "" {
			return true
		} else {
			return false
		}
	} else {
		if dst == nil {
			return false
		} else {
			if *src == *dst {
				return true
			} else {
				return false
			}
		}
	}
}

func (s *Data) saveDrugLog(sqlAccess database.SqlAccess, index *sqldb.DrugUseRecordIndex, modifyType string) error {
	dbEntity := &sqldb.DrugUseRecordIndexModifiedLog{
		DURISerialNo:   &index.SerialNo,
		PatientID:      index.PatientID,
		DrugName:       index.DrugName,
		Dosage:         index.Dosage,
		Freq:           index.Freq,
		ModifyType:     modifyType,
		ModifyDateTime: index.LastUseDateTime,
	}
	_, err := sqlAccess.InsertSelective(dbEntity)
	if err != nil {
		return err
	}

	return nil
}

func (s *Data) CommitDietRecord(argument *doctor.DietRecordForAppEx) (string, business.Error) {
	res := argument.SerialNo

	sqlAccess, err := s.sqlDatabase.NewAccess(true)
	if err != nil {
		return res, business.NewError(errors.InternalError, err)
	}
	defer sqlAccess.Close()

	dbEntity := &sqldb.SportDietRecord{}

	dietRecord := argument.ConvertToDietRecord()
	dbEntity.CopyFrom(dietRecord)
	var sno uint64
	if dbEntity.SerialNo == 0 {
		sno, err = sqlAccess.InsertSelective(dbEntity)
		if err != nil {
			return res, business.NewError(errors.InternalError, err)
		}

	} else {
		sno, err = sqlAccess.UpdateSelectiveByPrimaryKey(dbEntity)
		if err != nil {
			return res, business.NewError(errors.InternalError, err)
		}

	}
	happenDateTime := types.Time(*dbEntity.HappenDateTime)
	go func(patientId uint64, dataType string, time *types.Time) {
		s.uploadDataToPlatform(patientId, dataType, true, false, time)
	}(dbEntity.PatientID, "饮食记录", &happenDateTime)
	err = sqlAccess.Commit()
	if err != nil {
		return res, business.NewError(errors.InternalError, err)
	}

	//return res, nil
	return strconv.FormatUint(sno, 10), nil
}

func (s *Data) CommitSportRecord(argument *doctor.SportRecordForAppEx) (uint64, business.Error) {
	dbEntity := &sqldb.SportDietRecord{}
	dbEntity.CopyFromSportRecord(argument)

	var err error
	sno := argument.SerialNo
	if argument.SerialNo > 0 {
		_, err = s.sqlDatabase.UpdateSelectiveByPrimaryKey(dbEntity)
	} else {
		sno, err = s.sqlDatabase.InsertSelective(dbEntity)
	}

	// 查找当前积分规则
	pointEntity := &sqldb.ExpIntegralRule{}
	pointFilter := &sqldb.ExpIntegralRuleFilter{OrgCode: "20210422"}
	sqlPointFilter := s.sqlDatabase.NewFilter(pointFilter, false, false)
	err = s.sqlDatabase.SelectOne(pointEntity, sqlPointFilter)
	if err != nil {
		return 0, business.NewError(errors.InternalError, err)
	}

	//运动积分
	sportTime, err := strconv.Atoi(*dbEntity.ItemValue)
	if sportTime >= 20 && *dbEntity.Photo != "" {
		t := time.Now()
		today := time.Date(t.Year(), t.Month(), t.Day(), 0, 0, 0, 0, time.Local)
		dbIntEntity := &sqldb.ExpIntegral{}
		dbIntFilter := &sqldb.ExpIntegralFilter{
			PatientID:      argument.PatientID,
			Reason:         "每日运动",
			CreateDateTime: &today,
		}
		sqlIntFilter := s.sqlDatabase.NewFilter(dbIntFilter, false, false)
		sportPoint := int(*pointEntity.Sport)
		//判断今天是否已经给过积分
		err = s.sqlDatabase.SelectOne(dbIntEntity, sqlIntFilter)
		sportFlag := false
		if err == nil {
			sportFlag = true
			sportPoint = 0
		} else if err != nil {
			if !s.sqlDatabase.IsNoRows(err) {
				return 0, business.NewError(errors.InternalError, err)
			}
		}
		if !sportFlag {
			err = s.AddIntegralForPatient(argument.PatientID, uint64(sportPoint), "每日运动")
			if err != nil {
				return 0, business.NewError(errors.InternalError, err)
			}
		}

		//同步在总积分表里更新积分，需先判断是否存在该用户

		//dbTotalEntity := &sqldb.Exptotalpoint{}
		//dbTotalFilter := &sqldb.ExptotalPointFilter{
		//	PatientID: argument.PatientID,
		//}
		//sqlTotalFilter := s.sqlDatabase.NewFilter(dbTotalFilter,false,false)
		//err = s.sqlDatabase.SelectOne(dbTotalEntity,sqlTotalFilter)
		//if err == nil {
		//	newTotalPoint := dbTotalEntity.TotalPoint + uint64(sportPoint)
		//	dbTotalUpdate := &sqldb.ExptotalPointUpdate{
		//		TotalPoint: newTotalPoint,
		//	}
		//	s.sqlDatabase.Update(dbTotalUpdate,sqlTotalFilter)
		//} else {
		//	dbInfoEntity := &sqldb.PatientUserBaseInfo{}
		//	dbInfoFilter := &sqldb.PatientUserBaseInfoUserIdFilter{
		//		UserID: argument.PatientID,
		//	}
		//	sqlInfoFilter := s.sqlDatabase.NewFilter(dbInfoFilter,false,false)
		//	err = s.sqlDatabase.SelectOne(dbInfoEntity,sqlInfoFilter)
		//	if err == nil {
		//		dbNewEntity := &sqldb.Exptotalpoint{
		//			PatientID: argument.PatientID,
		//			PatientName: dbInfoEntity.Name,
		//			TotalPoint: uint64(sportPoint),
		//		}
		//		_, err = s.sqlDatabase.Insert(dbNewEntity)
		//	}
		//}
	}

	if err != nil {
		return 0, business.NewError(errors.InternalError, err)
	}
	happenDateTime := types.Time(*dbEntity.HappenDateTime)
	go func(patientId uint64, dataType string, time *types.Time) {
		s.uploadDataToPlatform(patientId, dataType, true, false, time)
	}(dbEntity.PatientID, "运动记录", &happenDateTime)
	return sno, nil
}

func (s *Data) CommitDiscomfortRecord(argument *doctor.DiscomfortRecordForAppEx) (uint64, business.Error) {
	dbEntity := &sqldb.DiscomfortRecord{}
	dbEntity.CopyFromAppEx(argument)
	sqlAccess, err := s.sqlDatabase.NewAccess(false)
	if err != nil {
		return 0, business.NewError(errors.InternalError, err)
	}
	defer sqlAccess.Close()
	sno := argument.SerialNo
	update := false
	if argument.SerialNo > 0 {
		_, err = sqlAccess.UpdateSelectiveByPrimaryKey(dbEntity)
		update = true
	} else {
		sno, err = sqlAccess.InsertSelective(dbEntity)
	}
	if err != nil {
		return 0, business.NewError(errors.InternalError, err)
	}

	go func(sn uint64) {
		if update {
			manageIndex, err := s.getManagedIndex(sqlAccess, argument.PatientID)
			noManageIndex := false
			if err != nil {
				if sqlAccess.IsNoRows(err) {
					//return 0, business.NewError(errors.InternalError, fmt.Errorf("患者ID(%d)不存在", dbEntity.PatientID))
					noManageIndex = true
				} else {
					return
				}
			}
			providerType := config.ManagementProviderTypeUnknown
			uri := ""
			if noManageIndex {
				providerType = config.ManagementProviderTypeHbp
				uri = s.cfg.Management.Providers.Hbp.Api.Uri.DeleteDiscomfort
			} else {
				if strings.Contains(*manageIndex.ManageClass, "高血压") {
					providerType = config.ManagementProviderTypeHbp
					uri = s.cfg.Management.Providers.Hbp.Api.Uri.DeleteDiscomfort
				} else if strings.Contains(*manageIndex.ManageClass, "糖尿病") {
					providerType = config.ManagementProviderTypeDm
					uri = s.cfg.Management.Providers.Dm.Api.Uri.DeleteDiscomfort
				}
			}
			assess := &manage.DeleteBloodPressureRecordInput{
				PatientID: fmt.Sprint(argument.PatientID),
				SerialNo:  fmt.Sprint(argument.SerialNo),
			}
			s.client.ManageBusinessPost("删除患者不适症数据", dbEntity.PatientID, providerType, uri, assess)
		}
		s.chDiscomfortRecord <- sn
	}(sno)

	return sno, nil
}

func (s *Data) CommitUsabilityStayTimeInfo(argument *doctor.UsabilityStayTime) (uint64, business.Error) {
	dbEntity := &sqldb.UsabilityStayTime{}
	dbEntity.CopyFrom(argument)

	sno, err := s.sqlDatabase.InsertSelective(dbEntity)
	if err != nil {
		return 0, business.NewError(errors.InternalError, fmt.Errorf(""))
	}

	return sno, nil
}

func (s *Data) CommitUsabilityEventInfo(argument *doctor.UsabilityEvent) (uint64, business.Error) {
	dbEntity := &sqldb.UsabilityEvent{}
	dbEntity.CopyFrom(argument)

	sno, err := s.sqlDatabase.InsertSelective(dbEntity)
	if err != nil {
		return 0, business.NewError(errors.InternalError, fmt.Errorf(""))
	}

	return sno, nil
}

func (s *Data) CommitUsabilityDeviceInfo(argument *doctor.UsabilityDevice) (uint64, business.Error) {
	dbEntity := &sqldb.UsabilityDevice{}
	dbEntity.CopyFrom(argument)

	// 无效的设备
	if len(argument.DeviceIMEI) <= 0 && len(argument.PhoneBrand) <= 0 {
		return 0, business.NewError(errors.NotExist, fmt.Errorf(""))
	}

	dbFilter := &sqldb.UsabilityDeviceFilter{
		UserName: argument.UserName,
		AppId:    argument.AppId,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	dbOldEntity := &sqldb.UsabilityDevice{}
	bIsOldDevice := false
	var sno uint64
	err := s.sqlDatabase.SelectList(dbOldEntity, func() {
		if dbOldEntity.DeviceIMEI != nil {
			deviceIMEI := string(*dbOldEntity.DeviceIMEI)
			if deviceIMEI == argument.DeviceIMEI {
				bIsOldDevice = true
				sno = dbOldEntity.SerialNo
			}
		} else if argument.DeviceIMEI == "" && dbOldEntity.PhoneBrand != nil {
			phoneBrand := string(*dbOldEntity.PhoneBrand)
			if phoneBrand == argument.PhoneBrand {
				bIsOldDevice = true
				sno = dbOldEntity.SerialNo
			}
		}
	}, nil, sqlFilter)
	if err != nil && !s.sqlDatabase.IsNoRows(err) {
		return 0, business.NewError(errors.InternalError, fmt.Errorf(""))
	}

	if bIsOldDevice {
		dbEntity.SerialNo = sno
		_, err = s.sqlDatabase.UpdateSelectiveByPrimaryKey(dbEntity)
	} else {
		sno, err = s.sqlDatabase.InsertSelective(dbEntity)
	}
	if err != nil {
		return 0, business.NewError(errors.InternalError, fmt.Errorf(""))
	}

	return sno, nil
}

func (s *Data) CommitUsabilityFeedbackInfo(argument *doctor.UsabilityFeedBack) (uint64, business.Error) {
	dbEntity := &sqldb.UsabilityFeedBack{}
	dbEntity.CopyFrom(argument)

	sno, err := s.sqlDatabase.InsertSelective(dbEntity)
	if err != nil {
		return 0, business.NewError(errors.InternalError, err)
	}

	return sno, nil
}
