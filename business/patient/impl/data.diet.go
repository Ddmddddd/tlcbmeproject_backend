package impl

import (
	"strconv"
	"tlcbme_project/business"
	"tlcbme_project/data/entity/sqldb"
	"tlcbme_project/data/model/doctor"
	"tlcbme_project/errors"
)

func (s *Data) CommitThreeMeals(argument *doctor.DietRecordEx) (uint64, business.Error) {
	sqlAccess, err := s.sqlDatabase.NewAccess(true)
	if err != nil {
		return 0, business.NewError(errors.InternalError, err)
	}
	defer sqlAccess.Close()

	dbEntity := &sqldb.DietRecord{}
	dbEntity.CopyFromAppEx(argument)

	sno := argument.SerialNo
	if argument.SerialNo > 0 {
		_, err = sqlAccess.UpdateSelectiveByPrimaryKey(dbEntity)
		mdbEntity := &sqldb.DietContent{}
		mdbFilter := &sqldb.DietContentFilter{
			DietRecordSerialNo: argument.SerialNo,
		}
		sqlFilter := s.sqlDatabase.NewFilter(mdbFilter,false,false)
		_, err = sqlAccess.Delete(mdbEntity, sqlFilter)
		if err != nil {
			return 0, business.NewError(errors.InternalError, err)
		}
	} else {
		sno, err = sqlAccess.InsertSelective(dbEntity)
		if err != nil {
			return 0, business.NewError(errors.InternalError, err)
		}
	}
	list := argument.DietContents
	if len(list) > 0 {
		for _, v := range list {
			//如果是菜肴类型的查找菜肴组成转换成food列表插入
			//if v.NutritionSource == "recipe" {
			//	recipeEntity := &
			//}
			dbEntity2 := &sqldb.DietContent{}
			dbEntity2.CopyFrom(v)
			dbEntity2.DietRecordSerialNo = sno
			dbEntity2.PatientID = argument.PatientID
			_, err = sqlAccess.InsertSelective(dbEntity2)
			//因为如果前端删除某个食物后更新用餐记录，此方法并不会删除。故如果是更新，将原有记录content全部删除后再次添加
			//if dbEntity2.SerialNo > 0 {
			//	_, err = sqlAccess.UpdateSelectiveByPrimaryKey(dbEntity2)
			//} else {
			//	_, err = sqlAccess.InsertSelective(dbEntity2)
			//}
		}
	}

	err = sqlAccess.Commit()
	if err != nil {
		return 0, business.NewError(errors.InternalError, err)
	}

	return sno, nil
}

func (s *Data) 	GetMealContent (argument *doctor.DietContentFilter) ([]*doctor.DietContent, business.Error) {
	results := make([]*doctor.DietContent, 0)

	dbEntity := &sqldb.DietContent{}
	dbFilter := &sqldb.DietContentFilter{}
	dbFilter.CopyFrom(argument)
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	err := s.sqlDatabase.SelectList(dbEntity, func() {
		result := &doctor.DietContent{}
		dbEntity.CopyTo(result)
		results = append(results, result)
	}, nil, sqlFilter)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}

	return results, nil
}

func (s *Data) 	DeleteMealContent (argument *doctor.DietRecordFilter) (uint64, business.Error) {
	dbEntity := &sqldb.DietRecord{}
	dbFilter := &sqldb.DietRecordFilter{}
	dbFilter.CopyFrom(argument)
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	_, err := s.sqlDatabase.Delete(dbEntity, sqlFilter)
	if err != nil {
		return 0, business.NewError(errors.InternalError, err)
	}
	mdbEntity := &sqldb.DietContent{}
	mdbFilter := &sqldb.DietContentFilter{
		DietRecordSerialNo: argument.SerialNo,
	}
	msqlFilter := s.sqlDatabase.NewFilter(mdbFilter, false, false)
	_, err = s.sqlDatabase.Delete(mdbEntity, msqlFilter)
	if err != nil {
		return 0, business.NewError(errors.InternalError, err)
	}

	return 1, nil
}


func (s *Data) CommitTotalFood(argument *doctor.DietRecordTotalForApp) (uint64, business.Error) {
	sqlAccess, err := s.sqlDatabase.NewAccess(true)
	if err != nil {
		return 0, business.NewError(errors.InternalError, err)
	}
	defer sqlAccess.Close()

	MealArgument := &doctor.DietRecordEx{
		SerialNo: argument.DietSerialNo,
		EatDateTime: argument.EatDateTime,
		InputDateTime: argument.InputDateTime,
		DietType:argument.DietType,
		PatientID: argument.PatientID,
		DietContents: argument.DietContents,
	}

	sno,_ := s.CommitThreeMeals(MealArgument)

	var timePoint uint64
	switch argument.DietType {
	case "早餐":
		timePoint = 1
	case "中餐":
		timePoint = 2
	case "晚餐":
		timePoint = 3
	default:
		timePoint = 0
	}
	RecordArgumentEx := &doctor.DietRecordForAppEx{
		SerialNo: strconv.FormatUint(argument.RecordSerialNo, 10),
		PatientID: argument.PatientID,
		Kinds: "主食",
		Appetite: "100",
		Photo: argument.Photo,
		Memo: argument.Memo,
		HappenDateTime: argument.EatDateTime,
		HappenPlace: argument.Place,
		Type: timePoint,
		DietRecordNo: sno,
	}
	sno2,_ := s.CommitDietRecord(RecordArgumentEx)

	if argument.RecordSerialNo == 0 {
		sno3,_ := strconv.Atoi(sno2)
		TaskArgument := &doctor.ExpPatientTaskRecordInput{
			PatientID: argument.PatientID,
			TaskID: 0,
			TaskName: "拍照",
			ParentTaskName: &argument.DietType,
			CreateDateTime: argument.InputDateTime,
			Credit: 0,
			Record: struct {
				SportDietRecord interface{} `json:"sportDietRecord"`
			}{struct {
				SerialNo uint64 `json:"serialNo"`
			}{ SerialNo: uint64(sno3) } },
		}
		s.CommitTaskRecord(TaskArgument)
	}




	err = sqlAccess.Commit()
	if err != nil {
		return 0, business.NewError(errors.InternalError, err)
	}

	return sno, nil
}


func (s *Data) DeleteTotalFood(argument *doctor.DietRecordTotalDeleteForApp) (uint64, business.Error) {
	sqlAccess, err := s.sqlDatabase.NewAccess(true)
	if err != nil {
		return 0, business.NewError(errors.InternalError, err)
	}
	defer sqlAccess.Close()

	MealArgument := &doctor.DietRecordFilter{SerialNo: argument.DietSerialNo}
	s.DeleteMealContent(MealArgument)

	RecordArgumentEx := &doctor.DietRecordForAppEx{
		SerialNo: strconv.FormatUint(argument.RecordSerialNo, 10),
		PatientID: argument.PatientID,
	}
	s.DeleteDietRecord(RecordArgumentEx)

	err = sqlAccess.Commit()
	if err != nil {
		return 0, business.NewError(errors.InternalError, err)
	}

	return 0, nil
}