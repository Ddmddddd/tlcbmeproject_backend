package impl

import (
	"tlcbme_project/business"
	"tlcbme_project/data/entity/sqldb"
	"tlcbme_project/data/model/doctor"
	"tlcbme_project/data/model/manage"
	"tlcbme_project/errors"
	"tlcbme_project/server/config"
	"fmt"
	"github.com/ktpswjz/httpserver/types"
	"strings"
)

type dataManage struct {
	chBloodPressureRecord 	chan uint64
	chBloodGlucoseRecord  	chan uint64
	chDiscomfortRecord    	chan uint64
	chActionPlanRecord	    chan uint64
	chActionPlanRecordList	chan []uint64
}

func (s *Data) initManage() {
	s.chBloodPressureRecord = make(chan uint64, 1024)
	s.chBloodGlucoseRecord = make(chan uint64, 1024)
	s.chDiscomfortRecord = make(chan uint64, 1024)
	s.chActionPlanRecord = make(chan uint64, 1024)
	s.chActionPlanRecordList = make(chan []uint64, 1024)

	go func() {
		s.initBloodPressureRecord()
	}()
	go func() {
		s.initBloodGlucoseRecord()
	}()
	go func() {
		s.initDiscomfortRecord()
	}()
	go func() {
		s.initActionPlanRecord()
	}()
	go func() {
		s.initActionPlanRecordList()
	}()
}

func (s *Data) initBloodPressureRecord() {
	for {
		sno := <-s.chBloodPressureRecord

		s.UploadBloodMeasureData(sno)
	}
}

func (s *Data) initBloodGlucoseRecord() {
	for {
		sno := <-s.chBloodGlucoseRecord

		s.UploadBloodGlucoseData(sno)
	}
}

func (s *Data) initDiscomfortRecord() {
	for {
		sno := <-s.chDiscomfortRecord

		s.UploadDiscomfortData(sno)
	}
}

func (s *Data) initActionPlanRecord() {
	for {
		sno := <-s.chActionPlanRecord

		s.UploadActionPlanData(sno)
	}
}

func (s *Data) initActionPlanRecordList() {
	for {
		snos := <-s.chActionPlanRecordList

		s.UploadActionPlanListData(snos)
	}
}

func (s *Data) RegisterPatientAndDisease(patientID uint64) error {
	dbFilter := &sqldb.ViewManagedPatientIndexListFilter{}
	dbFilter.PatientID = &patientID
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	dbEntity := &sqldb.ViewManagedPatientIndex{}
	err := s.sqlDatabase.SelectOne(dbEntity, sqlFilter)
	if err != nil {
		return err
	}

	dataEntity := &doctor.ViewManagedPatientIndexEx{}
	dbEntity.CopyToEx(dataEntity, nil)

	argument := &manage.PatientInfoInput{}
	argument.PatientID = fmt.Sprint(dataEntity.PatientID)
	argument.Info.Name = dataEntity.PatientName
	argument.Info.Gender = dataEntity.SexText
	argument.Info.Birthday = dataEntity.DateOfBirth
	argument.Info.IdentityCardNumber = dataEntity.IdentityCardNumber

	dbFilter2 := &sqldb.PatientUserBaseInfoUserIdFilter{}
	dbFilter2.UserID = patientID
	sqlFilter2 := s.sqlDatabase.NewFilter(dbFilter2, false, false)
	dbEntity2 := &sqldb.PatientUserBaseInfo{}
	err = s.sqlDatabase.SelectOne(dbEntity2, sqlFilter2)
	if err != nil {
		return err
	}

	argument.BodyInfo.Wc = dbEntity2.Waistline
	argument.BodyInfo.Height = dbEntity2.Height
	argument.BodyInfo.Weight = dbEntity2.Weight

	providerType := config.ManagementProviderTypeHbp
	uri := s.cfg.Management.Providers.Hbp.Api.Uri.RegPatient

	result, err := s.client.ManageBusinessPost("患者注册", dataEntity.PatientID, providerType, uri, argument)
	manageClasses := strings.Split(dataEntity.ManageClass, ",")
	if err != nil {
		return err
	}
	if result.Code != 0 {
		return fmt.Errorf("%d-%s", result.Code, result.Message)
	}
	if len(manageClasses) > 0 {
		go func(patientID uint64, manageClasses []string) {
			s.registerDisease(patientID, manageClasses)
		}(dataEntity.PatientID, manageClasses)
	}
	return nil
}

func (s *Data) registerDisease(patientID uint64, manageClasses []string) business.Error {
	if len(manageClasses) < 1 {
		return business.NewError(errors.InternalError, fmt.Errorf("参数（manageClasses）为空"))
	}

	providerType := config.ManagementProviderTypeHbp
	uri := s.cfg.Management.Providers.Hbp.Api.Uri.RegDisease
	for _, manageClass := range manageClasses {
		argument := &manage.DiseaseInfoInput{}
		argument.PatientID = fmt.Sprint(patientID)
		argument.Info.Name = manageClass
		if manageClass == "高血压" {
			argument.Info.Code = 1
			providerType = config.ManagementProviderTypeHbp
		} else if manageClass == "糖尿病" {
			argument.Info.Code = 2
			providerType = config.ManagementProviderTypeDm

			dbFilter := &sqldb.ManagedPatientIndexFilterBase{
				PatientID: &patientID,
			}
			sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
			dbEntity := &sqldb.ManagedPatientIndexExt{}
			err := s.sqlDatabase.SelectOne(dbEntity, sqlFilter)
			if err == nil {
				ext := &doctor.ManagedPatientIndexExt{}
				dbEntity.CopyTo(ext)
				argument.Info.Type = ext.Dm.Type.Name
			}
		} else {
			continue
		}
		businessName := fmt.Sprintf("疾病注册-%s", manageClass)
		result, err := s.client.ManageBusinessPost(businessName, patientID, providerType, uri, argument)
		if err != nil {
			return business.NewError(errors.InternalError, err)
		}
		if result.Code != 0 {
			if result.Code == 4 {
				go func(userId uint64) {
					s.RegisterPatientAndDisease(userId)
				}(patientID)
			} else {
				return business.NewError(errors.InternalError, fmt.Errorf("%d-%s", result.Code, result.Message))
			}
		}
	}
	return nil
}

func (s *Data) RegisterDisease(patientID uint64) error {
	dbFilter := &sqldb.ViewManagedPatientIndexListFilter{}
	dbFilter.PatientID = &patientID
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	dbEntity := &sqldb.ViewManagedPatientIndex{}
	err := s.sqlDatabase.SelectOne(dbEntity, sqlFilter)
	if err != nil {
		return err
	}
	dataEntity := &doctor.ViewManagedPatientIndexEx{}
	dbEntity.CopyToEx(dataEntity, nil)
	manageClasses := strings.Split(dataEntity.ManageClass, ",")
	if len(manageClasses) > 0 {
		go func(patientID uint64, manageClasses []string) {
			s.registerDisease(patientID, manageClasses)
		}(dataEntity.PatientID, manageClasses)
	}
	if err != nil {
		return err
	}
	return nil
}

func (s *Data) uploadBloodMeasureData(dataEntity *sqldb.BloodPressureRecord) (int, error) {
	if dataEntity == nil {
		return 0, nil
	}

	argument := &manage.BloodPressureInput{}
	argument.PatientID = fmt.Sprint(dataEntity.PatientID)
	argument.Record.SerialNo = fmt.Sprint(dataEntity.SerialNo)
	argument.Record.SystolicPressure = dataEntity.SystolicPressure
	argument.Record.DiastolicPressure = dataEntity.DiastolicPressure
	argument.Record.HeartRate = dataEntity.HeartRate
	if dataEntity.MeasureDateTime != nil {
		measureDateTime := types.Time(*dataEntity.MeasureDateTime)
		argument.Record.MeasureDateTime = &measureDateTime
	}

	providerType := config.ManagementProviderTypeHbp
	uri := s.cfg.Management.Providers.Hbp.Api.Uri.UploadBloodPressure

	result, err := s.client.ManageBusinessPost("上传患者血压心率数据", dataEntity.PatientID, providerType, uri, argument)
	if err != nil {
		return 0, err
	}

	return result.Code, nil
}

func (s *Data) uploadBloodGlucoseData(dataEntity *sqldb.BloodGlucoseRecord) (int, error) {
	argument := &manage.BloodGlucoseInput{}
	argument.PatientID = fmt.Sprint(dataEntity.PatientID)
	argument.Record.SerialNo = fmt.Sprint(dataEntity.SerialNo)
	argument.Record.BloodGlucose = dataEntity.BloodGlucose
	if dataEntity.BloodKetone != nil {
		argument.Record.Other.BloodKetone = *dataEntity.BloodKetone
	}
	argument.Record.TimePoint = dataEntity.TimePoint
	if dataEntity.MeasureDateTime != nil {
		measureDateTime := types.Time(*dataEntity.MeasureDateTime)
		argument.Record.MeasureDateTime = &measureDateTime
	}
	argument.Record.BloodType = argument.Record.ToCode()

	providerType := config.ManagementProviderTypeDm
	uri := s.cfg.Management.Providers.Dm.Api.Uri.UploadBloodGlucose

	result, err := s.client.ManageBusinessPost("上传患者血糖数据", dataEntity.PatientID, providerType, uri, argument)
	if err != nil {
		return 0, err
	}

	return result.Code, nil
}

func (s *Data) uploadDiscomfortData(dataEntity *sqldb.DiscomfortRecord) (int, error) {
	sqlAccess, err := s.sqlDatabase.NewAccess(false)
	if err != nil {
		return 0, err
	}
	defer sqlAccess.Close()

	if dataEntity == nil {
		return 0, nil
	}

	argument := &manage.DiscomfortInput{}
	argument.PatientID = fmt.Sprint(dataEntity.PatientID)
	argument.Record.SerialNo = fmt.Sprint(dataEntity.SerialNo)
	argument.Record.Discomfort = strings.Split(dataEntity.Discomfort, ",")
	if dataEntity.HappenDateTime != nil {
		happenDateTime := types.Time(*dataEntity.HappenDateTime)
		argument.Record.HappenDateTime = &happenDateTime
	}

	dbEntity := &sqldb.ManagedPatientIndex{}
	dbFilter := &sqldb.ManagedPatientIndexFilterBase{PatientID: &dataEntity.PatientID}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)

	err = sqlAccess.SelectOne(dbEntity, sqlFilter)
	noManageIndex := false
	if err != nil {
		if sqlAccess.IsNoRows(err) {
			//return 0, business.NewError(errors.InternalError, fmt.Errorf("患者ID(%d)不存在", dbEntity.PatientID))
			noManageIndex = true
		} else {
			return 0, err
		}
	}
	providerType := config.ManagementProviderTypeUnknown
	uri := ""
	result := manage.Result{}
	if noManageIndex {
		providerType = config.ManagementProviderTypeHbp
		uri = s.cfg.Management.Providers.Hbp.Api.Uri.UploadDiscomfort
	} else {
		if strings.Contains(*dbEntity.ManageClass, "高血压") {
			providerType = config.ManagementProviderTypeHbp
			uri = s.cfg.Management.Providers.Hbp.Api.Uri.UploadDiscomfort
		} else if strings.Contains(*dbEntity.ManageClass, "糖尿病") {
			providerType = config.ManagementProviderTypeDm
			uri = s.cfg.Management.Providers.Dm.Api.Uri.UploadDiscomfort
		}
	}

	if providerType != config.ManagementProviderTypeUnknown {
		result, err := s.client.ManageBusinessPost("上传患者不适记录数据", dataEntity.PatientID, providerType, uri, argument)
		if err != nil {
			return 0, err
		}
		return result.Code, nil
	}
	return result.Code, nil
}

func (s *Data) UploadBloodMeasureData(serialNo uint64) (int, error) {
	dataFilter := &sqldb.BloodPressureRecordFilter{
		SerialNo: &serialNo,
	}
	dataEntity := &sqldb.BloodPressureRecord{}
	sqlFilter := s.sqlDatabase.NewFilter(dataFilter, false, false)
	err := s.sqlDatabase.SelectOne(dataEntity, sqlFilter)
	if err != nil {
		return 0, err
	}

	return s.uploadBloodMeasureData(dataEntity)
}

func (s *Data) UploadBloodGlucoseData(serialNo uint64) (int, error) {
	dataFilter := &sqldb.BloodGlucoseRecordFilter{
		SerialNo: &serialNo,
	}
	dataEntity := &sqldb.BloodGlucoseRecord{}
	sqlFilter := s.sqlDatabase.NewFilter(dataFilter, false, false)
	err := s.sqlDatabase.SelectOne(dataEntity, sqlFilter)
	if err != nil {
		return 0, err
	}

	return s.uploadBloodGlucoseData(dataEntity)
}

func (s *Data) UploadDiscomfortData(serialNo uint64) (int, error) {
	dataFilter := &sqldb.DiscomfortRecordFilter{
		SerialNo: &serialNo,
	}
	dataEntity := &sqldb.DiscomfortRecord{}
	sqlFilter := s.sqlDatabase.NewFilter(dataFilter, false, false)
	err := s.sqlDatabase.SelectOne(dataEntity, sqlFilter)
	if err != nil {
		return 0, err
	}

	return s.uploadDiscomfortData(dataEntity)
}

func (s *Data) UploadBloodMeasureDataAll(patientId uint64) (int, error) {
	count := int(0)
	dataFilter := &sqldb.BloodPressureRecordDataFilter{
		PatientID: patientId,
	}
	dataEntity := &sqldb.BloodPressureRecord{}
	sqlFilter := s.sqlDatabase.NewFilter(dataFilter, false, false)
	err := s.sqlDatabase.SelectList(dataEntity, func() {
		s.uploadBloodMeasureData(dataEntity)
		count++
	}, nil, sqlFilter)
	if err != nil {
		return 0, err
	}

	return count, nil
}

func (s *Data) UploadBloodGlucoseDataAll(patientId uint64) (int, error) {
	count := int(0)
	dataFilter := &sqldb.BloodGlucoseRecordFilter{
		PatientID: &patientId,
	}
	dataEntity := &sqldb.BloodGlucoseRecord{}
	sqlFilter := s.sqlDatabase.NewFilter(dataFilter, false, false)
	err := s.sqlDatabase.SelectList(dataEntity, func() {
		s.uploadBloodGlucoseData(dataEntity)
		count++
	}, nil, sqlFilter)
	if err != nil {
		return 0, err
	}

	return count, nil
}

func (s *Data) UploadDiscomfortDataAll(patientId uint64) (int, error) {
	count := int(0)
	dataFilter := &sqldb.DiscomfortRecordDataFilter{
		PatientID: patientId,
	}
	dataEntity := &sqldb.DiscomfortRecord{}
	sqlFilter := s.sqlDatabase.NewFilter(dataFilter, false, false)
	err := s.sqlDatabase.SelectList(dataEntity, func() {
		s.uploadDiscomfortData(dataEntity)
		count++
	}, nil, sqlFilter)
	if err != nil {
		return 0, err
	}

	return count, nil
}

func (s *Data) UploadActionPlanData(serialNo uint64) (int, error) {
	dataFilter := &sqldb.ExpActionPlanRecordFilter{
		SerialNo: &serialNo,
	}
	dataEntity := &sqldb.ExpActionPlanRecord{}
	sqlFilter := s.sqlDatabase.NewFilter(dataFilter, false, false)
	err := s.sqlDatabase.SelectOne(dataEntity, sqlFilter)
	if err != nil {
		return 0, err
	}

	return s.uploadActionPlanData(dataEntity)
}

func (s *Data) uploadActionPlanData(dataEntity *sqldb.ExpActionPlanRecord) (int, error) {
	sqlAccess, err := s.sqlDatabase.NewAccess(false)
	if err != nil {
		return 0, err
	}
	defer sqlAccess.Close()

	if dataEntity == nil {
		return 0, nil
	}

	argument := &manage.ActionPlanInput{}
	argument.PatientID = fmt.Sprint(dataEntity.PatientID)
	argument.ActionPlanRecord.SerialNo = fmt.Sprint(dataEntity.SerialNo)
	argument.ActionPlanRecord.SeqActionPlan = dataEntity.SeqActionPlan
	argument.ActionPlanRecord.Completion = dataEntity.Completion
	actionTime := types.Time(*dataEntity.CreateDateTime)
	argument.ActionPlanRecord.ActionTime = &actionTime

	providerType := config.ManagementProviderTypeDm
	uri := s.cfg.Management.Providers.Dm.Api.Uri.UploadActionPlanData

	result, err := s.client.ManageBusinessPost("上传患者生活计划数据", dataEntity.PatientID, providerType, uri, argument)
	if err != nil {
		return 0, err
	}
	return result.Code, nil
}

func (s *Data) UploadActionPlanListData(snos []uint64) (int, error) {
	results := make([]*doctor.ExpActionPlanRecord, 0)
	dataFilter := &sqldb.ExpActionPlanRecordInFilter{
		SerialNo: snos,
	}
	dataEntity := &sqldb.ExpActionPlanRecord{}
	sqlFilter := s.sqlDatabase.NewFilter(dataFilter, false, false)
	err := s.sqlDatabase.SelectList(dataEntity, func() {
		result := &doctor.ExpActionPlanRecord{}
		dataEntity.CopyTo(result)
		results = append(results, result)
	}, nil, sqlFilter)
	if err != nil {
		return 0, err
	}

	return s.uploadActionPlanListData(results)
}

func (s *Data) uploadActionPlanListData(results []*doctor.ExpActionPlanRecord) (int, error) {
	sqlAccess, err := s.sqlDatabase.NewAccess(false)
	if err != nil {
		return 0, err
	}
	defer sqlAccess.Close()

	if results == nil || len(results) == 0 {
		return 0, nil
	}

	argument := &manage.ActionPlanListInput{}
	argument.PatientID = fmt.Sprint(results[0].PatientID)
	actionPlans := make([]*manage.ActionPlanRecord, 0)
	for _, record := range results {
		actionPlan := &manage.ActionPlanRecord{}
		actionPlan.SerialNo = fmt.Sprint(record.SerialNo)
		actionPlan.Completion = record.Completion
		actionPlan.ActionTime = record.CreateDateTime
		actionPlan.SeqActionPlan = record.SeqActionPlan
		actionPlans = append(actionPlans, actionPlan)
	}
	argument.ActionPlanRecord = actionPlans

	providerType := config.ManagementProviderTypeDm
	uri := s.cfg.Management.Providers.Dm.Api.Uri.UploadActionPlanListData

	result, err := s.client.ManageBusinessPost("上传患者生活计划数据", results[0].PatientID, providerType, uri, argument)
	if err != nil {
		return 0, err
	}
	return result.Code, nil
}