package impl

import (
	"encoding/json"
	"fmt"
	"github.com/aliyun/alibaba-cloud-sdk-go/services/dysmsapi"
	"github.com/ktpswjz/httpserver/security/hash"
	"github.com/ktpswjz/httpserver/types"
	"io/ioutil"
	"math/rand"
	"net/http"
	"strconv"
	"strings"
	"time"
	"tlcbme_project/business"
	"tlcbme_project/business/patient/api"
	"tlcbme_project/data/entity/sqldb"
	"tlcbme_project/data/enum"
	"tlcbme_project/data/model"
	"tlcbme_project/data/model/doctor"
	"tlcbme_project/data/model/manage"
	"tlcbme_project/database"
	"tlcbme_project/database/memory"
	"tlcbme_project/errors"
	"tlcbme_project/server/config"
	"tlcbme_project/server/notify"
)

type Authentication struct {
	base

	passwordFormat uint64
}

func NewAuthentication(cfg *config.Config, log types.Log, sqlDatabase database.SqlDatabase, memoryToken memory.Token, passwordFormat uint64, notifyChannels notify.ChannelCollection, client business.Client) api.Authentication {
	instance := &Authentication{passwordFormat: passwordFormat}
	instance.SetLog(log)
	instance.client = client
	instance.sqlDatabase = sqlDatabase
	instance.memoryToken = memoryToken
	instance.notifyChannels = notifyChannels
	instance.cfg = cfg

	return instance
}

func (s *Authentication) Authenticate() func(account, password string) (uint64, error) {
	return s.authenticate
}

func (s *Authentication) Logined() func(userID uint64, token *model.Token) {
	return s.logined
}

func (s *Authentication) ValidateUser() func(userName string) (uint64, error) {
	return s.validateUser
}

func (s *Authentication) CreateUser() func(userInfoCreate *doctor.PatientUserInfoCreateForApp) (uint64, error) {
	return s.createUser
}
func (s *Authentication) RegisterPatient() func(patientID uint64) error {
	return s.registerPatient
}

func (s *Authentication) GetWeChatOpenID() func(filter *model.WeChatCodeFilter) (*doctor.WeChatCode2SessionResult, error) {
	return s.getWeChatOpenID
}

func (s *Authentication) GetPatientByOpenID() func(filter *model.WeChatLoginFilter) (*sqldb.PatientUserAuths, error) {
	return s.getPatientByOpenID
}

func (s *Authentication) AuditPatient() func(argument *doctor.PatientUserAuditEx) business.Error {
	return s.auditPatient
}

func (s *Authentication) CreateDietPlan() func(patientID uint64) business.Error {
	return s.createDietPlan
}

func (s *Authentication) SendCodeWithPhone() func(phone *model.SendCodePhone) (string, error) {
	return s.sendCodeWithPhone
}

func (s *Authentication) NewChangePasswordWithPhone() func(argument *model.NewChangePasswordWithPhone) (string, error) {
	return s.newChangePasswordWithPhone
}

func (s *Authentication) ChangePassword(userID uint64, oldPassword, newPassword string) business.Error {
	sqlAccess, err := s.sqlDatabase.NewAccess(false)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}
	defer sqlAccess.Close()

	user, err := s.getUserAuthByID(sqlAccess, userID)
	if err != nil {
		if s.sqlDatabase.IsNoRows(err) {
			business.NewError(errors.NotExist, err)
		} else {
			return business.NewError(errors.InternalError, err)
		}
	}

	// 验证旧密码
	dbPassword := user.Password
	if user.PasswordFormat == 0 {
		if dbPassword != oldPassword {
			return business.NewError(errors.InternalError, fmt.Errorf("旧密码不正确"))
		}
	} else {
		passwordHashed, err := hash.Hash(oldPassword, user.PasswordFormat)
		if err != nil {
			return business.NewError(errors.InternalError, err)
		}
		if strings.ToLower(passwordHashed) != strings.ToLower(dbPassword) {
			return business.NewError(errors.InternalError, fmt.Errorf("旧密码不正确"))
		}
	}

	// 设置新密码
	password := newPassword
	if s.passwordFormat != 0 {
		password, err = hash.Hash(newPassword, s.passwordFormat)
		if err != nil {
			return business.NewError(errors.Unknown, err)
		}
	}

	dbUpdate := &sqldb.PatientUserAuthsPasswordUpdate{
		Password:       password,
		PasswordFormat: s.passwordFormat,
	}
	dbFilter := &sqldb.PatientUserAuthsUserIdFilter{
		UserID: userID,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	_, err = s.sqlDatabase.Update(dbUpdate, sqlFilter)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}

	return nil
}

func (s *Authentication) ChangePhoneNumber(userID uint64, phoneNumber string) business.Error {
	sqlAccess, err := s.sqlDatabase.NewAccess(true)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}
	defer sqlAccess.Close()

	_, err = s.getUserAuthByID(sqlAccess, userID)
	if err != nil {
		if s.sqlDatabase.IsNoRows(err) {
			business.NewError(errors.NotExist, err)
		} else {
			return business.NewError(errors.InternalError, err)
		}
	}

	dbFilter := &sqldb.PatientUserAuthsFilter{
		MobilePhone: phoneNumber,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	dbEntity := &sqldb.PatientUserAuths{}
	err = sqlAccess.SelectOne(dbEntity, sqlFilter)
	if err != nil {
		if s.sqlDatabase.IsNoRows(err) {
			business.NewError(errors.InternalError, fmt.Errorf("该手机号已被绑定"))
		} else {
			return business.NewError(errors.InternalError, err)
		}
	}

	// 设置新手机号
	dbUpdatePatientUserAuths := &sqldb.PatientUserAuthsPhoneUpdate{
		UserID:      userID,
		MobilePhone: &phoneNumber,
	}
	_, err = sqlAccess.UpdateSelectiveByPrimaryKey(dbUpdatePatientUserAuths)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}

	// 同步到患者用户信息表
	dbUpdatePatientUserBaseInfo := &sqldb.PatientUserBaseInfoUpdatePhone{
		UserID: userID,
		Phone:  &phoneNumber,
	}
	_, err = sqlAccess.UpdateSelectiveByPrimaryKey(dbUpdatePatientUserBaseInfo)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}

	err = sqlAccess.Commit()
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}

	return nil
}

func (s *Authentication) authenticate(account, password string) (uint64, error) {
	dbEntity, err := s.getUserByAccount(account, nil)
	if err != nil {
		return 0, err
	}

	dbPassword := dbEntity.Password
	if dbEntity.PasswordFormat == 0 {
		if dbPassword != password {
			return 0, fmt.Errorf("密码错误")
		}
	} else {
		passwordHashed, err := hash.Hash(password, dbEntity.PasswordFormat)
		if err != nil {
			return 0, err
		}
		if strings.ToLower(passwordHashed) != strings.ToLower(dbPassword) {
			return 0, fmt.Errorf("密码错误")
		}
	}

	if dbEntity.Status == 1 {
		return 0, fmt.Errorf("账号已被冻结")
	} else if dbEntity.Status == 9 {
		return 0, fmt.Errorf("账号已注销")
	}

	now := time.Now()
	dbUpdate := &sqldb.PatientUserAuthsLoginUpdate{
		LastLoginDateTime: &now,
		LoginCount:        dbEntity.LoginCount + 1,
	}
	dbFilter := &sqldb.PatientUserAuthsUserIdFilter{
		UserID: dbEntity.UserID,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	s.sqlDatabase.Update(dbUpdate, sqlFilter)

	return dbEntity.UserID, nil
}

func (s *Authentication) logined(userID uint64, token *model.Token) {
	if userID > 0 {

	}
}

func (s *Authentication) getUserByAccount(account string, excludedId *uint64) (*sqldb.PatientUserAuths, error) {
	sqlFilters := make([]database.SqlFilter, 0)

	dbFilter := &sqldb.PatientUserBaseInfoAuthenticationFilter{
		UserName:    account,
		MobilePhone: account,
		Email:       account,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, true, false)
	sqlFilters = append(sqlFilters, sqlFilter)

	if excludedId != nil {
		dbFilter := &sqldb.PatientUserBaseInfoExcludedIdFilter{
			UserID: *excludedId,
		}
		sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
		sqlFilters = append(sqlFilters, sqlFilter)
	}

	dbEntity := &sqldb.PatientUserAuths{}
	err := s.sqlDatabase.SelectOne(dbEntity, sqlFilters...)
	if err != nil {
		if s.sqlDatabase.IsNoRows(err) {
			return nil, fmt.Errorf("账号'%s'不存在", account)
		}
		return nil, err
	}

	return dbEntity, nil
}

func (s *Authentication) validateUser(userName string) (uint64, error) {
	dbEntity := &sqldb.PatientUserAuths{UserName: userName}
	dbFilter := &sqldb.PatientUserAuthsFilter{UserName: userName}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	err := s.sqlDatabase.SelectOne(dbEntity, sqlFilter)
	if s.sqlDatabase.IsNoRows(err) {
		return 0, nil
	} else if err != nil {
		return 0, err
	}
	return 1, nil
}

func (s *Authentication) createUser(userInfoCreate *doctor.PatientUserInfoCreateForApp) (uint64, error) {
	if userInfoCreate == nil {
		return 0, fmt.Errorf("参数为空")
	}

	sqlAccess, err := s.sqlDatabase.NewAccess(true)
	if err != nil {
		return 0, err
	}
	defer sqlAccess.Close()

	userID, err := s.createUserAuth(sqlAccess, userInfoCreate)
	if err != nil {
		return 0, fmt.Errorf("创建账号失败: %s", err.Error())
	}
	_, err = s.createUserBasicInfo(sqlAccess, userInfoCreate, userID)
	if err != nil {
		return 0, fmt.Errorf("创建基本信息失败: %s", err.Error())
	}
	_, err = s.createUserManagementAppReview(sqlAccess, userInfoCreate, userID)
	if err != nil {
		return 0, fmt.Errorf("创建管理申请审核失败: %s", err.Error())
	}

	err = sqlAccess.Commit()
	if err != nil {
		return 0, err
	}

	s.WriteNotify(&notify.Message{
		BusinessID: notify.BusinessDoctor,
		NotifyID:   notify.DoctorAuditPatient,
		Time:       types.Time(time.Now()),
	})

	return userID, nil
}

func (s *Authentication) createUserAuth(sqlAccess database.SqlAccess, argument *doctor.PatientUserInfoCreateForApp) (uint64, error) {
	now := time.Now()
	phone := string(argument.PhoneNumber)
	dbEntity := &sqldb.PatientUserAuths{
		UserName:       argument.UserName,
		MobilePhone:    &phone,
		RegistDateTime: &now,
	}
	dbFilter := &sqldb.PatientUserAuthsFilter{
		UserName:    argument.UserName,
		MobilePhone: phone,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, true, false)
	err := sqlAccess.SelectOne(dbEntity, sqlFilter)
	if err != nil {
		if !sqlAccess.IsNoRows(err) {
			return 0, err
		}
	} else {
		if dbEntity.UserName == argument.UserName {
			return 0, fmt.Errorf("用户名%s已存在", argument.UserName)
		} else {
			return 0, fmt.Errorf("手机号为%s已被注册", phone)
		}
	}

	password := argument.Password
	passwordFormat := s.passwordFormat
	if passwordFormat != 0 {
		pwd, err := hash.Hash(password, passwordFormat)
		if err != nil {
			return 0, err
		}
		password = pwd
	}
	dbEntity.PasswordFormat = passwordFormat
	dbEntity.Password = password

	return sqlAccess.InsertSelective(dbEntity)
}

func (s *Authentication) createUserBasicInfo(sqlAccess database.SqlAccess, argument *doctor.PatientUserInfoCreateForApp, userID uint64) (uint64, error) {
	dbEntity := &sqldb.PatientUserBaseInfo{}
	dbEntity.CopyFromApp(argument)
	dbEntity.UserID = userID
	//dbFilter := &sqldb.PatientUserBaseInfoFilter{
	//	IdentityCardNumber: &argument.IdentityCardNumber,
	//}
	//sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	//err := sqlAccess.SelectOne(dbFilter, sqlFilter)
	//if err != nil {
	//	if !sqlAccess.IsNoRows(err) {
	//		return 0, err
	//	}
	//} else {
	//	return 0, fmt.Errorf("身份证号%s已被注册", argument.IdentityCardNumber)
	//}
	// 新增体重记录
	if argument.Weight > 0 {
		now := time.Now()
		dbEntityWeight := &sqldb.WeightRecord{
			PatientID:       userID,
			Weight:          argument.Weight,
			InputDateTime:   &now,
			MeasureDateTime: &now,
		}
		_, err := sqlAccess.InsertSelective(dbEntityWeight)
		if err != nil {
			return 0, err
		}
	}

	return sqlAccess.InsertSelective(dbEntity)
}

func (s *Authentication) createUserManagementAppReview(sqlAccess database.SqlAccess, argument *doctor.PatientUserInfoCreateForApp, userID uint64) (uint64, error) {
	visitOrgCode := string(argument.HospitalCode)
	healthManagerID := uint64(argument.HealthManagerID)
	applicationFrom := string(argument.SourceFrom)
	diagnosis := string(argument.Diagnosis)
	dbEntity := &sqldb.ManagementApplicationReview{
		Diagnosis:       &diagnosis,
		ApplicationFrom: &applicationFrom,
		VisitOrgCode:    &visitOrgCode,
		Status:          0,
	}
	if healthManagerID > 0 {
		dbEntity.HealthManagerID = &healthManagerID
	}
	dbEntity.PatientID = userID
	dbFilter := &sqldb.ManagementApplicationReviewFilter{
		PatientID: userID,
		Status:    0,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	err := sqlAccess.SelectOne(dbFilter, sqlFilter)
	if err != nil {
		if !sqlAccess.IsNoRows(err) {
			return 0, err
		}
	} else {
		return 0, fmt.Errorf("ID为%d的患者的管理申请审核已存在", userID)
	}

	return sqlAccess.InsertSelective(dbEntity)
}

func (s *Authentication) registerPatient(patientID uint64) error {
	dbFilter := &sqldb.PatientUserBaseInfoUserIdFilter{}
	dbFilter.UserID = patientID
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	dbEntity := &sqldb.PatientUserBaseInfo{}
	err := s.sqlDatabase.SelectOne(dbEntity, sqlFilter)
	if err != nil {
		return err
	}

	dataEntity := &doctor.PatientUserBaseInfo{}
	dbEntity.CopyTo(dataEntity)

	argument := &manage.PatientInfoInput{}
	argument.PatientID = fmt.Sprint(dataEntity.UserID)
	argument.Info.Name = dataEntity.Name
	switch *dataEntity.Sex {
	case 0:
		argument.Info.Gender = "未知"
	case 1:
		argument.Info.Gender = "男"
	case 2:
		argument.Info.Gender = "女"
	case 9:
		argument.Info.Gender = "未说明"
	}
	argument.Info.Birthday = dataEntity.DateOfBirth
	argument.Info.IdentityCardNumber = dataEntity.IdentityCardNumber
	argument.Info.Phone = dataEntity.Phone

	argument.BodyInfo.Weight = dbEntity.Weight
	argument.BodyInfo.Height = dbEntity.Height
	argument.BodyInfo.Wc = dbEntity.Waistline

	providerType := config.ManagementProviderTypeHbp
	uri := s.cfg.Management.Providers.Hbp.Api.Uri.RegPatient

	result, err := s.client.ManageBusinessPost("患者注册", dataEntity.UserID, providerType, uri, argument)
	if err != nil {
		return err
	}
	if result.Code != 0 {
		return fmt.Errorf("%d-%s", result.Code, result.Message)
	}
	return nil

}

func (s *Authentication) PatientBindWeChat(argument *doctor.PatientUserAuthsEx) (uint64, business.Error) {
	if argument == nil {
		return 0, business.NewError(errors.InternalError, fmt.Errorf("参数为空"))
	}

	sqlAccess, err := s.sqlDatabase.NewAccess(true)
	if err != nil {
		return 0, business.NewError(errors.InternalError, err)
	}
	defer sqlAccess.Close()

	_, err = s.getUserAuthByID(sqlAccess, argument.UserID)
	if err != nil {
		if sqlAccess.IsNoRows(err) {
			return 0, business.NewError(errors.InternalError, fmt.Errorf("患者ID(%d)不存在", argument.UserID))
		} else {
			return 0, business.NewError(errors.InternalError, err)
		}
	}

	dbFilter := &sqldb.PatientUserAuthsExFilter{
		UserID:       argument.UserID,
		IdentityType: argument.IdentityType,
		Identitier:   argument.Identitier,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)

	dbEntity := &sqldb.PatientUserAuthsEx{}
	err = s.sqlDatabase.SelectOne(dbEntity, sqlFilter)
	if err != nil {
		dbEntity.CopyFrom(argument)

		sno, err := sqlAccess.InsertSelective(dbEntity)
		if err != nil {
			return 0, business.NewError(errors.InternalError, err)
		}

		err = sqlAccess.Commit()
		if err != nil {
			return 0, business.NewError(errors.InternalError, err)
		}

		return sno, nil
	}

	dbUpdate := &sqldb.PatientUserAuthsExStatusUpdate{
		Status: "0",
	}
	sno, err := s.sqlDatabase.Update(dbUpdate, sqlFilter)
	if err != nil {
		return sno, business.NewError(errors.InternalError, err)
	}
	return sno, nil
}

func (s *Authentication) PatientReleaseWeChat(userID uint64, identityType string, identitier string) business.Error {

	dbUpdate := &sqldb.PatientUserAuthsExStatusUpdate{
		Status: strconv.FormatUint(enum.BindWeChatStatuses.Stop().Key, 10),
	}
	dbFilter := &sqldb.PatientUserAuthsExFilter{
		UserID:       userID,
		IdentityType: identityType,
		Identitier:   identitier,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)

	dbEntity := &sqldb.PatientUserAuthsEx{}
	err := s.sqlDatabase.SelectOne(dbEntity, sqlFilter)
	if err != nil {
		if s.sqlDatabase.IsNoRows(err) {
			return business.NewError(errors.LoginAccountNotExit, err)
		}
	}

	_, err = s.sqlDatabase.Update(dbUpdate, sqlFilter)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}

	return nil
}

func (s *Authentication) getWeChatOpenID(filter *model.WeChatCodeFilter) (*doctor.WeChatCode2SessionResult, error) {
	appid := ""
	appsecret := ""
	if filter.Env == enum.AppEnvs.Test().Key {
		//测试服app对应appid与appsecret
		appid = "wxe646133265a7d522"
		appsecret = "089b9c9fcfbc4fbd480c94901b134daa"
	} else if filter.Env == enum.AppEnvs.Zj().Key {
		//浙大健康小微
		appid = "wx49722946d26413a2"
		appsecret = "d7e9c2cce5811bf71ebaca9dc9317bd9"
	} else if filter.Env == enum.AppEnvs.Nx().Key {
		//慢病微管家
		appid = "wxaaba909bef38802e"
		appsecret = "bd7aabd279e0111365e67eefa6d1dc36"
	} else if filter.Env == enum.AppEnvs.WeightTest().Key {
		//体重项目测试服
		appid = "wxdd20aa2fb7c5967c"
		appsecret = "408df9d0604556f44e57a4ff157239b2"
	} else if filter.Env == enum.AppEnvs.Weight().Key {
		//体重项目
		appid = "wx02ad655a859f9031"
		appsecret = "ae2bfc5a50eddbc9025c958146f6c3c9"
	}
	res, err := http.Get("https://api.weixin.qq.com/sns/jscode2session?appid=" + appid + "&secret=" + appsecret +
		"&grant_type=authorization_code" + "&js_code=" + filter.Code)
	if err != nil {
		return nil, fmt.Errorf("与微信服务器连接失败！请稍后重试！")
	}

	defer res.Body.Close()
	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return nil, fmt.Errorf("内容解析失败！请稍后重试！")
	}
	result := &doctor.WeChatCode2SessionResult{}
	json.Unmarshal([]byte(body), &result)
	if result.ErrCode != 0 {
		return nil, fmt.Errorf(result.ErrMsg)
	}
	return result, nil
}

func (s *Authentication) getPatientByOpenID(filter *model.WeChatLoginFilter) (*sqldb.PatientUserAuths, error) {
	sqlFilters := make([]database.SqlFilter, 0)
	if filter != nil {
		dbFilter := &sqldb.WeChatLoginFilter{}
		dbFilter.CopyFrom(filter)
		sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
		sqlFilters = append(sqlFilters, sqlFilter)
	}

	dbEntity := &sqldb.PatientUserAuthsEx{}
	err := s.sqlDatabase.SelectOne(dbEntity, sqlFilters...)
	if err != nil {
		if s.sqlDatabase.IsNoRows(err) {
			return nil, fmt.Errorf("微信open-id:'%s'不存在或未绑定", filter.Identitier)
		}
		return nil, err
	}

	sqlUserFilters := make([]database.SqlFilter, 0)
	dbUserFilter := &sqldb.PatientUserAuthsUserIdFilter{
		UserID: dbEntity.UserID,
	}
	sqlUserFilter := s.sqlDatabase.NewFilter(dbUserFilter, true, false)
	sqlUserFilters = append(sqlUserFilters, sqlUserFilter)

	dbUserEntity := &sqldb.PatientUserAuths{}
	err = s.sqlDatabase.SelectOne(dbUserEntity, sqlUserFilters...)
	if err != nil {
		if s.sqlDatabase.IsNoRows(err) {
			return nil, fmt.Errorf("账号'%d'不存在", dbUserEntity.UserID)
		}
		return nil, err
	}

	now := time.Now()
	dbUpdate := &sqldb.PatientUserAuthsLoginUpdate{
		LastLoginDateTime: &now,
		LoginCount:        dbUserEntity.LoginCount + 1,
	}
	dbFilter := &sqldb.PatientUserAuthsUserIdFilter{
		UserID: dbUserEntity.UserID,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	s.sqlDatabase.Update(dbUpdate, sqlFilter)

	return dbUserEntity, nil
}

func (s *Authentication) ChangeNickname(userID uint64, nickname string) business.Error {
	sqlAccess, err := s.sqlDatabase.NewAccess(true)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}
	defer sqlAccess.Close()

	dbUpdate := &sqldb.PatientUserBaseInfoUpdateNickname{
		Nickname: &nickname,
	}
	dbFilter := &sqldb.PatientUserBaseInfoUserIdFilter{
		UserID: userID,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)

	_, err = sqlAccess.Update(dbUpdate, sqlFilter)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}

	err = sqlAccess.Commit()
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}

	return nil
}

func (s *Authentication) auditPatient(argument *doctor.PatientUserAuditEx) business.Error {
	if argument == nil {
		return business.NewError(errors.InputError, fmt.Errorf("参数为空"))
	}

	sqlAccess, err := s.sqlDatabase.NewAccess(true)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}
	defer sqlAccess.Close()

	dbEntityAuth := &sqldb.ManagementApplicationReviewAuth{}
	dbEntityAuth.CopyFrom(argument)
	count, err := sqlAccess.UpdateSelectiveByPrimaryKey(dbEntityAuth)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}
	if count != 1 {
		return business.NewError(errors.NotExist, fmt.Errorf("患者(id=%d)不存在", argument.PatientID))
	}

	// 审核状态 0-未审核，1-审核通过，2-审核不通过
	if argument.Status == 1 {
		dbEntity := &sqldb.ManagedPatientIndex{
			PatientID:           argument.PatientID,
			OrgCode:             &argument.ReviewerOrgCode,
			DoctorID:            argument.DoctorID,
			DoctorName:          &argument.DoctorName,
			HealthManagerID:     argument.HealthManagerID,
			HealthManagerName:   &argument.HealthManagerName,
			PatientActiveDegree: 1,
		}
		if len(argument.PatientFeatures) > 0 {
			features := strings.Join(argument.PatientFeatures, ",")
			dbEntity.PatientFeature = &features
		}

		now := time.Now()
		ext := &doctor.ManagedPatientIndexExt{}
		ext.Htn.ManageLevelStartDateTime = types.Time(now)
		ext.Htn.ManageLevel = 0
		ext.Dm.ManageLevelStartDateTime = types.Time(now)
		ext.Dm.ManageLevel = 0
		ext.Dm.Medications = make([]string, 0)

		classCount := len(argument.ManageClasses)
		if classCount > 0 {
			sb := strings.Builder{}
			dbEntityClass := &sqldb.ManagedPatientClass{
				PatientID: argument.PatientID,
			}
			for classIndex := 0; classIndex < classCount; classIndex++ {
				classItem := argument.ManageClasses[classIndex]
				dbEntityClass.ManageClassCode = classItem.ItemCode

				_, err := sqlAccess.InsertSelective(dbEntityClass)
				if err != nil {
					return business.NewError(errors.InternalError, err)
				}

				// 1-高血压, 2-糖尿病, 3-慢阻肺
				if classItem.ItemCode == 1 {
					dbEntityHtn := &sqldb.HtnManageDetail{
						PatientID:                argument.PatientID,
						ManageLevel:              0,
						ManageLevelStartDateTime: &now,
					}
					_, err := sqlAccess.InsertSelective(dbEntityHtn)
					if err != nil {
						return business.NewError(errors.InternalError, err)
					}
				} else if classItem.ItemCode == 2 {
					dbEntityDm := &sqldb.DmManageDetail{
						PatientID:                argument.PatientID,
						ManageLevel:              0,
						ManageLevelStartDateTime: &now,
					}
					_, err := sqlAccess.InsertSelective(dbEntityDm)
					if err != nil {
						return business.NewError(errors.InternalError, err)
					}

					ext.Dm.Type.Name = argument.DmType
					ext.Dm.Medications = argument.DmMedications
				}

				if classIndex > 0 {
					sb.WriteString(",")
				}
				sb.WriteString(classItem.ItemName)
			}

			classes := sb.String()
			dbEntity.ManageClass = &classes
		}

		extData, err := json.Marshal(ext)
		if err != nil {
			return business.NewError(errors.InternalError, err)
		}
		extJson := string(extData)

		dbEntity.ManageStartDateTime = &now
		dbEntity.ManageStatus = enum.ManageStatuses.InManagement().Key
		dbEntity.Ext = &extJson

		_, err = sqlAccess.InsertSelective(dbEntity)
		if err != nil {
			return business.NewError(errors.InternalError, err)
		}
	}
	err = sqlAccess.Commit()
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}

	return nil
}

func (s *Authentication) createDietPlan(patientID uint64) business.Error {
	plans := make([]*doctor.ExpDietPlanRepository, 0)
	dbEntity := &sqldb.ExpDietPlanRepository{}
	dbFilter := &sqldb.ExpDietPlanRepositoryNilFilter{}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	err := s.sqlDatabase.SelectList(dbEntity, func() {
		plan := &doctor.ExpDietPlanRepository{}
		dbEntity.CopyTo(plan)
		plans = append(plans, plan)
	}, nil, sqlFilter)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}

	t := time.Now()
	today := time.Date(t.Year(), t.Month(), t.Day(), 0, 0, 0, 0, time.Local)
	nextWeek := t.AddDate(0, 0, 7)
	nextWeekDay := time.Date(nextWeek.Year(), nextWeek.Month(), nextWeek.Day(), 0, 0, 0, 0, time.Local)
	dbPlanEntity := &sqldb.ExpPatientDietPlan{
		PatientID:      patientID,
		StartDate:      &today,
		EndDate:        &nextWeekDay,
		Level:          "中等",
		ChangeStatus:   1,
		CreateDateTime: &t,
	}
	for _, plan := range plans {
		dbPlanEntity.DietPlan = plan.Plan
		_, err := s.sqlDatabase.InsertSelective(dbPlanEntity)
		if err != nil {
			return business.NewError(errors.InternalError, err)
		}
	}

	return nil
}

func (s *Authentication) sendCodeWithPhone(phone *model.SendCodePhone) (string, error) {
	client, err := dysmsapi.NewClientWithAccessKey("cn-zhangjiakou", "LTAI5tP3gxPj3sDXzPKjcpzG", "TOgOg1lawOxKmpKdIzDkPJtCibDhUw")
	/* use STS Token
	client, err := dysmsapi.NewClientWithStsToken("cn-zhangjiakou", "<your-access-key-id>", "<your-access-key-secret>", "<your-sts-token>")
	*/

	request := dysmsapi.CreateSendSmsRequest()
	request.Scheme = "https"

	code := GenValidateCode(6)
	request.PhoneNumbers = phone.Phone
	request.SignName = "TLC体重管理助手"
	request.TemplateCode = "SMS_232161005"
	request.TemplateParam = "{\"code\":" + code + "}"

	response, err := client.SendSms(request)
	if err != nil {
		fmt.Print(err.Error())
	}
	fmt.Printf("response is %#v\n", response)
	return code, nil
}

func GenValidateCode(width int) string {
	numeric := [10]byte{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}
	r := len(numeric)
	rand.Seed(time.Now().UnixNano())

	var sb strings.Builder
	for i := 0; i < width; i++ {
		fmt.Fprintf(&sb, "%d", numeric[rand.Intn(r)])
	}
	return sb.String()
}

func (s *Authentication) newChangePasswordWithPhone(argument *model.NewChangePasswordWithPhone) (string, error) {
	data, err := s.validateUser(argument.UserName)
	if err != nil {
		return "该手机号不存在", err
	}
	if data != 1 {
		return "该手机号不存在", err
	}

	sqlAccess, err := s.sqlDatabase.NewAccess(false)
	if err != nil {
		return "未知错误", err
	}
	defer sqlAccess.Close()

	// 设置新密码
	password := argument.Password
	if s.passwordFormat != 0 {
		password, err = hash.Hash(argument.Password, s.passwordFormat)
		if err != nil {
			return "未知错误", err
		}
	}

	dbUpdate := &sqldb.PatientUserAuthsPasswordUpdate{
		Password:       password,
		PasswordFormat: s.passwordFormat,
	}
	dbFilter := &sqldb.PatientUserAuthsUserNameFilter{
		UserName: argument.UserName,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	_, err = s.sqlDatabase.Update(dbUpdate, sqlFilter)
	if err != nil {
		return "未知错误", err
	}

	return "成功", nil
}
