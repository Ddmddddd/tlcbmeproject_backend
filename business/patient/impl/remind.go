package impl

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"
	"time"
	"tlcbme_project/business"
	"tlcbme_project/data/entity/sqldb"
	"tlcbme_project/data/model/doctor"
	"tlcbme_project/errors"
)

func (s *Data) WxRemindCreate(argument *doctor.WeChatRemindTaskCreate) (uint64, business.Error) {
	sqlAccess, err := s.sqlDatabase.NewAccess(true)
	if err != nil {
		return 0, business.NewError(errors.InternalError, err)
	}
	defer sqlAccess.Close()

	dbEntity := &sqldb.WeChatRemindTask{}
	dbEntity.CopyFromCreate(argument)

	sno, err := sqlAccess.InsertSelective(dbEntity)
	if err != nil {
		return 0, business.NewError(errors.InternalError, err)
	}
	err = sqlAccess.Commit()
	if err != nil {
		return 0, business.NewError(errors.InternalError, err)
	}

	return sno, nil
}

type AccessToken struct {
	Token     string `json:"access_token" note:"获取到的凭证"`
	ExpiresIn uint64 `json:"expires_in" note:"凭证有效时间，单位：秒。目前是7200秒之内的值。"`
	ErrCode   uint64 `json:"errcode" note:"错误码"`
	ErrMsg    string `json:"errmsg" note:"错误信息"`
}

type Response struct {
	Errcode uint64 `json:"errcode" note:"错误码"`
	Errmsg  string `json:"errmsg" note:"错误信息"`
}

func (s *Data) GetWeChatAccessTokenWithExp() (*AccessToken, error) {
	appid := s.cfg.WeChatAppInfo.AppId
	appsecret := s.cfg.WeChatAppInfo.AppSecret

	res, err := http.Get("https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=" + appid + "&secret=" + appsecret)
	if err != nil {
		return nil, fmt.Errorf("与微信服务器连接失败！请稍后重试！")
	}
	defer res.Body.Close()
	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return nil, fmt.Errorf("内容解析失败！请稍后重试！")
	}
	result := AccessToken{}
	json.Unmarshal([]byte(body), &result)
	if result.ErrCode != 0 {
		return nil, fmt.Errorf(result.ErrMsg)
	}

	return &result, nil
}

func (s *Data) GetWeChatAccessToken() (string, error) {
	appid := s.cfg.WeChatAppInfo.AppId
	appsecret := s.cfg.WeChatAppInfo.AppSecret

	res, err := http.Get("https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=" + appid + "&secret=" + appsecret)
	if err != nil {
		return "", fmt.Errorf("与微信服务器连接失败！请稍后重试！")
	}
	defer res.Body.Close()
	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return "", fmt.Errorf("内容解析失败！请稍后重试！")
	}
	result := AccessToken{}
	json.Unmarshal([]byte(body), &result)
	if result.ErrCode != 0 {
		return "", fmt.Errorf(result.ErrMsg)
	}

	return result.Token, nil
}

func (s *Data) SubscribeMessageSend(token string, argument *doctor.WeChatRemindSend) business.Error {
	url := "https://api.weixin.qq.com/cgi-bin/message/subscribe/send?access_token=" + token
	res, code, err := s.client.Post(url, argument)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}

	result := Response{}
	if code == 200 {
		err = json.Unmarshal([]byte(res), &result)
		if err != nil {
			return business.NewError(errors.InternalError, err)
		}
	}

	now := time.Now()
	dbEntity := &sqldb.WeChatRemindRecord{}
	dbEntity.CopyFromSend(argument)
	response, err := json.Marshal(result)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}
	dbEntity.Response = string(response)
	dbEntity.SendDateTime = &now
	_, err = s.sqlDatabase.Insert(dbEntity)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}

	return nil
}

func (s *Data) SmsRemindCreate(argument *doctor.TimingTaskCreate) (uint64, business.Error) {
	sqlAccess, err := s.sqlDatabase.NewAccess(true)
	if err != nil {
		return 0, business.NewError(errors.InternalError, err)
	}
	defer sqlAccess.Close()

	dbOrgEntity := &sqldb.ManagedPatientIndex{}
	dbOrgFilter := &sqldb.ManagedPatientIndexPatientIDFilter{
		PatientID: argument.Patientid,
	}
	sqlOrgFilter := s.sqlDatabase.NewFilter(dbOrgFilter, false, false)
	sqlAccess.SelectOne(dbOrgEntity, sqlOrgFilter)

	argument.Operatorid = *dbOrgEntity.HealthManagerID
	argument.DoctorName = string(*dbOrgEntity.DoctorName)

	dbEntity := &sqldb.Timingtask{}
	dbEntity.CopyFromCreate(argument)

	sno, err := sqlAccess.InsertSelective(dbEntity)
	if err != nil {
		return 0, business.NewError(errors.InternalError, err)
	}
	err = sqlAccess.Commit()
	if err != nil {
		return 0, business.NewError(errors.InternalError, err)
	}

	return sno, nil
}

func (s *Data) PhotoFeedbackRemindCreate(argument *doctor.WeChatRemindTaskCreate) (uint64, business.Error) {
	dbEntity := &sqldb.WeChatRemindTask{}
	dbEntity.CopyFromCreate(argument)

	sno, err := s.sqlDatabase.InsertSelective(dbEntity)
	if err != nil {
		return 0, business.NewError(errors.InternalError, err)
	}

	return sno, nil
}

type Token struct {
	accessToken *AccessToken `json:"accessToken"`
	startTime   time.Time    `json:"startTime"`
}

var token *Token

func (s *Data) SendPhotoFeedbackRemind(argument *doctor.WeChatRemindTaskSend, sno uint64) business.Error {
	if !validateToken(token) {
		if token == nil {
			token = &Token{}
		}
		t, err := s.GetWeChatAccessTokenWithExp()
		if err != nil {
			return business.NewError(errors.Exception, err)
		}
		token.accessToken = t
		token.startTime = time.Now()
	}

	dbEntity := &sqldb.WeChatRemindTask{}
	dbFilter := &sqldb.WeChatRemindTaskBaseFilter{
		PatientID:  argument.PatientID,
		TemplateID: &argument.TemplateID,
		Status:     0,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	err := s.sqlDatabase.SelectOne(dbEntity, sqlFilter)
	if err != nil {
		if s.sqlDatabase.IsNoRows(err) {
			return nil
		}
		return business.NewError(errors.InternalError, err)
	}

	dbDictEntity := &sqldb.WeChatRemindDict{}
	dbDictFilter := &sqldb.WeChatRemindDictFilter{
		TemplateID: argument.TemplateID,
	}
	sqlDictFilter := s.sqlDatabase.NewFilter(dbDictFilter, false, false)
	err = s.sqlDatabase.SelectOne(dbDictEntity, sqlDictFilter)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}

	sendArgument := &doctor.WeChatRemindSend{
		PatientID:        dbEntity.PatientID,
		ReceiverOpenID:   *dbEntity.OpenID,
		TemplateID:       *dbEntity.TemplateID,
		Page:             s.setPage(*dbDictEntity.Page, sno),
		Data:             s.setData(dbDictEntity.Data),
		Lang:             *dbDictEntity.Lang,
		MiniprogramState: *dbDictEntity.MiniprogramState,
	}

	//clockId := "pSVHpuQgJesWJ7sVIK44zLAP1J79OTFRjOhFTKHm_ko"
	//if strings.EqualFold(argument.TemplateID, clockId) {
	//	sendArgument.Data = s.setData2(dbDictEntity.Data)
	//} else {
	//	sendArgument.Data = s.setData(dbDictEntity.Data)
	//}
	busErr := s.SubscribeMessageSend(token.accessToken.Token, sendArgument)
	if busErr != nil {
		return busErr
	}

	dbEntity.Status = 1
	_, err = s.sqlDatabase.UpdateByPrimaryKey(dbEntity)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}

	return nil
}

func (s *Data) SubscribeMessageSendJudge() business.Error {
	template := "pSVHpuQgJesWJ7sVIK44zLAP1J79OTFRjOhFTKHm_ko"
	patientList := make([]*sqldb.WeChatRemindTaskTemplatePatientID, 0)
	dbEntity := &sqldb.WeChatRemindTaskTemplatePatientID{}
	dbFilter := &sqldb.WeChatRemindTaskTemplateFilter{
		TemplateID: &template,
		Status:     0,
		Type:       4,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	err := s.sqlDatabase.SelectDistinct(dbEntity, func() {
		temp := &sqldb.WeChatRemindTaskTemplatePatientID{
			PatientID: dbEntity.PatientID,
		}
		patientList = append(patientList, temp)
	}, nil, sqlFilter)
	if err != nil {
		if s.sqlDatabase.IsNoRows(err) {
			return nil
		}
		return business.NewError(errors.InternalError, err)
	}

	for _, patient := range patientList {
		//判断是否要给这个用户发送提醒
		argument := &doctor.WeChatRemindTaskSend{
			PatientID:  patient.PatientID,
			TemplateID: template,
		}
		s.SendPhotoFeedbackRemind(argument, 0)
	}

	return nil
}

//验证token是否过期
func validateToken(t *Token) bool {
	if t == nil {
		return false
	}

	if t.startTime.Add(time.Duration(t.accessToken.ExpiresIn-100) * time.Second).Before(time.Now()) {
		fmt.Println("validateToken")
		return false
	}
	return true
}

type thing struct {
	Value string `json:"value"`
}

type Time struct {
	Value string `json:"value"`
}

type DataJson struct {
	Thing1 thing `json:"thing1"`
	Thing4 thing `json:"thing4"`
}

type DataJson2 struct {
	Thing1 thing `json:"thing1"`
	Thing4 thing `json:"thing4"`
	Thing6 thing `json:"thing6"`
	Time2  Time  `json:"time2"`
}

func (s *Data) setData(data string) interface{} {
	i := DataJson{}
	err := json.Unmarshal([]byte(data), &i)
	if err != nil {
		panic(err)
	}
	return i
}

func (s *Data) setData2(data string) interface{} {
	i := DataJson2{}
	err := json.Unmarshal([]byte(data), &i)
	if err != nil {
		panic(err)
	}
	return i
}

func (s *Data) setPage(page string, sno uint64) string {
	newPage := strings.Replace(page, "${commentId}", strconv.FormatUint(sno, 10), -1)
	return newPage
}
