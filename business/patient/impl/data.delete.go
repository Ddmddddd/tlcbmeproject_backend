package impl

import (
	"fmt"
	"strconv"
	"strings"
	"tlcbme_project/business"
	"tlcbme_project/data/entity/sqldb"
	"tlcbme_project/data/model/doctor"
	"tlcbme_project/data/model/manage"
	"tlcbme_project/errors"
	"tlcbme_project/server/config"
)

func (s *Data) DeleteBloodPressureRecord(argument *doctor.BloodPressureRecordForAppEx) business.Error {
	dbEntity := &sqldb.BloodPressureRecord{}
	dbEntity.CopyFromAppEx(argument)

	dbFilter := &sqldb.BloodPressureRecordFilter{SerialNo: &argument.SerialNo}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	_, err := s.sqlDatabase.Delete(dbEntity, sqlFilter)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}

	go func(patientId, sno uint64) {
		providerType := config.ManagementProviderTypeHbp
		uri := s.cfg.Management.Providers.Hbp.Api.Uri.DeleteRecord
		assess := &manage.DeleteBloodPressureRecordInput{
			PatientID: fmt.Sprint(patientId),
			SerialNo:  fmt.Sprint(sno),
		}
		s.client.ManageBusinessPost("删除患者血压数据", patientId, providerType, uri, assess)
	}(argument.PatientID, argument.SerialNo)

	return nil
}

func (s *Data) DeleteBloodGlucoseRecord(argument *doctor.BloodGlucoseRecordForAppEx) business.Error {
	dbEntity := &sqldb.BloodGlucoseRecord{}
	dbEntity.CopyFromAppEx(argument)

	dbFilter := &sqldb.BloodGlucoseRecordSerialNoFilter{SerialNo: &argument.SerialNo}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	_, err := s.sqlDatabase.Delete(dbEntity, sqlFilter)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}

	go func(patientId, sno uint64) {
		providerType := config.ManagementProviderTypeDm
		uri := s.cfg.Management.Providers.Dm.Api.Uri.DeleteRecord
		assess := &manage.DeleteBloodGlucoseRecordInput{
			PatientID: fmt.Sprint(patientId),
			SerialNo:  fmt.Sprint(sno),
		}
		s.client.ManageBusinessPost("删除患者血糖数据", patientId, providerType, uri, assess)
	}(argument.PatientID, argument.SerialNo)

	return nil
}

func (s *Data) DeleteWeightRecord(argument *doctor.WeightRecordForAppEx) business.Error {
	sqlAccess, err := s.sqlDatabase.NewAccess(true)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}
	defer sqlAccess.Close()

	if *argument.BloodGlucoseNo != 0 {
		gluArgument := &doctor.BloodGlucoseRecordForAppEx{
			PatientID: argument.PatientID,
			BloodGlucoseRecordForApp: doctor.BloodGlucoseRecordForApp{
				SerialNo: *argument.BloodGlucoseNo,
			},
		}
		s.DeleteBloodGlucoseRecord(gluArgument)
	}

	if *argument.BloodPressureNo != 0 {
		bpArgument := &doctor.BloodPressureRecordForAppEx{
			PatientID: argument.PatientID,
			BloodPressureRecordForApp: doctor.BloodPressureRecordForApp{
				SerialNo: *argument.BloodPressureNo,
			},
		}
		s.DeleteBloodPressureRecord(bpArgument)
	}

	dbEntity := &sqldb.WeightRecord{}
	dbEntity.CopyFromAppEx(argument)

	dbFilter := &sqldb.WeightRecordFilter{SerialNo: &argument.SerialNo}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	_, err = s.sqlDatabase.Delete(dbEntity, sqlFilter)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}

	err = sqlAccess.Commit()
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}
	return nil
}

func (s *Data) DeleteDrugRecord(argument *doctor.DrugRecordForAppEx) business.Error {
	sqlAccess, err := s.sqlDatabase.NewAccess(true)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}
	defer sqlAccess.Close()

	var sno uint64
	dbEntity := &sqldb.DrugUseRecord{}
	if len(argument.SerialNo) > 0 {
		arrSNO := strings.Split(argument.SerialNo, ",")
		for _, v := range arrSNO {
			sno, _ = strconv.ParseUint(v, 10, 64)
			dbEntity.SerialNo = sno
			dbFilter := &sqldb.DrugRecordFilter{SerialNo: &sno}
			sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
			_, err := sqlAccess.Delete(dbEntity, sqlFilter)
			if err != nil {
				return business.NewError(errors.InternalError, err)
			}
		}
	}

	err = sqlAccess.Commit()
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}

	return nil
}

func (s *Data) DeleteDietRecord(argument *doctor.DietRecordForAppEx) business.Error {
	sqlAccess, err := s.sqlDatabase.NewAccess(true)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}
	defer sqlAccess.Close()

	var sno uint64
	dbEntity := &sqldb.SportDietRecord{}
	if len(argument.SerialNo) > 0 {
		arrSNO := strings.Split(argument.SerialNo, ",")
		for _, v := range arrSNO {
			sno, _ = strconv.ParseUint(v, 10, 64)
			dbEntity.SerialNo = sno
			dbFilter := &sqldb.SportDietRecordFilter{SerialNo: &sno}
			sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
			_, err = sqlAccess.Delete(dbEntity, sqlFilter)
			if err != nil {
				return business.NewError(errors.InternalError, err)
			}
			//删除饮食评论任务
			sqlString := `delete from ExpPatientTaskRecord where Record->'$.sportDietRecord.serialNo' = ?`
			row := sqlAccess.QueryRow(sqlString, sno)
			err = row.Scan()
			if err != nil && !s.sqlDatabase.IsNoRows(err) {
				return business.NewError(errors.InternalError, err)
			}
		}
	}

	err = sqlAccess.Commit()
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}

	return nil
}

func (s *Data) DeleteSportRecord(argument *doctor.SportRecordForAppEx) business.Error {
	dbEntity := &sqldb.SportDietRecord{}
	dbEntity.CopyFromSportRecord(argument)

	dbFilter := &sqldb.SportDietRecordFilter{SerialNo: &argument.SerialNo}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	_, err := s.sqlDatabase.Delete(dbEntity, sqlFilter)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}

	return nil
}

func (s *Data) DeleteDiscomfortRecord(argument *doctor.DiscomfortRecordForAppEx) business.Error {
	dbEntity := &sqldb.DiscomfortRecord{}
	dbEntity.CopyFromAppEx(argument)

	sqlAccess, err := s.sqlDatabase.NewAccess(false)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}
	defer sqlAccess.Close()

	dbFilter := &sqldb.DiscomfortRecordFilter{SerialNo: &argument.SerialNo}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)

	_, err = sqlAccess.Delete(dbEntity, sqlFilter)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}

	dbEntity2 := &sqldb.ManagedPatientIndex{}
	dbFilter2 := &sqldb.ManagedPatientIndexFilterBase{PatientID: &argument.PatientID}
	sqlFilter2 := s.sqlDatabase.NewFilter(dbFilter2, false, false)
	err = sqlAccess.SelectOne(dbEntity2, sqlFilter2)
	noManageIndex := false
	if err != nil {
		if sqlAccess.IsNoRows(err) {
			//return 0, business.NewError(errors.InternalError, fmt.Errorf("患者ID(%d)不存在", dbEntity.PatientID))
			noManageIndex = true
		} else {
			return business.NewError(errors.InternalError, err)
		}
	}

	providerType := config.ManagementProviderTypeUnknown
	uri := ""
	if noManageIndex {
		providerType = config.ManagementProviderTypeHbp
		uri = s.cfg.Management.Providers.Hbp.Api.Uri.DeleteDiscomfort
	} else {
		if strings.Contains(*dbEntity2.ManageClass, "高血压") {
			providerType = config.ManagementProviderTypeHbp
			uri = s.cfg.Management.Providers.Hbp.Api.Uri.DeleteDiscomfort
		} else if strings.Contains(*dbEntity2.ManageClass, "糖尿病") {
			providerType = config.ManagementProviderTypeDm
			uri = s.cfg.Management.Providers.Dm.Api.Uri.DeleteDiscomfort
		}
	}

	info := &manage.DeleteDiscomfortInput{
		PatientID: fmt.Sprint(argument.PatientID),
		DataId:    fmt.Sprint(argument.SerialNo),
	}
	if providerType != config.ManagementProviderTypeUnknown {
		_, err := s.client.ManageBusinessPost("删除患者不适记录数据", argument.PatientID, providerType, uri, info)
		if err != nil {
			return business.NewError(errors.InternalError, err)
		}
		return nil
	}
	return nil

}
