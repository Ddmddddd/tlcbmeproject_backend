package impl

import (
	"fmt"
	"github.com/ktpswjz/httpserver/types"
	"strconv"
	"time"
	"tlcbme_project/business"
	"tlcbme_project/data/entity/sqldb"
	"tlcbme_project/data/model/doctor"
	"tlcbme_project/errors"
)

func (s *Data) ListBloodPressureRecord(filter *doctor.BloodPressureRecordDataFilterEx) ([]*doctor.BloodPressureRecordForApp, business.Error) {
	results := make([]*doctor.BloodPressureRecordForApp, 0)

	dbEntity := &sqldb.BloodPressureRecord{}
	dbFilter := &sqldb.BloodPressureRecordDataFilter{}
	dbFilter.CopyFrom(filter)
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	dbOrder := &sqldb.BloodPressureRecordDataOrder{}

	err := s.sqlDatabase.SelectList(dbEntity, func() {
		result := &doctor.BloodPressureRecordForApp{}
		dbEntity.CopyToApp(result)
		results = append(results, result)
	}, dbOrder, sqlFilter)
	if err != nil {
		return results, business.NewError(errors.InternalError, err)
	}

	return results, nil
}

func (s *Data) ListBloodGlucoseRecord(filter *doctor.BloodGlucoseRecordDataFilterEx) ([]*doctor.BloodGlucoseRecordForApp, business.Error) {
	results := make([]*doctor.BloodGlucoseRecordForApp, 0)

	dbEntity := &sqldb.BloodGlucoseRecord{}
	dbFilter := &sqldb.BloodGlucoseRecordDataFilter{}
	dbFilter.CopyFrom(filter)
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	dbOrder := &sqldb.BloodGlucoseRecordOrder{}

	err := s.sqlDatabase.SelectList(dbEntity, func() {
		result := &doctor.BloodGlucoseRecordForApp{}
		dbEntity.CopyToApp(result)
		results = append(results, result)
	}, dbOrder, sqlFilter)
	if err != nil {
		return results, business.NewError(errors.InternalError, err)
	}

	return results, nil
}

func (s *Data) ListWeightRecord(filter *doctor.WeightRecordDataFilterEx) ([]*doctor.WeightRecordForApp, business.Error) {
	//原本用weightRecord的历史记录 改成用viewWeightPage

	results := make([]*doctor.WeightRecordForApp, 0)
	dbEntity := &sqldb.WeightRecord{}
	dbFilter := &sqldb.WeightRecordDataFilter{}
	dbFilter.CopyFrom(filter)
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	dbOrder := &sqldb.WeightRecordDataOrder{}
	err := s.sqlDatabase.SelectList(dbEntity, func() {
		result := &doctor.WeightRecordForApp{}
		dbEntity.CopyToApp(result)
		results = append(results, result)
	}, dbOrder, sqlFilter)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}
	dbOder2 := &sqldb.WeightRecordDataDescOrder{}
	dbFilter2 := &sqldb.WeightRecordDataLowFilter{}
	dbFilter2.CopyFrom(filter)
	sqlFilter2 := s.sqlDatabase.NewFilter(dbFilter2, false, false)
	value := float64(0)
	if filter.MeasureStartDate != nil {
		err = s.sqlDatabase.SelectPage(dbEntity, func(total, page, size, index uint64) {

		}, func() {
			value = dbEntity.Weight
		}, 1, 1, dbOder2, sqlFilter2)
		if err != nil {
			return nil, business.NewError(errors.InternalError, err)
		}
	}
	for index, v := range results {
		if index > 0 {
			difference := v.Weight - results[index-1].Weight
			v.Difference = &difference
			if difference > 0 {
				v.IsUp = true
			}
		}
	}
	if len(results) > 0 {
		if value > 0 {
			difference := results[0].Weight - value
			results[0].Difference = &difference
			if difference > 0 {
				results[0].IsUp = true
			}

		}
	}
	return results, nil
}

func (s *Data) ListWeightBloodRecord(filter *doctor.ViewWeightPageDataFilterEx) ([]*doctor.ViewWeightPage, business.Error) {
	//原本用weightRecord的历史记录 改成用viewWeightPage
	results := make([]*doctor.ViewWeightPage, 0)
	dbEntity := &sqldb.ViewWeightPage{}
	dbFilter := &sqldb.ViewWeightPageDataFilter{}
	dbFilter.CopyFrom(filter)
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	dbOrder := &sqldb.ViewWeightPageDataOrder{}
	err := s.sqlDatabase.SelectList(dbEntity, func() {
		result := &doctor.ViewWeightPage{}
		dbEntity.CopyTo(result)
		results = append(results, result)
	}, dbOrder, sqlFilter)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}
	return results, nil
}

func (s *Data) ListDrugRecord(filter *doctor.DrugRecordDataFilterEx) ([]*doctor.DrugRecordForApp, business.Error) {
	results := make([]*doctor.DrugRecordForApp, 0)

	sqlAccess, err := s.sqlDatabase.NewAccess(false)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}
	defer sqlAccess.Close()

	dbEntity := &sqldb.DrugRecordDataEx{}
	dbFilter := &sqldb.DrugRecordDataFilter{}
	dbFilter.CopyFrom(filter)
	sqlString := "SELECT group_concat(SerialNo) AS SerialNo,group_concat(DrugName) AS DrugName,group_concat(Dosage) AS Dosage,min(TimePoint) As TimePoint,min(Memo) AS Memo,UseDateTime FROM DrugUseRecord WHERE PatientId = ? AND UseDateTime >= ? AND UseDateTime < ? group by UseDateTime order by UseDateTime ASC"
	rows, err := sqlAccess.Query(sqlString, filter.PatientID, dbFilter.UseStartDate, dbFilter.UseEndDate)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}
	defer rows.Close()

	for rows.Next() {
		err = rows.Scan(&dbEntity.SerialNo, &dbEntity.DrugName, &dbEntity.Dosage, &dbEntity.TimePoint, &dbEntity.Memo, &dbEntity.UseDateTime)
		if err != nil {
			return nil, business.NewError(errors.InternalError, err)
		}
		result := &doctor.DrugRecordForApp{}
		dbEntity.CopyToApp(result)
		results = append(results, result)
	}

	return results, nil
}

func (s *Data) ListDietRecord(filter *doctor.SportDietRecordDataFilterEx) ([]*doctor.DietRecordForApp, business.Error) {
	results := make([]*doctor.DietRecordForApp, 0)

	sqlAccess, err := s.sqlDatabase.NewAccess(false)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}
	defer sqlAccess.Close()

	dbEntity := &sqldb.DietRecordDataEx{}
	dbFilter := &sqldb.SportDietRecordDataFilter{}
	dbFilter.CopyFrom(filter)
	dbFilter.RecordType = "饮食"
	sqlString := "SELECT group_concat(SerialNo) AS SerialNo,group_concat(ItemName) AS ItemName,group_concat(ItemValue) AS ItemValue,min(TimePoint) As TimePoint, min(Photo) AS photo, min(Memo) AS Memo,HappenDateTime, HappenPlace, DietRecordNo FROM SportDietRecord WHERE PatientId = ? AND HappenDateTime >= ? AND HappenDateTime < ? AND RecordType = ? group by HappenDateTime order by HappenDateTime ASC"
	rows, err := sqlAccess.Query(sqlString, filter.PatientID, dbFilter.HappenStartDate, dbFilter.HappenEndDate, dbFilter.RecordType)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}
	defer rows.Close()

	for rows.Next() {
		err = rows.Scan(&dbEntity.SerialNo, &dbEntity.Kinds, &dbEntity.Appetite, &dbEntity.TimePoint, &dbEntity.Photo, &dbEntity.Memo, &dbEntity.HappenDateTime, &dbEntity.HappenPlace, &dbEntity.DietRecordNo)
		if err != nil {
			return nil, business.NewError(errors.InternalError, err)
		}

		result := &doctor.DietRecordForApp{}
		dbEntity.CopyToApp(result)
		results = append(results, result)
	}

	return results, nil
}

func (s *Data) GetDietRecord(filter *doctor.SportDietRecordFilter) (*doctor.DietRecordForApp, business.Error) {
	dbEntity := &sqldb.DietRecordDataEx{}
	dbFilter := &sqldb.SportDietRecordFilter{
		SerialNo: &filter.SerialNo,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)

	err := s.sqlDatabase.SelectOne(dbEntity, sqlFilter)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}

	result := &doctor.DietRecordForApp{}
	dbEntity.CopyToApp(result)

	return result, nil
}

func (s *Data) ListSportRecord(filter *doctor.SportDietRecordDataFilterEx) ([]*doctor.SportRecordForApp, business.Error) {
	results := make([]*doctor.SportRecordForApp, 0)

	dbEntity := &sqldb.SportDietRecord{}
	dbFilter := &sqldb.SportDietRecordDataFilter{}
	dbFilter.CopyFrom(filter)
	dbFilter.RecordType = "运动"
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	dbOrder := &sqldb.SportDietRecordDataOrder{}
	err := s.sqlDatabase.SelectList(dbEntity, func() {
		result := &doctor.SportRecordForApp{}
		dbEntity.CopyToSportRecord(result)
		results = append(results, result)
	}, dbOrder, sqlFilter)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}

	return results, nil
}

func (s *Data) ListDiscomfortRecord(filter *doctor.DiscomfortRecordDataFilterEx) ([]*doctor.DiscomfortRecordForApp, business.Error) {
	results := make([]*doctor.DiscomfortRecordForApp, 0)

	dbEntity := &sqldb.DiscomfortRecord{}
	dbFilter := &sqldb.DiscomfortRecordDataFilter{}
	dbFilter.CopyFrom(filter)
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	dbOrder := &sqldb.DiscomfortRecordDataOrder{}

	err := s.sqlDatabase.SelectList(dbEntity, func() {
		result := &doctor.DiscomfortRecordForApp{}
		dbEntity.CopyToApp(result)
		results = append(results, result)
	}, dbOrder, sqlFilter)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}

	return results, nil
}

func (s *Data) ListStepRecord(filter *doctor.SportDietRecordDataFilterEx) ([]*doctor.StepRecordData, business.Error) {
	results := make([]*doctor.StepRecordData, 0)

	dbEntity := &sqldb.StepRecordData{}
	dbFilter := &sqldb.StepRecordDataFilter{}
	dbFilter.CopyFrom(filter)
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	dbOrder := &sqldb.SportDietRecordDataOrder{}

	err := s.sqlDatabase.SelectList(dbEntity, func() {
		result := &doctor.StepRecordData{}
		dbEntity.CopyTo(result)
		results = append(results, result)
	}, dbOrder, sqlFilter)

	if err != nil {
		return results, business.NewError(errors.InternalError, err)
	}

	return results, nil
}

func (s *Data) OneDayStepRecord(filter *doctor.SportDietRecordDataFilterEx) (*doctor.SportRecordForApp, business.Error) {
	result := &doctor.SportRecordForApp{}

	dbEntity := &sqldb.SportDietRecord{}
	dbFilter := &sqldb.StepRecordDataFilter{}
	dbFilter.CopyFrom(filter)
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)

	err := s.sqlDatabase.SelectOne(dbEntity, sqlFilter)
	if s.sqlDatabase.IsNoRows(err) {
		return nil, nil
	} else if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}

	dbEntity.CopyToSportRecord(result)
	return result, nil
}

func (s *Data) TodayRecord(argument *doctor.PatientInfoBase) (*doctor.UserRecordCollection, business.Error) {
	todayTime := time.Now()
	todayDate := time.Date(todayTime.Year(), todayTime.Month(), todayTime.Day(), 0, 0, 0, 0, todayTime.Location())
	startDate := types.Time(todayDate)
	endDate := types.Time(todayDate)
	todayRecords := &doctor.UserRecordCollection{}

	bpRecordDataFilterEx := &doctor.BloodPressureRecordDataFilterEx{
		PatientID: argument.PatientID,
		BloodPressureRecordDataFilter: doctor.BloodPressureRecordDataFilter{
			MeasureStartDate: &startDate,
			MeasureEndDate:   &endDate,
		},
	}
	bpRecords, businessErr := s.ListBloodPressureRecord(bpRecordDataFilterEx)
	if businessErr != nil {
		return nil, businessErr
	}
	todayRecords.BloodPressureRecords = bpRecords

	weightRecordDataFilterEx := &doctor.WeightRecordDataFilterEx{
		PatientID: argument.PatientID,
		WeightRecordDataFilter: doctor.WeightRecordDataFilter{
			MeasureStartDate: &startDate,
			MeasureEndDate:   &endDate,
		},
	}
	weightRecords, businessErr := s.ListWeightRecord(weightRecordDataFilterEx)
	if businessErr != nil {
		return nil, businessErr
	}
	todayRecords.WeightRecords = weightRecords

	drugRecordDataFilterEx := &doctor.DrugRecordDataFilterEx{
		PatientID: argument.PatientID,
		DrugRecordDataFilter: doctor.DrugRecordDataFilter{
			UseStartDate: &startDate,
			UseEndDate:   &endDate,
		},
	}
	drugRecords, businessErr := s.ListDrugRecord(drugRecordDataFilterEx)
	if businessErr != nil {
		return nil, businessErr
	}
	todayRecords.DrugRecords = drugRecords

	dietRecordDataFilterEx := &doctor.SportDietRecordDataFilterEx{
		PatientID: argument.PatientID,
		SportDietRecordDataFilter: doctor.SportDietRecordDataFilter{
			HappenStartDate: &startDate,
			HappenEndDate:   &endDate,
		},
	}
	dietRecords, businessErr := s.ListDietRecord(dietRecordDataFilterEx)
	if businessErr != nil {
		return nil, businessErr
	}
	todayRecords.DietRecords = dietRecords

	sportRecordDataFilterEx := &doctor.SportDietRecordDataFilterEx{
		PatientID: argument.PatientID,
		SportDietRecordDataFilter: doctor.SportDietRecordDataFilter{
			HappenStartDate: &startDate,
			HappenEndDate:   &endDate,
		},
	}
	sportRecords, businessErr := s.ListSportRecord(sportRecordDataFilterEx)
	if businessErr != nil {
		return nil, businessErr
	}
	todayRecords.SportRecords = sportRecords

	discomfortRecordDataFilterEx := &doctor.DiscomfortRecordDataFilterEx{
		PatientID: argument.PatientID,
		DiscomfortRecordDataFilter: doctor.DiscomfortRecordDataFilter{
			HappenStartDate: &startDate,
			HappenEndDate:   &endDate,
		},
	}
	discomfortRecords, businessErr := s.ListDiscomfortRecord(discomfortRecordDataFilterEx)
	if businessErr != nil {
		return nil, businessErr
	}
	todayRecords.DiscomfortRecords = discomfortRecords

	bloodGlucoseRecordDataFilterEx := &doctor.BloodGlucoseRecordDataFilterEx{
		PatientID: argument.PatientID,
		BloodGlucoseRecordDataFilter: doctor.BloodGlucoseRecordDataFilter{
			MeasureStartDate: &startDate,
			MeasureEndDate:   &endDate,
		},
	}
	bloodGlucoseRecords, businessErr := s.ListBloodGlucoseRecord(bloodGlucoseRecordDataFilterEx)
	if businessErr != nil {
		return nil, businessErr
	}
	todayRecords.BloodGlucoseRecords = bloodGlucoseRecords

	return todayRecords, nil
}

func (s *Data) MonthlyReport(filter *doctor.ViewMonthlyReportFilterEx) (*doctor.MonthlyReportForApp, business.Error) {
	result := &doctor.MonthlyReportForApp{}

	dbEntity := &sqldb.ViewMonthlyReport{}
	dbFilter := &sqldb.ViewMonthlyReportFilter{}
	dbFilter.CopyFrom(filter)
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)

	err := s.sqlDatabase.SelectOne(dbEntity, sqlFilter)
	if s.sqlDatabase.IsNoRows(err) {
		return nil, nil
	} else if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}

	viewMonthlyReport := &doctor.ViewMonthlyReport{}
	dbEntity.CopyTo(viewMonthlyReport)
	result.ViewMonthlyReport = *viewMonthlyReport

	year, _ := strconv.Atoi(filter.YearMonth[0:4])
	month, _ := strconv.Atoi(filter.YearMonth[5:7])
	monthlyReportDateInfo := doctor.ParseMonthlyReportDateInfo(year, month)
	startDate := types.Time(*monthlyReportDateInfo.StartDate)
	endDate := types.Time(*monthlyReportDateInfo.EndDate)

	sbpLine := make([]doctor.RecordDataPoint, 0)
	dbpLine := make([]doctor.RecordDataPoint, 0)
	hrLine := make([]doctor.RecordDataPoint, 0)
	bmiLine := make([]doctor.RecordDataPoint, 0)

	if viewMonthlyReport.BpMeasuredTimes > 0 {
		bpRecordDataFilterEx := &doctor.BloodPressureRecordDataFilterEx{
			PatientID: filter.PatientID,
			BloodPressureRecordDataFilter: doctor.BloodPressureRecordDataFilter{
				MeasureStartDate: &startDate,
				MeasureEndDate:   &endDate,
			},
		}
		bpRecords, businessErr := s.ListBloodPressureRecord(bpRecordDataFilterEx)
		if businessErr != nil {
			return nil, businessErr
		}

		index := float64(0)
		totalSBP := bpRecords[0].SystolicPressure
		totalDBP := bpRecords[0].DiastolicPressure
		bpCount := uint64(1)
		var totalHR, hrCount uint64
		if bpRecords[0].HeartRate != nil {
			totalHR = uint64(*bpRecords[0].HeartRate)
			if totalHR > 0 {
				hrCount = 1
			}
		}
		mDate := bpRecords[0].MeasureDateTime.String()[0:10]
		for i := 1; i < len(bpRecords); i++ {
			bpRecord := bpRecords[i]
			sbp := bpRecord.SystolicPressure
			dbp := bpRecord.DiastolicPressure
			var hr uint64
			if bpRecord.HeartRate != nil {
				hr = uint64(*bpRecord.HeartRate)
			}
			tempDate := bpRecord.MeasureDateTime.String()[0:10]
			if tempDate == mDate {
				totalSBP += sbp
				totalDBP += dbp
				bpCount++
				if hr > 0 {
					totalHR += hr
					hrCount++
				}
			} else {
				avgSBP := totalSBP / bpCount
				avgDBP := totalDBP / bpCount
				sbpPoint := doctor.RecordDataPoint{Index: index, Value: float64(avgSBP), Date: mDate}
				dbpPoint := doctor.RecordDataPoint{Index: index, Value: float64(avgDBP), Date: mDate}
				sbpLine = append(sbpLine, sbpPoint)
				dbpLine = append(dbpLine, dbpPoint)
				if hrCount > 0 {
					avgHR := totalHR / hrCount
					hrPoint := doctor.RecordDataPoint{Index: index, Value: float64(avgHR), Date: mDate}
					hrLine = append(hrLine, hrPoint)
				}

				index++
				totalSBP = sbp
				totalDBP = dbp
				bpCount = 1
				totalHR = hr
				if hr > 0 {
					hrCount = 1
				}
				mDate = tempDate
			}
		}

		// last day
		lastAvgSBP := totalSBP / bpCount
		lastAvgDBP := totalDBP / bpCount
		lastSbpPoint := doctor.RecordDataPoint{Index: index, Value: float64(lastAvgSBP), Date: mDate}
		lastDbpPoint := doctor.RecordDataPoint{Index: index, Value: float64(lastAvgDBP), Date: mDate}
		sbpLine = append(sbpLine, lastSbpPoint)
		dbpLine = append(dbpLine, lastDbpPoint)
		if hrCount > 0 {
			lastAvgHR := totalHR / hrCount
			lastHrPoint := doctor.RecordDataPoint{Index: index, Value: float64(lastAvgHR), Date: mDate}
			hrLine = append(hrLine, lastHrPoint)
		}
	}
	result.SBPLine = sbpLine
	result.DBPLine = dbpLine
	result.HRLine = hrLine

	if viewMonthlyReport.WeightMeasuredTimes > 0 {
		weightRecordDataFilterEx := &doctor.WeightRecordDataFilterEx{
			PatientID: filter.PatientID,
			WeightRecordDataFilter: doctor.WeightRecordDataFilter{
				MeasureStartDate: &startDate,
				MeasureEndDate:   &endDate,
			},
		}
		weightRecords, businessErr := s.ListWeightRecord(weightRecordDataFilterEx)
		if businessErr != nil {
			return nil, businessErr
		}

		index := float64(0)
		tempHeight := float64(*viewMonthlyReport.CurrentHeight) / 100
		totalWeight := weightRecords[0].Weight
		weightCount := uint64(1)
		mDate := weightRecords[0].MeasureDateTime.String()[0:10]
		for i := 1; i < len(weightRecords); i++ {
			weightRecord := weightRecords[i]
			weight := weightRecord.Weight
			tempDate := weightRecord.MeasureDateTime.String()[0:10]
			if tempDate == mDate {
				totalWeight += weight
				weightCount++
			} else {
				tempWeight := totalWeight / float64(weightCount)
				tempBMI := tempWeight / tempHeight / tempHeight
				bmi, _ := strconv.ParseFloat(fmt.Sprintf("%.2f", tempBMI), 64)
				bmiPoint := doctor.RecordDataPoint{Index: index, Value: bmi, Date: mDate}
				bmiLine = append(bmiLine, bmiPoint)

				index++
				totalWeight = weight
				weightCount = 1
				mDate = tempDate
			}
		}

		// last day
		lastWeight := totalWeight / float64(weightCount)
		lastTempBMI := lastWeight / tempHeight / tempHeight
		lastBMI, _ := strconv.ParseFloat(fmt.Sprintf("%.2f", lastTempBMI), 64)
		lastBMIPoint := doctor.RecordDataPoint{Index: index, Value: lastBMI, Date: mDate}
		bmiLine = append(bmiLine, lastBMIPoint)
	}
	result.BMILine = bmiLine

	return result, nil
}

func (s *Data) GetIntegral(filter *doctor.ChatMsgPatientID) (*doctor.Integral, business.Error) {
	if filter == nil {
		return nil, business.NewError(errors.InternalError, fmt.Errorf("filter nil"))
	}
	dbEntity := &sqldb.ExpIntegral{}
	dbFilter := &sqldb.ExpIntegralPatientFilter{
		PatientID: filter.PatientID,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	result := &doctor.Integral{}

	err := s.sqlDatabase.SelectList(dbEntity, func() {
		if dbEntity.Reason == "三餐打卡" {
			result.Diet += dbEntity.Integral
		} else if dbEntity.Reason == "晨起称重" {
			result.Weigh += dbEntity.Integral
		} else if dbEntity.Reason == "每日运动" || dbEntity.Reason == "微信步数" {
			result.Sport += dbEntity.Integral
		} else {
			result.Other += dbEntity.Integral
		}
	}, nil, sqlFilter)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}
	return result, nil
}

func (s *Data) GetGradeRule(orgCode string) (*doctor.ExpIntegralRule, business.Error) {
	if len(orgCode) == 0 {
		return nil, business.NewError(errors.InternalError, fmt.Errorf("orgCode empty"))
	}
	dbEntity := &sqldb.ExpIntegralRule{}
	dbFilter := &sqldb.ExpIntegralRuleFilter{
		OrgCode: orgCode,
	}

	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	result := &doctor.ExpIntegralRule{}

	err := s.sqlDatabase.SelectOne(dbEntity, sqlFilter)
	if err != nil {
		if s.sqlDatabase.IsNoRows(err) {
			return result, nil
		}
		return result, business.NewError(errors.InternalError, err)
	}
	dbEntity.CopyTo(result)
	return result, nil
}

func (s *Data) UpdateGradeRule(rule *doctor.ExpIntegralRule) (uint64, business.Error) {
	if rule == nil {
		return 0, business.NewError(errors.InternalError, fmt.Errorf("rule nil"))
	}

	dbEntity := &sqldb.ExpIntegralRule{}
	dbEntity.CopyFrom(rule)
	if rule.SerialNo > 0 {
		data, err := s.sqlDatabase.UpdateByPrimaryKey(dbEntity)
		if err != nil {
			return data, business.NewError(errors.InternalError, err)
		}
		return data, nil
	} else {
		data, err := s.sqlDatabase.InsertSelective(dbEntity)
		if err != nil {
			return data, business.NewError(errors.InternalError, err)
		}
		return data, nil
	}
}
