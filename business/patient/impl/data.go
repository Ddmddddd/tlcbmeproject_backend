package impl

import (
	"crypto/aes"
	"crypto/cipher"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"github.com/ktpswjz/httpserver/types"
	"strconv"
	"strings"
	"time"
	"tlcbme_project/business"
	"tlcbme_project/business/patient/api"
	"tlcbme_project/data/entity/sqldb"
	"tlcbme_project/data/model/doctor"
	"tlcbme_project/data/model/manage"
	"tlcbme_project/database"
	"tlcbme_project/database/memory"
	"tlcbme_project/errors"
	"tlcbme_project/server/config"
	"tlcbme_project/server/notify"
)

type Data struct {
	base
	dataManage
}

func NewData(cfg *config.Config, log types.Log, sqlDatabase database.SqlDatabase, nSqlDatabase database.SqlDatabase, memoryToken memory.Token, client business.Client, notifyChannels notify.ChannelCollection) api.Data {
	instance := &Data{}
	instance.cfg = cfg
	instance.SetLog(log)
	instance.sqlDatabase = sqlDatabase
	instance.nSqlDatabase = nSqlDatabase
	instance.memoryToken = memoryToken
	instance.client = client
	instance.notifyChannels = notifyChannels

	instance.initManage()

	return instance
}

func (s *Data) CreateBloodPressureRecord(argument *doctor.BloodPressureRecord) (uint64, business.Error) {
	if argument == nil {
		return 0, business.NewError(errors.InternalError, fmt.Errorf("参数为空"))
	}

	sqlAccess, err := s.sqlDatabase.NewAccess(true)
	if err != nil {
		return 0, business.NewError(errors.InternalError, err)
	}
	defer sqlAccess.Close()

	_, err = s.getUserAuthByID(sqlAccess, argument.PatientID)
	if err != nil {
		if sqlAccess.IsNoRows(err) {
			return 0, business.NewError(errors.InternalError, fmt.Errorf("患者ID(%d)不存在", argument.PatientID))
		} else {
			return 0, business.NewError(errors.InternalError, err)
		}
	}

	dbEntity := &sqldb.BloodPressureRecord{}
	dbEntity.CopyFrom(argument)

	sno, err := sqlAccess.InsertSelective(dbEntity)
	if err != nil {
		return 0, business.NewError(errors.InternalError, err)
	}
	measureDateTime := types.Time(*dbEntity.MeasureDateTime)
	go func(patientId uint64, dataType string, time *types.Time) {
		s.uploadDataToPlatform(patientId, dataType, true, false, time)
	}(dbEntity.PatientID, "血压测量", &measureDateTime)

	err = sqlAccess.Commit()
	if err != nil {
		return 0, business.NewError(errors.InternalError, err)
	}

	go func(sn uint64) {
		s.chBloodPressureRecord <- sn
	}(sno)

	s.WriteNotify(&notify.Message{
		BusinessID: notify.BusinessDoctor,
		NotifyID:   notify.DoctorUpdateBloodPressure,
		Time:       types.Time(time.Now()),
		Data:       argument.PatientID,
	})

	return sno, nil
}

func (s *Data) StatBloodPressureRecord(filter *doctor.BloodPressureRecordDataFilterEx) (*doctor.BloodPressureRecordDataStat, business.Error) {
	result := &doctor.BloodPressureRecordDataStat{
		Min:   nil,
		Max:   nil,
		Items: make([]*doctor.BloodPressureRecordData, 0),
	}

	dbEntity := &sqldb.BloodPressureRecordData{}
	dbFilter := &sqldb.BloodPressureRecordDataFilter{}
	dbFilter.CopyFrom(filter)
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	dbOrder := &sqldb.BloodPressureRecordDataOrder{}

	err := s.sqlDatabase.SelectList(dbEntity, func() {
		item := &doctor.BloodPressureRecordData{}
		dbEntity.CopyTo(item)
		result.Items = append(result.Items, item)

		if result.Min == nil {
			result.Min = &doctor.BloodPressureRecordData{}
			dbEntity.CopyTo(result.Min)
		} else {
			result.Min.GetMin(item)
		}

		if result.Max == nil {
			result.Max = &doctor.BloodPressureRecordData{}
			dbEntity.CopyTo(result.Max)
		} else {
			result.Max.GetMax(item)
		}
	}, dbOrder, sqlFilter)

	if err != nil {
		return result, business.NewError(errors.InternalError, err)
	}

	return result, nil
}

func (s *Data) PatientLogined(argument *doctor.PatientInfoBase) (*doctor.LoginedDataForApp, business.Error) {
	result := &doctor.LoginedDataForApp{}
	managementPlan := &result.ManagementPlan
	loginUseInfo := &result.LoginUserInfo

	err := s.loadManagementPlan(argument.PatientID, managementPlan)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}

	dbPatientBaseEntity := &sqldb.PatientUserBaseInfo{}
	dbPatientFilter := &sqldb.PatientUserBaseInfoUserIdFilter{UserID: argument.PatientID}
	sqlPatientFilter := s.sqlDatabase.NewFilter(dbPatientFilter, false, false)
	err = s.sqlDatabase.SelectOne(dbPatientBaseEntity, sqlPatientFilter)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}
	dbPatientBaseEntity.CopyToApp(loginUseInfo)

	dbAuthsEntity := &sqldb.PatientUserAuths{}
	dbAuthsFilter := &sqldb.PatientUserAuthsUserIdFilter{UserID: argument.PatientID}
	sqlAuthsFilter := s.sqlDatabase.NewFilter(dbAuthsFilter, false, false)
	err = s.sqlDatabase.SelectOne(dbAuthsEntity, sqlAuthsFilter)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}
	if dbAuthsEntity.RegistDateTime != nil {
		loginUseInfo.UserName = dbAuthsEntity.UserName
		registDateTime := types.Time(*dbAuthsEntity.RegistDateTime)
		loginUseInfo.RegistDate = &registDateTime
	}

	dbAuthsExEntity := &sqldb.PatientUserAuthsEx{}
	dbAuthsExFilter := &sqldb.PatientUserAuthsExFilter{UserID: argument.PatientID, IdentityType: "宁医大预约挂号"}
	sqlAuthsExFilter := s.sqlDatabase.NewFilter(dbAuthsExFilter, false, false)
	err = s.sqlDatabase.SelectOne(dbAuthsExEntity, sqlAuthsExFilter)
	if err == nil {
		loginUseInfo.UUID = string(*dbAuthsExEntity.Credential)
	} else if !s.sqlDatabase.IsNoRows(err) {
		return nil, business.NewError(errors.InternalError, err)
	}

	todayRecords, buinessErr := s.TodayRecord(argument)
	if buinessErr != nil {
		return nil, buinessErr
	}
	result.TodayRecords = *todayRecords

	chatDoctorList, businessErr := s.GetChatDoctorList(argument)
	if businessErr != nil {
		return nil, buinessErr
	}
	result.ChatDoctorList = chatDoctorList

	dietPlanChangeList, businessErr := s.GetChangedDietPlan(argument.PatientID)
	if businessErr != nil {
		return nil, buinessErr
	}
	result.DietPlanChangeList = dietPlanChangeList

	return result, nil
}

func (s *Data) loadManagementPlan(patientID uint64, managementPlan *doctor.ManagementPlanForApp) error {
	managementPlanSourceGXY := &doctor.ManagementPlanSource{}
	dbManagementPlanEntityGXY := &sqldb.ManagementPlan{}
	dbFilterGXY := &sqldb.ManagementPlanFilter{
		PatientID: &patientID,
		PlanType:  "高血压",
		Statuses:  []uint64{1},
	}
	sqlFilterGXY := s.sqlDatabase.NewFilter(dbFilterGXY, false, false)
	errGXY := s.sqlDatabase.SelectOne(dbManagementPlanEntityGXY, sqlFilterGXY)
	if errGXY != nil || len(dbManagementPlanEntityGXY.Content) <= 0 {
		//return fmt.Errorf("查询出错")
		managementPlan.InitializeDefault()
	} else {
		errGXY = json.Unmarshal([]byte(dbManagementPlanEntityGXY.Content), managementPlanSourceGXY)
		if errGXY != nil {
			return errGXY
		}
		managementPlan.InitializeFrom(managementPlanSourceGXY)
	}

	managementPlanSourceTNB := &doctor.ManagementPlanSource{}
	dbManagementPlanEntityTNB := &sqldb.ManagementPlan{}
	dbFilterTNB := &sqldb.ManagementPlanFilter{
		PatientID: &patientID,
		PlanType:  "糖尿病",
		Statuses:  []uint64{1},
	}
	sqlFilterTNB := s.sqlDatabase.NewFilter(dbFilterTNB, false, false)
	errTNB := s.sqlDatabase.SelectOne(dbManagementPlanEntityTNB, sqlFilterTNB)
	if errTNB == nil && len(dbManagementPlanEntityTNB.Content) > 0 {
		errTNB = json.Unmarshal([]byte(dbManagementPlanEntityTNB.Content), managementPlanSourceTNB)
		if errTNB != nil {
			return errTNB
		}
		if s.sqlDatabase.IsNoRows(errGXY) {
			managementPlan.InitializeFrom(managementPlanSourceTNB)
		} else {
			managementPlan.AddGLUPlan(managementPlanSourceTNB)
		}
	}

	return nil
}

func (s *Data) CheckAppUpdate(argument *doctor.AppVersionRecordFromUser) (*doctor.AppVersionRecord, business.Error) {
	if argument == nil {
		return nil, business.NewError(errors.InternalError, fmt.Errorf("参数为空"))
	}

	dbAppVerEntity := &sqldb.AppVersionHistory{}
	dbAppVerFilter := &sqldb.AppVersionHistoryCodeFilter{VersionCode: argument.VersionCode}
	sqlAppVerFilter := s.sqlDatabase.NewFilter(dbAppVerFilter, false, false)

	err := s.sqlDatabase.SelectOne(dbAppVerEntity, sqlAppVerFilter)
	if s.sqlDatabase.IsNoRows(err) {
		return nil, business.NewError(errors.NotSupport, fmt.Errorf(""))
	} else if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}

	dbEntity := &sqldb.UserVersionRecord{}
	dbFilter := &sqldb.UserVersionRecordUserFilter{UserID: argument.PatientID}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)

	err = s.sqlDatabase.SelectOne(dbEntity, sqlFilter)
	if s.sqlDatabase.IsNoRows(err) {
		dbEntity.UserID = argument.PatientID
		dbEntity.VersionCode = dbAppVerEntity.VersionCode
		dbEntity.VersionName = dbAppVerEntity.VersionName
		_, err = s.sqlDatabase.InsertSelective(dbEntity)
	} else if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	} else if dbEntity.VersionCode < argument.VersionCode {
		dbEntity.VersionCode = dbAppVerEntity.VersionCode
		dbEntity.VersionName = dbAppVerEntity.VersionName
		_, err = s.sqlDatabase.UpdateSelectiveByPrimaryKey(dbEntity)
	}
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}

	dbNewestAppVerEntity := &sqldb.AppVersionRecord{}
	sqlNewestAppVerEntity := s.sqlDatabase.NewEntity()
	sqlNewestAppVerEntity.Parse(dbNewestAppVerEntity)
	sqlBuilder := s.sqlDatabase.NewBuilder()
	sqlBuilder.Select(sqlNewestAppVerEntity.ScanFields(), false).From(sqlNewestAppVerEntity.Name())
	sqlBuilder.Append("WHERE VersionCode > ? order by VersionCode desc LIMIT 1")
	query := sqlBuilder.Query()

	sqlAccess, err := s.sqlDatabase.NewAccess(true)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}
	defer sqlAccess.Close()

	row := sqlAccess.QueryRow(query, argument.VersionCode)
	err = row.Scan(sqlNewestAppVerEntity.ScanArgs()...)
	if s.sqlDatabase.IsNoRows(err) {
		return nil, business.NewError(errors.NoVersionUpdateChecked, fmt.Errorf(""))
	} else if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}

	result := &doctor.AppVersionRecord{}
	dbNewestAppVerEntity.CopyTo(result)

	return result, nil
}

func (s *Data) ListDivision(argument *doctor.DivisionBaseInfo) ([]*doctor.DivisionBaseInfo, business.Error) {
	results := make([]*doctor.DivisionBaseInfo, 0)

	sqlAccess, err := s.sqlDatabase.NewAccess(false)
	if err != nil {
		return results, business.NewError(errors.InternalError, err)
	}
	defer sqlAccess.Close()

	dbEntity := &sqldb.DivisionBaseInfo{}
	var sqlString = ""
	if argument.ItemCode == "" {
		sqlString = "SELECT DISTINCT ItemCode,ItemName,FullName FROM OrgDict INNER JOIN DivisionDict ON DivisionCode = ItemCode WHERE OrgDict.IsValid = 1"
	} else {
		sqlString = "SELECT DISTINCT ItemCode,ItemName,FullName FROM OrgDict INNER JOIN DivisionDict ON DivisionCode = ItemCode WHERE OrgDict.IsValid = 1 and DivisionDict.ParentDivisionCode = "
		sqlString = strings.Join([]string{sqlString, argument.ItemCode}, "")
	}
	rows, err := sqlAccess.Query(sqlString)
	if err != nil {
		return results, business.NewError(errors.InternalError, err)
	}
	defer rows.Close()

	for rows.Next() {
		err = rows.Scan(&dbEntity.ItemCode, &dbEntity.ItemName, &dbEntity.FullName)
		if err != nil {
			return results, business.NewError(errors.InternalError, err)
		}

		result := &doctor.DivisionBaseInfo{}
		dbEntity.CopyTo(result)
		results = append(results, result)
	}

	return results, nil
}

func (s *Data) ListOrgAndDoctor(argument *doctor.OrgAndDoctorFilter) ([]*doctor.OrgAndDoctorInfo, business.Error) {
	results := make([]*doctor.OrgAndDoctorInfo, 0)

	sqlAccess, err := s.sqlDatabase.NewAccess(false)
	if err != nil {
		return results, business.NewError(errors.InternalError, err)
	}
	defer sqlAccess.Close()

	dbOrgEntity := &sqldb.OrgDict{}
	dbOrgFilter := &sqldb.OrgDictDivisionCodeFilter{DivisionCode: argument.DivisionCode, IsValid: 1}
	sqlOrgFilter := s.sqlDatabase.NewFilter(dbOrgFilter, false, false)

	err = s.sqlDatabase.SelectList(dbOrgEntity, func() {
		result := &doctor.OrgAndDoctorInfo{}
		result.OrgCode = dbOrgEntity.OrgCode
		result.OrgName = dbOrgEntity.OrgName
		results = append(results, result)
	}, nil, sqlOrgFilter)
	if err != nil {
		return results, business.NewError(errors.InternalError, err)
	}

	for _, v := range results {
		doctors := make([]doctor.DoctorUserAuthsBaseInfo, 0)
		dbDoctorEntity := &sqldb.DoctorUserAuthsBaseInfo{}
		sqlString := "SELECT DISTINCT v.UserID AS UserID,d.UserName AS UserName,i.`Name` AS `Name` FROM VerifiedDoctorUser AS v INNER JOIN DoctorUserAuths AS d ON v.UserID = d.UserID INNER JOIN DoctorUserBaseInfo AS i ON v.UserID = i.UserID WHERE v.OrgCode = ? AND (v.UserType = ? OR v.UserType = 11) AND v.VerifiedStatus = 1 AND d.`Status` = 0"
		rows, err := sqlAccess.Query(sqlString, v.OrgCode, argument.UserType)
		if s.sqlDatabase.IsNoRows(err) {
			rows.Close()
			continue
		} else if err != nil {
			rows.Close()
			return results, business.NewError(errors.InternalError, err)
		}

		for rows.Next() {
			err = rows.Scan(&dbDoctorEntity.UserID, &dbDoctorEntity.UserName, &dbDoctorEntity.Name)
			if err != nil {
				return results, business.NewError(errors.InternalError, err)
			}

			tmpDoctor := doctor.DoctorUserAuthsBaseInfo{}
			dbDoctorEntity.CopyTo(&tmpDoctor)
			doctors = append(doctors, tmpDoctor)
		}
		v.Doctors = doctors
		rows.Close()
	}

	return results, nil
}

func (s *Data) SearchPatientBodyMassIndex(filter *doctor.PatientInfoBase) (*float64, business.Error) {
	dbFilter := &sqldb.PatientUserBaseInfoBmiFilter{}
	if filter != nil {
		dbFilter.UserID = filter.PatientID
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	dbEntity := &sqldb.PatientUserBaseInfoBmi{}
	err := s.sqlDatabase.SelectOne(dbEntity, sqlFilter)
	if err != nil {
		if s.sqlDatabase.IsNoRows(err) {
			return nil, business.NewError(errors.NotExist, err)
		} else {
			return nil, business.NewError(errors.InternalError, err)
		}
	}

	return dbEntity.GetBmi(), nil
}

func (s *Data) CreateMonthlyReport(argument *doctor.MonthlyReportYearMonth) business.Error {
	nowYear := time.Now().Year()
	nowMonth := int(time.Now().Month())
	year, _ := strconv.Atoi(argument.YearMonth[0:4])
	month, _ := strconv.Atoi(argument.YearMonth[5:7])
	if (year != nowYear && year != nowYear-1) ||
		(year == nowYear && month != nowMonth-1) ||
		(year == nowYear-1 && (month != 12 || nowMonth != 1)) {
		return business.NewError(errors.InternalError, fmt.Errorf("无效的时间，不支持生成月报"))
	}
	monthlyReportDateInfo := doctor.ParseMonthlyReportDateInfo(year, month)
	monthlyReportCreateData := &doctor.MonthlyReportCreateData{
		MonthlyReportDateInfo: monthlyReportDateInfo,
		YearMonth:             argument.YearMonth,
	}

	patientList := make([]*sqldb.PatientUserBaseInfoMonthly, 0)
	dbEntity := &sqldb.PatientUserBaseInfoMonthly{}
	err := s.sqlDatabase.SelectList(dbEntity, func() {
		temp := &sqldb.PatientUserBaseInfoMonthly{
			UserID: dbEntity.UserID,
			Height: dbEntity.Height,
		}
		patientList = append(patientList, temp)
	}, nil)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}

	for _, v := range patientList {
		monthlyReportCreateData.PatientID = v.UserID
		monthlyReportCreateData.Height = v.Height
		err = s.createMonthlyReportDetails(monthlyReportCreateData)
	}

	return nil
}

func (s *Data) createMonthlyReportDetails(argument *doctor.MonthlyReportCreateData) error {
	dbRegistDateEntity := &sqldb.PatientUserAuthsRegistDate{}
	dbRegistDateFilter := &sqldb.PatientUserAuthsUserIdFilter{UserID: argument.PatientID}
	sqlRegistDateFilter := s.sqlDatabase.NewFilter(dbRegistDateFilter, false, false)
	err := s.sqlDatabase.SelectOne(dbRegistDateEntity, sqlRegistDateFilter)
	if err != nil {
		return fmt.Errorf("患者(ID=%d)不存在", argument.PatientID)
	}

	monthlyReportValidDays := uint64(argument.DayNum)
	registDate := time.Time(*dbRegistDateEntity.RegistDateTime)
	startDate := time.Time(*argument.StartDate)
	endDate := time.Time(*argument.EndDate)
	if registDate.After(endDate) {
		return fmt.Errorf("患者(ID=%d)的注册时间不满足月报生成条件", argument.PatientID)
	} else if registDate.After(startDate) && registDate.Before(endDate) {
		duration := registDate.Sub(startDate)
		monthlyReportValidDays -= uint64(duration / (time.Hour * 24))
	}

	managementPlan := &doctor.ManagementPlanForApp{}
	err = s.loadManagementPlan(argument.PatientID, managementPlan)
	if err != nil {
		return fmt.Errorf("患者(ID=%d)的管理计划加载失败", argument.PatientID)
	}

	dbMonthlyCompleteness := &sqldb.MonthlyCompleteness{}
	dbFilter := &sqldb.MonthlyCompletenessFilter{
		PatientId: argument.PatientID,
		YearMonth: argument.YearMonth,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	err = s.sqlDatabase.SelectOne(dbMonthlyCompleteness, sqlFilter)
	if !s.sqlDatabase.IsNoRows(err) {
		return err
	}

	dbMonthlyCompleteness.PatientID = argument.PatientID
	dbMonthlyCompleteness.YearMonth = argument.YearMonth
	dbMonthlyBPInfo := &sqldb.MonthlyBPInfo{
		PatientID: argument.PatientID,
		YearMonth: argument.YearMonth,
	}
	dbMonthlyHRInfo := &sqldb.MonthlyHRInfo{
		PatientID: argument.PatientID,
		YearMonth: argument.YearMonth,
	}
	bpDataFilter := &doctor.BloodPressureRecordDataFilterEx{
		PatientID: argument.PatientID,
		BloodPressureRecordDataFilter: doctor.BloodPressureRecordDataFilter{
			MeasureStartDate: argument.StartDate,
			MeasureEndDate:   argument.EndDate,
		},
	}
	bpAndhrDataList, _ := s.ListBloodPressureRecord(bpDataFilter)
	bpNum := len(bpAndhrDataList)
	hrNum := uint64(0)
	if bpNum > 0 {
		dbMonthlyCompleteness.BpMeasuredTimes = uint64(bpNum)
		dbMonthlyBPInfo.CurrentBP = fmt.Sprintf("%d/%d", bpAndhrDataList[bpNum-1].SystolicPressure, bpAndhrDataList[bpNum-1].DiastolicPressure)
		dbMonthlyBPInfo.GoalBP = fmt.Sprintf("%s/%s", managementPlan.GoalSBP, managementPlan.GoalDBP)
		var bpNormalTimes, bpHigherTimes, bpLowerTimes, hrNormalTimes, hrHigherTimes, hrLowerTimes uint64
		var maxSBPIndex, minSBPIndex, maxDBPIndex, minDBPIndex, maxBPRangeIndex, minBPRangeIndex int
		var totalSBP, totalDBP, totalBPRange uint64
		var maxHRIndex, minHRIndex int
		var totalHR uint64
		for i, v := range bpAndhrDataList {
			bpRange := s.getBPDiffABS(v.SystolicPressure, v.DiastolicPressure)
			totalSBP += v.SystolicPressure
			totalDBP += v.DiastolicPressure
			totalBPRange += bpRange
			if bpAndhrDataList[maxSBPIndex].SystolicPressure <= v.SystolicPressure {
				maxSBPIndex = i
			}
			if bpAndhrDataList[minSBPIndex].SystolicPressure >= v.SystolicPressure {
				minSBPIndex = i
			}
			if bpAndhrDataList[maxDBPIndex].DiastolicPressure <= v.DiastolicPressure {
				maxDBPIndex = i
			}
			if bpAndhrDataList[minDBPIndex].DiastolicPressure >= v.DiastolicPressure {
				minDBPIndex = i
			}
			if s.getBPDiffABS(bpAndhrDataList[maxBPRangeIndex].SystolicPressure, bpAndhrDataList[maxBPRangeIndex].DiastolicPressure) <= bpRange {
				maxBPRangeIndex = i
			}
			if s.getBPDiffABS(bpAndhrDataList[minBPRangeIndex].SystolicPressure, bpAndhrDataList[minBPRangeIndex].DiastolicPressure) >= bpRange {
				minBPRangeIndex = i
			}

			if v.SystolicPressure >= 140 || v.DiastolicPressure >= 90 {
				bpHigherTimes++
			} else if v.SystolicPressure < 90 || v.DiastolicPressure < 60 {
				bpLowerTimes++
			} else {
				bpNormalTimes++
			}

			if v.HeartRate != nil {
				hr := uint64(*v.HeartRate)
				if hr > 0 {
					hrNum++
					totalHR += hr
					if bpAndhrDataList[maxHRIndex].HeartRate == nil || uint64(*bpAndhrDataList[maxHRIndex].HeartRate) <= hr {
						maxHRIndex = i
					}
					if bpAndhrDataList[minHRIndex].HeartRate == nil || uint64(*bpAndhrDataList[minHRIndex].HeartRate) >= hr {
						minHRIndex = i
					}

					if hr > 100 {
						hrHigherTimes++
					} else if hr < 50 {
						hrLowerTimes++
					} else {
						hrNormalTimes++
					}
				}
			}
		}
		dbMonthlyBPInfo.MaxSBP = bpAndhrDataList[maxSBPIndex].SystolicPressure
		dateTime := time.Time(*bpAndhrDataList[maxSBPIndex].MeasureDateTime)
		dbMonthlyBPInfo.MaxSBPTime = &dateTime
		dbMonthlyBPInfo.MinSBP = bpAndhrDataList[minSBPIndex].SystolicPressure
		dateTime = time.Time(*bpAndhrDataList[minSBPIndex].MeasureDateTime)
		dbMonthlyBPInfo.MinSBPTime = &dateTime
		dbMonthlyBPInfo.MaxDBP = bpAndhrDataList[maxDBPIndex].DiastolicPressure
		dateTime = time.Time(*bpAndhrDataList[maxDBPIndex].MeasureDateTime)
		dbMonthlyBPInfo.MaxDBPTime = &dateTime
		dbMonthlyBPInfo.MinDBP = bpAndhrDataList[minDBPIndex].DiastolicPressure
		dateTime = time.Time(*bpAndhrDataList[minDBPIndex].MeasureDateTime)
		dbMonthlyBPInfo.MinDBPTime = &dateTime
		dbMonthlyBPInfo.MaxRange = bpAndhrDataList[maxBPRangeIndex].SystolicPressure - bpAndhrDataList[maxBPRangeIndex].DiastolicPressure
		dateTime = time.Time(*bpAndhrDataList[maxBPRangeIndex].MeasureDateTime)
		dbMonthlyBPInfo.MaxRangeTime = &dateTime
		dbMonthlyBPInfo.MinRange = bpAndhrDataList[minBPRangeIndex].SystolicPressure - bpAndhrDataList[minBPRangeIndex].DiastolicPressure
		dateTime = time.Time(*bpAndhrDataList[minBPRangeIndex].MeasureDateTime)
		dbMonthlyBPInfo.MinRangeTime = &dateTime
		dbMonthlyBPInfo.AvgSBP = totalSBP / uint64(bpNum)
		dbMonthlyBPInfo.AvgDBP = totalDBP / uint64(bpNum)
		dbMonthlyBPInfo.AvgRange = totalBPRange / uint64(bpNum)
		dbMonthlyBPInfo.NormalTimes = bpNormalTimes
		dbMonthlyBPInfo.HigherTimes = bpHigherTimes
		dbMonthlyBPInfo.LowerTimes = bpLowerTimes

		if hrNum > 0 {
			dbMonthlyCompleteness.HrMeasuredTimes = hrNum
			dbMonthlyHRInfo.MaxHR = uint64(*bpAndhrDataList[maxHRIndex].HeartRate)
			dateTime = time.Time(*bpAndhrDataList[maxHRIndex].MeasureDateTime)
			dbMonthlyHRInfo.MaxHRTime = &dateTime
			dbMonthlyHRInfo.MinHR = uint64(*bpAndhrDataList[minHRIndex].HeartRate)
			dateTime = time.Time(*bpAndhrDataList[minHRIndex].MeasureDateTime)
			dbMonthlyHRInfo.MinHRTime = &dateTime
			dbMonthlyHRInfo.AvgHR = totalHR / hrNum
			dbMonthlyHRInfo.NormalTimes = hrNormalTimes
			dbMonthlyHRInfo.HigherTimes = hrHigherTimes
			dbMonthlyHRInfo.LowerTimes = hrLowerTimes
		}
	}

	dbMonthlyWeightInfo := &sqldb.MonthlyWeightInfo{
		PatientID: argument.PatientID,
		YearMonth: argument.YearMonth,
	}
	weightDataFilter := &doctor.WeightRecordDataFilterEx{
		PatientID: argument.PatientID,
		WeightRecordDataFilter: doctor.WeightRecordDataFilter{
			MeasureStartDate: argument.StartDate,
			MeasureEndDate:   argument.EndDate,
		},
	}
	weightDataList, _ := s.ListWeightRecord(weightDataFilter)
	weightNum := len(weightDataList)
	if weightNum > 0 {
		dbMonthlyCompleteness.WeightMeasuredTimes = uint64(weightNum)
		goalBMI, err := strconv.ParseFloat(managementPlan.GoalBMI, 64)
		if err != nil {
			goalBMI = float64(24.0)
		}
		currentWeight := weightDataList[weightNum-1].Weight
		tempHeight := float64(1.68)
		if argument.Height != nil {
			tempHeight = float64(*argument.Height) / 100
		}
		currentBMITemp := currentWeight / tempHeight / tempHeight
		currentBMI, _ := strconv.ParseFloat(fmt.Sprintf("%.2f", currentBMITemp), 64)
		targetWeightTemp := goalBMI * tempHeight * tempHeight
		targetWeight, _ := strconv.ParseFloat(fmt.Sprintf("%.1f", targetWeightTemp), 64)
		currentHeight := uint64(tempHeight * 100)
		dbMonthlyWeightInfo.CurrentHeight = &currentHeight
		dbMonthlyWeightInfo.CurrentWeight = currentWeight
		dbMonthlyWeightInfo.CurrentBMI = currentBMI
		dbMonthlyWeightInfo.TargetWeight = targetWeight
	}

	drugDataFilter := &doctor.DrugRecordDataFilterEx{
		PatientID: argument.PatientID,
		DrugRecordDataFilter: doctor.DrugRecordDataFilter{
			UseStartDate: argument.StartDate,
			UseEndDate:   argument.EndDate,
		},
	}
	drugDataList, _ := s.ListDrugRecord(drugDataFilter)
	drugNum := len(drugDataList)
	if drugNum > 0 {
		dbMonthlyCompleteness.DrugTakenTimes = uint64(drugNum)
	}

	if bpNum <= 0 && weightNum <= 0 && drugNum <= 0 {
		return fmt.Errorf("患者(ID=%d)的本月没有记录任何数据", argument.PatientID)
	}
	dbMonthlyCompleteness.BpPlanedTimes = monthlyReportValidDays * uint64(len(managementPlan.BPTasks))
	dbMonthlyCompleteness.WeightPlanedTimes = monthlyReportValidDays * uint64(len(managementPlan.WeightTasks))
	dbMonthlyCompleteness.DrugPlanedTimes = monthlyReportValidDays * uint64(len(managementPlan.DrugTasks))
	duration := endDate.Sub(registDate)
	dbMonthlyCompleteness.DaysInManagement = uint64(duration / (time.Hour * 24))

	sqlAccess, err := s.sqlDatabase.NewAccess(true)
	if err != nil {
		return err
	}
	defer sqlAccess.Close()

	_, err = sqlAccess.InsertSelective(dbMonthlyCompleteness)
	if err != nil {
		return err
	}
	if bpNum > 0 {
		_, err = sqlAccess.InsertSelective(dbMonthlyBPInfo)
		if err != nil {
			return err
		}
	}
	if hrNum > 0 {
		_, err = sqlAccess.InsertSelective(dbMonthlyHRInfo)
		if err != nil {
			return err
		}
	}
	if weightNum > 0 {
		_, err = sqlAccess.InsertSelective(dbMonthlyWeightInfo)
	}
	err = sqlAccess.Commit()
	if err != nil {
		return err
	}

	return nil
}

func (s *Data) getBPDiffABS(sbp, dbp uint64) uint64 {
	abs := uint64(0)
	if sbp >= dbp {
		abs = sbp - dbp
	} else {
		abs = dbp - sbp
	}
	return abs
}

func (s *Data) PatientCheckWeChatStatus(argument *doctor.PatientUserAuthsEx) (*doctor.PatientWeChatStatus, business.Error) {
	result := &doctor.PatientWeChatStatus{}

	dbFilter := &sqldb.PatientUserAuthsExFilter{
		UserID:       argument.UserID,
		IdentityType: argument.IdentityType,
		Identitier:   argument.Identitier,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)

	dbEntity := &sqldb.PatientUserAuthsEx{}

	err := s.sqlDatabase.SelectOne(dbEntity, sqlFilter)
	if err != nil {
		if s.sqlDatabase.IsNoRows(err) {
			result.Status = 1
			return result, nil
		}
	}
	result.Status, _ = strconv.ParseUint(*dbEntity.Status, 10, 64)

	return result, nil
}

func (s *Data) UploadDataToManage(sno uint64) error {
	dbEntity := &sqldb.WeightRecord{}
	dbFilter := &sqldb.WeightRecordFilter{
		SerialNo: &sno,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	err := s.sqlDatabase.SelectOne(dbEntity, sqlFilter)
	if err != nil {
		return err
	}
	providerType := config.ManagementProviderTypeHbp
	uri := s.cfg.Management.Providers.Hbp.Api.Uri.UploadBloodPressure
	argument := &manage.WeightInput{}
	argument.PatientID = fmt.Sprint(dbEntity.PatientID)
	argument.Weight.Value = &dbEntity.Weight
	snoStr1 := fmt.Sprintf("%s-2", fmt.Sprint(sno))
	argument.Weight.DataId = &snoStr1
	if dbEntity.MeasureDateTime == nil {
		argument.Weight.MeasuredTime = nil
	} else {
		measureDateTime := types.Time(*dbEntity.MeasureDateTime)
		argument.Weight.MeasuredTime = &measureDateTime
	}

	if dbEntity.Waistline != nil {
		wc := &manage.WcData{}
		snoStr2 := fmt.Sprintf("%s-3", fmt.Sprint(sno))
		wc.DataId = &snoStr2
		wc.Value = dbEntity.Waistline
		if dbEntity.MeasureDateTime == nil {
			wc.MeasuredTime = nil
		} else {
			measureDateTime := types.Time(*dbEntity.MeasureDateTime)
			wc.MeasuredTime = &measureDateTime
		}
		argument.Wc = wc
	}
	if dbEntity.Height != nil {
		height := &manage.HeightData{}
		snoStr3 := fmt.Sprintf("%s-1", fmt.Sprint(sno))
		height.DataId = &snoStr3
		height.Value = dbEntity.Height
		if dbEntity.MeasureDateTime == nil {
			height.MeasuredTime = nil
		} else {
			measureDateTime := types.Time(*dbEntity.MeasureDateTime)
			height.MeasuredTime = &measureDateTime
		}
		argument.Height = height
	}

	_, err = s.client.ManageBusinessPost("上传患者身高体重腰围数据", dbEntity.PatientID, providerType, uri, argument)
	if err != nil {
		return err
	}
	return nil
}

func (s *Data) DecryptWeRunData(argument *doctor.WeRunCryptDataInput) (*doctor.WeRunData, business.Error) {
	aesKey, err := base64.StdEncoding.DecodeString(argument.SessionKey)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}
	cipherText, err := base64.StdEncoding.DecodeString(argument.EncryptedData)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}
	ivBytes, err := base64.StdEncoding.DecodeString(argument.Iv)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}
	block, err := aes.NewCipher(aesKey)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}
	mode := cipher.NewCBCDecrypter(block, ivBytes)
	mode.CryptBlocks(cipherText, cipherText)
	cipherText, err = pkcs7Unpad(cipherText, block.BlockSize())
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}

	result := &doctor.WeRunDataList{}
	err = json.Unmarshal(cipherText, &result)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}
	err = s.saveRecentWeStepRecord(argument.PatientID, result.StepInfoList)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}
	return result.StepInfoList[30], nil
}

// pkcs7Unpad returns slice of the original data without padding
func pkcs7Unpad(data []byte, blockSize int) ([]byte, error) {
	if blockSize <= 0 {
		return nil, fmt.Errorf("invalid block size")
	}
	if len(data)%blockSize != 0 || len(data) == 0 {
		return nil, fmt.Errorf("invalid PKCS7 data")
	}
	c := data[len(data)-1]
	n := int(c)
	if n == 0 || n > len(data) {
		return nil, fmt.Errorf("invalid padding on input")
	}
	for i := 0; i < n; i++ {
		if data[len(data)-n+i] != c {
			return nil, fmt.Errorf("invalid padding on input")
		}
	}
	return data[:len(data)-n], nil
}

func (s *Data) saveRecentWeStepRecord(patientID uint64, results []*doctor.WeRunData) error {
	lastStepRecord := &sqldb.SportDietRecord{}

	sqlEntity := s.sqlDatabase.NewEntity()
	sqlEntity.Parse(lastStepRecord)
	sqlBuilder := s.sqlDatabase.NewBuilder()
	sqlBuilder.Select(sqlEntity.ScanFields(), false).From(sqlEntity.Name())
	sqlBuilder.Append("WHERE PatientID = ? and ItemName = ? order by SerialNo desc LIMIT 1")
	query := sqlBuilder.Query()

	sqlAccess, err := s.sqlDatabase.NewAccess(true)
	if err != nil {
		return err
	}
	defer sqlAccess.Close()

	row := sqlAccess.QueryRow(query, patientID, "微信运动")
	err = row.Scan(sqlEntity.ScanArgs()...)
	if err != nil {
		if !sqlAccess.IsNoRows(err) {
			return err
		}
	}
	sportDatas := make([]*sqldb.SportDietRecord, 0)
	// 不存储当天的数据
	today := time.Now()
	i := len(results) - 1
	recentWeRunDataTime := time.Unix(int64(results[i].TimeStamp), 0)
	if today.Day() == recentWeRunDataTime.Day() {
		i--
	}
	for ; i >= 0; i-- {
		item := results[i]
		sportData := &sqldb.SportDietRecord{}
		sportData.CopyFromWeRunData(patientID, item)
		happenDateTime := time.Time(*sportData.HappenDateTime).Add(-1000)
		if lastStepRecord.HappenDateTime != nil && lastStepRecord.HappenDateTime.After(happenDateTime) {
			break
		}
		//如果是一条训练营开始后的运动新纪录则进行积分累加计算
		err = s.addSportPoint(sportData)
		sportDatas = append(sportDatas, sportData)
	}

	for i = len(sportDatas) - 1; i >= 0; i-- {
		_, err = s.sqlDatabase.InsertSelective(sportDatas[i])
		if err != nil {
			return err
		}
	}

	return nil
}

func (s *Data) addSportPoint(sportData *sqldb.SportDietRecord) error {
	sqlAccess, err := s.sqlDatabase.NewAccess(true)
	if err != nil {
		return err
	}
	defer sqlAccess.Close()

	// 查找当前积分规则
	pointEntity := &sqldb.ExpIntegralRule{}
	pointFilter := &sqldb.ExpIntegralRuleFilter{OrgCode: "20210422"}
	sqlPointFilter := s.sqlDatabase.NewFilter(pointFilter, false, false)
	err = sqlAccess.SelectOne(pointEntity, sqlPointFilter)
	if err != nil {
		return err
	}

	beginDate := time.Date(2021, 12, 9, 23, 59, 59, 59, sportData.HappenDateTime.Location())
	if sportData.HappenDateTime.Before(beginDate) {
		return nil
	}

	step, err := strconv.ParseInt(*sportData.ItemValue, 10, 64)
	point := 0
	if step > 10000 {
		point = int(*pointEntity.ThirdSteps)
	} else if step > 8000 {
		point = int(*pointEntity.SecondSteps)
	} else if step > 6000 {
		point = int(*pointEntity.FirstSteps)
	}
	if point != 0 {
		dbEntity := &sqldb.ExpIntegral{
			PatientID:      sportData.PatientID,
			Integral:       uint64(point),
			Reason:         "微信步数",
			CreateDateTime: sportData.HappenDateTime,
		}
		_, err = s.sqlDatabase.InsertSelective(dbEntity)
		if err != nil {
			return err
		}

		////同步在总积分表里更新积分，需先判断是否存在该用户
		//dbTotalEntity := &sqldb.Exptotalpoint{}
		//dbTotalFilter := &sqldb.ExptotalPointFilter{
		//	PatientID: sportData.PatientID,
		//}
		//sqlTotalFilter := s.sqlDatabase.NewFilter(dbTotalFilter,false,false)
		//err = sqlAccess.SelectOne(dbTotalEntity,sqlTotalFilter)
		//if err == nil {
		//	newTotalPoint := dbTotalEntity.TotalPoint + uint64(point)
		//	dbTotalUpdate := &sqldb.ExptotalPointUpdate{
		//		TotalPoint: newTotalPoint,
		//	}
		//	sqlAccess.Update(dbTotalUpdate,sqlTotalFilter)
		//} else {
		//	dbInfoEntity := &sqldb.PatientUserBaseInfo{}
		//	dbInfoFilter := &sqldb.PatientUserBaseInfoUserIdFilter{
		//		UserID: sportData.PatientID,
		//	}
		//	sqlInfoFilter := s.sqlDatabase.NewFilter(dbInfoFilter,false,false)
		//	err = sqlAccess.SelectOne(dbInfoEntity,sqlInfoFilter)
		//	if err == nil {
		//		dbNewEntity := &sqldb.Exptotalpoint{
		//			PatientID: sportData.PatientID,
		//			PatientName: dbInfoEntity.Name,
		//			TotalPoint: uint64(point),
		//		}
		//		_, err = sqlAccess.Insert(dbNewEntity)
		//	}
		//}
	}

	err = sqlAccess.Commit()

	return nil

}
