package impl

import (
	"fmt"
	"github.com/ktpswjz/httpserver/types"
	"time"
	"tlcbme_project/business"
	"tlcbme_project/data/entity/nutrient"
	"tlcbme_project/data/entity/sqldb"
	"tlcbme_project/data/model/doctor"
	"tlcbme_project/data/model/nutrient"
	"tlcbme_project/errors"
)

type rel struct {
	// 权重
	w float64
	// nrv
	nrv float64
}

var dataMap = map[string]rel{
	"na":      {w: 0.0735, nrv: 2000},
	"fat":     {w: 0.0711, nrv: 60},
	"protein": {w: 0.0723, nrv: 60},
	"ca":      {w: 0.0699, nrv: 800},
	"k":       {w: 0.0613, nrv: 2000},
	"mg":      {w: 0.0600, nrv: 300},
	"df":      {w: 0.0711, nrv: 25},
	"fe":      {w: 0.0662, nrv: 15},
	"va":      {w: 0.0650, nrv: 800},
	"vb1":     {w: 0.0650, nrv: 1.4},
	"vb2":     {w: 0.0650, nrv: 1.4},
	"vc":      {w: 0.0600, nrv: 100},
	"vd":      {w: 0.0650, nrv: 5},
	"zn":      {w: 0.0637, nrv: 15},
}

func (s *Data) NutrientAnalysis(filter *doctor.NutrientAnalysisFilter) (*doctor.NutrientAnalysisResult, business.Error) {
	result := &doctor.NutrientAnalysisResult{}
	result.AllIntake = &doctor.Intake{}
	result.BreakfastIntake = &doctor.Intake{}
	result.LunchIntake = &doctor.Intake{}
	result.DinnerIntake = &doctor.Intake{}

	result.DietPlan = &doctor.DietPlanRecord{}

	err := s.energyAnalysis(filter, result)
	if err != nil {
		return nil, business.NewError(errors.InternalError, fmt.Errorf("获取推荐摄入量失败：%s", err))
	}
	err = s.getActualEnergy(filter, result)
	if err != nil {
		result.Status = true
		return result, business.NewError(errors.InternalError, fmt.Errorf("获取实际摄入量失败：%s", err))
	}

	result.GradeList = append(result.GradeList, &doctor.Grade{
		DateTime: filter.AnalysisDateTime,
		Value:    result.Grade,
	})
	result.EnergyList = append(result.EnergyList, &doctor.Grade{
		DateTime: filter.AnalysisDateTime,
		Value:    result.AllIntake.Energy.Actual,
	})

	conclusion := make([]string, 0)
	carbohydrateEnergy := result.AllIntake.Carbohydrate.Actual * 4
	fat := result.AllIntake.Fat.Actual * 9
	protein := result.AllIntake.Protein.Actual * 4
	if carbohydrateEnergy < result.AllIntake.Carbohydrate.RecommendMax && carbohydrateEnergy >= result.AllIntake.Carbohydrate.RecommendMin {
		conclusion = append(conclusion, "碳水化合物：非常不错，请继续保持！")
	} else if carbohydrateEnergy < result.AllIntake.Carbohydrate.RecommendMin {
		conclusion = append(conclusion, "碳水化合物：再去补充一些食物，才能满足您的健康需要哦！")
	} else {
		conclusion = append(conclusion, "碳水化合物：今天摄入的能量太多了，快去运动消耗多余的能量吧！")
	}
	if protein < result.AllIntake.Protein.RecommendMin {
		conclusion = append(conclusion, "蛋白质：您今天摄入的蛋白质偏少，建议补充鱼、肉、蛋、奶！")
	} else if protein > result.AllIntake.Protein.RecommendMax {
		conclusion = append(conclusion, "蛋白质：今天的肉蛋达标了，请继续保持！")
	} else {
		conclusion = append(conclusion, "蛋白质：今天的肉蛋达标了，请继续保持！")
	}
	if fat >= result.AllIntake.Fat.RecommendMin {
		conclusion = append(conclusion, "脂肪：油炸食物、肉类吃多了，建议减少摄入哦")
	} else {
		conclusion = append(conclusion, "脂肪：摄入合理，给你点个赞！")
	}
	result.Conclusion = conclusion

	//饮食计划评估部分
	err = s.WaterPlace(filter, result)
	if err != nil {
		return nil, business.NewError(errors.InternalError, fmt.Errorf("获取当日饮水和用餐地点失败：%s", err))
	}

	return result, nil
}

func (s *Data) energyAnalysis(filter *doctor.NutrientAnalysisFilter, result *doctor.NutrientAnalysisResult) error {
	sqlAccess, err := s.sqlDatabase.NewAccess(false)
	if err != nil {
		return err
	}
	defer sqlAccess.Close()

	dbPatientInfoEntity := &sqldb.PatientUserBaseInfo{}
	dbFilter := &sqldb.PatientUserBaseInfoUserIdFilter{
		UserID: filter.PatientID,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	err = sqlAccess.SelectOne(dbPatientInfoEntity, sqlFilter)
	if err != nil {
		return err
	}
	dbIndicator := &sqldb.ExpPatientNutritionalIndicator{}
	err = sqlAccess.SelectOne(dbIndicator, sqlFilter)
	if err != nil {
		if s.sqlDatabase.IsNoRows(err) {
		} else {
			return err
		}

	}
	age := int64(18)
	if dbPatientInfoEntity.DateOfBirth != nil {
		age2 := time.Now().Sub(*dbPatientInfoEntity.DateOfBirth)
		age = int64(age2.Hours() / (24 * 365))
	}
	sex := uint64(1)
	if dbPatientInfoEntity.Sex != nil {
		sex = *dbPatientInfoEntity.Sex
	}
	labourIntensity := "轻度"
	if dbPatientInfoEntity.JobType != nil {
		labourIntensity = *dbPatientInfoEntity.JobType
	}

	energy := s.getRecommendEnergy(age, sex, labourIntensity)

	s.setRecommendEnergy(result, energy, dbIndicator)

	s.setRecommendTraceElement(age, sex, result)

	return nil
}

func (s *Data) setRecommendEnergy(result *doctor.NutrientAnalysisResult, energy float64, dbIndicator *sqldb.ExpPatientNutritionalIndicator) {
	// 总量
	// RecommendMax代表推荐值
	if dbIndicator.GoalEnergy != nil {
		energy = float64(*dbIndicator.GoalEnergy)
	}

	goalCarbohydrateMin := float64(50)
	goalCarbohydrateMax := float64(65)
	goalFatMin := float64(20)
	goalFatMax := float64(30)
	goalProteinMin := float64(10)
	goalProteinMax := float64(15)
	if dbIndicator.GoalCarbohydrateMin != nil {
		goalCarbohydrateMin = *dbIndicator.GoalCarbohydrateMin
	}
	if dbIndicator.GoalCarbohydrateMax != nil {
		goalCarbohydrateMax = *dbIndicator.GoalCarbohydrateMax
	}
	if dbIndicator.GoalFatMin != nil {
		goalFatMin = *dbIndicator.GoalFatMin
	}
	if dbIndicator.GoalFatMax != nil {
		goalFatMax = *dbIndicator.GoalFatMax
	}
	if dbIndicator.GoalProteinMin != nil {
		goalProteinMin = *dbIndicator.GoalProteinMin
	}
	if dbIndicator.GoalProteinMax != nil {
		goalProteinMax = *dbIndicator.GoalProteinMax
	}

	result.AllIntake.Energy.RecommendMax = energy
	result.AllIntake.Carbohydrate.RecommendMin = result.AllIntake.Energy.RecommendMax * float64(goalCarbohydrateMin) / 100
	result.AllIntake.Carbohydrate.RecommendMax = result.AllIntake.Energy.RecommendMax * float64(goalCarbohydrateMax) / 100
	result.AllIntake.Fat.RecommendMin = result.AllIntake.Energy.RecommendMax * float64(goalFatMin) / 100
	result.AllIntake.Fat.RecommendMax = result.AllIntake.Energy.RecommendMax * float64(goalFatMax) / 100
	result.AllIntake.Protein.RecommendMin = result.AllIntake.Energy.RecommendMax * float64(goalProteinMin) / 100
	result.AllIntake.Protein.RecommendMax = result.AllIntake.Energy.RecommendMax * float64(goalProteinMax) / 100
	// 早餐
	result.BreakfastIntake.Energy.RecommendMax = energy * 3 / 10
	result.BreakfastIntake.Carbohydrate.RecommendMin = result.BreakfastIntake.Energy.RecommendMax * float64(goalCarbohydrateMin) / 100
	result.BreakfastIntake.Carbohydrate.RecommendMax = result.BreakfastIntake.Energy.RecommendMax * float64(goalCarbohydrateMax) / 100
	result.BreakfastIntake.Fat.RecommendMin = result.BreakfastIntake.Energy.RecommendMax * float64(goalFatMin) / 100
	result.BreakfastIntake.Fat.RecommendMax = result.BreakfastIntake.Energy.RecommendMax * float64(goalFatMax) / 100
	result.BreakfastIntake.Protein.RecommendMin = result.BreakfastIntake.Energy.RecommendMax * float64(goalProteinMin) / 100
	result.BreakfastIntake.Protein.RecommendMax = result.BreakfastIntake.Energy.RecommendMax * float64(goalProteinMax) / 100

	// 午餐
	result.LunchIntake.Energy.RecommendMax = energy * 4 / 10
	result.LunchIntake.Carbohydrate.RecommendMin = result.LunchIntake.Energy.RecommendMax * float64(goalCarbohydrateMin) / 100
	result.LunchIntake.Carbohydrate.RecommendMax = result.LunchIntake.Energy.RecommendMax * float64(goalCarbohydrateMax) / 100
	result.LunchIntake.Fat.RecommendMin = result.LunchIntake.Energy.RecommendMax * float64(goalFatMin) / 100
	result.LunchIntake.Fat.RecommendMax = result.LunchIntake.Energy.RecommendMax * float64(goalFatMax) / 100
	result.LunchIntake.Protein.RecommendMin = result.LunchIntake.Energy.RecommendMax * float64(goalProteinMin) / 100
	result.LunchIntake.Protein.RecommendMax = result.LunchIntake.Energy.RecommendMax * float64(goalProteinMax) / 100

	// 晚餐
	result.DinnerIntake.Energy.RecommendMax = energy * 3 / 10
	result.DinnerIntake.Carbohydrate.RecommendMin = result.DinnerIntake.Energy.RecommendMax * float64(goalCarbohydrateMin) / 100
	result.DinnerIntake.Carbohydrate.RecommendMax = result.DinnerIntake.Energy.RecommendMax * float64(goalCarbohydrateMax) / 100
	result.DinnerIntake.Fat.RecommendMin = result.DinnerIntake.Energy.RecommendMax * float64(goalFatMin) / 100
	result.DinnerIntake.Fat.RecommendMax = result.DinnerIntake.Energy.RecommendMax * float64(goalFatMax) / 100
	result.DinnerIntake.Protein.RecommendMin = result.DinnerIntake.Energy.RecommendMax * float64(goalProteinMin) / 100
	result.DinnerIntake.Protein.RecommendMax = result.DinnerIntake.Energy.RecommendMax * float64(goalProteinMax) / 100
}

func (s *Data) getRecommendEnergy(age int64, sex uint64, labourIntensity string) float64 {
	if age >= 14 && age < 18 {
		if sex == 1 {
			if labourIntensity == "轻度" {
				return 2500
			} else if labourIntensity == "中度" {
				return 2850
			} else if labourIntensity == "重度" {
				return 3200
			}
		} else if sex == 2 {
			if labourIntensity == "轻度" {
				return 2000
			} else if labourIntensity == "中度" {
				return 2300
			} else if labourIntensity == "重度" {
				return 2550
			}
		}
	} else if age >= 18 && age < 50 {
		if sex == 1 {
			if labourIntensity == "轻度" {
				return 2250
			} else if labourIntensity == "中度" {
				return 2600
			} else if labourIntensity == "重度" {
				return 3000
			}
		} else if sex == 2 {
			if labourIntensity == "轻度" {
				return 1800
			} else if labourIntensity == "中度" {
				return 2100
			} else if labourIntensity == "重度" {
				return 2400
			}
		}
	} else if age >= 50 && age < 65 {
		if sex == 1 {
			if labourIntensity == "轻度" {
				return 2100
			} else if labourIntensity == "中度" {
				return 2450
			} else if labourIntensity == "重度" {
				return 2800
			}
		} else if sex == 2 {
			if labourIntensity == "轻度" {
				return 1750
			} else if labourIntensity == "中度" {
				return 2050
			} else if labourIntensity == "重度" {
				return 2350
			}
		}
	} else if age >= 65 && age < 80 {
		if sex == 1 {
			if labourIntensity == "轻度" {
				return 2050
			} else if labourIntensity == "中度" {
				return 2350
			} else if labourIntensity == "重度" {
				return 3000
			}
		} else if sex == 2 {
			if labourIntensity == "轻度" {
				return 1700
			} else if labourIntensity == "中度" {
				return 1950
			} else if labourIntensity == "重度" {
				return 2400
			}
		}
	} else if age >= 80 {
		if sex == 1 {
			if labourIntensity == "轻度" {
				return 1900
			} else if labourIntensity == "中度" {
				return 2200
			} else if labourIntensity == "重度" {
				return 3000
			}
		} else if sex == 2 {
			if labourIntensity == "轻度" {
				return 1500
			} else if labourIntensity == "中度" {
				return 1750
			} else if labourIntensity == "重度" {
				return 2400
			}
		}
	}
	return 2000
}

func (s *Data) getActualEnergy(filter *doctor.NutrientAnalysisFilter, result *doctor.NutrientAnalysisResult) error {
	defer func() {
		if pan := recover(); pan != nil {
			fmt.Println(pan)
		}
	}()
	// 得到当天吃的所有食物
	list, err := s.getSomeDayFood(filter)
	if err != nil {
		return err
	}
	morningList := make([]*doctor.DietContent, 0)
	noonList := make([]*doctor.DietContent, 0)
	nightList := make([]*doctor.DietContent, 0)
	foodKindWeight := &doctor.FoodKindWeight{}

	foodKindCount := &doctor.DietPlanRecord{}

	for _, item := range list {
		if item.DietType == "早餐" || item.DietType == "早加" {
			for _, content := range item.DietContents {
				if content.NutritionSource == "recipe" {
					comps := s.getRecipeCompose(content)
					for _, comp := range comps {
						morningList = append(morningList, comp)
						s.addFoodWeight(comp.FoodTypeId, foodKindWeight, comp.MeasureValue, foodKindCount)
					}
				} else {
					morningList = append(morningList, content)
					s.addFoodWeight(content.FoodTypeId, foodKindWeight, content.MeasureValue, foodKindCount)
				}
			}
		} else if item.DietType == "中餐" || item.DietType == "中加" {
			for _, content := range item.DietContents {
				if content.NutritionSource == "recipe" {
					comps := s.getRecipeCompose(content)
					for _, comp := range comps {
						noonList = append(noonList, comp)
						s.addFoodWeight(comp.FoodTypeId, foodKindWeight, comp.MeasureValue, foodKindCount)
					}
				} else {
					noonList = append(noonList, content)
					s.addFoodWeight(content.FoodTypeId, foodKindWeight, content.MeasureValue, foodKindCount)
				}
			}
		} else if item.DietType == "晚餐" || item.DietType == "晚加" {
			for _, content := range item.DietContents {
				if content.NutritionSource == "recipe" {
					comps := s.getRecipeCompose(content)
					for _, comp := range comps {
						nightList = append(nightList, comp)
						s.addFoodWeight(comp.FoodTypeId, foodKindWeight, comp.MeasureValue, foodKindCount)
					}
				} else {
					nightList = append(nightList, content)
					s.addFoodWeight(content.FoodTypeId, foodKindWeight, content.MeasureValue, foodKindCount)
				}
			}
		}
	}
	result.FoodKindWeight = *foodKindWeight
	result.DietPlan = foodKindCount

	if len(morningList) == 0 && len(nightList) == 0 && len(noonList) == 0 {
		return fmt.Errorf("当天没有提交三餐记录")
	}
	// 获得食物成分
	morningComponents, err := s.getFoodComponent(morningList)
	if err != nil {
		return err
	}
	noonComponents, err := s.getFoodComponent(noonList)
	if err != nil {
		return err
	}
	nightComponents, err := s.getFoodComponent(nightList)
	if err != nil {
		return err
	}
	// 早餐实际摄入
	result.BreakfastIntake.Energy.Actual = morningComponents.ENERGY_KC
	result.BreakfastIntake.Carbohydrate.Actual = morningComponents.CARBOHYDRATE
	result.BreakfastIntake.Fat.Actual = morningComponents.FETT
	result.BreakfastIntake.Protein.Actual = morningComponents.PROTEIN
	// 午餐实际摄入
	result.LunchIntake.Energy.Actual = noonComponents.ENERGY_KC
	result.LunchIntake.Carbohydrate.Actual = noonComponents.CARBOHYDRATE
	result.LunchIntake.Fat.Actual = noonComponents.FETT
	result.LunchIntake.Protein.Actual = noonComponents.PROTEIN
	// 晚餐实际摄入
	result.DinnerIntake.Energy.Actual = nightComponents.ENERGY_KC
	result.DinnerIntake.Carbohydrate.Actual = nightComponents.CARBOHYDRATE
	result.DinnerIntake.Fat.Actual = nightComponents.FETT
	result.DinnerIntake.Protein.Actual = nightComponents.PROTEIN
	// 一天实际摄入
	result.AllIntake.Energy.Actual = result.BreakfastIntake.Energy.Actual + result.LunchIntake.Energy.Actual + result.DinnerIntake.Energy.Actual
	result.AllIntake.Carbohydrate.Actual = result.BreakfastIntake.Carbohydrate.Actual + result.LunchIntake.Carbohydrate.Actual + result.DinnerIntake.Carbohydrate.Actual
	result.AllIntake.Fat.Actual = result.BreakfastIntake.Fat.Actual + result.LunchIntake.Fat.Actual + result.DinnerIntake.Fat.Actual
	result.AllIntake.Protein.Actual = result.BreakfastIntake.Protein.Actual + result.LunchIntake.Protein.Actual + result.DinnerIntake.Protein.Actual

	// 微量元素
	result.CA.Actual = morningComponents.CA + noonComponents.CA + noonComponents.CA
	result.DF.Actual = morningComponents.DF + noonComponents.DF + noonComponents.DF
	result.POT.Actual = morningComponents.POT + noonComponents.POT + noonComponents.POT
	result.MG.Actual = morningComponents.MG + noonComponents.MG + noonComponents.MG
	result.FE.Actual = morningComponents.FE + noonComponents.FE + noonComponents.FE
	result.ZN.Actual = morningComponents.ZN + noonComponents.ZN + noonComponents.ZN
	result.VITA.Actual = morningComponents.VITA + noonComponents.VITA + noonComponents.VITA
	result.VITB1.Actual = morningComponents.VITB1 + noonComponents.VITB1 + noonComponents.VITB1
	result.VITB2.Actual = morningComponents.VITB2 + noonComponents.VITB2 + noonComponents.VITB2
	result.VITC.Actual = morningComponents.VITC + noonComponents.VITC + noonComponents.VITC
	result.VITD.Actual = morningComponents.VITD + noonComponents.VITD + noonComponents.VITD

	result.CHO.Actual = morningComponents.CHO + noonComponents.CHO + noonComponents.CHO
	result.USP.Actual = morningComponents.USP + noonComponents.USP + noonComponents.USP
	result.IODINE.Actual = morningComponents.IODINE + noonComponents.IODINE + noonComponents.IODINE
	result.SE.Actual = morningComponents.SE + noonComponents.SE + noonComponents.SE
	result.VITB6.Actual = morningComponents.VITB6 + noonComponents.VITB6 + noonComponents.VITB6
	result.VITE.Actual = morningComponents.VITE + noonComponents.VITE + noonComponents.VITE
	result.Na.Actual = morningComponents.NA + noonComponents.NA + noonComponents.NA
	allFood := &nutrientdb.Food{
		ENERGY_KC: &result.AllIntake.Energy.Actual,
		PROTEIN:   &result.AllIntake.Protein.Actual,
		DF:        &result.DF.Actual,
		CA:        &result.CA.Actual,
		FE:        &result.FE.Actual,
		MG:        &result.MG.Actual,
		ZN:        &result.ZN.Actual,
		POT:       &result.POT.Actual,
		VITA:      &result.VITA.Actual,
		VITB1:     &result.VITB1.Actual,
		VITB2:     &result.VITB2.Actual,
		VITC:      &result.VITC.Actual,
		NA:        &result.Na.Actual,
		FETT:      &result.AllIntake.Fat.Actual,
	}
	result.Grade = s.setGrade(allFood)

	return nil
}

func (s *Data) getSomeDayFood(filter *doctor.NutrientAnalysisFilter) ([]*doctor.DietRecordEx, error) {
	sqlAccess, err := s.sqlDatabase.NewAccess(false)
	if err != nil {
		return nil, err
	}
	defer sqlAccess.Close()

	list := make([]*doctor.DietRecordEx, 0)
	dbEntity := &sqldb.DietRecord{}
	startTime := filter.AnalysisDateTime.ToDate(0)
	endTime := filter.AnalysisDateTime.ToDate(1)
	//fmt.Println(startTime.String())
	//fmt.Println(endTime.String())
	dbFilter := &sqldb.DietRecordFilterEx{
		PatientID:        filter.PatientID,
		EatStartDateTime: startTime,
		EatEndDateTime:   endTime,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	err = sqlAccess.SelectList(dbEntity, func() {
		re := &doctor.DietRecordEx{}
		dbEntity.CopyToEx(re)
		dataList := make([]*doctor.DietContent, 0)
		//
		dbEntity2 := &sqldb.DietContent{}
		dbFilter2 := &sqldb.DietContentFilter{
			DietRecordSerialNo: dbEntity.SerialNo,
		}
		sqlFilter2 := s.sqlDatabase.NewFilter(dbFilter2, false, false)
		_ = s.sqlDatabase.SelectList(dbEntity2, func() {
			re2 := &doctor.DietContent{}
			dbEntity2.CopyTo(re2)
			dataList = append(dataList, re2)
		}, nil, sqlFilter2)
		re.DietContents = dataList
		list = append(list, re)
	}, nil, sqlFilter)
	if err != nil {
		return nil, err
	}
	return list, nil
}

func (s *Data) getFoodComponent(contents []*doctor.DietContent) (*nutrient.Foods, error) {
	sqlaccess, err := s.nSqlDatabase.NewAccess(false)
	if err != nil {
		return nil, err
	}
	defer sqlaccess.Close()
	foods := &nutrient.Foods{}

	for _, item := range contents {
		dbEntity := &nutrientdb.Viewfood{}
		dbFilter := &nutrientdb.FoodFilter{
			Serial_No: item.NutritionSerialNo,
			Source:    item.NutritionSource,
		}
		sqlFilter := s.nSqlDatabase.NewFilter(dbFilter, false, false)
		err = sqlaccess.SelectOne(dbEntity, sqlFilter)
		if err != nil {
			return nil, err
		}
		//grade := s.setGrade(dbEntity)

		if dbEntity.ENERGY_KC != nil {
			foods.ENERGY_KC += *dbEntity.ENERGY_KC * item.MeasureValue / 100
		}
		if dbEntity.FETT != nil {
			foods.FETT += *dbEntity.FETT * item.MeasureValue / 100
		}
		if dbEntity.CARBOHYDRATE != nil {
			foods.CARBOHYDRATE += *dbEntity.CARBOHYDRATE * item.MeasureValue / 100
		}
		if dbEntity.PROTEIN != nil {
			foods.PROTEIN += *dbEntity.PROTEIN * item.MeasureValue / 100
		}
		if dbEntity.DF != nil {
			foods.DF += *dbEntity.DF * item.MeasureValue / 100
		}
		if dbEntity.CA != nil {
			foods.CA += *dbEntity.CA * item.MeasureValue / 100
		}
		if dbEntity.POT != nil {
			foods.POT += *dbEntity.POT * item.MeasureValue / 100
		}
		if dbEntity.MG != nil {
			foods.MG += *dbEntity.MG * item.MeasureValue / 100
		}
		if dbEntity.FE != nil {
			foods.FE += *dbEntity.FE * item.MeasureValue / 100
		}
		if dbEntity.VITA != nil {
			foods.VITA += *dbEntity.VITA * item.MeasureValue / 100
		}
		if dbEntity.VITB1 != nil {
			foods.VITB1 += *dbEntity.VITB1 * item.MeasureValue / 100
		}
		if dbEntity.VITB2 != nil {
			foods.VITB2 += *dbEntity.VITB2 * item.MeasureValue / 100
		}
		if dbEntity.VITC != nil {
			foods.VITC += *dbEntity.VITC * item.MeasureValue / 100
		}
		if dbEntity.VITD != nil {
			foods.VITD += *dbEntity.VITD * item.MeasureValue / 100
		}
		if dbEntity.ZN != nil {
			foods.ZN += *dbEntity.ZN * item.MeasureValue / 100
		}
		if dbEntity.CHO != nil {
			foods.CHO += *dbEntity.CHO * item.MeasureValue / 100
		}
		if dbEntity.USP != nil {
			foods.USP += *dbEntity.USP * item.MeasureValue / 100
		}
		if dbEntity.IODINE != nil {
			foods.IODINE += *dbEntity.IODINE * item.MeasureValue / 100
		}
		if dbEntity.SE != nil {
			foods.SE += *dbEntity.SE * item.MeasureValue / 100
		}
		if dbEntity.VITB6 != nil {
			foods.VITB6 += *dbEntity.VITB6 * item.MeasureValue / 100
		}
		if dbEntity.VITE != nil {
			foods.VITE += *dbEntity.VITE * item.MeasureValue / 100
		}
		if dbEntity.NA != nil {
			foods.NA += *dbEntity.NA * item.MeasureValue / 100
		}
		//foods.Grade += grade
	}
	return foods, nil
}

func (s *Data) setRecommendTraceElement(age int64, sex uint64, result *doctor.NutrientAnalysisResult) {
	result.CHO.RecommendMax = 300 * 1.2 * 1000
	// 胆固醇
	result.DF.RecommendMin = 25
	result.DF.RecommendMax = 30

	s.setTrace(&result.POT, 3600)
	s.setTrace(&result.VITC, 200)
	s.setTrace(&result.VITD, 10)
	s.setTrace(&result.USP, 400)
	s.setTrace(&result.IODINE, 120)
	s.setTrace(&result.SE, 60)
	s.setTrace(&result.VITB6, 1.4)

	s.setTrace(&result.VITE, 14)
	if sex == 1 {
		s.setTrace(&result.FE, 12)
		s.setTrace(&result.ZN, 12.5)
		s.setTrace(&result.VITA, 800)
		s.setTrace(&result.VITB1, 1.4)
		s.setTrace(&result.VITB2, 1.4)
	} else if sex == 2 {
		s.setTrace(&result.FE, 20)
		s.setTrace(&result.ZN, 7.5)
		s.setTrace(&result.VITA, 700)
		s.setTrace(&result.VITB1, 1.2)
		s.setTrace(&result.VITB2, 1.2)
	}
	if age < 50 {
		result.Na.RecommendMax = 2000 * 1.2
		s.setTrace(&result.CA, 800)
		s.setTrace(&result.MG, 300)
	} else if age >= 50 && age < 65 {
		result.Na.RecommendMax = 1900 * 1.2
		s.setTrace(&result.CA, 1000)
		s.setTrace(&result.MG, 300)
	} else if age >= 65 {
		result.Na.RecommendMax = 1800 * 1.2
		s.setTrace(&result.CA, 1000)
		s.setTrace(&result.MG, 320)
	}
}

func (s *Data) setTrace(compare *doctor.NumberCompare, data float64) {
	compare.RecommendMin = data * 0.8
	compare.RecommendMax = data * 1.2
}

func (s *Data) setGrade(food *nutrientdb.Food) float64 {
	// 推荐

	t1 := float64(0)
	t2 := float64(0)
	t3 := float64(0)
	t4 := float64(0)
	t5 := float64(0)
	t6 := float64(0)
	t7 := float64(0)
	t8 := float64(0)
	t9 := float64(0)
	t10 := float64(0)
	t11 := float64(0)
	t12 := float64(0)

	t13 := float64(0)
	t14 := float64(0)
	if food.ENERGY_KC != nil {
		if food.PROTEIN != nil {
			// 蛋白质
			t1 = getTrace(*food.PROTEIN, *food.ENERGY_KC, dataMap["protein"].w, dataMap["protein"].nrv)
		}
		if food.DF != nil {
			// 膳食纤维
			t2 = getTrace(*food.DF, *food.ENERGY_KC, dataMap["df"].w, dataMap["df"].nrv)
		}
		if food.CA != nil {
			// 钙
			t3 = getTrace(*food.CA, *food.ENERGY_KC, dataMap["ca"].w, dataMap["ca"].nrv)
		}
		if food.FE != nil {
			// 铁
			t4 = getTrace(*food.FE, *food.ENERGY_KC, dataMap["fe"].w, dataMap["fe"].nrv)
		}
		if food.MG != nil {
			// 镁
			t6 = getTrace(*food.MG, *food.ENERGY_KC, dataMap["mg"].w, dataMap["mg"].nrv)
		}
		if food.ZN != nil {
			// 锌
			t5 = getTrace(*food.ZN, *food.ENERGY_KC, dataMap["zn"].w, dataMap["zn"].nrv)
		}
		if food.POT != nil {
			// 钾
			t7 = getTrace(*food.POT, *food.ENERGY_KC, dataMap["k"].w, dataMap["k"].nrv)
		}
		if food.VITA != nil {
			// va
			t8 = getTrace(*food.VITA, *food.ENERGY_KC, dataMap["va"].w, dataMap["va"].nrv)
		}
		if food.VITB1 != nil {
			// vb1
			t9 = getTrace(*food.VITB1, *food.ENERGY_KC, dataMap["vb1"].w, dataMap["vb1"].nrv)
		}
		if food.VITB2 != nil {
			// vb2
			t10 = getTrace(*food.VITB2, *food.ENERGY_KC, dataMap["vb2"].w, dataMap["vb2"].nrv)
		}
		if food.VITC != nil {
			// vc
			t11 = getTrace(*food.VITC, *food.ENERGY_KC, dataMap["vc"].w, dataMap["vc"].nrv)
		}
		if food.VITD != nil {
			// vd
			t12 = getTrace(*food.VITD, *food.ENERGY_KC, dataMap["vd"].w, dataMap["vd"].nrv)
		}

		// 限制性
		if food.NA != nil {
			// 钠
			t13 = getTrace(*food.NA, *food.ENERGY_KC, dataMap["na"].w, dataMap["na"].nrv)
		}
		if food.FETT != nil {
			// 脂肪
			t14 = getTrace(*food.FETT, *food.ENERGY_KC, dataMap["fat"].w, dataMap["fat"].nrv)
		}
	}
	tt := t1 + t2 + t3 + t4 + t5 + t6 + t7 + t8 + t9 + t10 + t11 + t12
	tx := t13 + t14

	return tt - tx
}

func getTrace(trace, energy, w, nrv float64) float64 {
	return trace / (energy / 100) * w / nrv * 100
}

func (s *Data) getRecipeCompose(content *doctor.DietContent) []*doctor.DietContent {
	sqlAccess, err := s.nSqlDatabase.NewAccess(false)
	if err != nil {
		return nil
	}
	defer sqlAccess.Close()
	//查菜肴ID
	recipeEntity := &nutrientdb.Recipe_lib{}
	recipeFilter := &nutrientdb.MaxRecipeNo{SerialNo: content.NutritionSerialNo}
	sqlRecipeFilter := s.nSqlDatabase.NewFilter(recipeFilter, false, false)
	err = sqlAccess.SelectOne(recipeEntity, sqlRecipeFilter)
	if err != nil {
		return nil
	}
	//查组成食物有哪些
	composeFilter := &nutrientdb.ComposeFilterByRecipeID{
		RECIPE_ID: recipeEntity.ID,
	}
	sqlComposeFilter := s.nSqlDatabase.NewFilter(composeFilter, false, false)
	composeEntity := &nutrientdb.Recipe_compose{}
	composeList := make([]*nutrient.RecipeCompose, 0)
	total := float64(0)
	_ = sqlAccess.SelectList(composeEntity, func() {
		total = total + composeEntity.WEIGHT
		res := &nutrient.RecipeCompose{}
		composeEntity.CopyTo(res)
		composeList = append(composeList, res)
	}, nil, sqlComposeFilter)
	results := make([]*doctor.DietContent, 0)
	for _, value := range composeList {
		typeEntity := &nutrientdb.Food{}
		typeFilter := &nutrientdb.FoodIDFilter{
			ID: &value.FoodID,
		}
		sqlTypeFilter := s.nSqlDatabase.NewFilter(typeFilter, false, false)
		err = sqlAccess.SelectOne(typeEntity, sqlTypeFilter)
		res := &doctor.DietContent{}
		res.SerialNo = content.SerialNo
		res.NutritionSerialNo = typeEntity.Serial_No
		res.NutritionSource = "food"
		res.FoodName = value.FoodName
		res.Memo = content.Memo
		res.MeasureValue = content.MeasureValue / total * value.WEIGHT
		res.DietRecordSerialNo = content.DietRecordSerialNo
		res.FoodTypeId = *typeEntity.FOOD_TYPE_ID
		res.PatientID = content.PatientID
		//res := &doctor.DietContent{
		//	SerialNo:           content.SerialNo,
		//	NutritionSerialNo:  typeEntity.Serial_No,
		//	NutritionSource:    "food",
		//	FoodName:           value.FoodName,
		//	MeasureValue:       content.MeasureValue / total * value.WEIGHT,
		//	DietRecordSerialNo: content.DietRecordSerialNo,
		//	FoodTypeId:         *typeEntity.FOOD_TYPE_ID,
		//	PatientID:          content.PatientID,
		//	Memo:               content.Memo,
		//}
		results = append(results, res)
	}
	return results
}

func (s *Data) addFoodWeight(kind string, weight *doctor.FoodKindWeight, num float64, count *doctor.DietPlanRecord) {
	//if source == "recipe" {
	//	sqlAccess, err := s.nSqlDatabase.NewAccess(false)
	//	if err != nil {
	//		return
	//	}
	//	defer sqlAccess.Close()
	//	//查菜肴ID
	//	recipeEntity := &nutrientdb.Recipe_lib{}
	//	recipeFilter := &nutrientdb.MaxRecipeNo{SerialNo: sourceID}
	//	sqlRecipeFilter := s.nSqlDatabase.NewFilter(recipeFilter, false, false)
	//	err = sqlAccess.SelectOne(recipeEntity, sqlRecipeFilter)
	//	//查组成食物有哪些
	//	composeFilter := &nutrientdb.ComposeFilterByRecipeID{
	//		RECIPE_ID: recipeEntity.ID,
	//	}
	//	sqlComposeFilter := s.nSqlDatabase.NewFilter(composeFilter, false, false)
	//	composeEntity := &nutrientdb.Recipe_compose{}
	//	composeList := make([]*nutrient.RecipeCompose, 0)
	//	_ = sqlAccess.SelectList(composeEntity, func() {
	//		res := &nutrient.RecipeCompose{}
	//		composeEntity.CopyTo(res)
	//		composeList = append(composeList, res)
	//	}, nil, sqlComposeFilter)
	//	//查组成食物的food_type_id
	//	kinds := make([]string, 0)
	//	nums := make([]float64, 0)
	//	fen, err := strconv.ParseFloat(kind,64)
	//	ratio := num / fen
	//	for _, compose := range composeList {
	//		typeEntity := &nutrientdb.Food{}
	//		typeFilter := &nutrientdb.FoodIDFilter{
	//			ID: &compose.FoodID,
	//		}
	//		sqlTypeFilter := s.nSqlDatabase.NewFilter(typeFilter, false, false)
	//		err = sqlAccess.SelectOne(typeEntity, sqlTypeFilter)
	//		kinds = append(kinds, *typeEntity.FOOD_TYPE_ID)
	//		nums = append(nums, ratio * compose.WEIGHT)
	//	}
	//	for i, k := range kinds {
	//		s.CountAndWeight(k, weight, nums[i], count)
	//	}
	//} else {
	s.CountAndWeight(kind, weight, num, count)

}

func (s *Data) CountAndWeight(kind string, weight *doctor.FoodKindWeight, num float64, count *doctor.DietPlanRecord) {
	oil := []string{"4001", "4002", "4000"}
	salt := []string{"4207"}
	sugar := []string{"3901", "3902"}
	dairy := []string{"3101", "3703", "3702", "3703", "3104", "3105", "3106", "3100"}
	soybean := []string{"2401"}
	nut := []string{"2801", "2802", "2800"}
	livestockPoultry := []string{"2901", "2902", "2903", "2904", "2905", "2906", "3001", "3002", "3003", "3004", "3005", "2900", "3000"}
	fish := []string{"3301", "3302", "3304", "3305", "3300"}
	egg := []string{"3201", "3202", "3203", "3204", "3200"}
	vegetable := []string{"2501", "2502", "2503", "2504", "2505", "2506", "2507", "2508", "2601", "2602", "2500", "2600"}
	fruits := []string{"2701", "2702", "2703", "2704", "2705", "2706", "2700"}
	potatoes := []string{"2201", "2202", "2203", "2204", "2205", "2206", "2301", "2302", "2402", "2403", "2404", "2405", "2200", "2300"}

	//饮食目标新增分类
	sweet := []string{"3500", "3501", "3502"}
	drink := []string{"3701", "3702"}
	mushroom := []string{"2600", "2601"}

	if s.MatchFoodType(oil, kind) {
		weight.Oil += num
	} else if s.MatchFoodType(salt, kind) {
		weight.Salt += num
	} else if s.MatchFoodType(sugar, kind) {
		weight.Sugar += num
	} else if s.MatchFoodType(dairy, kind) {
		weight.Dairy += num
		count.Milk += 1
	} else if s.MatchFoodType(soybean, kind) {
		weight.Soybean += num
		count.Bean += 1
	} else if s.MatchFoodType(nut, kind) {
		weight.Nut += num
	} else if s.MatchFoodType(livestockPoultry, kind) {
		weight.LivestockPoultry += num
	} else if s.MatchFoodType(fish, kind) {
		weight.Fish += num
		count.Fish += 1
	} else if s.MatchFoodType(egg, kind) {
		weight.Egg += num
	} else if s.MatchFoodType(vegetable, kind) {
		weight.Vegetable += num
		count.Vegetable += 1
	} else if s.MatchFoodType(fruits, kind) {
		weight.Fruits += num
		count.Fruit += 1
	} else if s.MatchFoodType(potatoes, kind) {
		weight.Potatoes += num
		count.Grain += 1
	} else if s.MatchFoodType(sweet, kind) {
		count.Sweet += 1
	} else if s.MatchFoodType(drink, kind) {
		count.Drink += 1
	} else if s.MatchFoodType(mushroom, kind) {
		count.Mushroom += 1
	}
	kindNum := make([]string, 0)
	if weight.KindNum != nil {
		kindNum = weight.KindNum
	} else {
		kindNum = make([]string, 0)
	}
	kindNum = append(kindNum, kind)
	weight.KindNum = kindNum
}

func (s *Data) MatchFoodType(types []string, typeId string) bool {
	for _, v := range types {
		if v == typeId {
			return true
		}
	}
	return false
}

func (s *Data) WaterPlace(filter *doctor.NutrientAnalysisFilter, result *doctor.NutrientAnalysisResult) error {
	sqlAccess, err := s.sqlDatabase.NewAccess(false)
	if err != nil {
		return err
	}
	defer sqlAccess.Close()

	startTime := filter.AnalysisDateTime.ToDate(0)
	endTime := filter.AnalysisDateTime.ToDate(1)
	placeFilter := &doctor.SportDietRecordDataFilterEx{
		PatientID: filter.PatientID,
		SportDietRecordDataFilter: doctor.SportDietRecordDataFilter{
			HappenStartDate: (*types.Time)(startTime),
			HappenEndDate:   (*types.Time)(endTime),
		},
	}
	placeList, _ := s.ListDietRecord(placeFilter)
	placeCount := 0
	for _, v := range placeList {
		if v.HappenPlace == "聚餐" {
			placeCount += 1
		}
	}

	waterEntity := &sqldb.WeightRecord{}
	waterFilter := &sqldb.WeightRecordDataFilter{
		PatientID:        filter.PatientID,
		MeasureStartDate: startTime,
		MeasureEndDate:   endTime,
	}
	sqlFilter := s.sqlDatabase.NewFilter(waterFilter, false, false)
	err = sqlAccess.SelectOne(waterEntity, sqlFilter)
	waterCount := 0
	if err != nil {
		if s.sqlDatabase.IsNoRows(err) {
			waterCount = 0
		} else {
			return err
		}
	}
	if waterEntity.Water != nil && *waterEntity.Water > 2000 {
		waterCount = 1
	}
	result.DietPlan.Water = uint64(waterCount)
	result.DietPlan.Place = uint64(placeCount)

	return nil
}
