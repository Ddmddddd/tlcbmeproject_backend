package impl

import (
	"tlcbme_project/business"
	"tlcbme_project/data/entity/sqldb"
	"tlcbme_project/data/model/doctor"
	"tlcbme_project/data/model/manage"
	"tlcbme_project/errors"
	"tlcbme_project/server/config"
	"tlcbme_project/server/notify"
	"fmt"
	"github.com/ktpswjz/httpserver/types"
	"strings"
	"time"
)

func (s *Data) CreateBloodGlucoseRecord(argument *doctor.BloodGlucoseRecordCreateEx) (uint64, business.Error) {
	if argument == nil {
		return 0, business.NewError(errors.InternalError, fmt.Errorf("参数为空"))
	}

	dbEntity := &sqldb.BloodGlucoseRecord{}
	dbEntity.CopyFromCreateEx(argument)
	now := time.Now()
	dbEntity.InputDateTime = &now
	if dbEntity.MeasureDateTime == nil {
		dbEntity.MeasureDateTime = &now
	}
	measureDateTime := types.Time(*dbEntity.MeasureDateTime)
	go func(patientId uint64, dataType string, time *types.Time) {
		s.uploadDataToPlatform(patientId, dataType, true, false, time)
	}(dbEntity.PatientID, "血糖监测", &measureDateTime)

	return s.saveBloodGlucoseRecord(dbEntity)
}

func (s *Data) saveBloodGlucoseRecord(dbEntity *sqldb.BloodGlucoseRecord) (uint64, business.Error) {
	if dbEntity == nil {
		return 0, business.NewError(errors.InternalError, fmt.Errorf("参数为空"))
	}

	sqlAccess, err := s.sqlDatabase.NewAccess(true)
	if err != nil {
		return 0, business.NewError(errors.InternalError, err)
	}
	defer sqlAccess.Close()

	manageIndex, err := s.getManagedIndex(sqlAccess, dbEntity.PatientID)
	noManageIndex := false
	if err != nil {
		if sqlAccess.IsNoRows(err) {
			//return 0, business.NewError(errors.InternalError, fmt.Errorf("患者ID(%d)不存在", dbEntity.PatientID))
			noManageIndex = true
		} else {
			return 0, business.NewError(errors.InternalError, err)
		}
	}

	sno := dbEntity.SerialNo
	update := false
	if dbEntity.SerialNo > 0 {
		_, err = sqlAccess.UpdateSelectiveByPrimaryKey(dbEntity)
		update = true
	} else {
		sno, err = sqlAccess.InsertSelective(dbEntity)
	}
	if err != nil {
		return 0, business.NewError(errors.InternalError, err)
	}
	if !noManageIndex {
		if dbEntity.TimePoint != nil {
			ext := &doctor.ManagedPatientIndexExt{}
			manageIndex.CopyToExt(ext)
			needUpdate := false

			if strings.Contains(*dbEntity.TimePoint, "空腹") {
				ext.Dm.Fpg = &dbEntity.BloodGlucose
				needUpdate = true
			} else if strings.Contains(*dbEntity.TimePoint, "餐后") {
				ext.Dm.Ppg = &dbEntity.BloodGlucose
				needUpdate = true
			}

			if needUpdate {
				dbEntityExtUpdate := &sqldb.ManagedPatientIndexExtUpdate{}
				dbEntityExtUpdate.CopyFrom(ext)
				dbEntityExtUpdate.SerialNo = manageIndex.SerialNo

				_, err = sqlAccess.UpdateByPrimaryKey(dbEntityExtUpdate)
				if err != nil {
					return 0, business.NewError(errors.InternalError, err)
				}
			}
		}
	}

	err = sqlAccess.Commit()
	if err != nil {
		return 0, business.NewError(errors.InternalError, err)
	}

	go func(sn uint64, patientId uint64) {
		if update {
			providerType := config.ManagementProviderTypeDm
			uri := s.cfg.Management.Providers.Dm.Api.Uri.DeleteRecord
			assess := &manage.DeleteBloodGlucoseRecordInput{
				PatientID: fmt.Sprint(patientId),
				SerialNo:  fmt.Sprint(sno),
			}
			s.client.ManageBusinessPost("删除患者血糖数据", patientId, providerType, uri, assess)
		}
		s.chBloodGlucoseRecord <- sn
	}(sno, dbEntity.PatientID)

	s.WriteNotify(&notify.Message{
		BusinessID: notify.BusinessDoctor,
		NotifyID:   notify.DoctorUpdateBloodGlucose,
		Time:       types.Time(time.Now()),
		Data:       dbEntity.PatientID,
	})

	return sno, nil
}
