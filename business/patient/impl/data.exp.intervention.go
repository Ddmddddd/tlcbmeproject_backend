package impl

import (
	"encoding/json"
	"fmt"
	"github.com/ktpswjz/httpserver/types"
	"math"
	"strings"
	"time"
	"tlcbme_project/business"
	"tlcbme_project/data/entity/sqldb"
	"tlcbme_project/data/model/doctor"
	"tlcbme_project/errors"
)

func (s *Data) GetNutritionIntervention(patientID uint64) (*doctor.ExpPatientNutritionInterventionTotal, business.Error) {
	sqlAccess, err := s.sqlDatabase.NewAccess(true)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}
	defer sqlAccess.Close()
	//获取基本营养数据
	dbEntity := &sqldb.ExpPatientNutritionalIndicator{}
	dbFilter := &sqldb.PatientUserBaseInfoUserIdFilter{
		UserID: patientID,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	err = sqlAccess.SelectOne(dbEntity, sqlFilter)
	if err != nil {
		//if s.sqlDatabase.IsNoRows(err) {
		//	dbEntity,_ = s.CreateNutritionIntervention(patientID)
		//} else {
		return nil, business.NewError(errors.InternalError, err)
		//}
	}
	indicatorResult := &doctor.ExpPatientNutritionalIndicator{}
	dbEntity.CopyTo(indicatorResult)
	//获取饮食习惯列表
	planResults := make([]*doctor.ExpPatientDietPlanFour, 0)
	planEntity := &sqldb.ExpPatientDietPlanFour{}
	planFilter := &sqldb.ExpPatientDietPlanFilter{
		PatientID: patientID,
	}
	sqlFilter = s.sqlDatabase.NewFilter(planFilter, false, false)
	err = sqlAccess.SelectList(planEntity, func() {
		planResult := &doctor.ExpPatientDietPlanFour{}
		planEntity.CopyToFour(planResult)
		planResults = append(planResults, planResult)
	}, nil, sqlFilter)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}
	//获取饮食方案列表
	programResults := make([]*doctor.ExpPatientDietProgram, 0)
	if indicatorResult.DietProgramID != nil {
		programResults, _ = s.getProgramContent(patientID)
	}
	//programID := indicatorResult.DietProgramID
	//programEntity := &sqldb.ExpDietProgramRepository{}
	//programFilter := &sqldb.ExpDietProgramRepositoryFilter{
	//	ProgramID: programID,
	//}
	//sqlFilter = s.sqlDatabase.NewFilter(programFilter, false, false)
	//programResults := make([]*doctor.ExpDietProgramRepository, 0)
	//err = sqlAccess.SelectList(programEntity, func() {
	//	programResult := &doctor.ExpDietProgramRepository{}
	//	programEntity.CopyTo(programResult)
	//	programResults = append(programResults, programResult)
	//}, nil, sqlFilter)
	//if err != nil {
	//	return nil, business.NewError(errors.InternalError, err)
	//}
	//整合数据输出
	result := &doctor.ExpPatientNutritionInterventionTotal{
		GoalEnergy:          indicatorResult.GoalEnergy,
		GoalCarbohydrateMin: indicatorResult.GoalCarbohydrateMin,
		GoalCarbohydrateMax: indicatorResult.GoalCarbohydrateMax,
		GoalFatMin:          indicatorResult.GoalFatMin,
		GoalFatMax:          indicatorResult.GoalFatMax,
		GoalProteinMin:      indicatorResult.GoalProteinMin,
		GoalProteinMax:      indicatorResult.GoalProteinMax,
		GoalWeight:          indicatorResult.GoalWeight,
		GoalProtein:         indicatorResult.GoalProtein,
		GoalDF:              indicatorResult.GoalDF,
		DietProgramID:       indicatorResult.DietProgramID,
		DietPlanList:        planResults,
		DietProgramList:     programResults,
	}
	err = sqlAccess.Commit()
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}
	return result, nil

}

func (s *Data) UpdateNutritionIntervention(argument *doctor.ExpPatientNutritionalIndicator) business.Error {
	sqlAccess, err := s.sqlDatabase.NewAccess(true)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}
	defer sqlAccess.Close()

	//如果更改了能量值就修改饮食方案，但是不强求体重和能量值之间的对应关系
	x := (*argument.GoalEnergy - 800) / 100
	program := math.Round(x)
	if program <= 0 {
		program = 0
	} else if program > 13 {
		program = 13
	}
	programID := uint64(program)
	argument.DietProgramID = &programID

	dbEntity := &sqldb.ExpPatientNutritionalIndicator{}
	dbEntity.CopyFrom(argument)
	dbFilter := &sqldb.ExpPatientNutritionIndicatorFilterByUserID{
		UserID: argument.UserID,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)

	_, err = sqlAccess.UpdateSelective(dbEntity, sqlFilter)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}
	err = sqlAccess.Commit()
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}
	return nil
}

//这是版本1的方法
func (s *Data) CreateNutritionIntervention(patientID uint64) (*sqldb.ExpPatientNutritionalIndicator, business.Error) {
	sqlAccess, err := s.sqlDatabase.NewAccess(true)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}
	defer sqlAccess.Close()

	dbPatientInfoEntity := &sqldb.PatientUserBaseInfo{}
	dbFilter := &sqldb.PatientUserBaseInfoUserIdFilter{
		UserID: patientID,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	err = sqlAccess.SelectOne(dbPatientInfoEntity, sqlFilter)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}

	age := int64(18)
	if dbPatientInfoEntity.DateOfBirth != nil {
		age2 := time.Now().Sub(*dbPatientInfoEntity.DateOfBirth)
		age = int64(age2.Hours() / (24 * 365))
	}
	sex := uint64(1)
	if dbPatientInfoEntity.Sex != nil {
		sex = *dbPatientInfoEntity.Sex
	}
	labourIntensity := "轻度"
	if dbPatientInfoEntity.JobType != nil {
		labourIntensity = *dbPatientInfoEntity.JobType
	}
	energy := s.getRecommendEnergy(age, sex, labourIntensity)
	pmin := float64(10)
	pmax := float64(15)
	cmin := float64(50)
	cmax := float64(65)
	fmin := float64(20)
	fmax := float64(30)
	dbIndicator := &sqldb.ExpPatientNutritionalIndicator{
		GoalEnergy:          &energy,
		GoalProteinMin:      &pmin,
		GoalProteinMax:      &pmax,
		GoalCarbohydrateMin: &cmin,
		GoalCarbohydrateMax: &cmax,
		GoalFatMin:          &fmin,
		GoalFatMax:          &fmax,
		UserID:              patientID,
	}

	_, err = sqlAccess.InsertSelective(dbIndicator)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}

	err = sqlAccess.Commit()
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}
	return dbIndicator, nil
}

func (s *Data) GetNutritionInterventionQuestion() ([]*doctor.ExpNutritionQuestionRepository, business.Error) {
	results := make([]*doctor.ExpNutritionQuestionRepository, 0)
	dbEntity := &sqldb.ExpNutritionQuestionRepository{}
	err := s.sqlDatabase.SelectList(dbEntity, func() {
		result := &doctor.ExpNutritionQuestionRepository{}
		dbEntity.CopyTo(result)
		results = append(results, result)
	}, nil)
	if err != nil {
		return results, business.NewError(errors.InternalError, err)
	}
	return results, nil
}

func (s *Data) ResultNutritionInterventionQuestion(argument *doctor.ExpPatientNutritionalQuestionInput) business.Error {
	sqlAccess, err := s.sqlDatabase.NewAccess(true)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}
	defer sqlAccess.Close()
	//先更新基本信息
	infoEntity := &sqldb.PatientUserInfoUpdateQuestion{}
	infoEntity.CopyFromQuestionInput(argument)
	infoFilter := &sqldb.PatientUserBaseInfoUserIdFilter{
		UserID: argument.UserID,
	}
	sqlFilter := s.sqlDatabase.NewFilter(infoFilter, false, false)
	_, err = sqlAccess.Update(infoEntity, sqlFilter)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}
	//存储用户问卷记录 新增question表
	//查询问卷题目和答案对应文本存在数据库看比较方便
	answerStr, _ := s.NutritionInterventionQuestionContent(argument.QuestionList, argument.AnswerList)
	quesEntity := &sqldb.ExpPatientNutritionQuestion{}
	quesEntity.CopyFromQuestionInput(argument)
	quesEntity.Answer = &answerStr
	_, err = sqlAccess.Insert(quesEntity)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}
	//新写一个计算方法 新增/更新indicator表
	indiResult, _ := s.calculateIndicator(argument)
	indiEntity := &sqldb.ExpPatientNutritionalIndicator{}
	indiEntity.CopyFrom(indiResult)
	//sindiEntity := &sqldb.ExpPatientNutritionalIndicator{}
	//err = sqlAccess.SelectOne(sindiEntity, sqlFilter)

	value, err := sqlAccess.UpdateSelective(indiEntity, sqlFilter)
	if value == 0 {
		_, err = sqlAccess.InsertSelective(indiEntity)
		if err != nil {
			return business.NewError(errors.InternalError, err)
		}
	}
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}
	//新写一个方法 推断要哪些饮食习惯 并新增/更新dietplan表 之前的全删除
	planDelEntity := &sqldb.ExpPatientDietPlan{}
	planDelFilter := &sqldb.ExpPatientDietPlanFilter{
		PatientID: argument.UserID,
	}
	sqlDelFilter := s.sqlDatabase.NewFilter(planDelFilter, false, false)
	_, err = sqlAccess.Delete(planDelEntity, sqlDelFilter)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}
	planResults := make([]*doctor.ExpDietPlanRepository, 0)
	planResults, _ = s.calculateDietPlan(argument)
	//planFourResults := make([]*doctor.ExpPatientDietPlanFour, 0)
	dateTime := time.Time(*argument.InputDate)
	for index, value := range planResults {
		//planFourResult := &doctor.ExpPatientDietPlanFour{
		//	SerialNo: value.SerialNo,
		//	PatientID: *argument.UserID,
		//	DietPlan: value.Plan,
		//	PicUrl: value.PicUrl,
		//	StartDate: argument.InputDate,
		//}
		//planFourResults = append(planFourResults, planFourResult)
		fmt.Println(index)
		planEntity := &sqldb.ExpPatientDietPlan{
			PatientID: argument.UserID,
			DietPlan:  value.Plan,
			PicUrl:    value.PicUrl,
			StartDate: &dateTime,
			Level:     "重要",
		}
		_, err = sqlAccess.InsertSelective(planEntity)
		if err != nil {
			return business.NewError(errors.InternalError, err)
		}
	}
	err = sqlAccess.Commit()
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}
	//获取饮食方案数据
	//programResults := make([]*doctor.ExpDietProgramRepository, 0)
	//programResults,_ = s.getProgramContent(*indiResult.DietProgramID)
	//result := &doctor.ExpPatientNutritionInterventionTotal{
	//	GoalEnergy: indiResult.GoalEnergy,
	//	GoalCarbohydrateMin: indiResult.GoalCarbohydrateMin,
	//	GoalCarbohydrateMax: indiResult.GoalCarbohydrateMax,
	//	GoalFatMin: indiResult.GoalFatMin,
	//	GoalFatMax: indiResult.GoalFatMax,
	//	GoalProteinMin: indiResult.GoalProteinMin,
	//	GoalProteinMax: indiResult.GoalProteinMax,
	//	GoalWeight: indiResult.GoalWeight,
	//	GoalProtein: indiResult.GoalProtein,
	//	GoalDF: indiResult.GoalDF,
	//	DietProgramID: indiResult.DietProgramID,
	//	DietPlanList: planFourResults,
	//	DietProgramList: programResults,
	//}
	return nil
}

func (s *Data) calculateIndicator(argument *doctor.ExpPatientNutritionalQuestionInput) (*doctor.ExpPatientNutritionalIndicator, business.Error) {
	actualWeight := *argument.Weight
	actualHeight := float64(*argument.Height)

	rate := 0.85
	sex := uint64(1)
	if argument.Sex != nil {
		sex = *argument.Sex
	}
	if sex == 1 {
		rate = 0.9
	}
	weight := (actualHeight - 100) * rate

	bmi := actualWeight / (actualHeight * actualHeight / 10000)
	energy := 1600.0
	pmin := float64(15)
	pmax := float64(20)
	cmin := float64(40)
	cmax := float64(55)
	fmin := float64(20)
	fmax := float64(30)
	if bmi >= 24 {
		if argument.JobType == "重度" {
			rate = 35
		} else if argument.JobType == "中等" {
			rate = 30
		} else {
			rate = 22.5
		}
		energy = weight * rate
	} else {
		age := int64(18)
		if argument.DateOfBirth != nil {
			age2 := time.Now().Sub(time.Time(*argument.DateOfBirth))
			age = int64(age2.Hours() / (24 * 365))
		}
		labourIntensity := argument.JobType
		energy = s.getRecommendEnergy(age, sex, labourIntensity)
		pmin = float64(10)
		pmax = float64(15)
		cmin = float64(50)
		cmax = float64(65)
		fmin = float64(20)
		fmax = float64(30)
	}

	protein := actualWeight * 1.35
	df := 27.5

	//计算饮食方案ID
	x := (energy - 800) / 100
	program := math.Round(x)
	if program <= 0 {
		program = 0
	} else if program > 13 {
		program = 13
	}
	programID := uint64(program)

	//把饮食方案存储到个人的饮食方案表
	s.addUserDietProgram(programID, argument.UserID)

	result := &doctor.ExpPatientNutritionalIndicator{
		UserID:              argument.UserID,
		GoalEnergy:          &energy,
		GoalProteinMin:      &pmin,
		GoalProteinMax:      &pmax,
		GoalCarbohydrateMin: &cmin,
		GoalCarbohydrateMax: &cmax,
		GoalFatMin:          &fmin,
		GoalFatMax:          &fmax,
		GoalWeight:          &weight,
		GoalProtein:         &protein,
		GoalDF:              &df,
		DietProgramID:       &programID,
		UpdateTime:          argument.InputDate,
	}
	return result, nil
}

func (s *Data) addUserDietProgram(program uint64, userID uint64) business.Error {
	sqlAccess, err := s.sqlDatabase.NewAccess(true)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}
	defer sqlAccess.Close()
	//获取饮食方案模板 然后存入用户个人饮食方案的表 要先删掉之前的

	planDelEntity := &sqldb.ExpPatientDietProgram{}
	planDelFilter := &sqldb.ExpPatientDietProgramFilter{
		PatientID: &userID,
	}
	sqlDelFilter := s.sqlDatabase.NewFilter(planDelFilter, false, false)
	_, err = sqlAccess.Delete(planDelEntity, sqlDelFilter)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}

	modelEntity := &sqldb.ExpDietProgramRepository{}
	modelFilter := &sqldb.ExpDietProgramRepositoryFilter{
		ProgramID: &program,
	}
	modelResults := make([]*doctor.ExpDietProgramRepository, 0)
	sqlModelFilter := s.sqlDatabase.NewFilter(modelFilter, false, false)
	err = sqlAccess.SelectList(modelEntity, func() {
		result := &doctor.ExpDietProgramRepository{}
		modelEntity.CopyTo(result)
		modelResults = append(modelResults, result)
	}, nil, sqlModelFilter)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}
	dateTime := types.Time(time.Now())
	for index, value := range modelResults {
		userResult := &doctor.ExpPatientDietProgram{
			PatientID:  &userID,
			MealTime:   value.MealTime,
			Recipe:     value.Recipe,
			Food:       value.Food,
			Quantity:   value.Quantity,
			Estimate:   value.Estimate,
			Memo:       value.Memo,
			UpdateDate: &dateTime,
		}
		fmt.Println(index)
		userEntity := &sqldb.ExpPatientDietProgram{}
		userEntity.CopyFrom(userResult)
		_, err = sqlAccess.InsertSelective(userEntity)
		if err != nil {
			return business.NewError(errors.InternalError, err)
		}
	}
	err = sqlAccess.Commit()
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}
	return nil
}

func (s *Data) calculateDietPlan(argument *doctor.ExpPatientNutritionalQuestionInput) ([]*doctor.ExpDietPlanRepository, business.Error) {
	sqlAccess, err := s.sqlDatabase.NewAccess(true)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}
	defer sqlAccess.Close()

	var answerA []uint64
	var answerB []uint64
	var answerC []uint64
	var questionIn []uint64
	for index, value := range argument.AnswerList {
		switch value {
		case 0:
			answerA = append(answerA, argument.QuestionList[index])
			break
		case 1:
			answerB = append(answerB, argument.QuestionList[index])
			break
		case 2:
			answerC = append(answerC, argument.QuestionList[index])
			break
		}
	}
	if len(answerC) > 0 {
		if len(answerC) > 3 {
			questionIn = answerC[:4]
		} else {
			if len(answerB) > 0 {
				questionIn = answerC
				i := 0
				for !(len(questionIn) == 4 || i == len(answerB)) {
					questionIn = append(questionIn, answerB[i])
					i++
				}
			} else {
				questionIn = answerC
			}
		}
	} else {
		if len(answerB) > 0 {
			if len(answerB) > 3 {
				questionIn = answerB[:4]
			} else {
				questionIn = answerB
			}
		}
	}
	results := make([]*doctor.ExpDietPlanRepository, 0)
	//for index := range questionIn {
	dbEntity := &sqldb.ExpDietPlanRepository{}
	dbFilter := &sqldb.ExpDietPlanRepositoryFilter{
		Level: questionIn,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	err = sqlAccess.SelectList(dbEntity, func() {
		result := &doctor.ExpDietPlanRepository{}
		dbEntity.CopyTo(result)
		results = append(results, result)
	}, nil, sqlFilter)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}
	//}
	err = sqlAccess.Commit()
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}
	return results, nil
}

func (s *Data) getProgramContent(id uint64) ([]*doctor.ExpPatientDietProgram, business.Error) {
	programEntity := &sqldb.ExpPatientDietProgram{}
	programFilter := &sqldb.ExpPatientDietProgramFilter{
		PatientID: &id,
	}
	sqlFilter := s.sqlDatabase.NewFilter(programFilter, false, false)
	programResults := make([]*doctor.ExpPatientDietProgram, 0)
	err := s.sqlDatabase.SelectList(programEntity, func() {
		programResult := &doctor.ExpPatientDietProgram{}
		programEntity.CopyTo(programResult)
		programResults = append(programResults, programResult)
	}, nil, sqlFilter)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}
	return programResults, nil
}

func (s *Data) IfNutritionInterventionQuestion(argument *doctor.ExpPatientNutritionalQuestionFilter) (bool, business.Error) {
	dbEntity := &sqldb.ExpPatientNutritionQuestion{}
	dbFilter := &sqldb.ExpPatientNutritionQuestionFilter{
		PatientID: argument.PatientID,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	err := s.sqlDatabase.SelectOne(dbEntity, sqlFilter)
	if err != nil {
		if s.sqlDatabase.IsNoRows(err) {
			return false, nil
		}
		return false, business.NewError(errors.InternalError, err)
	}
	return true, nil
}

func (s *Data) NutritionInterventionQuestionContent(questions []uint64, answers []uint64) (string, business.Error) {
	dbEntity := &sqldb.ExpNutritionQuestionRepository{}
	questionsList := make([]*doctor.ExpNutritionQuestionRepository, 0)
	resultsMap := make(map[string]string)
	err := s.sqlDatabase.SelectList(dbEntity, func() {
		question := &doctor.ExpNutritionQuestionRepository{}
		dbEntity.CopyTo(question)
		questionsList = append(questionsList, question)
	}, nil)
	if err != nil {
		return "", business.NewError(errors.InternalError, err)
	}
	var i, j int
	for i = 0; i < len(questions); i++ {
		qno := questions[i]
		ano := answers[i]
		var qContent, aContent string
		for j = 0; j < len(questionsList); j++ {
			if questionsList[j].SerialNo == qno {
				qContent = questionsList[j].Title
				answersList := strings.Split(questionsList[j].Option, string('，'))
				aContent = answersList[ano]
				break
			}
		}
		resultsMap[qContent] = aContent
	}
	resultsJson, _ := json.Marshal(resultsMap)
	resultsStr := string(resultsJson)
	return resultsStr, nil
}

func (s *Data) CreateNutritionInterventionReport(dateTime *time.Time) {
	patientList := make([]*sqldb.PatientUserBaseInfoMonthly, 0)
	dbEntity := &sqldb.PatientUserBaseInfoMonthly{}
	_ = s.sqlDatabase.SelectList(dbEntity, func() {
		temp := &sqldb.PatientUserBaseInfoMonthly{
			UserID: dbEntity.UserID,
			Height: dbEntity.Height,
		}
		patientList = append(patientList, temp)
	}, nil)
	ch := make(chan int, 100)
	for _, v := range patientList {
		ch <- 1
		go s.workerNI(ch, dateTime, v)
	}
}

func (s *Data) workerNI(ch chan int, dateTime *time.Time, v *sqldb.PatientUserBaseInfoMonthly) {
	sqlAccess, err := s.sqlDatabase.NewAccess(false)
	if err != nil {
		return
	}
	defer sqlAccess.Close()
	//weekTime := make([]*time.Time, 0)
	//lastDay := *dateTime
	//for i := 0; i < 7; i++ {
	//	lastDay = lastDay.AddDate(0, 0, -1)
	//	weekTime = append(weekTime, &lastDay)
	//}
	startDate := dateTime.AddDate(0, 0, -6)
	//endDate := dateTime.AddDate(0, 0, 0)
	date1 := types.Time(startDate)
	date2 := types.Time(*dateTime)

	results := make([]*doctor.NutritionalAnalysisRecord, 0)
	start := date1.ToDate(0)
	end := date2.ToDate(1)
	dbFilter := &sqldb.NutritionalAnalysisRecordFilterEx{
		PatientID:     v.UserID,
		StartDateTime: start,
		EndDateTime:   end,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	dbEntity := &sqldb.NutritionalAnalysisRecord{}
	err = sqlAccess.SelectList(dbEntity, func() {
		result := &doctor.NutritionalAnalysisRecord{}
		dbEntity.CopyTo(result)
		results = append(results, result)
	}, nil, sqlFilter)
	if err != nil {
	}

	if len(results) != 0 {
		s.CreateNutritionInterventionReportRecord(results, v.UserID, start, end)
	}

	<-ch
}

func (s *Data) CreateNutritionInterventionReportRecord(results []*doctor.NutritionalAnalysisRecord, userID uint64, start *time.Time, end *time.Time) {
	sqlAccess, err := s.sqlDatabase.NewAccess(false)
	if err != nil {
		return
	}
	defer sqlAccess.Close()

	count := uint64(len(results))
	energy := float64(0)
	protein := float64(0)
	fat := float64(0)
	carbohydrate := float64(0)
	df := float64(0)

	habitResult := &doctor.DietPlanRecord{
		Grain:     0,
		Vegetable: 0,
		Fruit:     0,
		Oil:       0,
		Sweet:     0,
		Drink:     0,
		Pickle:    0,
		Milk:      0,
		Fish:      0,
		Bean:      0,
		Mushroom:  0,
		Water:     0,
		Place:     0,
	}

	for _, result := range results {
		energy = energy + result.Energy
		protein = protein + *result.Protein
		fat = fat + *result.Fat
		carbohydrate = carbohydrate + *result.Carbohydrate
		df = df + *result.DietaryFiber
		habitResult.Grain += result.DietPlan.Grain
		habitResult.Vegetable += result.DietPlan.Vegetable
		habitResult.Fruit += result.DietPlan.Fruit
		habitResult.Oil += result.DietPlan.Oil
		habitResult.Sweet += result.DietPlan.Sweet
		habitResult.Drink += result.DietPlan.Drink
		habitResult.Pickle += result.DietPlan.Pickle
		habitResult.Milk += result.DietPlan.Milk
		habitResult.Fish += result.DietPlan.Fish
		habitResult.Bean += result.DietPlan.Bean
		habitResult.Mushroom += result.DietPlan.Mushroom
		habitResult.Water += result.DietPlan.Water
		habitResult.Place += result.DietPlan.Place
	}

	energy = energy / float64(count)
	protein = protein / float64(count)
	fat = fat / float64(count)
	carbohydrate = carbohydrate / float64(count)
	df = df / float64(count)

	//获取饮食目标列表
	planResults := make([]string, 0)
	planEntity := &sqldb.ExpPatientDietPlanFour{}
	planFilter := &sqldb.ExpPatientDietPlanFilter{
		PatientID: userID,
	}
	sqlFilter := s.sqlDatabase.NewFilter(planFilter, false, false)
	err = sqlAccess.SelectList(planEntity, func() {
		//planResult := &doctor.ExpPatientDietPlanFour{}
		//planEntity.CopyToFour(planResult)
		planResults = append(planResults, planEntity.DietPlan)
	}, nil, sqlFilter)
	if err != nil {
		return
	}

	DietPlanReport := make(map[string]uint64)
	for _, plan := range planResults {
		s.CreateDietPlanReport(plan, DietPlanReport, habitResult)
	}
	HabitJson, _ := json.Marshal(DietPlanReport)
	HabitStr := string(HabitJson)
	dbEntity := &sqldb.ExpNutritionInterventionRecord{
		PatientID:    &userID,
		StartDate:    start,
		EndDate:      end,
		RecordCount:  &count,
		Energy:       &energy,
		Protein:      &protein,
		Fat:          &fat,
		Carbohydrate: &carbohydrate,
		DF:           &df,
		Habit:        &HabitStr,
	}

	_, err = sqlAccess.InsertSelective(dbEntity)
	if err != nil {

	}

}

func (s *Data) CreateDietPlanReport(plan string, report map[string]uint64, result *doctor.DietPlanRecord) {
	if plan == "每天至少有1餐吃粗粮及全谷物" {
		report[plan] = result.Grain
	} else if plan == "每天吃3种及以上蔬菜" {
		report[plan] = result.Vegetable
	} else if plan == "每天吃1-2种水果" {
		report[plan] = result.Fruit
	} else if plan == "每天喝水2000毫升及以上" {
		report[plan] = result.Water
	} else if plan == "每周油炸食品不超过2次" {
		report[plan] = result.Oil
	} else if plan == "每周甜点不超过2次" {
		report[plan] = result.Sweet
	} else if plan == "每周聚餐不超过2次" {
		report[plan] = result.Place
	} else if plan == "每周喝含糖饮料不超过2次" {
		report[plan] = result.Drink
	} else if plan == "每周腌肉、火腿、咸菜等不超过2次" {
		report[plan] = result.Pickle
	} else if plan == "每天喝1杯（300毫升左右）牛奶" {
		report[plan] = result.Milk
	} else if plan == "每周吃3次及以上水产品（鱼虾贝类）" {
		report[plan] = result.Fish
	} else if plan == "每周吃3次以上豆制品" {
		report[plan] = result.Bean
	} else if plan == "每周吃3次以上菌菇类食物" {
		report[plan] = result.Mushroom
	}
}

func (s *Data) GetWeeklyReport(patientID uint64) (*doctor.ExpNutritionInterventionRecord, business.Error) {
	sqlAccess, err := s.sqlDatabase.NewAccess(false)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}
	defer sqlAccess.Close()

	now := time.Now()
	week := now.Weekday()
	dur := 0 - int(week)
	if week == 0 {
		dur = -7
	}
	date1 := now.AddDate(0, 0, dur)
	date2 := types.Time(date1)
	date3 := date2.ToDate(0)
	dbEntity := &sqldb.ExpNutritionInterventionRecord{}
	dbFilter := &sqldb.ExpNutritionInterventionRecordFilter{
		PatientID: &patientID,
		EndDate:   date3,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	err = sqlAccess.SelectOne(dbEntity, sqlFilter)
	if err != nil {
		if s.sqlDatabase.IsNoRows(err) {
			return nil, nil
		} else {
			return nil, business.NewError(errors.InternalError, err)
		}
	}
	result := &doctor.ExpNutritionInterventionRecord{}
	dbEntity.CopyTo(result)

	return result, nil
}

func (s *Data) UpdateWeeklyHabitReport(argument *doctor.ExpNutritionInterventionHabitReportHabitUpdate) business.Error {
	sqlAccess, err := s.sqlDatabase.NewAccess(true)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}
	defer sqlAccess.Close()

	dbEntity := &sqldb.ExpNutritionInterventionRecordHabitUpdate{
		Habit: argument.Habit,
	}
	dbFilter := &sqldb.ExpNutritionInterventionRecordNoFilter{
		SerialNo: argument.SerialNo,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)

	_, err = sqlAccess.UpdateSelective(dbEntity, sqlFilter)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}
	err = sqlAccess.Commit()
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}
	return nil
}

func (s *Data) UpdateDietProgram(argument *doctor.ExpPatientDietProgram) business.Error {
	sqlAccess, err := s.sqlDatabase.NewAccess(true)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}
	defer sqlAccess.Close()

	dbEntity := &sqldb.ExpPatientDietProgram{}
	dbEntity.CopyFrom(argument)
	dbFilter := &sqldb.ExpPatientDietProgramFilter{
		PatientID: argument.PatientID,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)

	_, err = sqlAccess.UpdateSelective(dbEntity, sqlFilter)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}
	err = sqlAccess.Commit()
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}
	return nil
}

func (s *Data) UpdateWholeDietProgram(argument *doctor.ExpPatientDietProgramWholeUpdate) business.Error {
	dbEntity := &sqldb.ExpPatientDietProgram{}
	for _, dietProgram := range argument.DietProgramList {
		dbEntity.CopyFrom(dietProgram)
		_, err := s.sqlDatabase.UpdateSelectiveByPrimaryKey(dbEntity)
		if err != nil {
			return business.NewError(errors.InternalError, err)
		}
	}
	return nil
}

func (s *Data) GetDietPlanTemplate() ([]*doctor.ExpDietPlanRepository, business.Error) {
	results := make([]*doctor.ExpDietPlanRepository, 0)
	dbEntity := &sqldb.ExpDietPlanRepository{}
	err := s.sqlDatabase.SelectList(dbEntity, func() {
		result := &doctor.ExpDietPlanRepository{}
		dbEntity.CopyTo(result)
		results = append(results, result)
	}, nil)
	if err != nil {
		return make([]*doctor.ExpDietPlanRepository, 0), business.NewError(errors.InternalError, err)
	}
	return results, nil
}

func (s *Data) DeletePatientDietPlan(argument *doctor.ExpPatientDietPlanDelete) business.Error {
	dbEntity := &sqldb.ExpPatientDietPlan{}
	dbFilter := &sqldb.ExpPatientDietPlanSno{SerialNo: argument.SerialNo}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	_, err := s.sqlDatabase.Delete(dbEntity, sqlFilter)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}
	return nil
}
