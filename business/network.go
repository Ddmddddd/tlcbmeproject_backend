package business

import (
	"tlcbme_project/data/model/manage"
	"tlcbme_project/data/model/platform"
	"tlcbme_project/server/config"
	"github.com/ktpswjz/httpserver/http/client"
)

type Client interface {
	Post(url string, argument interface{}, headers ...client.Header) ([]byte, int, error)
	ManagePost(providerType config.ManagementProviderType, uri string, argument interface{}) (*manage.Result, error)
	ManageBusinessPost(businessName string, patientId uint64, providerType config.ManagementProviderType, uri string, argument interface{}) (*manage.Result, error)
	PlatformPost(uri string, argument interface{}) (*platform.Result, error)
	PlatformBusinessPost(businessName string, patientId uint64, uri string, argument interface{}) (*platform.Result, error)
	Get(url string, argument interface{}) ([]byte, int, error)
}
