package business

import "github.com/ktpswjz/httpserver/types"

type Error interface {
	Error() types.Error
	Detail() error
}

func NewError(error types.Error, detail error) Error {
	return &innerError{
		error:  error,
		detail: detail,
	}
}

type innerError struct {
	error  types.Error
	detail error
}

func (s *innerError) Error() types.Error {
	return s.error
}

func (s *innerError) Detail() error {
	return s.detail
}
