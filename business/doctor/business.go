package doctor

import (
	"github.com/ktpswjz/httpserver/types"
	"tlcbme_project/business"
	"tlcbme_project/business/doctor/api"
	"tlcbme_project/business/doctor/impl"
	"tlcbme_project/database"
	"tlcbme_project/database/memory"
	"tlcbme_project/server/config"
	"tlcbme_project/server/notify"
)

type Business interface {
	Dict() api.Dict
	Authentication() api.Authentication
	UserBasicInfo() api.UserBasicInfo
	Verified() api.Verified
	Patient() api.Patient
	Followup() api.Followup
	Manage() api.Manage
	RiskAssess() api.RiskAssess
	BloodPressure() api.BloodPressure
	BloodGlucose() api.BloodGlucose
	Drug() api.Drug
	Assessment() api.Assessment
	Chat() api.Chat
	Sms() api.Sms
	Oauth() api.Oauth
	Instrument() api.Instrument
	Exp() api.Exp
	Stat() api.Stat
}

func NewBusiness(cfg *config.Config,
	log types.Log,
	sqlDatabase database.SqlDatabase,
	nSqlDatabase database.SqlDatabase,
	memoryToken memory.Token,
	client business.Client,
	notifyChannels notify.ChannelCollection) Business {

	instance := &innerBusiness{cfg: cfg, sqlDatabase: sqlDatabase, nSqlDatabase: nSqlDatabase, memoryToken: memoryToken, client: client, notifyChannels: notifyChannels}
	instance.SetLog(log)
	instance.init()

	return instance
}

type innerBusiness struct {
	types.Base

	cfg            *config.Config
	sqlDatabase    database.SqlDatabase
	nSqlDatabase   database.SqlDatabase
	memoryToken    memory.Token
	client         business.Client
	notifyChannels notify.ChannelCollection

	dict           api.Dict
	authentication api.Authentication
	userBasicInfo  api.UserBasicInfo
	verified       api.Verified
	patient        api.Patient
	followup       api.Followup
	manage         api.Manage
	riskAssess     api.RiskAssess
	bloodPressure  api.BloodPressure
	bloodGlucose   api.BloodGlucose
	drug           api.Drug
	assessment     api.Assessment
	chat           api.Chat
	sms            api.Sms
	oauth          api.Oauth
	instrument     api.Instrument
	exp            api.Exp
	stat           api.Stat
}

func (s *innerBusiness) init() {
	s.authentication = impl.NewAuthentication(s.GetLog(), s.sqlDatabase, s.memoryToken, s.cfg.Site.Doctor.User.PasswordFormat)
	s.userBasicInfo = impl.NewUserBasicInfo(s.GetLog(), s.sqlDatabase, s.memoryToken)
	s.dict = impl.NewDict(s.GetLog(), s.sqlDatabase, s.memoryToken)
	s.verified = impl.NewVerified(s.GetLog(), s.sqlDatabase, s.memoryToken)
	s.patient = impl.NewPatient(s.cfg, s.GetLog(), s.sqlDatabase, s.memoryToken, s.notifyChannels, s.cfg.Site.Patient.User.PasswordFormat, s.client)
	s.followup = impl.NewFollowup(s.cfg, s.GetLog(), s.sqlDatabase, s.memoryToken, s.notifyChannels, s.client)
	s.manage = impl.NewManage(s.cfg, s.GetLog(), s.sqlDatabase, s.memoryToken, s.notifyChannels, s.client)
	s.riskAssess = impl.NewRiskAssess(s.GetLog(), s.sqlDatabase, s.memoryToken, s.notifyChannels)
	s.bloodPressure = impl.NewBloodPressure(s.GetLog(), s.sqlDatabase, s.memoryToken, s.notifyChannels)
	s.bloodGlucose = impl.NewBloodGlucose(s.GetLog(), s.sqlDatabase, s.memoryToken, s.notifyChannels)
	s.drug = impl.NewDrug(s.GetLog(), s.sqlDatabase, s.memoryToken, s.notifyChannels)
	s.assessment = impl.NewAssessment(s.cfg, s.GetLog(), s.sqlDatabase, s.memoryToken, s.notifyChannels, s.client)
	s.chat = impl.NewChat(s.cfg, s.GetLog(), s.sqlDatabase, s.memoryToken, s.notifyChannels, s.client)
	s.sms = impl.NewSms(s.cfg, s.GetLog(), s.sqlDatabase, s.memoryToken, s.notifyChannels, s.client)
	s.oauth = impl.NewOauth(s.cfg, s.GetLog(), s.sqlDatabase, s.memoryToken, s.notifyChannels, s.client)
	s.instrument = impl.NewInstrument(s.cfg, s.GetLog(), s.sqlDatabase, s.notifyChannels, s.client, s.cfg.Site.Patient.User.PasswordFormat)
	s.exp = impl.NewExp(s.cfg, s.GetLog(), s.sqlDatabase, s.memoryToken, s.notifyChannels, s.client)
	s.stat = impl.NewStat(s.cfg, s.GetLog(), s.sqlDatabase, s.memoryToken, s.notifyChannels, s.client)
}

func (s *innerBusiness) Authentication() api.Authentication {
	return s.authentication
}

func (s *innerBusiness) UserBasicInfo() api.UserBasicInfo {
	return s.userBasicInfo
}

func (s *innerBusiness) Dict() api.Dict {
	return s.dict
}

func (s *innerBusiness) Verified() api.Verified {
	return s.verified
}

func (s *innerBusiness) Patient() api.Patient {
	return s.patient
}

func (s *innerBusiness) Followup() api.Followup {
	return s.followup
}

func (s *innerBusiness) Manage() api.Manage {
	return s.manage
}

func (s *innerBusiness) RiskAssess() api.RiskAssess {
	return s.riskAssess
}

func (s *innerBusiness) BloodPressure() api.BloodPressure {
	return s.bloodPressure
}

func (s *innerBusiness) BloodGlucose() api.BloodGlucose {
	return s.bloodGlucose
}

func (s *innerBusiness) Drug() api.Drug {
	return s.drug
}

func (s *innerBusiness) Assessment() api.Assessment {
	return s.assessment
}

func (s *innerBusiness) Chat() api.Chat {
	return s.chat
}

func (s *innerBusiness) Sms() api.Sms {
	return s.sms
}
func (s *innerBusiness) Oauth() api.Oauth {
	return s.oauth
}
func (s *innerBusiness) Instrument() api.Instrument {
	return s.instrument
}

func (s *innerBusiness) Exp() api.Exp {
	return s.exp
}
func (s *innerBusiness) Stat() api.Stat {
	return s.stat
}
