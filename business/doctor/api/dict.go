package api

import (
	"tlcbme_project/business"
	"tlcbme_project/data/model"
	"tlcbme_project/data/model/doctor"
	"tlcbme_project/data/model/jinscale"
)

type Dict interface {
	CreateOrg(argument *doctor.OrgDictCreate) (uint64, business.Error)
	ListOrg(argument *doctor.DictKeywordFilter) ([]*doctor.OrgDict, business.Error)
	EditOrg(argument *doctor.OrgDictUpdate) (uint64, business.Error)
	DeleteOrg(argument *doctor.DictFilter) (uint64, business.Error)

	CreateDevice(argument *doctor.DeviceDictCreate) (uint64, business.Error)
	ListDevice(argument *doctor.DictKeywordFilter) ([]*doctor.DeviceDict, business.Error)
	DeleteDevice(argument *doctor.DictFilter) (uint64, business.Error)
	EditDevice(argument *doctor.DeviceDictUpdate) (uint64, business.Error)

	CreateDrug(argument *doctor.DrugDictCreate) (uint64, business.Error)
	ListDrugAdmin(argument *doctor.DictKeywordFilter) ([]*doctor.DrugDict, business.Error)
	DeleteDrug(argument *doctor.DictFilter) (uint64, business.Error)
	EditDrug(argument *doctor.DrugDictUpdate) (uint64, business.Error)

	ListDoctor(filter *doctor.ViewDoctorUserDictFilter, orgCode string) ([]*doctor.ViewDoctorUserDict, business.Error)
	ListPatientFeature(filter *doctor.DictCodeItemFilter) ([]*doctor.DictCodeItem, business.Error)
	ListManageClass(filter *doctor.DictCodeItemFilter) ([]*doctor.DictCodeItem, business.Error)

	SearchDivisionTree(filter *doctor.DivisionDictTreeFilter) ([]interface{}, business.Error)
	SearchDivisionParentCodes(filter *doctor.DivisionDictDeleteFilter) ([]string, business.Error)
	CreateDivision(argument *doctor.DivisionDictCreate) business.Error
	DeleteDivision(argument *doctor.DivisionDictFilter) (uint64, business.Error)

	ListDrug() ([]*doctor.DrugDict, business.Error)

	ListSmsParam() ([]*doctor.SmsTemplateParam, business.Error)
	ListSmsContent() ([]*doctor.SmsTemplate, business.Error)

	ListFoodCategory() ([]*doctor.ExpFoodCategory, business.Error)
	ListFoodPageByCategory(pageIndex, pageSize uint64, filter *doctor.ExpFoodDictCategoryFilter) (*model.PageResult, business.Error)
	FoodSearch(filter *doctor.FoodSearchFilter) ([]*doctor.ExpFoodDict, business.Error)

	UploadBaseJinScale(argument *jinscale.BaseScale) business.Error
	UploadWeightJinScale(argument *jinscale.WeightScale) business.Error
	UploadDietJinScale(argument *jinscale.DietScale) business.Error
	UploadExerciseJinScale(argument *jinscale.ExerciseScale) business.Error
	UploadFitnessJinScale(argument *jinscale.FitnessScale) business.Error
	UploadPostureJinScale(argument *jinscale.PostureScale) business.Error
	UploadFitnessEndJinScale(argument *jinscale.FitnessEndScale) business.Error
	UploadPostureEndJinScale(argument *jinscale.PostureEndScale) business.Error
	UploadEndJinScale(argument *jinscale.EndScale) business.Error
	UploadNewEndJinScale(argument *jinscale.NewEndScale) business.Error
	UploadBaseHealthJinScale(argument *jinscale.BaseHealthScale) business.Error
	UploadSportsTeamJinScale(argument *jinscale.SportsTeamScale) business.Error
	UploadTestWangScale(argument *jinscale.TestWangScale) business.Error

	GetKnowledgeLinkList() ([]*doctor.ExpKnowledgeLink, business.Error)

	GetCampList() ([]string, business.Error)
}
