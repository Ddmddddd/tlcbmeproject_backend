package api

import (
	"tlcbme_project/business"
	"tlcbme_project/data/model/doctor"
)

type Verified interface {
	Save(argument *doctor.VerifiedDoctorUserEdit) (uint64, business.Error)
	Detail(argument *doctor.VerifiedDoctorUserFilter) (*doctor.VerifiedDoctorUserEx, business.Error)
	RightIDList(argument *doctor.DoctorAddedRightFilter) ([]*doctor.DoctorAddedRight, business.Error)
	SaveRightIDList(argument *doctor.DoctorAddedRight) (uint64, business.Error)
}
