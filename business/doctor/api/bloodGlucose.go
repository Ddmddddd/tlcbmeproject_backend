package api

import (
	"tlcbme_project/business"
	"tlcbme_project/data/model"
	"tlcbme_project/data/model/doctor"
)

type BloodGlucose interface {
	SearchRecordTrendList(filter *doctor.BloodGlucoseRecordFilter) ([]*doctor.BloodGlucoseRecordTrend, business.Error)
	SearchRecordTableList(pageIndex, pageSize uint64, filter *doctor.BloodGlucoseRecordFilter) (*model.PageResult, business.Error)
}
