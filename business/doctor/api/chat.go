package api

import (
	"tlcbme_project/business"
	"tlcbme_project/data/model/doctor"
)

type Chat interface {
	SendChatMessageFromDoctor(argument *doctor.DoctorPatientChatMsg) (uint64, business.Error)
	SendChatMessageReadFromDoctor(argument *doctor.DoctorPatientChatMsgRead) business.Error

	GetChatMessageList(argument *doctor.ChatMsgUserInfo) ([]*doctor.DoctorPatientChatMsg, business.Error)
	GetChatPatientLatsMsgList(argument *doctor.DoctorInfoBase) ([]*doctor.DoctorPatienLastChatMsg, business.Error)

	CountUnReadMsg(filter *doctor.ChatUnReadMsgCountFilter) (uint64, business.Error)
}
