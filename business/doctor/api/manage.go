package api

import (
	"tlcbme_project/business"
	"tlcbme_project/data/model/doctor"
)

type Manage interface {
	SearchHtnDetailList(filter *doctor.HtnManageDetailFilter) ([]*doctor.HtnManageDetail, business.Error)
	SearchHtnLevelTrend(filter *doctor.TrendLevelHtnFilter) (*doctor.TrendLevelHtn, business.Error)
	SearchDmLevelTrend(filter *doctor.TrendLevelDmFilter) (*doctor.TrendLevelDm, business.Error)
	SearchManageStatus(filter *doctor.ManagedPatientIndexFilterBase) (*doctor.ManagedPatientIndexStatus, business.Error)
	TerminateManage(argument *doctor.ManagedPatientIndexTerminate, userName string, userID uint64) (uint64, business.Error)
	RecoverManage(argument *doctor.ManagedPatientIndexFilterBase) (uint64, business.Error)
	UpdateManagementPlan(argument *doctor.ManagementPlanAdjust) (uint64, business.Error)
}
