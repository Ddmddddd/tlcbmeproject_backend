package api

import (
	"tlcbme_project/business"
	"tlcbme_project/data/model"
	"tlcbme_project/data/model/doctor"
	"tlcbme_project/data/model/manage"
)

type Patient interface {
	CreateUser(argument *doctor.PatientUserCreateEx) (uint64, business.Error)
	ListViewManagedPatientIndexPage(pageIndex, pageSize uint64, filters []*doctor.ViewManagedPatientIndexListFilterEx) (*model.PageResult, business.Error)
	GetManagedIndex(filter *doctor.ManagedPatientIndexFilterBase) (*doctor.ManagedPatientIndex, business.Error)
	SaveAlerts(patientIndex *doctor.ManagedPatientIndex, alerts []manage.InputDataWarnings) business.Error
	ListAlertRecordPage(pageIndex, pageSize uint64, filter *doctor.AlertRecordFilter) (*model.PageResult, business.Error)
	ListAlert(filters []*doctor.AlertRecordFilterEx) ([]*doctor.AlertPatientInfo, business.Error)
	SearchAlert(filter *doctor.PatientInfoBase) ([]*doctor.AlertPatientRecord, business.Error)
	CountAlert(filters []*doctor.AlertRecordFilterEx) (uint64, business.Error)
	IgnoreAlert(filter *doctor.AlertRecordIgnoreEx) (uint64, business.Error)

	SearchPatientOne(filter *doctor.ViewPatientFilter) (*doctor.ViewPatientEx, business.Error)
	SearchPatientBasicInfoOne(filter *doctor.ViewPatientBaseInfoFilterEx) (*doctor.ViewPatientBaseInfoEx, business.Error)
	SaveManagement(patientIndex *doctor.ManagedPatientIndex, management *manage.InputDataManagement, flag uint64) business.Error
	SearchManagementPlanList(filter *doctor.ManagementPlanFilter) ([]*doctor.ManagementPlanEx, business.Error)

	SearchManagementApplicationReviewPage(pageIndex, pageSize uint64, filter *doctor.ViewManagementApplicationReviewFilterEx) (*model.PageResult, business.Error)
	AuditPatientUser(argument *doctor.PatientUserAuditEx) (uint64, uint64, business.Error)
	SearchAuditCount(filter *doctor.ManagementApplicationReviewFilter) (uint64, business.Error)

	SearchViewManagedIndexMangeLevelCount(filter *doctor.ViewManagedIndexFilterEx) (*doctor.ViewManagedIndexCountManageLevel, business.Error)
	SearchViewManagedIndexDmTypeCount(filter *doctor.ViewManagedIndexFilterEx) (*doctor.ViewDmManagedIndexCountManageLevel, business.Error)
	SearchViewManagedIndexPage(pageIndex, pageSize uint64, filters []*doctor.ViewManagedIndexFilterEx) (*model.PageResult, business.Error)
	SearchViewSportDietRecordPage(pageIndex, pageSize uint64, filter *doctor.ViewSportDietRecordFilter) (*model.PageResult, business.Error)
	SearchDiscomfortRecordPage(pageIndex, pageSize uint64, filter *doctor.DiscomfortRecordDataFilterEx) (*model.PageResult, business.Error)
	SearchViewStatDiscomfortList(filter *doctor.ViewStatDiscomfortFilter) ([]*doctor.ViewStatDiscomfort, business.Error)

	SearchPatientHeightAndWeight(filter *doctor.PatientInfoBase) (*doctor.PatientUserHeightAndWeight, business.Error)

	UpdateComplianceRateAndPatientActiveDegree(index *doctor.ManagedPatientIndex, complianceRate uint64, patientActiveDegree uint64) business.Error
	UpdatePatientHeight(argument *doctor.PatientUserHeight) (uint64, business.Error)
	ModifyPatientUserBaseInfo(argument *doctor.PatientUserBaseInfoModify, doctorName string, doctorID uint64) (uint64, uint64, business.Error)

	UpdatePatientDiagnosismemo(argument *doctor.ManagementApplicationReviewMemo) (uint64, business.Error)
	ListPatientWeightPhoto(argument *doctor.WeightRecordDataFilterEx) ([]*doctor.WeightRecordForPhoto, business.Error)
	//GetPatientClockDay(argument *doctor.ExpPatientTaskRecordGetListInput)  ([][]time.Time, business.Error)
}
