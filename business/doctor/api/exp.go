package api

import (
	"tlcbme_project/business"
	"tlcbme_project/data/model"
	"tlcbme_project/data/model/doctor"
	"tlcbme_project/data/model/manage"
)

type Exp interface {
	//弃用GetPatientExpInfo(argument *doctor.ExpPatientInfoGet) (*doctor.ExpPatient, business.Error)
	IncludePatientInExp(argument *doctor.ExpPatientCreate, regFlag bool) business.Error
	ChangePatientStatus(argument *doctor.ExpPatientStatusChange, primary bool) business.Error
	GetAllDoctorExpCommonScale() ([]*doctor.ExpCommonScale, business.Error)
	GetExpCommonScale(argument *doctor.ExpCommonScaleSerialNoFilter) (*doctor.ExpCommonScale, business.Error)
	CommitPatientExpScaleRecord(argument *doctor.ExpScaleRecordCreate) ([]*doctor.ExpExerciseBase, business.Error)
	EditPatientExpActionPlan(argument *doctor.ExpPatientActionPlanEdit) business.Error
	CreatePatientExpActionPlan(argument *doctor.ExpPatientActionPlanCreate) business.Error
	GetPatientActionPlanRecordInfo(argument *doctor.ExpActionPlanRecordTimeFilter) ([]*doctor.ExpActionPlanRecordForDoctor, business.Error)
	GetPatientTaskRecordList(argument *doctor.ExpPatientTaskRecordGetListInput) ([]*doctor.ExpPatientTaskRecords, business.Error)
	GetPatientTaskRecord(argument *doctor.ExpPatientTaskRecordGetInput) (*doctor.ExpPatientTaskRecord, business.Error)
	GetPatientDietRecord(argument *doctor.SportDietRecordFilter) (*doctor.SportDietRecord, business.Error)
	GetPatientDietRecordComment(argument *doctor.ExpDoctorCommentToPatientSportDietRecordFilter) (*doctor.ExpDoctorCommentToPatient, business.Error)
	CommitPatientDietRecordComment(argument *doctor.ExpDoctorCommentToPatientCreate) (uint64, business.Error)
	GetCommentTemplate(argument *doctor.ExpDoctorCommentTemplateType) ([]*doctor.ExpDoctorCommentTemplate, business.Error)
	AddCommentTemplate(argument *doctor.ExpDoctorCommentTemplateCreate) business.Error
	DeleteCommentTemplate(argument *doctor.ExpDoctorCommentTemplateDelete) business.Error
	TopCommentTemplate(argument *doctor.ExpDoctorCommentTemplateDelete) business.Error
	GetUnCommentTaskCount(argument *doctor.ExpPatientTaskRecordGetByName) (uint64, business.Error)
	GetUnCommentTaskPage(index, size uint64, filter *doctor.ExpPatientTaskRecordGetByName) (*model.PageResult, business.Error)
	GetCurrentDayWithoutClockingInPage(index uint64, size uint64, filters *doctor.ExpPatientTaskRecordGetByName) (*model.PageResult, business.Error)
	GetCommentRecordPage(index, size uint64, filter *doctor.ExpPatientTaskRecordGetByNameAll) (*model.PageResult, business.Error)
	DeleteCommentRecord(argument *doctor.ExpPatientTaskRecordDelete) business.Error
	GetPatientCheckData(argument *doctor.ExpPatientTaskRecordGetCheckDataInput) ([]*doctor.ExpPatientTaskRecordCheckData, business.Error)
	GetPatientBodyExamImage(patientID uint64) ([]*doctor.ExpPatientBodyExamImage, business.Error)
	UploadPatientFile(argument *doctor.ExpPatientUploadFile) business.Error
	GetPatientFile(argument *doctor.ExpPatientUploadFileGet) (*doctor.ExpPatientUploadFile, business.Error)
	GetPatientDietPlan(argument *doctor.ExpPatientDietPlanGet) ([]*doctor.ExpPatientDietPlan, business.Error)
	SavePatientDietPlan(argument *doctor.ExpPatientDietPlanSave) business.Error
	SaveAndDelayPatientDietPlan(argument *doctor.ExpPatientDietPlanSave) business.Error
	StartDietPlan(patientID uint64) business.Error

	SaveBarriers(patientIndex *doctor.ManagedPatientIndex, barrierResult manage.BarrierResult) business.Error
	SaveActionPlans(patientIndex *doctor.ManagedPatientIndex, actionPlans []manage.InputDataActionPlan) business.Error
	GetTaskTree() ([]*doctor.ExpTaskRepositoryTree, business.Error)
	AddTask(argument *doctor.ExpTaskRepositoryAddInput) (uint64, business.Error)
	EditTask(argument *doctor.ExpTaskRepositoryEditInput) (uint64, business.Error)
	DeleteTask(argument *doctor.ExpTaskRepositoryDeleteInput) (uint64, business.Error)
	GetKnowledgeByType(argument *doctor.ExpKnowledgeTypeInput) ([]*doctor.ExpKnowledge, business.Error)
	EditKnowledge(argument *doctor.ExpKnowledgeEditInput) business.Error
	AddKnowledge(argument *doctor.ExpKnowledgeAddInput) (uint64, business.Error)
	DeleteKnowledge(argument *doctor.ExpKnowledgeDeleteInput) business.Error
	GetQuestionByKnowledgeID(argument *doctor.ExpKnowledgeIDInput) ([]*doctor.ExpQuestionToKnowledge, business.Error)
	AddQuestion(argument *doctor.ExpQuestionToKnowledgeAddInput) (uint64, business.Error)
	DeleteQuestion(argument *doctor.ExpQuestionToKnowledgeDeleteInput) business.Error
	EditQuestion(argument *doctor.ExpQuestionToKnowledgeEditInput) business.Error
	CreateFeatureToTask(argument *doctor.ExpPatientFeatureToTask) business.Error
	EditFeatureToTask(argument *doctor.ExpPatientFeatureToTask) business.Error
	AddCamp(argument *doctor.CampListAddInput) (uint64, business.Error)
	ListCampPage(index uint64, size uint64) (*model.PageResult, business.Error)
    ListCampDict() ([]*doctor.CampListDict, business.Error)
	GetCamp(argument *doctor.CampListSerialNo) (*doctor.CampListInfo, business.Error)
	UpdateCamp(argument *doctor.CampListUpdateInput) (uint64, business.Error)
	DeleteCamp(argument *doctor.CampListSerialNo) business.Error

}
