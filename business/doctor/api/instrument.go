package api

import (
	"tlcbme_project/business"
	"tlcbme_project/data/model/doctor"
)

type Instrument interface {
	ExistIDNumber(argument string) (bool, uint64, business.Error)
	CreateUser(argument *doctor.PatientUserInfoCreateForApp) (uint64, business.Error)
	GetHospitalCode(deviceCode string) (string, string, business.Error)
}
