package api

import (
	"tlcbme_project/business"
	"tlcbme_project/data/model"
	"tlcbme_project/data/model/doctor"
	"tlcbme_project/data/model/manage"
)

type RiskAssess interface {
	SaveRecord(patientIndex *doctor.ManagedPatientIndex, argument *manage.InputDataRiskAssess) business.Error

	SearchRecordPage(pageIndex, pageSize uint64, filter *doctor.RiskAssessRecordFilter) (*model.PageResult, business.Error)
}
