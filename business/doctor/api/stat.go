package api

import (
	"tlcbme_project/business"
	"tlcbme_project/data/model/stat"
)

type Stat interface {
	GetPatientManageCount(input *stat.StatInput) ([]*stat.StatOut, business.Error)
	GetBloodPressureLevelCount(input *stat.StatInput) ([]*stat.StatOut, business.Error)
	GetBloodGlucoseLevelCount(input *stat.StatInput) ([]*stat.StatOut, business.Error)
	GetBloodPressureCount(input *stat.StatInput) ([]*stat.StatOut, business.Error)
	GetBloodPressureRate(input *stat.StatInput) ([]*stat.StatOut, business.Error)
	GetBloodGlucoseCount(input *stat.StatInput) ([]*stat.StatOut, business.Error)
	GetBloodGlucoseRate(input *stat.StatInput) ([]*stat.StatOut, business.Error)
	GetOtherData(input *stat.StatInput) ([]*stat.StatOut, business.Error)
	GetFollowupData(input *stat.StatInput) ([]*stat.StatOut, business.Error)
	GetAlertData(input *stat.StatInput) ([]*stat.StatOut, business.Error)
	GetBloodPressureRiskData(input *stat.StatInput) ([]*stat.StatOut, business.Error)
	GetBloodGlucoseRiskData(input *stat.StatInput) ([]*stat.StatOut, business.Error)
}
