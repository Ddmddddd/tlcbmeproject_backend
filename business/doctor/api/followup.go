package api

import (
	"tlcbme_project/business"
	"tlcbme_project/data/model"
	"tlcbme_project/data/model/doctor"
)

type Followup interface {
	SearchFollowupPlanPage(pageIndex, pageSize uint64, filter *doctor.ViewFollowupPlanFilterEx) (*model.PageResult, business.Error)
	SearchFollowupPlanCount(filter *doctor.ViewFollowupPlanFilterEx) (*doctor.ViewFollowupPlanCount, business.Error)
	SaveFollowupRecord(argument *doctor.FollowupRecordSaveEx) (uint64, business.Error)
	SearchFollowupRecordPage(pageIndex, pageSize uint64, filter *doctor.FollowupRecordFilter) (*model.PageResult, business.Error)

	CreatePlan(argument *doctor.FollowupPlan) (uint64, business.Error)
	IgnorePlan(argument *doctor.FollowupPlanIgnore) (uint64, business.Error)
}
