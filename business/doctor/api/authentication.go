package api

import (
	"tlcbme_project/business"
	"tlcbme_project/data/model"
	"tlcbme_project/data/model/doctor"
)

type Authentication interface {
	Authenticate() func(account, password string) (uint64, error)
	Logined() func(userID uint64, token *model.Token)
	CreateAccount(argument *doctor.DoctorUserAuthsCreate) (uint64, business.Error)
	CreateAccountOneTime(argument *doctor.DoctorUserAuthsCreateOneTime) (uint64, business.Error)
	ListAccount(filter *doctor.DoctorUserAuthsInfoFilter) ([]*doctor.DoctorUserAuthsInfo, business.Error)
	ListAccountLike(filter *doctor.DoctorUserAuthsInfoLikeFilter) ([]*doctor.ViewDoctorUser, business.Error)
	EditAccount(argument *doctor.DoctorUserAuthsEdit) (uint64, business.Error)
	SetAccountPassword(argument *doctor.DoctorUserAuthsPassword) (uint64, business.Error)
	Detail(argument *doctor.ViewDoctorUserFilter) (*doctor.ViewDoctorUserEx, business.Error)
	ChangePassword(userID uint64, oldPassword, newPassword string) business.Error
}
