package api

import (
	"tlcbme_project/business"
	"tlcbme_project/data/model"
	"tlcbme_project/data/model/doctor"
	"tlcbme_project/data/model/manage"
)

type Assessment interface {
	CreateAssessmentRecord(argument *doctor.AssessmentRecordCreateEx) (uint64, uint64, business.Error)
	UpdateAssessmentRecord(argument *doctor.AssessmentRecordUpdate) (uint64, business.Error)
	SearchAssessmentRecordOne(filter *doctor.AssessmentRecordFilter) (*doctor.AssessmentRecord, business.Error)
	SearchAssessmentRecordPage(pageIndex, pageSize uint64, filter *doctor.AssessmentRecordFilter) (*model.PageResult, business.Error)

	SaveRecord(patientIndex *doctor.ManagedPatientIndex, argument *manage.InputDataRiskAssess) business.Error
}
