package api

import (
	"tlcbme_project/business"
	"tlcbme_project/data/model/doctor"
)

type Sms interface {
	SendSms(argument *doctor.SmsParam) (string, business.Error)
	GetSmsRecordList(argument *doctor.SmsSendRecordFilter) ([]*doctor.SmsSendRecordExt, business.Error)
}
