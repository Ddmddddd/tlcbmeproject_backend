package api

import (
	"tlcbme_project/business"
	"tlcbme_project/data/model/doctor"
)

type UserBasicInfo interface {
	Save(argument *doctor.DoctorUserBaseInfoEdit) (uint64, business.Error)
	Detail(argument *doctor.DoctorUserBaseInfoFilter) (*doctor.DoctorUserBaseInfoEx, business.Error)
}
