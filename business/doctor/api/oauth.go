package api

import (
	"tlcbme_project/business"
	"tlcbme_project/data/model/oauth"
)

type Oauth interface {
	GetOauthUserInfo(argument *oauth.Input) (account *oauth.UserAccount, err business.Error)
}
