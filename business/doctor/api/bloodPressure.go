package api

import (
	"tlcbme_project/business"
	"tlcbme_project/data/model"
	"tlcbme_project/data/model/doctor"
)

type BloodPressure interface {
	SearchTrendSingleList(filter *doctor.BloodPressureRecordTrendFilter) (*doctor.BloodPressureRecordTrendStat, business.Error)
	SearchTrendSingleRange(filter *doctor.BloodPressureRecordDataFilterEx) (*doctor.BloodPressureRecordTrendStat, business.Error)
	SearchRecordLastOne(filter *doctor.BloodPressureRecordPatientFilter) (*doctor.BloodPressureRecord, business.Error)
	SearchTableSingleRange(pageIndex, pageSize uint64, filter *doctor.BloodPressureRecordDataFilterEx) (*model.PageResult, business.Error)
}
