package api

import (
	"tlcbme_project/business"
	"tlcbme_project/data/model/doctor"
)

type Drug interface {
	SearchDrugUseRecordIndexList(filter *doctor.DrugUseRecordIndexFilter) ([]*doctor.DrugUseRecordIndexEx, business.Error)
	SearchDrugUseRecordMonthGroupList(filter *doctor.DrugUseRecordFilter) ([]*doctor.DrugUseRecordMonthGroup, business.Error)
	SearchDrugUseRecordIndexModifiedLogList(filter *doctor.DrugUseRecordIndexModifiedLogFilter) ([]*doctor.DrugUseRecordIndexModifiedLog, business.Error)
	CreateDrugUseRecordIndexModifiedLog(argument *doctor.DrugUseRecordIndexModifiedLogCreate) (uint64, business.Error)
	CreateDrugUseRecordIndex(argument *doctor.DrugRecordForDoctorList, userName string) (uint64, business.Error)
}
