package impl

import (
	"tlcbme_project/business"
	"tlcbme_project/business/doctor/api"
	"tlcbme_project/data/entity/sqldb"
	"tlcbme_project/data/model"
	"tlcbme_project/data/model/doctor"
	"tlcbme_project/database"
	"tlcbme_project/database/memory"
	"tlcbme_project/errors"
	"tlcbme_project/server/notify"
	"github.com/ktpswjz/httpserver/types"
)

type BloodGlucose struct {
	base
}

func NewBloodGlucose(log types.Log, sqlDatabase database.SqlDatabase, memoryToken memory.Token, notifyChannels notify.ChannelCollection) api.BloodGlucose {
	instance := &BloodGlucose{}
	instance.SetLog(log)
	instance.sqlDatabase = sqlDatabase
	instance.memoryToken = memoryToken
	instance.notifyChannels = notifyChannels

	return instance
}

func (s *BloodGlucose) SearchRecordTrendList(filter *doctor.BloodGlucoseRecordFilter) ([]*doctor.BloodGlucoseRecordTrend, business.Error) {
	dbFilter := &sqldb.BloodGlucoseRecordFilter{}
	dbFilter.CopyFrom(filter)
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	dbOrder := &sqldb.BloodGlucoseRecordOrder{}
	dbEntity := &sqldb.BloodGlucoseRecord{}

	results := make([]*doctor.BloodGlucoseRecordTrend, 0)
	err := s.sqlDatabase.SelectList(dbEntity, func() {
		result := &doctor.BloodGlucoseRecordTrend{}
		dbEntity.CopyToTrend(result)
		results = append(results, result)
	}, dbOrder, sqlFilter)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}

	return results, nil
}

func (s *BloodGlucose) SearchRecordTableList(pageIndex, pageSize uint64, filter *doctor.BloodGlucoseRecordFilter) (*model.PageResult, business.Error) {
	dbFilter := &sqldb.BloodGlucoseRecordFilter{}
	dbFilter.CopyFrom(filter)
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	dbOrder := &sqldb.BloodGlucoseRecordDownOrder{}
	dbEntity := &sqldb.BloodGlucoseRecord{}

	pageResult := &model.PageResult{}
	datas := make([]*doctor.BloodGlucoseRecord, 0)
	err := s.sqlDatabase.SelectPage(dbEntity, func(total, page, size, index uint64) {
		pageResult.Total = total
		pageResult.Count = page
		pageResult.Size = size
		pageResult.Index = index
	}, func() {
		data := &doctor.BloodGlucoseRecord{}
		dbEntity.CopyTo(data)
		datas = append(datas, data)
	}, pageSize, pageIndex, dbOrder, sqlFilter)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}
	pageResult.Data = datas

	return pageResult, nil
}
