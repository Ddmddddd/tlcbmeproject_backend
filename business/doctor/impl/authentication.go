package impl

import (
	"tlcbme_project/business"
	"tlcbme_project/business/doctor/api"
	"tlcbme_project/data/entity/sqldb"
	"tlcbme_project/data/model"
	"tlcbme_project/data/model/doctor"
	"tlcbme_project/database"
	"tlcbme_project/database/memory"
	"tlcbme_project/errors"
	"fmt"
	"github.com/ktpswjz/httpserver/security/hash"
	"github.com/ktpswjz/httpserver/types"
	"strings"
	"time"
)

type Authentication struct {
	base

	passwordFormat uint64
}

func NewAuthentication(log types.Log, sqlDatabase database.SqlDatabase, memoryToken memory.Token, passwordFormat uint64) api.Authentication {
	instance := &Authentication{passwordFormat: passwordFormat}
	instance.SetLog(log)
	instance.sqlDatabase = sqlDatabase
	instance.memoryToken = memoryToken

	return instance
}

func (s *Authentication) Authenticate() func(account, password string) (uint64, error) {
	return s.authenticate
}

func (s *Authentication) Logined() func(userID uint64, token *model.Token) {
	return s.logined
}

func (s *Authentication) CreateAccount(argument *doctor.DoctorUserAuthsCreate) (uint64, business.Error) {
	if argument == nil {
		return 0, business.NewError(errors.InputError, fmt.Errorf("参数为空"))
	}
	if argument.UserName == "" {
		return 0, business.NewError(errors.InputInvalid, fmt.Errorf("用户名为空"))
	}
	if argument.Password == "" {
		return 0, business.NewError(errors.InputInvalid, fmt.Errorf("登录密码为空"))
	}

	dbEntity, err := s.getUserByAccount(argument.UserName, nil)
	if err == nil && dbEntity != nil {
		return 0, business.NewError(errors.InputInvalid, fmt.Errorf("用户名%s已存在", argument.UserName))
	}
	if len(argument.MobilePhone) > 0 {
		dbEntity, err = s.getUserByAccount(argument.MobilePhone, nil)
		if err == nil && dbEntity != nil {
			return 0, business.NewError(errors.InputInvalid, fmt.Errorf("手机号%s已被注册", argument.MobilePhone))
		}
	}
	if len(argument.Email) > 0 {
		dbEntity, err = s.getUserByAccount(argument.Email, nil)
		if err == nil && dbEntity != nil {
			return 0, business.NewError(errors.InputInvalid, fmt.Errorf("电子邮箱地址%s已被注册", argument.Email))
		}
	}

	password := argument.Password
	if s.passwordFormat != 0 {
		password, err = hash.Hash(argument.Password, s.passwordFormat)
		if err != nil {
			return 0, business.NewError(errors.Unknown, err)
		}
	}

	now := time.Now()
	dbEntity = &sqldb.DoctorUserAuths{}
	dbEntity.UserName = argument.UserName
	dbEntity.MobilePhone = &argument.MobilePhone
	dbEntity.Email = &argument.Email
	dbEntity.Status = 0
	dbEntity.LoginCount = 0
	dbEntity.UseSeconds = 0
	dbEntity.RegistDateTime = &now
	dbEntity.LastLoginDateTime = nil
	dbEntity.Password = password
	dbEntity.PasswordFormat = s.passwordFormat

	sqlEntity := s.sqlDatabase.NewEntity()
	err = sqlEntity.Parse(dbEntity)
	if err != nil {
		return 0, business.NewError(errors.Unknown, err)
	}

	sqlBuilder := s.sqlDatabase.NewBuilder()
	sqlBuilder.Insert(sqlEntity.Name())
	fieldCount := sqlEntity.FieldCount()
	for fieldIndex := 0; fieldIndex < fieldCount; fieldIndex++ {
		field := sqlEntity.Field(fieldIndex)
		if field.AutoIncrement() {
			continue
		}

		sqlBuilder.Value(field.Name(), field.Value())
	}
	sqlAccess, err := s.sqlDatabase.NewAccess(false)
	if err != nil {
		return 0, business.NewError(errors.Unknown, err)
	}
	defer sqlAccess.Close()

	stmt, err := sqlAccess.Prepare(sqlBuilder.Query())
	if err != nil {
		return 0, business.NewError(errors.Unknown, err)
	}
	result, err := stmt.Exec(sqlBuilder.Args()...)
	if err != nil {
		return 0, business.NewError(errors.Unknown, err)
	}
	id, err := result.LastInsertId()
	if err != nil {
		return 0, business.NewError(errors.Unknown, err)
	}

	return uint64(id), nil
}
func (s *Authentication) CreateAccountOneTime(argument *doctor.DoctorUserAuthsCreateOneTime) (uint64, business.Error) {
	if argument == nil {
		return 0, business.NewError(errors.InputError, fmt.Errorf("参数为空"))
	}
	if argument.UserName == "" {
		return 0, business.NewError(errors.InputInvalid, fmt.Errorf("用户名为空"))
	}
	if argument.Password == "" {
		return 0, business.NewError(errors.InputInvalid, fmt.Errorf("登录密码为空"))
	}
	if argument.OrgCode == "" {
		return 0, business.NewError(errors.InputInvalid, fmt.Errorf("执行机构为空"))
	}
	if argument.Name == "" {
		return 0, business.NewError(errors.InputInvalid, fmt.Errorf("姓名为空"))
	}

	dbEntity, err := s.getUserByAccount(argument.UserName, nil)
	if err == nil && dbEntity != nil {
		return 0, business.NewError(errors.InputInvalid, fmt.Errorf("用户名%s已存在", argument.UserName))
	}
	if len(argument.MobilePhone) > 0 {
		dbEntity, err = s.getUserByAccount(argument.MobilePhone, nil)
		if err == nil && dbEntity != nil {
			return 0, business.NewError(errors.InputInvalid, fmt.Errorf("手机号%s已被注册", argument.MobilePhone))
		}
	}

	password := argument.Password
	if s.passwordFormat != 0 {
		password, err = hash.Hash(argument.Password, s.passwordFormat)
		if err != nil {
			return 0, business.NewError(errors.Unknown, err)
		}
	}

	now := time.Now()
	email := string("")
	dbEntity = &sqldb.DoctorUserAuths{}
	dbEntity.UserName = argument.UserName
	dbEntity.MobilePhone = &argument.MobilePhone
	dbEntity.Status = argument.Status
	dbEntity.LoginCount = 0
	dbEntity.UseSeconds = 0
	dbEntity.RegistDateTime = &now
	dbEntity.LastLoginDateTime = nil
	dbEntity.Password = password
	dbEntity.PasswordFormat = s.passwordFormat
	dbEntity.Email = &email

	sqlEntity := s.sqlDatabase.NewEntity()
	err = sqlEntity.Parse(dbEntity)
	if err != nil {
		return 0, business.NewError(errors.Unknown, err)
	}

	sqlBuilder := s.sqlDatabase.NewBuilder()
	sqlBuilder.Insert(sqlEntity.Name())
	fieldCount := sqlEntity.FieldCount()
	for fieldIndex := 0; fieldIndex < fieldCount; fieldIndex++ {
		field := sqlEntity.Field(fieldIndex)
		if field.AutoIncrement() {
			continue
		}

		sqlBuilder.Value(field.Name(), field.Value())
	}
	sqlAccess, err := s.sqlDatabase.NewAccess(true)
	if err != nil {
		return 0, business.NewError(errors.Unknown, err)
	}
	defer sqlAccess.Close()

	stmt, err := sqlAccess.Prepare(sqlBuilder.Query())
	if err != nil {
		return 0, business.NewError(errors.Unknown, err)
	}
	result, err := stmt.Exec(sqlBuilder.Args()...)
	if err != nil {
		return 0, business.NewError(errors.Unknown, err)
	}
	id, err := result.LastInsertId()
	if err != nil {
		return 0, business.NewError(errors.Unknown, err)
	}
	dbEntity2 := &sqldb.DoctorUserBaseInfo{Name: argument.Name, UserID: uint64(id)}
	_, err = sqlAccess.InsertSelective(dbEntity2)
	if err != nil {
		return 0, business.NewError(errors.InternalError, err)
	}
	dbEntity3 := &sqldb.VerifiedDoctorUser{UserID: uint64(id), OrgCode: &argument.OrgCode, UserType: argument.UserType, VerifiedStatus: argument.VerifiedStatus}
	_, err = sqlAccess.InsertSelective(dbEntity3)
	if err != nil {
		return 0, business.NewError(errors.InternalError, err)
	}
	err = sqlAccess.Commit()
	if err != nil {
		return 0, business.NewError(errors.InternalError, err)
	}

	return uint64(id), nil
}

func (s *Authentication) ListAccount(filter *doctor.DoctorUserAuthsInfoFilter) ([]*doctor.DoctorUserAuthsInfo, business.Error) {
	results := make([]*doctor.DoctorUserAuthsInfo, 0)

	dbEntity := &sqldb.DoctorUserAuthsInfo{}
	var dbFilter *sqldb.DoctorUserAuthsInfoFilter = nil
	if filter != nil {
		dbFilter = &sqldb.DoctorUserAuthsInfoFilter{}
		dbFilter.CopyFrom(filter)
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)

	err := s.sqlDatabase.SelectList(dbEntity, func() {
		result := &doctor.DoctorUserAuthsInfo{}
		dbEntity.CopyTo(result)
		results = append(results, result)
	}, nil, sqlFilter)

	if err != nil {
		return results, business.NewError(errors.InternalError, err)
	}

	return results, nil
}

func (s *Authentication) ListAccountLike(filter *doctor.DoctorUserAuthsInfoLikeFilter) ([]*doctor.ViewDoctorUser, business.Error) {
	results := make([]*doctor.ViewDoctorUser, 0)

	dbEntity := &sqldb.ViewDoctorUser{}
	var dbAccountFilter *sqldb.DoctorUserAuthsInfoLikeFilter = nil
	var dbStatusFilter *sqldb.DoctorUserAuthsInfoStatusFilter = nil
	if filter != nil {
		dbAccountFilter = &sqldb.DoctorUserAuthsInfoLikeFilter{}
		dbAccountFilter.CopyFrom(filter)

		dbStatusFilter = &sqldb.DoctorUserAuthsInfoStatusFilter{}
		dbStatusFilter.CopyFrom(filter)
	}
	sqlAccountFilter := s.sqlDatabase.NewFilter(dbAccountFilter, true, false)
	sqlStatusFilter := s.sqlDatabase.NewFilter(dbStatusFilter, false, false)

	err := s.sqlDatabase.SelectList(dbEntity, func() {
		result := &doctor.ViewDoctorUser{}
		dbEntity.CopyTo(result)
		results = append(results, result)
	}, nil, sqlAccountFilter, sqlStatusFilter)

	if err != nil {
		return results, business.NewError(errors.InternalError, err)
	}

	return results, nil
}

func (s *Authentication) EditAccount(argument *doctor.DoctorUserAuthsEdit) (uint64, business.Error) {
	if argument == nil {
		return 0, business.NewError(errors.InputError, fmt.Errorf("参数为空"))
	}

	dbEntity, err := s.getUserById(argument.UserID)
	if err != nil {
		if s.sqlDatabase.IsNoRows(err) {
			return 0, business.NewError(errors.InputError, fmt.Errorf("ID不存在"))
		} else {
			return 0, business.NewError(errors.InputError, err)
		}
	}

	dbEntity, err = s.getUserByAccount(argument.UserName, &argument.UserID)
	if err == nil && dbEntity != nil {
		return 0, business.NewError(errors.InputInvalid, fmt.Errorf("用户名%s已存在", argument.UserName))
	}
	if len(argument.MobilePhone) > 0 {
		dbEntity, err = s.getUserByAccount(argument.MobilePhone, &argument.UserID)
		if err == nil && dbEntity != nil {
			return 0, business.NewError(errors.InputInvalid, fmt.Errorf("手机号%s已被注册", argument.MobilePhone))
		}
	}
	if len(argument.Email) > 0 {
		dbEntity, err = s.getUserByAccount(argument.Email, &argument.UserID)
		if err == nil && dbEntity != nil {
			return 0, business.NewError(errors.InputInvalid, fmt.Errorf("电子邮箱地址%s已被注册", argument.Email))
		}
	}

	dbEdit := &sqldb.DoctorUserAuthsEdit{}
	dbEdit.CopyFrom(argument)

	dbFilter := &sqldb.DoctorUserAuthsUpdateFilter{
		UserID: argument.UserID,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)

	_, err = s.sqlDatabase.UpdateSelective(dbEdit, sqlFilter)
	if err != nil {
		return 0, business.NewError(errors.InternalError, err)
	}

	return argument.UserID, nil
}

func (s *Authentication) SetAccountPassword(argument *doctor.DoctorUserAuthsPassword) (uint64, business.Error) {
	if argument == nil {
		return 0, business.NewError(errors.InputError, fmt.Errorf("参数为空"))
	}
	if argument.UserID < 1 {
		return 0, business.NewError(errors.InputInvalid, fmt.Errorf("ID无效"))
	}
	if argument.Password == "" {
		return 0, business.NewError(errors.InputInvalid, fmt.Errorf("登录密码为空"))
	}

	dbEntity, err := s.getUserById(argument.UserID)
	if err != nil {
		if s.sqlDatabase.IsNoRows(err) {
			return 0, business.NewError(errors.InputError, fmt.Errorf("ID不存在"))
		} else {
			return 0, business.NewError(errors.InputError, err)
		}
	}

	password := argument.Password
	if dbEntity.PasswordFormat != 0 {
		password, err = hash.Hash(argument.Password, dbEntity.PasswordFormat)
		if err != nil {
			return 0, business.NewError(errors.Unknown, err)
		}
	}
	if password == dbEntity.Password {
		return dbEntity.UserID, nil
	}

	if s.passwordFormat != 0 {
		password, err = hash.Hash(argument.Password, s.passwordFormat)
		if err != nil {
			return 0, business.NewError(errors.Unknown, err)
		}
	} else {
		password = argument.Password
	}
	dbEdit := &sqldb.DoctorUserAuthsPassword{
		Password:       password,
		PasswordFormat: s.passwordFormat,
	}
	dbFilter := &sqldb.DoctorUserAuthsInfoFilter{
		UserID: &argument.UserID,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	_, err = s.sqlDatabase.UpdateSelective(dbEdit, sqlFilter)
	if err != nil {
		return 0, business.NewError(errors.InternalError, err)
	}

	return argument.UserID, nil
}

func (s *Authentication) Detail(argument *doctor.ViewDoctorUserFilter) (*doctor.ViewDoctorUserEx, business.Error) {
	if argument == nil {
		return nil, business.NewError(errors.InputError, fmt.Errorf("参数为空"))
	}
	if argument.UserID < 1 {
		return nil, business.NewError(errors.InputInvalid, fmt.Errorf("用户ID'%d'无效", argument.UserID))
	}

	dbEntity := &sqldb.ViewDoctorUser{}
	dbFilter := &sqldb.ViewDoctorUserFilter{UserID: argument.UserID}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	err := s.sqlDatabase.SelectOne(dbEntity, sqlFilter)
	if err != nil {
		if s.sqlDatabase.IsNoRows(err) {
			return nil, business.NewError(errors.NotExist, err)
		} else {
			return nil, business.NewError(errors.InternalError, err)
		}
	}

	result := &doctor.ViewDoctorUserEx{}
	dbEntity.CopyToEx(result)

	return result, nil
}

func (s *Authentication) ChangePassword(userID uint64, oldPassword, newPassword string) business.Error {
	user, err := s.getUserById(userID)
	if err != nil {
		if s.sqlDatabase.IsNoRows(err) {
			business.NewError(errors.NotExist, err)
		} else {
			return business.NewError(errors.InternalError, err)
		}
	}

	// 验证旧密码
	dbPassword := user.Password
	if user.PasswordFormat == 0 {
		if dbPassword != oldPassword {
			return business.NewError(errors.InternalError, fmt.Errorf("旧密码不正确"))
		}
	} else {
		passwordHashed, err := hash.Hash(oldPassword, user.PasswordFormat)
		if err != nil {
			return business.NewError(errors.InternalError, err)
		}
		if strings.ToLower(passwordHashed) != strings.ToLower(dbPassword) {
			return business.NewError(errors.InternalError, fmt.Errorf("旧密码不正确"))
		}
	}

	// 设置新密码
	password := newPassword
	if s.passwordFormat != 0 {
		password, err = hash.Hash(newPassword, s.passwordFormat)
		if err != nil {
			return business.NewError(errors.Unknown, err)
		}
	}

	dbUpdate := &sqldb.DoctorUserAuthsPasswordUpdate{
		Password:       password,
		PasswordFormat: s.passwordFormat,
	}
	dbFilter := &sqldb.DoctorUserAuthsUpdateFilter{
		UserID: userID,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	_, err = s.sqlDatabase.Update(dbUpdate, sqlFilter)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}

	return nil
}

func (s *Authentication) authenticate(account, password string) (uint64, error) {
	dbEntity, err := s.getUserByAccount(account, nil)
	if err != nil {
		return 0, err
	}

	dbPassword := dbEntity.Password
	if dbEntity.PasswordFormat == 0 {
		if dbPassword != password {
			return 0, fmt.Errorf("密码错误")
		}
	} else {
		passwordHashed, err := hash.Hash(password, dbEntity.PasswordFormat)
		if err != nil {
			return 0, err
		}
		if strings.ToLower(passwordHashed) != strings.ToLower(dbPassword) {
			return 0, fmt.Errorf("密码错误")
		}
	}

	if dbEntity.Status == 1 {
		return 0, fmt.Errorf("账号已被冻结")
	} else if dbEntity.Status == 9 {
		return 0, fmt.Errorf("账号已注销")
	}

	now := time.Now()
	dbUpdate := &sqldb.DoctorUserAuthsLoginUpdate{
		LastLoginDateTime: &now,
		LoginCount:        dbEntity.LoginCount + 1,
	}
	dbFilter := &sqldb.DoctorUserAuthsIdFilter{
		UserID: dbEntity.UserID,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	s.sqlDatabase.Update(dbUpdate, sqlFilter)

	return dbEntity.UserID, nil
}

func (s *Authentication) logined(userID uint64, token *model.Token) {
	if userID < 1 || token == nil {
		return
	}

	dbEntity := &sqldb.VerifiedDoctorUser{}
	dbFilter := &sqldb.VerifiedDoctorUserFilter{UserID: userID}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	err := s.sqlDatabase.SelectOne(dbEntity, sqlFilter)
	if err == nil {
		if dbEntity.UserType != nil {
			token.UserType = *dbEntity.UserType
			//if token.UserType == enum.DoctorUserTypes.DoctorAndHealthManager().Key {
			//	token.UserType = enum.DoctorUserTypes.Doctor().Key
			//}
		}
		if dbEntity.OrgCode != nil {
			token.OrgCode = *dbEntity.OrgCode
		}
	}

	dbEntity2 := &sqldb.DoctorUserBaseInfo{}
	dbFilter2 := &sqldb.DoctorUserBaseInfoFilter{UserID: userID}
	sqlFilter2 := s.sqlDatabase.NewFilter(dbFilter2, false, false)
	err = s.sqlDatabase.SelectOne(dbEntity2, sqlFilter2)
	if err == nil {
		token.UserName = dbEntity2.Name
	}

	filter := &doctor.DoctorAddedRightFilter{
		UserID: userID,
	}
	var rightIDList []uint64
	data, be := s.listAccountRights(filter)
	if be == nil {
		for _, v := range data {
			rightIDList = append(rightIDList, v.RightID)
		}
		token.RightIDList = rightIDList
	}

}

func (s *Authentication) getUserByAccount(account string, excludedId *uint64) (*sqldb.DoctorUserAuths, error) {
	sqlFilters := make([]database.SqlFilter, 0)

	dbFilter := &sqldb.DoctorUserAuthsAuthenticationFilter{
		UserName:    account,
		MobilePhone: account,
		Email:       account,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, true, false)
	sqlFilters = append(sqlFilters, sqlFilter)

	if excludedId != nil {
		dbFilter := &sqldb.DoctorUserAuthsExcludedIdFilter{
			UserID: *excludedId,
		}
		sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
		sqlFilters = append(sqlFilters, sqlFilter)
	}

	dbEntity := &sqldb.DoctorUserAuths{}
	err := s.sqlDatabase.SelectOne(dbEntity, sqlFilters...)
	if err != nil {
		if s.sqlDatabase.IsNoRows(err) {
			return nil, fmt.Errorf("账号'%s'不存在", account)
		}
		return nil, err
	}

	return dbEntity, nil
}

func (s *Authentication) getUserById(id uint64) (*sqldb.DoctorUserAuths, error) {
	dbEntity := &sqldb.DoctorUserAuths{}
	dbFilter := &sqldb.DoctorUserAuthsUpdateFilter{
		UserID: id,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	err := s.sqlDatabase.SelectOne(dbEntity, sqlFilter)
	if err != nil {
		return nil, err
	}

	return dbEntity, nil
}
func (s *Authentication) listAccountRights(filter *doctor.DoctorAddedRightFilter) ([]*doctor.DoctorAddedRight, business.Error) {
	results := make([]*doctor.DoctorAddedRight, 0)

	dbEntity := &sqldb.DoctorAddedRight{}
	var dbFilter *sqldb.DoctorAddedRightFilter = nil
	if filter != nil {
		dbFilter = &sqldb.DoctorAddedRightFilter{}
		dbFilter.CopyFrom(filter)
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)

	err := s.sqlDatabase.SelectList(dbEntity, func() {
		result := &doctor.DoctorAddedRight{}
		dbEntity.CopyTo(result)
		results = append(results, result)
	}, nil, sqlFilter)

	if err != nil {
		return results, business.NewError(errors.InternalError, err)
	}

	return results, nil
}
