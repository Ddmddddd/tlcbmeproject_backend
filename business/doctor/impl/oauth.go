package impl

import (
	"tlcbme_project/business"
	"tlcbme_project/business/doctor/api"
	"tlcbme_project/data/entity/sqldb"
	"tlcbme_project/data/model/oauth"
	"tlcbme_project/database"
	"tlcbme_project/database/memory"
	"tlcbme_project/errors"
	"tlcbme_project/server/config"
	"tlcbme_project/server/notify"
	"encoding/json"
	"fmt"
	"github.com/ktpswjz/httpserver/types"
	"time"
)

type Oauth struct {
	base
}

func NewOauth(cfg *config.Config, log types.Log, sqlDatabase database.SqlDatabase, memoryToken memory.Token, notifyChannels notify.ChannelCollection, client business.Client) api.Oauth {
	instance := &Oauth{}
	instance.SetLog(log)
	instance.cfg = cfg
	instance.sqlDatabase = sqlDatabase
	instance.client = client
	instance.memoryToken = memoryToken
	instance.notifyChannels = notifyChannels

	return instance
}

func (s *Oauth) GetOauthUserInfo(argument *oauth.Input) (*oauth.UserAccount, business.Error) {
	tokenUrl := s.cfg.Platform.Providers.Api.BaseUrl + s.cfg.Platform.Providers.Api.Uri.TokenData
	tokenUrl += "?appid=" + s.cfg.Platform.Providers.AppId
	tokenUrl += "&secret=" + s.cfg.Platform.Providers.AppSecret
	tokenUrl += "&code=" + argument.Code
	data, code, err := s.client.Post(tokenUrl, nil)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}
	result := &oauth.Output{}
	userResult := &oauth.UserResult{}
	if code == 200 {
		err = json.Unmarshal(data, result)
		if err != nil {
			return nil, business.NewError(errors.InternalError, err)
		}
		userUrl := s.cfg.Platform.Providers.Api.BaseUrl + s.cfg.Platform.Providers.Api.Uri.UserData
		userUrl += "?token=" + result.Token
		userUrl += "&openid=" + argument.OpenId
		userUrl += "&code=" + argument.Code
		userdata, userCode, err := s.client.Get(userUrl, nil)
		if err != nil {
			return nil, business.NewError(errors.InternalError, err)
		}
		if userCode == 200 {
			err = json.Unmarshal(userdata, userResult)
			if err != nil {
				return nil, business.NewError(errors.InternalError, err)
			}
			return s.oauthLogin(userResult)
		}
	} else {
		return nil, nil
	}

	return nil, nil
}

func (s *Oauth) oauthLogin(userResult *oauth.UserResult) (*oauth.UserAccount, business.Error) {
	sqlFilters := make([]database.SqlFilter, 0)
	dbFilter := &sqldb.DoctorUserAuthsAuthenticationFilter{
		UserName:    userResult.UserName,
		MobilePhone: userResult.UserNo,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, true, false)
	sqlFilters = append(sqlFilters, sqlFilter)
	dbEntity := &sqldb.DoctorUserAuths{}
	err := s.sqlDatabase.SelectOne(dbEntity, sqlFilters...)
	if err != nil {
		if s.sqlDatabase.IsNoRows(err) {
			return nil, business.NewError(errors.InternalError, fmt.Errorf("账号不存在"))
		}
		return nil, business.NewError(errors.InternalError, fmt.Errorf("账号不存在"))
	}
	if dbEntity.Status == 1 {
		return nil, business.NewError(errors.InternalError, fmt.Errorf("账号已被冻结"))
	} else if dbEntity.Status == 9 {
		return nil, business.NewError(errors.InternalError, fmt.Errorf("账号已被注销"))
	}

	now := time.Now()
	dbUpdate := &sqldb.DoctorUserAuthsLoginUpdate{
		LastLoginDateTime: &now,
		LoginCount:        dbEntity.LoginCount + 1,
	}
	authDbFilter := &sqldb.DoctorUserAuthsIdFilter{}
	sqlFilter = s.sqlDatabase.NewFilter(authDbFilter, false, false)
	s.sqlDatabase.Update(dbUpdate, sqlFilter)

	return &oauth.UserAccount{
		UserName: dbEntity.UserName,
		UserId:   dbEntity.UserID,
	}, nil
}
