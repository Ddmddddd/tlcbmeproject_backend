package impl

import (
	"tlcbme_project/business"
	"tlcbme_project/business/doctor/api"
	"tlcbme_project/data/entity/sqldb"
	"tlcbme_project/data/model/stat"
	"tlcbme_project/database"
	"tlcbme_project/database/memory"
	"tlcbme_project/server/config"
	"tlcbme_project/server/notify"
	"github.com/ktpswjz/httpserver/types"
	"time"
)

type Stat struct {
	base
}

const (
	Three_Months = 1
	Six_Months   = 2

	Year_Count = 3
	All_Count  = 4
)

const (
	Data_Patient_All    = 1
	Data_Patient_Active = 2

	Data_BloodPressure_level_New = 3
	Data_BloodPressure_level_One = 4
	Data_BloodPressure_level_Two = 5
	Data_BloodPressure_level_End = 6

	Data_BloodGlcouse_level_New   = 7
	Data_BloodGlcouse_level_One   = 8
	Data_BloodGlcouse_level_Two   = 9
	Data_BloodGlcouse_level_Three = 10
	Data_BloodGlcouse_level_End   = 11

	Data_BloodPressure_All_Count      = 12
	Data_BloodPressure_Abnormal_Count = 13

	Data_BloodPressure_Rate = 14

	Data_BloodGlcouse_All_Count      = 15
	Data_BloodGlcouse_Abnormal_Count = 16

	Data_BloodGlcouse_Rate = 17

	Data_Sport  = 18
	Data_Diet   = 19
	Data_Drug   = 20
	Data_Weight = 21

	Data_Followup_Normal = 22
	Data_Followup_Alret  = 23

	Data_Alert_Rate  = 24
	Data_Alert_Count = 25

	Data_BloodPressure_High_Risk   = 26
	Data_BloodPressure_Medium_Risk = 27
	Data_BloodPressure_Low_Risk    = 28

	Data_BloodGlcouse_High_Risk   = 29
	Data_BloodGlcouse_Medium_Risk = 30
	Data_BloodGlcouse_Low_Risk    = 31
)

func NewStat(cfg *config.Config, log types.Log, sqlDatabase database.SqlDatabase, memoryToken memory.Token, notifyChannels notify.ChannelCollection, client business.Client) api.Stat {
	instance := &Stat{}
	instance.SetLog(log)
	instance.cfg = cfg
	instance.sqlDatabase = sqlDatabase
	instance.client = client
	instance.memoryToken = memoryToken
	instance.notifyChannels = notifyChannels

	return instance
}

func (s *Stat) GetPatientManageCount(input *stat.StatInput) ([]*stat.StatOut, business.Error) {
	return s.getData(input, Data_Patient_All, Data_Patient_Active)
}

func (s *Stat) GetBloodPressureLevelCount(input *stat.StatInput) ([]*stat.StatOut, business.Error) {
	return s.getData(input, Data_BloodPressure_level_New, Data_BloodPressure_level_One,
		Data_BloodPressure_level_Two, Data_BloodPressure_level_End)
}

func (s *Stat) GetBloodGlucoseLevelCount(input *stat.StatInput) ([]*stat.StatOut, business.Error) {
	return s.getData(input, Data_BloodGlcouse_level_New, Data_BloodGlcouse_level_One,
		Data_BloodGlcouse_level_Two, Data_BloodGlcouse_level_Three, Data_BloodGlcouse_level_End)
}

func (s *Stat) GetBloodPressureCount(input *stat.StatInput) ([]*stat.StatOut, business.Error) {
	return s.getData(input, Data_BloodPressure_All_Count, Data_BloodPressure_Abnormal_Count)
}

func (s *Stat) GetBloodPressureRate(input *stat.StatInput) ([]*stat.StatOut, business.Error) {
	return s.getData(input, Data_BloodPressure_Rate)
}

func (s *Stat) GetBloodGlucoseCount(input *stat.StatInput) ([]*stat.StatOut, business.Error) {
	return s.getData(input, Data_BloodGlcouse_All_Count, Data_BloodGlcouse_Abnormal_Count)
}

func (s *Stat) GetBloodGlucoseRate(input *stat.StatInput) ([]*stat.StatOut, business.Error) {
	return s.getData(input, Data_BloodGlcouse_Rate)
}

func (s *Stat) GetOtherData(input *stat.StatInput) ([]*stat.StatOut, business.Error) {
	return s.getData(input, Data_Diet, Data_Sport, Data_Weight, Data_Drug)
}

func (s *Stat) GetFollowupData(input *stat.StatInput) ([]*stat.StatOut, business.Error) {
	return s.getData(input, Data_Followup_Normal, Data_Followup_Alret)
}

func (s *Stat) GetAlertData(input *stat.StatInput) ([]*stat.StatOut, business.Error) {
	return s.getData(input, Data_Alert_Count, Data_Alert_Rate)
}

func (s *Stat) GetBloodPressureRiskData(input *stat.StatInput) ([]*stat.StatOut, business.Error) {
	return s.getData(input, Data_BloodPressure_High_Risk, Data_BloodPressure_Medium_Risk, Data_BloodPressure_Low_Risk)
}

func (s *Stat) GetBloodGlucoseRiskData(input *stat.StatInput) ([]*stat.StatOut, business.Error) {
	return s.getData(input, Data_BloodGlcouse_High_Risk, Data_BloodGlcouse_Medium_Risk, Data_BloodGlcouse_Low_Risk)
}

func (s *Stat) getSqlFilters(mode uint64, org string, dataTypes ...uint64) []database.SqlFilter {
	sqlFilters := make([]database.SqlFilter, 0)
	now := time.Now()
	if mode == Three_Months {
		j := 3
		if dataTypes[0] == Data_BloodPressure_High_Risk || dataTypes[0] == Data_BloodGlcouse_High_Risk {
			j = 1
		}
		for i := 0; i < j; i++ {
			for _, dataType := range dataTypes {
				dbFilter := &sqldb.StatisticrecordFilter{
					Year:    uint64(now.AddDate(0, -i, 0).Year()),
					Month:   uint64(now.AddDate(0, -i, 0).Month()),
					Orgcode: org,
					Type:    dataType,
				}
				sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, true)
				sqlFilters = append(sqlFilters, sqlFilter)
			}
		}
	} else if mode == Six_Months {
		for i := 0; i < 6; i++ {
			for _, dataType := range dataTypes {
				dbFilter := &sqldb.StatisticrecordFilter{
					Year:    uint64(now.AddDate(0, -i, 0).Year()),
					Month:   uint64(now.AddDate(0, -i, 0).Month()),
					Orgcode: org,
					Type:    dataType,
				}
				sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, true)
				sqlFilters = append(sqlFilters, sqlFilter)
			}
		}
	} else if mode == Year_Count {
		now := time.Now()
		yearStart := time.Date(now.Year(), 1, 1, 0, 0, 0, 0, time.Local)
		i := 0
		for now.AddDate(0, -i, 0).After(yearStart) {
			for _, dataType := range dataTypes {
				dbFilter := &sqldb.StatisticrecordFilter{
					Year:    uint64(now.AddDate(0, -i, 0).Year()),
					Month:   uint64(now.AddDate(0, -i, 0).Month()),
					Orgcode: org,
					Type:    dataType,
				}
				sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, true)
				sqlFilters = append(sqlFilters, sqlFilter)
			}
			i++
		}
		return sqlFilters
	} else if mode == All_Count {
		for _, dataType := range dataTypes {
			dbFilter := &sqldb.StatisticrecordAllFilter{
				Orgcode: org,
				Type:    dataType,
			}
			sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, true)
			sqlFilters = append(sqlFilters, sqlFilter)
		}
	}
	return sqlFilters
}

func (s *Stat) getData(input *stat.StatInput, dataTypes ...uint64) ([]*stat.StatOut, business.Error) {
	results := make([]*stat.StatOut, 0)
	dbEntity := &sqldb.Statisticrecord{}
	sqlFilters := s.getSqlFilters(input.Mode, input.OrgCode, dataTypes...)
	s.sqlDatabase.SelectList(dbEntity, func() {
		result := &stat.StatOut{}
		dbEntity.CopyToExt(result)
		results = append(results, result)
	}, nil, sqlFilters...)
	return results, nil
}
