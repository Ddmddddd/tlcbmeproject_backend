package impl

import (
	"encoding/json"
	"fmt"
	"github.com/ktpswjz/httpserver/types"
	"sort"
	"strconv"
	"strings"
	"time"
	"tlcbme_project/business"
	"tlcbme_project/business/doctor/api"
	"tlcbme_project/data/entity/sqldb"
	"tlcbme_project/data/enum"
	"tlcbme_project/data/model"
	"tlcbme_project/data/model/doctor"
	"tlcbme_project/data/model/manage"
	"tlcbme_project/database"
	"tlcbme_project/database/memory"
	"tlcbme_project/errors"
	"tlcbme_project/server/config"
	"tlcbme_project/server/notify"
)

type Exp struct {
	base
}

func NewExp(cfg *config.Config, log types.Log, sqlDatabase database.SqlDatabase, memoryToken memory.Token, notifyChannels notify.ChannelCollection, client business.Client) api.Exp {
	instance := &Exp{}
	instance.cfg = cfg
	instance.SetLog(log)
	instance.sqlDatabase = sqlDatabase
	instance.memoryToken = memoryToken
	instance.notifyChannels = notifyChannels
	instance.client = client

	return instance
}

//获取患者实验相关信息 弃用
//func (s *Exp) GetPatientExpInfo(argument *doctor.ExpPatientInfoGet) (*doctor.ExpPatient, business.Error) {
//	if argument == nil {
//		return nil, business.NewError(errors.InputError, fmt.Errorf("参数为空"))
//	}
//
//	dbFilter := &sqldb.ExpPatientIDFilter{
//		PatientID: argument.PatientID,
//	}
//	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
//	dbEntity := &sqldb.ExpPatient{}
//	err := s.sqlDatabase.SelectOne(dbEntity, sqlFilter)
//	if err != nil {
//		if s.sqlDatabase.IsNoRows(err) {
//			return nil, business.NewError(errors.InputInvalid, fmt.Errorf("患者未纳入管理"))
//		} else {
//			return nil, business.NewError(errors.InternalError, err)
//		}
//	}
//
//	data := &doctor.ExpPatient{}
//	dbEntity.CopyTo(data)
//
//	return data, nil
//}

//将患者纳入实验
func (s *Exp) IncludePatientInExp(argument *doctor.ExpPatientCreate, regFlag bool) business.Error {
	if argument == nil {
		return business.NewError(errors.InputError, fmt.Errorf("参数为空"))
	}
	if argument.PatientID == 0 {
		return business.NewError(errors.InputInvalid, fmt.Errorf("患者ID为空"))
	}
	if argument.Type == 0 {
		return business.NewError(errors.InputInvalid, fmt.Errorf("类型为空"))
	}

	//regFlag == true, 患者仍在注册过程中，且患者必然为糖尿病患者，否则需要判断患者是否在糖尿病管理中
	if !regFlag {
		dbFilter := &sqldb.ManagedPatientIndexPatientIDFilter{
			PatientID: argument.PatientID,
		}
		sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
		dbEntity := &sqldb.ManagedPatientIndex{}
		err := s.sqlDatabase.SelectOne(dbEntity, sqlFilter)
		if err != nil {
			if s.sqlDatabase.IsNoRows(err) {
				return business.NewError(errors.InputInvalid, fmt.Errorf("患者未纳入管理"))
			} else {
				return business.NewError(errors.InternalError, err)
			}
		} else if dbEntity.ManageStatus != 0 {
			return business.NewError(errors.InputInvalid, fmt.Errorf("患者已被终止管理"))
		} else if dbEntity.ManageClass != nil {
			manageClassArr := strings.Split(*dbEntity.ManageClass, ",")
			flag := false
			for _, manageClass := range manageClassArr {
				if manageClass == "糖尿病" {
					flag = true
					break
				}
			}
			if !flag {
				return business.NewError(errors.InputInvalid, fmt.Errorf("患者未纳入糖尿病管理"))
			}
		}

		dbExpFilter := &sqldb.ExpPatientIDFilter{
			PatientID: argument.PatientID,
		}
		sqlExpFilter := s.sqlDatabase.NewFilter(dbExpFilter, false, false)
		dbExpEntity := &sqldb.ExpPatient{}
		err = s.sqlDatabase.SelectOne(dbExpEntity, sqlExpFilter)
		if dbExpEntity.SerialNo != 0 {
			if dbExpEntity.Status == enum.ExpStatuses.InExp().Key || dbExpEntity.Status == enum.ExpStatuses.Scale().Key || dbExpEntity.Status == enum.ExpStatuses.Task().Key {
				return business.NewError(errors.InputInvalid, fmt.Errorf("患者已存在，请勿重复纳入"))
			} else {
				//患者曾纳入到实验中，又被移除，需要恢复管理
				statusArgument := &doctor.ExpPatientStatusChange{
					PatientID:      argument.PatientID,
					SerialNo:       dbExpEntity.SerialNo,
					Status:         enum.ExpStatuses.InExp().Key,
					UpdateDateTime: argument.CreateDateTime,
				}
				newErr := s.ChangePatientStatus(statusArgument, true)
				if newErr != nil {
					return newErr
				}

				return nil
			}
		}
	}

	//插入数据
	sqlAccess, err := s.sqlDatabase.NewAccess(true)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}
	defer sqlAccess.Close()

	dbEntityCreate := &sqldb.ExpPatient{}
	dbEntityCreate.CopyFromCreate(argument)
	dbEntityCreate.Status = enum.ExpStatuses.Task().Key

	_, err = sqlAccess.InsertSelective(dbEntityCreate)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}
	err = sqlAccess.Commit()
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}

	//go func(patientID uint64) {
	//	s.CreateDefaultScale(patientID)
	//}(argument.PatientID)

	//调用引擎，纳入实验，弃用
	go func(patientID uint64) {
		s.RegisterPatientExp(patientID, regFlag)
	}(argument.PatientID)

	return nil
}

//修改患者实验状态，包括移除出实验与重新纳入实验
func (s *Exp) ChangePatientStatus(argument *doctor.ExpPatientStatusChange, primary bool) business.Error {
	sqlAccess, err := s.sqlDatabase.NewAccess(true)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}
	defer sqlAccess.Close()

	dbUpdate := &sqldb.ExpPatientStatusUpdate{}
	dbUpdate.CopyFromStatusChange(argument)

	if primary == true {
		_, err = sqlAccess.UpdateSelectiveByPrimaryKey(dbUpdate)
	} else {
		dbFilter := &sqldb.ExpPatientIDFilter{
			PatientID: argument.PatientID,
		}
		sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)

		_, err = sqlAccess.Update(dbUpdate, sqlFilter)
	}

	if err != nil {
		return business.NewError(errors.InternalError, err)
	}
	err = sqlAccess.Commit()
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}

	//调用引擎，纳入实验
	go func(patientID uint64) {
		s.ChangePatientExpStatus(patientID)
	}(argument.PatientID)

	return nil
}

//获取所有实验量表
func (s *Exp) GetAllDoctorExpCommonScale() ([]*doctor.ExpCommonScale, business.Error) {
	data := make([]*doctor.ExpCommonScale, 0)
	dbFilter := &sqldb.ExpCommonScaleTypeExclude{
		ScaleType: enum.ScaleTypes.Default().Key,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)

	dbEntity := &sqldb.ExpCommonScale{}
	err := s.sqlDatabase.SelectList(dbEntity, func() {
		result := &doctor.ExpCommonScale{}
		dbEntity.CopyTo(result)
		data = append(data, result)
	}, nil, sqlFilter)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}

	return data, nil
}

//获取实验量表
func (s *Exp) GetExpCommonScale(argument *doctor.ExpCommonScaleSerialNoFilter) (*doctor.ExpCommonScale, business.Error) {
	dbFilter := &sqldb.ExpCommonScaleSerialNoFilter{
		SerialNo: argument.SerialNo,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	dbEntity := &sqldb.ExpCommonScale{}
	err := s.sqlDatabase.SelectOne(dbEntity, sqlFilter)
	if err != nil {
		if s.sqlDatabase.IsNoRows(err) {
			return nil, business.NewError(errors.InputInvalid, fmt.Errorf("量表不存在"))
		} else {
			return nil, business.NewError(errors.InternalError, err)
		}
	}

	data := &doctor.ExpCommonScale{}
	dbEntity.CopyTo(data)

	return data, nil
}

//提交患者量表记录
func (s *Exp) CommitPatientExpScaleRecord(argument *doctor.ExpScaleRecordCreate) ([]*doctor.ExpExerciseBase, business.Error) {
	sqlAccess, err := s.sqlDatabase.NewAccess(true)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}
	defer sqlAccess.Close()

	dbEntity := &sqldb.ExpScaleRecord{}
	dbEntity.CopyFromCreate(argument)

	//更新数据库
	_, err = sqlAccess.Insert(dbEntity)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}
	dbAssessEntity := &sqldb.AssessmentRecord{}
	dbAssessEntity.CopyFromScale(argument)
	_, err = sqlAccess.InsertSelective(dbAssessEntity)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}
	var exerciseList []*doctor.ExpExerciseBase
	//简单判断，存在不妥
	if argument.ScaleID == 4 {
		var scaleResult []*doctor.NewQuestion
		err = json.Unmarshal([]byte(dbEntity.Result), &scaleResult)
		if err != nil {
			return nil, business.NewError(errors.InternalError, err)
		}
		var businessErr business.Error
		exerciseList, businessErr = s.CreateExerciseListByScale(scaleResult)
		if err != nil {
			return nil, businessErr
		}
	}

	err = sqlAccess.Commit()
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}

	//dbPatientEntity := &sqldb.ExpPatient{}
	//dbFilter := &sqldb.ExpPatientIDFilter{
	//	PatientID: argument.PatientID,
	//}

	//sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	//s.sqlDatabase.SelectOne(dbPatientEntity, sqlFilter)
	//if err != nil {
	//	//患者未纳入实验，但想要做评估，不需要生活计划
	//	if s.sqlDatabase.IsNoRows(err) {
	//		go func(sno uint64) {
	//			s.CommitPatientExpScaleRecordToEng(sno, false)
	//		}(sno)
	//		return nil, nil
	//	}  else {
	//		return nil, business.NewError(errors.InternalError, err)
	//	}
	//}
	////患者已经被移出管理 或 问卷本身不需要，不需要生活计划
	//if dbPatientEntity.Status == 0 || argument.ScaleType != enum.ScaleTypes.Action().Key {
	//	go func(sno uint64) {
	//		_ = s.CommitPatientExpScaleRecordToEng(sno, false)
	//	}(sno)
	//	return nil, nil
	//} else {
	//	go func(sno uint64) {
	//		_ = s.CommitPatientExpScaleRecordToEng(sno, true)
	//	}(sno)
	//}

	//若所有量表已完成，将患者的实验状态置为正常
	//if len(argument.UndoneScales) == 1 && argument.UndoneScales[0] == argument.ScaleID {
	//	dbUpdateEntity := &sqldb.ExpPatientStatusUpdateByID{
	//		Status: enum.ExpStatuses.InExp().Key, //正常管理
	//		UpdateDateTime: dbEntity.CreateDateTime,
	//	}
	//	_, err = s.sqlDatabase.UpdateSelective(dbUpdateEntity, sqlFilter)
	//	if err != nil {
	//		return 0, business.NewError(errors.InternalError, err)
	//	}
	//}

	return exerciseList, nil
}

func (s *Exp) CreateExerciseListByScale(scaleResult []*doctor.NewQuestion) ([]*doctor.ExpExerciseBase, business.Error) {
	results := make([]*doctor.ExpExerciseBase, 0)
	dbEntity := &sqldb.ExpExerciseBase{}
	dbFilter := &sqldb.ExpExerciseBaseExclude{}

	snos := make(map[uint64]bool)
	if len(scaleResult[11].Checked) > 0 {
		for i := 0; i < len(scaleResult[11].Checked); i++ {
			dbFilter.Target = append(dbFilter.Target, scaleResult[11].Checked[i])
		}
		sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)

		dbSnoEntity := &sqldb.ExpExerciseBaseSerialNo{}
		err := s.sqlDatabase.SelectList(dbSnoEntity, func() {
			sno := dbSnoEntity.SerialNo
			snos[sno] = true
		}, nil, sqlFilter)
		if err != nil {
			return nil, business.NewError(errors.InternalError, err)
		}
	}

	emptyFilter := &sqldb.EmptyExpExerciseBaseFilter{}
	sqlFilter := s.sqlDatabase.NewFilter(emptyFilter, false, false)
	err := s.sqlDatabase.SelectList(dbEntity, func() {
		result := &doctor.ExpExerciseBase{}
		dbEntity.CopyTo(result)
		val, ok := snos[result.SerialNo]
		if !ok || !val {
			results = append(results, result)
		}
	}, nil, sqlFilter)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}

	return results, nil
}

func (s *Exp) GetPatientActionPlanRecordInfo(argument *doctor.ExpActionPlanRecordTimeFilter) ([]*doctor.ExpActionPlanRecordForDoctor, business.Error) {
	results := make([]*doctor.ExpActionPlanRecordForDoctor, 0)
	sqlAccess, err := s.sqlDatabase.NewAccess(true)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}
	defer sqlAccess.Close()

	dbEntity := &sqldb.ExpActionPlan{}
	dbFilter := &sqldb.ExpActionPlanPatientIDAndStatusFilter{
		PatientID: argument.PatientID,
		Status:    0, //正常状态
	}
	dbOrder := &sqldb.ExpActionPlanOrder{}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	startDateTime := time.Time(*argument.StartDateTime)
	err = sqlAccess.SelectList(dbEntity, func() {
		result := &doctor.ExpActionPlanRecordForDoctor{}
		result.ActionPlanID = dbEntity.SerialNo
		result.ActionPlan = dbEntity.ActionPlan
		result.Editor = enum.EditorTypes.Value(dbEntity.EditorType)
		result.Level = dbEntity.Level
		result.Status = dbEntity.Status
		dbRecordEntity := &sqldb.ExpActionPlanRecord{}
		dbRecordFilter := &sqldb.ExpActionPlanRecordForDoctorFilter{
			ActionPlanID:   result.ActionPlanID,
			CreateDateTime: &startDateTime,
			Completion:     2, //正常完成
		}
		sqlRecordFilter := s.sqlDatabase.NewFilter(dbRecordFilter, false, false)
		fullActionTimes, err := s.sqlDatabase.SelectCount(dbRecordEntity, sqlRecordFilter)
		if err != nil {
			return
		}
		dbRecordFilter.Completion = 1 //部分完成，无感记录
		partActionTimes, err := s.sqlDatabase.SelectCount(dbRecordEntity, sqlRecordFilter)
		if err != nil {
			return
		}
		var completeInfo strings.Builder
		completeInfo.WriteString("正常完成" + strconv.FormatUint(fullActionTimes, 10) + "次，部分完成" + strconv.FormatUint(partActionTimes, 10) + "次")
		result.CompleteInfo = completeInfo.String()
		results = append(results, result)
	}, dbOrder, sqlFilter)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}
	err = sqlAccess.Commit()
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}

	return results, nil
}

func (s *Exp) GetPatientTaskRecordList(argument *doctor.ExpPatientTaskRecordGetListInput) ([]*doctor.ExpPatientTaskRecords, business.Error) {
	results := make([]*doctor.ExpPatientTaskRecords, 0)
	dbEntity := &sqldb.ExpPatientTaskRecord{}
	sqlEntity := &sqldb.ExpPatientTaskRecordGetByPatientID{
		PatientID: argument.PatientID,
	}
	sqlFilter := s.sqlDatabase.NewFilter(sqlEntity, false, false)
	dbOrder := &sqldb.ExpPatientTaskRecordSerialNoOrder{}

	parentTaskMap := make(map[string]int)
	err := s.sqlDatabase.SelectList(dbEntity, func() {
		i, ok := parentTaskMap[*dbEntity.ParentTaskName]
		if !ok {
			result := &doctor.ExpPatientTaskRecords{
				ParentTaskName: *dbEntity.ParentTaskName,
				RecordTaskSnos: make([]uint64, 0),
				RecordTask:     make([]string, 0),
			}
			results = append(results, result)
			parentTaskMap[*dbEntity.ParentTaskName] = len(results) - 1
			i = len(results) - 1
		}
		results[i].RecordTaskSnos = append(results[i].RecordTaskSnos, dbEntity.SerialNo)
		results[i].RecordTask = append(results[i].RecordTask, dbEntity.TaskName)
	}, dbOrder, sqlFilter)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}

	return results, nil
}

func (s *Exp) GetPatientTaskRecord(argument *doctor.ExpPatientTaskRecordGetInput) (*doctor.ExpPatientTaskRecord, business.Error) {
	dbEntity := &sqldb.ExpPatientTaskRecord{}
	sqlEntity := &sqldb.ExpPatientTaskRecordGetBySerialNo{
		SerialNo: argument.SerialNo,
	}
	sqlFilter := s.sqlDatabase.NewFilter(sqlEntity, false, false)

	err := s.sqlDatabase.SelectOne(dbEntity, sqlFilter)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}

	result := &doctor.ExpPatientTaskRecord{}
	dbEntity.CopyTo(result)

	return result, nil
}

func (s *Exp) GetPatientDietRecord(argument *doctor.SportDietRecordFilter) (*doctor.SportDietRecord, business.Error) {
	dbEntity := &sqldb.SportDietRecord{}
	sqlEntity := &sqldb.SportDietRecordFilter{
		SerialNo: &argument.SerialNo,
	}
	sqlFilter := s.sqlDatabase.NewFilter(sqlEntity, false, false)

	err := s.sqlDatabase.SelectOne(dbEntity, sqlFilter)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}

	result := &doctor.SportDietRecord{}
	dbEntity.CopyTo(result)

	return result, nil
}

func (s *Exp) GetPatientDietRecordComment(argument *doctor.ExpDoctorCommentToPatientSportDietRecordFilter) (*doctor.ExpDoctorCommentToPatient, business.Error) {
	dbEntity := &sqldb.ExpDoctorCommentToPatient{}
	sqlEntity := &sqldb.ExpDoctorCommentToPatientSportDietRecordFilter{
		SportDietRecord: argument.SportDietRecord,
	}
	sqlFilter := s.sqlDatabase.NewFilter(sqlEntity, false, false)

	err := s.sqlDatabase.SelectOne(dbEntity, sqlFilter)
	if err != nil {
		if s.sqlDatabase.IsNoRows(err) {
			return nil, nil
		} else {
			return nil, business.NewError(errors.InternalError, err)
		}
	}

	result := &doctor.ExpDoctorCommentToPatient{}
	dbEntity.CopyTo(result)

	return result, nil
}

func (s *Exp) CommitPatientDietRecordComment(argument *doctor.ExpDoctorCommentToPatientCreate) (uint64, business.Error) {
	now := time.Now()
	if argument.SerialNo == 0 {
		dbEntity := &sqldb.ExpDoctorCommentToPatient{}
		dbFilter := &sqldb.ExpDoctorCommentToPatientSportDietRecordFilter{
			SportDietRecord: argument.SportDietRecord,
		}
		sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
		err := s.sqlDatabase.SelectOne(dbEntity, sqlFilter)
		if err != nil {
			if s.sqlDatabase.IsNoRows(err) {
				dbEntity.CopyFromCreate(argument)
				dbEntity.CreateDateTime = &now
				sno, err := s.sqlDatabase.InsertSelective(dbEntity)
				if err != nil {
					return 0, business.NewError(errors.InternalError, err)
				}
				return sno, nil
			}
			return 0, business.NewError(errors.InternalError, err)
		}

	}
	dbEntity := &sqldb.ExpDoctorCommentToPatientUpdate{}
	dbEntity.CopyFromCreate(argument)
	dbEntity.UpdateTime = &now
	sno, err := s.sqlDatabase.UpdateSelectiveByPrimaryKey(dbEntity)
	if err != nil {
		return 0, business.NewError(errors.InternalError, err)
	}

	return sno, nil
}

func (s *Exp) GetCommentTemplate(argument *doctor.ExpDoctorCommentTemplateType) ([]*doctor.ExpDoctorCommentTemplate, business.Error) {
	//1. query template
	dbTemps := make([]*doctor.ExpDoctorCommentTemplate, 0)
	dbEntity := &sqldb.ExpDoctorCommentTemplate{}
	dbFilter1 := &sqldb.ExpDoctorCommentTemplateTypeFilter{
		TemplateType: argument.TemplateType,
		OpenType:     1,
		EditorID:     argument.EditorID,
	}
	sqlFilter1 := s.sqlDatabase.NewFilter(dbFilter1, false, false)
	dbFilter2 := &sqldb.ExpDoctorCommentTemplateOpenTypeFilter{
		OpenType: 0,
	}
	sqlFilter2 := s.sqlDatabase.NewFilter(dbFilter2, false, true)
	dbOrder := &sqldb.ExpDoctorCommentTemplateOpenTypeOrderDesc{}
	err := s.sqlDatabase.SelectList(dbEntity, func() {
		result := &doctor.ExpDoctorCommentTemplate{}
		dbEntity.CopyTo(result)
		dbTemps = append(dbTemps, result)
	}, dbOrder, sqlFilter1, sqlFilter2)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}
	//2. query template order
	argument2 := &doctor.ExpDoctorCommentTemplateOrderEditor{
		EditorID: argument.EditorID,
	}
	dbTempOrder, err2 := s.getCommentTemplateOrder(argument2)
	if err2 != nil {
		return dbTemps, nil
	}
	order := make([]uint64, 0)
	err = json.Unmarshal([]byte(*dbTempOrder.TemplateOrder), &order)
	if err != nil {
		return dbTemps, business.NewError(errors.InternalError, err)
	}
	orderLen := len(order)
	if orderLen > 0 {
		//3. sort
		result := make([]*doctor.ExpDoctorCommentTemplate, 0)
		mapT := make(map[uint64]*doctor.ExpDoctorCommentTemplate, 0)
		for _, v := range dbTemps {
			mapT[v.SerialNo] = v
		}
		for _, v := range order {
			result = append(result, mapT[v])
			delete(mapT, v)
		}
		for _, v := range dbTemps {
			_, ok := mapT[v.SerialNo]
			if ok {
				result = append(result, mapT[v.SerialNo])
				delete(mapT, v.SerialNo)
			}
		}
		return result, nil
	}
	return dbTemps, nil
}

func (s *Exp) AddCommentTemplate(argument *doctor.ExpDoctorCommentTemplateCreate) business.Error {
	dbEntity := &sqldb.ExpDoctorCommentTemplate{}
	dbEntity.CopyFromCreate(argument)

	_, err := s.sqlDatabase.Insert(dbEntity)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}

	return nil
}

func (s *Exp) DeleteCommentTemplate(argument *doctor.ExpDoctorCommentTemplateDelete) business.Error {
	sqlAccess, errSql := s.sqlDatabase.NewAccess(true)
	if errSql != nil {
		return business.NewError(errors.InternalError, errSql)
	}
	defer sqlAccess.Close()
	//1. delete template table
	dbEntity := &sqldb.ExpDoctorCommentTemplate{}
	dbFilter := &sqldb.ExpDoctorCommentTemplateFilter{
		SerialNo: argument.SerialNo,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	count, err := sqlAccess.Delete(dbEntity, sqlFilter)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}
	if count > 0 {
		//2. update template order table
		dbEntity2 := &sqldb.ExpDoctorCommentTemplateOrder{}
		dbFilter2 := &sqldb.ExpDoctorCommentTemplateOrderEditorFilter{
			EditorID: argument.EditorID,
		}
		sqlFilter2 := s.sqlDatabase.NewFilter(dbFilter2, false, false)
		count, err = sqlAccess.SelectCount(dbEntity2, sqlFilter2)
		if err != nil {
			return business.NewError(errors.InternalError, err)
		}
		if count > 0 {
			err = sqlAccess.SelectOne(dbEntity2, sqlFilter2)
			if err != nil {
				return business.NewError(errors.InternalError, err)
			}
			order := make([]uint64, 0)
			err = json.Unmarshal([]byte(*dbEntity2.TemplateOrder), &order)
			if err != nil {
				return business.NewError(errors.InternalError, err)
			}
			orderUpdate := order[:0]
			for _, v := range order {
				if v != argument.SerialNo {
					orderUpdate = append(orderUpdate, v)
				}
			}
			if len(orderUpdate) > 0 {
				// update
				orderBt, err2 := json.Marshal(orderUpdate)
				orderStr := string(orderBt)
				if err2 != nil {
					return business.NewError(errors.InternalError, err)
				}
				dbEntity2.TemplateOrder = &orderStr
				_, err = sqlAccess.UpdateByPrimaryKey(dbEntity2)
				if err2 != nil {
					return business.NewError(errors.InternalError, err)
				}
			} else {
				// delete
				_, err = sqlAccess.Delete(dbEntity2, sqlFilter2)
				if err != nil {
					return business.NewError(errors.InternalError, err)
				}
			}
		}
	}
	err = sqlAccess.Commit()
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}

	return nil
}

func (s *Exp) getCommentTemplateOrder(argument *doctor.ExpDoctorCommentTemplateOrderEditor) (*sqldb.ExpDoctorCommentTemplateOrder, error) {
	dbEntity := &sqldb.ExpDoctorCommentTemplateOrder{}
	dbFilter := &sqldb.ExpDoctorCommentTemplateOrderEditorFilter{
		EditorID: argument.EditorID,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	err := s.sqlDatabase.SelectOne(dbEntity, sqlFilter)
	if err != nil {
		return nil, err
	}
	return dbEntity, nil
}

func (s *Exp) TopCommentTemplate(argument *doctor.ExpDoctorCommentTemplateDelete) business.Error {
	//1. query template table
	dbEntity := &sqldb.ExpDoctorCommentTemplate{}
	dbFilter := &sqldb.ExpDoctorCommentTemplateEditorFilter{
		EditorID: argument.EditorID,
		SerialNo: argument.SerialNo,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	dbFilter2 := &sqldb.ExpDoctorCommentTemplateOpenTypeFilter2{
		OpenType: 0,
		SerialNo: argument.SerialNo,
	}
	sqlFilter2 := s.sqlDatabase.NewFilter(dbFilter2, false, true)
	count, err := s.sqlDatabase.SelectCount(dbEntity, sqlFilter, sqlFilter2)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}
	if count == 0 {
		return nil
	}
	//2. update template order table
	dbEntity3 := &sqldb.ExpDoctorCommentTemplateOrder{}
	dbFilter3 := &sqldb.ExpDoctorCommentTemplateOrderEditorFilter{
		EditorID: argument.EditorID,
	}
	sqlFilter3 := s.sqlDatabase.NewFilter(dbFilter3, false, false)
	count, err = s.sqlDatabase.SelectCount(dbEntity3, sqlFilter3)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}
	if count > 0 {
		// update
		err = s.sqlDatabase.SelectOne(dbEntity3, sqlFilter3)
		if err != nil {
			return business.NewError(errors.InternalError, err)
		}
		order := make([]uint64, 0)
		err = json.Unmarshal([]byte(*dbEntity3.TemplateOrder), &order)
		if err != nil {
			return business.NewError(errors.InternalError, err)
		}
		orderUpdate := order[:0]
		for _, v := range order {
			if v != argument.SerialNo {
				orderUpdate = append(orderUpdate, v)
			}
		}
		orderUpdate = append(orderUpdate, 0)
		copy(orderUpdate[1:], orderUpdate[0:])
		orderUpdate[0] = argument.SerialNo
		orderBt, err2 := json.Marshal(orderUpdate)
		orderStr := string(orderBt)
		if err2 != nil {
			return business.NewError(errors.InternalError, err)
		}
		dbEntity3.TemplateOrder = &orderStr
		_, err = s.sqlDatabase.UpdateByPrimaryKey(dbEntity3)
		if err != nil {
			return business.NewError(errors.InternalError, err)
		}
	} else {
		// insert
		order := []uint64{argument.SerialNo}
		orderBt, err2 := json.Marshal(order)
		orderStr := string(orderBt)
		if err2 != nil {
			return business.NewError(errors.InternalError, err)
		}
		dbEntity4 := &sqldb.ExpDoctorCommentTemplateOrder{
			EditorID:      argument.EditorID,
			TemplateOrder: &orderStr,
		}
		_, err2 = s.sqlDatabase.Insert(dbEntity4)
		if err2 != nil {
			return business.NewError(errors.InternalError, err)
		}
	}

	return nil
}

func (s *Exp) GetUnCommentTaskCount(argument *doctor.ExpPatientTaskRecordGetByName) (uint64, business.Error) {
	//sqlAccess, err := s.sqlDatabase.NewAccess(false)
	//if err != nil {
	//	return 0, business.NewError(errors.InternalError, err)
	//}
	//defer sqlAccess.Close()
	//
	//sqlString := `select count(a.SerialNo) from ExpPatientTaskRecord as a right join
	//(select serialNo, JSON_EXTRACT(Record, '$.sportDietRecord.serialNo') as sportDietRecord from ExpPatientTaskRecord where TaskName = ?) as tmp
	//on a.SerialNo = tmp.serialNo where not EXISTS (select SportDietRecord from ExpDoctorCommentToPatient as b where tmp.sportDietRecord = b.SportDietRecord)`
	//row := sqlAccess.QueryRow(sqlString, argument.TaskName)
	//count := uint64(0)
	//err = row.Scan(&count)
	dbEntity := &sqldb.ViewCommentTask{}
	dbFilter := &sqldb.ViewCommentTaskHealthManagerIDFilter{
		HealthManagerID: &argument.HealthManagerID,
		TaskName:        &argument.TaskName,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	count, err := s.sqlDatabase.SelectCount(dbEntity, sqlFilter)
	if err != nil {
		return 0, business.NewError(errors.InternalError, err)
	}

	return count, nil
}

func (s *Exp) GetUnCommentTaskPage(index, size uint64, filter *doctor.ExpPatientTaskRecordGetByName) (*model.PageResult, business.Error) {
	pageResult := &model.PageResult{}
	results := make([]*doctor.ExpPatientTaskRecordForApp, 0)
	dbEntity := &sqldb.ViewCommentTask{}
	dbFilter := &sqldb.ViewCommentTaskHealthManagerIDFilter{}
	dbFilter.CopyFrom(filter)
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	dbOrder := &sqldb.ViewCommentTaskTimeOrderDesc{}
	err := s.sqlDatabase.SelectPage(dbEntity, func(total, page, size, index uint64) {
		pageResult.Total = total
		pageResult.Count = page
		pageResult.Size = size
		pageResult.Index = index
	}, func() {
		result := &doctor.ExpPatientTaskRecordForApp{}
		dbEntity.CopyToApp(result)
		results = append(results, result)
	}, size, index, dbOrder, sqlFilter)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}
	//dbEntity := &sqldb.ExpPatientTaskRecordForApp{}
	//sqlAccess, err := s.sqlDatabase.NewAccess(false)
	//if err != nil {
	//	return nil, business.NewError(errors.InternalError, err)
	//}
	//defer sqlAccess.Close()
	//
	//startIndex := (index - 1) * size
	//sqlString := `select a.SerialNo, PatientID, TaskID, ParentTaskName, TaskName, CreateDateTime, Record from ExpPatientTaskRecord as a right join
	//(select serialNo, JSON_EXTRACT(Record, '$.sportDietRecord.serialNo') as sportDietRecord from ExpPatientTaskRecord where TaskName = ?) as tmp
	//on a.SerialNo = tmp.serialNo where not EXISTS (select SportDietRecord from ExpDoctorCommentToPatient as b where tmp.sportDietRecord = b.SportDietRecord) limit ?, ?`
	//rows, err := sqlAccess.Query(sqlString, filter.TaskName, startIndex, size)
	//if err != nil {
	//	return nil, business.NewError(errors.InternalError, err)
	//}
	//defer rows.Close()
	//
	//for rows.Next() {
	//	err = rows.Scan(&dbEntity.SerialNo, &dbEntity.PatientID, &dbEntity.TaskID, &dbEntity.ParentTaskName, &dbEntity.TaskName, &dbEntity.CreateDateTime, &dbEntity.Record)
	//	if err != nil {
	//		return nil, business.NewError(errors.InternalError, err)
	//	}
	//	result := &doctor.ExpPatientTaskRecordForApp{}
	//	dbEntity.CopyToApp(result)
	//	results = append(results, result)
	//}
	//if err != nil {
	//	return nil, business.NewError(errors.InternalError, err)
	//}
	//
	//for _, item := range results {
	//	dbEntity := &sqldb.ViewPatientBaseInfo{}
	//	dbFilter := &sqldb.ViewPatientBaseInfoFilter{
	//		PatientID: &item.PatientID,
	//	}
	//	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	//	err := s.sqlDatabase.SelectOne(dbEntity, sqlFilter)
	//	if err != nil {
	//		return nil, business.NewError(errors.InternalError, err)
	//	}
	//
	//	dbEntity.CopyToApp(item)
	//}

	pageResult.Data = results
	//pageResult.Index = index
	//pageResult.Size = size
	//total, busErr := s.GetUnCommentTaskCount(&doctor.ExpPatientTaskRecordGetByName{TaskName:"拍照"})
	//if busErr != nil {
	//	return nil, busErr
	//}
	//pageResult.Total = total
	//pageResult.Count = uint64(len(results))

	return pageResult, nil
}

func (s *Exp) GetCurrentDayWithoutClockingInPage(index, size uint64, filter *doctor.ExpPatientTaskRecordGetByName) (*model.PageResult, business.Error) {
	pageResult := &model.PageResult{}
	results := make([]*doctor.ExpPatientTaskRecordForApp, 0)
	dbEntity := &sqldb.ExpPatientTaskRecord{}
	dbFilter := &sqldb.ExpPatientTaskRecordFilter{}
	dbFilter.CopyFrom(filter)
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	list := make([]uint64, 0)
	err := s.sqlDatabase.SelectList(dbEntity, func() {
		list = append(list, dbEntity.PatientID)
	}, nil, sqlFilter)

	dbEntity2 := &sqldb.ViewManagedIndex{}
	dbFilter2 := &sqldb.ManagedPatientIndexFilter{
		PatientID: list,
	}
	sqlFilter2 := s.sqlDatabase.NewFilter(dbFilter2, false, false)
	dbFilter3 := &sqldb.ManagedPatientIndexDoctorIDFilter{}
	if filter.DoctorID > 0 {
		dbFilter3.DoctorID = &filter.DoctorID
	}
	if filter.HealthManagerID > 0 {
		dbFilter3.HealthManagerID = &filter.HealthManagerID
	}

	sqlFilter3 := s.sqlDatabase.NewFilter(dbFilter3, true, false)

	err = s.sqlDatabase.SelectPage(dbEntity2, func(total, page, size, index uint64) {
		pageResult.Total = total
		pageResult.Count = page
		pageResult.Size = size
		pageResult.Index = index
	}, func() {
		result := &doctor.ExpPatientTaskRecordForApp{}
		dbEntity2.CopyToApp(result)
		results = append(results, result)
	}, size, index, nil, sqlFilter2, sqlFilter3)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}

	pageResult.Data = results
	return pageResult, nil
}

func (s *Exp) GetCommentRecordPage(index, size uint64, filter *doctor.ExpPatientTaskRecordGetByNameAll) (*model.PageResult, business.Error) {
	pageResult := &model.PageResult{}
	results := make([]*doctor.ExpPatientTaskRecordForApp, 0)
	dbEntity := &sqldb.ViewCommentRecord{}
	dbFilter := &sqldb.ViewCommentRecordAllFilter{}
	dbFilter.CopyFromAll(filter)
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	dbOrder := &sqldb.ViewCommentRecordTimeOrderDesc{}
	err := s.sqlDatabase.SelectPage(dbEntity, func(total, page, size, index uint64) {
		pageResult.Total = total
		pageResult.Count = page
		pageResult.Size = size
		pageResult.Index = index
	}, func() {
		result := &doctor.ExpPatientTaskRecordForApp{}
		dbEntity.CopyToApp(result)
		results = append(results, result)
	}, size, index, dbOrder, sqlFilter)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}
	//for _, item := range results {
	//	dbEntity := &sqldb.ViewPatientBaseInfo{}
	//	dbFilter := &sqldb.ViewPatientBaseInfoFilter{
	//		PatientID: &item.PatientID,
	//	}
	//	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	//	err := s.sqlDatabase.SelectOne(dbEntity, sqlFilter)
	//	if err != nil {
	//		return nil, business.NewError(errors.InternalError, err)
	//	}
	//
	//	dbEntity.CopyToApp(item)
	//}

	pageResult.Data = results
	return pageResult, nil
}

func (s *Exp) DeleteCommentRecord(argument *doctor.ExpPatientTaskRecordDelete) business.Error {
	sqlAccess, err := s.sqlDatabase.NewAccess(true)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}
	defer sqlAccess.Close()

	dbFilter := &sqldb.ExpPatientTaskListSno{
		SerialNo: argument.SerialNo,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	dbEntity := &sqldb.ExpPatientTaskRecord{}
	count, err2 := sqlAccess.SelectCount(dbEntity, sqlFilter)
	if err2 != nil {
		return nil
	}
	if count > 0 {
		err = sqlAccess.SelectOne(dbEntity, sqlFilter)
		if err != nil {
			return business.NewError(errors.InternalError, err)
		}
		// delete exppatienttaskrecord
		_, err = sqlAccess.Delete(dbEntity, sqlFilter)
		if err != nil {
			return business.NewError(errors.InternalError, err)
		}
		// delete expdoctorcommenttopatient
		record := dbEntity.Record
		recordJson := &doctor.ExpPatientTaskRecordJson{}
		if record != nil {
			err = json.Unmarshal([]byte(*record), &recordJson)
			if err != nil {
				return business.NewError(errors.InternalError, err)
			}
			sportDietRecord := recordJson.SportDietRecord.SerialNo
			if sportDietRecord != 0 {
				dbEntity2 := &sqldb.ExpDoctorCommentToPatient{}
				dbFilter2 := &sqldb.ExpDoctorCommentToPatientSportDietRecordFilter{
					SportDietRecord: sportDietRecord,
				}
				sqlFilter2 := s.sqlDatabase.NewFilter(dbFilter2, false, false)
				_, err = sqlAccess.Delete(dbEntity2, sqlFilter2)
				if err != nil {
					return business.NewError(errors.InternalError, err)
				}
			}
		}
	}

	err = sqlAccess.Commit()
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}
	return nil
}

func (s *Exp) GetPatientCheckData(argument *doctor.ExpPatientTaskRecordGetCheckDataInput) ([]*doctor.ExpPatientTaskRecordCheckData, business.Error) {
	results := make([]*doctor.ExpPatientTaskRecordCheckData, 0)
	dbEntity := &sqldb.ExpPatientTaskRecord{}
	dbFilter := &sqldb.ExpPatientTaskRecordGetByPatientIDAndParentTaskName{
		PatientID: argument.PatientID,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	for _, item := range argument.ParentTaskName {
		dbFilter.ParentTaskName = &item
		result := &doctor.ExpPatientTaskRecordCheckData{
			PatientID: argument.PatientID,
		}
		result.ParentTaskName = item
		count, err := s.sqlDatabase.SelectCount(dbEntity, sqlFilter)
		if err != nil {
			return nil, business.NewError(errors.InternalError, err)
		}
		result.Count = count
		results = append(results, result)
	}

	return results, nil
}

func (s *Exp) GetPatientBodyExamImage(patientID uint64) ([]*doctor.ExpPatientBodyExamImage, business.Error) {
	results := make([]*doctor.ExpPatientBodyExamImage, 0)
	dbEntity := &sqldb.ExpPatientBodyExamImage{}
	dbFilter := &sqldb.ExpPatientBodyExamImageFilter{
		PatientID: patientID,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	err := s.sqlDatabase.SelectList(dbEntity, func() {
		result := &doctor.ExpPatientBodyExamImage{}
		dbEntity.CopyTo(result)
		results = append(results, result)
	}, nil, sqlFilter)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}

	return results, nil
}

func (s *Exp) UploadPatientFile(argument *doctor.ExpPatientUploadFile) business.Error {
	dbEntity := &sqldb.ExpPatientUploadFile{}
	dbEntity.CopyFromCreate(argument)
	if dbEntity.SerialNo == 0 {
		_, err := s.sqlDatabase.InsertSelective(dbEntity)
		if err != nil {
			return business.NewError(errors.InternalError, err)
		}
	} else {
		_, err := s.sqlDatabase.UpdateByPrimaryKey(dbEntity)
		if err != nil {
			return business.NewError(errors.InternalError, err)
		}
	}

	return nil
}

func (s *Exp) GetPatientFile(argument *doctor.ExpPatientUploadFileGet) (*doctor.ExpPatientUploadFile, business.Error) {
	dbEntity := &sqldb.ExpPatientUploadFile{}
	dbFilter := &sqldb.ExpPatientUploadFileGet{
		PatientID: argument.PatientID,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	err := s.sqlDatabase.SelectOne(dbEntity, sqlFilter)
	if err != nil {
		if s.sqlDatabase.IsNoRows(err) {
			return nil, nil
		}
		return nil, business.NewError(errors.InternalError, err)
	}

	result := &doctor.ExpPatientUploadFile{}
	dbEntity.CopyTo(result)

	return result, nil
}

func (s *Exp) GetPatientDietPlan(argument *doctor.ExpPatientDietPlanGet) ([]*doctor.ExpPatientDietPlan, business.Error) {
	results := make([]*doctor.ExpPatientDietPlan, 0)
	dbEntity := &sqldb.ExpPatientDietPlan{}
	dbFilter := &sqldb.ExpPatientDietPlanFilter{
		PatientID: argument.PatientID,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	err := s.sqlDatabase.SelectList(dbEntity, func() {
		result := &doctor.ExpPatientDietPlan{}
		dbEntity.CopyTo(result)
		results = append(results, result)
	}, nil, sqlFilter)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}
	return results, nil
}

func (s *Exp) SavePatientDietPlan(argument *doctor.ExpPatientDietPlanSave) business.Error {
	for _, plan := range argument.Plans {
		//添加新条目的情况
		if plan.SerialNo == 0 {
			dbInsertEntity := &sqldb.ExpPatientDietPlanAddItem{}
			dbInsertEntity.CopyFrom(&plan)
			_, insertErr := s.sqlDatabase.Insert(dbInsertEntity)
			if insertErr != nil {
				return business.NewError(errors.InternalError, insertErr)
			}
		}
		//修改的情况
		dbEntity := &sqldb.ExpPatientDietPlan{}
		dbEntity.CopyFromEdit(&plan)
		_, err := s.sqlDatabase.UpdateSelectiveByPrimaryKey(dbEntity)
		if err != nil {
			return business.NewError(errors.InternalError, err)
		}
	}

	return nil
}

func (s *Exp) SaveAndDelayPatientDietPlan(argument *doctor.ExpPatientDietPlanSave) business.Error {
	dbEntity := &sqldb.ExpPatientDietPlan{}
	for _, plan := range argument.Plans {
		dbEntity.CopyFromEditAndDelay(&plan)
		_, err := s.sqlDatabase.UpdateSelectiveByPrimaryKey(dbEntity)
		if err != nil {
			return business.NewError(errors.InternalError, err)
		}
	}

	return nil
}

func (s *Exp) StartDietPlan(patientID uint64) business.Error {
	plans := make([]*doctor.ExpDietPlanRepository, 0)
	dbEntity := &sqldb.ExpDietPlanRepository{}
	dbFilter := &sqldb.ExpDietPlanRepositoryNilFilter{}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	err := s.sqlDatabase.SelectList(dbEntity, func() {
		plan := &doctor.ExpDietPlanRepository{}
		dbEntity.CopyTo(plan)
		plans = append(plans, plan)
	}, nil, sqlFilter)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}

	t := time.Now()
	today := time.Date(t.Year(), t.Month(), t.Day(), 0, 0, 0, 0, time.Local)
	nextWeek := t.AddDate(0, 0, 7)
	nextWeekDay := time.Date(nextWeek.Year(), nextWeek.Month(), nextWeek.Day(), 0, 0, 0, 0, time.Local)
	dbPlanEntity := &sqldb.ExpPatientDietPlan{
		PatientID:      patientID,
		StartDate:      &today,
		EndDate:        &nextWeekDay,
		Level:          "中等",
		ChangeStatus:   1,
		CreateDateTime: &t,
	}
	for _, plan := range plans {
		dbPlanEntity.PicUrl = plan.PicUrl
		dbPlanEntity.DietPlan = plan.Plan
		dbPlanEntity.PicUrl = plan.PicUrl
		_, err := s.sqlDatabase.InsertSelective(dbPlanEntity)
		if err != nil {
			return business.NewError(errors.InternalError, err)
		}
	}

	return nil
}

//保存健康饮食障碍并返回
func (s *Exp) SaveBarriers(patientIndex *doctor.ManagedPatientIndex, barrierResult manage.BarrierResult) business.Error {
	if patientIndex == nil {
		return business.NewError(errors.InternalError, fmt.Errorf("参数（patientIndex）为空"))
	}
	barriers := barrierResult.Barriers

	sqlAccess, err := s.sqlDatabase.NewAccess(true)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}
	defer sqlAccess.Close()

	now := time.Now()
	dbEntity := &sqldb.ExpPatientBarriersRecord{
		PatientID:      patientIndex.PatientID,
		CreateDateTime: &now,
		Status:         0,
	}

	s.abandonPatientBarrier(sqlAccess, patientIndex.PatientID)

	abnormalCount := 0
	for _, barrier := range barriers {
		dbEntity.CopyFromManage(&barrier)
		_, err := sqlAccess.InsertSelective(dbEntity)
		if err != nil {
			return business.NewError(errors.InternalError, err)
		}

		abnormalCount++
	}

	// update方法存在问题，小概率出现bug，解决方法是将该方法合并到提交量表信息func中
	sb := &strings.Builder{}
	sb.WriteString("共计")
	sb.WriteString(strconv.Itoa(abnormalCount))
	sb.WriteString("条不良饮食习惯")

	contentData, err := json.Marshal(barriers)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}

	dbUpdateEntity := &sqldb.AssessmentRecord{}
	sqlUpdateEntity := s.sqlDatabase.NewEntity()
	sqlUpdateEntity.Parse(dbUpdateEntity)
	sqlBuilder := s.sqlDatabase.NewBuilder()
	sqlBuilder.Update(sqlUpdateEntity.Name()).Set("Summary", "?").Set("Others", "?")
	sqlBuilder.Append("WHERE PatientID = ? and AssessmentSheetCode = 2 and AssessmentSheetVersion = ? order by SerialNo desc LIMIT 1")
	query := sqlBuilder.Query()

	sqlAccess.QueryRow(query, sb.String(), string(contentData), patientIndex.PatientID, barrierResult.AssessmentSheetVersion)

	if abnormalCount > 0 {
		err = sqlAccess.Commit()
		if err != nil {
			return business.NewError(errors.InternalError, err)
		}
	}

	s.WriteNotify(&notify.Message{
		BusinessID: notify.BusinessDoctor,
		NotifyID:   notify.DoctorExpBarrierInfo,
		Actor: notify.Actor{
			ID: patientIndex.PatientID,
		},
		Data: barriers,
		Time: types.Time(time.Now()),
	})

	return nil
}

//将患者原来的障碍状态置1，即取消
func (s *Exp) abandonPatientBarrier(sqlAccess database.SqlAccess, patientID uint64) error {
	dbEntity := &sqldb.ExpPatientBarriersRecordStatusUpdate{
		Status: 1,
	}
	dbFilter := &sqldb.ExpPatientBarriersRecordPatientIDAndStatusFilter{
		PatientID: patientID,
		Status:    0,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)

	_, err := sqlAccess.UpdateSelective(dbEntity, sqlFilter)
	if err != nil {
		return err
	}

	return nil
}

//保存生活计划并返回
func (s *Exp) SaveActionPlans(patientIndex *doctor.ManagedPatientIndex, actionPlans []manage.InputDataActionPlan) business.Error {
	if patientIndex == nil {
		return business.NewError(errors.InternalError, fmt.Errorf("参数（patientIndex）为空"))
	}
	if len(actionPlans) < 1 {
		return nil
	}

	sqlAccess, err := s.sqlDatabase.NewAccess(true)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}
	defer sqlAccess.Close()

	now := time.Now()
	dbEntity := &sqldb.ExpActionPlan{
		PatientID:      patientIndex.PatientID,
		CreateDateTime: &now,
		Status:         0, //0-使用中，1-停用
		EditorType:     0, //引擎规则
	}

	err = s.abandonPatientExpActionPlan(sqlAccess, patientIndex.PatientID)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}

	abnormalCount := 0
	for _, actionPlan := range actionPlans {
		dbEntity.CopyFromManage(&actionPlan)
		sqlAccess.InsertSelective(dbEntity)

		abnormalCount++
	}

	if abnormalCount > 0 {
		err = sqlAccess.Commit()
		if err != nil {
			return business.NewError(errors.InternalError, err)
		}
	}

	s.WriteNotify(&notify.Message{
		BusinessID: notify.BusinessDoctor,
		NotifyID:   notify.DoctorExpActionPlanInfo,
		Actor: notify.Actor{
			ID: patientIndex.PatientID,
		},
		Data: actionPlans,
		Time: types.Time(time.Now()),
	})

	return nil
}

//将患者原来由引擎生成的计划状态置1，即取消
func (s *Exp) abandonPatientExpActionPlan(sqlAccess database.SqlAccess, patientID uint64) error {

	dbEntity := &sqldb.ExpActionPlanStatusUpdate{
		Status: 1,
	}
	dbFilter := &sqldb.ExpActionPlanPatientIDAndStatusFilter{
		PatientID: patientID,
		Status:    0,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)

	_, err := sqlAccess.UpdateSelective(dbEntity, sqlFilter)
	if err != nil {
		return err
	}

	return nil
}

//新增患者的行动计划
func (s *Exp) CreatePatientExpActionPlan(argument *doctor.ExpPatientActionPlanCreate) business.Error {
	sqlAccess, err := s.sqlDatabase.NewAccess(true)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}
	defer sqlAccess.Close()

	dbEntity := &sqldb.ExpActionPlan{}
	dbEntity.CopyFromCreate(argument)

	//更新数据库
	_, err = sqlAccess.Insert(dbEntity)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}
	err = sqlAccess.Commit()
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}

	return nil
}

//修改患者的行动计划
func (s *Exp) EditPatientExpActionPlan(argument *doctor.ExpPatientActionPlanEdit) business.Error {
	sqlAccess, err := s.sqlDatabase.NewAccess(true)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}
	defer sqlAccess.Close()

	dbUpdate := &sqldb.ExpActionPlanEdit{}
	dbUpdate.CopyFromEdit(argument)

	//更新数据库
	_, err = sqlAccess.UpdateSelectiveByPrimaryKey(dbUpdate)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}

	now := time.Now()
	zeroHour := time.Date(now.Year(), now.Month(), now.Day(), 0, 0, 0, 0, time.UTC)
	dbRecordUpdate := &sqldb.ExpActionPlanRecordEdit{}
	dbRecordUpdate.CopyFromEdit(argument)
	dbFilter := &sqldb.ExpActionPlanRecordEditFilter{
		ActionPlanID:   argument.SerialNo,
		CreateDateTime: &zeroHour,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)

	_, err = sqlAccess.UpdateSelective(dbRecordUpdate, sqlFilter)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}

	err = sqlAccess.Commit()
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}

	go func(sno uint64) {
		s.EditActionPlan(sno)
	}(argument.SerialNo)

	return nil
}

//将糖尿病患者纳入到饮食实验中,引擎
func (s *Exp) RegisterPatientExp(patientID uint64, regFlag bool) error {
	dbFilter := &sqldb.ExpPatientIDFilter{
		PatientID: patientID,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	dbEntity := &sqldb.ExpPatient{}
	err := s.sqlDatabase.SelectOne(dbEntity, sqlFilter)
	if err != nil {
		return err
	}

	dataEntity := &doctor.ExpPatientEx{}
	dbEntity.CopyToEx(dataEntity)

	argument := &manage.ExpPatientInput{}
	argument.PatientID = fmt.Sprint(dataEntity.PatientID)
	argument.ManageType = dataEntity.Type
	argument.Type = enum.ExpStatuses.InExp().Key
	providerType := config.ManagementProviderTypeDm
	uri := s.cfg.Management.Providers.Dm.Api.Uri.RegExp

	result, err := s.client.ManageBusinessPost("饮食实验注册", dataEntity.PatientID, providerType, uri, argument)
	if err != nil {
		return err
	}
	if result.Code != 0 {
		//第一次注册时，如果返回的code==4，即相关病人信息或疾病信息还未注册好，需要重复注册实验，可以用缓存进行优化处理
		if regFlag && result.Code == 4 {
			go func(userId uint64) {
				s.RegisterPatientExp(userId, regFlag)
			}(patientID)
		} else {
			return fmt.Errorf("%d-%s", result.Code, result.Message)
		}
	}
	return nil
}

//修改糖尿病患者饮食实验的状态,引擎，包括管理分组与管理状态
func (s *Exp) ChangePatientExpStatus(patientID uint64) error {
	dbFilter := &sqldb.ExpPatientIDFilter{
		PatientID: patientID,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	dbEntity := &sqldb.ExpPatient{}
	err := s.sqlDatabase.SelectOne(dbEntity, sqlFilter)
	if err != nil {
		return err
	}

	dataEntity := &doctor.ExpPatientTypeAndStatus{}
	dbEntity.CopyToTypeAndStatus(dataEntity)

	argument := &manage.ExpPatientInput{}
	argument.PatientID = fmt.Sprint(dataEntity.PatientID)
	argument.ManageType = dataEntity.Type
	argument.Type = dataEntity.Status
	providerType := config.ManagementProviderTypeDm
	uri := s.cfg.Management.Providers.Dm.Api.Uri.ChangeExpStatus

	result, err := s.client.ManageBusinessPost("修改患者饮食实验状态", dataEntity.PatientID, providerType, uri, argument)
	if err != nil {
		return err
	}
	if result.Code != 0 {
		return fmt.Errorf("%d-%s", result.Code, result.Message)
	}
	return nil
}

//修改生活计划，包括计划内容，计划状态，计划优先级
func (s *Exp) EditActionPlan(sno uint64) error {
	dbFilter := &sqldb.ExpActionPlanFilter{
		SerialNo: sno,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	dbEntity := &sqldb.ExpActionPlan{}
	err := s.sqlDatabase.SelectOne(dbEntity, sqlFilter)
	if err != nil {
		return err
	}
	if dbEntity.SeqActionPlan == 0 {
		return nil
	}

	dataEntity := &doctor.ExpActionPlan{}
	dbEntity.CopyTo(dataEntity)

	argument := &manage.ExpActionPlanEditInput{}
	argument.PatientID = fmt.Sprint(dataEntity.PatientID)
	argument.ActionPlanEditInfo.SeqActionPlan = dataEntity.SeqActionPlan
	argument.ActionPlanEditInfo.ActionPlan = dataEntity.ActionPlan
	argument.ActionPlanEditInfo.EditType = dataEntity.EditorType
	argument.ActionPlanEditInfo.Status = dataEntity.Status
	argument.ActionPlanEditInfo.Level = dataEntity.Level
	providerType := config.ManagementProviderTypeDm
	uri := s.cfg.Management.Providers.Dm.Api.Uri.EditActionPlan

	result, err := s.client.ManageBusinessPost("修改患者生活计划", dataEntity.PatientID, providerType, uri, argument)
	if err != nil {
		return err
	}
	if result.Code != 0 {
		return fmt.Errorf("%d-%s", result.Code, result.Message)
	}
	return nil
}

//提交患者量表结果至引擎,flag标明患者是否需要生活计划
func (s *Exp) CommitPatientExpScaleRecordToEng(sno uint64, flag bool) error {
	dbFilter := &sqldb.ExpScaleRecordFilter{
		SerialNo: sno,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	dbEntity := &sqldb.ExpScaleRecord{}
	err := s.sqlDatabase.SelectOne(dbEntity, sqlFilter)
	if err != nil {
		return err
	}

	dataEntity := &doctor.ExpScaleRecord{}
	dbEntity.CopyTo(dataEntity)

	questions := &[]doctor.Question{}
	err = json.Unmarshal([]byte(dbEntity.Result), questions)
	if err != nil {
		return err
	}

	argument := &manage.ExpScaleRecordInput{}
	argument.PatientID = fmt.Sprint(dataEntity.PatientID)
	argument.ExpScaleRecord.SerialNo = fmt.Sprint(dataEntity.SerialNo)
	argument.ExpScaleRecord.AssessmentSheetVersion = dataEntity.ScaleID
	barrierInfos := make([]*manage.BarrierInfo, 0)
	for _, question := range *questions {
		barrierInfo := &manage.BarrierInfo{}
		if question.Type == "singleCheck" {
			barrierInfo.CheckedType = 0
		} else {
			barrierInfo.CheckedType = 1
		}
		barrierInfo.Degree = question.Degree
		barrierInfo.Barriers = question.Barriers
		barrierInfo.Checked = question.Checked
		barrierInfos = append(barrierInfos, barrierInfo)
	}
	argument.ExpScaleRecord.BarrierInfo = barrierInfos
	argument.Flag = flag
	providerType := config.ManagementProviderTypeDm
	uri := s.cfg.Management.Providers.Dm.Api.Uri.CommitPatientBarrierInfo

	result, err := s.client.ManageBusinessPost("提交患者量表信息", dataEntity.PatientID, providerType, uri, argument)
	if err != nil {
		return err
	}
	if result.Code != 0 {
		return fmt.Errorf("%d-%s", result.Code, result.Message)
	}
	return nil
}

func (s *Exp) GetTaskTree() ([]*doctor.ExpTaskRepositoryTree, business.Error) {
	results := doctor.TreeCollection{}

	dbEntity := &sqldb.ExpTaskRepository{}
	dbFilter := &sqldb.ExpTaskRepositoryStatusFilter{
		Status: 0,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)

	serialNoToIndexMap := make(map[uint64]int)
	i := 0

	err := s.sqlDatabase.SelectList(dbEntity, func() {
		result := &doctor.ExpTaskRepositoryTree{}
		dbEntity.CopyToTree(result)
		if dbEntity.ParentTaskID == 0 {
			results = append(results, result)
			serialNoToIndexMap[dbEntity.SerialNo] = i
			i++
		} else {
			parentTaskIndex := serialNoToIndexMap[dbEntity.ParentTaskID]
			if results[parentTaskIndex].Children == nil {
				results[parentTaskIndex].Children = doctor.TreeCollection{}
			}
			results[parentTaskIndex].Children = append(results[parentTaskIndex].Children, result)
		}
	}, nil, sqlFilter)
	sort.Sort(results)
	for _, tree := range results {
		sort.Sort(tree.Children)
	}

	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}

	return results, nil
}

func (s *Exp) AddTask(argument *doctor.ExpTaskRepositoryAddInput) (uint64, business.Error) {
	sqlAccess, err := s.sqlDatabase.NewAccess(true)
	if err != nil {
		return 0, business.NewError(errors.InternalError, err)
	}
	defer sqlAccess.Close()

	dbEntity := &sqldb.ExpTaskRepository{}
	dbEntity.CopyFromAddInput(argument)

	sno, err := sqlAccess.InsertSelective(dbEntity)
	if err != nil {
		return 0, business.NewError(errors.InternalError, err)
	}

	if argument.TaskName == enum.TaskNames.Scale().Value {
		dbScaleEntity := &sqldb.ExpSmallScale{
			TaskID:  sno,
			Content: "[]",
		}
		_, err = sqlAccess.Insert(dbScaleEntity)
		if err != nil {
			return 0, business.NewError(errors.InternalError, err)
		}
	}

	err = sqlAccess.Commit()
	if err != nil {
		return 0, business.NewError(errors.InternalError, err)
	}

	return sno, nil
}

func (s *Exp) EditTask(argument *doctor.ExpTaskRepositoryEditInput) (uint64, business.Error) {
	dbEntity := &sqldb.ExpTaskRepositoryEdit{}
	dbEntity.CopyFromEditInput(argument)

	sno, err := s.sqlDatabase.UpdateByPrimaryKey(dbEntity)
	if err != nil {
		return 0, business.NewError(errors.InternalError, err)
	}

	return sno, nil
}

func (s *Exp) DeleteTask(argument *doctor.ExpTaskRepositoryDeleteInput) (uint64, business.Error) {
	dbEntity := &sqldb.ExpTaskRepository{}
	dbEntity.CopyFromDeleteInput(argument)

	sno, err := s.sqlDatabase.UpdateSelectiveByPrimaryKey(dbEntity)
	if err != nil {
		return 0, business.NewError(errors.InternalError, err)
	}

	return sno, nil
}

func (s *Exp) GetKnowledgeByType(argument *doctor.ExpKnowledgeTypeInput) ([]*doctor.ExpKnowledge, business.Error) {
	results := make([]*doctor.ExpKnowledge, 0)

	dbEntity := &sqldb.ExpKnowledge{}
	dbFilter := &sqldb.ExpKnowledgeTypeFilter{
		Type:   argument.Type,
		Status: argument.Status,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)

	err := s.sqlDatabase.SelectList(dbEntity, func() {
		result := &doctor.ExpKnowledge{}
		dbEntity.CopyTo(result)
		results = append(results, result)
	}, nil, sqlFilter)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}

	return results, nil
}

func (s *Exp) EditKnowledge(argument *doctor.ExpKnowledgeEditInput) business.Error {
	dbEntity := &sqldb.ExpKnowledgeEdit{}
	dbEntity.CopyFrom(argument)

	sqlAccess, err := s.sqlDatabase.NewAccess(true)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}
	defer sqlAccess.Close()

	_, err = sqlAccess.UpdateByPrimaryKey(dbEntity)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}

	err = sqlAccess.Commit()
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}

	return nil
}

func (s *Exp) AddKnowledge(argument *doctor.ExpKnowledgeAddInput) (uint64, business.Error) {
	dbEntity := &sqldb.ExpKnowledge{}
	dbEntity.CopyFromAddInput(argument)

	sno, err := s.sqlDatabase.Insert(dbEntity)
	if err != nil {
		return 0, business.NewError(errors.InternalError, err)
	}

	return sno, nil
}

func (s *Exp) DeleteKnowledge(argument *doctor.ExpKnowledgeDeleteInput) business.Error {
	dbEntity := &sqldb.ExpKnowledgeDeleteInput{}
	dbEntity.CopyFromDeleteInput(argument)

	_, err := s.sqlDatabase.UpdateSelectiveByPrimaryKey(dbEntity)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}

	return nil
}

func (s *Exp) GetQuestionByKnowledgeID(argument *doctor.ExpKnowledgeIDInput) ([]*doctor.ExpQuestionToKnowledge, business.Error) {
	results := make([]*doctor.ExpQuestionToKnowledge, 0)

	dbEntity := &sqldb.ExpQuestionToKnowledge{}
	dbFilter := &sqldb.ExpQuestionKnowledgeIDFilter{
		KnowledgeID: argument.KnowledgeID,
		Status:      argument.Status,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)

	err := s.sqlDatabase.SelectList(dbEntity, func() {
		result := &doctor.ExpQuestionToKnowledge{}
		dbEntity.CopyTo(result)
		results = append(results, result)
	}, nil, sqlFilter)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}

	return results, nil
}

func (s *Exp) AddQuestion(argument *doctor.ExpQuestionToKnowledgeAddInput) (uint64, business.Error) {
	dbEntity := &sqldb.ExpQuestionToKnowledge{}
	dbEntity.CopyFromAddInput(argument)

	sno, err := s.sqlDatabase.Insert(dbEntity)
	if err != nil {
		return 0, business.NewError(errors.InternalError, err)
	}

	return sno, nil
}

func (s *Exp) DeleteQuestion(argument *doctor.ExpQuestionToKnowledgeDeleteInput) business.Error {
	dbEntity := &sqldb.ExpQuestionToKnowledgeDelete{}
	dbEntity.CopyFromDeleteInput(argument)

	_, err := s.sqlDatabase.UpdateSelectiveByPrimaryKey(dbEntity)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}

	return nil
}

func (s *Exp) EditQuestion(argument *doctor.ExpQuestionToKnowledgeEditInput) business.Error {
	dbEntity := &sqldb.ExpQuestionToKnowledgeEdit{}
	dbEntity.CopyFromEditInput(argument)

	_, err := s.sqlDatabase.UpdateSelectiveByPrimaryKey(dbEntity)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}

	return nil
}

func (s *Exp) CreateFeatureToTask(argument *doctor.ExpPatientFeatureToTask) business.Error {
	dbEntity := &sqldb.ExpPatientFeatureToTask{}
	dbEntity.CopyFrom(argument)

	_, err := s.sqlDatabase.InsertSelective(dbEntity)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}

	return nil
}

func (s *Exp) EditFeatureToTask(argument *doctor.ExpPatientFeatureToTask) business.Error {
	return nil
}

func (s *Exp) AddCamp(argument *doctor.CampListAddInput) (uint64,business.Error){
	dbEntity := &sqldb.CampList{}
	dbEntity.CopyFromAddInput(argument)
	now := time.Now()
	dbEntity.IsValid = 1
	dbEntity.CreateDateTime = &now
	dbEntity.UpdateDateTime = &now

	sno, err := s.sqlDatabase.Insert(dbEntity)
	if err != nil {
		return 0, business.NewError(errors.InternalError, err)
	}

	return sno, nil
}

func (s *Exp) ListCampPage(index uint64, size uint64) (*model.PageResult, business.Error) {
	pageResult := &model.PageResult{}
	results := make([]*doctor.CampListInfo, 0)
	dbEntity := &sqldb.CampList{}
	dbFilter := &sqldb.CampListValidFilter{
		IsValid: 1,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)

	err := s.sqlDatabase.SelectPage(dbEntity, func(total, page, size, index uint64) {
		pageResult.Total = total
		pageResult.Count = page
		pageResult.Size = size
		pageResult.Index = index
	}, func() {
		result := &doctor.CampListInfo{}
		dbEntity.CopyToInfo(result)
		results = append(results, result)
	}, size, index, nil, sqlFilter)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}

	pageResult.Data = results
	return pageResult, nil
}

func (s *Exp) ListCampDict() ([]*doctor.CampListDict, business.Error){
	dbEntity := &sqldb.CampListName{}
	dbFilter := &sqldb.CampListValidFilter{
		IsValid: 1,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)

	results := make([]*doctor.CampListDict, 0)
	err := s.sqlDatabase.SelectList(dbEntity, func() {
		result := &doctor.CampListDict{}
		result.Text = dbEntity.Name
		result.Value = dbEntity.Name
		results = append(results, result)
	}, nil, sqlFilter)

	if err != nil {
		if s.sqlDatabase.IsNoRows(err) {
			return make([]*doctor.CampListDict, 0), nil
		}
		return make([]*doctor.CampListDict, 0), business.NewError(errors.InternalError, err)
	}
	return results, nil
}

func (s *Exp) GetCamp(argument *doctor.CampListSerialNo) (*doctor.CampListInfo, business.Error){
	dbEntity := &sqldb.CampList{}
	dbFilter := &sqldb.CampListValidAndPrimaryKeyFilter{
		SerialNo: argument.SerialNo,
		IsValid: 1,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)

	err := s.sqlDatabase.SelectOne(dbEntity, sqlFilter)
	if err != nil {
		if s.sqlDatabase.IsNoRows(err) {
			return nil, nil
		}
		return nil, business.NewError(errors.InternalError, err)
	}

	result := &doctor.CampListInfo{}
	dbEntity.CopyToInfo(result)
	return result, nil
}

func (s *Exp) UpdateCamp(argument *doctor.CampListUpdateInput) (uint64, business.Error) {
	dbFilter := &sqldb.CampListValidAndPrimaryKeyFilter{
		SerialNo: argument.SerialNo,
		IsValid: 1,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)

	now := time.Now()
	dbEntity := &sqldb.CampListUpdate{
		Name: argument.Name,
		Desc: argument.Desc,
		UpdateDateTime: &now,
	}

	sno, err := s.sqlDatabase.Update(dbEntity, sqlFilter)
	if err != nil {
		return 0, business.NewError(errors.InternalError, err)
	}

	return sno, nil
}

func (s *Exp) DeleteCamp(argument *doctor.CampListSerialNo) business.Error{
	dbEntity := &sqldb.CampList{}
	dbFilter := &sqldb.CampListPrimaryKeyFilter{SerialNo: argument.SerialNo}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	_, err := s.sqlDatabase.Delete(dbEntity, sqlFilter)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}
	return nil
}