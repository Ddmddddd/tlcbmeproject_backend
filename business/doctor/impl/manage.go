package impl

import (
	"tlcbme_project/business"
	"tlcbme_project/business/doctor/api"
	"tlcbme_project/data/entity/sqldb"
	"tlcbme_project/data/enum"
	"tlcbme_project/data/model/doctor"
	"tlcbme_project/data/model/manage"
	"tlcbme_project/database"
	"tlcbme_project/database/memory"
	"tlcbme_project/errors"
	"tlcbme_project/server/config"
	"tlcbme_project/server/notify"
	"fmt"
	"github.com/ktpswjz/httpserver/types"
	"strings"
	"time"
)

type Manage struct {
	base
}

func NewManage(cfg *config.Config, log types.Log, sqlDatabase database.SqlDatabase, memoryToken memory.Token, notifyChannels notify.ChannelCollection, client business.Client) api.Manage {
	instance := &Manage{}
	instance.SetLog(log)
	instance.sqlDatabase = sqlDatabase
	instance.memoryToken = memoryToken
	instance.notifyChannels = notifyChannels
	instance.cfg = cfg
	instance.client = client

	return instance
}

func (s *Manage) SearchHtnDetailList(filter *doctor.HtnManageDetailFilter) ([]*doctor.HtnManageDetail, business.Error) {
	sqlFilters := make([]database.SqlFilter, 0)
	if filter != nil {
		dbFilter := &sqldb.HtnManageDetailFilter{}
		dbFilter.CopyFrom(filter)
		sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
		sqlFilters = append(sqlFilters, sqlFilter)
	}
	dbOrder := &sqldb.HtnManageDetailOrder{}
	dbEntity := &sqldb.HtnManageDetail{}

	results := make([]*doctor.HtnManageDetail, 0)
	err := s.sqlDatabase.SelectList(dbEntity, func() {
		result := &doctor.HtnManageDetail{}
		dbEntity.CopyTo(result)
		results = append(results, result)
	}, dbOrder, sqlFilters...)

	if err != nil {
		return results, business.NewError(errors.InternalError, err)
	}

	return results, nil
}

func (s *Manage) SearchHtnLevelTrend(filter *doctor.TrendLevelHtnFilter) (*doctor.TrendLevelHtn, business.Error) {
	sqlAccess, err := s.sqlDatabase.NewAccess(false)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}
	defer sqlAccess.Close()

	now := time.Now()
	date := time.Date(now.Year(), now.Month(), now.Day(), 0, 0, 0, 0, now.Location())
	level := &doctor.TrendLevelHtn{}
	levelStart := date
	var levelEnd *time.Time = nil

	// 等级记录
	sqlFilters := make([]database.SqlFilter, 0)
	if filter != nil {
		dbFilter := &sqldb.HtnManageDetailFilter{}
		dbFilter.PatientID = &filter.PatientID
		if filter.MonthAgo > 0 {
			startDate := date.Add(-time.Hour * 24 * 30 * time.Duration(filter.MonthAgo))
			dbFilter.ManageDateStart = &startDate
		}

		sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
		sqlFilters = append(sqlFilters, sqlFilter)
	}
	dbOrderLevel := &sqldb.HtnManageDetailOrder{}
	dbEntityLevel := &sqldb.HtnManageDetail{}

	level.Levels = make([]*doctor.TrendLevelHtnLevel, 0)
	err = sqlAccess.SelectList(dbEntityLevel, func() {
		result := &doctor.TrendLevelHtnLevel{}
		result.Level = dbEntityLevel.ManageLevel
		result.StartDateTime = types.Time(*dbEntityLevel.ManageLevelStartDateTime)
		level.Levels = append(level.Levels, result)

		if levelStart.After(*dbEntityLevel.ManageLevelStartDateTime) {
			levelStart = *dbEntityLevel.ManageLevelStartDateTime
		}
		if levelEnd == nil {
			levelEnd = dbEntityLevel.ManageLevelStartDateTime
		} else {
			if levelEnd.Before(*dbEntityLevel.ManageLevelStartDateTime) {
				levelEnd = dbEntityLevel.ManageLevelStartDateTime
			}
		}
	}, dbOrderLevel, sqlFilters...)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}

	// 随访记录
	sqlFilters = make([]database.SqlFilter, 0)
	if filter != nil {
		dbFilter := &sqldb.FollowupRecordFilter{}
		dbFilter.PatientID = &filter.PatientID
		if filter.MonthAgo > 0 {
			startDate := date.Add(-time.Hour * 24 * 30 * time.Duration(filter.MonthAgo))
			dbFilter.FollowupDateTimeStart = &startDate
		}

		sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
		sqlFilters = append(sqlFilters, sqlFilter)
	}
	dbOrderFollowup := &sqldb.FollowupRecordTrendOrder{}
	dbEntityFollowup := &sqldb.FollowupRecord{}

	level.Followups = make([]*doctor.TrendLevelHtnFollowup, 0)
	err = sqlAccess.SelectList(dbEntityFollowup, func() {
		result := &doctor.TrendLevelHtnFollowup{}
		result.Status = dbEntityFollowup.Status
		result.FollowupDateTime = types.Time(*dbEntityFollowup.FollowupDateTime)
		level.Followups = append(level.Followups, result)

		if levelStart.After(*dbEntityFollowup.FollowupDateTime) {
			levelStart = *dbEntityFollowup.FollowupDateTime
		}
	}, dbOrderFollowup, sqlFilters...)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}

	start := time.Date(levelStart.Year(), levelStart.Month(), levelStart.Day(), 0, 0, 0, 0, levelStart.Location())
	end := date.Add(time.Hour*24 - time.Millisecond)
	if levelEnd != nil {
		end = time.Date(levelEnd.Year(), levelEnd.Month(), levelEnd.Day(), 23, 59, 59, 999, levelEnd.Location())
	}
	if filter != nil {
		if !filter.AutoDateRange {
			if filter.MonthAgo > 0 {
				startDate := date.Add(-time.Hour * 24 * 30 * time.Duration(filter.MonthAgo))
				start = time.Date(startDate.Year(), startDate.Month(), startDate.Day(), 0, 0, 0, 0, startDate.Location())
			}
			end = date.Add(time.Hour*24 - time.Millisecond)
		}
	}

	level.DateStart = types.Time(start)
	level.DateEnd = types.Time(end)

	return level, nil
}
func (s *Manage) SearchDmLevelTrend(filter *doctor.TrendLevelDmFilter) (*doctor.TrendLevelDm, business.Error) {
	sqlAccess, err := s.sqlDatabase.NewAccess(false)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}
	defer sqlAccess.Close()

	now := time.Now()
	date := time.Date(now.Year(), now.Month(), now.Day(), 0, 0, 0, 0, now.Location())
	level := &doctor.TrendLevelDm{}
	levelStart := date
	var levelEnd *time.Time = nil

	// 等级记录
	sqlFilters := make([]database.SqlFilter, 0)
	if filter != nil {
		dbFilter := &sqldb.DmManageDetailFilter{}
		dbFilter.PatientID = &filter.PatientID
		if filter.MonthAgo > 0 {
			startDate := date.Add(-time.Hour * 24 * 30 * time.Duration(filter.MonthAgo))
			dbFilter.ManageDateStart = &startDate
		}

		sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
		sqlFilters = append(sqlFilters, sqlFilter)
	}
	dbOrderLevel := &sqldb.DmManageDetailOrder{}
	dbEntityLevel := &sqldb.DmManageDetail{}

	level.Levels = make([]*doctor.TrendLevelDmLevel, 0)
	err = sqlAccess.SelectList(dbEntityLevel, func() {
		result := &doctor.TrendLevelDmLevel{}
		result.Level = dbEntityLevel.ManageLevel
		result.StartDateTime = types.Time(*dbEntityLevel.ManageLevelStartDateTime)
		level.Levels = append(level.Levels, result)

		if levelStart.After(*dbEntityLevel.ManageLevelStartDateTime) {
			levelStart = *dbEntityLevel.ManageLevelStartDateTime
		}
		if levelEnd == nil {
			levelEnd = dbEntityLevel.ManageLevelStartDateTime
		} else {
			if levelEnd.Before(*dbEntityLevel.ManageLevelStartDateTime) {
				levelEnd = dbEntityLevel.ManageLevelStartDateTime
			}
		}
	}, dbOrderLevel, sqlFilters...)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}

	// 随访记录
	sqlFilters = make([]database.SqlFilter, 0)
	if filter != nil {
		dbFilter := &sqldb.FollowupRecordFilter{}
		dbFilter.PatientID = &filter.PatientID
		if filter.MonthAgo > 0 {
			startDate := date.Add(-time.Hour * 24 * 30 * time.Duration(filter.MonthAgo))
			dbFilter.FollowupDateTimeStart = &startDate
		}

		sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
		sqlFilters = append(sqlFilters, sqlFilter)
	}
	dbOrderFollowup := &sqldb.FollowupRecordTrendOrder{}
	dbEntityFollowup := &sqldb.FollowupRecord{}

	level.Followups = make([]*doctor.TrendLevelDmFollowup, 0)
	err = sqlAccess.SelectList(dbEntityFollowup, func() {
		result := &doctor.TrendLevelDmFollowup{}
		result.Status = dbEntityFollowup.Status
		result.FollowupDateTime = types.Time(*dbEntityFollowup.FollowupDateTime)
		level.Followups = append(level.Followups, result)

		if levelStart.After(*dbEntityFollowup.FollowupDateTime) {
			levelStart = *dbEntityFollowup.FollowupDateTime
		}
	}, dbOrderFollowup, sqlFilters...)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}

	start := time.Date(levelStart.Year(), levelStart.Month(), levelStart.Day(), 0, 0, 0, 0, levelStart.Location())
	end := date.Add(time.Hour*24 - time.Millisecond)
	if levelEnd != nil {
		end = time.Date(levelEnd.Year(), levelEnd.Month(), levelEnd.Day(), 23, 59, 59, 999, levelEnd.Location())
	}
	if filter != nil {
		if !filter.AutoDateRange {
			if filter.MonthAgo > 0 {
				startDate := date.Add(-time.Hour * 24 * 30 * time.Duration(filter.MonthAgo))
				start = time.Date(startDate.Year(), startDate.Month(), startDate.Day(), 0, 0, 0, 0, startDate.Location())
			}
			end = date.Add(time.Hour*24 - time.Millisecond)
		}
	}

	level.DateStart = types.Time(start)
	level.DateEnd = types.Time(end)

	return level, nil
}

func (s *Manage) SearchManageStatus(filter *doctor.ManagedPatientIndexFilterBase) (*doctor.ManagedPatientIndexStatus, business.Error) {
	sqlAccess, err := s.sqlDatabase.NewAccess(false)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}
	defer sqlAccess.Close()

	sqlFilters := make([]database.SqlFilter, 0)
	if filter != nil {
		dbFilter := &sqldb.ManagedPatientIndexFilterBase{}
		dbFilter.CopyFrom(filter)
		sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
		sqlFilters = append(sqlFilters, sqlFilter)
	}

	dbEntity := &sqldb.ManagedPatientIndexStatus{}
	err = sqlAccess.SelectOne(dbEntity, sqlFilters...)
	if err != nil {
		if s.sqlDatabase.IsNoRows(err) {
			return nil, business.NewError(errors.NotExist, err)
		} else {
			return nil, business.NewError(errors.InternalError, err)
		}
	}

	modelEntity := &doctor.ManagedPatientIndexStatus{}
	dbEntity.CopyTo(modelEntity)

	return modelEntity, nil
}

func (s *Manage) TerminateManage(argument *doctor.ManagedPatientIndexTerminate, userName string, userID uint64) (uint64, business.Error) {
	if argument == nil {
		return 0, business.NewError(errors.InputError, fmt.Errorf("argument is nil"))
	}

	index, err := s.getPatientMaster(argument.PatientID)
	if err != nil {
		if s.sqlDatabase.IsNoRows(err) {
			return 0, business.NewError(errors.NotExist, err)
		} else {
			return 0, business.NewError(errors.InternalError, err)
		}
	}
	if index.ManageStatus != enum.ManageStatuses.InManagement().Key {
		return 0, business.NewError(errors.NotSupport, fmt.Errorf("患者(ID=%d)%s", index.PatientID, enum.ManageStatuses.Value(index.ManageStatus)))
	}

	sqlAccess, err := s.sqlDatabase.NewAccess(false)
	if err != nil {
		return 0, business.NewError(errors.InternalError, err)
	}
	defer sqlAccess.Close()

	now := time.Now()
	dbEntity := &sqldb.ManagedPatientIndexTerminate{
		SerialNo:          index.SerialNo,
		ManageStatus:      enum.ManageStatuses.TerminatedManagement().Key,
		ManageEndDateTime: &now,
		ManageEndReason:   &argument.ManageEndReason,
	}
	_, err = sqlAccess.UpdateByPrimaryKey(dbEntity)
	if err != nil {
		return 0, business.NewError(errors.InternalError, err)
	}

	_, err = s.ignoreAlert(argument.PatientID, userID, userName)
	if err != nil {
		return 0, business.NewError(errors.InternalError, fmt.Errorf("自动忽略预警失败：%s", err))
	}
	_, err = s.ignorePlan(argument.PatientID)
	if err != nil {
		return 0, business.NewError(errors.InternalError, fmt.Errorf("自动忽略随访失败：%s", err))
	}

	providerType := config.ManagementProviderTypeUnknown
	uri := ""
	info := &manage.EnableManageInput{
		PatientID: fmt.Sprint(argument.PatientID),
	}
	hbp := true
	if strings.Contains(*index.ManageClass, "高血压") {
		hbp = false
		providerType = config.ManagementProviderTypeHbp
		uri = s.cfg.Management.Providers.Hbp.Api.Uri.ManagementProviderApiUri.StopService
		businessName := "终止患者管理"
		result, err := s.client.ManageBusinessPost(businessName, argument.PatientID, providerType, uri, info)
		if err != nil {
			return 0, business.NewError(errors.InternalError, err)
		}
		if result.Code != 0 {
			return 0, business.NewError(errors.InternalError, fmt.Errorf("%d-%s", result.Code, result.Message))
		}
		dbEntityHtn := &sqldb.HtnManageDetail{
			PatientID:                index.PatientID,
			ManageLevel:              9,
			ManageLevelStartDateTime: &now,
		}
		_, err = sqlAccess.InsertSelective(dbEntityHtn)
		if err != nil {
			return 0, business.NewError(errors.InternalError, err)
		}
	}
	if strings.Contains(*index.ManageClass, "糖尿病") {
		dbEntityHtn := &sqldb.DmManageDetail{
			PatientID:                index.PatientID,
			ManageLevel:              9,
			ManageLevelStartDateTime: &now,
		}
		_, err = sqlAccess.InsertSelective(dbEntityHtn)
		if err != nil {
			return 0, business.NewError(errors.InternalError, err)
		}
		if hbp {
			providerType = config.ManagementProviderTypeDm
			uri = s.cfg.Management.Providers.Dm.Api.Uri.ManagementProviderApiUri.StopService
			businessName := "终止患者管理"
			result, err := s.client.ManageBusinessPost(businessName, argument.PatientID, providerType, uri, info)
			if err != nil {
				return 0, business.NewError(errors.InternalError, err)
			}
			if result.Code != 0 {
				return 0, business.NewError(errors.InternalError, fmt.Errorf("%d-%s", result.Code, result.Message))
			}
		}
	}

	err = sqlAccess.Commit()
	if err != nil {
		return 0, business.NewError(errors.InternalError, err)
	}

	s.WriteNotify(&notify.Message{
		BusinessID: notify.BusinessDoctor,
		NotifyID:   notify.DoctorNewPatientAlert,
		Time:       types.Time(time.Now()),
		Data:       index.PatientID,
	})
	s.WriteNotify(&notify.Message{
		BusinessID: notify.BusinessDoctor,
		NotifyID:   notify.DoctorAuditPatient,
		Time:       types.Time(time.Now()),
		Data:       index.PatientID,
	})
	s.WriteNotify(&notify.Message{
		BusinessID: notify.BusinessDoctor,
		NotifyID:   notify.DoctorUpdateMangeStatus,
		Time:       types.Time(time.Now()),
		Data:       index.PatientID,
	})
	s.WriteNotify(&notify.Message{
		BusinessID: notify.BusinessDoctor,
		NotifyID:   notify.DoctorUpdateFollowup,
		Time:       types.Time(time.Now()),
		Data:       index.PatientID,
	})

	return index.SerialNo, nil
}

func (s *Manage) RecoverManage(argument *doctor.ManagedPatientIndexFilterBase) (uint64, business.Error) {
	if argument == nil {
		return 0, business.NewError(errors.InputError, fmt.Errorf("argument is nil"))
	}
	if argument.PatientID == nil {
		return 0, business.NewError(errors.InputError, fmt.Errorf("argument.PatientID is nil"))
	}

	index, err := s.getPatientMaster(*argument.PatientID)
	if err != nil {
		if s.sqlDatabase.IsNoRows(err) {
			return 0, business.NewError(errors.NotExist, err)
		} else {
			return 0, business.NewError(errors.InternalError, err)
		}
	}
	if index.ManageStatus == enum.ManageStatuses.InManagement().Key {
		return 0, business.NewError(errors.NotSupport, fmt.Errorf("患者(ID=%d)%s", index.PatientID, enum.ManageStatuses.Value(index.ManageStatus)))
	}

	sqlAccess, err := s.sqlDatabase.NewAccess(true)
	if err != nil {
		return 0, business.NewError(errors.InternalError, err)
	}
	defer sqlAccess.Close()

	now := time.Now()
	dbEntity := &sqldb.ManagedPatientIndexRecover{
		SerialNo:            index.SerialNo,
		ManageStatus:        enum.ManageStatuses.InManagement().Key,
		ManageStartDateTime: &now,
	}

	_, err = sqlAccess.UpdateByPrimaryKey(dbEntity)
	if err != nil {
		return 0, business.NewError(errors.InternalError, err)
	}

	providerType := config.ManagementProviderTypeUnknown
	uri := ""
	info := &manage.EnableManageInput{
		PatientID: fmt.Sprint(*argument.PatientID),
	}
	if strings.Contains(*index.ManageClass, "高血压") {
		providerType = config.ManagementProviderTypeHbp
		uri = s.cfg.Management.Providers.Hbp.Api.Uri.ManagementProviderApiUri.StartService
		businessName := "恢复患者管理"
		result, err := s.client.ManageBusinessPost(businessName, *argument.PatientID, providerType, uri, info)
		if err != nil {
			return 0, business.NewError(errors.InternalError, err)
		}
		if result.Code != 0 {
			return 0, business.NewError(errors.InternalError, fmt.Errorf("%d-%s", result.Code, result.Message))
		}

		dbEntityHtn := &sqldb.HtnManageDetail{
			PatientID:                index.PatientID,
			ManageLevel:              2,
			ManageLevelStartDateTime: &now,
		}
		ext := &doctor.ManagedPatientIndexExt{}
		if index.CopyToExt(ext) {
			dbEntityHtn.ManageLevel = ext.Htn.ManageLevel
		}
		_, err = sqlAccess.InsertSelective(dbEntityHtn)
		if err != nil {
			return 0, business.NewError(errors.InternalError, err)
		}
	} else if strings.Contains(*index.ManageClass, "糖尿病") {
		providerType = config.ManagementProviderTypeDm
		uri = s.cfg.Management.Providers.Dm.Api.Uri.ManagementProviderApiUri.StartService
		businessName := "恢复患者管理"
		result, err := s.client.ManageBusinessPost(businessName, *argument.PatientID, providerType, uri, info)
		if err != nil {
			return 0, business.NewError(errors.InternalError, err)
		}
		if result.Code != 0 {
			return 0, business.NewError(errors.InternalError, fmt.Errorf("%d-%s", result.Code, result.Message))
		}

		dbEntityHtn := &sqldb.DmManageDetail{
			PatientID:                index.PatientID,
			ManageLevel:              2,
			ManageLevelStartDateTime: &now,
		}
		ext := &doctor.ManagedPatientIndexExt{}
		if index.CopyToExt(ext) {
			dbEntityHtn.ManageLevel = ext.Dm.ManageLevel
		}
		_, err = sqlAccess.InsertSelective(dbEntityHtn)
		if err != nil {
			return 0, business.NewError(errors.InternalError, err)
		}
	}

	err = sqlAccess.Commit()
	if err != nil {
		return 0, business.NewError(errors.InternalError, err)
	}

	s.WriteNotify(&notify.Message{
		BusinessID: notify.BusinessDoctor,
		NotifyID:   notify.DoctorNewPatientAlert,
		Time:       types.Time(time.Now()),
		Data:       index.PatientID,
	})
	s.WriteNotify(&notify.Message{
		BusinessID: notify.BusinessDoctor,
		NotifyID:   notify.DoctorAuditPatient,
		Time:       types.Time(time.Now()),
		Data:       index.PatientID,
	})
	s.WriteNotify(&notify.Message{
		BusinessID: notify.BusinessDoctor,
		NotifyID:   notify.DoctorUpdateMangeStatus,
		Time:       types.Time(time.Now()),
		Data:       index.PatientID,
	})
	s.WriteNotify(&notify.Message{
		BusinessID: notify.BusinessDoctor,
		NotifyID:   notify.DoctorUpdateFollowup,
		Time:       types.Time(time.Now()),
		Data:       index.PatientID,
	})

	return index.SerialNo, nil
}

func (s *Manage) UpdateManagementPlan(argument *doctor.ManagementPlanAdjust) (uint64, business.Error) {
	if argument == nil {
		return 0, business.NewError(errors.InputError, fmt.Errorf("参数为空"))
	}

	sqlAccess, err := s.sqlDatabase.NewAccess(true)
	if err != nil {
		return 0, business.NewError(errors.InternalError, err)
	}
	defer sqlAccess.Close()

	dbEntity := &sqldb.ManagementPlanAdjust{}
	dbEntity.CopyFrom(argument)
	serialNo := dbEntity.SerialNo

	count, err := sqlAccess.UpdateSelectiveByPrimaryKey(dbEntity)
	if err != nil {
		return 0, business.NewError(errors.InternalError, err)
	}
	if count != 1 {
		return 0, business.NewError(errors.NotExist, fmt.Errorf("记录(%d)不存在", serialNo))
	}

	err = sqlAccess.Commit()
	if err != nil {
		return 0, business.NewError(errors.InternalError, err)
	}

	uploadInfo := &doctor.InputManageInfo{
		PatientID: fmt.Sprint(dbEntity.PatientID),
		Plans:     argument.Content.Advise.Plans,
		Goals:     argument.Content.Advise.Goals,
	}
	providerType := config.ManagementProviderTypeUnknown
	uri := ""
	if argument.PlanType == "高血压" {
		providerType = config.ManagementProviderTypeHbp
		uri = s.cfg.Management.Providers.Hbp.Api.Uri.ManagementProviderApiUri.UpdateSetting
	} else if argument.PlanType == "糖尿病" {
		providerType = config.ManagementProviderTypeDm
		uri = s.cfg.Management.Providers.Dm.Api.Uri.ManagementProviderApiUri.UpdateSetting
	}
	if providerType != config.ManagementProviderTypeUnknown {
		businessName := "更新管理计划"
		go func() {
			s.client.ManageBusinessPost(businessName, argument.PatientID, providerType, uri, uploadInfo)
		}()
	}

	return serialNo, nil
}

func (s *Manage) ignoreAlert(patientID uint64, userID uint64, userName string) (uint64, error) {
	dbEntity := &sqldb.AlertRecordIgnore{}
	dbEntity.Init(userID, userName)
	dbFilter := &sqldb.AlertRecordAutoIgnoreFilter{
		Status:    enum.AlertStatuses.Unprocessed().Key,
		PatientID: patientID,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	count, err := s.sqlDatabase.Update(dbEntity, sqlFilter)
	if err != nil {
		return 0, err
	}

	if count > 0 {
		s.WriteNotify(&notify.Message{
			BusinessID: notify.BusinessDoctor,
			NotifyID:   notify.DoctorNewPatientAlert,
			Time:       types.Time(time.Now()),
		})
	}

	return count, nil
}

func (s *Manage) ignorePlan(patientID uint64) (uint64, error) {
	memo := "终止管理，系统自动忽略"
	now := time.Now()
	dbEntity := &sqldb.FollowupPlanAutoIgnore{
		Status:     2,
		StatusMemo: &memo,
		UpdateTime: &now,
	}

	dbFilter := &sqldb.FollowupPlanAutoFilter{
		Status:    0,
		PatientID: &patientID,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)

	count, err := s.sqlDatabase.Update(dbEntity, sqlFilter)
	if err != nil {
		return 0, err
	}

	if count < 1 {
		return 0, nil
	}

	s.WriteNotify(&notify.Message{
		BusinessID: notify.BusinessDoctor,
		NotifyID:   notify.DoctorUpdateFollowup,
		Time:       types.Time(time.Now()),
	})

	return count, nil
}
