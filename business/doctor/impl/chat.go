package impl

import (
	"tlcbme_project/business"
	"tlcbme_project/business/doctor/api"
	"tlcbme_project/data/entity/sqldb"
	"tlcbme_project/data/model/doctor"
	"tlcbme_project/database"
	"tlcbme_project/database/memory"
	"tlcbme_project/errors"
	"tlcbme_project/server/config"
	"tlcbme_project/server/notify"
	"github.com/ktpswjz/httpserver/types"
	"time"
)

type Chat struct {
	base
}

func NewChat(cfg *config.Config, log types.Log, sqlDatabase database.SqlDatabase, memoryToken memory.Token, notifyChannels notify.ChannelCollection, client business.Client) api.Chat {
	instance := &Chat{}
	instance.SetLog(log)
	instance.cfg = cfg
	instance.sqlDatabase = sqlDatabase
	instance.client = client
	instance.memoryToken = memoryToken
	instance.notifyChannels = notifyChannels

	return instance
}

func (s *Chat) SendChatMessageFromDoctor(argument *doctor.DoctorPatientChatMsg) (uint64, business.Error) {
	dbEntity := &sqldb.DoctorPatientChatMsg{}
	dbEntity.CopyFrom(argument)
	sno, err := s.sqlDatabase.InsertSelective(dbEntity)
	if err != nil {
		return 0, business.NewError(errors.InternalError, err)
	}
	argument.SerialNo = sno

	s.WriteNotify(&notify.Message{
		BusinessID: notify.BusinessPatient,
		NotifyID:   notify.PatientNewChatMessage,
		Time:       types.Time(time.Now()),
		Actor:      notify.Actor{ID: argument.ReceiverID},
		Data:       argument,
	})
	s.WriteNotify(&notify.Message{
		BusinessID: notify.BusinessDoctor,
		NotifyID:   notify.DoctorNewChatMessage,
		Time:       types.Time(time.Now()),
		Data:       argument,
	})

	return sno, nil
}

func (s *Chat) SendChatMessageReadFromDoctor(argument *doctor.DoctorPatientChatMsgRead) business.Error {
	sqlAccess, err := s.sqlDatabase.NewAccess(true)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}
	defer sqlAccess.Close()

	for _, serialNo := range argument.MsgSerialNos {
		dbEntity := &sqldb.DoctorPatientChatMsgRead{}
		dbEntity.SerialNo = serialNo
		dbEntity.MsgFlag = 1
		_, err := sqlAccess.UpdateSelectiveByPrimaryKey(dbEntity)
		if err != nil {
			return business.NewError(errors.InternalError, err)
		}
	}
	err = sqlAccess.Commit()
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}

	s.WriteNotify(&notify.Message{
		BusinessID: notify.BusinessPatient,
		NotifyID:   notify.PatientChatMessageRead,
		Time:       types.Time(time.Now()),
		Actor:      notify.Actor{ID: argument.ReceiverID},
		Data:       argument,
	})
	s.WriteNotify(&notify.Message{
		BusinessID: notify.BusinessDoctor,
		NotifyID:   notify.DoctorCountUnReadMessage,
		Time:       types.Time(time.Now()),
	})

	return nil
}

func (s *Chat) GetChatMessageList(argument *doctor.ChatMsgUserInfo) ([]*doctor.DoctorPatientChatMsg, business.Error) {
	results := make([]*doctor.DoctorPatientChatMsg, 0)

	dbEntity := &sqldb.DoctorPatientChatMsg{}
	dbFilter1 := &sqldb.DoctorPatientChatMsgFilter{
		SenderID:   argument.PatientID,
		ReceiverID: argument.DoctorID,
	}
	dbFilter2 := &sqldb.DoctorPatientChatMsgFilter{
		SenderID:   argument.DoctorID,
		ReceiverID: argument.PatientID,
	}

	sqlFilter1 := s.sqlDatabase.NewFilter(dbFilter1, false, false)
	sqlFilter2 := s.sqlDatabase.NewFilter(dbFilter2, false, true)
	dbOrder := &sqldb.DoctorPatientChatMsgDateTimeOrder{}

	err := s.sqlDatabase.SelectList(dbEntity, func() {
		result := &doctor.DoctorPatientChatMsg{}
		dbEntity.CopyTo(result)
		results = append(results, result)
	}, dbOrder, sqlFilter1, sqlFilter2)
	if err != nil {
		return results, business.NewError(errors.InternalError, err)
	}
	s.WriteNotify(&notify.Message{
		BusinessID: notify.BusinessDoctor,
		NotifyID:   notify.DoctorCountUnReadMessage,
		Time:       types.Time(time.Now()),
	})

	return results, nil
}

func (s *Chat) CountUnReadMsg(filter *doctor.ChatUnReadMsgCountFilter) (uint64, business.Error) {
	count := uint64(0)

	dbEntity := &sqldb.ChatUnReadMsgCount{}
	dbFilter := &sqldb.CountChatUnReadMsgFilter{
		ReceiverID: filter.ReceiverID,
		MsgFlag:    filter.MsgFlag,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)

	err := s.sqlDatabase.SelectDistinct(dbEntity, func() {
		count++

	}, nil, sqlFilter)

	if err != nil {
		return count, business.NewError(errors.InternalError, err)
	}

	return count, nil
}

func (s *Chat) GetChatPatientLatsMsgList(argument *doctor.DoctorInfoBase) ([]*doctor.DoctorPatienLastChatMsg, business.Error) {
	results := make([]*doctor.DoctorPatienLastChatMsg, 0)
	counts := make([]*doctor.DoctorMsgCount, 0)
	sqlAccess, err := s.sqlDatabase.NewAccess(false)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}
	defer sqlAccess.Close()

	dbEntity := &sqldb.DoctorPatientLastChatMsg{}
	_, err = sqlAccess.Exec("set sql_mode='STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION'")
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}
	sqlString := `SELECT msg.SerialNo,msg.MsgContent,msg.PatientID,msg.MsgDateTime,patient.Name,patient.Sex from
	( select PatientID,MsgContent,SerialNo,MsgDateTime from ( 
	(select ReceiverID as patientID,MsgContent,SerialNo,MsgDateTime  from DoctorPatientChatMsg where SenderID = ? )
	union
	(select  SenderID as PatientID,MsgContent,SerialNo,MsgDateTime from DoctorPatientChatMsg where ReceiverID = ? )ORDER BY MsgDateTime DESC) as tmp 
	group by tmp.patientID  ORDER BY tmp.MsgDateTime DESC  ) as msg
	LEFT JOIN PatientUserBaseInfo as patient on msg.patientID = patient.UserID`
	rows, err := sqlAccess.Query(sqlString, argument.DoctorID, argument.DoctorID)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}
	defer rows.Close()

	for rows.Next() {
		err = rows.Scan(&dbEntity.SerialNo, &dbEntity.MsgContent, &dbEntity.PatientID, &dbEntity.MsgDateTime, &dbEntity.PatientName, &dbEntity.Sex)
		if err != nil {
			return nil, business.NewError(errors.InternalError, err)
		}
		result := &doctor.DoctorPatienLastChatMsg{}
		dbEntity.CopyToExt(result)
		results = append(results, result)
	}
	dbEntity2 := &sqldb.DoctorMsgCount{}
	sqlCountString := `select count(*) as count ,patient.PatientID,chat.ReceiverID from DoctorPatientChatMsg as chat left join
	( select patientID,MsgContent,SerialNo,MsgDateTime 
	from ( (select ReceiverID as patientID,MsgContent,SerialNo,MsgDateTime  from DoctorPatientChatMsg where SenderID = ? ) 
	union 
	(select  SenderID as PatientID,MsgContent,SerialNo,MsgDateTime from DoctorPatientChatMsg where ReceiverID = ? )
	ORDER BY MsgDateTime DESC) as tmp group by tmp.patientID  ORDER BY tmp.MsgDateTime DESC  ) as patient 
	on chat.SenderID = patient.patientID where  chat.MsgFlag = 0 group by patient.patientID having chat.ReceiverID = ?  `
	rows, err = sqlAccess.Query(sqlCountString, argument.DoctorID, argument.DoctorID, argument.DoctorID)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}
	for rows.Next() {
		err = rows.Scan(&dbEntity2.Count, &dbEntity2.PatientID, &dbEntity2.ReceiverID)
		if err != nil {
			return nil, business.NewError(errors.InternalError, err)
		}
		count := &doctor.DoctorMsgCount{}
		dbEntity2.CopyToExt(count)
		counts = append(counts, count)
	}
	for _, v1 := range counts {
		for _, v2 := range results {
			if *v1.PatientID == v2.PatientID {
				v2.Count = *v1.Count
			}
		}
	}

	return results, nil
}
