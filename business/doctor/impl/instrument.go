package impl

import (
	"tlcbme_project/business"
	"tlcbme_project/business/doctor/api"
	"tlcbme_project/data/entity/sqldb"
	"tlcbme_project/data/model/doctor"
	"tlcbme_project/data/model/manage"
	"tlcbme_project/database"
	"tlcbme_project/server/config"
	"tlcbme_project/server/notify"
	"fmt"
	"github.com/ktpswjz/httpserver/example/webserver/server/errors"
	"github.com/ktpswjz/httpserver/security/hash"
	"github.com/ktpswjz/httpserver/types"
	"time"
)

type Instrument struct {
	base

	passwordFormat uint64
}

func NewInstrument(cfg *config.Config, log types.Log, sqlDatabase database.SqlDatabase, notifyChannels notify.ChannelCollection, client business.Client, passwordFormat uint64) api.Instrument {
	instance := &Instrument{passwordFormat: passwordFormat}
	instance.SetLog(log)
	instance.cfg = cfg
	instance.sqlDatabase = sqlDatabase
	instance.notifyChannels = notifyChannels
	instance.client = client

	return instance
}

func (s *Instrument) ExistIDNumber(idNumber string) (bool, uint64, business.Error) {
	dbEntity := &sqldb.PatientUserBaseInfo{}
	dbFilter := &sqldb.PatientUserBaseInfoFilter{
		IdentityCardNumber: &idNumber,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	err := s.sqlDatabase.SelectOne(dbEntity, sqlFilter)
	if err != nil {
		if !s.sqlDatabase.IsNoRows(err) {
			return false, 0, business.NewError(errors.InternalError, err)
		} else {
			return false, 0, nil
		}
	}

	return true, dbEntity.UserID, nil
}

func (s *Instrument) CreateUser(argument *doctor.PatientUserInfoCreateForApp) (uint64, business.Error) {
	sqlAccess, err := s.sqlDatabase.NewAccess(true)
	if err != nil {
		return 0, business.NewError(errors.InternalError, err)
	}
	defer sqlAccess.Close()
	now := time.Now()
	phone := string(argument.PhoneNumber)
	dbEntity := &sqldb.PatientUserAuths{
		UserName:       argument.UserName,
		RegistDateTime: &now,
	}
	dbFilter := &sqldb.PatientUserAuthsFilter{
		UserName:    argument.UserName,
		MobilePhone: phone,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, true, false)
	err = sqlAccess.SelectOne(dbEntity, sqlFilter)
	if err != nil {
		if !sqlAccess.IsNoRows(err) {
			return 0, business.NewError(errors.InternalError, err)
		}
	} else {
		if dbEntity.UserName == argument.UserName {
			return 0, business.NewError(errors.InternalError, fmt.Errorf("用户名%s已存在", argument.UserName))
		} else {
			return 0, business.NewError(errors.InternalError, fmt.Errorf("手机号为%s已被注册", phone))
		}
	}

	password := argument.Password
	passwordFormat := s.passwordFormat
	if passwordFormat != 0 {
		pwd, err := hash.Hash(password, passwordFormat)
		if err != nil {
			return 0, business.NewError(errors.InternalError, err)
		}
		password = pwd
	}
	dbEntity.PasswordFormat = passwordFormat
	dbEntity.Password = password

	userId, err := sqlAccess.InsertSelective(dbEntity)
	if err != nil {
		return 0, business.NewError(errors.InternalError, fmt.Errorf("创建账号失败: %s", err.Error()))
	}
	_, err = s.createUserBasicInfo(sqlAccess, argument, userId)
	if err != nil {
		return 0, business.NewError(errors.InternalError, fmt.Errorf("创建基本信息失败: %s", err.Error()))
	}
	if argument.PhoneNumber == "" {
		_, err = s.createUserManagementAppReview(sqlAccess, argument, userId)
		if err != nil {
			return 0, business.NewError(errors.InternalError, fmt.Errorf("创建管理申请审核失败: %s", err.Error()))
		}
		s.WriteNotify(&notify.Message{
			BusinessID: notify.BusinessDoctor,
			NotifyID:   notify.DoctorAuditPatient,
			Time:       types.Time(time.Now()),
		})
	}

	input := &manage.PatientInfoInput{}
	input.PatientID = fmt.Sprint(userId)
	input.Info.Name = argument.Name
	input.Info.Gender = argument.Sex
	input.Info.Birthday = argument.DateOfBirth
	input.Info.IdentityCardNumber = argument.IdentityCardNumber

	providerType := config.ManagementProviderTypeHbp
	uri := s.cfg.Management.Providers.Hbp.Api.Uri.RegPatient

	result, err := s.client.ManageBusinessPost("患者注册", userId, providerType, uri, input)
	if err != nil {
		return userId, business.NewError(errors.InternalError, err)
	}
	if result.Code != 0 {
		return userId, business.NewError(errors.InternalError, fmt.Errorf("%d-%s", result.Code, result.Message))
	}
	err = sqlAccess.Commit()
	if err != nil {
		return 0, business.NewError(errors.InternalError, err)
	}
	return userId, nil
}

func (s *Instrument) GetHospitalCode(deviceCode string) (string, string, business.Error) {
	dbEntity := &sqldb.DeviceDict{}
	dbFilter := &sqldb.DeviceFilter{
		DeviceCode: deviceCode,
		IsValid:    uint64(1),
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	err := s.sqlDatabase.SelectOne(dbEntity, sqlFilter)
	if err != nil {
		return "", "", business.NewError(errors.InternalError, err)
	}

	return dbEntity.DeviceType, dbEntity.OrgCode, nil
}

func (s *Instrument) createUserBasicInfo(sqlAccess database.SqlAccess, argument *doctor.PatientUserInfoCreateForApp, userID uint64) (uint64, error) {
	dbEntity := &sqldb.PatientUserBaseInfo{}
	dbEntity.CopyFromInstrument(argument)
	dbEntity.UserID = userID
	dbFilter := &sqldb.PatientUserBaseInfoFilter{
		IdentityCardNumber: &argument.IdentityCardNumber,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	err := sqlAccess.SelectOne(dbFilter, sqlFilter)
	if err != nil {
		if !sqlAccess.IsNoRows(err) {
			return 0, err
		}
	} else {
		return 0, fmt.Errorf("身份证号%s已被注册", argument.IdentityCardNumber)
	}
	// 新增体重记录
	if argument.Weight > 0 {
		now := time.Now()
		dbEntityWeight := &sqldb.WeightRecord{
			PatientID:       userID,
			Weight:          argument.Weight,
			InputDateTime:   &now,
			MeasureDateTime: &now,
		}
		_, err = sqlAccess.InsertSelective(dbEntityWeight)
		if err != nil {
			return 0, err
		}
	}
	return sqlAccess.InsertSelective(dbEntity)
}

func (s *Instrument) createUserManagementAppReview(sqlAccess database.SqlAccess, argument *doctor.PatientUserInfoCreateForApp, userID uint64) (uint64, error) {
	visitOrgCode := string(argument.HospitalCode)
	healthManagerID := uint64(argument.HealthManagerID)
	applicationFrom := string(argument.SourceFrom)
	diagnosis := string(argument.Diagnosis)
	dbEntity := &sqldb.ManagementApplicationReview{
		Diagnosis:       &diagnosis,
		ApplicationFrom: &applicationFrom,
		VisitOrgCode:    &visitOrgCode,
		Status:          0,
	}
	if healthManagerID > 0 {
		dbEntity.HealthManagerID = &healthManagerID
	}
	dbEntity.PatientID = userID
	dbFilter := &sqldb.ManagementApplicationReviewFilter{
		PatientID: userID,
		Status:    0,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	err := sqlAccess.SelectOne(dbFilter, sqlFilter)
	if err != nil {
		if !sqlAccess.IsNoRows(err) {
			return 0, err
		}
	} else {
		return 0, fmt.Errorf("ID为%s的患者的管理申请审核已存在", userID)
	}

	return sqlAccess.InsertSelective(dbEntity)
}
