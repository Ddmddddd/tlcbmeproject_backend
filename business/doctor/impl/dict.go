package impl

import (
	"crypto"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"github.com/ktpswjz/httpserver/types"
	"io/ioutil"
	"net/http"
	"reflect"
	"strconv"
	"strings"
	"time"
	"tlcbme_project/business"
	"tlcbme_project/business/doctor/api"
	"tlcbme_project/data/entity/sqldb"
	"tlcbme_project/data/enum"
	"tlcbme_project/data/model"
	"tlcbme_project/data/model/doctor"
	"tlcbme_project/data/model/jinscale"
	"tlcbme_project/database"
	"tlcbme_project/database/memory"
	"tlcbme_project/errors"
)

type Dict struct {
	base
}

func NewDict(log types.Log, sqlDatabase database.SqlDatabase, memoryToken memory.Token) api.Dict {
	instance := &Dict{}
	instance.SetLog(log)
	instance.sqlDatabase = sqlDatabase
	instance.memoryToken = memoryToken

	return instance
}

func (s *Dict) CreateOrg(argument *doctor.OrgDictCreate) (uint64, business.Error) {
	if argument == nil {
		return 0, business.NewError(errors.InputError, fmt.Errorf("参数为空"))
	}
	if argument.OrgCode == "" {
		return 0, business.NewError(errors.InputInvalid, fmt.Errorf("机构代码为空"))
	}
	if argument.OrgName == "" {
		return 0, business.NewError(errors.InputInvalid, fmt.Errorf("机构名称为空"))
	}

	dbFilter := &sqldb.OrgDictFilter{
		OrgCode: argument.OrgCode,
		OrgName: argument.OrgName,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, true, false)
	dbEntity := &sqldb.OrgDict{}
	err := s.sqlDatabase.SelectOne(dbEntity, sqlFilter)
	if err == nil {
		return 0, business.NewError(errors.InputInvalid, fmt.Errorf("机构代码机构名称已存在"))
	} else {
		if !s.sqlDatabase.IsNoRows(err) {
			return 0, business.NewError(errors.InternalError, err)
		}
	}

	dbCreate := &sqldb.OrgDict{}
	dbCreate.CopyFromCreate(argument)
	sno, err := s.sqlDatabase.InsertSelective(dbCreate)
	if err != nil {
		return 0, business.NewError(errors.InternalError, err)
	}

	return sno, nil
}

func (s *Dict) ListOrg(argument *doctor.DictKeywordFilter) ([]*doctor.OrgDict, business.Error) {
	results := make([]*doctor.OrgDict, 0)

	dbFilter := &sqldb.OrgDictKeywordFilter{}
	dbFilter.CopyFrom(argument)
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, true, false)
	dbEntity := &sqldb.OrgDict{}
	err := s.sqlDatabase.SelectList(dbEntity, func() {
		result := &doctor.OrgDict{}
		dbEntity.CopyTo(result)
		results = append(results, result)
	}, nil, sqlFilter)

	if err != nil {
		return results, business.NewError(errors.InternalError, err)
	}

	return results, nil
}

func (s *Dict) EditOrg(argument *doctor.OrgDictUpdate) (uint64, business.Error) {
	if argument == nil {
		return 0, business.NewError(errors.InputError, fmt.Errorf("参数为空"))
	}
	if argument.SerialNo < 1 {
		return 0, business.NewError(errors.InputInvalid, fmt.Errorf("序列号'%d'无效", argument.SerialNo))
	}
	if argument.OrgName == "" {
		return 0, business.NewError(errors.InputInvalid, fmt.Errorf("机构名称为空"))
	}

	dbFilter := &sqldb.OrgDictFilter{}
	dbFilter.SerialNo = &argument.SerialNo
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	dbEntity := &sqldb.OrgDict{}
	err := s.sqlDatabase.SelectOne(dbEntity, sqlFilter)
	if err != nil {
		if s.sqlDatabase.IsNoRows(err) {
			return 0, business.NewError(errors.NotExist, fmt.Errorf("机构(序列号=%d)不存在", argument.SerialNo))
		} else {
			return 0, business.NewError(errors.InternalError, err)
		}
	}
	if argument.OrgName == dbEntity.OrgName {
		//return argument.SerialNo, nil
	}

	dbUpdate := &sqldb.OrgDictUpdate{}
	dbUpdate.CopyFrom(argument)
	_, err = s.sqlDatabase.UpdateSelective(dbUpdate, sqlFilter)
	if err != nil {
		return 0, business.NewError(errors.InternalError, err)
	}

	return argument.SerialNo, nil
}

func (s *Dict) DeleteOrg(argument *doctor.DictFilter) (uint64, business.Error) {
	if argument == nil {
		return 0, business.NewError(errors.InputError, fmt.Errorf("参数为空"))
	}
	if argument.SerialNo == nil {
		return 0, business.NewError(errors.InputInvalid, fmt.Errorf("序列号为空"))
	}
	if *argument.SerialNo < 1 {
		return 0, business.NewError(errors.InputInvalid, fmt.Errorf("序列号'%d'无效", argument.SerialNo))
	}

	dbFilter := &sqldb.OrgDictFilter{}
	dbFilter.SerialNo = argument.SerialNo
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	dbEntity := &sqldb.OrgDict{}
	count, err := s.sqlDatabase.Delete(dbEntity, sqlFilter)
	if err != nil {
		return 0, business.NewError(errors.InternalError, err)
	}

	return count, nil
}

func (s *Dict) ListDoctor(filter *doctor.ViewDoctorUserDictFilter, orgCode string) ([]*doctor.ViewDoctorUserDict, business.Error) {
	results := make([]*doctor.ViewDoctorUserDict, 0)

	dbFilter := &sqldb.ViewDoctorUserDictFilter{}
	dbFilter2 := &sqldb.ViewDoctorUserDictFilter{}
	dbFilter.CopyFrom(filter)
	if len(orgCode) > 0 {
		dbFilter.OrgCode = &orgCode
		dbFilter2.OrgCode = &orgCode
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, true)
	userType := enum.DoctorUserTypes.DoctorAndHealthManager().Key
	dbFilter2.UserType = &userType
	sqlFilter2 := s.sqlDatabase.NewFilter(dbFilter2, false, true)
	dbEntity := &sqldb.ViewDoctorUserDict{}
	err := s.sqlDatabase.SelectList(dbEntity, func() {
		result := &doctor.ViewDoctorUserDict{}
		dbEntity.CopyTo(result)
		results = append(results, result)
	}, nil, sqlFilter, sqlFilter2)

	if err != nil {
		return results, business.NewError(errors.InternalError, err)
	}

	return results, nil
}

func (s *Dict) ListPatientFeature(filter *doctor.DictCodeItemFilter) ([]*doctor.DictCodeItem, business.Error) {
	results := make([]*doctor.DictCodeItem, 0)

	dbFilter := &sqldb.PatientFeatureDictCodeItemFilter{}
	dbFilter.CopyFrom(filter)
	dbOrder := &sqldb.PatientFeatureOrder{}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	dbEntity := &sqldb.PatientFeatureDict{}
	err := s.sqlDatabase.SelectList(dbEntity, func() {
		result := &doctor.DictCodeItem{}
		dbEntity.CopyToCodeItem(result)
		results = append(results, result)
	}, dbOrder, sqlFilter)

	if err != nil {
		return results, business.NewError(errors.InternalError, err)
	}

	return results, nil
}

func (s *Dict) ListManageClass(filter *doctor.DictCodeItemFilter) ([]*doctor.DictCodeItem, business.Error) {
	results := make([]*doctor.DictCodeItem, 0)

	dbFilter := &sqldb.ManageClassDictCodeItemFilter{}
	dbFilter.CopyFrom(filter)
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	dbEntity := &sqldb.ManageClassDict{}
	err := s.sqlDatabase.SelectList(dbEntity, func() {
		result := &doctor.DictCodeItem{}
		dbEntity.CopyToCodeItem(result)
		results = append(results, result)
	}, nil, sqlFilter)

	if err != nil {
		return results, business.NewError(errors.InternalError, err)
	}

	return results, nil
}

func (s *Dict) SearchDivisionTree(filter *doctor.DivisionDictTreeFilter) ([]interface{}, business.Error) {
	results := &sqldb.DivisionDictTree{}
	results.Init()

	admin := false
	if filter != nil {
		admin = filter.Admin
	}

	dbEntity := &sqldb.DivisionDict{}
	dbOrder := &sqldb.DivisionDictOrder{}
	err := s.sqlDatabase.SelectList(dbEntity, func() {
		if admin {
			results.AppendAdmin(dbEntity)
		} else {
			results.Append(dbEntity)
		}
	}, dbOrder)
	if err != nil {
		return results.Data(), business.NewError(errors.InternalError, err)
	}

	return results.Data(), nil
}

func (s *Dict) SearchDivisionParentCodes(filter *doctor.DivisionDictDeleteFilter) ([]string, business.Error) {
	results := make([]string, 0)

	sqlAccess, err := s.sqlDatabase.NewAccess(false)
	if err != nil {
		return results, business.NewError(errors.InternalError, err)
	}
	defer sqlAccess.Close()

	itemCode := filter.ItemCode
	for {
		item, err := s.searchDivision(sqlAccess, itemCode)
		if err != nil {
			if s.sqlDatabase.IsNoRows(err) {
				break
			} else {
				return results, business.NewError(errors.InternalError, err)
			}
		}

		if item.ParentDivisionCode == nil {
			break
		}
		if len(*item.ParentDivisionCode) < 1 {
			break
		}

		results = append(results, *item.ParentDivisionCode)
		itemCode = *item.ParentDivisionCode
	}

	return results, nil
}

func (s *Dict) searchDivision(sqlAccess database.SqlAccess, code string) (*sqldb.DivisionDict, error) {
	dbEntity := &sqldb.DivisionDict{}

	dbFilter := &sqldb.DivisionDictFilter{
		ItemCode: code,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, true, false)
	err := sqlAccess.SelectOne(dbEntity, sqlFilter)
	if err != nil {
		return nil, err
	}

	return dbEntity, nil
}

func (s *Dict) CreateDivision(argument *doctor.DivisionDictCreate) business.Error {
	if argument == nil {
		return business.NewError(errors.InputError, fmt.Errorf("参数为空"))
	}
	if len(argument.Items) < 1 {
		return business.NewError(errors.InputInvalid, fmt.Errorf("行政区划为空"))
	}

	dbEntity := &sqldb.DivisionDict{}
	if argument.Parent != nil && len(*argument.Parent) > 0 {
		dbFilter := &sqldb.DivisionDictFilter{
			ItemCode: *argument.Parent,
		}
		sqlFilter := s.sqlDatabase.NewFilter(dbFilter, true, false)
		err := s.sqlDatabase.SelectOne(dbEntity, sqlFilter)
		if err != nil {
			if s.sqlDatabase.IsNoRows(err) {
				return business.NewError(errors.InputInvalid, fmt.Errorf("上级区划%s不存在", *argument.Parent))
			} else {
				return business.NewError(errors.InternalError, err)
			}
		}
	}

	sqlAccess, err := s.sqlDatabase.NewAccess(true)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}
	defer sqlAccess.Close()

	dbEntity.ParentDivisionCode = argument.Parent
	for _, item := range argument.Items {
		dbEntity.ItemCode = item.ItemCode
		dbEntity.ItemName = item.ItemName
		dbEntity.FullName = item.FullName
		_, err = sqlAccess.InsertSelective(dbEntity)
		if err != nil {
			return business.NewError(errors.InternalError, err)
		}
	}

	err = sqlAccess.Commit()
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}

	return nil
}

func (s *Dict) DeleteDivision(argument *doctor.DivisionDictFilter) (uint64, business.Error) {
	if argument == nil {
		return 0, business.NewError(errors.InputError, fmt.Errorf("参数为空"))
	}
	if len(argument.ItemCode) < 1 {
		return 0, business.NewError(errors.InputInvalid, fmt.Errorf("代码为空"))
	}

	dbFilter := &sqldb.DivisionDictFilter{}
	dbFilter.CopyFrom(argument)
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, true, false)
	dbEntity := &sqldb.DivisionDict{}

	count, err := s.sqlDatabase.Delete(dbEntity, sqlFilter)
	if err != nil {
		return 0, business.NewError(errors.InternalError, err)
	}

	return count, nil
}

func (s *Dict) ListDrug() ([]*doctor.DrugDict, business.Error) {
	results := make([]*doctor.DrugDict, 0)

	dbEntity := &sqldb.DrugDict{}
	sqlFilter := s.sqlDatabase.NewFilter(nil, true, false)
	err := s.sqlDatabase.SelectList(dbEntity, func() {
		result := &doctor.DrugDict{}
		dbEntity.CopyTo(result)
		results = append(results, result)
	}, nil, sqlFilter)

	if err != nil {
		return results, business.NewError(errors.InternalError, err)
	}

	return results, nil
}

func (s *Dict) ListSmsParam() ([]*doctor.SmsTemplateParam, business.Error) {
	results := make([]*doctor.SmsTemplateParam, 0)

	sqlFilter := s.sqlDatabase.NewFilter(nil, false, false)
	dbEntity := &sqldb.SmsTemplateParam{}
	err := s.sqlDatabase.SelectList(dbEntity, func() {
		result := &doctor.SmsTemplateParam{}
		dbEntity.CopyTo(result)
		results = append(results, result)
	}, nil, sqlFilter)

	if err != nil {
		return results, business.NewError(errors.InternalError, err)
	}

	return results, nil
}

func (s *Dict) ListSmsContent() ([]*doctor.SmsTemplate, business.Error) {
	results := make([]*doctor.SmsTemplate, 0)

	dbFilter := &sqldb.SmsTemplateFilter{
		IsValid: 1,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	dbEntity := &sqldb.SmsTemplate{}
	err := s.sqlDatabase.SelectList(dbEntity, func() {
		result := &doctor.SmsTemplate{}
		dbEntity.CopyTo(result)
		results = append(results, result)
	}, nil, sqlFilter)

	if err != nil {
		return results, business.NewError(errors.InternalError, err)
	}

	return results, nil
}

func (s *Dict) CreateDevice(argument *doctor.DeviceDictCreate) (uint64, business.Error) {
	if argument == nil {
		return 0, business.NewError(errors.InputError, fmt.Errorf("参数为空"))
	}

	dbFilter := &sqldb.DeviceFilter{
		DeviceCode: argument.DeviceCode,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	dbEntity := &sqldb.DeviceDict{}
	err := s.sqlDatabase.SelectOne(dbEntity, sqlFilter)
	if err == nil {
		return 0, business.NewError(errors.InputInvalid, fmt.Errorf("设备代码已存在"))
	} else {
		if !s.sqlDatabase.IsNoRows(err) {
			return 0, business.NewError(errors.InternalError, err)
		}
	}

	dbCreate := &sqldb.DeviceDict{}
	dbCreate.CopyFromCreate(argument)
	sno, err := s.sqlDatabase.InsertSelective(dbCreate)
	if err != nil {
		return 0, business.NewError(errors.InternalError, err)
	}

	return sno, nil
}

func (s *Dict) DeleteDevice(argument *doctor.DictFilter) (uint64, business.Error) {
	if argument == nil {
		return 0, business.NewError(errors.InputError, fmt.Errorf("参数为空"))
	}
	if argument.SerialNo == nil {
		return 0, business.NewError(errors.InputInvalid, fmt.Errorf("序列号为空"))
	}
	if *argument.SerialNo < 1 {
		return 0, business.NewError(errors.InputInvalid, fmt.Errorf("序列号'%d'无效", argument.SerialNo))
	}

	dbFilter := &sqldb.OrgDictFilter{}
	dbFilter.SerialNo = argument.SerialNo
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	dbEntity := &sqldb.DeviceDict{}
	count, err := s.sqlDatabase.Delete(dbEntity, sqlFilter)
	if err != nil {
		return 0, business.NewError(errors.InternalError, err)
	}

	return count, nil
}

func (s *Dict) EditDevice(argument *doctor.DeviceDictUpdate) (uint64, business.Error) {
	if argument == nil {
		return 0, business.NewError(errors.InputError, fmt.Errorf("参数为空"))
	}

	dbFilter := &sqldb.DeviceDictFilter{}
	dbFilter.SerialNo = &argument.SerialNo
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	dbEntity := &sqldb.DeviceDict{}
	err := s.sqlDatabase.SelectOne(dbEntity, sqlFilter)
	if err != nil {
		if s.sqlDatabase.IsNoRows(err) {
			return 0, business.NewError(errors.NotExist, fmt.Errorf("设备(序列号=%d)不存在", argument.SerialNo))
		} else {
			return 0, business.NewError(errors.InternalError, err)
		}
	}

	dbUpdate := &sqldb.DeviceDictUpdate{}
	dbUpdate.CopyFrom(argument)
	_, err = s.sqlDatabase.UpdateSelective(dbUpdate, sqlFilter)
	if err != nil {
		return 0, business.NewError(errors.InternalError, err)
	}

	return argument.SerialNo, nil
}

func (s *Dict) ListDevice(argument *doctor.DictKeywordFilter) ([]*doctor.DeviceDict, business.Error) {
	results := make([]*doctor.DeviceDict, 0)

	dbFilter := &sqldb.DeviceDictKeywordFilter{}
	dbFilter.CopyFrom(argument)
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, true, false)
	dbEntity := &sqldb.DeviceDict{}
	err := s.sqlDatabase.SelectList(dbEntity, func() {
		result := &doctor.DeviceDict{}
		dbEntity.CopyTo(result)
		results = append(results, result)
	}, nil, sqlFilter)

	if err != nil {
		return results, business.NewError(errors.InternalError, err)
	}

	return results, nil
}

func (s *Dict) CreateDrug(argument *doctor.DrugDictCreate) (uint64, business.Error) {
	if argument == nil {
		return 0, business.NewError(errors.InputError, fmt.Errorf("参数为空"))
	}

	ItemCode, _ := strconv.Atoi(argument.ItemCode)
	dbFilter := &sqldb.DrugFilter{
		ItemCode: uint64(ItemCode),
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	dbEntity := &sqldb.DrugDict{}
	err := s.sqlDatabase.SelectOne(dbEntity, sqlFilter)
	if err == nil {
		return 0, business.NewError(errors.InputInvalid, fmt.Errorf("药物代码已存在"))
	} else {
		if !s.sqlDatabase.IsNoRows(err) {
			return 0, business.NewError(errors.InternalError, err)
		}
	}

	dbCreate := &sqldb.DrugDict{}
	dbCreate.CopyFromCreate(argument)
	sno, err := s.sqlDatabase.InsertSelective(dbCreate)
	if err != nil {
		return 0, business.NewError(errors.InternalError, err)
	}

	return sno, nil
}

func (s *Dict) ListDrugAdmin(argument *doctor.DictKeywordFilter) ([]*doctor.DrugDict, business.Error) {
	results := make([]*doctor.DrugDict, 0)
	dbFilter := &sqldb.DrugDictKeywordFilter{}
	dbFilter.CopyFrom(argument)
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, true, false)
	dbEntity := &sqldb.DrugDict{}
	err := s.sqlDatabase.SelectList(dbEntity, func() {
		result := &doctor.DrugDict{}
		dbEntity.CopyTo(result)
		results = append(results, result)
	}, nil, sqlFilter)

	if err != nil {
		return results, business.NewError(errors.InternalError, err)
	}

	return results, nil
}

func (s *Dict) DeleteDrug(argument *doctor.DictFilter) (uint64, business.Error) {
	if argument == nil {
		return 0, business.NewError(errors.InputError, fmt.Errorf("参数为空"))
	}
	if argument.SerialNo == nil {
		return 0, business.NewError(errors.InputInvalid, fmt.Errorf("序列号为空"))
	}
	if *argument.SerialNo < 1 {
		return 0, business.NewError(errors.InputInvalid, fmt.Errorf("序列号'%d'无效", argument.SerialNo))
	}

	dbFilter := &sqldb.DictFilter{}
	dbFilter.SerialNo = argument.SerialNo
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	dbEntity := &sqldb.DrugDict{}
	count, err := s.sqlDatabase.Delete(dbEntity, sqlFilter)
	if err != nil {
		return 0, business.NewError(errors.InternalError, err)
	}

	return count, nil
}

func (s *Dict) EditDrug(argument *doctor.DrugDictUpdate) (uint64, business.Error) {
	if argument == nil {
		return 0, business.NewError(errors.InputError, fmt.Errorf("参数为空"))
	}

	dbFilter := &sqldb.DictFilter{}
	dbFilter.SerialNo = &argument.SerialNo
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	dbEntity := &sqldb.DrugDict{}
	err := s.sqlDatabase.SelectOne(dbEntity, sqlFilter)
	if err != nil {
		if s.sqlDatabase.IsNoRows(err) {
			return 0, business.NewError(errors.NotExist, fmt.Errorf("药物(序列号=%d)不存在", argument.SerialNo))
		} else {
			return 0, business.NewError(errors.InternalError, err)
		}
	}

	dbUpdate := &sqldb.DrugDictUpdate{}
	dbUpdate.CopyFrom(argument)
	_, err = s.sqlDatabase.UpdateSelective(dbUpdate, sqlFilter)
	if err != nil {
		return 0, business.NewError(errors.InternalError, err)
	}

	return argument.SerialNo, nil
}

func (s *Dict) ListFoodCategory() ([]*doctor.ExpFoodCategory, business.Error) {
	results := make([]*doctor.ExpFoodCategory, 0)

	dbEntity := &sqldb.ExpFoodCategory{}
	dbFilter := &sqldb.ExpFoodCategoryNilFilter{}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	err := s.sqlDatabase.SelectList(dbEntity, func() {
		result := &doctor.ExpFoodCategory{}
		dbEntity.CopyTo(result)
		results = append(results, result)
	}, nil, sqlFilter)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}
	return results, nil
}

func (s *Dict) ListFoodPageByCategory(pageIndex, pageSize uint64, filter *doctor.ExpFoodDictCategoryFilter) (*model.PageResult, business.Error) {
	sqlAccess, err := s.sqlDatabase.NewAccess(false)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}
	defer sqlAccess.Close()

	sqlFilters := make([]database.SqlFilter, 0)
	if filter != nil {
		dbFilter := &sqldb.ExpFoodDictCategoryFilter{}
		dbFilter.CopyFrom(filter)
		sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
		sqlFilters = append(sqlFilters, sqlFilter)
	}

	dbEntity := &sqldb.ExpFoodDict{}
	pageResult := &model.PageResult{}
	datas := make([]*doctor.ExpFoodDict, 0)
	err = sqlAccess.SelectPage(dbEntity, func(total, page, size, index uint64) {
		pageResult.Total = total
		pageResult.Count = page
		pageResult.Size = size
		pageResult.Index = index
	}, func() {
		data := &doctor.ExpFoodDict{}
		dbEntity.CopyTo(data)
		datas = append(datas, data)
	}, pageSize, pageIndex, nil, sqlFilters...)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}
	pageResult.Data = datas

	return pageResult, nil
}

func (s *Dict) FoodSearch(filter *doctor.FoodSearchFilter) ([]*doctor.ExpFoodDict, business.Error) {
	dbFilter := &sqldb.FoodSearchFilter{}
	dbFilter.CopyFrom(filter)
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)

	results := make([]*doctor.ExpFoodDict, 0)
	dbEntity := &sqldb.ExpFoodDict{}
	err := s.sqlDatabase.SelectList(dbEntity, func() {
		result := &doctor.ExpFoodDict{}
		dbEntity.CopyTo(result)
		results = append(results, result)
	}, nil, sqlFilter)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}

	return results, nil
}

func (s *Dict) UploadBaseJinScale(argument *jinscale.BaseScale) business.Error {
	phoneField, err := s.getPhoneField(argument.FormName)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}

	var obj interface{} = argument.Entry
	recordData, err := json.Marshal(argument)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}
	err = s.saveJinScaleRecord(obj, phoneField, argument.FormName, argument.Entry.SerialNumber, recordData)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}

	return nil
}

func (s *Dict) UploadWeightJinScale(argument *jinscale.WeightScale) business.Error {
	phoneField, err := s.getPhoneField(argument.FormName)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}

	var obj interface{} = argument.Entry
	recordData, err := json.Marshal(argument)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}
	err = s.saveJinScaleRecord(obj, phoneField, argument.FormName, argument.Entry.SerialNumber, recordData)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}

	return nil
}

func (s *Dict) UploadDietJinScale(argument *jinscale.DietScale) business.Error {
	phoneField, err := s.getPhoneField(argument.FormName)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}

	var obj interface{} = argument.Entry
	recordData, err := json.Marshal(argument)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}
	err = s.saveJinScaleRecord(obj, phoneField, argument.FormName, argument.Entry.SerialNumber, recordData)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}

	return nil
}

func (s *Dict) UploadExerciseJinScale(argument *jinscale.ExerciseScale) business.Error {
	phoneField, err := s.getPhoneField(argument.FormName)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}

	var obj interface{} = argument.Entry
	recordData, err := json.Marshal(argument)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}
	err = s.saveJinScaleRecord(obj, phoneField, argument.FormName, argument.Entry.SerialNumber, recordData)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}

	return nil
}

func (s *Dict) UploadFitnessJinScale(argument *jinscale.FitnessScale) business.Error {
	phoneField, err := s.getPhoneField(argument.FormName)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}

	var obj interface{} = argument.Entry
	recordData, err := json.Marshal(argument)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}
	err = s.saveJinScaleRecord(obj, phoneField, argument.FormName, argument.Entry.SerialNumber, recordData)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}

	return nil
}

func (s *Dict) UploadPostureJinScale(argument *jinscale.PostureScale) business.Error {
	phoneField, err := s.getPhoneField(argument.FormName)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}

	var obj interface{} = argument.Entry
	recordData, err := json.Marshal(argument)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}
	err = s.saveJinScaleRecord(obj, phoneField, argument.FormName, argument.Entry.SerialNumber, recordData)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}

	return nil
}

func (s *Dict) UploadFitnessEndJinScale(argument *jinscale.FitnessEndScale) business.Error {
	phoneField, err := s.getPhoneField(argument.FormName)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}

	var obj interface{} = argument.Entry
	recordData, err := json.Marshal(argument)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}
	err = s.saveJinScaleRecord(obj, phoneField, argument.FormName, argument.Entry.SerialNumber, recordData)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}

	return nil
}

func (s *Dict) UploadPostureEndJinScale(argument *jinscale.PostureEndScale) business.Error {
	phoneField, err := s.getPhoneField(argument.FormName)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}

	var obj interface{} = argument.Entry
	recordData, err := json.Marshal(argument)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}
	err = s.saveJinScaleRecord(obj, phoneField, argument.FormName, argument.Entry.SerialNumber, recordData)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}

	return nil
}

func (s *Dict) UploadEndJinScale(argument *jinscale.EndScale) business.Error {
	phoneField, err := s.getPhoneField(argument.FormName)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}

	var obj interface{} = argument.Entry
	recordData, err := json.Marshal(argument)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}
	err = s.saveJinScaleRecord(obj, phoneField, argument.FormName, argument.Entry.SerialNumber, recordData)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}

	return nil
}

func (s *Dict) UploadNewEndJinScale(argument *jinscale.NewEndScale) business.Error {
	phoneField, err := s.getPhoneField(argument.FormName)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}

	var obj interface{} = argument.Entry
	recordData, err := json.Marshal(argument)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}
	err = s.saveJinScaleRecord(obj, phoneField, argument.FormName, argument.Entry.SerialNumber, recordData)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}

	return nil
}

func (s *Dict) UploadBaseHealthJinScale(argument *jinscale.BaseHealthScale) business.Error {
	phoneField, err := s.getPhoneField(argument.FormName)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}

	var obj interface{} = argument.Entry
	recordData, err := json.Marshal(argument)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}
	err = s.saveJinScaleRecord(obj, phoneField, argument.FormName, argument.Entry.SerialNumber, recordData)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}

	return nil
}

func (s *Dict) getPhoneField(formName string) (string, error) {
	dbEntity := &sqldb.ExpJinScale{}
	dbFilter := &sqldb.ExpJinScaleNameFilter{
		ScaleName: formName,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	err := s.sqlDatabase.SelectOne(dbEntity, sqlFilter)
	if err != nil {
		return "no such scale", err
	}

	return dbEntity.PhoneField, nil
}

func (s *Dict) saveJinScaleRecord(obj interface{}, phoneField string, scaleName string, serialNumber float64, recordData []byte) error {
	phoneString, err := s.getValueByName(obj, phoneField)
	if err != nil {
		return err
	}
	dbPatientEntity := &sqldb.PatientUserBaseInfo{}
	dbPatientFilter := &sqldb.PatientUserBaseInfoPhoneFilter{
		Phone: &phoneString,
	}
	sqlPatientFilter := s.sqlDatabase.NewFilter(dbPatientFilter, false, false)

	err = s.sqlDatabase.SelectOne(dbPatientEntity, sqlPatientFilter)
	if err != nil {
		if !s.sqlDatabase.IsNoRows(err) {
			return err
		}
	}

	now := time.Now()
	dbRecordEntity := &sqldb.ExpJinScaleRecord{
		PatientID:      dbPatientEntity.UserID,
		ScaleName:      scaleName,
		SerialNumber:   serialNumber,
		Record:         string(recordData),
		CreateDateTime: &now,
	}
	_, err = s.sqlDatabase.Insert(dbRecordEntity)
	if err != nil {
		return err
	}

	return nil
}

func (s *Dict) getValueByName(obj interface{}, name string) (string, error) {
	v := reflect.ValueOf(obj)
	v = v.Elem()
	if !v.FieldByName(name).IsValid() {
		return "", fmt.Errorf("未找到字段")
	}
	return v.FieldByName(name).String(), nil
}

func (s *Dict) GetKnowledgeLinkList() ([]*doctor.ExpKnowledgeLink, business.Error) {
	results := make([]*doctor.ExpKnowledgeLink, 0)

	dbEntity := &sqldb.ExpKnowledgeLink{}
	err := s.sqlDatabase.SelectList(dbEntity, func() {
		result := &doctor.ExpKnowledgeLink{}
		dbEntity.CopyTo(result)
		results = append(results, result)
	}, nil, nil)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}

	return results, nil
}

func (s *Dict) UploadSportsTeamJinScale(argument *jinscale.SportsTeamScale) business.Error {
	phoneField, err := s.getPhoneField(argument.FormName)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}

	var obj interface{} = argument.Entry
	recordData, err := json.Marshal(argument)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}
	err = s.saveJinScaleRecord(obj, phoneField, argument.FormName, argument.Entry.SerialNumber, recordData)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}

	return nil
}

var client = http.Client{Timeout: 10 * time.Second}

func (s *Dict) UploadTestWangScale(argument *jinscale.TestWangScale) business.Error {
	app_secret := "pTUd43r56qgjyCuXFsGYiQwaW97IoOn8"
	now := time.Now()
	timestamp := strconv.FormatInt(now.Unix(), 10)
	short_id := "UZBZJvpAMU"
	app_key := argument.AppKey
	seq := 1
	var build strings.Builder
	build.WriteString(app_key)
	build.WriteString(strconv.Itoa(seq))
	build.WriteString(short_id)
	build.WriteString(timestamp)
	build.WriteString(app_secret)
	before := build.String()
	beforeByte := []byte(before)
	w := crypto.MD5.New()
	w.Write(beforeByte)
	signature := hex.EncodeToString(w.Sum(nil))
	url := "https://open.wenjuan.com/openapi/v4/get_rspd_detail?app_key=" + app_key + "&timestamp=" + timestamp + "&signature=" + signature + "&short_id=" + short_id + "&seq=" + strconv.Itoa(seq)
	resp, err := client.Get(url)
	//resp, err := http.Get(url)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}
	defer resp.Body.Close()
	res := &jinscale.TestResultWangScale{}
	//decoder := json.NewDecoder(resp.Body)
	//err = decoder.Decode(&result)
	body, _ := ioutil.ReadAll(resp.Body)
	fmt.Println(string(body))
	//var res jinscal
	json.Unmarshal(body, &res)
	recordData, err := json.Marshal(res.Data)
	recordDataS := string(recordData)
	//等有具体应用再继续开发就行 基本已经走通了 还差获取手机号码转换到系统用户ID那环
	dbRecordEntity := &sqldb.ExpJinScaleRecord{
		PatientID:      79,
		ScaleName:      argument.Params.EventData.ShortId,
		SerialNumber:   float64(argument.Params.EventData.Seq),
		Record:         recordDataS,
		CreateDateTime: &now,
	}
	_, err = s.sqlDatabase.Insert(dbRecordEntity)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}

	return nil
}

func (s *Dict) GetCampList() ([]string, business.Error) {
	dbEntity := &sqldb.CampList{}
	dbFilter := &sqldb.CampListValidFilter{
		IsValid: 1,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	var result []string
	err := s.sqlDatabase.SelectList(dbEntity, func() {
		result = append(result, dbEntity.Name)
	}, nil, sqlFilter)

	if err != nil {
		return result, business.NewError(errors.InternalError, err)
	}

	return result, nil

}
