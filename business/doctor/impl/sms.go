package impl

import (
	"tlcbme_project/business"
	"tlcbme_project/business/doctor/api"
	"tlcbme_project/data/entity/sqldb"
	"tlcbme_project/data/integration/aliyun"
	"tlcbme_project/data/model/aliyunsms"
	"tlcbme_project/data/model/doctor"
	"tlcbme_project/database"
	"tlcbme_project/database/memory"
	"tlcbme_project/errors"
	"tlcbme_project/server/config"
	"tlcbme_project/server/notify"
	"encoding/json"
	"fmt"
	"github.com/ktpswjz/httpserver/types"
	"time"
)

type Sms struct {
	base
}

func NewSms(cfg *config.Config, log types.Log, sqlDatabase database.SqlDatabase, memoryToken memory.Token, notifyChannels notify.ChannelCollection, client business.Client) api.Sms {
	instance := &Sms{}
	instance.SetLog(log)
	instance.cfg = cfg
	instance.sqlDatabase = sqlDatabase
	instance.client = client
	instance.memoryToken = memoryToken
	instance.notifyChannels = notifyChannels

	return instance
}

func (s *Sms) SendSms(argument *doctor.SmsParam) (string, business.Error) {
	sms := &aliyun.Sms{
		AccessKeyId:      s.cfg.IntegrationAliyun.Sms.AccessKeyId,
		AccessSecret:     s.cfg.IntegrationAliyun.Sms.AccessSecret,
		Timestamp:        time.Now().UTC().Format("2006-01-02T15:04:05Z"),
		Format:           "json",
		SignatureMethod:  "HMAC-SHA1",
		SignatureVersion: "1.0",
		SignatureNonce:   argument.SignatureNonce,
		Signature:        "",

		// 业务参数
		Action:        "SendSms",
		Version:       "2017-05-25",
		RegionId:      "cn-hangzhou",
		PhoneNumbers:  argument.PhoneNumber,
		SignName:      s.cfg.IntegrationAliyun.Sms.Templates.CCP.Sign,
		TemplateCode:  argument.TemplateCode,
		TemplateParam: argument.TemplateParam,
		OutId:         argument.OutId,
	}
	url := fmt.Sprintf("%s?%s", s.cfg.IntegrationAliyun.Sms.Url, sms.GetParam())
	data, code, err := s.client.Get(url, nil)
	if err != nil {
		return "", business.NewError(errors.InternalError, err)
	}
	result := &aliyunsms.Output{}
	if code == 200 {
		err = json.Unmarshal(data, result)
		if err != nil {
			return "", business.NewError(errors.InternalError, err)
		}
		if result.Message == "OK" {
			go func() {
				s.createSmsRecord(argument)
			}()
		}
		return result.Message, nil
	} else {
		result.Message = string(data)
		return result.Message, business.NewError(errors.InternalError, fmt.Errorf("response code: %d", code))
	}
}

func (s *Sms) GetSmsRecordList(filter *doctor.SmsSendRecordFilter) ([]*doctor.SmsSendRecordExt, business.Error) {
	results := make([]*doctor.SmsSendRecordExt, 0)

	dbEntity := &sqldb.SmsSendRecord{}
	dbFilter := &sqldb.SmsSendRecordFilter{}
	dbFilter.CopyFrom(filter)
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	dbOrder := &sqldb.SmsSendRecordOrder{}

	err := s.sqlDatabase.SelectList(dbEntity, func() {
		result := &doctor.SmsSendRecordExt{}
		dbEntity.CopyToExt(result)
		result.SenderName, _ = s.getDoctorName(dbEntity.Sender)
		results = append(results, result)
	}, dbOrder, sqlFilter)

	if err != nil {
		return results, business.NewError(errors.InternalError, err)
	}

	return results, nil
}

func (s *Sms) createSmsRecord(argument *doctor.SmsParam) error {
	dbEntity := &sqldb.SmsSendRecord{}
	dbEntity.CopyFromCreateExt(argument)
	_, err := s.sqlDatabase.Insert(dbEntity)
	if err != nil {
		return err
	}
	s.WriteNotify(&notify.Message{
		BusinessID: notify.BusinessDoctor,
		NotifyID:   notify.DoctorNewSmsRecord,
		Time:       types.Time(time.Now()),
		Data:       argument.PatientID,
	})
	return nil
}
