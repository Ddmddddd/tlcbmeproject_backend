package impl

import (
	"encoding/json"
	"fmt"
	"github.com/ktpswjz/httpserver/security/hash"
	"github.com/ktpswjz/httpserver/types"
	"strings"
	"time"
	"tlcbme_project/business"
	"tlcbme_project/business/doctor/api"
	"tlcbme_project/data/entity/sqldb"
	"tlcbme_project/data/enum"
	"tlcbme_project/data/model"
	"tlcbme_project/data/model/doctor"
	"tlcbme_project/data/model/manage"
	"tlcbme_project/database"
	"tlcbme_project/database/memory"
	"tlcbme_project/errors"
	"tlcbme_project/server/config"
	"tlcbme_project/server/notify"
)

type Patient struct {
	base

	passwordFormat uint64
	alertCodes     map[string]string
}

func NewPatient(cfg *config.Config, log types.Log, sqlDatabase database.SqlDatabase, memoryToken memory.Token, notifyChannels notify.ChannelCollection, passwordFormat uint64, client business.Client) api.Patient {
	instance := &Patient{}
	instance.SetLog(log)
	instance.cfg = cfg
	instance.sqlDatabase = sqlDatabase
	instance.client = client
	instance.memoryToken = memoryToken
	instance.notifyChannels = notifyChannels
	instance.passwordFormat = passwordFormat

	instance.alertCodes = make(map[string]string)

	instance.initAlertCodes()

	return instance
}

func (s *Patient) initAlertCodes() {
	s.alertCodes["B00"] = "低血压"
	s.alertCodes["B06"] = "单次血压过高异常"
	s.alertCodes["B07"] = "周血压异常"
	s.alertCodes["B08"] = "月血压异常"
	s.alertCodes["H02"] = "单次心率异常"
	s.alertCodes["H03"] = "周心率异常"
	s.alertCodes["D00"] = "出现不适"
	s.alertCodes["G01"] = "低血糖"
	s.alertCodes["G04"] = "酮症预警"
	s.alertCodes["G05"] = "血糖二偏高"
}

func (s *Patient) CreateUser(argument *doctor.PatientUserCreateEx) (uint64, business.Error) {
	if argument == nil {
		return 0, business.NewError(errors.InputError, fmt.Errorf("参数为空"))
	}
	if argument.Name == "" {
		return 0, business.NewError(errors.InputInvalid, fmt.Errorf("姓名为空"))
	}
	if argument.UserName == "" {
		return 0, business.NewError(errors.InputInvalid, fmt.Errorf("用户名为空"))
	}
	if argument.Password == "" {
		//return 0, business.NewError(errors.InputInvalid, fmt.Errorf("密码为空"))
	}

	sqlAccess, err := s.sqlDatabase.NewAccess(true)
	if err != nil {
		return 0, business.NewError(errors.InternalError, err)
	}
	defer sqlAccess.Close()

	userID, err := s.createUserAuth(sqlAccess, argument)
	if err != nil {
		return 0, business.NewError(errors.InputInvalid, fmt.Errorf("创建账号失败: %s", err.Error()))
	}
	_, err = s.createUserBasicInfo(sqlAccess, argument, userID)
	if err != nil {
		return 0, business.NewError(errors.InputInvalid, fmt.Errorf("创建基本信息失败: %s", err.Error()))
	}
	_, err = s.createUserManagedIndex(sqlAccess, argument, userID)
	if err != nil {
		return 0, business.NewError(errors.InputInvalid, fmt.Errorf("创建管理索引失败: %s", err.Error()))
	}

	err = sqlAccess.Commit()
	if err != nil {
		return 0, business.NewError(errors.InternalError, err)
	}

	s.WriteNotify(&notify.Message{
		BusinessID: notify.BusinessDoctor,
		NotifyID:   notify.DoctorNewPatient,
		Time:       types.Time(time.Now()),
		Data:       argument,
		Actor: notify.Actor{
			ID:   argument.ActorID,
			Name: argument.ActorName,
		},
	})

	return userID, nil
}

func (s *Patient) createUserAuth(sqlAccess database.SqlAccess, argument *doctor.PatientUserCreateEx) (uint64, error) {
	now := time.Now()
	dbEntity := &sqldb.PatientUserAuths{
		UserName:       argument.UserName,
		RegistDateTime: &now,
	}
	dbFilter := &sqldb.PatientUserAuthsFilter{
		UserName: argument.UserName,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	err := sqlAccess.SelectOne(dbEntity, sqlFilter)
	if err != nil {
		if !sqlAccess.IsNoRows(err) {
			return 0, err
		}
	} else {
		return 0, fmt.Errorf("账号%s已存在", argument.UserName)
	}

	password := argument.Password
	if s.passwordFormat != 0 {
		pwd, err := hash.Hash(password, s.passwordFormat)
		if err != nil {
			return 0, err
		}
		password = pwd
	}
	dbEntity.PasswordFormat = s.passwordFormat
	dbEntity.Password = password

	return sqlAccess.InsertSelective(dbEntity)
}

func (s *Patient) createUserBasicInfo(sqlAccess database.SqlAccess, argument *doctor.PatientUserCreateEx, userID uint64) (uint64, error) {
	dbEntity := &sqldb.PatientUserBaseInfo{
		UserID:             userID,
		IdentityCardNumber: &argument.IdentityCardNumber,
		Name:               argument.Name,
		Phone:              &argument.Phone,
		EducationLevel:     &argument.EducationLevel,
		JobType:            &argument.JobType,
		Height:             argument.Height,
		Weight:             argument.Weight,
		Waistline:          argument.Waistline,
	}
	if argument.Sex != nil {
		dbEntity.Sex = argument.Sex
	}
	if argument.DateOfBirth != nil {
		dateOfBirth := time.Time(*argument.DateOfBirth)
		dbEntity.DateOfBirth = &dateOfBirth
	}

	if len(argument.IdentityCardNumber) > 0 {
		dbFilter := &sqldb.PatientUserBaseInfoFilter{
			IdentityCardNumber: &argument.IdentityCardNumber,
		}
		sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
		err := sqlAccess.SelectOne(dbFilter, sqlFilter)
		if err != nil {
			if !sqlAccess.IsNoRows(err) {
				return 0, err
			}
		} else {
			return 0, fmt.Errorf("身份证号为%s的患者已存在", argument.IdentityCardNumber)
		}
	}

	return sqlAccess.InsertSelective(dbEntity)
}

func (s *Patient) createUserManagedIndex(sqlAccess database.SqlAccess, argument *doctor.PatientUserCreateEx, userID uint64) (uint64, error) {
	dbEntity := &sqldb.ManagedPatientIndex{
		PatientID:           userID,
		OrgCode:             &argument.OrgCode,
		DoctorID:            argument.DoctorID,
		DoctorName:          &argument.DoctorName,
		HealthManagerID:     argument.HealthManagerID,
		HealthManagerName:   &argument.HealthManagerName,
		PatientActiveDegree: 1,
	}
	if len(argument.PatientFeatures) > 0 {
		features := strings.Join(argument.PatientFeatures, ",")
		dbEntity.PatientFeature = &features
	}

	now := time.Now()
	ext := &doctor.ManagedPatientIndexExt{}
	ext.Htn.ManageLevelStartDateTime = types.Time(now)
	ext.Htn.ManageLevel = 0
	ext.Dm.ManageLevelStartDateTime = types.Time(now)
	ext.Dm.ManageLevel = 0
	ext.Dm.Medications = make([]string, 0)

	classCount := len(argument.ManageClasses)
	if classCount > 0 {
		sb := strings.Builder{}
		dbEntityClass := &sqldb.ManagedPatientClass{
			PatientID: userID,
		}
		for classIndex := 0; classIndex < classCount; classIndex++ {
			classItem := argument.ManageClasses[classIndex]
			dbEntityClass.ManageClassCode = classItem.ItemCode

			_, err := sqlAccess.InsertSelective(dbEntityClass)
			if err != nil {
				return 0, err
			}

			// 1-高血压, 2-糖尿病, 3-慢阻肺
			if classItem.ItemCode == 1 {
				dbEntityHtn := &sqldb.HtnManageDetail{
					PatientID:                userID,
					ManageLevel:              0,
					ManageLevelStartDateTime: &now,
				}
				_, err := sqlAccess.InsertSelective(dbEntityHtn)
				if err != nil {
					return 0, err
				}
			} else if classItem.ItemCode == 2 {
				dbEntityDm := &sqldb.DmManageDetail{
					PatientID:                userID,
					ManageLevel:              0,
					ManageLevelStartDateTime: &now,
				}
				_, err := sqlAccess.InsertSelective(dbEntityDm)
				if err != nil {
					return 0, err
				}
				ext.Dm.Type.Name = argument.DmType
				ext.Dm.Medications = argument.DmMedications
			}

			if classIndex > 0 {
				sb.WriteString(",")
			}
			sb.WriteString(classItem.ItemName)
		}

		classes := sb.String()
		dbEntity.ManageClass = &classes
	}

	extData, err := json.Marshal(ext)
	if err != nil {
		return 0, err
	}
	extJson := string(extData)

	dbEntity.ManageStartDateTime = &now
	dbEntity.ManageStatus = enum.ManageStatuses.InManagement().Key
	dbEntity.Ext = &extJson

	return sqlAccess.InsertSelective(dbEntity)
}

func (s *Patient) ListViewManagedPatientIndexPage(pageIndex, pageSize uint64, filters []*doctor.ViewManagedPatientIndexListFilterEx) (*model.PageResult, business.Error) {
	sqlFilters := make([]database.SqlFilter, 0)
	filterCount := len(filters)
	dbEntity := &sqldb.ViewManagedPatientIndex{}

	for filterIndex := 0; filterIndex < filterCount; filterIndex++ {
		filter := filters[filterIndex]
		if filter == nil {
			continue
		}
		dbFilter := &sqldb.ViewManagedPatientIndexListFilter{}
		dbFilter.CopyFrom(filter)
		sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, true)
		sqlFilters = append(sqlFilters, sqlFilter)
	}
	var dbOrder interface{}
	if len(filters) > 0 {
		dbOrder = dbEntity.GetOrder(filters[0].Order)
	}

	pageResult := &model.PageResult{}
	datas := make([]*doctor.ViewManagedPatientIndexEx, 0)
	err := s.sqlDatabase.SelectPage(dbEntity, func(total, page, size, index uint64) {
		pageResult.Total = total
		pageResult.Count = page
		pageResult.Size = size
		pageResult.Index = index
	}, func() {
		data := &doctor.ViewManagedPatientIndexEx{}
		//dbBmiEntity := &sqldb.PatientUserBaseInfoBmi{}
		//dbFilter := &sqldb.PatientUserBaseInfoBmiFilter{}
		//dbFilter.UserID = dbEntity.PatientID
		//sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
		//s.sqlDatabase.SelectOne(dbBmiEntity, sqlFilter)
		dbEntity.CopyToEx(data, nil)

		//Get Patient Team Information
		//dbTeamUserEntity := &sqldb.TlcTeamUser{}
		//dbTeamUserFilter := &sqldb.TlcTeamUserFilterByUserID{UserID: dbEntity.PatientID}
		//sqlTeamUserFilter := s.sqlDatabase.NewFilter(dbTeamUserFilter, false, false)
		//errIn := s.sqlDatabase.SelectOne(dbTeamUserEntity, sqlTeamUserFilter)
		//if errIn != nil {
		//	// Patient has not taken part in team
		//	data.TeamNickName = "暂无队伍信息"
		//	data.BelongToGroup = "暂无大队信息"
		//} else {
		//	dbTeamInfoEntity := &sqldb.TlcTeamInfo{}
		//	dbTeamInfoFilter := &sqldb.TlcTeamInfoFilterByTeamSerialNo{SerialNo: dbTeamUserEntity.TeamSerialNo}
		//	sqlTeamInfoFilter := s.sqlDatabase.NewFilter(dbTeamInfoFilter, false, false)
		//	s.sqlDatabase.SelectOne(dbTeamInfoEntity, sqlTeamInfoFilter)
		//	data.TeamNickName = dbTeamInfoEntity.TeamNickName
		//	data.BelongToGroup = dbTeamInfoEntity.BelongToGroup
		//}
		datas = append(datas, data)
	}, pageSize, pageIndex, dbOrder, sqlFilters...)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}
	pageResult.Data = datas

	return pageResult, nil
}

func (s *Patient) GetManagedIndex(filter *doctor.ManagedPatientIndexFilterBase) (*doctor.ManagedPatientIndex, business.Error) {
	dbFilter := &sqldb.ManagedPatientIndexFilterBase{}
	dbFilter.CopyFrom(filter)
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	dbEntity := &sqldb.ManagedPatientIndex{}

	err := s.sqlDatabase.SelectOne(dbEntity, sqlFilter)
	if err != nil {
		if s.sqlDatabase.IsNoRows(err) {
			return nil, business.NewError(errors.NotExist, err)
		} else {
			return nil, business.NewError(errors.InternalError, err)
		}
	}

	modelEntity := &doctor.ManagedPatientIndex{}
	dbEntity.CopyTo(modelEntity)

	return modelEntity, nil
}

func (s *Patient) SaveAlerts(patientIndex *doctor.ManagedPatientIndex, alerts []manage.InputDataWarnings) business.Error {
	if patientIndex == nil {
		return business.NewError(errors.InternalError, fmt.Errorf("参数（patientIndex）为空"))
	}
	if len(alerts) < 1 {
		return nil
	}

	sqlAccess, err := s.sqlDatabase.NewAccess(true)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}
	defer sqlAccess.Close()

	now := time.Now()
	dbEntity := &sqldb.AlertRecord{
		PatientID:       patientIndex.PatientID,
		ReceiveDateTime: &now,
	}

	abnormalCount := 0
	for _, alert := range alerts {
		_, ok := s.alertCodes[alert.Code]
		if !ok {
			continue
		}
		dbEntity.CopyFromManage(&alert)
		sqlAccess.InsertSelective(dbEntity)
		alertDateTime := types.Time(*dbEntity.AlertDateTime)
		dataType := ""
		if alert.Type == "心率" && (alert.Code == "H02" || alert.Code == "H03") {
			dataType = "心率异常"
		} else if alert.Type == "血压" && (alert.Code == "B00" || alert.Code == "B06" || alert.Code == "B07" || alert.Code == "B08") {
			dataType = "血压异常"
		} else if alert.Code == "D00" {
			dataType = "体征异常"
		}
		if dataType != "" {
			go func(patientId uint64, dataType string, time *types.Time) {
				s.uploadDataToPlatform(patientId, dataType, true, true, time)
			}(dbEntity.PatientID, dataType, &alertDateTime)
		}

		abnormalCount++
	}

	if abnormalCount > 0 {
		err = sqlAccess.Commit()
		if err != nil {
			return business.NewError(errors.InternalError, err)
		}
	}

	s.WriteNotify(&notify.Message{
		BusinessID: notify.BusinessDoctor,
		NotifyID:   notify.DoctorNewPatientAlert,
		Time:       types.Time(time.Now()),
	})

	s.WriteNotify(&notify.Message{
		BusinessID: notify.BusinessPatient,
		NotifyID:   notify.PatientNewAlert,
		Time:       types.Time(time.Now()),
		Actor: notify.Actor{
			ID: patientIndex.PatientID,
		},
		Data: alerts,
	})

	return nil
}

func (s *Patient) ListAlertRecordPage(pageIndex, pageSize uint64, filter *doctor.AlertRecordFilter) (*model.PageResult, business.Error) {
	dbFilter := &sqldb.AlertRecordFilter{}
	dbFilter.CopyFrom(filter)
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	dbOrder := &sqldb.AlertRecordOrder{}
	dbEntity := &sqldb.AlertRecord{}

	pageResult := &model.PageResult{}
	datas := make([]*doctor.AlertRecordEx, 0)
	err := s.sqlDatabase.SelectPage(dbEntity, func(total, page, size, index uint64) {
		pageResult.Total = total
		pageResult.Count = page
		pageResult.Size = size
		pageResult.Index = index
	}, func() {
		data := &doctor.AlertRecordEx{}
		dbEntity.CopyToEx(data)
		datas = append(datas, data)
	}, pageSize, pageIndex, dbOrder, sqlFilter)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}
	pageResult.Data = datas

	return pageResult, nil
}

func (s *Patient) ListAlert(filters []*doctor.AlertRecordFilterEx) ([]*doctor.AlertPatientInfo, business.Error) {
	results := make(doctor.AlertPatientCollection, 0)

	sqlFilters := make([]database.SqlFilter, 0)
	filterCount := len(filters)
	dbEntity := &sqldb.ViewAlertRecord{}
	for filterIndex := 0; filterIndex < filterCount; filterIndex++ {
		filter := filters[filterIndex]
		if filter == nil {
			continue
		}
		dbFilter := &sqldb.ViewAlertRecordFilter{}
		dbFilter.CopyFrom(filter)
		sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, true)
		sqlFilters = append(sqlFilters, sqlFilter)
	}

	err := s.sqlDatabase.SelectList(dbEntity, func() {
		item := &doctor.ViewAlertRecord{}
		dbEntity.CopyTo(item)

		result := results.GetInfo(item.PatientID)
		if result == nil {
			result = &doctor.AlertPatientInfo{}
			result.PatientID = item.PatientID
			result.Alert.BloodPressures = make([]doctor.AlertRecordEx, 0)
			result.Alert.BloodGlucoses = make([]doctor.AlertRecordEx, 0)
			result.Alert.Others = make([]doctor.AlertRecordEx, 0)
			s.fillPatientInfo(result)

			results = append(results, result)
		}

		alertRecord := doctor.AlertRecordEx{}
		alertRecord.SerialNo = item.SerialNo
		alertRecord.PatientID = item.PatientID
		alertRecord.AlertCode = item.AlertCode
		alertRecord.AlertType = item.AlertType
		alertRecord.AlertName = item.AlertName
		alertRecord.AlertReason = item.AlertReason
		alertRecord.AlertMessage = item.AlertMessage
		alertRecord.AlertDateTime = item.AlertDateTime
		alertRecord.Status = item.Status
		alertRecord.ProcessMode = item.ProcessMode

		if item.AlertType == "血压" && item.AlertCode == "B06" {
			result.Alert.BloodPressures = append(result.Alert.BloodPressures, alertRecord)
		} else if item.AlertType == "血糖" && (item.AlertCode == "G01" || item.AlertCode == "G04" || item.AlertCode == "G05") {
			result.Alert.BloodGlucoses = append(result.Alert.BloodGlucoses, alertRecord)
		} else {
			result.Alert.Others = append(result.Alert.Others, alertRecord)
		}

	}, nil, sqlFilters...)

	if err != nil {
		return results, business.NewError(errors.InternalError, err)
	}

	return results, nil
}

func (s *Patient) SearchAlert(filter *doctor.PatientInfoBase) ([]*doctor.AlertPatientRecord, business.Error) {
	dbFilter := &sqldb.AlertRecordAutoIgnoreFilter{
		PatientID: filter.PatientID,
		Status:    0,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	dbEntity := &sqldb.AlertRecord{}
	dbOrder := &sqldb.AlertRecordOrder{}
	results := make([]*doctor.AlertPatientRecord, 0)
	err := s.sqlDatabase.SelectList(dbEntity, func() {
		result := &doctor.AlertPatientRecord{}
		dbEntity.CopyToInfo(result)
		results = append(results, result)
	}, dbOrder, sqlFilter)
	if err != nil {
		return results, business.NewError(errors.InternalError, err)
	}

	return results, nil
}

func (s *Patient) CountAlert(filters []*doctor.AlertRecordFilterEx) (uint64, business.Error) {
	count := uint64(0)

	dbEntity := &sqldb.ViewAlertRecordCount{}
	sqlFilters := make([]database.SqlFilter, 0)
	filterCount := len(filters)
	for filterIndex := 0; filterIndex < filterCount; filterIndex++ {
		filter := filters[filterIndex]
		if filter == nil {
			continue
		}

		dbFilter := &sqldb.ViewAlertRecordFilter{}
		dbFilter.CopyFrom(filter)

		sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, true)
		sqlFilters = append(sqlFilters, sqlFilter)
	}

	err := s.sqlDatabase.SelectDistinct(dbEntity, func() {
		count++

	}, nil, sqlFilters...)

	if err != nil {
		return count, business.NewError(errors.InternalError, err)
	}

	return count, nil
}

func (s *Patient) IgnoreAlert(argument *doctor.AlertRecordIgnoreEx) (uint64, business.Error) {
	if argument == nil {
		return 0, business.NewError(errors.InputError, fmt.Errorf("参数为空"))
	}
	dbEntity := &sqldb.AlertRecordIgnore{}
	dbEntity.CopyFrom(argument)
	dbFilter := &sqldb.AlertRecordIgnoreFilter{}
	dbFilter.CopyFrom(argument)
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)

	count, err := s.sqlDatabase.UpdateSelective(dbEntity, sqlFilter)
	if err != nil {
		return 0, business.NewError(errors.InternalError, err)
	}

	if count > 0 {
		s.WriteNotify(&notify.Message{
			BusinessID: notify.BusinessDoctor,
			NotifyID:   notify.DoctorNewPatientAlert,
			Time:       types.Time(time.Now()),
		})
	}

	return count, nil
}

func (s *Patient) fillPatientInfo(info *doctor.AlertPatientInfo) error {
	if info == nil {
		return nil
	}

	sqlAccess, err := s.sqlDatabase.NewAccess(false)
	if err != nil {
		return err
	}
	defer sqlAccess.Close()

	masterIndexDbFilter := &sqldb.ViewManagedPatientIndexListFilter{
		PatientID: &info.PatientID,
	}
	masterIndexSqlFilter := s.sqlDatabase.NewFilter(masterIndexDbFilter, false, false)
	masterIndexDbEntity := &sqldb.ViewManagedPatientIndex{}
	err = sqlAccess.SelectOne(masterIndexDbEntity, masterIndexSqlFilter)
	if err != nil {
		return err
	}
	dbBmiEntity := &sqldb.PatientUserBaseInfoBmi{}
	dbFilter := &sqldb.PatientUserBaseInfoBmiFilter{}
	dbFilter.UserID = info.PatientID
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	err = s.sqlDatabase.SelectOne(dbBmiEntity, sqlFilter)
	if err != nil {
		if s.sqlDatabase.IsNoRows(err) {
			return err
		} else {
			return err
		}
	}
	masterIndexInfo := &doctor.ViewManagedPatientIndexEx{}
	masterIndexDbEntity.CopyToEx(masterIndexInfo, dbBmiEntity.GetBmi())
	info.PatientName = masterIndexInfo.PatientName
	info.Sex = masterIndexInfo.Sex
	info.SexText = masterIndexInfo.SexText
	info.DateOfBirth = masterIndexInfo.DateOfBirth
	info.Age = masterIndexInfo.Age
	info.Tag = masterIndexInfo.Tag

	patientDbFilter := &sqldb.PatientUserBaseInfoUserIdFilter{
		UserID: info.PatientID,
	}
	patientSqlFilter := s.sqlDatabase.NewFilter(patientDbFilter, false, false)
	patientDbEntity := &sqldb.PatientUserBaseInfo{}
	err = sqlAccess.SelectOne(patientDbEntity, patientSqlFilter)
	if err != nil {
		return err
	}
	info.Photo = string(patientDbEntity.Photo)

	return nil
}

func (s *Patient) SearchPatientOne(filter *doctor.ViewPatientFilter) (*doctor.ViewPatientEx, business.Error) {
	sqlFilters := make([]database.SqlFilter, 0)
	if filter != nil {
		dbFilter := &sqldb.ViewPatientFilter{}
		dbFilter.CopyFrom(filter)
		sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
		sqlFilters = append(sqlFilters, sqlFilter)
	}

	dbEntity := &sqldb.ViewPatient{}
	err := s.sqlDatabase.SelectOne(dbEntity, sqlFilters...)
	if err != nil {
		if s.sqlDatabase.IsNoRows(err) {
			return nil, business.NewError(errors.NotExist, err)
		} else {
			return nil, business.NewError(errors.InternalError, err)
		}
	}

	dbBmiEntity := &sqldb.PatientUserBaseInfoBmi{}
	dbFilter := &sqldb.PatientUserBaseInfoBmiFilter{}
	if filter != nil {
		dbFilter.UserID = *filter.PatientID
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	err = s.sqlDatabase.SelectOne(dbBmiEntity, sqlFilter)
	if err != nil {
		if s.sqlDatabase.IsNoRows(err) {
			return nil, business.NewError(errors.NotExist, err)
		} else {
			return nil, business.NewError(errors.InternalError, err)
		}
	}

	result := &doctor.ViewPatientEx{}
	dbEntity.CopyToEx(result, dbBmiEntity.GetBmi())

	return result, nil
}

func (s *Patient) SearchPatientBasicInfoOne(filter *doctor.ViewPatientBaseInfoFilterEx) (*doctor.ViewPatientBaseInfoEx, business.Error) {
	sqlFilters := make([]database.SqlFilter, 0)
	if filter != nil {
		dbFilter := &sqldb.ViewPatientBaseInfoFilter{}
		dbFilter.CopyFromEx(filter)
		sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
		sqlFilters = append(sqlFilters, sqlFilter)
	}

	dbEntity := &sqldb.ViewPatientBaseInfo{}
	err := s.sqlDatabase.SelectOne(dbEntity, sqlFilters...)
	if err != nil {
		if s.sqlDatabase.IsNoRows(err) {
			return nil, business.NewError(errors.NotExist, err)
		} else {
			return nil, business.NewError(errors.InternalError, err)
		}
	}

	result := &doctor.ViewPatientBaseInfoEx{}
	dbEntity.CopyToEx(result)

	return result, nil
}

func (s *Patient) SaveManagement(patientIndex *doctor.ManagedPatientIndex, management *manage.InputDataManagement, flag uint64) business.Error {
	if patientIndex == nil {
		return business.NewError(errors.InternalError, fmt.Errorf("参数（patientIndex）为空"))
	}
	if management == nil {
		return business.NewError(errors.InternalError, fmt.Errorf("参数（management）为空"))
	}

	sqlAccess, err := s.sqlDatabase.NewAccess(true)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}
	defer sqlAccess.Close()

	now := time.Now()
	startTime := &now
	if management.Advise != nil {
		dbEntity := &sqldb.ManagementPlan{}
		dbEntity.CopyFromPush(management, patientIndex.PatientID)

		if len(dbEntity.PlanType) > 0 {
			dbEntityUpdate := &sqldb.ManagementPlanUpdateStatus{
				Status: 2, // 0-未开始，1-使用中，2-已作废
			}
			dbFilterUpdate := &sqldb.ManagementPlanFilter{
				PatientID: &patientIndex.PatientID,
				PlanType:  dbEntity.PlanType,
				Statuses: []uint64{
					1,
				},
			}
			sqlFilterUpdate := s.sqlDatabase.NewFilter(dbFilterUpdate, false, false)
			_, err = sqlAccess.UpdateSelective(dbEntityUpdate, sqlFilterUpdate)
			if err != nil {
				return business.NewError(errors.InternalError, err)
			}
		}

		_, err = sqlAccess.InsertSelective(dbEntity)
		if err != nil {
			return business.NewError(errors.InternalError, err)
		}

		startTime = dbEntity.PlanStartDateTime
	}

	ext := &doctor.ManagedPatientIndexExt{}
	ext.Init(patientIndex.Ext)
	if flag == 1 {
		if ext.Htn.ManageLevel != uint64(management.Rank) {
			if management.Advise.Type == "高血压" {
				err = s.saveHtnManageDetail(sqlAccess, patientIndex.PatientID, uint64(management.Rank), startTime)
				if err != nil {
					return business.NewError(errors.InternalError, err)
				}
			}
			ext.Htn.ManageLevel = uint64(management.Rank)
			if startTime != nil {
				ext.Htn.ManageLevelStartDateTime = types.Time(*startTime)
			} else {
				ext.Htn.ManageLevelStartDateTime = types.Time(now)
			}
			dbEntityExtUpdate := &sqldb.ManagedPatientIndexExtUpdate{}
			dbEntityExtUpdate.CopyFrom(ext)
			dbEntityExtUpdate.SerialNo = patientIndex.SerialNo
			_, err = sqlAccess.UpdateByPrimaryKey(dbEntityExtUpdate)
			if err != nil {
				return business.NewError(errors.InternalError, err)
			}
		}
	} else if flag == 2 {
		if ext.Dm.ManageLevel != uint64(management.Rank) {
			err = s.saveDmManageDetail(sqlAccess, patientIndex.PatientID, uint64(management.Rank), startTime)
			if err != nil {
				return business.NewError(errors.InternalError, err)
			}
			ext.Dm.ManageLevel = uint64(management.Rank)
			if startTime != nil {
				ext.Dm.ManageLevelStartDateTime = types.Time(*startTime)
			} else {
				ext.Dm.ManageLevelStartDateTime = types.Time(now)
			}
			dbEntityExtUpdate := &sqldb.ManagedPatientIndexExtUpdate{}
			dbEntityExtUpdate.CopyFrom(ext)
			dbEntityExtUpdate.SerialNo = patientIndex.SerialNo
			_, err = sqlAccess.UpdateByPrimaryKey(dbEntityExtUpdate)
			if err != nil {
				return business.NewError(errors.InternalError, err)
			}
		}
	}
	err = sqlAccess.Commit()
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}

	return nil
}

func (s *Patient) SearchManagementPlanList(filter *doctor.ManagementPlanFilter) ([]*doctor.ManagementPlanEx, business.Error) {
	results := make([]*doctor.ManagementPlanEx, 0)

	dbEntity := &sqldb.ManagementPlan{}
	dbOrder := &sqldb.ManagementPlanOrder{}
	dbFilter := &sqldb.ManagementPlanFilter{}
	dbFilter.CopyFrom(filter)
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)

	err := s.sqlDatabase.SelectList(dbEntity, func() {
		result := &doctor.ManagementPlanEx{}
		dbEntity.CopyToEx(result)
		results = append(results, result)

	}, dbOrder, sqlFilter)

	if err != nil {
		return results, business.NewError(errors.InternalError, err)
	}

	return results, nil
}

func (s *Patient) SearchManagementApplicationReviewPage(pageIndex, pageSize uint64, filter *doctor.ViewManagementApplicationReviewFilterEx) (*model.PageResult, business.Error) {
	sqlAccess, err := s.sqlDatabase.NewAccess(false)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}
	defer sqlAccess.Close()
	total, err := s.searchManagementApplicationCount(sqlAccess, filter)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}
	size := pageSize
	if size < 1 {
		size = 1
	}
	pageCount := total / size
	if (total % size) != 0 {
		pageCount++
	}
	if pageIndex > pageCount {
		pageIndex = pageCount
	} else if pageIndex < 1 {
		pageIndex = 1
	}

	pageResult := &model.PageResult{
		Data: make([]*doctor.ViewManagementApplicationReview, 0),
	}
	pageResult.Total = total
	pageResult.Count = pageCount
	pageResult.Size = size
	pageResult.Index = pageIndex

	dbEntity := &sqldb.ViewManagementApplicationReview{}
	sqlEntity := s.sqlDatabase.NewEntity()
	err = sqlEntity.Parse(dbEntity)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}
	sqlBuilder := s.sqlDatabase.NewBuilder()
	sqlBuilder.Reset()
	sqlBuilder.Select(sqlEntity.ScanFields(), false).From(sqlEntity.Name())
	s.buildManagementApplicationPageWhere(filter, sqlBuilder)
	dbOrder := dbEntity.GetOrder(filter.Order)
	sqlEntityOrder := s.sqlDatabase.NewEntity()
	err = sqlEntityOrder.Parse(dbOrder)
	if err == nil {
		count := sqlEntityOrder.FieldCount()
		if count > 0 {
			sqlBuilder.Append(fmt.Sprintf("order by %s %s", sqlEntityOrder.Field(0).Name(), sqlEntityOrder.Field(0).Order()))

			for i := 1; i < count; i++ {
				sqlBuilder.Append(fmt.Sprintf(", %s %s", sqlEntityOrder.Field(i).Name(), sqlEntityOrder.Field(i).Order()))
			}
		}
	}
	startIndex := (pageIndex - 1) * size
	sqlBuilder.Append("LIMIT ?, ?", startIndex, size)

	query := sqlBuilder.Query()
	args := sqlBuilder.Args()
	rows, err := sqlAccess.Query(query, args...)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}
	defer rows.Close()
	datas := make([]*doctor.ViewManagementApplicationReviewEx, 0)
	for rows.Next() {
		err = rows.Scan(sqlEntity.ScanArgs()...)
		if err != nil {
			return nil, business.NewError(errors.InternalError, err)
		}

		data := &doctor.ViewManagementApplicationReviewEx{}
		dbEntity.CopyToEx(data)
		datas = append(datas, data)
	}
	pageResult.Data = datas

	return pageResult, nil
}

func (s *Patient) AuditPatientUser(argument *doctor.PatientUserAuditEx) (uint64, uint64, business.Error) {
	if argument == nil {
		return 0, 0, business.NewError(errors.InputError, fmt.Errorf("参数为空"))
	}

	sqlAccess, err := s.sqlDatabase.NewAccess(true)
	if err != nil {
		return 0, 0, business.NewError(errors.InternalError, err)
	}
	defer sqlAccess.Close()

	dbEntityAuth := &sqldb.ManagementApplicationReviewAuth{}
	dbEntityAuth.CopyFrom(argument)
	count, err := sqlAccess.UpdateSelectiveByPrimaryKey(dbEntityAuth)
	if err != nil {
		return 0, 0, business.NewError(errors.InternalError, err)
	}
	if count != 1 {
		return 0, 0, business.NewError(errors.NotExist, fmt.Errorf("患者(id=%d)不存在", argument.PatientID))
	}

	// 审核状态 0-未审核，1-审核通过，2-审核不通过
	if argument.Status == 1 {
		dbEntity := &sqldb.ManagedPatientIndex{
			PatientID:           argument.PatientID,
			OrgCode:             &argument.ReviewerOrgCode,
			DoctorID:            argument.DoctorID,
			DoctorName:          &argument.DoctorName,
			HealthManagerID:     argument.HealthManagerID,
			HealthManagerName:   &argument.HealthManagerName,
			PatientActiveDegree: 1,
		}
		if len(argument.PatientFeatures) > 0 {
			features := strings.Join(argument.PatientFeatures, ",")
			dbEntity.PatientFeature = &features
		}

		now := time.Now()
		ext := &doctor.ManagedPatientIndexExt{}
		ext.Htn.ManageLevelStartDateTime = types.Time(now)
		ext.Htn.ManageLevel = 0
		ext.Dm.ManageLevelStartDateTime = types.Time(now)
		ext.Dm.ManageLevel = 0
		ext.Dm.Medications = make([]string, 0)

		classCount := len(argument.ManageClasses)
		if classCount > 0 {
			sb := strings.Builder{}
			dbEntityClass := &sqldb.ManagedPatientClass{
				PatientID: argument.PatientID,
			}
			for classIndex := 0; classIndex < classCount; classIndex++ {
				classItem := argument.ManageClasses[classIndex]
				dbEntityClass.ManageClassCode = classItem.ItemCode

				_, err := sqlAccess.InsertSelective(dbEntityClass)
				if err != nil {
					return 0, 0, business.NewError(errors.InternalError, err)
				}

				// 1-高血压, 2-糖尿病, 3-慢阻肺
				if classItem.ItemCode == 1 {
					dbEntityHtn := &sqldb.HtnManageDetail{
						PatientID:                argument.PatientID,
						ManageLevel:              0,
						ManageLevelStartDateTime: &now,
					}
					_, err := sqlAccess.InsertSelective(dbEntityHtn)
					if err != nil {
						return 0, 0, business.NewError(errors.InternalError, err)
					}
				} else if classItem.ItemCode == 2 {
					dbEntityDm := &sqldb.DmManageDetail{
						PatientID:                argument.PatientID,
						ManageLevel:              0,
						ManageLevelStartDateTime: &now,
					}
					_, err := sqlAccess.InsertSelective(dbEntityDm)
					if err != nil {
						return 0, 0, business.NewError(errors.InternalError, err)
					}

					ext.Dm.Type.Name = argument.DmType
					ext.Dm.Medications = argument.DmMedications
				}

				if classIndex > 0 {
					sb.WriteString(",")
				}
				sb.WriteString(classItem.ItemName)
			}

			classes := sb.String()
			dbEntity.ManageClass = &classes
		}

		extData, err := json.Marshal(ext)
		if err != nil {
			return 0, 0, business.NewError(errors.InternalError, err)
		}
		extJson := string(extData)

		dbEntity.ManageStartDateTime = &now
		dbEntity.ManageStatus = enum.ManageStatuses.InManagement().Key
		dbEntity.Ext = &extJson

		_, err = sqlAccess.InsertSelective(dbEntity)
		if err != nil {
			return 0, 0, business.NewError(errors.InternalError, err)
		}

	}
	modelEntity := &doctor.PatientUserBaseInfoModify{}
	argument.CopyToEx(modelEntity)
	_, sno, err := s.modifyPatientUserBaseInfo(sqlAccess, modelEntity)
	if err != nil {
		return 0, 0, business.NewError(errors.InputInvalid, fmt.Errorf("修改失败: %s", err.Error()))
	}
	if len(argument.DiagnosisMemo) > 0 || len(argument.Diagnosis) > 0 {
		id, err := s.modifyApplicationReview(sqlAccess, modelEntity)
		if err != nil {
			return 0, 0, business.NewError(errors.InputInvalid, fmt.Errorf("修改失败: %s", err.Error()))
		}
		if id == 0 {
			_, err = s.createApplicationReview(sqlAccess, modelEntity)
		}
	}
	err = s.modifyPatientAuth(sqlAccess, argument.Phone, dbEntityAuth.PatientID)
	err = sqlAccess.Commit()
	if err != nil {
		return 0, 0, business.NewError(errors.InternalError, err)
	}

	s.WriteNotify(&notify.Message{
		BusinessID: notify.BusinessDoctor,
		NotifyID:   notify.DoctorAuditPatient,
		Time:       types.Time(time.Now()),
	})

	return argument.PatientID, sno, nil
}

func (s *Patient) SearchAuditCount(filter *doctor.ManagementApplicationReviewFilter) (uint64, business.Error) {
	sqlAccess, err := s.sqlDatabase.NewAccess(false)
	if err != nil {
		return 0, business.NewError(errors.InternalError, err)
	}
	defer sqlAccess.Close()
	count, err := s.searchManagementApplicationAuditCount(sqlAccess, filter)
	if err != nil {
		return 0, business.NewError(errors.InternalError, err)
	}

	return count, nil
}

func (s *Patient) SearchViewManagedIndexMangeLevelCount(filter *doctor.ViewManagedIndexFilterEx) (*doctor.ViewManagedIndexCountManageLevel, business.Error) {
	sqlAccess, err := s.sqlDatabase.NewAccess(false)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}
	defer sqlAccess.Close()

	dbFilter := &sqldb.ViewManagedIndexFilter{}
	dbFilter.CopyFromEx(filter)

	sqlBuilder := s.sqlDatabase.NewBuilder()
	sqlBuilder.Select("`ManageStatus`, `ManageLevel`, count(*)", false).From("`ViewManagedIndex`")
	if dbFilter.SerialNo != nil {
		sqlBuilder.WhereAnd("`SerialNo` = ? ", dbFilter.SerialNo)
	}
	if dbFilter.PatientID != nil {
		sqlBuilder.WhereAnd("`PatientID` = ? ", dbFilter.PatientID)
	}
	if dbFilter.ManageClassCode != nil {
		sqlBuilder.WhereAnd("`ManageClassCode` = ? ", dbFilter.ManageClassCode)
	}
	if dbFilter.DoctorID != nil && dbFilter.HealthManagerID != nil {
		sqlBuilder.WhereAnd("( `DoctorID` = ? ", dbFilter.DoctorID)
		sqlBuilder.WhereOr("`HealthManagerID` = ? ", dbFilter.DoctorID)
		sqlBuilder.Append(") ")
	}
	if dbFilter.DoctorID != nil && dbFilter.HealthManagerID == nil {
		sqlBuilder.WhereAnd(" `DoctorID` = ? ", dbFilter.DoctorID)
	}
	if dbFilter.HealthManagerID != nil && dbFilter.DoctorID == nil {
		sqlBuilder.WhereAnd("`HealthManagerID` = ? ", dbFilter.HealthManagerID)
	}
	if len(dbFilter.OrgCode) > 0 {
		sqlBuilder.WhereAnd("`OrgCode` = ? ", dbFilter.OrgCode)
	}
	if len(dbFilter.ManageLevel) > 0 {
		//sqlBuilder.WhereAnd("`ManageLevel` = ? ", dbFilter.ManageLevel)
	}
	if len(dbFilter.DmManageLevel) > 0 {
		sqlBuilder.WhereAnd("`DmManageLevel` = ? ", dbFilter.DmManageLevel)
	}
	if len(dbFilter.PatientName) > 0 {
		sqlBuilder.WhereAnd("`PatientName` like ? ", fmt.Sprint("%", dbFilter.PatientName, "%"))
	}
	if len(dbFilter.PatientActiveDegree) > 0 {
		sqlBuilder.WhereFormatAnd("`PatientActiveDegree` in %s ", dbFilter.PatientActiveDegree)
	}
	sqlBuilder.Append("group by `ManageStatus`, `ManageLevel`")

	sqlQuery := sqlBuilder.Query()
	sqlArgs := sqlBuilder.Args()

	sqlRows, err := sqlAccess.Query(sqlQuery, sqlArgs...)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}
	defer sqlRows.Close()

	result := &doctor.ViewManagedIndexCountManageLevel{
		Total:      0,
		Terminated: 0,
		Level0:     0,
		Level1:     0,
		Level2:     0,
	}

	terminatedStatus := enum.ManageStatuses.TerminatedManagement().Key
	var status, count uint64
	var level string
	for sqlRows.Next() {
		err = sqlRows.Scan(&status, &level, &count)
		if err != nil {
			return nil, business.NewError(errors.InternalError, err)
		}

		result.Total += count
		if status == terminatedStatus {
			result.Terminated += count
		} else {
			if level == "0" {
				result.Level0 += count
			} else if level == "1" {
				result.Level1 += count
			} else if level == "2" {
				result.Level2 += count
			}
		}
	}

	return result, nil
}

func (s *Patient) SearchViewManagedIndexDmTypeCount(filter *doctor.ViewManagedIndexFilterEx) (*doctor.ViewDmManagedIndexCountManageLevel, business.Error) {
	sqlAccess, err := s.sqlDatabase.NewAccess(false)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}
	defer sqlAccess.Close()

	dbFilter := &sqldb.ViewManagedIndexFilter{}
	dbFilter.CopyFromEx(filter)

	sqlBuilder := s.sqlDatabase.NewBuilder()
	sqlBuilder.Select("`ManageStatus`, `DmManageLevel`, count(*)", false).From("`ViewManagedIndex`")
	if dbFilter.SerialNo != nil {
		sqlBuilder.WhereAnd("`SerialNo` = ? ", dbFilter.SerialNo)
	}
	if dbFilter.PatientID != nil {
		sqlBuilder.WhereAnd("`PatientID` = ? ", dbFilter.PatientID)
	}
	if dbFilter.ManageClassCode != nil {
		sqlBuilder.WhereAnd("`ManageClassCode` = ? ", dbFilter.ManageClassCode)
	}
	if dbFilter.DoctorID != nil && dbFilter.HealthManagerID != nil {
		sqlBuilder.WhereAnd("(`DoctorID` = ? ", dbFilter.DoctorID)
		sqlBuilder.WhereOr("`HealthManagerID` = ? ", dbFilter.DoctorID)
		sqlBuilder.Append(") ")
	}
	if dbFilter.DoctorID != nil && dbFilter.HealthManagerID == nil {
		sqlBuilder.WhereAnd("`DoctorID` = ? ", dbFilter.DoctorID)
	}
	if dbFilter.HealthManagerID != nil && dbFilter.DoctorID == nil {
		sqlBuilder.WhereAnd("`HealthManagerID` = ? ", dbFilter.HealthManagerID)
	}
	if len(dbFilter.OrgCode) > 0 {
		sqlBuilder.WhereAnd("`OrgCode` = ? ", dbFilter.OrgCode)
	}
	if len(dbFilter.ManageLevel) > 0 {
		sqlBuilder.WhereAnd("`ManageLevel` = ? ", dbFilter.ManageLevel)
	}
	if len(dbFilter.DmManageLevel) > 0 {
		//sqlBuilder.WhereAnd("`DmManageLevel` = ? ", dbFilter.DmManageLevel)
	}
	if len(dbFilter.PatientName) > 0 {
		sqlBuilder.WhereAnd("`PatientName` like ? ", fmt.Sprint("%", dbFilter.PatientName, "%"))
	}
	if len(dbFilter.PatientActiveDegree) > 0 {
		sqlBuilder.WhereFormatAnd("`PatientActiveDegree` in %s ", dbFilter.PatientActiveDegree)
	}
	sqlBuilder.Append("group by `ManageStatus`, `DmManageLevel`")

	sqlQuery := sqlBuilder.Query()
	sqlArgs := sqlBuilder.Args()

	sqlRows, err := sqlAccess.Query(sqlQuery, sqlArgs...)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}
	defer sqlRows.Close()

	result := &doctor.ViewDmManagedIndexCountManageLevel{
		Total:      0,
		Terminated: 0,
		Level0:     0,
		Level10:    0,
		Level20:    0,
		Level30:    0,
	}

	terminatedStatus := enum.ManageStatuses.TerminatedManagement().Key
	var status, count uint64
	var level string
	for sqlRows.Next() {
		err = sqlRows.Scan(&status, &level, &count)
		if err != nil {
			return nil, business.NewError(errors.InternalError, err)
		}

		result.Total += count
		if status == terminatedStatus {
			result.Terminated += count
		} else {
			if level == "0" {
				result.Level0 += count
			} else if level == "10" {
				result.Level10 += count
			} else if level == "20" {
				result.Level20 += count
			} else if level == "30" {
				result.Level30 += count
			}
		}
	}

	return result, nil
}

func (s *Patient) SearchViewManagedIndexPage(pageIndex, pageSize uint64, filters []*doctor.ViewManagedIndexFilterEx) (*model.PageResult, business.Error) {
	sqlFilters := make([]database.SqlFilter, 0)
	filterCount := len(filters)
	dbEntity := &sqldb.ViewManagedIndex{}

	for filterIndex := 0; filterIndex < filterCount; filterIndex++ {
		filter := filters[filterIndex]
		if filter == nil {
			continue
		}
		dbFilter := &sqldb.ViewManagedIndexFilter{}
		dbFilter.CopyFromEx(filter)
		sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, true)
		sqlFilters = append(sqlFilters, sqlFilter)
	}
	var dbOrder interface{}
	if len(filters) > 0 {
		dbOrder = dbEntity.GetOrder(filters[0].Order)
	}

	pageResult := &model.PageResult{}
	datas := make([]*doctor.ViewManagedIndexEx, 0)
	err := s.sqlDatabase.SelectPage(dbEntity, func(total, page, size, index uint64) {
		pageResult.Total = total
		pageResult.Count = page
		pageResult.Size = size
		pageResult.Index = index
	}, func() {
		data := &doctor.ViewManagedIndexEx{}
		dbBmiEntity := &sqldb.PatientUserBaseInfoBmi{}
		dbFilter := &sqldb.PatientUserBaseInfoBmiFilter{}
		dbFilter.UserID = dbEntity.PatientID
		sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
		s.sqlDatabase.SelectOne(dbBmiEntity, sqlFilter)
		dbEntity.CopyToEx(data, dbBmiEntity.GetBmi())
		datas = append(datas, data)
	}, pageSize, pageIndex, dbOrder, sqlFilters...)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}
	pageResult.Data = datas

	return pageResult, nil
}

func (s *Patient) SearchViewSportDietRecordPage(pageIndex, pageSize uint64, filter *doctor.ViewSportDietRecordFilter) (*model.PageResult, business.Error) {
	dbFilter := &sqldb.ViewSportDietRecordFilter{}
	dbFilter.CopyFrom(filter)
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	dbOrder := &sqldb.ViewSportDietRecordOrder{}
	dbEntity := &sqldb.ViewSportDietRecord{}

	pageResult := &model.PageResult{}
	datas := make([]*doctor.ViewSportDietRecord, 0)
	err := s.sqlDatabase.SelectPage(dbEntity, func(total, page, size, index uint64) {
		pageResult.Total = total
		pageResult.Count = page
		pageResult.Size = size
		pageResult.Index = index
	}, func() {
		data := &doctor.ViewSportDietRecord{}
		dbEntity.CopyTo(data)
		datas = append(datas, data)
	}, pageSize, pageIndex, dbOrder, sqlFilter)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}
	pageResult.Data = datas

	return pageResult, nil
}

func (s *Patient) SearchDiscomfortRecordPage(pageIndex, pageSize uint64, filter *doctor.DiscomfortRecordDataFilterEx) (*model.PageResult, business.Error) {
	dbFilter := &sqldb.DiscomfortRecordDataFilter{}
	dbFilter.CopyFrom(filter)
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	dbOrder := &sqldb.DiscomfortRecordDataOrder{}
	dbEntity := &sqldb.DiscomfortRecord{}

	pageResult := &model.PageResult{}
	datas := make([]*doctor.DiscomfortRecord, 0)
	err := s.sqlDatabase.SelectPage(dbEntity, func(total, page, size, index uint64) {
		pageResult.Total = total
		pageResult.Count = page
		pageResult.Size = size
		pageResult.Index = index
	}, func() {
		data := &doctor.DiscomfortRecord{}
		dbEntity.CopyTo(data)
		datas = append(datas, data)
	}, pageSize, pageIndex, dbOrder, sqlFilter)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}
	pageResult.Data = datas

	return pageResult, nil
}

func (s *Patient) SearchViewStatDiscomfortList(filter *doctor.ViewStatDiscomfortFilter) ([]*doctor.ViewStatDiscomfort, business.Error) {
	sqlAccess, err := s.sqlDatabase.NewAccess(false)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}
	defer sqlAccess.Close()

	dbFilter := &sqldb.ViewDiscomfortRecordDataFilter{}
	if filter != nil {
		dbFilter.PatientID = filter.PatientID
		if filter.HappenDateTimeStart != nil {
			dbFilter.HappenStartDate = filter.HappenDateTimeStart.ToDate(0)
		}
		if filter.HappenDateTimeEnd != nil {
			dbFilter.HappenEndDate = filter.HappenDateTimeEnd.ToDate(1)
		}
	}
	sqlBuilder := s.sqlDatabase.NewBuilder()
	sqlBuilder.Select("`Discomfort`, COUNT(*) AS `Count` ", false)
	sqlBuilder.From(fmt.Sprintf("`%s`", dbFilter.TableName()))
	sqlBuilder.WhereAnd("`PatientID` = ?", dbFilter.PatientID)
	if dbFilter.HappenStartDate != nil {
		sqlBuilder.WhereAnd("`HappenDateTime` >= ? ", dbFilter.HappenStartDate)
	}
	if dbFilter.HappenEndDate != nil {
		sqlBuilder.WhereAnd("`HappenDateTime` < ? ", dbFilter.HappenEndDate)
	}
	sqlBuilder.Append(" GROUP BY `Discomfort`")
	sqlBuilder.Append(" ORDER BY `Count` DESC")

	sqlQuery := sqlBuilder.Query()
	sqlRows, err := sqlAccess.Query(sqlQuery, sqlBuilder.Args()...)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}
	defer sqlRows.Close()

	results := make([]*doctor.ViewStatDiscomfort, 0)
	discomfort := ""
	count := uint64(0)
	for sqlRows.Next() {
		err = sqlRows.Scan(&discomfort, &count)
		if err != nil {
			return nil, business.NewError(errors.InternalError, err)
		}

		result := &doctor.ViewStatDiscomfort{
			Discomfort: discomfort,
			Count:      count,
		}
		results = append(results, result)
	}

	return results, nil
}

func (s *Patient) SearchPatientHeightAndWeight(filter *doctor.PatientInfoBase) (*doctor.PatientUserHeightAndWeight, business.Error) {
	dbFilter := &sqldb.PatientUserBaseInfoUserIdFilter{}
	if filter != nil {
		dbFilter.UserID = filter.PatientID
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	dbEntity := &sqldb.PatientUserHeightAndWeight{}
	err := s.sqlDatabase.SelectOne(dbEntity, sqlFilter)
	if err != nil {
		if s.sqlDatabase.IsNoRows(err) {
			return nil, business.NewError(errors.NotExist, err)
		} else {
			return nil, business.NewError(errors.InternalError, err)
		}
	}

	modelEntity := &doctor.PatientUserHeightAndWeight{}
	dbEntity.CopyTo(modelEntity)

	dbEntityWaistline := &sqldb.PatientUserWaistline{}
	dbWaistlineFilter := &sqldb.PatientUserAuthsUserIdFilter{}
	if filter != nil {
		dbWaistlineFilter.UserID = filter.PatientID
	}
	sqlWaistlineFilter := s.sqlDatabase.NewFilter(dbWaistlineFilter, false, false)
	//sqlWaistlineOrder := &sqldb.WaistlineOrder{}
	err = s.sqlDatabase.SelectList(dbEntityWaistline, func() {
		if dbEntityWaistline.Waistline != nil {
			modelEntity.Waistline = dbEntityWaistline.Waistline
		}
	}, nil, sqlWaistlineFilter)
	if err != nil {
		if s.sqlDatabase.IsNoRows(err) {
			return nil, business.NewError(errors.NotExist, err)
		} else {
			return nil, business.NewError(errors.InternalError, err)
		}
	}
	return modelEntity, nil
}

func (s *Patient) UpdatePatientHeight(argument *doctor.PatientUserHeight) (uint64, business.Error) {
	dbFilter := &sqldb.PatientUserBaseInfoUserIdFilter{}
	if argument != nil {
		dbFilter.UserID = argument.PatientID
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	dbEntity := &sqldb.PatientUserHeight{}
	dbEntity.CopyFrom(argument)
	_, err := s.sqlDatabase.UpdateSelective(dbEntity, sqlFilter)
	if err != nil {
		if s.sqlDatabase.IsNoRows(err) {
			return 0, business.NewError(errors.NotExist, err)
		} else {
			return 0, business.NewError(errors.InternalError, err)
		}
	}

	return argument.PatientID, nil
}

func (s *Patient) UpdateComplianceRateAndPatientActiveDegree(index *doctor.ManagedPatientIndex, complianceRate uint64, patientActiveDegree uint64) business.Error {
	if index == nil {
		return business.NewError(errors.InputError, fmt.Errorf("参数为空"))
	}

	dbEntity := &sqldb.ManagedPatientIndexComplianceRateAndPatientActiveDegreeUpdate{
		SerialNo:            index.SerialNo,
		ComplianceRate:      complianceRate,
		PatientActiveDegree: patientActiveDegree,
	}

	_, err := s.sqlDatabase.UpdateByPrimaryKey(dbEntity)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}

	return nil
}

func (s *Patient) ModifyPatientUserBaseInfo(argument *doctor.PatientUserBaseInfoModify, doctorName string, doctorID uint64) (uint64, uint64, business.Error) {
	sqlAccess, err := s.sqlDatabase.NewAccess(true)
	if err != nil {
		return 0, 0, business.NewError(errors.InternalError, err)
	}
	defer sqlAccess.Close()
	//_, _, err = s.modifyPatientUserBaseInfo(sqlAccess, argument)
	//if err != nil {
	//	return 0, 0, business.NewError(errors.InputInvalid, fmt.Errorf("修改失败: %s", err.Error()))
	//}
	//err = s.modifyPatientAuth(sqlAccess, argument.Phone, argument.UserID)
	//if err != nil {
	//	return 0, 0, business.NewError(errors.InputInvalid, fmt.Errorf("修改失败: %s", err.Error()))
	//}
	_, err = s.modifyPatientIndex(sqlAccess, argument, doctorName, doctorID)
	if err != nil {
		return 0, 0, business.NewError(errors.InputInvalid, fmt.Errorf("修改失败: %s", err.Error()))
	}
	_, err = s.modifyPatientNutritionalIndicator(sqlAccess, argument)
	if err != nil {
		return 0, 0, business.NewError(errors.InputInvalid, fmt.Errorf("修改营养指标失败: %s", err.Error()))
	}
	if &argument.Country != nil && argument.Country != "" {
		_, err = s.modifyPatientUserCountry(sqlAccess, argument)
		if err != nil {
			return 0, 0, business.NewError(errors.InputInvalid, fmt.Errorf("训练营修改失败：%s", err.Error()))
		}
	}


	//if len(argument.DiagnosisMemo) > 0 || len(argument.Diagnosis) > 0 {
	//	id, err := s.modifyApplicationReview(sqlAccess, argument)
	//	if err != nil {
	//		return 0, 0, business.NewError(errors.InputInvalid, fmt.Errorf("修改失败: %s", err.Error()))
	//	}
	//	if id == 0 {
	//		_, err = s.createApplicationReview(sqlAccess, argument)
	//	}
	//}
	err = sqlAccess.Commit()
	if err != nil {
		return 0, 0, business.NewError(errors.InternalError, err)
	}
	return 0, 0, nil
}

func (s *Patient) modifyPatientUserCountry(sqlAccess database.SqlAccess, argument *doctor.PatientUserBaseInfoModify) (uint64, error) {
	dbEntity := &sqldb.PatientUserBaseInfoUpdateCountry{
		Country: argument.Country,
	}
	dbFilter := &sqldb.PatientUserBaseInfoUserIdFilter{
		UserID: argument.UserID,
	}

	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	_, err := sqlAccess.UpdateSelective(dbEntity, sqlFilter)
	if err != nil {
		return 0, err
	}
	return argument.UserID, nil
}

func (s *Patient) modifyPatientUserBaseInfo(sqlAccess database.SqlAccess, argument *doctor.PatientUserBaseInfoModify) (uint64, uint64, error) {
	//if len(argument.IdentityCardNumber) > 0 {
	//	dbEntity := &sqldb.PatientUserBaseInfo{}
	//	dbFilter := &sqldb.PatientUserBaseInfoFilter{
	//		IdentityCardNumber: &argument.IdentityCardNumber,
	//	}
	//	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	//	err := sqlAccess.SelectOne(dbEntity, sqlFilter)
	//	if err != nil {
	//		if !sqlAccess.IsNoRows(err) {
	//			return 0, 0, err
	//		}
	//	} else if dbEntity.UserID != argument.UserID {
	//		return 0, 0, fmt.Errorf("身份证号为%s的患者已存在", argument.IdentityCardNumber)
	//	}
	//}
	dbEntity := &sqldb.PatientUserBaseInfoExt{}
	dbEntity.CopyFromExt(argument)
	dbFilter := &sqldb.PatientUserBaseInfoUserIdFilter{
		UserID: argument.UserID,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	_, err := sqlAccess.UpdateSelective(dbEntity, sqlFilter)
	if err != nil {
		return 0, 0, err
	}
	sno := uint64(0)
	// 新增体重记录 不用判断是否和原来一样
	//if argument.Weight != nil {
	//	now := time.Now()
	//	dbEntityWeight := &sqldb.WeightRecord{
	//		PatientID:       argument.UserID,
	//		Weight:          *argument.Weight,
	//		Waistline:       argument.Waistline,
	//		Height:          argument.Height,
	//		InputDateTime:   &now,
	//		MeasureDateTime: &now,
	//	}
	//	sno, err = sqlAccess.InsertSelective(dbEntityWeight)
	//	if err != nil {
	//		return 0, 0, err
	//	}
	//
	//}
	return argument.UserID, sno, nil
}

func (s *Patient) modifyPatientIndex(sqlAccess database.SqlAccess, argument *doctor.PatientUserBaseInfoModify, doctorName string, doctorID uint64) (uint64, error) {

	dbFilter := &sqldb.ManagedPatientIndexFilterExtBase{
		PatientID: &argument.UserID,
	}
	dbEntity := &sqldb.ModifyPatientIndex{}
	dbEntity.CopyFromExt(argument)
	now := time.Now()
	ext := &doctor.ManagedPatientIndexExt{}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)

	index, _ := s.getPatientMaster(argument.UserID)
	entityIndex := &doctor.ManagedPatientIndex{}
	index.CopyTo(entityIndex)
	extMap := entityIndex.Ext.(map[string]interface{})
	data, err := json.Marshal(extMap)
	if err != nil {
		return 0, err
	}
	err = json.Unmarshal(data, ext)
	if err != nil {
		return 0, err
	}
	ext.Dm.Type.Name = argument.DmType
	ext.Dm.Medications = argument.DmMedications
	switch argument.ManageChangeType {
	case 0:
		sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
		_, err := sqlAccess.UpdateSelective(dbEntity, sqlFilter)
		if err != nil {
			return 0, err
		}
	// 高血压 变 糖尿病
	case 1:
		err := s.terminatedHtnManagement(sqlAccess, argument.ManageChangeType, argument, doctorName, doctorID)
		dbEntityClass := &sqldb.ManagedPatientClass{
			PatientID:       argument.UserID,
			ManageClassCode: 2,
		}
		_, err = sqlAccess.UpdateSelective(dbEntityClass, sqlFilter)
		if err != nil {
			return 0, err
		}
		dbEntityDm := &sqldb.DmManageDetail{
			PatientID:                argument.UserID,
			ManageLevel:              0,
			ManageLevelStartDateTime: &now,
		}
		dbEntityDm.ManageLevel = ext.Dm.ManageLevel
		_, err = sqlAccess.InsertSelective(dbEntityDm)
		if err != nil {
			return 0, err
		}
	// 高血压 变 糖尿病，高血压
	case 2:
		s.postStartDm(argument.UserID)
		dbEntityClass := &sqldb.ManagedPatientClass{
			PatientID:       argument.UserID,
			ManageClassCode: 2,
		}
		_, err := sqlAccess.InsertSelective(dbEntityClass)
		if err != nil {
			return 0, err
		}
		dbEntityDm := &sqldb.DmManageDetail{
			PatientID:                argument.UserID,
			ManageLevel:              0,
			ManageLevelStartDateTime: &now,
		}
		_, err = sqlAccess.InsertSelective(dbEntityDm)
		if err != nil {
			return 0, err
		}
	// 糖尿病 变 高血压
	case 3:
		err := s.terminatedDmManagement(sqlAccess, argument.ManageChangeType, argument, doctorName, doctorID)
		dbEntityClass := &sqldb.ManagedPatientClass{
			PatientID:       argument.UserID,
			ManageClassCode: 1,
		}
		_, err = sqlAccess.UpdateSelective(dbEntityClass, sqlFilter)
		if err != nil {
			return 0, err
		}
		dbEntityHtn := &sqldb.HtnManageDetail{
			PatientID:                argument.UserID,
			ManageLevel:              0,
			ManageLevelStartDateTime: &now,
		}
		dbEntityHtn.ManageLevel = ext.Htn.ManageLevel
		_, err = sqlAccess.InsertSelective(dbEntityHtn)
		if err != nil {
			return 0, err
		}
	// 糖尿病 变 糖尿病，高血压
	case 4:
		ext.Htn.ManageLevelStartDateTime = types.Time(now)
		ext.Htn.ManageLevel = 0
		s.postStartHtn(argument.UserID)
		dbEntityClass := &sqldb.ManagedPatientClass{
			PatientID:       argument.UserID,
			ManageClassCode: 1,
		}
		_, err := sqlAccess.InsertSelective(dbEntityClass)
		if err != nil {
			return 0, err
		}
		dbEntityDm := &sqldb.HtnManageDetail{
			PatientID:                argument.UserID,
			ManageLevel:              0,
			ManageLevelStartDateTime: &now,
		}
		dbEntityDm.ManageLevel = ext.Dm.ManageLevel
		_, err = sqlAccess.InsertSelective(dbEntityDm)
		if err != nil {
			return 0, err
		}
	//糖尿病，高血压 变 高血压
	case 5:
		dbEntityClass := &sqldb.ManagedPatientClass{}
		dbEntityClassFilter := &sqldb.ManagedPatientClass{
			PatientID:       argument.UserID,
			ManageClassCode: 2,
		}
		sqlClassFilter := s.sqlDatabase.NewFilter(dbEntityClassFilter, false, false)
		_, err = sqlAccess.Delete(dbEntityClass, sqlClassFilter)
		if err != nil {
			return 0, err
		}
		dbEntityHtn := &sqldb.DmManageDetail{
			PatientID:                argument.UserID,
			ManageLevel:              9,
			ManageLevelStartDateTime: &now,
		}
		_, err = sqlAccess.InsertSelective(dbEntityHtn)
		if err != nil {
			return 0, err
		}
		s.postStopDm(argument.UserID)
		_, err = s.ignoreFollowup(sqlAccess, argument.UserID, "糖尿病常规随访")
		if err != nil {
			return 0, err
		}
		err = s.ignorePlan(sqlAccess, argument.UserID, "糖尿病", doctorName, doctorID)
		if err != nil {
			return 0, err
		}
	//糖尿病，高血压 变 糖尿病
	case 6:
		ext.Htn.ManageLevelStartDateTime = types.Time(now)
		ext.Htn.ManageLevel = 0
		dbEntityClass := &sqldb.ManagedPatientClass{}
		dbEntityClassFilter := &sqldb.ManagedPatientClass{
			PatientID:       argument.UserID,
			ManageClassCode: 1,
		}
		sqlClassFilter := s.sqlDatabase.NewFilter(dbEntityClassFilter, false, false)
		_, err = sqlAccess.Delete(dbEntityClass, sqlClassFilter)
		if err != nil {
			return 0, err
		}
		dbEntityHtn := &sqldb.HtnManageDetail{
			PatientID:                argument.UserID,
			ManageLevel:              9,
			ManageLevelStartDateTime: &now,
		}
		_, err = sqlAccess.InsertSelective(dbEntityHtn)
		if err != nil {
			return 0, err
		}
		s.postStopHtn(argument.UserID)
		_, err = s.ignoreFollowup(sqlAccess, argument.UserID, "高血压常规随访")
		if err != nil {
			return 0, err
		}
		err = s.ignorePlan(sqlAccess, argument.UserID, "高血压", doctorName, doctorID)
		if err != nil {
			return 0, err
		}
	}
	extData, err := json.Marshal(ext)
	if err != nil {
		return 0, err
	}
	extJson := string(extData)
	dbEntity.Ext = &extJson
	_, err = sqlAccess.UpdateSelective(dbEntity, sqlFilter)
	if err != nil {
		return 0, err
	}
	s.WriteNotify(&notify.Message{
		BusinessID: notify.BusinessDoctor,
		NotifyID:   notify.DoctorUpdatePatientBaseInfo,
		Time:       types.Time(time.Now()),
	})

	return argument.UserID, nil
}

func (s *Patient) modifyApplicationReview(sqlAccess database.SqlAccess, argument *doctor.PatientUserBaseInfoModify) (uint64, error) {
	dbFilter := &sqldb.ManagementApplicationReviewExtFilter{
		PatientID: argument.UserID,
	}
	dbEntity := &sqldb.ManagementApplicationExt{}
	dbEntity.CopyFromExt(argument)
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	id, err := sqlAccess.UpdateSelective(dbEntity, sqlFilter)
	if err != nil {
		return 0, err
	}
	return id, nil
}

func (s *Patient) createApplicationReview(sqlAccess database.SqlAccess, argument *doctor.PatientUserBaseInfoModify) (uint64, error) {
	dbEntity := &sqldb.ManagementApplicationReview{
		PatientID:       argument.UserID,
		VisitOrgCode:    &argument.VisitOrgCode,
		Diagnosis:       &argument.Diagnosis,
		DiagnosisMemo:   &argument.DiagnosisMemo,
		HealthManagerID: argument.HealthManagerID,
		Status:          1,
	}
	id, err := sqlAccess.InsertSelective(dbEntity)
	if err != nil {
		return 0, err
	}
	return id, nil

}

func (s *Patient) searchManagementApplicationCount(sqlAccess database.SqlAccess, filter *doctor.ViewManagementApplicationReviewFilterEx) (uint64, error) {
	dbEntity := &sqldb.ViewManagementApplicationReview{}
	sqlBuilder := s.sqlDatabase.NewBuilder()
	sqlBuilder.Reset()
	sqlBuilder.Select("COUNT(*)", false).From(dbEntity.TableName())

	s.buildManagementApplicationPageWhere(filter, sqlBuilder)

	count := uint64(0)
	query := sqlBuilder.Query()
	row := sqlAccess.QueryRow(query, sqlBuilder.Args()...)
	err := row.Scan(&count)
	if err != nil {
		return 0, err
	}

	return count, nil
}

func (s *Patient) searchManagementApplicationAuditCount(sqlAccess database.SqlAccess, filter *doctor.ManagementApplicationReviewFilter) (uint64, error) {
	dbEntity := &sqldb.ViewManagementApplicationReview{}
	sqlBuilder := s.sqlDatabase.NewBuilder()
	sqlBuilder.Reset()
	sqlBuilder.Select("COUNT(*)", false).From(dbEntity.TableName())

	s.buildManagementApplicationPageAuditWhere(filter, sqlBuilder)

	count := uint64(0)
	query := sqlBuilder.Query()
	row := sqlAccess.QueryRow(query, sqlBuilder.Args()...)
	err := row.Scan(&count)
	if err != nil {
		return 0, err
	}

	return count, nil
}

func (s *Patient) buildManagementApplicationPageAuditWhere(filter *doctor.ManagementApplicationReviewFilter, builder database.SqlBuilder) {
	if filter == nil {
		return
	}
	dbFilter := &sqldb.ManagementApplicationReviewCountFilter{}
	dbFilter.CopyFrom(filter)
	if dbFilter.SerialNo != nil {
		builder.WhereAnd("`SerialNo` = ? ", dbFilter.SerialNo)
	}
	if dbFilter.HealthManagerID != nil {
		builder.WhereAnd("(`HealthManagerID` = ? ", dbFilter.HealthManagerID)
		builder.WhereOr("`HealthManagerID` is null ")
		builder.Append(") ")
	}
	if len(dbFilter.Statuses) > 0 {
		builder.WhereFormatAnd("`Status` in %s ", dbFilter.Statuses)
	}
	if len(dbFilter.VisitOrgCode) > 0 {
		builder.WhereAnd("`VisitOrgCode` = ? ", dbFilter.VisitOrgCode)
	}
}

func (s *Patient) buildManagementApplicationPageWhere(filter *doctor.ViewManagementApplicationReviewFilterEx, builder database.SqlBuilder) {
	if filter == nil {
		return
	}
	dbFilter := &sqldb.ViewManagementApplicationReviewFilter{}
	dbFilter.CopyFromEx(filter)

	if dbFilter.SerialNo != nil {
		builder.WhereAnd("`SerialNo` = ? ", dbFilter.SerialNo)
	}
	if dbFilter.PatientID != nil {
		builder.WhereAnd("`PatientID` = ? ", dbFilter.PatientID)
	}
	if len(dbFilter.PatientName) > 0 {
		builder.WhereAnd("`PatientName` like ? ", filter.PatientName)
	}
	if len(dbFilter.ApplicationFrom) > 0 {
		builder.WhereAnd("`ApplicationFrom` = ? ", dbFilter.ApplicationFrom)
	}
	if len(dbFilter.OrgVisitID) > 0 {
		builder.WhereAnd("`OrgVisitID` = ? ", dbFilter.OrgVisitID)
	}
	if dbFilter.HealthManagerID != nil {
		builder.WhereAnd("(`HealthManagerID` = ? ", dbFilter.HealthManagerID)
		builder.WhereOr("`HealthManagerID` is null ")
		builder.Append(") ")
	}
	if len(dbFilter.Statuses) > 0 {
		builder.WhereFormatAnd("`Status` in %s ", dbFilter.Statuses)
	}
	if len(dbFilter.IdentityCardNumber) > 0 {
		builder.WhereAnd("`IdentityCardNumber` = ? ", dbFilter.IdentityCardNumber)
	}
	if len(dbFilter.Phone) > 0 {
		builder.WhereAnd("`Phone` = ? ", dbFilter.Phone)
	}
	if len(dbFilter.VisitOrgCode) > 0 {
		builder.WhereAnd("`VisitOrgCode` = ? ", dbFilter.VisitOrgCode)
	}
}

func (s *Patient) terminatedHtnManagement(sqlAccess database.SqlAccess, manageChangeType uint64, argument *doctor.PatientUserBaseInfoModify, doctorName string, doctorID uint64) error {
	_, err := s.ignoreFollowup(sqlAccess, argument.UserID)
	if err != nil {
		return err
	}
	s.postStopHtn(argument.UserID)
	s.postStartDm(argument.UserID)
	err = s.ignorePlan(sqlAccess, argument.UserID, "高血压", doctorName, doctorID)
	if err != nil {
		return err
	}
	now := time.Now()
	dbEntityHtn := &sqldb.HtnManageDetail{
		PatientID:                argument.UserID,
		ManageLevel:              9,
		ManageLevelStartDateTime: &now,
	}
	_, err = sqlAccess.InsertSelective(dbEntityHtn)
	if err != nil {
		return err
	}

	return nil
}

func (s *Patient) ignoreFollowup(sqlAccess database.SqlAccess, patientID uint64, followupType ...string) (uint64, error) {
	memo := "终止管理，系统自动忽略"
	now := time.Now()
	dbEntity := &sqldb.FollowupPlanAutoIgnore{
		Status:     2,
		StatusMemo: &memo,
		UpdateTime: &now,
	}
	dbFilter := &sqldb.FollowupPlanAutoFilter{
		Status:    0,
		PatientID: &patientID,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	if len(followupType) > 0 {
		dbAutoFilter := &sqldb.FollowupPlanAutoTypeFilter{
			Status:       0,
			PatientID:    &patientID,
			FollowUpType: followupType[0],
		}
		sqlFilter = s.sqlDatabase.NewFilter(dbAutoFilter, false, false)
	}
	count, err := sqlAccess.Update(dbEntity, sqlFilter)
	if err != nil {
		return 0, err
	}

	if count < 1 {
		return 0, nil
	}

	s.WriteNotify(&notify.Message{
		BusinessID: notify.BusinessDoctor,
		NotifyID:   notify.DoctorUpdateFollowup,
		Time:       types.Time(time.Now()),
	})

	return count, nil
}

func (s *Patient) autoIgnoreAlert(patientID uint64, userID uint64, userName string) (uint64, error) {
	dbEntity := &sqldb.AlertRecordIgnore{}
	dbEntity.Init(userID, userName)
	dbFilter := &sqldb.AlertRecordAutoIgnoreFilter{
		Status:    enum.AlertStatuses.Unprocessed().Key,
		PatientID: patientID,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	count, err := s.sqlDatabase.Update(dbEntity, sqlFilter)
	if err != nil {
		return 0, err
	}

	if count > 0 {
		s.WriteNotify(&notify.Message{
			BusinessID: notify.BusinessDoctor,
			NotifyID:   notify.DoctorNewPatientAlert,
			Time:       types.Time(time.Now()),
		})
	}

	return count, nil
}

func (s *Patient) terminatedDmManagement(sqlAccess database.SqlAccess, manageChangeType uint64, argument *doctor.PatientUserBaseInfoModify, doctorName string, doctorID uint64) error {
	_, err := s.ignoreFollowup(sqlAccess, argument.UserID)
	if err != nil {
		return err
	}
	s.postStopDm(argument.UserID)
	s.postStartHtn(argument.UserID)
	err = s.ignorePlan(sqlAccess, argument.UserID, "糖尿病", doctorName, doctorID)
	if err != nil {
		return err
	}
	now := time.Now()
	dbEntityHtn := &sqldb.DmManageDetail{
		PatientID:                argument.UserID,
		ManageLevel:              9,
		ManageLevelStartDateTime: &now,
	}
	_, err = sqlAccess.InsertSelective(dbEntityHtn)
	if err != nil {
		return err
	}

	return nil
}

func (s *Patient) ignorePlan(sqlAccess database.SqlAccess, patientID uint64, planType string, doctorName string, doctorID uint64) error {
	now := time.Now()
	cancelStatus := uint64(2)
	dbEntity := &sqldb.ManagementPlan{
		PatientID:          patientID,
		LastModifyDateTime: &now,
		ModifierName:       &doctorName,
		ModifierID:         &doctorID,
		Status:             cancelStatus,
	}
	dbFilter := &sqldb.ManagementPlanAutoFilter{
		PatientID: &patientID,
		PlanType:  planType,
		Status:    1,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	_, err := sqlAccess.UpdateSelective(dbEntity, sqlFilter)
	if err != nil {
		return err
	}
	return nil
}

func (s *Patient) postStopDm(patientID uint64) error {
	providerType := config.ManagementProviderTypeDm
	uri := s.cfg.Management.Providers.Dm.Api.Uri.ManagementProviderApiUri.StopService
	info := &manage.EnableManageInput{
		PatientID: fmt.Sprint(patientID),
	}
	businessName := "终止患者糖尿病管理"
	go func() {
		s.client.ManageBusinessPost(businessName, patientID, providerType, uri, info)
	}()

	return nil
}

func (s *Patient) postStartHtn(patientID uint64) error {
	c1 := make(chan manage.Result)
	providerType := config.ManagementProviderTypeHbp
	uri := s.cfg.Management.Providers.Hbp.Api.Uri.RegDisease
	argument := &manage.DiseaseInfoInput{
		PatientID: fmt.Sprint(patientID),
	}
	argument.Info.Name = "高血压"
	argument.Info.Code = 1
	businessName := "疾病注册-高血压"
	go func() {
		result, _ := s.client.ManageBusinessPost(businessName, patientID, providerType, uri, argument)
		c1 <- *result
	}()
	res := <-c1
	if strings.Contains(res.Message, "已经存在") {
		s.postRestartHtn(patientID)
	}

	return nil
}

func (s *Patient) postStopHtn(patientID uint64) error {
	providerType := config.ManagementProviderTypeHbp
	uri := s.cfg.Management.Providers.Hbp.Api.Uri.ManagementProviderApiUri.StopService
	info := &manage.EnableManageInput{
		PatientID: fmt.Sprint(patientID),
	}
	businessName := "终止患者高血压管理"
	go func() {
		s.client.ManageBusinessPost(businessName, patientID, providerType, uri, info)
	}()

	return nil
}

func (s *Patient) postStartDm(patientID uint64) error {
	c1 := make(chan manage.Result)
	providerType := config.ManagementProviderTypeDm
	uri := s.cfg.Management.Providers.Hbp.Api.Uri.RegDisease
	argument := &manage.DiseaseInfoInput{
		PatientID: fmt.Sprint(patientID),
	}
	argument.Info.Name = "糖尿病"
	argument.Info.Code = 2
	businessName := "疾病注册-糖尿病"
	go func() {
		result, _ := s.client.ManageBusinessPost(businessName, patientID, providerType, uri, argument)
		c1 <- *result
	}()
	res := <-c1
	if strings.Contains(res.Message, "已经存在") {
		s.postRestartDm(patientID)
	}

	return nil
}

func (s *Patient) postRestartDm(patientID uint64) error {
	providerType := config.ManagementProviderTypeDm
	uri := s.cfg.Management.Providers.Dm.Api.Uri.StartService
	info := &manage.EnableManageInput{
		PatientID: fmt.Sprint(patientID),
	}
	businessName := "恢复患者管理"
	go func() {
		s.client.ManageBusinessPost(businessName, patientID, providerType, uri, info)
	}()

	return nil
}

func (s *Patient) postRestartHtn(patientID uint64) error {
	providerType := config.ManagementProviderTypeHbp
	uri := s.cfg.Management.Providers.Hbp.Api.Uri.StartService
	info := &manage.EnableManageInput{
		PatientID: fmt.Sprint(patientID),
	}
	businessName := "恢复患者管理"
	go func() {
		s.client.ManageBusinessPost(businessName, patientID, providerType, uri, info)
	}()

	return nil
}

func (s *Patient) modifyPatientAuth(sqlAccess database.SqlAccess, phone string, patientId uint64) error {
	dbFilter := &sqldb.PatientUserAuthsUserIdFilter{
		UserID: patientId,
	}
	dbEntity := &sqldb.PatientUserAuthsPhoneUpdate{
		UserID:      patientId,
		MobilePhone: &phone,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	_, err := sqlAccess.UpdateSelective(dbEntity, sqlFilter)
	if err != nil {
		return err
	}
	return nil
}

func (s *Patient) UpdatePatientDiagnosismemo(argument *doctor.ManagementApplicationReviewMemo) (uint64, business.Error) {
	dbFilter := &sqldb.ManagementApplicationReviewMemoFilter{}
	if argument != nil {
		dbFilter.PatientID = argument.PatientID
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	dbEntity := &sqldb.ManagementApplicationReviewMemo{}
	dbEntity.CopyFrom(argument)
	_, err := s.sqlDatabase.UpdateSelective(dbEntity, sqlFilter)
	if err != nil {
		if s.sqlDatabase.IsNoRows(err) {
			return 0, business.NewError(errors.NotExist, err)
		} else {
			return 0, business.NewError(errors.InternalError, err)
		}
	}

	return argument.PatientID, nil
}

func (s *Patient) ListPatientWeightPhoto(argument *doctor.WeightRecordDataFilterEx) ([]*doctor.WeightRecordForPhoto, business.Error) {
	dbFilter := &sqldb.WeightRecordPatientFilter{}
	if argument != nil {
		dbFilter.PatientID = argument.PatientID
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	dbEntity := &sqldb.WeightRecord{}
	results := make([]*doctor.WeightRecordForPhoto, 0)

	err := s.sqlDatabase.SelectList(dbEntity, func() {
		result := &doctor.WeightRecordForPhoto{}
		dbEntity.CopyToPhoto(result)
		results = append(results, result)
	}, nil, sqlFilter)

	if err != nil {
		return results, business.NewError(errors.InternalError, err)
	}

	return results, nil

}

func (s *Patient) modifyPatientNutritionalIndicator(sqlAccess database.SqlAccess, argument *doctor.PatientUserBaseInfoModify) (uint64, error) {
	dbEntity := &sqldb.ExpPatientNutritionalIndicator{}
	dbEntity2 := &sqldb.ExpPatientNutritionalIndicator{}
	dbEntity.CopyFromExt(argument)
	dbFilter := &sqldb.PatientUserBaseInfoUserIdFilter{
		UserID: argument.UserID,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)

	err := sqlAccess.SelectOne(dbEntity2, sqlFilter)
	if err != nil {
		if s.sqlDatabase.IsNoRows(err) {
			uno, _ := sqlAccess.InsertSelective(dbEntity)
			return uno, nil
		}
		return 0, err
	}

	uno, err := sqlAccess.UpdateSelective(dbEntity, sqlFilter)
	if err != nil {
		return 0, err
	}
	return uno, nil
}
