package impl

import (
	"tlcbme_project/business"
	"tlcbme_project/business/doctor/api"
	"tlcbme_project/data/entity/sqldb"
	"tlcbme_project/data/model/doctor"
	"tlcbme_project/database"
	"tlcbme_project/database/memory"
	"tlcbme_project/errors"
	"fmt"
	"github.com/ktpswjz/httpserver/types"
)

type Verified struct {
	base
}

func NewVerified(log types.Log, sqlDatabase database.SqlDatabase, memoryToken memory.Token) api.Verified {
	instance := &Verified{}
	instance.SetLog(log)
	instance.sqlDatabase = sqlDatabase
	instance.memoryToken = memoryToken

	return instance
}

func (s *Verified) Save(argument *doctor.VerifiedDoctorUserEdit) (uint64, business.Error) {
	if argument == nil {
		return 0, business.NewError(errors.InputError, fmt.Errorf("参数为空"))
	}
	if argument.UserID < 1 {
		return 0, business.NewError(errors.InputInvalid, fmt.Errorf("用户ID'%d'无效", argument.UserID))
	}

	dbEntityCheck := &sqldb.VerifiedDoctorUserInfo{}
	dbFilter := &sqldb.VerifiedDoctorUserFilter{UserID: argument.UserID}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	err := s.sqlDatabase.SelectOne(dbEntityCheck, sqlFilter)
	if err != nil {
		if s.sqlDatabase.IsNoRows(err) {
			dbEntity := &sqldb.VerifiedDoctorUser{}
			dbEntity.CopyFromEdit(argument)
			_, err = s.sqlDatabase.InsertSelective(dbEntity)
			if err != nil {
				return 0, business.NewError(errors.InternalError, err)
			}
		} else {
			return 0, business.NewError(errors.InternalError, err)
		}
	} else {
		dbEntityEdit := &sqldb.VerifiedDoctorUserEdit{}
		dbEntityEdit.CopyFrom(argument)
		_, err = s.sqlDatabase.UpdateSelective(dbEntityEdit, sqlFilter)
		if err != nil {
			return 0, business.NewError(errors.InternalError, err)
		}
	}

	return argument.UserID, nil
}

func (s *Verified) Detail(argument *doctor.VerifiedDoctorUserFilter) (*doctor.VerifiedDoctorUserEx, business.Error) {
	if argument == nil {
		return nil, business.NewError(errors.InputError, fmt.Errorf("参数为空"))
	}
	if argument.UserID < 1 {
		return nil, business.NewError(errors.InputInvalid, fmt.Errorf("用户ID'%d'无效", argument.UserID))
	}

	dbEntity := &sqldb.VerifiedDoctorUser{}
	dbFilter := &sqldb.VerifiedDoctorUserFilter{UserID: argument.UserID}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	err := s.sqlDatabase.SelectOne(dbEntity, sqlFilter)
	if err != nil {
		if s.sqlDatabase.IsNoRows(err) {
			return nil, business.NewError(errors.NotExist, err)
		} else {
			return nil, business.NewError(errors.InternalError, err)
		}
	}

	result := &doctor.VerifiedDoctorUserEx{}
	dbEntity.CopyToEx(result)

	if len(result.OrgCode) > 0 {
		result.OrgName = s.getOrgName(result.OrgCode)
	}

	return result, nil
}
func (s *Verified) RightIDList(argument *doctor.DoctorAddedRightFilter) ([]*doctor.DoctorAddedRight, business.Error) {
	results := make([]*doctor.DoctorAddedRight, 0)
	if argument.UserID < 1 {
		return nil, business.NewError(errors.InputInvalid, fmt.Errorf("用户ID'%d'无效", argument.UserID))
	}

	dbEntity := &sqldb.DoctorAddedRight{}
	var dbFilter *sqldb.DoctorAddedRightFilter = nil
	if argument != nil {
		dbFilter = &sqldb.DoctorAddedRightFilter{}
		dbFilter.CopyFrom(argument)
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)

	err := s.sqlDatabase.SelectList(dbEntity, func() {
		result := &doctor.DoctorAddedRight{}
		dbEntity.CopyTo(result)
		results = append(results, result)
	}, nil, sqlFilter)

	if err != nil {
		return results, business.NewError(errors.InternalError, err)
	}

	return results, nil
}
func (s *Verified) SaveRightIDList(argument *doctor.DoctorAddedRight) (uint64, business.Error) {

	if argument == nil {
		return 0, business.NewError(errors.InputError, fmt.Errorf("参数为空"))
	}
	if argument.UserID < 1 {
		return 0, business.NewError(errors.InputInvalid, fmt.Errorf("用户ID'%d'无效", argument.UserID))
	}
	dbEntity2 := &sqldb.DoctorAddedRight{}
	dbFilter := &sqldb.DoctorAddedRightFilter{UserID: argument.UserID}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	if argument.RightID == 0 {
		_, err := s.sqlDatabase.Delete(dbEntity2, sqlFilter)
		if err != nil {
			return 0, business.NewError(errors.InternalError, err)
		}
		return argument.UserID, nil
	} else {
		results := make([]*doctor.DoctorAddedRight, 0)
		dbEntity := &sqldb.DoctorAddedRight{}
		err := s.sqlDatabase.SelectList(dbEntity, func() {
			result := &doctor.DoctorAddedRight{}
			dbEntity.CopyTo(result)
			results = append(results, result)
		}, nil, sqlFilter)
		if err != nil {
			return 0, business.NewError(errors.InternalError, err)
		}
		for _, v := range results {
			if v.RightID == argument.RightID {
				return 0, business.NewError(errors.InputInvalid, fmt.Errorf("用户已拥有该权限"))
			}
		}
		_, e := s.sqlDatabase.InsertSelective(&sqldb.DoctorAddedRight{UserID: argument.UserID, RightID: argument.RightID})
		if e != nil {
			return 0, business.NewError(errors.InternalError, err)
		}
	}
	return argument.UserID, nil
}
