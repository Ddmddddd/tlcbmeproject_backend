package impl

import (
	"tlcbme_project/business"
	"tlcbme_project/business/doctor/api"
	"tlcbme_project/data/entity/sqldb"
	"tlcbme_project/data/model"
	"tlcbme_project/data/model/doctor"
	"tlcbme_project/data/model/manage"
	"tlcbme_project/database"
	"tlcbme_project/database/memory"
	"tlcbme_project/errors"
	"tlcbme_project/server/notify"
	"fmt"
	"github.com/ktpswjz/httpserver/types"
	"time"
)

type RiskAssess struct {
	base
}

func NewRiskAssess(log types.Log, sqlDatabase database.SqlDatabase, memoryToken memory.Token, notifyChannels notify.ChannelCollection) api.RiskAssess {
	instance := &RiskAssess{}
	instance.SetLog(log)
	instance.sqlDatabase = sqlDatabase
	instance.memoryToken = memoryToken
	instance.notifyChannels = notifyChannels

	return instance
}

func (s *RiskAssess) SaveRecord(patientIndex *doctor.ManagedPatientIndex, argument *manage.InputDataRiskAssess) business.Error {
	if patientIndex == nil {
		return business.NewError(errors.InternalError, fmt.Errorf("参数（patientIndex）为空"))
	}
	if argument == nil {
		return business.NewError(errors.InternalError, fmt.Errorf("参数（argument）为空"))
	}

	sqlAccess, err := s.sqlDatabase.NewAccess(true)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}
	defer sqlAccess.Close()

	now := time.Now()
	dbEntity := &sqldb.RiskAssessRecord{}
	dbEntity.PatientID = patientIndex.PatientID
	dbEntity.Level = uint64(argument.Level)
	dbEntity.Name = &argument.Name
	dbEntity.Memo = &argument.Memo
	dbEntity.ReceiveDateTime = &now
	if argument.NextSchedule != nil {
		nextSchedule := time.Time(*argument.NextSchedule)
		dbEntity.NextSchedule = &nextSchedule
	}

	_, err = sqlAccess.InsertSelective(dbEntity)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}

	err = sqlAccess.Commit()
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}

	return nil
}

func (s *RiskAssess) SearchRecordPage(pageIndex, pageSize uint64, filter *doctor.RiskAssessRecordFilter) (*model.PageResult, business.Error) {
	sqlAccess, err := s.sqlDatabase.NewAccess(false)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}
	defer sqlAccess.Close()

	sqlFilters := make([]database.SqlFilter, 0)
	if filter != nil {
		dbFilter := &sqldb.RiskAssessRecordFilter{}
		dbFilter.CopyFrom(filter)
		sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
		sqlFilters = append(sqlFilters, sqlFilter)
	}

	dbOrder := &sqldb.RiskAssessRecordOrder{}
	dbEntity := &sqldb.RiskAssessRecord{}
	pageResult := &model.PageResult{}
	datas := make([]*doctor.RiskAssessRecordEx, 0)
	err = sqlAccess.SelectPage(dbEntity, func(total, page, size, index uint64) {
		pageResult.Total = total
		pageResult.Count = page
		pageResult.Size = size
		pageResult.Index = index
	}, func() {
		data := &doctor.RiskAssessRecordEx{}
		dbEntity.CopyToEx(data)
		datas = append(datas, data)
	}, pageSize, pageIndex, dbOrder, sqlFilters...)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}
	pageResult.Data = datas

	return pageResult, nil
}
