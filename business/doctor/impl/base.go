package impl

import (
	"tlcbme_project/business"
	"tlcbme_project/data/entity/sqldb"
	"tlcbme_project/data/model/doctor"
	"tlcbme_project/database"
	"tlcbme_project/database/memory"
	"tlcbme_project/errors"
	"tlcbme_project/server/config"
	"tlcbme_project/server/notify"
	"fmt"
	"github.com/ktpswjz/httpserver/types"
	"time"
)

type base struct {
	types.Base

	cfg            *config.Config
	sqlDatabase    database.SqlDatabase
	memoryToken    memory.Token
	client         business.Client
	notifyChannels notify.ChannelCollection
}

func (s *base) getOrgName(code string) string {
	dbEntity := &sqldb.OrgDictName{}
	dbFilter := &sqldb.OrgDictFilter{OrgCode: code}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	err := s.sqlDatabase.SelectOne(dbEntity, sqlFilter)
	if err != nil {
		return ""
	}

	return dbEntity.OrgName
}

func (s *base) getPatientMaster(patientId uint64) (*sqldb.ManagedPatientIndex, error) {
	dbEntity := &sqldb.ManagedPatientIndex{}
	dbFilter := &sqldb.ManagedPatientIndexFilterBase{PatientID: &patientId}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	err := s.sqlDatabase.SelectOne(dbEntity, sqlFilter)
	if err != nil {
		return nil, err
	}

	return dbEntity, nil
}

func (s *base) getDoctorName(doctorID uint64) (string, error) {
	dbEntity := &sqldb.DoctorUserBaseInfo{}
	dbFilter := &sqldb.DoctorUserBaseInfoFilter{UserID: doctorID}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	err := s.sqlDatabase.SelectOne(dbEntity, sqlFilter)
	if err != nil {
		return "", err
	}

	return dbEntity.Name, nil
}

func (s base) WriteNotify(message *notify.Message) {
	if s.notifyChannels == nil {
		return
	}

	go func(msg *notify.Message) {
		s.notifyChannels.Write(message)
	}(message)
}

func (s *base) saveHtnManageDetail(sqlAccess database.SqlAccess, patientId, manageLevel uint64, startTime *time.Time) error {
	dbEntity := &sqldb.HtnManageDetail{}
	dbEntity.PatientID = patientId
	dbEntity.ManageLevel = manageLevel
	dbEntity.ManageLevelStartDateTime = startTime

	_, err := sqlAccess.InsertSelective(dbEntity)
	if err != nil {
		return err
	}
	s.WriteNotify(&notify.Message{
		BusinessID: notify.BusinessDoctor,
		NotifyID:   notify.DoctorUpdateAssessment,
		Time:       types.Time(time.Now()),
		Data:       patientId,
	})

	return nil
}
func (s *base) saveDmManageDetail(sqlAccess database.SqlAccess, patientId, manageLevel uint64, startTime *time.Time) error {
	dbEntity := &sqldb.DmManageDetail{}
	dbEntity.PatientID = patientId
	dbEntity.ManageLevel = manageLevel
	dbEntity.ManageLevelStartDateTime = startTime

	_, err := sqlAccess.InsertSelective(dbEntity)
	if err != nil {
		return err
	}
	s.WriteNotify(&notify.Message{
		BusinessID: notify.BusinessDoctor,
		NotifyID:   notify.DoctorUpdateAssessment,
		Time:       types.Time(time.Now()),
		Data:       patientId,
	})

	return nil
}
func (s *base) uploadDataToPlatform(patientId uint64, dataType string, needPatientName, needDoctorName bool, time *types.Time) (uint64, business.Error) {
	name := "未知"
	dbEntity := &sqldb.ViewPatientOrg{}
	dbFilter := &sqldb.ViewPatientOrgFilterBase{PatientID: &patientId}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	err := s.sqlDatabase.SelectOne(dbEntity, sqlFilter)
	if err != nil {
		return 0, business.NewError(errors.InternalError, err)
	}
	uploadData := &doctor.UploadData{}
	dbEntity.CopyToExt(uploadData)
	if !needDoctorName {
		uploadData.DoctorName = &name
	}
	if !needPatientName {
		uploadData.PatientName = name
	}
	uploadData.DataType = dataType
	uploadData.Time = string(time.String())
	uri := s.cfg.Platform.Providers.Api.Uri.UploadData
	result, err := s.client.PlatformBusinessPost("慢病系统数据监测资源", patientId, uri, uploadData)
	if err != nil {
		return 0, business.NewError(errors.InternalError, err)
	}
	if result.Code != 200 || result.Msg != "" {
		return 0, business.NewError(errors.InternalError, fmt.Errorf("%d-%s", result.Code, result.Msg))
	}
	return patientId, nil
}
