package impl

import (
	"tlcbme_project/business"
	"tlcbme_project/business/doctor/api"
	"tlcbme_project/data/entity/sqldb"
	"tlcbme_project/data/model/doctor"
	"tlcbme_project/database"
	"tlcbme_project/database/memory"
	"tlcbme_project/errors"
	"fmt"
	"github.com/ktpswjz/httpserver/types"
)

type UserBasicInfo struct {
	base
}

func NewUserBasicInfo(log types.Log, sqlDatabase database.SqlDatabase, memoryToken memory.Token) api.UserBasicInfo {
	instance := &UserBasicInfo{}
	instance.SetLog(log)
	instance.sqlDatabase = sqlDatabase
	instance.memoryToken = memoryToken

	return instance
}

func (s *UserBasicInfo) Save(argument *doctor.DoctorUserBaseInfoEdit) (uint64, business.Error) {
	if argument == nil {
		return 0, business.NewError(errors.InputError, fmt.Errorf("参数为空"))
	}
	if argument.UserID < 1 {
		return 0, business.NewError(errors.InputInvalid, fmt.Errorf("用户ID'%d'无效", argument.UserID))
	}

	dbEntityCheck := &sqldb.DoctorUserBaseInfoFilter{}
	dbFilter := &sqldb.DoctorUserBaseInfoFilter{UserID: argument.UserID}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	err := s.sqlDatabase.SelectOne(dbEntityCheck, sqlFilter)
	if err != nil {
		if s.sqlDatabase.IsNoRows(err) {
			dbEntity := &sqldb.DoctorUserBaseInfo{}
			dbEntity.CopyFromEdit(argument)
			_, err = s.sqlDatabase.InsertSelective(dbEntity)
			if err != nil {
				return 0, business.NewError(errors.InternalError, err)
			}
		} else {
			return 0, business.NewError(errors.InternalError, err)
		}
	} else {
		dbEntityEdit := &sqldb.DoctorUserBaseInfoEdit{}
		dbEntityEdit.CopyFrom(argument)
		_, err = s.sqlDatabase.UpdateSelective(dbEntityEdit, sqlFilter)
		if err != nil {
			return 0, business.NewError(errors.InternalError, err)
		}
	}

	return argument.UserID, nil
}

func (s *UserBasicInfo) Detail(argument *doctor.DoctorUserBaseInfoFilter) (*doctor.DoctorUserBaseInfoEx, business.Error) {
	if argument == nil {
		return nil, business.NewError(errors.InputError, fmt.Errorf("参数为空"))
	}
	if argument.UserID < 1 {
		return nil, business.NewError(errors.InputInvalid, fmt.Errorf("用户ID'%d'无效", argument.UserID))
	}

	dbEntity := &sqldb.DoctorUserBaseInfo{}
	dbFilter := &sqldb.DoctorUserBaseInfoFilter{UserID: argument.UserID}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	err := s.sqlDatabase.SelectOne(dbEntity, sqlFilter)
	if err != nil {
		if s.sqlDatabase.IsNoRows(err) {
			return nil, business.NewError(errors.NotExist, err)
		} else {
			return nil, business.NewError(errors.InternalError, err)
		}
	}

	result := &doctor.DoctorUserBaseInfoEx{}
	dbEntity.CopyToEx(result)

	return result, nil
}
