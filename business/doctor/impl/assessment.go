package impl

import (
	"tlcbme_project/business"
	"tlcbme_project/business/doctor/api"
	"tlcbme_project/data/entity/sqldb"
	"tlcbme_project/data/enum"
	"tlcbme_project/data/model"
	"tlcbme_project/data/model/doctor"
	"tlcbme_project/data/model/manage"
	"tlcbme_project/database"
	"tlcbme_project/database/memory"
	"tlcbme_project/errors"
	"tlcbme_project/server/config"
	"tlcbme_project/server/notify"
	"fmt"
	"github.com/ktpswjz/httpserver/types"
	"strings"
	"time"
)

type Assessment struct {
	base
}

func NewAssessment(cfg *config.Config, log types.Log, sqlDatabase database.SqlDatabase, memoryToken memory.Token, notifyChannels notify.ChannelCollection, client business.Client) api.Assessment {
	instance := &Assessment{}
	instance.SetLog(log)
	instance.cfg = cfg
	instance.sqlDatabase = sqlDatabase
	instance.memoryToken = memoryToken
	instance.notifyChannels = notifyChannels
	instance.client = client

	return instance
}

func (s *Assessment) CreateAssessmentRecord(argument *doctor.AssessmentRecordCreateEx) (uint64, uint64, business.Error) {
	sqlAccess, err := s.sqlDatabase.NewAccess(true)
	if err != nil {
		return 0, 0, business.NewError(errors.InternalError, err)
	}
	defer sqlAccess.Close()
	if argument == nil {
		return 0, 0, business.NewError(errors.InputError, fmt.Errorf("argument is nill"))
	}
	_, err = s.getPatientMaster(argument.PatientID)
	if err != nil {
		if sqlAccess.IsNoRows(err) {
			return 0, 0, business.NewError(errors.NotExist, fmt.Errorf("患者不存在:%s", err.Error()))
		} else {
			return 0, 0, business.NewError(errors.InternalError, err)
		}
	}

	providerType := config.ManagementProviderTypeUnknown
	uri := ""
	if strings.Contains(argument.AssessmentName, "高血压") {
		providerType = config.ManagementProviderTypeHbp
		uri = s.cfg.Management.Providers.Hbp.Api.Uri.UploadAssess
	} else {
		providerType = config.ManagementProviderTypeDm
		uri = s.cfg.Management.Providers.Dm.Api.Uri.UploadAssess
	}
	assess := &manage.PatientHbpAssessmentInput{
		PatientID: fmt.Sprint(argument.PatientID),
		Data:      argument.Content,
	}
	result, err := s.client.ManageBusinessPost("上传患者评估数据", argument.PatientID, providerType, uri, assess)
	if err != nil {
		return 0, 0, business.NewError(errors.InternalError, err)
	}
	if result.Code != 0 {
		return 0, 0, business.NewError(errors.InternalError, fmt.Errorf("%d-%s", result.Code, result.Message))
	}

	resultData := &manage.InputDataRiskAssessOutput{}
	err = result.GetData(resultData)
	if err != nil {
		return 0, 0, business.NewError(errors.InternalError, err)
	}
	if strings.Contains(argument.AssessmentName, "高血压") {
		argument.Summary = enum.RiskAssessLevels.Value(resultData.RiskAssess.Level)
	} else {
		argument.Summary = enum.DmRiskAssessLevels.Value(resultData.RiskAssess.Level)
		argument.Others = resultData.Others
	}

	now := time.Now()
	dbEntity := &sqldb.AssessmentRecord{}
	dbEntity.CopyFromCreateEx(argument)
	dbEntity.AssessDateTime = &now

	sno, err := sqlAccess.InsertSelective(dbEntity)
	if err != nil {
		return 0, 0, business.NewError(errors.InternalError, err)
	}
	weightSno := uint64(0)
	if argument.Weight != 0 {
		now := time.Now()

		dbEntityWeight := &sqldb.WeightRecord{
			PatientID:       argument.PatientID,
			Weight:          argument.Weight,
			InputDateTime:   &now,
			MeasureDateTime: &now,
			Height:          &argument.Height,
			Waistline:       &argument.Waistline,
		}
		if argument.Height == 0 {
			dbEntityWeight.Height = nil
		}
		weightSno, err = sqlAccess.InsertSelective(dbEntityWeight)
		if err != nil {
			return 0, 0, business.NewError(errors.InternalError, err)
		}

		weightEntity := &sqldb.PatientUserBaseInfoUpdateWeight{
			Weight:    &argument.Weight,
			Height:    &argument.Height,
			Waistline: &argument.Waistline,
			UserID:    argument.PatientID,
		}
		if argument.Height == 0 {
			weightEntity.Height = nil
		}
		if argument.Waistline == 0 {
			weightEntity.Waistline = nil
		}
		dbFilterAlert := &sqldb.PatientUserBaseInfoUserIdFilter{
			UserID: argument.PatientID,
		}
		sqlFilter := s.sqlDatabase.NewFilter(dbFilterAlert, false, false)
		_, err = sqlAccess.UpdateSelective(weightEntity, sqlFilter)
		if err != nil {
			return 0, 0, business.NewError(errors.InternalError, err)
		}
	}
	err = sqlAccess.Commit()
	if err != nil {
		return 0, 0, business.NewError(errors.InternalError, err)
	}

	return sno, weightSno, nil
}

func (s *Assessment) UpdateAssessmentRecord(argument *doctor.AssessmentRecordUpdate) (uint64, business.Error) {
	if argument == nil {
		return 0, business.NewError(errors.InputError, fmt.Errorf("arument is nill"))
	}

	dbEntity := &sqldb.AssessmentRecordUpdate{}
	dbEntity.CopyFrom(argument)

	sno, err := s.sqlDatabase.InsertSelective(dbEntity)
	if err != nil {
		return 0, business.NewError(errors.InternalError, err)
	}

	s.WriteNotify(&notify.Message{
		BusinessID: notify.BusinessDoctor,
		NotifyID:   notify.DoctorUpdateAssessment,
		Time:       types.Time(time.Now()),
		Data:       argument.SerialNo,
	})

	return sno, nil
}

func (s *Assessment) SearchAssessmentRecordOne(filter *doctor.AssessmentRecordFilter) (*doctor.AssessmentRecord, business.Error) {
	sqlAccess, err := s.sqlDatabase.NewAccess(false)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}
	defer sqlAccess.Close()

	sqlFilters := make([]database.SqlFilter, 0)
	if filter != nil {
		dbFilter := &sqldb.AssessmentRecordFilter{}
		dbFilter.CopyFrom(filter)
		sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
		sqlFilters = append(sqlFilters, sqlFilter)
	}

	dbEntity := &sqldb.AssessmentRecord{}
	err = sqlAccess.SelectOne(dbEntity, sqlFilters...)
	if err != nil {
		if s.sqlDatabase.IsNoRows(err) {
			return nil, business.NewError(errors.NotExist, err)
		} else {
			return nil, business.NewError(errors.InternalError, err)
		}
	}

	modelEntity := &doctor.AssessmentRecord{}
	dbEntity.CopyTo(modelEntity)

	return modelEntity, nil
}

func (s *Assessment) SearchAssessmentRecordPage(pageIndex, pageSize uint64, filter *doctor.AssessmentRecordFilter) (*model.PageResult, business.Error) {
	sqlAccess, err := s.sqlDatabase.NewAccess(false)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}
	defer sqlAccess.Close()

	sqlFilters := make([]database.SqlFilter, 0)
	if filter != nil {
		dbFilter := &sqldb.AssessmentRecordFilter{}
		dbFilter.CopyFrom(filter)
		sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
		sqlFilters = append(sqlFilters, sqlFilter)
	}

	dbOrder := &sqldb.AssessmentRecordOrder{}
	dbEntity := &sqldb.AssessmentRecordListItem{}
	pageResult := &model.PageResult{}
	datas := make([]*doctor.AssessmentRecordListItem, 0)
	err = sqlAccess.SelectPage(dbEntity, func(total, page, size, index uint64) {
		pageResult.Total = total
		pageResult.Count = page
		pageResult.Size = size
		pageResult.Index = index
	}, func() {
		data := &doctor.AssessmentRecordListItem{}
		dbEntity.CopyTo(data)
		datas = append(datas, data)
	}, pageSize, pageIndex, dbOrder, sqlFilters...)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}
	pageResult.Data = datas

	return pageResult, nil
}

func (s *Assessment) SaveRecord(patientIndex *doctor.ManagedPatientIndex, argument *manage.InputDataRiskAssess) business.Error {
	if patientIndex == nil {
		return business.NewError(errors.InternalError, fmt.Errorf("参数（patientIndex）为空"))
	}
	if argument == nil {
		return business.NewError(errors.InternalError, fmt.Errorf("参数（argument）为空"))
	}

	now := time.Now()
	dbEntity := &sqldb.AssessmentRecord{
		PatientID:      patientIndex.PatientID,
		AssessDateTime: &now,
	}

	if argument.Level == 0 {
		providerType := config.ManagementProviderTypeHbp
		uri := s.cfg.Management.Providers.Hbp.Api.Uri.UploadAssess
		assess := &manage.PatientHbpAssessmentInput{
			PatientID: fmt.Sprint(patientIndex.PatientID),
		}
		result, err := s.client.ManageBusinessPost("上传患者评估数据", patientIndex.PatientID, providerType, uri, assess)
		if err != nil {
			return business.NewError(errors.InternalError, err)
		}
		if result.Code != 0 {
			return business.NewError(errors.InternalError, fmt.Errorf("%d-%s", result.Code, result.Message))
		}

		resultData := &manage.InputDataRiskAssessOutput{}
		err = result.GetData(resultData)
		if err != nil {
			return business.NewError(errors.InternalError, err)
		}
		dbEntity.AssessmentName = "高血压危险分层评估"
		dbEntity.Summary = enum.RiskAssessLevels.Value(resultData.RiskAssess.Level)
	} else {
		dbEntity.AssessmentName = argument.Name
		dbEntity.Summary = enum.RiskAssessLevels.Value(argument.Level)
	}

	sqlAccess, err := s.sqlDatabase.NewAccess(false)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}
	defer sqlAccess.Close()

	_, err = sqlAccess.InsertSelective(dbEntity)
	if err != nil {
		return business.NewError(errors.InternalError, err)
	}

	return nil
}
