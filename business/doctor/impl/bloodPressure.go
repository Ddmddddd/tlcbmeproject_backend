package impl

import (
	"tlcbme_project/business"
	"tlcbme_project/business/doctor/api"
	"tlcbme_project/data/entity/sqldb"
	"tlcbme_project/data/model"
	"tlcbme_project/data/model/doctor"
	"tlcbme_project/database"
	"tlcbme_project/database/memory"
	"tlcbme_project/errors"
	"tlcbme_project/server/notify"
	"fmt"
	"github.com/ktpswjz/httpserver/types"
	"time"
)

type BloodPressure struct {
	base
}

func NewBloodPressure(log types.Log, sqlDatabase database.SqlDatabase, memoryToken memory.Token, notifyChannels notify.ChannelCollection) api.BloodPressure {
	instance := &BloodPressure{}
	instance.SetLog(log)
	instance.sqlDatabase = sqlDatabase
	instance.memoryToken = memoryToken
	instance.notifyChannels = notifyChannels

	return instance
}

func (s *BloodPressure) SearchTrendSingleList(filter *doctor.BloodPressureRecordTrendFilter) (*doctor.BloodPressureRecordTrendStat, business.Error) {
	totalCount := filter.MaxCount
	halfCount := int(totalCount / 2)
	if halfCount < 1 {
		return nil, business.NewError(errors.InputInvalid, fmt.Errorf("count must be grater then 1"))
	}

	sqlAccess, err := s.sqlDatabase.NewAccess(false)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}
	defer sqlAccess.Close()

	pageIndex := uint64(1)
	pageSize := uint64(totalCount)
	dbEntity := &sqldb.BloodPressureRecordTrend{}

	ups := make([]*doctor.BloodPressureRecordTrend, 0)
	dbOrderUp := &sqldb.BloodPressureRecordUpOrder{}
	dbFilter := &sqldb.BloodPressureRecordTrendFilter{
		PatientID: filter.PatientID,
	}
	if filter.MeasureDateTime != nil {
		measureDateTime := time.Time(*filter.MeasureDateTime)
		dbFilter.MeasureDateTimeStart = &measureDateTime
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	err = sqlAccess.SelectPage(dbEntity, func(total, page, size, index uint64) {
	}, func() {
		data := &doctor.BloodPressureRecordTrend{}
		dbEntity.CopyTo(data)
		ups = append(ups, data)
	}, pageSize, pageIndex, dbOrderUp, sqlFilter)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}

	downs := make([]*doctor.BloodPressureRecordTrend, 0)
	dbOrderDown := &sqldb.BloodPressureRecordLastOrder{}
	if filter.MeasureDateTime != nil {
		measureDateTime := time.Time(*filter.MeasureDateTime)
		dbFilter.MeasureDateTimeStart = nil
		dbFilter.MeasureDateTimeEnd = &measureDateTime
	}
	sqlFilter = s.sqlDatabase.NewFilter(dbFilter, false, false)
	err = sqlAccess.SelectPage(dbEntity, func(total, page, size, index uint64) {
	}, func() {
		data := &doctor.BloodPressureRecordTrend{}
		dbEntity.CopyTo(data)
		downs = append(downs, data)
	}, pageSize, pageIndex, dbOrderDown, sqlFilter)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}

	upCount := len(ups)
	downCount := len(downs)
	if upCount < 1 && downCount < 1 {
		return nil, business.NewError(errors.NotExist, nil)
	}

	trend := &doctor.BloodPressureRecordTrendStat{
		Records: make([]*doctor.BloodPressureRecordTrend, 0),
	}
	if upCount+downCount <= totalCount {
		index := 0
		for downIndex := downCount - 1; downIndex >= 0; downIndex-- {
			trend.Records = append(trend.Records, downs[downIndex])
			index++
		}
		trend.CurrentIndex = index
		for upIndex := 0; upIndex < upCount; upIndex++ {
			trend.Records = append(trend.Records, ups[upIndex])
			index++
		}
	} else {
		index := 0
		if downCount <= halfCount {
			for downIndex := downCount - 1; downIndex >= 0; downIndex-- {
				trend.Records = append(trend.Records, downs[downIndex])
				index++
			}
			trend.CurrentIndex = index
			for upIndex := 0; upIndex < totalCount-downCount; upIndex++ {
				trend.Records = append(trend.Records, ups[upIndex])
				index++
			}
		} else {
			if upCount < halfCount {
				for downIndex := downCount - upCount - 1; downIndex >= 0; downIndex-- {
					trend.Records = append(trend.Records, downs[downIndex])
					index++
				}
				trend.CurrentIndex = index
				for upIndex := 0; upIndex < totalCount-index; upIndex++ {
					trend.Records = append(trend.Records, ups[upIndex])
					index++
				}
			} else {
				for downIndex := downCount - halfCount - 1; downIndex >= 0; downIndex-- {
					trend.Records = append(trend.Records, downs[downIndex])
					index++
				}
				trend.CurrentIndex = index
				for upIndex := 0; upIndex <= totalCount-index; upIndex++ {
					trend.Records = append(trend.Records, ups[upIndex])
					index++
				}
			}
		}
	}

	trend.Stat()

	return trend, nil
}

func (s *BloodPressure) SearchTrendSingleRange(filter *doctor.BloodPressureRecordDataFilterEx) (*doctor.BloodPressureRecordTrendStat, business.Error) {
	sqlAccess, err := s.sqlDatabase.NewAccess(false)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}
	defer sqlAccess.Close()

	trend := &doctor.BloodPressureRecordTrendStat{
		Records: make([]*doctor.BloodPressureRecordTrend, 0),
	}

	dbFilter := &sqldb.BloodPressureRecordTrendFilter{
		PatientID: filter.PatientID,
	}
	if filter.MeasureStartDate != nil {
		dbFilter.MeasureDateTimeStart = filter.MeasureStartDate.ToDate(0)
	}
	if filter.MeasureEndDate != nil {
		dbFilter.MeasureDateTimeEnd = filter.MeasureEndDate.ToDate(1)
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	dbOrder := &sqldb.BloodPressureRecordUpOrder{}
	dbEntity := &sqldb.BloodPressureRecordTrend{}
	err = s.sqlDatabase.SelectList(dbEntity, func() {
		data := &doctor.BloodPressureRecordTrend{}
		dbEntity.CopyTo(data)
		trend.Records = append(trend.Records, data)
	}, dbOrder, sqlFilter)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}

	trend.Stat()

	return trend, nil
}

func (s *BloodPressure) SearchTableSingleRange(pageIndex, pageSize uint64, filter *doctor.BloodPressureRecordDataFilterEx) (*model.PageResult, business.Error) {
	dbFilter := &sqldb.BloodPressureRecordDataFilter{}
	dbFilter.CopyFrom(filter)
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	dbOrder := &sqldb.BloodPressureRecordLastOrder{}
	dbEntity := &sqldb.BloodPressureRecordCreate{}

	pageResult := &model.PageResult{}
	datas := make([]*doctor.BloodPressureRecordCreate, 0)
	err := s.sqlDatabase.SelectPage(dbEntity, func(total, page, size, index uint64) {
		pageResult.Total = total
		pageResult.Count = page
		pageResult.Size = size
		pageResult.Index = index
	}, func() {
		data := &doctor.BloodPressureRecordCreate{}
		dbEntity.CopyTo(data)
		datas = append(datas, data)
	}, pageSize, pageIndex, dbOrder, sqlFilter)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}
	pageResult.Data = datas

	return pageResult, nil
}

func (s *BloodPressure) SearchRecordLastOne(filter *doctor.BloodPressureRecordPatientFilter) (*doctor.BloodPressureRecord, business.Error) {
	sqlAccess, err := s.sqlDatabase.NewAccess(false)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}
	defer sqlAccess.Close()

	sqlFilters := make([]database.SqlFilter, 0)
	if filter != nil {
		dbFilter := &sqldb.BloodPressureRecordPatientFilter{
			PatientID: filter.PatientID,
		}
		sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
		sqlFilters = append(sqlFilters, sqlFilter)
	}

	dbOrder := &sqldb.BloodPressureRecordLastOrder{}
	dbEntity := &sqldb.BloodPressureRecord{}
	datas := make([]*doctor.BloodPressureRecord, 0)
	err = sqlAccess.SelectPage(dbEntity, func(total, page, size, index uint64) {
	}, func() {
		data := &doctor.BloodPressureRecord{}
		dbEntity.CopyTo(data)
		datas = append(datas, data)
	}, 1, 1, dbOrder, sqlFilters...)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}

	if len(datas) > 0 {
		return datas[0], nil
	}

	return nil, business.NewError(errors.NotExist, nil)
}
