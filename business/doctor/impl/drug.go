package impl

import (
	"tlcbme_project/business"
	"tlcbme_project/business/doctor/api"
	"tlcbme_project/data/entity/sqldb"
	"tlcbme_project/data/model/doctor"
	"tlcbme_project/database"
	"tlcbme_project/database/memory"
	"tlcbme_project/errors"
	"tlcbme_project/server/notify"
	"fmt"
	"github.com/ktpswjz/httpserver/types"
	"time"
)

type Drug struct {
	base
}

func NewDrug(log types.Log, sqlDatabase database.SqlDatabase, memoryToken memory.Token, notifyChannels notify.ChannelCollection) api.Drug {
	instance := &Drug{}
	instance.SetLog(log)
	instance.sqlDatabase = sqlDatabase
	instance.memoryToken = memoryToken
	instance.notifyChannels = notifyChannels

	return instance
}

func (s *Drug) SearchDrugUseRecordIndexList(filter *doctor.DrugUseRecordIndexFilter) ([]*doctor.DrugUseRecordIndexEx, business.Error) {
	sqlFilters := make([]database.SqlFilter, 0)
	if filter != nil {
		dbFilter := &sqldb.DrugUseRecordIndexFilter{}
		dbFilter.CopyFrom(filter)
		sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
		sqlFilters = append(sqlFilters, sqlFilter)
	}
	dbOrder := &sqldb.DrugUseRecordIndexOrder{}
	dbEntity := &sqldb.DrugUseRecordIndex{}

	results := make([]*doctor.DrugUseRecordIndexEx, 0)
	err := s.sqlDatabase.SelectList(dbEntity, func() {
		result := &doctor.DrugUseRecordIndexEx{}
		dbEntity.CopyToEx(result)
		results = append(results, result)
	}, dbOrder, sqlFilters...)

	if err != nil {
		return results, business.NewError(errors.InternalError, err)
	}

	return results, nil
}

func (s *Drug) SearchDrugUseRecordMonthGroupList(filter *doctor.DrugUseRecordFilter) ([]*doctor.DrugUseRecordMonthGroup, business.Error) {
	sqlFilters := make([]database.SqlFilter, 0)
	if filter != nil {
		dbFilter := &sqldb.DrugUseRecordFilter{}
		dbFilter.CopyFrom(filter)
		sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
		sqlFilters = append(sqlFilters, sqlFilter)
	}
	dbOrder := &sqldb.DrugRecordOrder{}
	dbEntity := &sqldb.DrugUseRecord{}

	results := make(doctor.DrugUseRecordMonthGroupCollection, 0)
	err := s.sqlDatabase.SelectList(dbEntity, func() {
		result := &doctor.DrugUseRecord{}
		dbEntity.CopyTo(result)
		year := dbEntity.UseDateTime.Year()
		month := int(dbEntity.UseDateTime.Month())
		group := results.GetGroup(year, month)
		if group == nil {
			group = &doctor.DrugUseRecordMonthGroup{
				Year:    year,
				Month:   month,
				Records: make([]*doctor.DrugUseRecord, 0),
			}
			results = append(results, group)
		}
		group.Records = append(group.Records, result)
	}, dbOrder, sqlFilters...)

	if err != nil {
		return results, business.NewError(errors.InternalError, err)
	}

	return results, nil
}

func (s *Drug) SearchDrugUseRecordIndexModifiedLogList(filter *doctor.DrugUseRecordIndexModifiedLogFilter) ([]*doctor.DrugUseRecordIndexModifiedLog, business.Error) {
	sqlFilters := make([]database.SqlFilter, 0)
	if filter != nil {
		dbFilter := &sqldb.DrugUseRecordIndexModifiedLogFilter{}
		dbFilter.CopyFrom(filter)
		sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
		sqlFilters = append(sqlFilters, sqlFilter)
	}
	dbOrder := &sqldb.DrugUseRecordIndexModifiedLogOrder{}
	dbEntity := &sqldb.DrugUseRecordIndexModifiedLog{}

	results := make([]*doctor.DrugUseRecordIndexModifiedLog, 0)
	err := s.sqlDatabase.SelectList(dbEntity, func() {
		result := &doctor.DrugUseRecordIndexModifiedLog{}
		dbEntity.CopyTo(result)
		results = append(results, result)
	}, dbOrder, sqlFilters...)

	if err != nil {
		return results, business.NewError(errors.InternalError, err)
	}

	return results, nil
}

func (s *Drug) CreateDrugUseRecordIndexModifiedLog(argument *doctor.DrugUseRecordIndexModifiedLogCreate) (uint64, business.Error) {
	if argument == nil {
		return 0, business.NewError(errors.InputError, fmt.Errorf("argument is nil"))
	}
	sqlAccess, err := s.sqlDatabase.NewAccess(true)
	if err != nil {
		return 0, business.NewError(errors.InternalError, err)
	}
	defer sqlAccess.Close()

	now := time.Now()
	dbEntity := &sqldb.DrugUseRecordIndexModifiedLog{}
	dbEntity.CopyFromCreate(argument)
	dbEntity.ModifyDateTime = &now
	sno, err := sqlAccess.InsertSelective(dbEntity)
	if err != nil {
		return 0, business.NewError(errors.InternalError, err)
	}

	if argument.DURISerialNo != nil {
		if argument.ModifyType == "调整用法" {
			dbEntityUpdate := &sqldb.DrugUseRecordIndexUpdateUsage{
				SerialNo:           *argument.DURISerialNo,
				Dosage:             &argument.Dosage,
				Freq:               &argument.Freq,
				IsModified:         1,
				LastModifyDateTime: &now,
			}
			_, err = sqlAccess.UpdateByPrimaryKey(dbEntityUpdate)
			if err != nil {
				return 0, business.NewError(errors.InternalError, err)
			}
		} else if argument.ModifyType == "停止使用" {
			dbEntityUpdate := &sqldb.DrugUseRecordIndexUpdateStatus{
				SerialNo:           *argument.DURISerialNo,
				Status:             1,
				IsModified:         1,
				LastModifyDateTime: &now,
			}
			_, err = sqlAccess.UpdateByPrimaryKey(dbEntityUpdate)
			if err != nil {
				return 0, business.NewError(errors.InternalError, err)
			}
		}
	}

	err = sqlAccess.Commit()
	if err != nil {
		return 0, business.NewError(errors.InternalError, err)
	}

	s.WriteNotify(&notify.Message{
		BusinessID: notify.BusinessDoctor,
		NotifyID:   notify.DoctorUpdateDrug,
		Time:       types.Time(time.Now()),
		Data:       argument.PatientID,
	})

	return sno, nil
}

func (s *Drug) CreateDrugUseRecordIndex(argument *doctor.DrugRecordForDoctorList, userName string) (uint64, business.Error) {
	if argument == nil {
		return 0, business.NewError(errors.InputError, fmt.Errorf("argument is nil"))
	}
	if len(argument.DrugList) == 0 {
		return 0, business.NewError(errors.InputError, fmt.Errorf("argument length is 0"))
	}
	now := time.Now()
	var index uint64
	sqlAccess, err := s.sqlDatabase.NewAccess(true)
	if err != nil {
		return 0, business.NewError(errors.InternalError, err)
	}
	defer sqlAccess.Close()

	for i := 0; i < len(argument.DrugList); i++ {
		entity := &doctor.DrugUseRecordIndex{}
		argument.DrugList[i].CreateBy = userName
		argument.DrugList[i].CopyTo(entity)
		dbFilter := &sqldb.DrugUseRecordIndexFilter{
			PatientID: &argument.DrugList[i].PatientID,
			DrugName:  argument.DrugList[i].DrugName,
		}

		dbEntity := &sqldb.DrugUseRecordIndex{}
		sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
		err = sqlAccess.SelectOne(dbEntity, sqlFilter)
		if err != nil {
			if !s.sqlDatabase.IsNoRows(err) {
				return 0, business.NewError(errors.InternalError, err)
			}
			dbEntity.CopyFrom(entity)
			sno, err := sqlAccess.InsertSelective(dbEntity)
			if err != nil {
				return 0, business.NewError(errors.InternalError, err)
			}
			dbEntity.SerialNo = sno
			index = sno
			modifyType := "新增用药记录"
			if entity.FirstUseDateTime != nil {
				time1 := time.Time(*entity.FirstUseDateTime)
				dbEntity.FirstUseDateTime = &time1
			}
			err = s.saveDrugLog(sqlAccess, dbEntity, modifyType)
			if err != nil {
				return 0, business.NewError(errors.InternalError, err)
			}
		} else {
			if dbEntity.Status != 0 {
				dbEntity.Status = 0
				dbEntity.Dosage = &entity.Dosage
				dbEntity.Freq = &entity.Freq
				modifyType := "新增用药记录"
				if entity.FirstUseDateTime != nil {
					time1 := time.Time(*entity.FirstUseDateTime)
					dbEntity.FirstUseDateTime = &time1
				}
				dbEntity.IsModified = 1
				dbEntity.LastModifyDateTime = &now

				_, err = sqlAccess.UpdateByPrimaryKey(dbEntity)
				if err != nil {
					return 0, business.NewError(errors.InternalError, err)
				}
				err = s.saveDrugLog(sqlAccess, dbEntity, modifyType)
				if err != nil {
					return 0, business.NewError(errors.InternalError, fmt.Errorf("DrugUseRecordIndexModifiedLog 新增失败：%s", err))
				}
			} else {
				if dbEntity.Freq == nil || dbEntity.Dosage == nil || entity.Freq != *dbEntity.Freq || entity.Dosage != *dbEntity.Dosage {
					dbEntity.Dosage = &entity.Dosage
					dbEntity.Freq = &entity.Freq
					dbEntity.IsModified = 1
					dbEntity.LastModifyDateTime = &now
					if entity.FirstUseDateTime != nil {
						time1 := time.Time(*entity.FirstUseDateTime)
						dbEntity.FirstUseDateTime = &time1
					}
					_, err = sqlAccess.UpdateByPrimaryKey(dbEntity)
					if err != nil {
						return 0, business.NewError(errors.InternalError, err)
					}
					err = s.saveDrugLog(sqlAccess, dbEntity, "调整用法")
					if err != nil {
						return 0, business.NewError(errors.InternalError, err)
					}
				}
			}
		}

	}
	err = sqlAccess.Commit()
	if err != nil {
		return 0, business.NewError(errors.InternalError, err)
	}
	return index, nil

}

func (s *Drug) saveDrugLog(sqlAccess database.SqlAccess, index *sqldb.DrugUseRecordIndex, modifyType string) error {
	now := time.Now()

	dbEntity := &sqldb.DrugUseRecordIndexModifiedLog{
		DURISerialNo:   &index.SerialNo,
		PatientID:      index.PatientID,
		DrugName:       index.DrugName,
		Dosage:         index.Dosage,
		Freq:           index.Freq,
		ModifyType:     modifyType,
		ModifyDateTime: &now,
	}
	_, err := sqlAccess.InsertSelective(dbEntity)
	if err != nil {
		return err
	}

	return nil
}
