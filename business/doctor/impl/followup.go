package impl

import (
	"tlcbme_project/business"
	"tlcbme_project/business/doctor/api"
	"tlcbme_project/data/entity/sqldb"
	"tlcbme_project/data/enum"
	"tlcbme_project/data/model"
	"tlcbme_project/data/model/doctor"
	"tlcbme_project/data/model/manage"
	"tlcbme_project/database"
	"tlcbme_project/database/memory"
	"tlcbme_project/errors"
	"tlcbme_project/server/config"
	"tlcbme_project/server/notify"
	"fmt"
	"github.com/ktpswjz/httpserver/types"
	"strings"
	"time"
)

type Followup struct {
	base
}

func NewFollowup(cfg *config.Config, log types.Log, sqlDatabase database.SqlDatabase, memoryToken memory.Token, notifyChannels notify.ChannelCollection, client business.Client) api.Followup {
	instance := &Followup{}
	instance.SetLog(log)
	instance.cfg = cfg
	instance.sqlDatabase = sqlDatabase
	instance.memoryToken = memoryToken
	instance.notifyChannels = notifyChannels
	instance.client = client
	return instance
}

func (s *Followup) SearchFollowupPlanPage(pageIndex, pageSize uint64, filter *doctor.ViewFollowupPlanFilterEx) (*model.PageResult, business.Error) {
	sqlAccess, err := s.sqlDatabase.NewAccess(false)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}
	defer sqlAccess.Close()

	var followStatus *uint64 = nil
	if filter != nil {
		if filter.FollowStatus != nil {
			status := *filter.FollowStatus
			followStatus = &status
		}
	}

	total, err := s.searchFollowupPlanCount(sqlAccess, filter, followStatus)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}
	size := pageSize
	if size < 1 {
		size = 1
	}
	pageCount := total / size
	if (total % size) != 0 {
		pageCount++
	}
	if pageIndex > pageCount {
		pageIndex = pageCount
	} else if pageIndex < 1 {
		pageIndex = 1
	}

	pageResult := &model.PageResult{
		Data: make([]*doctor.ViewFollowupPlanEx, 0),
	}
	pageResult.Total = total
	pageResult.Count = pageCount
	pageResult.Size = size
	pageResult.Index = pageIndex
	if total < 1 {
		pageResult.Extend = s.SearchFollowupPlayPageExtend(sqlAccess, filter)
		return pageResult, nil
	}

	dbEntity := &sqldb.ViewFollowupPlan{}
	sqlEntity := s.sqlDatabase.NewEntity()
	err = sqlEntity.Parse(dbEntity)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}
	sqlBuilder := s.sqlDatabase.NewBuilder()
	sqlBuilder.Reset()
	sqlBuilder.Select(sqlEntity.ScanFields(), false).From(sqlEntity.Name())
	s.buildFollowupPlanPageWhere(filter, followStatus, sqlBuilder)

	dbOrder := dbEntity.GetOrder(filter.Order)
	sqlEntityOrder := s.sqlDatabase.NewEntity()
	err = sqlEntityOrder.Parse(dbOrder)
	if err == nil {
		count := sqlEntityOrder.FieldCount()
		if count > 0 {
			sqlBuilder.Append(fmt.Sprintf("order by %s %s", sqlEntityOrder.Field(0).Name(), sqlEntityOrder.Field(0).Order()))

			for i := 1; i < count; i++ {
				sqlBuilder.Append(fmt.Sprintf(", %s %s", sqlEntityOrder.Field(i).Name(), sqlEntityOrder.Field(i).Order()))
			}
		}
	}
	startIndex := (pageIndex - 1) * size
	sqlBuilder.Append("LIMIT ?, ?", startIndex, size)

	query := sqlBuilder.Query()
	args := sqlBuilder.Args()
	rows, err := sqlAccess.Query(query, args...)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}
	defer rows.Close()

	datas := make([]*doctor.ViewFollowupPlanEx, 0)
	for rows.Next() {
		err = rows.Scan(sqlEntity.ScanArgs()...)
		if err != nil {
			return nil, business.NewError(errors.InternalError, err)
		}

		data := &doctor.ViewFollowupPlanEx{}
		dbEntity.CopyToEx(data)
		datas = append(datas, data)
	}
	pageResult.Data = datas

	count := &doctor.ViewFollowupPlanCount{
		Total:    0,
		Waiting:  0,
		Finished: 0,
		Ignored:  0,
	}
	if followStatus == nil {
		count.Total = pageResult.Total

		followStatusTmp := enum.FollowupPlanStatuses.Waiting().Key
		count.Waiting, err = s.searchFollowupPlanCount(sqlAccess, filter, &followStatusTmp)
		if err == nil {
			count.Finished = count.Total - count.Waiting
		}
		followStatusTmp = enum.FollowupPlanStatuses.Ignored().Key
		count.Ignored, err = s.searchFollowupPlanCount(sqlAccess, filter, &followStatusTmp)
		if err == nil {
			count.Finished -= count.Ignored
		}

	} else {
		count.Total, err = s.searchFollowupPlanCount(sqlAccess, filter, nil)
		if err == nil {
			if *followStatus == enum.FollowupPlanStatuses.Waiting().Key {
				count.Waiting = pageResult.Total

				followStatusTmp := enum.FollowupPlanStatuses.Ignored().Key
				count.Ignored, err = s.searchFollowupPlanCount(sqlAccess, filter, &followStatusTmp)

				count.Finished = count.Total - count.Waiting - count.Ignored
			} else if *followStatus == enum.FollowupPlanStatuses.Finished().Key {
				count.Finished = pageResult.Total

				followStatusTmp := enum.FollowupPlanStatuses.Ignored().Key
				count.Ignored, err = s.searchFollowupPlanCount(sqlAccess, filter, &followStatusTmp)

				count.Waiting = count.Total - count.Finished - count.Ignored
			} else if *followStatus == enum.FollowupPlanStatuses.Ignored().Key {
				count.Ignored = pageResult.Total

				followStatusTmp := enum.FollowupPlanStatuses.Finished().Key
				count.Finished, err = s.searchFollowupPlanCount(sqlAccess, filter, &followStatusTmp)

				count.Waiting = count.Total - count.Finished - count.Ignored
			}
		}
	}

	pageResult.Extend = count

	return pageResult, nil
}

func (s *Followup) SearchFollowupPlanCount(filter *doctor.ViewFollowupPlanFilterEx) (*doctor.ViewFollowupPlanCount, business.Error) {
	sqlAccess, err := s.sqlDatabase.NewAccess(false)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}
	defer sqlAccess.Close()

	totalCount, err := s.searchFollowupPlanCount(sqlAccess, filter, nil)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}
	count := &doctor.ViewFollowupPlanCount{
		Total:    totalCount,
		Waiting:  0,
		Finished: 0,
	}

	if totalCount > 0 {
		followStatus := enum.FollowupPlanStatuses.Waiting().Key
		waitingCount, err := s.searchFollowupPlanCount(sqlAccess, filter, &followStatus)
		if err != nil {
			return nil, business.NewError(errors.InternalError, err)
		}
		count.Waiting = waitingCount
		count.Finished = totalCount - waitingCount
	}

	return count, nil
}

func (s *Followup) SearchFollowupPlayPageExtend(sqlAccess database.SqlAccess, filter *doctor.ViewFollowupPlanFilterEx) *doctor.ViewFollowupPlanCount {
	count := &doctor.ViewFollowupPlanCount{
		Total:    0,
		Waiting:  0,
		Finished: 0,
		Ignored:  0,
	}

	total, err := s.searchFollowupPlanCount(sqlAccess, filter, nil)
	if err != nil {
		return count
	}
	count.Total = total

	status := enum.FollowupPlanStatuses.Waiting().Key
	count.Waiting, err = s.searchFollowupPlanCount(sqlAccess, filter, &status)
	if err != nil {
		return count
	}

	status = enum.FollowupPlanStatuses.Finished().Key
	count.Finished, err = s.searchFollowupPlanCount(sqlAccess, filter, &status)
	if err != nil {
		return count
	}
	count.Ignored = count.Total - count.Finished - count.Waiting

	return count
}

func (s *Followup) searchFollowupPlanCount(sqlAccess database.SqlAccess, filter *doctor.ViewFollowupPlanFilterEx, followStatus *uint64) (uint64, error) {
	dbEntity := &sqldb.ViewFollowupPlan{}
	sqlBuilder := s.sqlDatabase.NewBuilder()
	sqlBuilder.Reset()
	sqlBuilder.Select("COUNT(*)", false).From(dbEntity.TableName())

	if filter != nil {
		filter.FollowStatus = followStatus
	}
	s.buildFollowupPlanPageWhere(filter, followStatus, sqlBuilder)

	count := uint64(0)
	query := sqlBuilder.Query()
	row := sqlAccess.QueryRow(query, sqlBuilder.Args()...)
	err := row.Scan(&count)
	if err != nil {
		return 0, err
	}

	return count, nil
}

func (s *Followup) SaveFollowupRecord(argument *doctor.FollowupRecordSaveEx) (uint64, business.Error) {
	if argument == nil {
		return 0, business.NewError(errors.InputError, fmt.Errorf("参数为空"))
	}

	sqlAccess, err := s.sqlDatabase.NewAccess(true)
	if err != nil {
		return 0, business.NewError(errors.InternalError, err)
	}
	defer sqlAccess.Close()

	dbEntity := &sqldb.FollowupRecord{}
	dbEntity.CopyFromSaveEx(argument)

	dbFilter := &sqldb.FollowupRecordFilter{
		PatientID: &dbEntity.PatientID,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	followupTimes, err := sqlAccess.SelectCount(dbEntity, sqlFilter)
	if err != nil {
		return 0, business.NewError(errors.InternalError, err)
	}

	serialNo := dbEntity.SerialNo
	if serialNo > 0 {
		count, err := sqlAccess.UpdateSelectiveByPrimaryKey(dbEntity)
		if err != nil {
			return 0, business.NewError(errors.InternalError, err)
		}
		if count != 1 {
			return 0, business.NewError(errors.NotExist, fmt.Errorf("记录(%d)不存在", serialNo))
		}
	} else {
		sno, err := sqlAccess.InsertSelective(dbEntity)
		if err != nil {
			return 0, business.NewError(errors.InternalError, err)
		}
		serialNo = sno
		followupTimes += 1

		dbEntityAlert := &sqldb.AlertRecordFollowup{}
		dbEntityAlert.Status = enum.AlertStatuses.Processed().Key
		dbEntityAlert.ProcessMode = enum.AlertProcessModes.Followup().Key
		dbEntityAlert.FollowUpSerialNo = serialNo

		dbFilterAlert := &sqldb.AlertRecordFollowupFilter{
			PatientID: dbEntity.PatientID,
		}
		sqlFilter = s.sqlDatabase.NewFilter(dbFilterAlert, false, false)
		_, err = sqlAccess.UpdateSelective(dbEntityAlert, sqlFilter)
		if err != nil {
			return 0, business.NewError(errors.InternalError, err)
		}

		if dbEntity.FollowupPlanSerialNo != nil {
			now := time.Now()
			dbEntityPlan := &sqldb.FollowupPlanStatusUpdate{}
			dbEntityPlan.SerialNo = *dbEntity.FollowupPlanSerialNo
			dbEntityPlan.Status = enum.FollowupPlanStatuses.Finished().Key
			dbEntityPlan.UpdateTime = &now
			planStatus := uint64(0)
			dbFilterPlan := &sqldb.FollowupPlanFilter{
				PatientID: &argument.PatientID,
				Status:    &planStatus,
			}
			sqlFilter = s.sqlDatabase.NewFilter(dbFilterPlan, false, false)
			_, err := sqlAccess.UpdateSelective(dbEntityPlan, sqlFilter)
			if err != nil {
				return 0, business.NewError(errors.InternalError, err)
			}
		} else {
			// 删除随访计划
			planStatus := uint64(0)
			dbFilterPlan := &sqldb.FollowupPlanFilter{
				PatientID: &argument.PatientID,
				Status:    &planStatus,
			}
			if argument.FollowupTemplateCode == 1 {
				followType := "高血压常规随访"
				dbFilterPlan.FollowUpType = &followType
			} else if argument.FollowupTemplateCode == 2 {
				followType := "糖尿病常规随访"
				dbFilterPlan.FollowUpType = &followType
			} else if argument.FollowupTemplateCode == 3 {
				//dbFilterPlan.FollowUpType = "糖尿病及高血压常规随访"
			}
			if argument.FollowupTemplateCode > 0 {
				sqlFilter = s.sqlDatabase.NewFilter(dbFilterPlan, false, false)
				dbEntityPlan := &sqldb.FollowupPlan{}
				_, err = sqlAccess.Delete(dbEntityPlan, sqlFilter)
				if err != nil {
					return 0, business.NewError(errors.InternalError, err)
				}
			}

		}
	}

	dbEntityMaster := &sqldb.ManagedPatientIndexFollowupUpdate{}
	dbEntityMaster.PatientID = dbEntity.PatientID
	dbEntityMaster.FollowupTimes = followupTimes
	dbEntityMaster.LastFollowupDate = dbEntity.FollowupDateTime
	if argument.Status == enum.FollowupStatuses.Loss().Key {
		if argument.FailureReason == "亡故" {
			manageStatus := enum.ManageStatuses.TerminatedManagement().Key
			dbEntityMaster.ManageStatus = &manageStatus
			dbEntityMaster.ManageEndDateTime = dbEntity.FollowupDateTime
			manageEndReasonBuilder := strings.Builder{}
			manageEndReasonBuilder.WriteString("亡故(")
			if argument.DeathTime != nil {
				manageEndReasonBuilder.WriteString(argument.DeathTime.ToDate(0).Format("2006-01-02"))
				manageEndReasonBuilder.WriteString("，")
			}
			manageEndReasonBuilder.WriteString("argument.CauseOfDeath")
			manageEndReasonBuilder.WriteString(")")
			manageEndReason := manageEndReasonBuilder.String()
			dbEntityMaster.ManageEndReason = &manageEndReason

			err = s.saveHtnManageDetail(sqlAccess, argument.PatientID, 9, dbEntity.FollowupDateTime)
			if err != nil {
				return 0, business.NewError(errors.InternalError, err)
			}
		}
	}

	count, err := sqlAccess.UpdateSelectiveByPrimaryKey(dbEntityMaster)
	if err != nil {
		return 0, business.NewError(errors.InternalError, err)
	}
	if count != 1 {
		return 0, business.NewError(errors.InputInvalid, fmt.Errorf("患者(ID=%d)不存在", dbEntity.PatientID))
	}
	followupDateTime := types.Time(*dbEntity.FollowupDateTime)
	dataType := ""
	if argument.FollowupType == "预警干预" {
		dataType = "干预"
	} else {
		dataType = "随访"
	}
	go func(patientId uint64, dataType string, time *types.Time) {
		s.uploadDataToPlatform(patientId, dataType, false, true, time)
	}(dbEntity.PatientID, dataType, &followupDateTime)

	// 插入定时任务表
	dbEntityTiming := &sqldb.Timingtask{}
	dbEntityTiming.CopyFromFollow(argument)
	if argument.FollowupTemplateCode != 3 {
		dbEntityTiming.Tasktype = argument.FollowupTemplateCode
		_, err := sqlAccess.InsertSelective(dbEntityTiming)
		if err != nil {
			return 0, business.NewError(errors.InternalError, err)
		}
	} else {
		dbEntityTiming.Tasktype = 1
		_, err = sqlAccess.InsertSelective(dbEntityTiming)
		dbEntityTiming.Tasktype = 2
		_, err = sqlAccess.InsertSelective(dbEntityTiming)
		if err != nil {
			return 0, business.NewError(errors.InternalError, err)
		}
	}

	err = sqlAccess.Commit()
	if err != nil {
		return 0, business.NewError(errors.InternalError, err)
	}

	providerType := config.ManagementProviderTypeUnknown
	uri := ""
	flag := false
	// 1-高血压随访记录表, 2-糖尿病随访记录表, 3-慢阻肺随访记录表, 4-公卫高血压患者随访服务记录表, 5-公卫2型糖尿病患者随访服务记录表
	if argument.FollowupTemplateCode == 1 {
		providerType = config.ManagementProviderTypeHbp
		uri = s.cfg.Management.Providers.Hbp.Api.Uri.UploadFollowup
	} else if argument.FollowupTemplateCode == 2 {
		providerType = config.ManagementProviderTypeDm
		uri = s.cfg.Management.Providers.Dm.Api.Uri.UploadFollowup
	} else if argument.FollowupTemplateCode == 3 {
		providerType = config.ManagementProviderTypeHbp
		flag = true
	}
	if providerType != config.ManagementProviderTypeUnknown {
		uploadInfo := &manage.FollowupInfoInput{
			PatientID: fmt.Sprint(dbEntity.PatientID),
			Info: manage.FollowupInfo{
				FollowupMethod: dbEntity.FollowupMethod,
				Status:         dbEntity.Status,
				FailureInfo: manage.FollowupFailureInfo{
					FailureReason: dbEntity.FailureReason,
					DeathTime:     argument.DeathTime,
					CauseOfDeath:  dbEntity.CauseOfDeath,
				},
				Content: argument.Content,
			},
		}
		if dbEntity.FollowupDateTime != nil {
			followupDateTime := types.Time(*dbEntity.FollowupDateTime)
			uploadInfo.Info.FollowupDateTime = &followupDateTime
		}
		if uploadInfo.Info.Status > 0 {
			uploadInfo.Info.Status = 1
		}
		if flag {
			go func(patientId uint64, uploadInfo *manage.FollowupInfoInput) {
				businessName := "提交患者随访结果"
				s.client.ManageBusinessPost(businessName, patientId, config.ManagementProviderTypeHbp, s.cfg.Management.Providers.Hbp.Api.Uri.UploadFollowup, uploadInfo)
			}(dbEntity.PatientID, uploadInfo)
			go func(patientId uint64, uploadInfo *manage.FollowupInfoInput) {
				businessName := "提交患者随访结果"
				s.client.ManageBusinessPost(businessName, patientId, config.ManagementProviderTypeDm, s.cfg.Management.Providers.Dm.Api.Uri.UploadFollowup, uploadInfo)
			}(dbEntity.PatientID, uploadInfo)
		} else {
			go func(patientId uint64, providerType config.ManagementProviderType, uri string, uploadInfo *manage.FollowupInfoInput) {
				businessName := "提交患者随访结果"
				s.client.ManageBusinessPost(businessName, patientId, providerType, uri, uploadInfo)
			}(dbEntity.PatientID, providerType, uri, uploadInfo)
		}
	}

	s.WriteNotify(&notify.Message{
		BusinessID: notify.BusinessDoctor,
		NotifyID:   notify.DoctorNewPatientAlert,
		Time:       types.Time(time.Now()),
	})
	s.WriteNotify(&notify.Message{
		BusinessID: notify.BusinessDoctor,
		NotifyID:   notify.DoctorAuditPatient,
		Time:       types.Time(time.Now()),
	})
	s.WriteNotify(&notify.Message{
		BusinessID: notify.BusinessDoctor,
		NotifyID:   notify.DoctorUpdateFollowup,
		Time:       types.Time(time.Now()),
	})

	return serialNo, nil
}

func (s *Followup) SearchFollowupRecordPage(pageIndex, pageSize uint64, filter *doctor.FollowupRecordFilter) (*model.PageResult, business.Error) {
	sqlAccess, err := s.sqlDatabase.NewAccess(false)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}
	defer sqlAccess.Close()

	sqlFilters := make([]database.SqlFilter, 0)
	if filter != nil {
		dbFilter := &sqldb.FollowupRecordFilter{}
		dbFilter.CopyFrom(filter)
		sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
		sqlFilters = append(sqlFilters, sqlFilter)
	}

	dbOrder := &sqldb.FollowupRecordOrder{}
	dbEntity := &sqldb.FollowupRecord{}
	pageResult := &model.PageResult{}
	datas := make([]*doctor.FollowupRecordEx, 0)
	err = sqlAccess.SelectPage(dbEntity, func(total, page, size, index uint64) {
		pageResult.Total = total
		pageResult.Count = page
		pageResult.Size = size
		pageResult.Index = index
	}, func() {
		data := &doctor.FollowupRecordEx{}
		dbEntity.CopyToEx(data)
		datas = append(datas, data)
	}, pageSize, pageIndex, dbOrder, sqlFilters...)
	if err != nil {
		return nil, business.NewError(errors.InternalError, err)
	}
	pageResult.Data = datas

	return pageResult, nil
}

func (s *Followup) CreatePlan(argument *doctor.FollowupPlan) (uint64, business.Error) {
	if argument == nil {
		return 0, business.NewError(errors.InputError, fmt.Errorf("参数为空"))
	}
	if argument.FollowupDate == nil {
		return 0, business.NewError(errors.InputInvalid, fmt.Errorf("缺少随访日期"))
	}

	sqlAccess, err := s.sqlDatabase.NewAccess(false)
	if err != nil {
		return 0, business.NewError(errors.InternalError, err)
	}
	defer sqlAccess.Close()

	sno := uint64(0)
	status := uint64(0)
	dbFilter := &sqldb.FollowupPlanFilter{
		PatientID:    &argument.PatientID,
		Status:       &status,
		FollowUpType: &argument.FollowUpType,
	}
	sqlFilter := s.sqlDatabase.NewFilter(dbFilter, false, false)
	dbEntity := &sqldb.FollowupPlan{}
	err = sqlAccess.SelectOne(dbEntity, sqlFilter)
	if err != nil {
		if !s.sqlDatabase.IsNoRows(err) {
			return 0, business.NewError(errors.InternalError, err)
		}

		dbEntity.CopyFrom(argument)
		now := time.Now()
		dbEntity.CreateDateTime = &now
		sno, err = sqlAccess.InsertSelective(dbEntity)
		if err != nil {
			return 0, business.NewError(errors.InternalError, err)
		}
	} else {
		followupDate := time.Time(*argument.FollowupDate)
		dbEntity.FollowupDate = &followupDate
		_, err = sqlAccess.UpdateByPrimaryKey(dbEntity)
		if err != nil {
			return 0, business.NewError(errors.InternalError, err)
		}
		sno = dbEntity.SerialNo
	}

	s.WriteNotify(&notify.Message{
		BusinessID: notify.BusinessDoctor,
		NotifyID:   notify.DoctorUpdateFollowup,
		Time:       types.Time(time.Now()),
	})

	return sno, nil
}

func (s *Followup) IgnorePlan(argument *doctor.FollowupPlanIgnore) (uint64, business.Error) {
	if argument == nil {
		return 0, business.NewError(errors.InputError, fmt.Errorf("参数为空"))
	}

	now := time.Now()
	dbEntity := &sqldb.FollowupPlanStatusUpdate{
		SerialNo:   argument.SerialNo,
		Status:     2,
		StatusMemo: &argument.StatusMemo,
		UpdateTime: &now,
	}

	sqlAccess, err := s.sqlDatabase.NewAccess(false)
	if err != nil {
		return 0, business.NewError(errors.InternalError, err)
	}
	defer sqlAccess.Close()

	count, err := sqlAccess.UpdateSelectiveByPrimaryKey(dbEntity)
	if err != nil {
		return 0, business.NewError(errors.InternalError, err)
	}

	if count < 1 {
		return 0, nil
	}

	s.WriteNotify(&notify.Message{
		BusinessID: notify.BusinessDoctor,
		NotifyID:   notify.DoctorUpdateFollowup,
		Time:       types.Time(time.Now()),
	})

	providerType := config.ManagementProviderTypeUnknown
	uri := ""
	if argument.FollowUpType == "高血压常规随访" {
		providerType = config.ManagementProviderTypeHbp
		uri = s.cfg.Management.Providers.Hbp.Api.Uri.UploadFollowup
	} else if argument.FollowUpType == "糖尿病常规随访" {
		providerType = config.ManagementProviderTypeDm
		uri = s.cfg.Management.Providers.Dm.Api.Uri.UploadFollowup
	} else {
		return count, nil
	}

	if providerType != config.ManagementProviderTypeUnknown {
		failureReason := "用户忽略"
		followupDateTime := types.Time(time.Now())
		uploadInfo := &manage.FollowupInfoInput{
			PatientID: fmt.Sprint(argument.PatientID),
			Info: manage.FollowupInfo{
				Status: 0,
				FailureInfo: manage.FollowupFailureInfo{
					FailureReason: &failureReason,
				},
				FollowupDateTime: &followupDateTime,
			},
		}

		go func(patientId uint64, providerType config.ManagementProviderType, uri string, uploadInfo *manage.FollowupInfoInput) {
			businessName := "提交患者随访结果"
			s.client.ManageBusinessPost(businessName, patientId, providerType, uri, uploadInfo)
		}(argument.PatientID, providerType, uri, uploadInfo)
	}

	return count, nil
}

func (s *Followup) buildFollowupPlanPageWhere(filter *doctor.ViewFollowupPlanFilterEx, followStatus *uint64, builder database.SqlBuilder) {
	if filter == nil {
		return
	}
	dbFilter := &sqldb.ViewFollowupPlanFilter{}
	dbFilter.CopyFromEx(filter)

	builder.WhereAnd("( `OrgCode` = ? ", dbFilter.OrgCode)
	if len(dbFilter.ManageStatuses) > 0 {
		builder.WhereFormatAnd("`ManageStatus` in %s ", dbFilter.ManageStatuses)
	}
	if filter.DoctorID != nil {
		builder.WhereAnd("( `DoctorID` = ? ", filter.DoctorID)
		builder.WhereOr("`HealthManagerID` = ? ", filter.DoctorID)
		builder.Append(") ")
	}
	if len(filter.PatientName) > 0 {
		builder.WhereAnd("`PatientName` like ? ", fmt.Sprint("%", dbFilter.PatientName, "%"))
	}
	if followStatus != nil {
		builder.WhereAnd("`FollowStatus` = ? ", followStatus)
	}
	builder.Append(") ")

	if filter.Today {
		now := types.Time(time.Now())
		today := now.ToDate(0)
		tomorrow := now.ToDate(1)

		builder.WhereAnd("`FollowupDate` < ? ", tomorrow)

		if followStatus == nil {
			builder.WhereAnd("( `FollowupDate` >= ? ", today)
			builder.WhereOr("`FollowStatus` = ? ", enum.FollowupPlanStatuses.Waiting().Key)
			builder.WhereOr("( `FollowStatus` != ? ", enum.FollowupPlanStatuses.Waiting().Key)
			builder.WhereAnd(" `UpdateTime` >= ? ", today)
			builder.WhereAnd("`UpdateTime` < ? ", tomorrow)
			builder.Append(") ")
			builder.Append(") ")
		} else {
			if *followStatus == enum.FollowupPlanStatuses.Waiting().Key {

			} else {
				builder.WhereAnd("( `FollowupDate` >= ? ", today)
				builder.WhereOr("( `UpdateTime` >= ? ", today)
				builder.WhereAnd("`UpdateTime` < ? ", tomorrow)
				builder.Append(") ")
				builder.Append(") ")
			}
		}

	} else {
		if dbFilter.FollowupDateStart != nil {
			builder.WhereAnd("`FollowupDate` >= ? ", dbFilter.FollowupDateStart)
		}
		if dbFilter.FollowupDateEnd != nil {
			builder.WhereAnd("`FollowupDate` < ? ", dbFilter.FollowupDateEnd)
		}
	}
}
