package main

import (
	"flag"
	"fmt"
	"os"
	"path/filepath"
)

func main() {
	appFolderPath, _ := filepath.Abs(filepath.Dir(os.Args[0]))
	parentFolderPath := filepath.Dir(appFolderPath)
	srcRootPath := filepath.Join(parentFolderPath, "src")
	fileSuffixName := ".vue"

	srcRootPath = "/home/dev/vue/tlcbme_project/doctor/src"
	flag.StringVar(&srcRootPath, "src", srcRootPath, "源代码根目录")
	flag.StringVar(&fileSuffixName, "suf", fileSuffixName, "代码文件后缀名")
	flag.Parse()

	fmt.Println("源代码根目录:", srcRootPath)
	fmt.Println("代码文件后缀名:", fileSuffixName)
	fmt.Println("正在统计...")

	handler := &Handler{
		SrcRootPath:    srcRootPath,
		FileSuffixName: fileSuffixName,
		ExcludeDirs:    make([]string, 0),
	}
	handler.ExcludeDirs = append(handler.ExcludeDirs, "/tool")

	handler.Handle()
}
