package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
	"sync"
)

var (
	mutex *sync.Mutex = new(sync.Mutex)
)

type Handler struct {
	SrcRootPath    string
	FileSuffixName string
	ExcludeDirs    []string
	FileCount      int
	LineCount      int
	CommentCount   int
	EmptyCount     int
}

func (s *Handler) Handle() {

	// sync chan using for waiting
	done := make(chan bool)
	go s.codeLineSum(s.SrcRootPath, done)
	<-done

	fmt.Println("统计完成")
	fmt.Println("文件数:", s.FileCount)
	fmt.Println("代码行:", s.LineCount)
	fmt.Println("--------------------------------------")
	fmt.Println("注释行:", s.CommentCount)
	fmt.Println("空白行:", s.EmptyCount)
	fmt.Println("有效行:", s.LineCount-s.EmptyCount-s.CommentCount)
	fmt.Println("--------------------------------------")
}

// compute souce file line number
func (s *Handler) codeLineSum(root string, done chan bool) {
	var goes int              // children goroutines number
	godone := make(chan bool) // sync chan using for waiting all his children goroutines finished
	isDstDir := s.checkDir(root)
	defer func() {
		if pan := recover(); pan != nil {
			fmt.Printf("root: %s, panic:%#v\n", root, pan)
		}

		// waiting for his children done
		for i := 0; i < goes; i++ {
			<-godone
		}

		// this goroutine done, notify his parent
		done <- true
	}()
	if !isDstDir {
		return
	}

	rootfi, err := os.Lstat(root)
	s.checkerr(err)

	rootdir, err := os.Open(root)
	s.checkerr(err)
	defer rootdir.Close()

	if rootfi.IsDir() {
		fis, err := rootdir.Readdir(0)
		s.checkerr(err)
		for _, fi := range fis {
			if strings.HasPrefix(fi.Name(), ".") {
				continue
			}
			goes++
			if fi.IsDir() {
				go s.codeLineSum(root+"/"+fi.Name(), godone)
			} else {
				go s.readfile(root+"/"+fi.Name(), godone)
			}
		}
	} else {
		goes = 1 // if rootfi is a file, current goroutine has only one child
		go s.readfile(root, godone)
	}
}

func (s *Handler) readfile(filename string, done chan bool) {
	var line int
	var comment int
	var empty int
	isDstFile := strings.HasSuffix(filename, s.FileSuffixName)
	defer func() {
		if pan := recover(); pan != nil {
			fmt.Printf("filename: %s, panic:%#v\n", filename, pan)
		}
		if isDstFile {
			s.addLineNum(line, comment, empty)
			fmt.Printf("file %s complete, 总行数 = %d, 注释行 = %d, 空白行 = %d\n", filename, line, comment, empty)
		}
		// this goroutine done, notify his parent
		done <- true
	}()
	if !isDstFile {
		return
	}

	file, err := os.Open(filename)
	s.checkerr(err)
	defer file.Close()

	reader := bufio.NewReader(file)
	for {
		lineData, isPrefix, err := reader.ReadLine()
		if err != nil {
			break
		}
		if !isPrefix {
			line++

			if lineData == nil {
				empty++
			} else {
				lineText := strings.TrimSpace((string)(lineData))
				lineTextLen := len(lineText)
				if lineTextLen == 0 {
					empty++
				} else if lineTextLen > 1 {
					if lineText[0] == '/' && lineText[1] == '/' {
						comment++
					}
				}
			}
		}
	}
}

// check whether this dir is the dest dir
func (s *Handler) checkDir(dirpath string) bool {
	// 判断该文件夹是否在被排除的范围之内
	if s.ExcludeDirs == nil {
		return true
	}

	for _, dir := range s.ExcludeDirs {
		if s.SrcRootPath+dir == dirpath {
			return false
		}
	}
	return true
}

func (s *Handler) addLineNum(total, comment, empty int) {
	// 获取锁
	mutex.Lock()
	// defer语句在函数返回时调用, 确保锁被释放
	defer mutex.Unlock()
	s.LineCount += total
	s.CommentCount += comment
	s.EmptyCount += empty
	s.FileCount += 1

}

// if error happened, throw a panic, and the panic will be recover in defer function
func (s *Handler) checkerr(err error) {
	if err != nil {
		// 在发生错误时调用panic, 程序将立即停止正常执行, 开始沿调用栈往上抛, 直到遇到recover
		// 对于java程序员, 可以将panic类比为exception, 而recover则是try...catch
		panic(err.Error())
	}
}
