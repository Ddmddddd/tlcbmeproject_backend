package config

type Site struct {
	Doctor Source `json:"doctor"`
	Admin  Source `json:"admin"`
	Doc    Source `json:"doc"`
}
