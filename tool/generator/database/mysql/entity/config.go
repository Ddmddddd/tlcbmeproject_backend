package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"sync"
)

type Config struct {
	mutex sync.RWMutex

	Database CfgDatabase `json:"database" note:"数据库"`
	Entity   CfgPackage  `json:"entity" note:"实体程序包"`
	Model    CfgPackage  `json:"model" note:"模型程序包"`
}

func newConfig() *Config {
	return &Config{
		Database: CfgDatabase{
			Server:   "39.100.99.97",
			Port:     3306,
			Schema:   "cdmwb_dev",
			Charset:  "utf8",
			User:     "zju",
			Password: "zjubio2019",
		},
		Entity: CfgPackage{
			Name:   "sqldb",
			Path:   "tlcbme_project/data/entity/sqldb",
			Folder: "../../src/tlcbme_project/data/entity/sqldb",
		},
		Model: CfgPackage{
			Name:   "doctor",
			Path:   "tlcbme_project/data/model/doctor",
			Folder: "../../src/tlcbme_project/data/model/doctor",
		},
	}
}

func (s *Config) DriverName() string {
	return s.Database.DriverName()
}

func (s *Config) SourceName() string {
	return s.Database.SourceName()
}

func (s *Config) LoadFromFile(filePath string) error {
	s.mutex.Lock()
	defer s.mutex.Unlock()

	bytes, err := ioutil.ReadFile(filePath)
	if err != nil {
		return err
	}

	return json.Unmarshal(bytes, s)
}

func (s *Config) SaveToFile(filePath string) error {
	s.mutex.Lock()
	defer s.mutex.Unlock()

	bytes, err := json.MarshalIndent(s, "", "    ")
	if err != nil {
		return err
	}

	fileFolder := filepath.Dir(filePath)
	_, err = os.Stat(fileFolder)
	if os.IsNotExist(err) {
		os.MkdirAll(fileFolder, 0777)
	}

	file, err := os.Create(filePath)
	if err != nil {
		return err
	}
	defer file.Close()

	_, err = fmt.Fprint(file, string(bytes[:]))

	return err
}

type CfgDatabase struct {
	Server   string `json:"server" note:"服务器名称或IP, 默认127.0.0.1"`
	Port     int    `json:"port" note:"服务器端口, 默认3306"`
	Schema   string `json:"schema" note:"数据库名称, 默认cdmwb"`
	Charset  string `json:"charset" note:"字符集, 默认utf8"`
	User     string `json:"user" note:"登录名"`
	Password string `json:"password" note:"登陆密码"`
}

func (s *CfgDatabase) DriverName() string {
	return "mysql"
}

func (s *CfgDatabase) SourceName() string {
	return fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?charset=%s&parseTime=true&loc=Local",
		s.User,
		s.Password,
		s.Server,
		s.Port,
		s.Schema,
		s.Charset)
}

type CfgPackage struct {
	Name   string `json:"name" note:"包名称"`
	Path   string `json:"path" note:"包路径"`
	Folder string `json:"folder" note:"输出文件夹路径"`
}
