package main

import (
	"database/sql"
	"fmt"

	_ "github.com/go-sql-driver/mysql"
	"strings"
)

type Database struct {
	DriverName string
	SourceName string
	SchemaName string
}

func (s *Database) Tables() ([]*Table, error) {
	entities := make([]*Table, 0)
	db, err := sql.Open(s.DriverName, s.SourceName)
	if err != nil {
		return entities, err
	}
	defer db.Close()

	query := fmt.Sprintf("SELECT `table_name`, `table_comment` from `information_schema`.`tables` where `table_schema`=?")
	rows, err := db.Query(query, s.SchemaName)
	if err != nil {
		return entities, err
	}

	for rows.Next() {
		entity := &Table{}
		err = rows.Scan(&entity.Name, &entity.Comment)
		if err == nil {
			entities = append(entities, entity)
		}
	}

	return entities, nil
}

func (s *Database) Columns(tableName string) ([]*Column, error) {
	entities := make([]*Column, 0)
	db, err := sql.Open(s.DriverName, s.SourceName)
	if err != nil {
		return entities, err
	}
	defer db.Close()

	query := fmt.Sprintf("SELECT `column_name`, `data_type`, `column_comment`, `extra`, `is_nullable`, `column_key` from `information_schema`.`columns` where TABLE_NAME=? and table_schema=?")
	rows, err := db.Query(query, tableName, s.SchemaName)
	if err != nil {
		return entities, err
	}

	extra := ""
	nullAble := ""
	key := ""
	for rows.Next() {
		entity := &Column{}
		err = rows.Scan(&entity.Name, &entity.Type, &entity.Comment, &extra, &nullAble, &key)
		if err == nil {
			if strings.ToLower(extra) == "auto_increment" {
				entity.AutoIncrement = true
			}
			if strings.ToLower(nullAble) == "yes" {
				entity.NullAble = true
			}
			if strings.ToLower(key) == "pri" {
				entity.PrimaryKey = true
			}
			entities = append(entities, entity)
		}
	}

	return entities, nil
}
