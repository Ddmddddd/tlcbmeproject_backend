package main

import (
	"fmt"
	"strings"
)

type Args struct {
	config string

	help   bool
	save   bool
	model  bool
	entity bool
}

func newArgs() *Args {
	return &Args{
		model:  true,
		entity: true,
	}
}

func (s *Args) Parse(key, value string) {
	if key == strings.ToLower("-config") {
		s.config = value
	} else if key == strings.ToLower("-h") ||
		key == strings.ToLower("-help") ||
		key == strings.ToLower("--help") {
		s.help = true
	} else if key == strings.ToLower("-save") {
		s.save = true
	} else if key == strings.ToLower("-model") {
		if strings.ToLower("value") == "false" {
			s.model = false
		}
	} else if key == strings.ToLower("-entity") {
		if strings.ToLower("value") == "false" {
			s.entity = false
		}
	}
}

func (s *Args) ShowHelp() {
	fmt.Println(" -help:		", "[可选]显示帮助")
	fmt.Println(" -config:	", "[可选]指定配置文件路径")
	fmt.Println(" -save:		", "[可选]保存配置文件")
	fmt.Println(" -model:	", "[可选]是否生成数据模型, 如-model=false(默认true)")
	fmt.Println(" -entity:	", "[可选]是否生成数据实体, 如-model=true(默认true)")
}
