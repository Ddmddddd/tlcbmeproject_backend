package main

import (
	"fmt"
	"os"
	"path/filepath"
	"sort"
	"strings"
)

type Package struct {
	Name   string
	Path   string
	Folder string
}

func (s *Package) openFile(name string, overwrite bool) (*os.File, error) {
	path := filepath.Join(s.Folder, fmt.Sprintf("%s.go", name))

	info := ""
	_, err := os.Stat(path)
	if err == nil || os.IsExist(err) {
		info = ", overwrite"
		if !overwrite {
			return nil, os.ErrExist
		}
	}

	os.MkdirAll(s.Folder, 0777)
	file, err := os.OpenFile(path, os.O_WRONLY|os.O_TRUNC|os.O_CREATE, 0666)
	if err != nil {
		return nil, err
	}

	apsPath, _ := filepath.Abs(path)
	fmt.Print("file: ", apsPath)
	fmt.Println(info)

	return file, nil
}

func (s *Package) toFirstUpper(v string) string {
	count := len(v)
	if count == 1 {
		return strings.ToUpper(v)
	} else if count > 1 {
		rs := []rune(v)
		return strings.ToUpper(string(rs[0:1])) + string(rs[1:])
	}

	return v
}

func (s *Package) toFirstLower(v string) string {
	count := len(v)
	if count == 1 {
		return strings.ToUpper(v)
	} else if count > 1 {
		rs := []rune(v)
		return strings.ToLower(string(rs[0:1])) + string(rs[1:])
	}

	return v
}

func (s *Package) toNoPointerType(v string) string {
	rs := []rune(v)
	if len(rs) < 2 {
		return v
	}

	return string(rs[1:])
}

func (s *Package) toRuntimeType(v string, nullAble bool) string {
	if v == "varchar" || v == "text" || v == "json" {
		if nullAble {
			return "*string"
		} else {
			return "string"
		}
	} else if v == "longtext" || v == "blob" {
		return "[]byte"
	} else if v == "int" || v == "int64" || v == "bigint" {
		if nullAble {
			return "*uint64"
		} else {
			return "uint64"
		}
	} else if v == "uint" || v == "uint64" {
		if nullAble {
			return "*uint64"
		} else {
			return "uint64"
		}
	} else if v == "float" || v == "decimal" {
		if nullAble {
			return "*float64"
		} else {
			return "float64"
		}
	} else if v == "datetime" || v == "date" {
		return "*time.Time"
	}

	return v
}

func (s *Package) importPackages(columns []*Column) []string {
	pkgs := make([]string, 0)

	tmps := make(map[string]string, 0)
	for _, column := range columns {
		if column.Type == "datetime" || column.Type == "date" {
			if _, ok := tmps[column.Type]; !ok {
				tmps[column.Type] = column.Type
				pkgs = append(pkgs, "\"time\"")
			}
		} else if column.Type == "json" {
			if _, ok := tmps[column.Type]; !ok {
				tmps[column.Type] = column.Type
				pkgs = append(pkgs, "\"encoding/json\"")
			}
		}
	}

	return pkgs
}

type ModelPackage struct {
	Package
}

func (s *ModelPackage) Generate(table *Table, columns []*Column) error {
	entityFile, err := s.openFile(s.toFirstLower(table.Name), true)
	if err != nil {
		return err
	}
	defer entityFile.Close()
	fmt.Fprintln(entityFile, "package", s.Name)
	fmt.Fprintln(entityFile, "")

	importPkgs := s.importPackages(columns)
	importPkgsCount := len(importPkgs)
	if importPkgsCount == 1 {
		fmt.Fprintln(entityFile, "import", importPkgs[0])
		fmt.Fprintln(entityFile, "")
	} else if importPkgsCount > 1 {
		sort.Slice(importPkgs, func(i, j int) bool {
			return strings.Compare(importPkgs[i], importPkgs[j]) < 0
		})
		fmt.Fprintln(entityFile, "import", "(")
		for _, importPkg := range importPkgs {
			fmt.Fprint(entityFile, "	", importPkg)
			fmt.Fprintln(entityFile)
		}
		fmt.Fprintln(entityFile, ")")
		fmt.Fprintln(entityFile, "")
	}

	entityName := s.toFirstUpper(table.Name)
	fmt.Fprintln(entityFile, "type", entityName, "struct", "{")
	columnNameMaxLength := 0
	columnTypeMaxLength := 0
	for _, column := range columns {
		n := len(s.toFirstUpper(column.Name))
		if columnNameMaxLength < n {
			columnNameMaxLength = n
		}

		n = len(s.toRuntimeType(column.Type, column.NullAble))
		if columnTypeMaxLength < n {
			columnTypeMaxLength = n
		}
	}

	for _, column := range columns {
		columnName := s.toFirstUpper(column.Name)
		fmt.Fprint(entityFile, "	", columnName)
		n := len(columnName)
		for i := n; i <= columnNameMaxLength; i++ {
			fmt.Fprint(entityFile, " ")
		}

		columnType := s.toRuntimeType(column.Type, column.NullAble)
		fmt.Fprint(entityFile, columnType)
		n = len(columnType)
		for i := n; i <= columnTypeMaxLength; i++ {
			fmt.Fprint(entityFile, " ")
		}

		fmt.Fprint(entityFile, "`json:\"", s.toFirstLower(column.Name), "\"")
		fmt.Fprint(entityFile, " note:\"", column.GetNote(), "\"")
		fmt.Fprintln(entityFile, "`")
	}
	fmt.Fprintln(entityFile, "}")

	// ext file
	extFile, err := s.openFile(fmt.Sprintf("%s.ext", s.toFirstLower(table.Name)), false)
	if err != nil {
		if os.IsExist(err) {
			return nil
		} else {
			return err
		}
	}
	defer extFile.Close()
	fmt.Fprintln(extFile, "package", s.Name)

	return nil
}

func (s *ModelPackage) toRuntimeType(v string, nullAble bool) string {
	if v == "datetime" || v == "date" {
		return "*types.Time"
	} else if v == "varchar" || v == "text" || v == "longtext" {
		return "string"
	} else if v == "json" {
		return "interface{}"
	}

	return s.Package.toRuntimeType(v, nullAble)
}

func (s *ModelPackage) importPackages(columns []*Column) []string {
	pkgs := make([]string, 0)

	tmps := make(map[string]string, 0)
	for _, column := range columns {
		if column.Type == "datetime" || column.Type == "date" {
			if _, ok := tmps[column.Type]; !ok {
				tmps[column.Type] = column.Type
				pkgs = append(pkgs, "\"github.com/ktpswjz/httpserver/types\"")
			}
		}
	}

	return pkgs
}

type EntityPackage struct {
	Package
}

func (s *EntityPackage) Generate(table *Table, columns []*Column, modelPkg *ModelPackage) error {
	entityFile, err := s.openFile(s.toFirstLower(table.Name), true)
	if err != nil {
		return err
	}
	defer entityFile.Close()
	fmt.Fprintln(entityFile, "package", s.Name)
	fmt.Fprintln(entityFile, "")

	importPkgs := s.importPackages(columns)
	if modelPkg != nil {
		importPkgs = append(importPkgs, fmt.Sprintf("\"%s\"", modelPkg.Path))
		modelimportPkgs := modelPkg.importPackages(columns)
		if len(modelimportPkgs) > 0 {
			importPkgs = append(importPkgs, modelimportPkgs...)
		}
	}
	importPkgsCount := len(importPkgs)
	if importPkgsCount == 1 {
		fmt.Fprintln(entityFile, "import", importPkgs[0])
		fmt.Fprintln(entityFile, "")
	} else if importPkgsCount > 1 {
		sort.Slice(importPkgs, func(i, j int) bool {
			return strings.Compare(importPkgs[i], importPkgs[j]) < 0
		})
		fmt.Fprintln(entityFile, "import", "(")
		for _, importPkg := range importPkgs {
			fmt.Fprint(entityFile, "	", importPkg)
			fmt.Fprintln(entityFile)
		}
		fmt.Fprintln(entityFile, ")")
		fmt.Fprintln(entityFile, "")
	}

	entityName := s.toFirstUpper(table.Name)
	// base for table name
	entityBaseName := fmt.Sprintf("%sBase", entityName)
	fmt.Fprintln(entityFile, "type", entityBaseName, "struct", "{")
	fmt.Fprintln(entityFile, "}")
	fmt.Fprintln(entityFile, "")
	fmt.Fprint(entityFile, "func (s ", entityBaseName, ") TableName()", " string")
	fmt.Fprintln(entityFile, " {")
	fmt.Fprint(entityFile, "	return \"", table.Name, "\"")
	fmt.Fprintln(entityFile, "")
	fmt.Fprintln(entityFile, "}")
	fmt.Fprintln(entityFile, "")

	entityComments := table.GetComments()
	if len(entityComments) > 0 {
		for _, entityComment := range entityComments {
			fmt.Fprint(entityFile, "//")
			if len(entityComment) > 1 {
				fmt.Fprint(entityFile, " ", entityComment)
			}
			fmt.Fprintln(entityFile)
		}
	}
	fmt.Fprintln(entityFile, "type", entityName, "struct", "{")
	fmt.Fprint(entityFile, "	", entityBaseName)
	fmt.Fprintln(entityFile, "")
	for _, column := range columns {
		note := strings.TrimSpace(column.GetNote())
		fmt.Fprint(entityFile, "	//")
		if len(note) > 0 {
			fmt.Fprint(entityFile, " ", note)
		}
		fmt.Fprintln(entityFile)

		fmt.Fprint(entityFile, "	", s.toFirstUpper(column.Name))
		fmt.Fprint(entityFile, " ", s.toRuntimeType(column.Type, column.NullAble))
		fmt.Fprint(entityFile, " `sql:\"", column.Name, "\"")
		if column.AutoIncrement {
			fmt.Fprint(entityFile, " auto:\"true\"")
		}
		if column.PrimaryKey {
			fmt.Fprint(entityFile, " primary:\"true\"")
		}
		fmt.Fprintln(entityFile, "`")
	}
	fmt.Fprintln(entityFile, "}")

	if modelPkg != nil {
		fmt.Fprintln(entityFile, "")
		fmt.Fprint(entityFile, "func (s *", entityName, ") CopyTo(target *", modelPkg.Name, ".", entityName)
		fmt.Fprintln(entityFile, ") {")
		fmt.Fprintln(entityFile, "	if target == nil {")
		fmt.Fprintln(entityFile, "		return")
		fmt.Fprintln(entityFile, "	}")
		for _, column := range columns {
			columnName := s.toFirstUpper(column.Name)
			sourceType := s.toRuntimeType(column.Type, column.NullAble)
			targetType := modelPkg.toRuntimeType(column.Type, column.NullAble)
			if sourceType == targetType {
				fmt.Fprintf(entityFile, "	target.%s = s.%s", columnName, columnName)
			} else {
				tmpValName := s.toFirstLower(columnName)
				if column.Type == "json" {
					if column.NullAble {
						fmt.Fprint(entityFile, "	", tmpValName, " := \"\"")
						fmt.Fprintln(entityFile)
						fmt.Fprintf(entityFile, "	if s.%s != nil {", columnName)
						fmt.Fprintln(entityFile)
						fmt.Fprint(entityFile, "		", tmpValName, " = *s.", columnName)
						fmt.Fprintln(entityFile)
						fmt.Fprintln(entityFile, "	}")
					} else {
						fmt.Fprint(entityFile, "	", tmpValName, " := s.", columnName)
						fmt.Fprintln(entityFile)
					}

					fmt.Fprintf(entityFile, "	if len(%s) > 0 {", tmpValName)
					fmt.Fprintln(entityFile)
					fmt.Fprintf(entityFile, "		json.Unmarshal([]byte(%s), &target.%s)", tmpValName, columnName)
					fmt.Fprintln(entityFile)
					fmt.Fprintln(entityFile, "	}")

				} else if strings.HasPrefix(targetType, "*") {
					if strings.HasPrefix(sourceType, "*") {
						fmt.Fprintf(entityFile, "	if s.%s == nil {", columnName)
						fmt.Fprintln(entityFile)
						fmt.Fprintf(entityFile, "		target.%s = nil", columnName)
						fmt.Fprintln(entityFile)
						fmt.Fprintln(entityFile, "	}", "else", "{")
						fmt.Fprint(entityFile, "		", tmpValName, " := ", s.toNoPointerType(targetType), "(*s.", columnName, ")")
						fmt.Fprintln(entityFile, "")
						fmt.Fprint(entityFile, "		target.", columnName, " = &", tmpValName)
						fmt.Fprintln(entityFile)
						fmt.Fprint(entityFile, "	}")
					} else {
						fmt.Fprint(entityFile, "	", tmpValName, " := ", s.toNoPointerType(targetType), "(s.", columnName, ")")
						fmt.Fprintln(entityFile, "")
						fmt.Fprint(entityFile, "	target.", columnName, " = &", tmpValName)
					}
				} else {
					if strings.HasPrefix(sourceType, "*") {
						fmt.Fprintf(entityFile, "	if s.%s != nil {", columnName)
						fmt.Fprintln(entityFile)
						fmt.Fprint(entityFile, "		target.", columnName, " = ", targetType, "(*s.", columnName, ")")
						fmt.Fprintln(entityFile)
						fmt.Fprint(entityFile, "	}")
					} else {
						fmt.Fprint(entityFile, "	target.", columnName, " = ", targetType, "(s.", columnName, ")")
					}
				}
			}

			fmt.Fprintln(entityFile, "")
		}
		fmt.Fprintln(entityFile, "}")

		fmt.Fprintln(entityFile, "")
		fmt.Fprint(entityFile, "func (s *", entityName, ") CopyFrom(source *", modelPkg.Name, ".", entityName)
		fmt.Fprintln(entityFile, ") {")
		fmt.Fprintln(entityFile, "	if source == nil {")
		fmt.Fprintln(entityFile, "		return")
		fmt.Fprintln(entityFile, "	}")
		for _, column := range columns {
			columnName := s.toFirstUpper(column.Name)
			sourceType := modelPkg.toRuntimeType(column.Type, column.NullAble)
			targetType := s.toRuntimeType(column.Type, column.NullAble)
			if sourceType == targetType {
				fmt.Fprintf(entityFile, "	s.%s = source.%s", columnName, columnName)
			} else {
				tmpValName := s.toFirstLower(columnName)
				if column.Type == "json" {
					fmt.Fprintf(entityFile, "	if source.%s != nil {", columnName)
					fmt.Fprintln(entityFile)
					fmt.Fprintf(entityFile, "		%sData, err := json.Marshal(source.%s)", tmpValName, columnName)
					fmt.Fprintln(entityFile)
					fmt.Fprintf(entityFile, "		if err == nil {")
					fmt.Fprintln(entityFile)
					fmt.Fprintf(entityFile, "			%s := string(%sData)", tmpValName, tmpValName)
					fmt.Fprintln(entityFile)
					if column.NullAble {
						fmt.Fprint(entityFile, "			s.", columnName, " = &", tmpValName)
					} else {
						fmt.Fprint(entityFile, "			s.", columnName, " = ", tmpValName)
					}
					fmt.Fprintln(entityFile)
					fmt.Fprintln(entityFile, "		}")

					fmt.Fprintln(entityFile, "	}")

				} else if strings.HasPrefix(targetType, "*") {
					if strings.HasPrefix(sourceType, "*") {
						fmt.Fprintf(entityFile, "	if source.%s == nil {", columnName)
						fmt.Fprintln(entityFile)
						fmt.Fprintf(entityFile, "		s.%s = nil", columnName)
						fmt.Fprintln(entityFile)
						fmt.Fprintln(entityFile, "	}", "else", "{")
						fmt.Fprint(entityFile, "		", tmpValName, " := ", s.toNoPointerType(targetType), "(*source.", columnName, ")")
						fmt.Fprintln(entityFile, "")
						fmt.Fprint(entityFile, "		s.", columnName, " = &", tmpValName)
						fmt.Fprintln(entityFile)
						fmt.Fprint(entityFile, "	}")
					} else {
						fmt.Fprint(entityFile, "	", tmpValName, " := ", s.toNoPointerType(targetType), "(source.", columnName, ")")
						fmt.Fprintln(entityFile, "")
						fmt.Fprint(entityFile, "	s.", columnName, " = &", tmpValName)
					}
				} else {
					if strings.HasPrefix(sourceType, "*") {
						fmt.Fprintf(entityFile, "	if source.%s != nil {", columnName)
						fmt.Fprintln(entityFile)
						fmt.Fprint(entityFile, "		s.", columnName, " = ", targetType, "(*source.", columnName, ")")
						fmt.Fprintln(entityFile)
						fmt.Fprint(entityFile, "	}")
					} else {
						fmt.Fprint(entityFile, "	s.", columnName, " = ", targetType, "(source.", columnName, ")")
					}
				}
			}
			fmt.Fprintln(entityFile, "")
		}
		fmt.Fprintln(entityFile, "}")
	}

	// ext file
	extFile, err := s.openFile(fmt.Sprintf("%s.ext", s.toFirstLower(table.Name)), false)
	if err != nil {
		if os.IsExist(err) {
			return nil
		} else {
			return err
		}
	}
	defer extFile.Close()
	fmt.Fprintln(extFile, "package", s.Name)

	return nil
}
