package main

import "strings"

type Table struct {
	Name    string
	Comment string
}

func (s *Table) GetComments() []string {
	comments := make([]string, 0)
	if len(s.Comment) > 0 {
		lines := strings.Split(s.Comment, "\n")
		for _, line := range lines {
			if len(line) > 0 {
				comments = append(comments, line)
			}
		}
	}

	return comments
}
