package main

import (
	"fmt"
	"os"
	"time"
)

func main() {
	now := time.Now()
	db := &Database{
		DriverName: cfg.DriverName(),
		SourceName: cfg.SourceName(),
		SchemaName: cfg.Database.Schema,
	}
	ts, err := db.Tables()
	if err != nil {
		fmt.Println(err)
		os.Exit(0)
	}
	if len(ts) < 1 {
		fmt.Println("no tables")
		os.Exit(0)
	}

	pkgModel := &ModelPackage{}
	pkgModel.Name = cfg.Model.Name
	pkgModel.Path = cfg.Model.Path
	pkgModel.Folder = cfg.Model.Folder

	pkgEntity := &EntityPackage{}
	pkgEntity.Name = cfg.Entity.Name
	pkgEntity.Path = cfg.Entity.Path
	pkgEntity.Folder = cfg.Entity.Folder

	modelCount := 0
	entityCount := 0

	for _, table := range ts {
		fmt.Println("table:", table.Name)
		cs, err := db.Columns(table.Name)
		if err != nil {
			fmt.Println(err)
			os.Exit(0)
		}

		if args.model {
			err = pkgModel.Generate(table, cs)
			if err != nil {
				fmt.Println(err)
				os.Exit(0)
			}
			modelCount++
		}

		if args.entity {
			err = pkgEntity.Generate(table, cs, pkgModel)
			if err != nil {
				fmt.Println(err)
				os.Exit(0)
			}
			entityCount++
		}
	}

	fmt.Println("tables:", len(ts), ", models:", modelCount, ", time elapsed:", time.Now().Sub(now))
}
