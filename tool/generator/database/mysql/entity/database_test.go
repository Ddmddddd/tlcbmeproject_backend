package main

import "testing"

func TestDatabase_Tables(t *testing.T) {
	cfg := newConfig()
	db := &Database{
		DriverName: cfg.DriverName(),
		SourceName: cfg.SourceName(),
		SchemaName: cfg.Database.Schema,
	}

	ts, err := db.Tables()
	if err != nil {
		t.Fatal(err)
	}
	t.Log(ts)
}

func TestDatabase_Columns(t *testing.T) {
	cfg := newConfig()
	db := &Database{
		DriverName: cfg.DriverName(),
		SourceName: cfg.SourceName(),
		SchemaName: cfg.Database.Schema,
	}

	cs, err := db.Columns("test")
	if err != nil {
		t.Fatal(err)
	}
	t.Log(cs)
}
