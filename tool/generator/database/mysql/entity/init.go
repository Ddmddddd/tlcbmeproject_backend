package main

import (
	"fmt"
	"github.com/ktpswjz/httpserver/types"
	"os"
	"path/filepath"
)

const (
	moduleType    = "tool"
	moduleName    = "entity"
	moduleRemark  = "慢病管理平台数据库实体生成工具"
	moduleVersion = "1.0.1.0"
)

var (
	cfg  = newConfig()
	args = newArgs()
)

func init() {
	moduleArgs := &types.Args{}
	moduleArgs.Parse(os.Args, moduleType, moduleName, moduleVersion, moduleRemark, args)

	rootFolder := filepath.Dir(moduleArgs.ModuleFolder())
	configPath := args.config
	if configPath == "" {
		configPath = filepath.Join(rootFolder, "cfg", "tlcbme_project.entity.json")
	}

	_, err := os.Stat(configPath)
	if os.IsNotExist(err) {
		err = cfg.SaveToFile(configPath)
		if err != nil {
			fmt.Println("generate configure file fail: ", err)
		}
	} else {
		err = cfg.LoadFromFile(configPath)
		if err != nil {
			fmt.Println("load configure file fail: ", err)
		}
	}

	if args.save {
		err = cfg.SaveToFile(configPath)
		if err != nil {
			fmt.Println("generate configure file fail:", err)
		} else {
			fmt.Println("generate configure file success:", configPath)
		}
		os.Exit(0)
	}
}
