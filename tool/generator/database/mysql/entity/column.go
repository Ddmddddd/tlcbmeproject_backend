package main

import "strings"

type Column struct {
	Name          string
	Type          string
	Comment       string
	AutoIncrement bool
	NullAble      bool
	PrimaryKey    bool
}

func (s *Column) GetName() string {
	cs := strings.Split(s.Comment, "\n")
	if len(cs) > 0 {
		return cs[0]
	}

	return ""
}

func (s *Column) GetExample() string {
	cs := strings.Split(s.Comment, "\n")
	if len(cs) > 1 {
		return cs[1]
	}

	return ""
}

func (s *Column) GetComment() string {
	cs := strings.Split(s.Comment, "\n")
	if len(cs) > 3 {
		return cs[3]
	}

	return ""
}

func (s *Column) GetNote() string {
	note := &strings.Builder{}
	cs := strings.Split(s.Comment, "\n")
	if len(cs) > 0 {
		note.WriteString(cs[0])
	}
	if len(cs) > 3 {
		note.WriteString(" ")
		note.WriteString(cs[3])
	}
	if len(cs) > 1 {
		note.WriteString(" 例如:")
		note.WriteString(cs[1])
	}

	return note.String()
}
